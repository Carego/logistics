<div ng-show="operatorLogin || superAdminLogin" title="Branch Stock Information">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		
		<!-- Find By Branch Form -->
		<form name="stnTransferForm" ng-submit="submitTransferForm(stnTransferForm, transfer)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="branchCode" name="branchCode" ng-model="transfer.branchCode" ng-click="OpenBranchDB()" ng-required="true" readonly="readonly">
					<label>To Branch</label>
				</div>
				
				<div class="col s3 input-field">
		    		<select name="transferType" id="transferType" ng-model="transfer.transferType" ng-required="true" ng-init="transfer.transferType = 'cnmt'">
						<option value='cnmt'>CNMT</option>
						<option value='chln'>CHALLAN</option>
						<option value='sedr'>SEDR</option>
					</select>
					<label>Type</label>
				</div>       
				
				<div class="col s3 input-field">
		       		<input class="validate" type ="text" ng-click="openTotalDB()" readonly>
					<label>Start No.</label>
				</div>				
					
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" id="stnSubmit" name ="stnSubmit" value="Transfer" ng-disabled="submitButton">
	       		</div>	
			</div>		
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="openTotalDB" ng-hide="openTotalDBFlag">
		<input type="text" name="Total No." ng-model="totalNo"  placeholder="Total No" ng-change="make()">
		<table>			
			<tr ng-repeat="no in noList">				
				<td>
					<input type="text" name="transfer.startList[$index]" id="transfer.startList[$index]" ng-model="transfer.startList[$index]" placeholder="Start No" ng-required>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Check Available" ng-click="isStnAvailable()">
				</td>
			</tr>
		</table>
	</div>
	
	</form>

</div>