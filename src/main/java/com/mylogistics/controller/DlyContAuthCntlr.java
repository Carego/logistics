package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DlyContAuthDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DlyContAuth;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DlyContAuthCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private DlyContAuthDAO dlyContAuthDAO;
	
	@RequestMapping(value = "/getDCACustCode", method = RequestMethod.POST)
	public @ResponseBody Object getDCACustCode() {
		System.out.println("enter into getDCACustCode function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Customer> custList = customerDAO.getAllCustomer();
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getDCAStateList", method = RequestMethod.POST)
	public @ResponseBody Object getDCAStateList() {
		System.out.println("enter into getDCAStateList function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Station> stList = stationDAO.getStationData();
		if(!stList.isEmpty()){
			map.put("list",stList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/saveDCA", method = RequestMethod.POST)
	public @ResponseBody Object saveDCA(@RequestBody DlyContAuth dlyContAuth) {
		System.out.println("enter into saveDCA function");
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		dlyContAuth.setbCode(currentUser.getUserBranchCode());
		dlyContAuth.setUserCode(currentUser.getUserCode());
		int temp = dlyContAuthDAO.saveDCA(dlyContAuth);
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/chkExstFrCust", method = RequestMethod.POST)
	public @ResponseBody Object chkExstFrCust(@RequestBody String custCode) {
		System.out.println("enter into saveDCA function");
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<DlyContAuth> dcaList = dlyContAuthDAO.getDCAbyCustCode(custCode);
		System.out.println("size of dcaList = "+dcaList.size());
		if(!dcaList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		return map;
	}
}
