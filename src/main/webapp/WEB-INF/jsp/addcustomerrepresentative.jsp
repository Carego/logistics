<title>addcr</title>

<div class="row">
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
	<form class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="AddCustomerRepForm" form
		ng-submit="nextAddCR(AddCustomerRepForm,custrep)">
		<div class="row">
			<div class="input-field col s12 m6 l6">
				<input type="text" id="text2" name="custCode"
					ng-model="custrep.custCode" readonly> <label>Customer
					Code</label>
			</div>
			<div class="input-field col s12 m6 l6">
				<input type="text" name="custRefNo" id="custRefNo"
					ng-model="custrep.custRefNo" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label>Customer Reference
					Number</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input type="text" id="crName" name="crName"
					ng-model="custrep.crName" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label>Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m4 l4">
				<input type="text" name="crDesignation" id="crDesignation"
					ng-model="custrep.crDesignation" ng-minlength="3" ng-maxlength="40"
					ng-model-options="{ debounce: 200 }"
					ng-keyup="getCustDesigList(custrep)" ng-blur="fillCustDesigVal()"
					ng-required="true"> <label>Designation</label>
			</div>
			<div class="input-field col s12 m4 l4">
				<input type="text" name="crMobileNo" id="crMobileNo"
					ng-model="custrep.crMobileNo" ng-minlength="4" ng-maxlength="15"
					ng-required="true"> <label>Mobile No</label>
			</div>
			<div class="input-field col s12 m4 l4">
				<input type="email" name="crEmailId" ng-model="custrep.crEmailId"
					ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/"
					ng-maxlength="40" ng-required="true"> <label>Email
					ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 center">
				<input type="submit" value="Submit">
			</div>
		</div>

	</form>
</div>
<%-- 
		<form name="AddCustomerRepForm" form ng-submit="nextAddCR(AddCustomerRepForm,custrep)">
	
		<table border="0">
			
				<tr>
					<td>Customer Code: *</td>
					<td><input type ="text" id="text2" name ="custCode" ng-model="custrep.custCode" readonly>
				</tr>
			
				<tr>
					<td>Customer Representative Name: *</td>
					<td><input type="text" name ="crName"  ng-model="custrep.crName" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer Representative Designation: *</td>
					<td><input type="text" name ="crDesignation" ng-model="custrep.crDesignation" ng-minlength="3" ng-maxlength="40"  ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer Representative Mobile No.: *</td>
					<td><input type="number" name ="crMobileNo" id="crMobileNo" ng-model="custrep.crMobileNo" ng-minlength="4" ng-maxlength="15" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer Representative Email Id: *</td>
					<td><input type="email" name ="crEmailId" ng-model="custrep.crEmailId" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40" ng-required="true"></td>	
				</tr>
				
				<tr>
					<td>Customer Reference Number: *</td>
					<td><input type="text" name ="custRefNo"  ng-model="custrep.custRefNo" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>	
				</tr>
				
				<tr>
					<td colspan="2"><input type="submit" value="Submit"></td>
				</tr>
			</table>
		</form>
 --%>