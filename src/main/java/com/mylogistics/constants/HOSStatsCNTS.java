package com.mylogistics.constants;

public class HOSStatsCNTS {

	public static final String HOSS_ID = "hOSStatsId";
	
	public static final String HOSS_BRCODE = "hOSStatsBranchCode";
	
	public static final String HOSS_CNMT_ODR = "hOSStatsCnmtOdr";
	
	public static final String HOSS_CNMT_PER = "hOSStatsCnmtPer";
	
	public static final String HOSS_CNMT_AVG_PER = "hOSStatsCnmtAvgPer";
	
	public static final String HOSS_CNMT_E_ODR = "hOSStatsCnmtExptOdr";
	
	public static final String HOSS_CHLN_ODR = "hOSStatsChlnOdr";
	
	public static final String HOSS_CHLN_PER = "hOSStatsChlnPer";
	
	public static final String HOSS_CHLN_AVG_PER = "hOSStatsChlnAvgPer";
	
	public static final String HOSS_CHLN_E_ODR = "hOSStatsChlnExptPer";
	
	public static final String HOSS_SEDR_ODR = "hOSStatsSedrOdr";
	
	public static final String HOSS_SEDR_PER = "hOSStatsSedrPer";
	
	public static final String HOSS_SEDR_AVG_PER = "hOSStatsAvgPer";
	
	public static final String HOSS_SEDR_E_ODR = "hOSStatsExptOdr";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
