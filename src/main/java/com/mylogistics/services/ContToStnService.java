package com.mylogistics.services;

import java.util.List;

import com.mylogistics.model.ContToStn;

public interface ContToStnService {

	public List<ContToStn> getAllCTS();

    public void addCTS(ContToStn contToStn);

    public void deleteCTS(ContToStn contToStn);

    public void deleteAllCTS();
}
