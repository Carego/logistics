package com.mylogistics.model;


import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transfervehicle")
public class TransferVehicle {
	
	@Id
	@Column(name="tvId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int tvId;
	
	@Column(name="vvId")
	private int vvId;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="userCode",length=5)
	private String userCode;
	
	@Column(name="ownId")
	private int ownId; 
	
	@Column(name="oldOwnId")
	private int oldOwnId;
	
	@Column(name="transferDate")
	private Date transferDate;
	
	@Column(name="transferId",length=64)
	private String transferId;

	public int getTvId() {
		return tvId;
	}

	public void setTvId(int tvId) {
		this.tvId = tvId;
	}

	public int getVvId() {
		return vvId;
	}

	public void setVvId(int vvId) {
		this.vvId = vvId;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getOwnId() {
		return ownId;
	}

	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}

	public int getOldOwnId() {
		return oldOwnId;
	}

	public void setOldOwnId(int oldOwnId) {
		this.oldOwnId = oldOwnId;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public String getTransferId() {
		return transferId;
	}

	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}
	

}
