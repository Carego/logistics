<div ng-show="operatorLogin || superAdminLogin">
<title>Add Station</title>
<div class="row">
	<div class="col s2 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s8 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="StationForm" ng-submit="Submit(StationForm,station)">
      		
      		<div class="row">
	<div  class="input-field col s4">
		<input type="text" name="stateName" id="stateNameId" ng-model="stateName" ng-click="getState()" readonly required>
		<label>State Name</label>
	</div>
	<div  class="input-field col s4">
		<input type="text" name="districtName" id="districtNameId" ng-model="station.stnDistrict" ng-click="getState()" readonly required>
		<label>District Name</label>
	</div>
	<div  class="input-field col s4">
		<input type="text" name="cityName" id="cityNameId" ng-model="station.stnCity" ng-click="getState()" readonly required>
		<label>City Name</label>
	</div>
	<div  class="input-field col s4">
		<input type="text" name="stnName" id="stnNameId" ng-model="station.stnName" ng-click="getState()" readonly required>
		<label>Station Name</label>
	</div>
	<div  class="input-field col s4">
		<input type="text" name="pinCode" id="pinCodeId" ng-model="station.stnPin" ng-keyup="getStnByPin()" minlength="6" maxlength="6" required>
		<label>PIN Code</label>
	</div>
	
	</div>
      		
      		
      		<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
      			</div>
			</form>
			<div class="col s2"> &nbsp; </div>
   </div>

<div id="viewStationDetailsDB" ng-hide="viewStationDetailsFlag">

<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Station <span class="teal-text text-lighten-2">{{station.stnName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Name:</td>
	                <td>{{station.stnName}}</td>
	            </tr>
	            
	            <tr>
	                <td>District:</td>
	                <td>{{station.stnDistrict}}</td>
	            </tr>
	            
	            <tr>
	                <td>State Code:</td>
	                <td>{{station.stateCode}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{station.stnPin}}</td>
	            </tr>
	      </table>
	      
<input type="button" value="Save" ng-click="saveStation(station)">
<input type="button" value="Cancel" ng-click="closeViewStationDetailsDB()">
	      
	  </div>
	</div>         
</div>







	<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnName">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.pinCode }}</td>
				<td>{{ stn.stationName }}</td>
				<td>{{ stn.city }}</td>
				<td>{{ stn.district }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	

</div>