var app = angular.module('application');

app.controller('BankDetOwnCntlr',['$scope','$location','$http','FileUploadService','$filter',
                                      function($scope,$location,$http,FileUploadService,$filter){
	console.log("BankDetOwnCntlr started");


	$scope.ownerCodeDBFlag = true;
	$scope.owner = {};
	$scope.ownerCodeList = [];
	
	var chq=0;

	

	$scope.saveOwnerCode = function(ownrCode){
		console.log("enter into saveOwnerCode----->"+ownrCode);
		$scope.owner.ownCode = ownrCode;
		$('div#ownerCodeDB').dialog('close');
		$scope.ownerCodeDBFlag = true;

	}

	$scope.getOwnCodeByIsBnk = function(ownCode){
		
		if(ownCode.length<3)
			return;
		var response = $http.post($scope.projectName+'/getOwnCodeByIsBnk',ownCode);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("Owner Code  List--->>"+data.ownerCodeList);
				$scope.ownerCodeList = data.ownerCodeList;
				
				$scope.ownerCodeDBFlag = false;
				$('div#ownerCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Owner Code",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.ownerCodeDBFlag = true;
					}
				});

				$('div#ownerCodeDB').dialog('open');
				
			}else{
				console.log("Error in bringing data from getBrkCodeByIsBnk");
			}

		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});

	}

	$scope.addBankOwnDet = function(BankDetOwnForm){
		console.log("enter into addBankOwnDet"); 
		if(BankDetOwnForm.$invalid){
			console.log("Form Invalid");
			$scope.alertToast("Form Invalid");
		}else{
			
			console.log("Onwer code "+$scope.owner.ownCode);
			console.log("Owner Account number --"+$scope.owner.ownAccntNo);
			console.log("Owner ifsc "+$scope.owner.ownIfsc);
			console.log("Owner bank micr" + $scope.owner.ownMicr);
			console.log("Owner bank branch "+$scope.owner.ownBnkBranch);
			
			if(chq==0){
				$scope.alertToast("Upload image first");
				return;
			}
			
			var response = $http.post($scope.projectName+'/updateOwnBnkD',$scope.owner);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					
					$scope.alertToast("Success");
					for(var i = 0 ; i < $scope.ownerCodeList.length;i++){
						if($scope.owner.ownCode === $scope.ownerCodeList[i] ){
							$scope.ownerCodeList.splice(i,1);
						}
					}
					$scope.owner = {};
					chq=0;
				}else{
					console.log("Error in bringing data from updateOwnBnkD");
					$scope.alertToast("Error");
				}

			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});	
		}
		
	}
	
	
	$scope.uploadCCImage = function(ccImage){
		console.log("enter into uploadDecImg function");
		var file = ccImage;
		
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/upldOwnChqImgBnkN";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		       chq=1;
		}	
	}
	
	//$scope.getOwnCodeByIsBnk();

	console.log("BankDetOwnCntlr ended");
}]);