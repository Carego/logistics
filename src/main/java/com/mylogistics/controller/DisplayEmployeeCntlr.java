package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.EmployeeOldDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.EmployeeOld;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayEmployeeCntlr {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private EmployeeOldDAO employeeOldDAO;
	
	@Autowired
	private BranchDAO branchDAO;
		
	@Autowired
	private HttpSession httpSession;

	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@RequestMapping(value="/displayEmpDataForAdmin",method = RequestMethod.POST)
	public @ResponseBody Object displayEmpDataForAdmin(){
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode=currentUser.getUserBranchCode();
		System.out.println("entered into displayEmpDataForAdmin----"+branchCode);
		List<Employee> employeeList = employeeDAO.getAllEmployeeForAdmin(branchCode);
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(employeeList.isEmpty())) {
			   map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			   map.put("list",employeeList);
  		}else {
  			map.put(ConstantsValues.RESULT, "No more employee to show...");
		}
		return map;	
	}
	
	@RequestMapping(value="/getAllEmpListForAdmin",method = RequestMethod.POST)
	public @ResponseBody Object getAllEmpListForAdmin(){
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode=currentUser.getUserBranchCode();
		System.out.println("branch code of current user------>"+branchCode);
		
		List<Employee> empCodeList = employeeDAO.getAllEmpForAdmin(branchCode);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(empCodeList.isEmpty())) {			
			   map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			   map.put("empCodeList",empCodeList);
  		}else {
  			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
		
	@RequestMapping(value = "/updateIsViewEmployee", method = RequestMethod.POST)
    public @ResponseBody Object updateIsViewEmployee(@RequestBody String selection) {

        String contids[] = selection.split(",");
        int temp=employeeDAO.updateEmployeeisViewTrue(contids);
        Map<String, Object> map = new HashMap<String, Object>();
        if(temp>0){
            map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }
        else{
            map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }
        return map;
    }
		
	@RequestMapping(value = "/editEmployeeSubmit", method = RequestMethod.POST)
    public @ResponseBody Object editEmployeeSubmit(@RequestBody Employee employee){
                
        Map<String,String> map = new HashMap<String, String>();
        
        int oldEmployeeId = employee.getEmpId();
        List<Employee> emplist = employeeDAO.getOldEmployee(oldEmployeeId);
        if (!(emplist.isEmpty())) {
        	 EmployeeOld employeeOld = new EmployeeOld();
        	 
        	 employeeOld.setEmpId(emplist.get(0).getEmpId());
        	 employeeOld.setEmpName(emplist.get(0).getEmpName());
        	 employeeOld.setEmpCode(emplist.get(0).getEmpCode());
        	 employeeOld.setEmpAdd(emplist.get(0).getEmpAdd());
        	 employeeOld.setEmpPin(emplist.get(0).getEmpPin());
        	 employeeOld.setEmpCity(emplist.get(0).getEmpCity());
        	 employeeOld.setEmpState(emplist.get(0).getEmpState());
        	 employeeOld.setEmpSex(emplist.get(0).getEmpSex());
        	 employeeOld.setEmpDOB(emplist.get(0).getEmpDOB());
        	 employeeOld.setEmpDOAppointment(emplist.get(0).getEmpDOAppointment());
        	 employeeOld.setEmpDesignation(emplist.get(0).getEmpDesignation());
        	 employeeOld.setEmpBankAcNo(emplist.get(0).getEmpBankAcNo());
        	 employeeOld.setEmpBankBranch(emplist.get(0).getEmpBankBranch());
        	 employeeOld.setEmpBankIFS(emplist.get(0).getEmpBankIFS());
        	 employeeOld.setEmpBankName(emplist.get(0).getEmpBankName());
        	 employeeOld.setEmpBasic(emplist.get(0).getEmpBasic());
        	 employeeOld.setEmpEduQuali(emplist.get(0).getEmpEduQuali());
        	 employeeOld.setEmpESI(emplist.get(0).getEmpESI());
        	 employeeOld.setEmpESIDispensary(emplist.get(0).getEmpESIDispensary());
        	 employeeOld.setEmpESINo(emplist.get(0).getEmpESINo());
        	 employeeOld.setEmpFatherName(emplist.get(0).getEmpFatherName());
        	 employeeOld.setEmpGross(emplist.get(0).getEmpGross());
        	 employeeOld.setEmpHRA(emplist.get(0).getEmpHRA());
        	 employeeOld.setEmpLicenseExp(emplist.get(0).getEmpLicenseExp());
        	 employeeOld.setEmpLicenseNo(emplist.get(0).getEmpLicenseNo());
        	 employeeOld.setEmpLoanBal(emplist.get(0).getEmpLoanBal());
        	 employeeOld.setEmpLoanPayment(emplist.get(0).getEmpLoanPayment());
        	 employeeOld.setEmpMailId(emplist.get(0).getEmpMailId());
        	 employeeOld.setEmpMobAmt(emplist.get(0).getEmpMobAmt());
        	 employeeOld.setEmpNominee(emplist.get(0).getEmpNominee());
        	 employeeOld.setEmpOtherAllowance(emplist.get(0).getEmpOtherAllowance());
        	 employeeOld.setEmpPanNo(emplist.get(0).getEmpPanNo());
        	 employeeOld.setEmpPF(emplist.get(0).getEmpPF());
        	 employeeOld.setEmpPFNo(emplist.get(0).getEmpPFNo());
        	 employeeOld.setEmpPhNo(emplist.get(0).getEmpPhNo());
        	 employeeOld.setEmpPresentAdd(emplist.get(0).getEmpPresentAdd());
        	 employeeOld.setEmpPresentCity(emplist.get(0).getEmpPresentCity());
        	 employeeOld.setEmpPresentPin(emplist.get(0).getEmpPresentPin());
        	 employeeOld.setEmpPresentState(emplist.get(0).getEmpPresentState());
        	 employeeOld.setEmpTelephoneAmt(emplist.get(0).getEmpTelephoneAmt());
        	 employeeOld.setbCode(emplist.get(0).getbCode());
        	 employeeOld.setBranchCode(emplist.get(0).getBranchCode());
        	 employeeOld.setUserCode(emplist.get(0).getUserCode());
        	 employeeOld.setNetSalary(emplist.get(0).getNetSalary());
        	 employeeOld.setIsTerminate(emplist.get(0).getIsTerminate());
        	 employeeOld.setView(emplist.get(0).isView());
        	 
        	 int tmp = employeeOldDAO.saveEmployeeOldToDB(employeeOld);
        	 if (tmp>=0) {
 	        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			 }else {
				  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			 }
        }else{
        	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
        }
        
        int temp = employeeDAO.updateEmployeeToDB(employee);
        if (temp>=0) {
        	FAMaster fAMaster = new FAMaster();
        	List<FAMaster> fAMasterlist = faMasterDAO.retrieveFAMaster(employee.getEmpFaCode());
        	  		
    		if (!(fAMasterlist.isEmpty())) {
    			fAMaster = fAMasterlist.get(0);
    			fAMaster.setFaMfaName(employee.getEmpName());
    		    int tmp = faMasterDAO.updateFaMaster(fAMaster);
    		    if(tmp >= 0){
    		    	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
    		    }else{
    		    	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
    		    }
    		}else{
    		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
    		}
        	
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
    	return map;
    }
	
	@RequestMapping(value = "/getBranchIsNCR", method = RequestMethod.POST)
    public @ResponseBody Object getBranchIsNCR(@RequestBody String branchCode) {
		System.out.println("Entered into getBranchIsNCR function in controller"+branchCode);

		Branch branch = new Branch();
		List<Branch> list = branchDAO.retrieveBranch(branchCode);
  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			branch = list.get(0);
			String isNCR = branch.getIsNCR();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("isNCR",isNCR);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	 }
	
	@RequestMapping(value = "/getModifiedEmpDetail", method = RequestMethod.POST)
	  public @ResponseBody Object getModifiedEmpDetail(@RequestBody String empCode){
		 
	   
		List<EmployeeOld> empList = employeeOldDAO. getModifiedEmp(empCode);
		  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(empList.isEmpty())) {
			
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("list",empList);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	}
	
	 @RequestMapping(value = "/empDataByEmpCodeTemp", method = RequestMethod.POST)
	  public @ResponseBody Object empDataByEmpCodeTemp(@RequestBody String empCodeTemp){
		 
	    Employee employee = new Employee();
		List<Employee> list = employeeDAO.empDataByEmpCodeTemp(empCodeTemp);
		  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			employee = list.get(0);
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("employee",employee);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	   }
	
	
}
