'use strict';

var app = angular.module('application');

app.controller('OnAccMRCntlrN',['$scope','$location','$http','$filter','$sce',
                              function($scope,$location,$http,$filter,$sce){
	
	$scope.mr = {};
	$scope.customer = {};
	$scope.bank = {};
	
	$scope.custList = [];
	$scope.bankList = [];
	
	$scope.selCustFlag = true;
	$scope.selBankFlag = true;
	$scope.isRequired = true;
	$scope.duplicateFlag = true;
	$scope.showFormErrorMsg = false;
	$scope.commonValidationFlag = false;
	$scope.chequeValidationFlag = false;
	$scope.rtgsValidationFlag = false;
	$scope.isFormSubmitted = false;
	
	$('#netAmtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#netAmtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 15)){
			e.preventDefault();
		}
	});
	
	
	$('#chqId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#chqId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 10)){
			e.preventDefault();
		}
	});
	
	
	$scope.getMrDetail = function(){
		console.log("enter into getMrDetail function");
		var response = $http.post($scope.projectName+'/getMrDetail');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.csDt = data.cssDt;
				  $scope.cssDt = data.cssDt;
				  $scope.mr.mrDate = $scope.csDt;
				  if($scope.currentBranch === "CRPO") {
					  $("#csDtId").attr("readonly", false);
				  } else {
					  $("#csDtId").attr("readonly", true);
				  }
				  $scope.custList = data.list;
				  $scope.bankList = data.bnkList;
			  } else {
				  $scope.errorToast("SERVER ERROR");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
	
	$scope.chngDt = function(){
		console.log("enter into chngDt function");
		if($scope.csDt > $scope.cssDt){
			$scope.alertToast("You can't enter greater than "+$scope.mr.mrDate+" date");
			$scope.csDt = $scope.mr.mrDate;
		}else{
			$scope.mr.mrDate = $scope.csDt;
		}
	}
	
	$scope.selectCust = function(){
		console.log("enter into selectCust function");
		$scope.selCustFlag = false;
		$('div#selCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCustFlag = true;
		    }
			});
		
		$('div#selCustId').dialog('open');	
	}
	
	
	
	$scope.saveCustomer = function(cust){
		console.log("enter into saveCustomer function");
		$('div#selCustId').dialog('close');	
		$scope.customer = cust;
		$scope.mr.mrCustId = cust.custId;
	}
	
	$scope.selectBank = function(){
		console.log("enter into selectBank function");
		$scope.selBankFlag = false;
		$('div#selBankId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Bank",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();        
		        $scope.selBankFlag = true;
		    }
			});
		
		$('div#selBankId').dialog('open');	
	}
	
	
	$scope.saveBank = function(bnk){
		console.log("enter into saveBank funciton");
		$('div#selBankId').dialog('close');
		$scope.bank = bnk;
		$scope.mr.mrBnkId = bnk.bnkId;
	}
	
	
	$scope.chngMRPay = function(){
		console.log("enter into chngMRPay function");
		if($scope.mr.mrPayBy === 'C'){
			
			$('#bnkId').attr("disabled","disabled");
			$scope.bank.bnkName = "";
			$scope.mr.bnkId = "";
			
			$('#chqId').attr("disabled","disabled");
			$scope.mr.mrChqNo = "";
			
			$('#custBnkId').attr("disabled","disabled");
			$scope.mr.mrCustBnkName = "";
			
			$('#refId').attr("disabled","disabled");
			$scope.mr.mrRtgsRefNo = "";
			
			$scope.isRequired = false;
			$scope.commonValidationFlag = false;
			$scope.chequeValidationFlag = false;
			$scope.rtgsValidationFlag = false;
			
		}else if($scope.mr.mrPayBy === 'Q'){
			
			$('#bnkId').removeAttr("disabled");
			$('#chqId').removeAttr("disabled");
			$('#custBnkId').removeAttr("disabled");
			$('#refId').attr("disabled","disabled");

			$scope.mr.mrRtgsRefNo = "";
			
			$scope.commonValidationFlag = true;
			$scope.chequeValidationFlag = true;
			$scope.rtgsValidationFlag = false;
			$scope.isRequired = true;
			
		}else if($scope.mr.mrPayBy === 'R'){
			
			$scope.mr.mrChqNo = "";
			$('#chqId').attr("disabled","disabled");
			$('#bnkId').removeAttr("disabled");
			$('#refId').removeAttr("disabled");
			$('#custBnkId').removeAttr("disabled");
			
			$scope.isRequired = true;
			$scope.commonValidationFlag = true;
			$scope.chequeValidationFlag = false;
			$scope.rtgsValidationFlag = true;
			
		}else{
			console.log("invalid Receipt method");
			$scope.isRequired = true;
		}
	}
	
	$scope.mrSubmit = function(mrForm){
		console.log("enter into mrForm function = "+mrForm.$invalid +" ::" + mrForm.$submitted);
		if(mrForm.$invalid){
			$scope.isFormSubmitted = true;
		} else {
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/mrSubmit',$scope.mr);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.duplicateFlag = false;
				    	$('div#duplicateDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.duplicateFlag = true;
						    }
							});
						$('div#duplicateDB').dialog('open');
				  }else{
					  $scope.saveMR();
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });	
		}
	}
	
	
	$scope.cancelMR = function(){
		console.log("enter into cancelMR function");
		$('div#duplicateDB').dialog('close');
		$('#saveId').removeAttr("disabled");
	}
	
	$scope.intiOnAccMrForm = function () {
		$scope.mr = {};
		$scope.mr.mrPayBy = 'C';
		$scope.customer = {};
		$scope.bank = {};
		$scope.csDt = $scope.cssDt;
		$scope.mr.mrDate = $scope.csDt;
		$("#bnkId").attr("disabled", "disabled");
		$("#chqId").attr("disabled", "disabled");
		$("#custBnkId").attr("disabled", "disabled");
		$("#refId").attr("disabled", "disabled");
		$scope.commonValidationFlag = false;
		$scope.chequeValidationFlag = false;
		$scope.rtgsValidationFlag = false;
		$scope.isFormSubmitted = false;
		$("#errorMsgDiv").empty();
	}
	
	
	$scope.saveMR = function(){
		console.log("enter into saveMR function");
		var response = $http.post($scope.projectName+'/mrFinalSubmitN',$scope.mr);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success") {
				  $scope.intiOnAccMrForm();
				  $scope.alertToast("Successfully generat MR NO. = "+data.mrNo);
				  if( $scope.duplicateFlag === false){
					  $scope.duplicateFlag = true;
					  $('div#duplicateDB').dialog('close');
				  }
				$('#saveId').removeAttr("disabled");
			  } else {
				  if (data.validationList != undefined) {
					  var errorMsg = "";
					  for (var i=0; i<data.validationList.length; i++) {
						  errorMsg = errorMsg +"* "+data.validationList[i] + "<br>";
					  }
					  $("#errorMsgDiv").empty().append(errorMsg);
				  } else {
					  $scope.errorToast(data.msg);
				  }
				  $('#saveId').removeAttr("disabled");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
				$('#saveId').removeAttr("disabled");
		   });
	}
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getMrDetail();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);
