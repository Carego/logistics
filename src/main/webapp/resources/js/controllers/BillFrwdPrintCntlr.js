'use strict';

var app = angular.module('application');

app.controller('BillFrwdPrintCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	$scope.bfNo = "";
	$scope.bfMap = {};
	$scope.loadingFlag = false;
	$scope.printVsFlag = true;
	
	
	$scope.getBFPrint = function(){
		console.log("enter into getBFPrint function");
		if(angular.isUndefined($scope.bfNo)){
			$scope.alerrToast("Please enter a valid BillForwarding No");
		}else{
			$scope.loadingFlag = true;
			var response = $http.post($scope.projectName+'/getBFPrint',$scope.bfNo);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.loadingFlag = false;
					  $scope.bfMap = data.bfMap;
					  console.log("$scope.bfMap = "+$scope.bfMap);
					  if($scope.bfDt==null || $scope.bfDt.isundefined || $scope.bfDt=="")
						  $scope.bfDt=$filter('date')($scope.bfMap.bf.creationTS, "yyyy-MM-dd");
					  console.log("bfDt="+$scope.bfDt);
					  $scope.printVsFlag = false;
				    	$('div#printVsDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.printVsFlag = true;
						    }
							});
						$('div#printVsDB').dialog('open');
						
				  }else{
					  $scope.loadingFlag = false;
					  $scope.alertToast("Server Error");
				  }
			   });
			   response.error(function(data, status, headers, config) {
				   $scope.loadingFlag = false;
					$scope.errorToast(data.result);
			   });
		}
	}
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
	}	

}]);