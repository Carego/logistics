package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.services.VoucherService;

@Repository
public interface ChqPayVoucherDAO {

	public Map<String,Object> saveChqPayVoucher(VoucherService voucherService , List<Map<String,Object>> faList);
}
