<div ng-show="operatorLogin || superAdminLogin">
<title>View Customer</title>

<div class="row">
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
	<div class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

		<div class="input-field col s6">
			<input type="text" id="arrRepCode" name="arCode" ng-model="arCode"
				required readonly ng-click="OpenARCodevDB()" /> <label>Enter
				Arrival Report Code</label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit"
				value="Submit" ng-click="ViewAr(arCode)">
		</div>



		<div id="showARDetails" ng-hide="showARDetailsFlag">
			<h4>
				Here's the detail of <span class="teal-text text-lighten-2">{{ar.arCode}}</span>:
			</h4>
			<form name="ARForm" ng-submit="updateAR(ARForm)">
				<table class="table-bordered table-hover table-condensed">
					<tr>
						<td>Branch Code:</td>
						<td><input type="text" id="branchCode" name="branchCode"
							ng-model="ar.branchCode" value={{ar.branchCode}}
							ng-click="OpenBranchCodeDB()" readonly></td>
					</tr>
					<tr>
						<td>Arrival Report Code:</td>
						<td><input type="text" id="arCode" name="arCode"
							ng-model="ar.arCode" value={{ar.arCode}} readonly></td>
					</tr>

					<tr>
						<td>Arrival Report Date:</td>
						<td><input type="text" id="arDt" name="arDt" ng-model="arDt"
							value={{ar.arDt}}></td>
					</tr>

					<tr>
						<td>Received Weight:</td>
						<td><input type="number" id="arRcvWt" name="arRcvWt"
							ng-model="ar.arRcvWt" value={{ar.arRcvWt}} ng-required="true"
							ng-minlength="1" ng-maxlength="9" step="0.01" min="0.00"></td>
					</tr>

					<tr>
						<td>Package:</td>
						<td><input type="number" id="arPkg" name="arPkg"
							ng-model="ar.arPkg" value={{ar.arPkg}} step="0.01" min="0.00"
							ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Unloading:</td>
						<td><input type="text" id="arUnloading" name="arUnloading"
							ng-model="ar.arUnloading" value={{ar.arUnloading}} step="0.01"
							min="0.00" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Claim:</td>
						<td><input type="text" id="arClaim" name="arClaim"
							ng-model="ar.arClaim" value={{ar.arClaim}} step="0.01" min="0.00"
							ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Detention:</td>
						<td><input type="text" id="arDetention" name="arDetention"
							ng-model="ar.arDetention" value={{ar.arDetention}} step="0.01"
							min="0.00" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Other:</td>
						<td><input type="text" id="arOther" name="arOther"
							ng-model="ar.arOther" value={{ar.arOther}} step="0.01" min="0.00"
							ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Late Delivery:</td>
						<td><input type="text" id="arLateDelivery"
							name="arLateDelivery" ng-model="ar.arLateDelivery"
							value={{ar.arLateDelivery}} step="0.01" min="0.00"
							ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Late Acknowledgement:</td>
						<td><input type="text" id="arLateAck" name="arLateAck"
							ng-model="ar.arLateAck" value={{ar.arLateAck}} step="0.01"
							min="0.00" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
					</tr>

					<tr>
						<td>Reporting Date :</td>
						<td><input type="text" id="arRepDt" name="arRepDt"
							ng-model="arRepDt" value={{ar.arRepDt}}></td>
					</tr>

					<tr>
						<td>Reporting Time:</td>
						<td><input type="text" id="arRepTime" name="arRepTime"
							ng-model="arRepTime" value={{ar.arRepTime}}></td>
					</tr>

					<tr>
						<td>Challan Code:</td>
						<td><input type="text" id="archlnCode" name="archlnCode"
							ng-model="ar.archlnCode" value={{ar.archlnCode}} readonly></td>
					</tr>
				</table>
				<div class="row">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn"
							file-model="arImage"> <label>Choose File</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadArImage()">Upload</a>
					</div>
				</div>
				<input type="submit" value="Submit!">
			</form>
		</div>
	</div>
	<div class="col s3">&nbsp;</div>
</div>

<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

	<input type="text" name="filterTextbox"
		ng-model="filterTextbox.branchCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
		</tr>
		<tr ng-repeat="branch in branchList | filter:filterTextbox"">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ branch.branchCode }}" ng-model="branchCode"
				ng-click="saveBranchCode(branch)"></td>
			<td>{{ branch.branchCode }}</td>
			<td>{{ branch.branchName }}</td>
			<td>{{ branch.branchPin }}</td>
		</tr>
	</table>

</div>

<div id="arCodeDB" ng-hide="ArCodeDBFlag">
	<input type="text" name="filterTextbox" ng-model="filterTextbox"
		placeholder="Search Arrival Report Code">
	<table>
		<tr ng-repeat="arCode in arList | filter:filterTextbox">
			<td><input type="radio" name="arCode" value="{{ arCode }}"
				ng-model="$parent.arCode" ng-click="saveArCode(arCode)"></td>
			<td>{{ arCode }}</td>
		</tr>
	</table>
</div>


</div>


