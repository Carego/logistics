'use strict';

var app = angular.module('application');

app.controller('EnquiryCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("EnquiryCntlr Started");
	
	$scope.mrList = [];	
	$scope.myChln = {};
	$scope.myCnmt = {};
	$scope.chlnNarr = "";
	
	$scope.branchList = [];
	$scope.branch = {};
	$scope.codeList = [];
	$scope.faMList = [];
	$scope.cancelArFlag=false;
	
	$scope.enquiryList = [];
	$scope.enquiryList1 = [];
	$scope.cashStmtList = [];
	$scope.lhpvAdvList = [];
	$scope.lhpvBalList = [];
	$scope.lhpvSupList = [];
	
	$scope.cnmtInvoiceNoList = [];
	$scope.billMrList = [];
	
	$scope.billDetByMr = [];
	
	$scope.lhpvAdvList = [];
	$scope.lhpvAdv = {};
	$scope.lhpvBalList = [];
	$scope.lhpvBal = {};
	$scope.lhpvSupList = [];
	$scope.lhpvSup = {};	
	
	$scope.ar = {};
	
	$scope.faCodeFrom = "";
	$scope.faCodeTo = "";
	$scope.faName = "";
	$scope.fromDate = "";
	$scope.toDate = "";
	
	
	$scope.detailCnmtFlag = false;
	$scope.detailChlnFlag = false;
	$scope.detailBillFlag = false;
	$scope.lhpvAdvTblFlag = false;
	$scope.lhpvBalTblFlag = false;
	$scope.lhpvSupTblFlag = false;
	$scope.lhpvAdvEnquiryTblFlag = false;
	$scope.lhpvBalEnquiryTblFlag = false;
	$scope.lhpvSupEnquiryTblFlag = false;
	$scope.mrEnquiryTblFlag = false;
	$scope.billEnquiryByMr = false;	
	
	$scope.cashStmtFound = false;
	$scope.lhpvAdvFound = false;
	$scope.lhpvBalFound = false;
	$scope.lhpvSupFound = false;
	
	$scope.amtFlag = false;
	$scope.faCodeFlag = false;
	
	$scope.cashStmtEnquiryTblFlag = false;
	$scope.otherEnquiryTblFlag = false;
	
	$scope.arTblFlag = false;	
	
	$scope.branchDBFlag = true;
	$scope.codeDBFlag = true;
	$scope.showInvoiceDBFlag = true;
	$scope.billMrDBFlag = true;	
	
	$scope.loadingFlag = false;
	
	$scope.chlnDetectionFlag = false;
	$scope.chlnToolTaxFlag = false;
	$scope.chlnHeightFlag = false;
	$scope.chlnUnionFlag = false;
	$scope.chlnTwoPointFlag = false;
	$scope.chlnWeightmentChgFlag = false;
	$scope.chlnCraneChgFlag = false;
	$scope.chlnOthersFlag = false;
	
	
	
	/*$scope.lhpvAdvListDBFlag = true;
	$scope.lhpvBalListDBFlag = true;
	$scope.lhpvSupListDBFlag = true;*/	
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$scope.type = "";
		$('div#branchDB').dialog('close');
		$scope.resetTbl();
	}
	
	$scope.getTypeCodeList = function() {
		
		console.log("getTypeCode");	
		$scope.cancelArFlag=false;
		$scope.code = "";		
		$scope.resetTbl();		
		var brNTypeMap = {				
				"type"		:	$scope.type
		}
		if($scope.type === "cnmt")
			$("#select").text("Select CNMT No.");
		else if($scope.type === "chln")
			$("#select").text("Select Challan No.");
		else if($scope.type === "sedr")
			$("#select").text("Select SEDR No.");
		else if($scope.type === "bill")
			$("#select").text("Select Bill No.");
		else if($scope.type === "mr")
			$("#select").text("Select MR No.");
		else if($scope.type === "loryNo")
			$("#select").text("Select Lorry No.");
		else if($scope.type === "chq")
			$("#select").text("Select Cheqne No.");
		else if($scope.type === "amt"){
			$("#select").text("Enter Amount ");
			$scope.faCodeFrom = "";
			$scope.faCodeTo = "";
		}		
		if($scope.type === 'amt'){			
			$scope.amtFlag = true;			
			return null;
		}else{			
			$scope.amtFlag = false;
		}
		
	}
	
	$scope.saveCode = function(code) {
		console.log("saveCode()");
		$scope.code = code;
		$('div#codeDB').dialog('close');
		$scope.resetTbl();
	}
	
	$scope.submitEnquiry = function(enquiryForm) {
		console.log("submitEnquiry()");
		if (enquiryForm.$invalid) {
			console.log("invalidate: "+enquiryForm.$invalid);
			if (enquiryForm.branchName.$invalid) {
				$scope.alertToast("please enter valid branchName");
			} else if(enquiryForm.typeName.$invalid){
				$scope.alertToast("please enter valid Type");
			} else if (enquiryForm.codeName.$invalid) {
				$scope.alertToast("please enter valid code");
			} 
		} else {
			console.log("invalidate: "+enquiryForm.$invalid);
			
			var brTypeNCodeMap = {					
					"type"			:	$scope.type,
					"code"			:	$scope.code,
					"fromDate"		:	$scope.fromDate,
					"toDate"		:	$scope.toDate,
					"faCodeFrom"	:	$scope.faCodeFrom,
					"faCodeTo"		:	$scope.faCodeTo
			}
			$scope.resetTbl();
			var res = $http.post($scope.projectName+'/getEnquiry', brTypeNCodeMap);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					//console.log("getEnquiry result: "+data.result);
					//console.log("enquiryList: "+JSON.stringify(data.enquiryList));
					console.log("Data Length = "+data.enquiryList.length);
					 if(data.enquiryList.length > 0){						 
						 for(var i=0; i<data.enquiryList.length; i++){
							 var cnmt = data.enquiryList[i];							 
							 var chlnList = cnmt.chlnList;
							 if(cnmt != undefined && chlnList != undefined)
							 for(var j=0; j<chlnList.length; j++){							 
								 if(chlnList[j].chlnDetection > 0){
									 $scope.chlnDetectionFlag = true;
									 console.log("Detection = "+$scope.chlnDetectionFlag);
								 }
								 if(chlnList[j].chlnToolTax > 0)
									 $scope.chlnToolTaxFlag = true;								 
								 if(chlnList[j].chlnHeight > 0)
									 $scope.chlnHeightFlag = true;
								 if(chlnList[j].chlnUnion > 0)
									 $scope.chlnUnionFlag = true;
								 if(chlnList[j].chlnTwoPoint > 0)
									 $scope.chlnTwoPointFlag = true;
								 if(chlnList[j].chlnWeightmentChg > 0)
									 $scope.chlnWeightmentChgFlag = true;
								 if(chlnList[j].chlnCraneChg > 0)
									 $scope.chlnCraneChgFlag = true;
								 if(chlnList[j].chlnOthers > 0)
									 $scope.chlnOthersFlag = true;
							 }
						 }
					 }
					
					if($scope.type=='sedr'&& data.enquiryList[0].isCancel){
						//$scope.ar=data.enquiryList;
						console.log("SEDR"+ data.enquiryList[0].isCancel );
						$scope.ar=data.enquiryList[0];
						console.log($scope.ar.branchCode)
						$scope.cancelArFlag=true;
						$scope.allFlag=false;
						$scope.otherEnquiryTblFlag=false;
					}else{
						$scope.cancelArFlag=false;
						$scope.allFlag=true;
					}
					if($scope.type === "chq"){				
						for(var i=0; i<data.enquiryList.length; i++){
							if(data.enquiryList[i].cashStmtList != null){
								$scope.cashStmtList = data.enquiryList[i].cashStmtList;								
								$scope.cashStmtEnquiryTblFlag = true;
								$scope.lhpvBalEnquiryTblFlag = false;
								$scope.lhpvAdvEnquiryTblFlag = false;
								$scope.lhpvSupEnquiryTblFlag = false;
								$scope.otherEnquiryTblFlag = false;
								$scope.mrEnquiryTblFlag = false;
								$scope.billEnquiryByMr = false;
								$scope.cancelArFlag=false;
							}else if(data.enquiryList[i].lhpvAdvList != null){
								$scope.lhpvAdvList = data.enquiryList[i].lhpvAdvList;
								$scope.lhpvAdvEnquiryTblFlag = true;
								$scope.lhpvBalEnquiryTblFlag = false;
								$scope.lhpvSupEnquiryTblFlag = false;
								$scope.cashStmtEnquiryTblFlag = false;			
								$scope.otherEnquiryTblFlag = false;
								$scope.mrEnquiryTblFlag = false;
								$scope.billEnquiryByMr = false;
								$scope.cancelArFlag=false;
							}else if(data.enquiryList[i].lhpvBalList != null){
								$scope.lhpvBalList = data.enquiryList[i].lhpvBalList;
								$scope.lhpvBalEnquiryTblFlag = true;								
								$scope.lhpvAdvEnquiryTblFlag = false;
								$scope.lhpvSupEnquiryTblFlag = false;
								$scope.cashStmtEnquiryTblFlag = false;			
								$scope.otherEnquiryTblFlag = false;
								$scope.mrEnquiryTblFlag = false;
								$scope.billEnquiryByMr = false;
								$scope.cancelArFlag=false;
							}else if(data.enquiryList[i].lhpvSupList != null){
								$scope.lhpvSupList = data.enquiryList[i].lhpvSupList;
								$scope.lhpvSupEnquiryTblFlag = true;
								$scope.lhpvBalEnquiryTblFlag = false;
								$scope.lhpvAdvEnquiryTblFlag = false;
								$scope.cashStmtEnquiryTblFlag = false;			
								$scope.otherEnquiryTblFlag = false;
								$scope.mrEnquiryTblFlag = false;
								$scope.billEnquiryByMr = false;
								$scope.cancelArFlag=false;
							}
						}						
					}else if($scope.type === "mr"){
						$scope.mrList = data.enquiryList;
						if($scope.mrList[0].billDt == null)
							$scope.alertToast("No Bill Detail Found !");
						else{
							$scope.billEnquiryByMr = true;
							$scope.billDetByMr = $scope.mrList[0].billDt;
						}						
						$scope.mrEnquiryTblFlag = true;					
					}else if($scope.type === "amt"){
						$scope.otherEnquiryTblFlag = false;							
						for(var i=0; i<data.enquiryList.length; i++){
							if(data.enquiryList[i].cashStmtList != null){
								$scope.cashStmtList = data.enquiryList[i].cashStmtList;
								$scope.cashStmtEnquiryTblFlag = true;
							}else if(data.enquiryList[i].lhpvAdvList != null){
								$scope.lhpvAdvList = data.enquiryList[i].lhpvAdvList;								
							}else if(data.enquiryList[i].lhpvBalList != null){
								$scope.lhpvBalList = data.enquiryList[i].lhpvBalList;
								$scope.lhpvBalEnquiryTblFlag = true;
							}else if(data.enquiryList[i].lhpvSupList != null){
								$scope.lhpvSupList = data.enquiryList[i].lhpvSupList;
								$scope.lhpvSupEnquiryTblFlag = true;
							}
						}
						if($scope.cashStmtList.length > 0)
							$scope.cashStmtEnquiryTblFlag = true;
						else
							$scope.cashStmtEnquiryTblFlag = false;
						if($scope.lhpvAdvList.length > 0)
							$scope.lhpvAdvEnquiryTblFlag = true;
						else
							$scope.lhpvAdvEnquiryTblFlag = false;
						if($scope.lhpvBalList.length > 0)
							$scope.lhpvBalEnquiryTblFlag = true;
						else
							$scope.lhpvBalEnquiryTblFlag = false;
						if($scope.lhpvSupList.length > 0)
							$scope.lhpvSupEnquiryTblFlag = true;
						else
							$scope.lhpvSupEnquiryTblFlag = false;						
						$scope.otherEnquiryTblFlag = false;
						$scope.mrEnquiryTblFlag = false;	
						$scope.billEnquiryByMr = false;						
					}else{
						$scope.leftMenuFlag = false;												
						$scope.enquiryList = data.enquiryList;			
						$scope.otherEnquiryTblFlag = true;
						$scope.cashStmtEnquiryTblFlag = false;
						$scope.lhpvAdvEnquiryTblFlag = false;
						$scope.lhpvBalEnquiryTblFlag = false;
						$scope.lhpvSupEnquiryTblFlag = false;
						$scope.mrEnquiryTblFlag = false;	
						$scope.billEnquiryByMr = false;	
					}
					
				}else if(data.result === "notFound"){
					$scope.alertToast("No Such Record Found !");
					$scope.cashStmtList = [];
					$scope.lhpvAdvList = [];
					$scope.lhpvBalList = [];
					$scope.lhpvSupList = [];
					$scope.mrList = [];
					$scope.billDetByMr = [];
					$scope.enquiryList = [];					
					
					$scope.leftMenuFlag = false;
					$scope.otherEnquiryTblFlag = false;
					$scope.cashStmtEnquiryTblFlag = false;
					$scope.lhpvAdvEnquiryTblFlag = false;
					$scope.lhpvBalEnquiryTblFlag = false;
					$scope.lhpvSupEnquiryTblFlag = false;
					$scope.mrEnquiryTblFlag = false;	
					$scope.billEnquiryByMr = false;	
					$scope.cancelArFlag=false;
				}else{
					console.log("getEnquiry result: "+data.result);
					$scope.enquiryList = [];
				}
			});
			
			res.error(function(data, status, headers, config) {
				console.log("error in response getEnquiry: "+data);
			});			
		}
	}
	
	$scope.getDetailCnmt = function() {
		console.log("getDetailCnmt()");
		$scope.detailCnmtFlag = $scope.detailCnmtFlag ? false : true; 
		$scope.cancelArFlag=false;
	}
	
	$scope.getDetailChln = function() {
		console.log("getDetailChln()");
		$scope.detailChlnFlag = $scope.detailChlnFlag ? false : true;
		$scope.cancelArFlag=false;
	}
	
	$scope.getDetailBill = function() {
		console.log("getDetailBill()");
		$scope.detailBillFlag = $scope.detailBillFlag ? false : true;
		$scope.cancelArFlag=false;
	}
	
	$scope.showInvoiceList = function(cnmtInvoiceNoList, cnmtCode) {
		console.log("showInvoiceList()");
		$scope.cnmtInvoiceNoList = cnmtInvoiceNoList;
		$scope.cancelArFlag=false;
		
		$scope.showInvoiceDBFlag = false;
		$('div#showInvoiceDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Invoice No of "+cnmtCode,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.showInvoiceDBFlag = true;
			}
		});
		$('div#showInvoiceDB').dialog('open');
	}
	
	$scope.showMrList = function(mrList, billNo) {
		console.log("showMrList()");
		$scope.billMrList = mrList;
		
		$scope.billMrDBFlag = false;
		$('div#billMrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bill No "+billNo,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.billMrDBFlag = true;
			}
		});
		$('div#billMrDB').dialog('open');
	}
	
	$scope.getMrDetail = function(mr) {
		$('div#billMrDB').dialog('close');
		console.log("getMrDetail()");
		console.log("mr no: "+mr.mrNo);
		console.log("mr Id: "+mr.mrId);
	}
	
	$scope.showLhpvAdvTbl = function(lhpvAdvList, chlnNoAdv) {
		console.log("showLhpvAdvTbl()");
		$scope.lhpvAdvList = lhpvAdvList;
		$scope.lhpvAdvTblFlag = $scope.lhpvAdvTblFlag ? false : true;
		$scope.chlnNoAdv = chlnNoAdv;
	}
	
	$scope.showLhpvBalTbl = function(lhpvBalList, chlnNoBal) {
		console.log("showLhpvBalList()");
		$scope.lhpvBalList = lhpvBalList;
		$scope.lhpvBalTblFlag = $scope.lhpvBalTblFlag ? false : true;
		$scope.chlnNoBal = chlnNoBal;
	}
	
	$scope.showLhpvSupList = function(lhpvSupList, chlnNoSup) {
		console.log("showLhpvSupList()");
		$scope.lhpvSupList = lhpvSupList;
		$scope.lhpvSupTblFlag = $scope.lhpvSupTblFlag ? false : true;
		$scope.chlnNoSup = chlnNoSup;
	}
	
	$scope.showArTbl = function(ar, chlnNoAr) {
		console.log("showArTbl()");
		$scope.ar = ar;
		$scope.arTblFlag = $scope.arTblFlag ? false : true;
		$scope.chlnNoAr = chlnNoAr;
	}
	
	$scope.resetTbl = function(){
		$scope.detailCnmtFlag = false;
		$scope.detailChlnFlag = false;
		$scope.detailBillFlag = false;
		$scope.lhpvAdvTblFlag = false;
		$scope.lhpvBalTblFlag = false;
		$scope.lhpvSupTblFlag = false;
		$scope.arTblFlag = false;
		
		$scope.cashStmtEnquiryTblFlag = false;
		$scope.lhpvAdvEnquiryTblFlag = false;
		$scope.lhpvBalEnquiryTblFlag = false;
		$scope.lhpvSupEnquiryTblFlag = false;
		$scope.otherEnquiryTblFlag = false;
		$scope.mrEnquiryTblFlag = false;
		$scope.billEnquiryByMr = false;	
		
		$scope.enquiryList = [];
		$scope.cashStmtList = [];
		$scope.lhpvAdvList = [];
		$scope.lhpvBalList = [];
		$scope.lhpvSupList = [];
		$scope.billDetByMr = [];
		$scope.mrList = [];		
	}
	
	$scope.getBillDetByMr = function(mr){
		var brTypeNCodeMap = {
				"branchId"	:	$scope.branch.branchId,
				"type"		:	"mr",
				"code"		:	mr
		}
		var res = $http.post($scope.projectName+'/getBillDetByMr',brTypeNCodeMap);
		res.success(function(data, status, headers, config){			
			if(data.result === "success"){
				$scope.cashStmtEnquiryTblFlag = true;
				$scope.lhpvAdvEnquiryTblFlag = false;
				$scope.lhpvBalEnquiryTblFlag = false;
				$scope.lhpvSupEnquiryTblFlag = false;
				$scope.otherEnquiryTblFlag = false;
				$scope.mrEnquiryTblFlag = false;
				$scope.billEnquiryByMr = true;
				$scope.billDetByMr = data.billDetList;		
				$scope.alertToast("Success !");
			}else if(data.result === "notFound"){
				$scope.billEnquiryByMr = false;				
				$scope.alertToast("Bill Detail Not Found !");	
			}else{
				console.log("getEnquiry result: "+data.result);
				$scope.billDetByMr = [];				
			}
		});
		res.error(function(data, status, headers, config){
			console.log("error in response getEnquiry: "+data);
		});		
	}
	
	$scope.getFaCode = function(){		
		// Find length of faCode
		if($scope.faCode.length != 2)
			return null;
		
		$scope.loadingFlag = true;
		
		console.log("enter into getFaCode function");		
		var faObj = {
				"faCode"	:	$scope.faCode,
				"bCode"		:	$scope.branch.branchId
		}
		var response = $http.post($scope.projectName+'/getFaCodeByCode',faObj);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.loadingFlag = false;				
				$scope.faMList = data.faMasterList;
				$scope.openFaCodeDB();
				console.log("size of $scope.faMList =" +$scope.faMList.length);
			}else if(data.result === "notFound"){
				$scope.alertToast("FaCode Not Found !");
				$scope.loadingFlag = false;
			}else{
				$scope.loadingFlag = false;
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.loadingFlag = false;
			console.log(data);
		});
	}
	
	$scope.openFaCodeDB = function(){
		console.log("enter into openFaCodeDB function");
		$scope.faCodeFlag = true;
		$('div#faCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#faCodeDB').dialog('open');
	}
	
	$scope.saveFaCode = function(faM){		
		$('div#faCodeDB').dialog('close');
		$scope.faCodeFlag = false;
		console.log("enter into saveFaCode function");		
		$scope.faCode = faM.faMfaCode;		
		$scope.faName = faM.faMfaName;
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("EnquiryCntlr Ended");
}]);