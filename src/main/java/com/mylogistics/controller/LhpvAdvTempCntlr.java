package com.mylogistics.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrhWiseMunsDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.DAO.MunsianaDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Owner;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.BrhWiseMuns;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class LhpvAdvTempCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private MunsianaDAO munsianaDAO;
	
	@Autowired
	private BrhWiseMunsDAO brhWiseMunsDAO;
	
	
	@RequestMapping(value = "/getLhpvDetTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvDet() {  
		System.out.println("Enter into getLhpvDet---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<Map<String,Object>> bnkList = new ArrayList<>();
				bnkList = bankMstrDAO.getBnkNameAndCode(branchCode);
				/*LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatus(branchCode);
					if(lhpvStatus == null){*/
						map.put(ConstantsValues.BRANCH, branchList.get(0));
						//map.put("lhpvStatus",lhpvStatus);
						map.put("bnkList",bnkList);
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					/*}else{
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					}*/
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/getChequeNoFrLHPVTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrLHPV(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNoFrLHPV---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		ChequeLeaves chequeLeaves= bankMstrDAO.getChqListByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getChqLByBnk(bankCode,CType);
		if(!chqList.isEmpty()){
			System.out.println("***************** 1");
			map.put("list",chqList);
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getChlnAndBrOwnTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnAndBrOwn() {  
		System.out.println("Enter into getChlnAndBrOwn---->");
		//User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<>();	
		
		/*List<String> chlnList = challanDAO.getChCodeByBrh(currentUser.getUserBranchCode());*/
		List<String> chlnList = challanDAO.getChlnCodeFrLHA();
		
		if(!chlnList.isEmpty()){
			for(int i=0;i<chlnList.size();i++){
				for(int j=i+1;j<chlnList.size();j++){
					if(chlnList.get(i).equalsIgnoreCase(chlnList.get(j))){
						chlnList.remove(j);
						j--;
					}
				}
			}
		}
		
		List<Map<String,String>> brkList = brokerDAO.getBrkNCF();
		List<Map<String,String>> ownList = ownerDAO.getOwnNCF();
		
		List<Map<String,String>> actBOList = new ArrayList<>();
		
		if(!brkList.isEmpty()){
			/*for(int i=0;i<brkList.size();i++){
				if(Integer.parseInt(brkList.get(i).get("panImgId")) <= 0){
					brkList.remove(i);
					i = i-1;
				}
			}*/
			actBOList = brkList;
		}
		
		if(!ownList.isEmpty()){
			for(int i=0;i<ownList.size();i++){
				//if(Integer.parseInt(ownList.get(i).get("panImgId")) > 0){
					actBOList.add(ownList.get(i));
				//}	
			}
		}
		
		if(!chlnList.isEmpty() && !actBOList.isEmpty()){
			map.put("chlnList", chlnList);
			map.put("actBOList",actBOList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	
	@RequestMapping(value = "/getChlnFrLATemp" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnFrLA(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChlnFrLA---->");	
		Map<String, Object> map = new HashMap<>();	
		String chlnCode = clientMap.get("chlnCode");
		String brkOwnFaCode = clientMap.get("brkOwnFaCode");
		float tdsPer = (float) 0.0;
		
		if(brkOwnFaCode != null){
			if(brkOwnFaCode.substring(0,2).equals("08")){
				Owner own = ownerDAO.getOwnByFaCode(brkOwnFaCode);
				if(own != null){
					tdsPer = own.getOwnPanIntRt();
				}
			}else if(brkOwnFaCode.substring(0,2).equals("09")){
				Broker brk = brokerDAO.getBrkByFaCode(brkOwnFaCode);
				if(brk != null){
					tdsPer = brk.getBrkPanIntRt();
				}
			}else{
				System.out.println("invalid owner/broker code");
			}
		}
		
		System.out.println("value of tdsPer = "+tdsPer);
		
		if(chlnCode != null){
			List<Challan> chlnList = challanDAO.getChallanList(chlnCode);
			if(!chlnList.isEmpty()){
				//double chlnAdv = Double.parseDouble(chlnList.get(0).getChlnAdvance());
				int res = challanDAO.checkLA(chlnList.get(0).getChlnId());
				double chlnAdv = chlnList.get(0).getChlnRemAdv();
				System.out.println("chlnAdv = "+chlnAdv);
				String brhCode = chlnList.get(0).getBranchCode();
				BrhWiseMuns brhWiseMuns = brhWiseMunsDAO.getBrhMuns(Integer.parseInt(brhCode));
				double munsAmt = 0.0;
				double tdsAmt = 0.0;
				double csDis = 0.0;
				if(chlnAdv > 0){
					if(brhWiseMuns != null){
						munsAmt = brhWiseMuns.getBwmMuns();
						float csDisPer   = brhWiseMuns.getBwmCsDis();
						/*float brhTds = brhWiseMuns.getBwmTds();
						System.out.println("brhTds = "+brhTds);
						tdsAmt = (chlnAdv * brhTds) / 100;
						DecimalFormat df = new DecimalFormat("#.##");
						String angleFormated = df.format(tdsAmt);
						tdsAmt = Double.parseDouble(angleFormated);*/
						
						System.out.println("csDisPer = "+csDisPer);
						csDis  = (chlnAdv * csDisPer) / 100;
						DecimalFormat df = new DecimalFormat("#.##");
						String angleFormated = df.format(csDis);
						csDis = Double.parseDouble(angleFormated);
								
						tdsAmt = (chlnAdv * tdsPer) / 100;
					}else{
						munsAmt = munsianaDAO.getMunsAmt(chlnAdv);
						csDis  = (chlnAdv * 0.01);
						tdsAmt = (chlnAdv * tdsPer) / 100;
					}
				}
				
				System.out.println("value of tdsAmt = "+tdsAmt);
				
				
				if(res > 0){
					map.put("isCal",false);
				}else{
					map.put("isCal",true);
				}
				map.put("chln",chlnList.get(0));
				map.put("munsAmt",munsAmt);
				map.put("tdsAmt",tdsAmt);
				map.put("chlnAdv",chlnAdv);
				map.put("csDis",csDis);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/submitLhpvAdvTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object submitLhpvAdv(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitLhpvAdv---->");	
		Map<String, Object> map = new HashMap<>();
		if(!voucherService.getLhpvAdvList().isEmpty()){
			Map<String, Object> resMap = new HashMap<>();
			resMap = lhpvStatusDAO.checkBackDtLhpv(voucherService.getDateTemp());
			if(String.valueOf(resMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){
				LhpvStatus lhpvStatus = (LhpvStatus) resMap.get("lhpv");
				voucherService.setLhpvStatus(lhpvStatus);
				int laNo = lhpvStatusDAO.saveLhpvAdvTemp(voucherService);
				if(laNo > 0){
					map.put("laNo",laNo);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("msg","SERVER ERROR");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg",String.valueOf(resMap.get("msg")));
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
}
