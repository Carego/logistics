package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Customer;
import com.mylogistics.model.User;

@Repository
public interface CustomerDAO {
	
	//Shikha: retrieves single row from database
	public List<Customer> getCustomer(String custCode);
	
	//Shikha: calculates count
	public long totalCount();
	
	//Shikha: retrieves last customer from database
	public int getLastCustomer();
	
	//Shikha: retrieves multiple row from from database
	public List<String> getCustomerCode();
	
	//Shikha: saves customer to database
	public int saveCustomerToDB(Customer cust , Branch branch);
	
	//Shikha: retrieves customer and customer rep information
	public Map<String,Object> getCustomerNCustomerRep(String crCode);
	
	//Shikha: updates customer
	public int updateCustomerByCustCode(Customer customer);

	//Shikha: retrieves customer isverify field
	public List<Customer> getCustomerisVerifyNo();
	
	//Shikha: updates customer isverify field
	public int updateCustomerisVerify(String code[]);
	
	//Shikha: retrieves customer code,name and contract
	public List<Customer> getCustomerCodeNameContract();
	
	//Shikha: retrieves the list of customer whose isView is false
	public List<Customer> getCustmerisViewFalse(String branchCode);
	
	//Sanjana: get customerList for dailyContract
	public List<Customer> getCustomerListForDailyContract();
	
	public List<String> getAllCustCode(String branchCode);
	
	//Satarupa: get all customers by branch code
	public List<Customer> getCustomerByBranchCode(String branchCode);
	
	//customer list for view
	public List<Customer> getCustomerFV();
	
	public int updateCustomer(Customer customer);
	
	public List<Customer> getCustomerForCnmt(String branchCode);
	
	public int updateCustomerById(Customer customer);
	
	public List<Customer> getAllCustomer();
	
	public List<Branch> getAllCustBranch(int custId);
	
	public List<String> getAllCustBrCode(String custFaCode);
	
	//public String getCustName(int custId);
	
	public List<Map<String,Object>> getCustNCI();
	
	public List<Map<String,Object>> getGstCust();
	
	public List<Map<String,Object>> getRelGstCust();
	
	public Customer getCustById(int id);
	
	public int updateBillPer(Customer customer);
	
	public int checkBillPer(int custId , Date billDt);
	
	public List<Customer> getCustFrBillSbmsn();
	
	public List<Map<String, Object>> getCustListByBranch(int branchId);
	
	// With Session Management 
	
	public List<Customer> getCustomerByName_FaCode(Session session, User user, String nameFaCode, List<String> proList);
	
	public List<Map<String, Object>> getCustomerByGroup(Session session, User user, String groupId);
	
	public int saveUpdateCustGstNo(Session session, User user, Map<String,String> custData);
	
	public Customer getCustomer(int custId);

	public List<Customer> getRelConsignee(Session session,User user,List<String> proList);

	public int getCustCode(String custName, String bcode);
	 

}