package com.mylogistics.model.dataintegration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "miscfademo")
public class MiscFaDemo {

	@Id
	@Column(name="miscFaId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int miscFaId;
	
	@Column(name="miscFaName")
	private String miscFaName;
	
	@Column(name="miscFaCode")
	private String miscFaCode;

	public int getMiscFaId() {
		return miscFaId;
	}

	public void setMiscFaId(int miscFaId) {
		this.miscFaId = miscFaId;
	}

	public String getMiscFaName() {
		return miscFaName;
	}

	public void setMiscFaName(String miscFaName) {
		this.miscFaName = miscFaName;
	}

	public String getMiscFaCode() {
		return miscFaCode;
	}

	public void setMiscFaCode(String miscFaCode) {
		this.miscFaCode = miscFaCode;
	}

	
	
}
