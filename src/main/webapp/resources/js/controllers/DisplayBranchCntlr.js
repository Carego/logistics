'use strict';

var app = angular.module('application');

app.controller('DisplayBranchCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.selection=[];
	$scope.branch={};
	$scope.branchCodeDBFlag = true;
	$scope.editBranchDetailsDBFlag = true;
	$scope.branchModalRadio = false ;
	$scope.branchAutoRadio = false;
	$scope.branchTableDBFlag = true;
	$scope.addBranchStockFlag = true;
	$scope.operatorCodeFlag = true;
	$scope.availableTags = [];
	$scope.hbon = {};
	$scope.showCloseDateFlag = true;
	$scope.closeDateFlag = true;
	$scope.hOSLast = {};
	$scope.branchList = [];
	
	$scope.branchDirectorDBFlag = true;
	$scope.branchMarketingHDDBFlag = true;
	$scope.branchOutStandingHDDBFlag = true;
	$scope.branchMarketingDBFlag = true;
	$scope.branchMngrDBFlag = true;
	$scope.branchCashierDBFlag = true;
	$scope.branchTrafficDBFlag = true;
	$scope.branchAreaMngrDBFlag = true;
	$scope.branchRegionalMngrDBFlag = true;
	$scope.branchStationCodeDBFlag = true;
	
	$scope.viewBranchDetailsFlag = true;
	$scope.branchOldList = [];
	$scope.creationTSDBFlag = true;
	$scope.viewBranchOldDetailsFlag = true;
	$scope.branchOld ={};
	$scope.station = {};
	
	$scope.displayBranchForSuperAdmin = function(){
		console.log("Entered into displayBranchForSuperAdmin function------>");
		var response = $http.post($scope.projectName+'/displayBranchForSuperAdmin');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.branchList = data.list;
				$scope.branchTableDBFlag = false;
				for(var i=0;i<$scope.branchList.length;i++){
					$scope.branchList[i].creationTS =  $filter('date')($scope.branchList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);				
			}else{
				$scope.branchTableDBFlag = true;
				$scope.alertToast(data.result);
			}	
			$scope.getAllBranchesList();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.toggleSelection = function toggleSelection(branchId) {
		   var idx = $scope.selection.indexOf(branchId);
		   
		   // is currently selected
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
			   }
		   // is newly selected
		   else {
			   $scope.selection.push(branchId);
		   }
	   }	  

	$scope.verifyBranch = function(){   
		console.log("Entered into verifyBranch function---");
		if(!$scope.selection.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
			 var response = $http.post($scope.projectName+'/updateIsViewBranch',$scope.selection.toString());
				response.success(function(data, status, headers, config){
					$scope.successToast(data.result);
					$scope.displayBranchForSuperAdmin();
				});     
				response.error(function(data, status, headers, config) {
		           $scope.errorToast(data.result);
				});
		 }
		
	}
	
	$scope.enableBranchCodeTextbox = function(){
		$('#branchTextList').removeAttr("disabled");
		$('#branchAutoTextList').attr("disabled","disabled");
		$scope.branchCodeAutoList="";
	}
	
	$scope.enableAutoBranchCodeTextBox = function(){
		$('#branchAutoTextList').removeAttr("disabled");
		$('#branchTextList').attr("disabled","disabled");
		$scope.branchCodeModalList="";
		
		$("#creationTSId").attr('disabled','disabled');
		$scope.branchOldList ="";
		$scope.branchOld ="";
		$scope.creationTSModel ="";
		$scope.viewBranchOldDetailsFlag = true;
		$scope.branchTableDBFlag = false;
	}
	
	$scope.OpenBranchCodeDB = function(){
		$scope.branchCodeDBFlag = false;
		$scope.branchOld = "";
		$scope.creationTSModel ="";
		$scope.branchOldList ="";
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch Code",
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
	});
    	$('div#branchCodeDB').dialog('open');
	}
	
	$scope.saveBranchCode =  function(branch){
		console.log("enter into saveBranchCode----->"+branch.branchCode);
		$scope.branchCodeModalList = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.branchCodeDBFlag = true;
		$scope.filterTextbox = "";
		$("#creationTSId").removeAttr('disabled');
		$scope.showBranchDetails(branch.branchCode);
	}
	
	$scope.showBranchDetails = function(branchCode){
		$scope.branchTableDBFlag = true;
		$scope.editBranchDetailsDBFlag = false;
		var response = $http.post($scope.projectName+'/branchDetails', branchCode);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.branch = data.branch;
				$scope.branchStationName = $scope.branch.branchStationCode;
				 $scope.viewBranchOldDetailsFlag = true;
				if($scope.branch.isOpen === "yes"){
					$scope.showCloseDateFlag = true;
				}else if($scope.branch.isOpen === "no"){
					$scope.showCloseDateFlag = false;
					$scope.closeDateFlag = false;
				}
				$scope.getModifiedBranchDetail(branchCode);
			}else{
				console.log(data);
			}				
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getbranchCodeList = function(){
		console.log("Enter into getbranchCodeList function");
	      $( "#branchAutoTextList" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	}
	
	$scope.getBranchByBrCodeTemp = function(branchCodeAutoList){
		console.log("Entered into getBranchByBrCodeTemp function");
		if(angular.isUndefined(branchCodeAutoList) || branchCodeAutoList === null || branchCodeAutoList === ""){
			$scope.alertToast("Please enter branch code...")
		}else{
			$scope.branchTableDBFlag=true;
				
			$scope.code = $( "#branchAutoTextList" ).val();
			console.log("$scope.code---------"+$scope.code);
				
			var response = $http.post($scope.projectName+'/getBranchByBrCodeTemp',$scope.code);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.branch = data.branch;
					$scope.branchStationName = $scope.branch.branchStationCode;
					$scope.editBranchDetailsDBFlag = false;
					 $scope.viewBranchOldDetailsFlag = true;
					if($scope.branch.isOpen === "yes"){
						$scope.showCloseDateFlag = true;
					}else if($scope.branch.isOpen === "no"){
						$scope.showCloseDateFlag = false;
						$scope.closeDateFlag = false;
					}
				}else if(data.result==="error"){
					$scope.editBranchDetailsDBFlag = true;
					$scope.alertToast("There is no such branch...")
				}	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}
	
	$scope.backToList = function(){
		if(!$scope.branchList.length){
			$scope.branchTableDBFlag = true;
		}else{
			$scope.branchTableDBFlag = false;
		}
		$scope.editBranchDetailsDBFlag = true;
		$scope.branchCodeModalList="";
		$scope.branchCodeAutoList="";
		$scope.branchModalRadio = false ;
		$scope.branchAutoRadio = false;
		$('#branchTextList').attr("disabled","disabled");
		$('#branchAutoTextList').attr("disabled","disabled");
		
		$("#creationTSId").attr('disabled','disabled');
		$scope.branchOldList ="";
		$scope.branchOld ="";
		$scope.creationTSModel ="";
		$scope.viewBranchOldDetailsFlag = true;
	}
	

	$scope.editBranchSubmit = function(BranchForm,branch){
		console.log("enter into editBranchSubmit function--->");
		
		if(BranchForm.$invalid){
			if(BranchForm.branchName.$invalid){
				$scope.alertToast("Please enter branch name between 3-40 characters...");
			}else if(BranchForm.branchAdd.$invalid){
				$scope.alertToast("Please enter branch address between 3-255 characters...");
			}else if(BranchForm.branchCity.$invalid){
				$scope.alertToast("Please enter branch city between 3-40 characters...");
			}else if(BranchForm.branchState.$invalid){
				$scope.alertToast("Please enter branch state between 3-40 characters...");
			}else if(BranchForm.branchPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits...");
			}else if(BranchForm.branchPhone.$invalid){
				$scope.alertToast("Please enter correct phone number between 4-15 digits....");
			}else if(BranchForm.branchFax.$invalid){
				$scope.alertToast("Please enter correct branch fax number between 8-20 digits....");
			}else if(BranchForm.branchEmailId.$invalid){
				$scope.alertToast("Please enter correct Email id.....");
			}else if(BranchForm.branchWebsite.$invalid){
				$scope.alertToast("Please enter website between 3-255 characters.....");
			}
		}else{
			 $scope.viewBranchDetailsFlag = false;
				$('div#viewBranchDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Branch Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewBranchDetailsDB').dialog('open');
		}
	}
	
	$scope.saveEdittedBranch = function(branch){
		console.log("Entered into saveEdittedBranch function");
		var response = $http.post($scope.projectName+'/saveEdittedBranch', branch);
		response.success(function(data, status, headers, config) {
	
			if(data.result==="success"){
				$('div#viewBranchDetailsDB').dialog('close');
				$scope.viewBranchDetailsFlag = true;
				$scope.editBranchDetailsDBFlag = true;
				$scope.successToast(data.result);
				$scope.getModifiedBranchDetail($scope.branch.branchCode);
			}else{
				console.log(data);
			}		
		});
		response.error(function(data, status, headers, config) {
		$scope.errorToast(data);
	    });
	}
	
	$scope.closeViewBranchDetailsDB = function(){
		$('div#viewBranchDetailsDB').dialog('close');
	    $scope.viewBranchDetailsFlag = true;
	}
	
	$scope.getOperatorCode = function(){
		console.log("enter into getOperatorCode function");
		var response = $http.post($scope.projectName+'/getOperatorCode');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.operatorList = data.empList;
				console.log("employee list from server === > "+data.empList);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
		$scope.errorToast(data);
	    });
	}
		
	$scope.addBranchStock = function(branch){
		console.log("enter into addBranchStock function");
		$scope.addBranchStockFlag = false;
		console.log("----"+branch.branchCode);
		$scope.hbon.disBranchCode=branch.branchCode;
		
		$('div#addBranchStockModel').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
		});
		$('div#addBranchStockModel').dialog('open');
	}
	
	$scope.selectOperatorCode = function(){
		console.log("enter into selectOperatorCode function");	
		$scope.operatorCodeFlag = false;
		$('div#selectOperatorCodeModel').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
		});
		$('div#selectOperatorCodeModel').dialog('open');
	}
		
	$scope.selectOperator = function(empCode){
		console.log("enter into selectOperator function");	
		$scope.hbon.operatorCode = empCode;
		$scope.operatorCodeFlag = true;
		$('div#selectOperatorCodeModel').dialog('close');
	}
	
	$scope.saveBranchStock = function(hbon){
		console.log("enter into saveBranchStock function");	
		if($scope.hbon.noCnmt>$scope.hOSLast.hOSCnmt){
			$scope.alertToast("enter less cnmt");
		}else if($scope.hbon.noChln>$scope.hOSLast.hOSChln){
			$scope.alertToast("enter less chln");
		}else if($scope.hbon.noSedr>$scope.hOSLast.hOSSedr){
			$scope.alertToast("enter less sedr");
		}else{
			var response = $http.post($scope.projectName+'/saveBranchStock',hbon);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.EmployeeList = data.empList;
					$scope.submitHOSLast();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		    });
		}
		$('div#addBranchStockModel').dialog('close');
		$scope.addBranchStockFlag = true;
	}
	
	$scope.getAllBranchesList = function(){
		console.log("entered into getAllBranchesList function------>");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchCodeList = data.branchCodeList;
				for(var i=0;i<$scope.branchCodeList.length;i++){
					$scope.availableTags[i] = $scope.branchCodeList[i].branchCodeTemp;;
				}
				$scope.getOperatorCode();
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.getLastHOSLast = function(){
		console.log("entered into getLastHOSLast function------>");
		var response = $http.post($scope.projectName+'/getLastHOSLast');
		response.success(function(data, status, headers, config){
			$scope.hOSLast = data.hOSL;
			/*console.log("value in getLastHOSLast---->"+$scope.hOSLast.hOSCnmt);
			console.log("value in getLastHOSLast---->"+$scope.hOSLast.hOSChln);
			console.log("value in getLastHOSLast---->"+$scope.hOSLast.hOSSedr);*/
			$scope.alertToast("success");
			$scope.displayBranchForSuperAdmin();
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.submitHOSLast = function(){
		console.log("Entered into submitHOSLast function---");
		$scope.hOSLast.hOSCnmt = parseInt($scope.hOSLast.hOSCnmt)-parseInt($scope.hbon.noCnmt);
		$scope.hOSLast.hOSChln = parseInt($scope.hOSLast.hOSChln)-parseInt($scope.hbon.noChln);
		$scope.hOSLast.hOSSedr = parseInt($scope.hOSLast.hOSSedr)-parseInt($scope.hbon.noSedr);
		$scope.hOSLast.hOSCnmtDisOrRec = $scope.hbon.noCnmt;
		$scope.hOSLast.hOSChlnDisOrRec = $scope.hbon.noChln;
		$scope.hOSLast.hOSSedrDisOrRec = $scope.hbon.noSedr;		
		var response = $http.post($scope.projectName+'/submitHOSLast',$scope.hOSLast);
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				console.log("message from server --->>"+data.result);
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.getListOfEmployee = function(){
		   console.log(" entered into getListOfEmployee------>");
		   var response = $http.post($scope.projectName+'/getListOfEmp');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				    $scope.employeeList = data.list;
				    $scope.successToast(data.result);
				}else{
					console.log(data);
				}
			   $scope.getStationCodeData();
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	   }
	
	$scope.openBranchDirectorDB = function(){
		 $scope.branchDirectorDBFlag = false;
			$('div#branchDirectorDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchDirectorDB').dialog('open');
	}
	
	$scope.openBranchMarketingHDDB = function(){
		 $scope.branchMarketingHDDBFlag = false;
			$('div#branchMarketingHDDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchMarketingHDDB').dialog('open');
	}
	
	$scope.openBranchOutStandingHDDB = function(){
		 $scope.branchOutStandingHDDBFlag = false;
			$('div#branchOutStandingHDDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchOutStandingHDDB').dialog('open');
	}
	
	$scope.openBranchMarketingDB = function(){
		 $scope.branchMarketingDBFlag = false;
			$('div#branchMarketingDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchMarketingDB').dialog('open');
	}
	
	$scope.openBranchMngrDB = function(){
		 $scope.branchMngrDBFlag = false;
			$('div#branchMngrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchMngrDB').dialog('open');
	}
	
	$scope.openBranchCashierDB = function(){
		 $scope.branchCashierDBFlag = false;
			$('div#branchCashierDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchCashierDB').dialog('open');
	}
	
	$scope.openBranchTrafficDB = function(){
		 $scope.branchTrafficDBFlag = false;
			$('div#branchTrafficDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchTrafficDB').dialog('open');
	}
	
	$scope.openBranchAreaMngrDB = function(){
		 $scope.branchAreaMngrDBFlag = false;
			$('div#branchAreaMngrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchAreaMngrDB').dialog('open');
	}
	
	$scope.openBranchRegionalMngrDB = function(){
		 $scope.branchRegionalMngrDBFlag = false;
			$('div#branchRegionalMngrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchRegionalMngrDB').dialog('open');
	}
	
	
	$scope.saveBranchDirector =  function(empBranchDirector){
		console.log("enter into saveBranchDirector----->");
		$scope.branch.branchDirector = empBranchDirector.empCode;
		$('div#branchDirectorDB').dialog('close');
		$scope.branchDirectorDBFlag = true;	
	}
	
	$scope.saveBranchMarketingHD =  function(empBranchMarketingHD){
		console.log("enter into saveBranchMarketingHD----->");
		$scope.branch.branchMarketingHD = empBranchMarketingHD.empCode;
		$('div#branchMarketingHDDB').dialog('close');
		$scope.branchMarketingHDDBFlag = true;
	}
	
	$scope.saveBranchOutStandingHD =  function(empBranchOutStandingHD){
		console.log("enter into saveBranchOutStandingHD----->");
		$scope.branch.branchOutStandingHD = empBranchOutStandingHD.empCode;
		$('div#branchOutStandingHDDB').dialog('close');
		$scope.branchOutStandingHDDBFlag = true;
		
	}
	
	$scope.saveBranchMarketing =  function(empBranchMarketing){
		console.log("enter into saveBranchMarketing----->");
		$scope.branch.branchMarketing = empBranchMarketing.empCode;
		$('div#branchMarketingDB').dialog('close');
		$scope.branchMarketingDBFlag = true;
		
	}
	
	$scope.saveBranchMngr =  function(empBranchMngr){
		console.log("enter into saveBranchMngr----->");
		$scope.branch.branchMngr = empBranchMngr.empCode;
		$('div#branchMngrDB').dialog('close');
		$scope.branchMngrDBFlag = true;
		
	}
	
	$scope.saveBranchCashier =  function(empBranchCashier){
		console.log("enter into saveBranchCashier----->");
		$scope.branch.branchCashier = empBranchCashier.empCode;
		$('div#branchCashierDB').dialog('close');
		$scope.branchCashierDBFlag = true;
		
	}
	
	$scope.saveBranchTraffic =  function(empBranchTraffic){
		console.log("enter into saveBranchTraffic----->");
		$scope.branch.branchTraffic = empBranchTraffic.empCode;
		$('div#branchTrafficDB').dialog('close');
		$scope.branchTrafficDBFlag = true;
		
	}
	
	$scope.saveBranchAreaMngr =  function(empBranchAreaMngr){
		console.log("enter into saveBranchAreaMngr----->");
		$scope.branch.branchAreaMngr = empBranchAreaMngr.empCode;
		$('div#branchAreaMngrDB').dialog('close');
		$scope.branchAreaMngrDBFlag = true;
		
	}
	
	
	$scope.saveBranchRegionalMngr =  function(empBranchRegionalMngr){
		console.log("enter into saveBranchRegionalMngr----->"); 
		$scope.branch.branchRegionalMngr = empBranchRegionalMngr.empCode;
		$('div#branchRegionalMngrDB').dialog('close');
		 $scope.branchRegionalMngrDBFlag = true;
		 
	}
	
	$scope.getStationCodeData = function(){
		console.log("getStationCodeData------>");
		var response = $http.post($scope.projectName+'/getStationCodeData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stationList = data.list;
				$scope.successToast(data.result);
				console.log("data from server-->"+$scope.stationList);
				$scope.getLastHOSLast();
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.openBranchStationCodeDB = function(){
			$scope.branchStationCodeDBFlag = false;
			$('div#branchStationCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Station Code",
				draggable: true,
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
			});
		
			$('div#branchStationCodeDB').dialog('open');
	}
	
	$scope.saveBranchStationCode =  function(station){
		console.log("enter into saveBranchStationCode----->"+station.stnCode);
		$scope.branchStationName = station.stnName;
		$scope.branch.branchStationCode = station.stnCode;
		$('div#branchStationCodeDB').dialog('close');
		$scope.branchStationCodeDBFlag = true;
	}
	
       $('#branchname').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchEmailId').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchWebsite').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });

	   
	      
	   $('#branchPhone').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#branchPhone').keypress(function(e) {
	       if (this.value.length == 15) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchFax').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#branchFax').keypress(function(e) {
	       if (this.value.length == 20) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#branchPin').keypress(function(e) {
	       if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $scope.getModifiedBranchDetail = function(branchCode){
		   console.log("Entered into getModifiedBranchDetail function----");
		   var response = $http.post($scope.projectName+'/getModifiedBranchDetail',branchCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
					$scope.branchOldList = data.list;
					for(var i=0;i<$scope.branchOldList.length;i++){
						$scope.branchOldList[i].creationTS =  $filter('date')($scope.branchOldList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
					}
			   }else if(data.result==="error"){
					$scope.alertToast("Selected branch has never been modified....")
					$("#creationTSId").attr('disabled','disabled');
			   }	
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		   });
	   }
	   
	   $scope.OpenCreationTSDB = function(){
		   $scope.creationTSDBFlag = false;
			$('div#creationTSDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Modification Time List",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
			});
			$('div#creationTSDB').dialog('open');
	   }
	   
	   $scope.saveCreationTS = function(creationTime){
		   $scope.creationTSModel = creationTime;
		   $('div#creationTSDB').dialog('close');
		   $scope.creationTSDBFlag = true;
		   $scope.editBranchDetailsDBFlag = true;
		   for(var i=0;i<$scope.branchOldList.length;i++){
			   if($scope.branchOldList[i].creationTS === creationTime){
				   console.log("Entered into if--")
				   $scope.branchOld = $scope.branchOldList[i];
				   $scope.viewBranchOldDetailsFlag = false;
			   }
		   }
	   }
	 
	   
	   
	   if($scope.superAdminLogin === true || $scope.superAdminLogin === true){
		   $scope.getListOfEmployee();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		
	
	//$scope.getLastHOSLast();
//	$scope.displayBranchForSuperAdmin();
	
}]);
