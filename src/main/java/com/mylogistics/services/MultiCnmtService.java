package com.mylogistics.services;

import java.util.List;
import java.util.Map;


public interface MultiCnmtService {

	public List<Map<String,Object>> getAllMultiCnmt();

    public void addMultiCnmt(Map<String,Object> multiCnmtCode);

    public void deleteMultiCnmt(Map<String,Object> map);

    public void deleteAllMultiCnmt();
    
}
