package com.mylogistics.pdf;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mylogistics.services.CodePatternService;

public class TrialReportPDFBuilder extends AbstractITextPdfView {

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> map, Document doc, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		List<Map<String, Object>> faCodeNameList = (List<Map<String, Object>>) map.get("faCodeNameList");
		Map<String, Object> trialRepMap = (Map<String, Object>) map.get("trialReportMap");
		double cashInClosing = (double) map.get("cashInClosing");
		double totalDr = 0.0;
		double totalCr = 0.0;
		
		Paragraph paragraph = new Paragraph();
		
		Font fontHeading = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		fontHeading.setSize(25);
		
		Chunk sigUnderline = new Chunk("TRIAL REPORT");
		sigUnderline.setUnderline(0.1f, -2f);
		    
		paragraph.setAlignment(Element.ALIGN_CENTER);
		paragraph.setFont(fontHeading);
		paragraph.add(sigUnderline);
		
		Paragraph subPara = new Paragraph(); 
		
		Chunk subChunk; 
		
		if ((boolean) trialRepMap.get("isBranch")) {
			subChunk = new Chunk("("+String.valueOf(trialRepMap.get("branchName"))+")");
			
		} else {
			subChunk = new Chunk("(Consolidated)");
		}
		
		Font subFont = FontFactory.getFont(FontFactory.COURIER_BOLD);
		
		subPara.setAlignment(Element.ALIGN_CENTER);
		subPara.setFont(subFont);
		subPara.setSpacingAfter(10);
		subPara.setSpacingBefore(0);
		subPara.add(subChunk);
		
		doc.add(paragraph);
		doc.add(subPara);
		
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100.0f);
		table.setWidths(new float[] {0.6f, 2.0f, 0.7f, 0.7f});
		table.setSpacingBefore(4);
		
		// define font for table header row
		Font headerfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		headerfont.setSize(10);
		headerfont.setColor(BaseColor.WHITE);
		
		// define table header cell
		PdfPCell headerCell1 = new PdfPCell();
		headerCell1.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell1.setColspan(2);
		headerCell1.setPaddingTop(5);
		headerCell1.setPaddingBottom(5);
		headerCell1.setPaddingLeft(2);
		headerCell1.setPaddingRight(2);
		headerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell headerCell2 = new PdfPCell();
		headerCell2.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell2.setColspan(2);
		headerCell2.setPaddingTop(5);
		headerCell2.setPaddingBottom(5);
		headerCell2.setPaddingLeft(2);
		headerCell2.setPaddingRight(2);
		headerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		
		// write table header 
		headerCell1.setPhrase(new Phrase("Financial Year: "+trialRepMap.get("finYr"), headerfont));
		table.addCell(headerCell1);
		headerCell2.setPhrase(new Phrase("Upto Date: "+CodePatternService.getFormatedDateString(String.valueOf(trialRepMap.get("uptoDt"))), headerfont));
		table.addCell(headerCell2);
		
		//font for subheader
		Font subHeaderFont = FontFactory.getFont(FontFactory.COURIER_BOLD);
		subHeaderFont.setSize(12);
		subHeaderFont.setColor(BaseColor.WHITE);
		
		//cell for subheader
		PdfPCell subHeaderCell = new PdfPCell();
		subHeaderCell.setBackgroundColor(new BaseColor(38, 166, 154));
		subHeaderCell.setColspan(1);
		subHeaderCell.setPaddingTop(5);
		subHeaderCell.setPaddingBottom(5);
		subHeaderCell.setPaddingLeft(5);
		subHeaderCell.setPaddingRight(5);
		subHeaderCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		//write table sub header
		subHeaderCell.setPhrase(new Phrase("A/C Code", subHeaderFont));
		table.addCell(subHeaderCell);
		subHeaderCell.setPhrase(new Phrase("A/C Head", subHeaderFont));
		table.addCell(subHeaderCell);
		subHeaderCell.setPhrase(new Phrase("Debit", subHeaderFont));
		table.addCell(subHeaderCell);
		subHeaderCell.setPhrase(new Phrase("Credit", subHeaderFont));
		table.addCell(subHeaderCell);
		
		//font for table
		Font font = FontFactory.getFont(FontFactory.TIMES);
		font.setSize(9);
		font.setColor(BaseColor.BLACK);
		
		//cell for table
		PdfPCell cell = new PdfPCell();
		cell.setColspan(1);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell cellLeftAlign = new PdfPCell();
		cellLeftAlign.setColspan(1);
		
		//font for balance
		Font fontBalance = FontFactory.getFont(FontFactory.TIMES);
		fontBalance.setSize(10);
		fontBalance.setColor(BaseColor.BLUE);
		
		//cell for Balance
		PdfPCell cellBalance = new PdfPCell();
		cellBalance.setColspan(2);
		cellBalance.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//cell for total
		PdfPCell cellTotalColSpan2 = new PdfPCell();
		cellTotalColSpan2.setColspan(2);
		cellTotalColSpan2.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cellTotalColSpan2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellTotalColSpan1 = new PdfPCell();
		cellTotalColSpan1.setColspan(1);
		cellTotalColSpan1.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cellTotalColSpan1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		//font for balance
		Font fontTotal = FontFactory.getFont(FontFactory.TIMES);
		fontBalance.setSize(10);

		DecimalFormat df = new DecimalFormat("#.00");
        df.setMaximumFractionDigits(2);
		
		for (Map<String, Object> faCodeName : faCodeNameList) {
		
			cell.setPhrase(new Phrase(String.valueOf(faCodeName.get("csFaCode")), font));
			table.addCell(cell);
			cellLeftAlign.setPhrase(new Phrase(String.valueOf(faCodeName.get("csFaName")), font));
			table.addCell(cellLeftAlign);
			
			if (String.valueOf(faCodeName.get("csDrCrBalance")).equalsIgnoreCase("D")) {
				cell.setPhrase(new Phrase(df.format(Double.parseDouble(String.valueOf(faCodeName.get("csAmtBalance")))), font));
				table.addCell(cell);
				
				cell.setPhrase(new Phrase("", font));
				table.addCell(cell);
				
				totalDr = totalDr + Double.parseDouble(String.valueOf(faCodeName.get("csAmtBalance")));
				
			}else if (String.valueOf(faCodeName.get("csDrCrBalance")).equalsIgnoreCase("C")) {
				cell.setPhrase(new Phrase("", font));
				table.addCell(cell);
				
				cell.setPhrase(new Phrase(df.format(Double.parseDouble(String.valueOf(faCodeName.get("csAmtBalance")))), font));
				table.addCell(cell);
				
				totalCr = totalCr + Double.parseDouble(String.valueOf(faCodeName.get("csAmtBalance")));
			} else {
				cell.setPhrase(new Phrase("", font));
				table.addCell(cell);
				
				cell.setPhrase(new Phrase("", font));
				table.addCell(cell);
			}
		}
		
		totalDr = totalDr + cashInClosing;
		
		/*cellBalance.setPhrase(new Phrase("1110131", fontBalance));
		table.addCell(cellBalance);*/
		
		cellBalance.setPhrase(new Phrase("CLOSING BALANCE (cash in branch)", fontBalance));
		table.addCell(cellBalance);
		
		cell.setPhrase(new Phrase(df.format(cashInClosing), fontBalance));
		table.addCell(cell);
		cell.setPhrase(new Phrase("", fontBalance));
		table.addCell(cell);
		
		cellTotalColSpan2.setPhrase(new Phrase("Total", fontTotal));
		table.addCell(cellTotalColSpan2);
		
		cellTotalColSpan1.setPhrase(new Phrase(df.format(totalDr), fontTotal));
		table.addCell(cellTotalColSpan1);
		cellTotalColSpan1.setPhrase(new Phrase(df.format(totalCr), fontTotal));
		table.addCell(cellTotalColSpan1);
		
		doc.add(table);
	}

}
