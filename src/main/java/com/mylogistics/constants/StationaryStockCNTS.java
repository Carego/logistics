package com.mylogistics.constants;

public class StationaryStockCNTS {

	public static final String SS_ID = "stStkId";
	
	public static final String SS_CODE = "stStkCode";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
