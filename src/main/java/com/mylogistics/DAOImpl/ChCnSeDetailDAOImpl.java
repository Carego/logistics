package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ChCnSeDetailDAO;
import com.mylogistics.model.ChCnSeDetail;

public class ChCnSeDetailDAOImpl implements ChCnSeDetailDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
		public  ChCnSeDetailDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		}
	
	@Transactional
	public int saveChCnSE(ChCnSeDetail chCnSeDetail){
		   int temp;
		   	try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 session.save(chCnSeDetail);
				 transaction.commit();
				 System.out.println("saveChCnSE  Data is inserted successfully");
				session.flush();
				 temp= 1;
				}
			catch(Exception e){
				e.printStackTrace();
				temp= -1;
			}
		   	session.clear();
		   	session.close();
	   		return temp;
		 }
	
/*		@SuppressWarnings("unchecked")
		@Transactional
		public List<ChCnSeDetail> getLastCnmt(){
			List<ChCnSeDetail> list = new ArrayList<ChCnSeDetail>();
			
			try{
				session = this.sessionFactory.openSession();
				list = session.createQuery("from ChCnSeDetail where chCnSeStatus='cnmt' order by chCnSeId DESC").setMaxResults(1).list();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			session.close();
			return list;
	}
		
		@SuppressWarnings("unchecked")
		@Transactional
		public List<ChCnSeDetail> getLastChln(){
			List<ChCnSeDetail> list = new ArrayList<ChCnSeDetail>();
			
			try{
				session = this.sessionFactory.openSession();
				list = session.createQuery("from ChCnSeDetail where chCnSeStatus='chln' order by chCnSeId DESC").setMaxResults(1).list();
				session.flush();
		 		}
			catch(Exception e){
				e.printStackTrace();
			}
			 
			 session.close();
			 return list;
		}
		
		@SuppressWarnings("unchecked")
		@Transactional
		public List<ChCnSeDetail> getLastSedr(){
			List<ChCnSeDetail> list = new ArrayList<ChCnSeDetail>();
			
			try{
				session = this.sessionFactory.openSession();
				list = session.createQuery("from ChCnSeDetail where chCnSeStatus='sedr' order by chCnSeId DESC").setMaxResults(1).list();
				 session.flush();
		 		}
			catch(Exception e){
				e.printStackTrace();
			}
			
			 session.close();
			
			 return list;
		}*/
		
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ChCnSeDetail> getLastStationary(String status) {
		List<ChCnSeDetail> list = new ArrayList<ChCnSeDetail>();

		try {
			session = this.sessionFactory.openSession();
			list = session
					.createQuery(
							"from ChCnSeDetail where chCnSeStatus='status' order by chCnSeId DESC")
					.setMaxResults(1).list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();

		return list;
	}
}
