<div ng-show="operatorLogin || superAdminLogin">

<div class="row">
	<form name=empWiseForm class="col s12 card"
		style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="empWiseForm" ng-submit="submitForm(empWiseForm)">

		<div class="row">
			
			<div class="col s1">
				<input type="radio" name="btn" ng-click="newEmp()" />
			</div>
			<div class="input-field col s5">
				<input type="text" name="newEmpName" id="newEmpId"
					ng-model="newEmp.empName" ng-click="openNewEmpDB()" disabled="disabled"
					readonly> <label>Select New Employee</label>
			</div>
			
			<div class="col s1">
				<input type="radio" name="btn" ng-click="existEmp()" />
			</div>
			<div class="input-field col s5">
				<input type="text" name="existEmpName" id="existEmpId"
					ng-model="existEmp.empName" ng-click="openExistEmpDB()"
					disabled="disabled" readonly> <label>Select Existing Employee</label>
			</div>
			
		</div>
		
		<div class="row">
			<div class="input-field col s6">
				<input type="text" name="brName" id="brId" ng-model="branch.branchName" ng-click="openBranchDB()" 
					disabled="disabled"  ng-required="true" readonly> 
					<label>Assigned Branch</label>
			</div>	
		</div>
		
		<div class="row">
			<div class="col s12 center">
				<input class="btn" type="submit" value="Submit">
			</div>
		</div>
	</form>
</div>	


<div id="newEmpDB" ng-hide="newEmpDBFlag">
		<input type="text" name="filterNewEmpName" ng-model="filterNewEmp" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewEmp">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpCode"
							ng-click="saveNewEmp(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpDB" ng-hide="existEmpDBFlag">
		<input type="text" name="filterExistEmpName" ng-model="filterExistEmp" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExistEmp">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpCode"
							ng-click="saveExistEmp(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="openBrDB" ng-hide="openBrDBFlag">
		<input type="text" name="filterBrhName" ng-model="filterBrhName" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Branch Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="branch in branchList | filter:filterBrhName">
					<td><input type="radio" name="branch" value="{{ branch }}" ng-model="branchCode"
							ng-click="saveBranch(branch)"></td>
					<td>{{ branch.branchName }}</td>
					<td>{{ branch.branchFaCode }}</td>
				</tr>
			</table>
</div>


<div id="saveDB" ng-hide="saveFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				
				<tr>
					<td>New Employee</td>
					<td>{{ newEmp.empName }}</td>
				</tr>
				
				<tr>
					<td>Existing Employee</td>
					<td>{{ existEmp.empName }}</td>
				</tr>
				
				<tr>
					<td>Branch</td>
					<td>{{ branch.branchName }}</td>
				</tr>
			</table>
			<input type="button" value="Save" id="saveId" ng-click="save()"/>
			<input type="button" value="Cancel" ng-click="back()"/>
		</div>
</div>

</div>