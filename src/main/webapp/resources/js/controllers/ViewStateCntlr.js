'use strict';

var app = angular.module('application');

app.controller('ViewStateCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.stateCodeDBFlag = true;
	$scope.stateCodeDetailsFlag = true;
	$scope.stateDetailsFlag = false;
	$scope.state = {};
	
	  $scope.getStateDetails = function(){
			console.log("Entered into getStateDetails function------>");
			var response = $http.post($scope.projectName+'/getStateDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stateList = data.list;
					for(var i=0;i<$scope.stateList.length;i++){
						$scope.stateList[i].creationTS =  $filter('date')($scope.stateList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
					}
					$scope.successToast(data.result);
					//$scope.getStateCodeDetails();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
			
		}
	  
	  $scope.openStateCodeDB = function(){
		  $scope.stateCodeDBFlag = false;
			$('div#stateCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				draggable: true,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "State Code",
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
				});
			
			$('div#stateCodeDB').dialog('open');
	  }
	  
	  $scope.saveStateCode =  function(state){
			console.log("enter into saveStateCode----->"+state.stateCode);
			$scope.stateCode = state.stateCode;
			$scope.stateName = state.stateName;
			$('div#stateCodeDB').dialog('close');
			$scope.stateCodeDBFlag = true;
		}
			
		/*$scope.getStateCodeDetails = function(){
			console.log("getStateCodeDetails------>");
			var response = $http.post($scope.projectName+'/getStateCodeDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stateCodeList = data.list;
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}*/
		
		$scope.submitCode = function(stateName,stateCode){
			console.log("enter into submit function--->"+$scope.stateCode);
			$scope.code=$('#sName').val();
			if($scope.code === ""){
				$scope.alertToast("Please enter state code....");
			}else{
				var response = $http.post($scope.projectName+'/stateDetails', stateCode);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.state = data.state;
						$scope.stateCodeDetailsFlag = false;
						$scope.stateDetailsFlag = true;
						$scope.time =  $filter('date')($scope.state.creationTS, 'MM/dd/yyyy hh:mm:ss');
						$scope.successToast(data.result);
						
					}else{
						console.log(data);
					}				
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}
		
		$scope.updateState = function(StateForm){
			console.log("Entered into updateState function");
			
			 if(StateForm.$invalid){
					 if(StateForm.stateName.$invalid){
						$scope.alertToast("Please enter State Name between 3-40 characters... ");
					}else if(StateForm.stateLryPrefix.$invalid){
						$scope.alertToast("Please enter at least one lorry prefix of maximum 3 characters... ");
					}else if(StateForm.stateSTD.$invalid){
						$scope.alertToast("Please enter State STD of 2-5 digits... ");
					}
			 }else{	
				 var response = $http.post($scope.projectName+'/updateState',$scope.state);
					console.log("********************");
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							console.log("success");
							$scope.successToast(data.result);
							$scope.state="";
							$scope.stateCode="";
							$scope.stateCodeDetailsFlag = true;
							$scope.stateDetailsFlag = false;
							$scope.getStateDetails();
						}else{
							console.log(data);
						}		
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
				    });
					
			 }
			
		}
		
	   $('#std').keypress(function(key) {
		       if(key.charCode < 48 || key.charCode > 57)
		           return false;
	   });

	   $('#std').keypress(function(e) {
		       if (this.value.length == 5) {
		           e.preventDefault();
		       }
	   });
	  
	   if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		   $scope.getStateDetails();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		
	 
	
}]);