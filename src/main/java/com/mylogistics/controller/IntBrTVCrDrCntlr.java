package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.IBTVoucherDAO;
import com.mylogistics.DAO.TempIntBrTvDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.TempIntBrTv;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.IBTService;
import com.mylogistics.services.VoucherService;

@Controller
public class IntBrTVCrDrCntlr {

	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private IBTVoucherDAO ibtVoucherDAO;
	
	@Autowired
	private TempIntBrTvDAO tempIntBrTvDAO;
	
	
	
	@RequestMapping(value = "/getVDetFrIBTV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrIBTV() {  
		System.out.println("Enter into getVDetFrIBTV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				/*List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());*/
				if(cashStmtStatus != null){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					//map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	
	
	@RequestMapping(value = "/getBrListFrIBTV" , method = RequestMethod.POST)  
	public @ResponseBody Object getBrListFrIBTV() {  
		System.out.println("Enter into getBrListFrIBTV---->");
		Map<String, Object> map = new HashMap<>();	
		List<Branch> brList = branchDAO.getAllActiveBranches();
		if(!brList.isEmpty()){
			map.put("list",brList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/getPendingIBTV" , method = RequestMethod.POST)  
	public @ResponseBody Object getPendingIBTV(@RequestBody String brFaCode) {  
		System.out.println("Enter into getPendingIBTV---->");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<>();	
		List<TempIntBrTv> tibtList = new ArrayList<>();
		tibtList = tempIntBrTvDAO.getOpenIBTV(brFaCode);
		
		List<Map<String,Object>> bnkList = bankMstrDAO.getBnkNameAndCode(currentUser.getUserBranchCode());
		System.out.println("size of bnkList = "+bnkList.size());
		map.put("bnkList",bnkList);
		map.put("list",tibtList);
		return map;
	}	
	
	
	
	@RequestMapping(value = "/clrIBTV" , method = RequestMethod.POST)  
	public @ResponseBody Object clrIBTV(@RequestBody TempIntBrTv tempIntBrTv) {  
		System.out.println("Enter into clrIBTV---->");
		Map<String, Object> map = new HashMap<>();	
		int res = ibtVoucherDAO.clrSIBTV(tempIntBrTv);
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	
	@RequestMapping(value = "/getLoginBrDet" , method = RequestMethod.POST)  
	public @ResponseBody Object getLoginBrDet(@RequestBody String brFaCode) {  
		System.out.println("Enter into getLoginBrDet---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Branch> brList = branchDAO.retrieveBranch(currentUser.getUserBranchCode());
		if(!brList.isEmpty()){
			Branch branch = brList.get(0);
			map.put("branch",branch);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/clrAllIBTV" , method = RequestMethod.POST)  
	public @ResponseBody Object clrAllIBTV(@RequestBody IBTService ibtService) {  
		System.out.println("Enter into getLoginBrDet---->");
		Map<String, Object> map = new HashMap<>();
		List<TempIntBrTv> tibtList = ibtService.getTibList();
		if(!tibtList.isEmpty()){
			map = ibtVoucherDAO.clrAllIBTV(ibtService);
			int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitIBTVouch" , method = RequestMethod.POST)  
	public @ResponseBody Object submitIBTVouch(@RequestBody IBTService ibtService) {  
		System.out.println("Enter into submitIBTVouch---->");
		Map<String, Object> map = new HashMap<>();	
		VoucherService voucherService = ibtService.getVoucherService();
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.IBTV_VOUCHER)){
			map = ibtVoucherDAO.saveIBTVoucher(ibtService);
			int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	//TODO Back Data
	@RequestMapping(value = "/getVDetFrIBTVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrIBTVBack() {  
		System.out.println("Enter into getVDetFrIBTV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				/*List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());*/
				if(cashStmtStatus != null){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					//map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	
}
