package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "famaster")
public class FAMaster {

	@Id
	@Column(name="faMId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int faMId;
	
	@Column(name="faMfaCode")
	private String faMfaCode;
	
	@Column(name="faMfaType")
	private String faMfaType;
	
	@Column(name="faMfaName")
	private String faMfaName;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	/*@JsonManagedReference(value = "faCs")
	@JsonIgnore
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="famaster_cashstmt",
	joinColumns=@JoinColumn(name="faMId"),
	inverseJoinColumns=@JoinColumn(name="csId"))
	private List<CashStmt> cashStmtList = new ArrayList<CashStmt>();*/
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getFaMId() {
		return faMId;
	}

	public void setFaMId(int faMId) {
		this.faMId = faMId;
	}

	public String getFaMfaCode() {
		return faMfaCode;
	}

	public void setFaMfaCode(String faMfaCode) {
		this.faMfaCode = faMfaCode;
	}

	public String getFaMfaType() {
		return faMfaType;
	}

	public void setFaMfaType(String faMfaType) {
		this.faMfaType = faMfaType;
	}

	public String getFaMfaName() {
		return faMfaName;
	}

	public void setFaMfaName(String faMfaName) {
		this.faMfaName = faMfaName;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	/*public List<CashStmt> getCashStmtList() {
		return cashStmtList;
	}

	public void setCashStmtList(List<CashStmt> cashStmtList) {
		this.cashStmtList = cashStmtList;
	}*/
	
}
