<div ng-show="operatorLogin || superAdminLogin" >
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		
		<!-- Find By Branch Form -->
		<form name="ImageForm" id="imageForm" method="POST" ng-submit="readImage(ImageForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
				<div class="col s3 input-field">
		    		<select name="imageType" id="imageType" ng-change="changeFlag()" ng-model="image.imageType" ng-init="image.imageType='cnmt'" required>
						<option value='cnmt'>CNMT</option>
						<option value='own'>Owner</option>
						<option value='brk'>Broker</option>
						<option value='chln'>Challan</option>
						<option value='lry'>Lorry No.</option>
						<option value='bil'>Bill No.</option>
						<option value='bf'>Bill Submission</option>
					</select>
					<label>Select image type</label>
				</div>
				
				<div class="col s3 input-field" ng-hide="ownBrkFlag">
		    		<select name="ownBrkType" id="ownBrkImgType" ng-model="image.ownBrkImgType" ng-init="image.ownBrkImgType='pan'">
						<option value='pan'>PAN</option>
						<option value='dec'>Declaration</option>
						<option value='chq'>A/C proof</option>
						<option value='rc'>RC</option>
						<option value='pd'>Policy Doc</option>
						<option value='ps'>Permit Slip</option>						
					</select>
					<label>Select PAN/Dec.</label>
				</div>
				
				<div class="col s3 input-field" ng-hide="ownBrkFlag">		       		
		       		<input class="validate" type ="text" id="ownBrkNC" name="ownBrkNC" ng-model="image.ownBrkNC" ng-keyup="getList($event.keyCode)">
		       		<label>Name/Challan/Lorry</label>
		       	</div>	
				
				<div class="col s3 input-field" ng-hide="cnmtFlag">		       		
		       		<input class="validate" type ="text" id="cnmtNo" name="cnmtNo" ng-model="image.cnmtNo">
		       		<label>CNMT No.</label>
		       	</div>	
		       	
		       	<div class="col s3 input-field" ng-hide="cnmtFlag">		       		
		       		<input class="validate" type ="text" id="cnmtChlnNo" name="cnmtChlnNo" ng-model="image.cnmtChlnNo">
		       		<label>Challan No.</label>
		       	</div>
		       	
		       	<div class="col s3 input-field" ng-hide="bilFlag">		       		
		       		<input class="validate" type ="text" id="bilNo" name="bilNo" ng-model="image.bilNo">
		       		<label>BILL No.</label>
		       	</div>
		       	
		       	<div class="col s3 input-field" ng-hide="bfNoFlag">		       		
		       		<input class="validate" type ="text" id="bfNo" name="bfNo" ng-model="image.bfNo">
		       		<label>BILL Forwarding No.</label>
		       	</div>	
		       					       	
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" value="View Image" >
	       		</div>
			</div>
			
			
			
		</form>
		
	</div>	
	
	<div id ="ownBrkDB" ng-hide="ownBrkDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr> 	  	  	  
 	  	  		<th></th>
 	  	  	  <th>S.No. </th>
 	  	  	  <th>FaCode</th>
 	  	  	  <th>Name</th>	  	    
 	  	  </tr>
		  <tr ng-repeat="obj in ownBrkList | filter:filterTextbox1">
		  	  <td><input type="radio" name="code" value="{{ obj.faCode }}" ng-click="saveOwnBrk(obj)"></td>		 	 
              <td>{{$index + 1}}</td>
              <td>{{obj.faCode}}</td>
              <td>{{obj.name}}</td>
          </tr>
      </table> 
	</div>

</div>