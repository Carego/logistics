'use strict';

var app = angular.module('application');

app.controller('UnholdDocsCntlr',['$scope','$location','$http','$filter','AuthorisationService',
                                     function($scope,$location,$http,$filter,AuthorisationService){
	
	
                          console.log("hiiiiiiiiiiiiiii");
                          $scope.chlnCodeDBFlag=true;
                          $scope.obj={};
                          $scope.openChlnCodeDb=function(){
                        	  $scope.obj={};
                        	  if($scope.docSer==="CH"){
                        		  var response = $http.post($scope.projectName+'/getChlnFrUnhold');
                            		response.success(function(data, status, headers, config) {
                            			if(data.result === "success"){
                            				$scope.chlnCodeList=data.list;
                              				console.log(data);
                              				$scope.chlnCodeDBFlag=false;
                              				$('div#chlnCodeDB').dialog({
                              					autoOpen: false,
                              					modal:true,
                              					resizable: false,
                              					show: UDShow,
                              					title:"Select Challan",
                              					hide: UDHide,
                              					position: UDPos,
                              					draggable: true,
                              					close: function(event, ui) { 
                              				        $(this).dialog('destroy');
                              				        $(this).hide();
                              				        $scope.arCodeDBFlag = true;
                              				    }
                              					});
                              		    	$('div#chlnCodeDB').dialog('open'); 
                              				
                            			}else{
                              				$scope.alertToast("There is no any challan on hold");
                              				console.log("error in fetching result from challan");
                              			}
                              		});
                              		response.error(function(data, status, headers, config) {
                              			$scope.errorToast(data.result);
                              		});
                        	  }
                        	 
                      				
                      			
                          }
                          
                          $scope.changeDocs=function(docSer){
                        	  $scope.docSer=docSer;
                        	  console.log("$scope.docSer "+$scope.docSer);
                        	 /* if($scope.docSer==="CH"){
                        		  var response = $http.post($scope.projectName+'/getChlnFrUnhold');
                            		response.success(function(data, status, headers, config) {
                            			if(data.result === "success"){
                            				$scope.chlnCodeList=data.list;
                              				console.log(data);
                            			}else{
                              				$scope.alertToast("There is no any challan on hold");
                              				console.log("error in fetching result from challan");
                              			}
                              		});
                              		response.error(function(data, status, headers, config) {
                              			$scope.errorToast(data.result);
                              		});
                        	  }*/
                          }
                          
                          $scope.saveChln=function(chlnCode){
                        	  $scope.chlnCode=chlnCode;
                        	  $('div#chlnCodeDB').dialog('close'); 
                        	  
                        	  var response=$http.post($scope.projectName+'/getObjFrUnhldDocs',$scope.chlnCode);
                        	  response.success(function(data,status,headers,config){
                        		 if(data.result==="success"){
                        			 $scope.obj=data.list;
                        			 console.log(data.list);
                        			 console.log($scope.obj);
                        			 $scope.obj.unHoldDt=$filter('date')(new Date, "yyyy-MM-dd");
                        			 console.log("date=="+$scope.obj.unHoldDt);
                        		 }else{
                        			 $scope.errorToast("there is some problem");
                        		 }
                        	  });
                        	  response.error(function(data, status, headers, config) {
                        		 $scope.errorToast(data.result); 
                        	  });
                        	  
                          }
                          
                          $scope.unHoldDocs=function(){
                        	  console.log($scope.obj);
                        	  
                        	  var response=$http.post($scope.projectName+'/allowDocsFrHold',$scope.obj);
                        	  response.success(function(data,status,headers,config){
                        		 if(data.result==="success"){
                        			 $scope.alertToast("successfully unHold");
                        			 $scope.obj={};
                        			 $scope.chlnCode="";
                        		 }else{
                        			 $scope.errorToast("there is some problem");
                        		 } 
                        	  });
                        	  response.error(function(data, status, headers, config) {
                         		 $scope.errorToast(data.result); 
                         	  });  
                        	  
                        	  
                          }
                          
                          
	
}]);