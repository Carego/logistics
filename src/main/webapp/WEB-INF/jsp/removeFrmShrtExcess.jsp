<div ng-show="operatorLogin || superAdminLogin">
<title>Remove From Short Excess</title>
<div class="row">
	<div class="col s2 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s8 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="formName" ng-submit="removeBillFromShrtExs(formName)">
	
	<div class="row">
	<div  class="input-field col s6">
		<input type="text" name="customerName" id="customerNameId" ng-model="customer.custName" ng-click="openCustDB()" readonly>
		<label>Customer Name</label>
	</div>
	<div  class="input-field col s4">
		<input type="text" name="billNoName" id="billNoId" ng-model="billNo"  required>
		<label>Bill No.</label>
	</div>
	</div>
	
	<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
      			</div>

</form>
</div>

 <div id ="custDB" ng-hide="custDBFlag" class="noprint">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Customer Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="cust in custList | filter:filterTextbox2">
		 	  <td><input type="radio" name="cust" value="{{ cust }}" ng-click="saveCustForEdit(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
    

</div>
