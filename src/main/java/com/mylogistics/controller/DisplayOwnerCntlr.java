
package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.model.Owner;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayOwnerCntlr {
	
	@Autowired
	OwnerDAO ownerDAO;
	
	@Autowired
    private HttpSession httpSession;

	
	/*@RequestMapping(value="/getOwnIsViewNo	",method = RequestMethod.POST)
	public @ResponseBody Object getDlyContIsViewNo(){
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		String branchCode = currentUser.getUserBranchCode();
		
		List<Owner> owners = ownerDAO.getOwnertisViewFalse(branchCode);
		
		if(!owners.isEmpty()){
			map.put("list",owners);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,"No more owner to show");
		}
		return map;
	}
	
	@RequestMapping(value = "/getOwnerCode" , method = RequestMethod.GET)  
	  public @ResponseBody Object getOwnerCode(){
	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> owners = ownerDAO.getOwnerCodes(branchCode);
		String ownCodes [] = new String[owners.size()];
		
		for(int i=0;i<owners.size();i++){
			ownCodes[i]=owners.get(i);
		}
		
		return ownCodes;
		
		} 
	
	@RequestMapping(value = "/ownerDetails", method = RequestMethod.POST)
	public @ResponseBody Object ownerDetails(@RequestBody String ownCode) {
		Owner owner = new Owner();
		List<Owner> oList = ownerDAO.getOwnerList(ownCode);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (!oList.isEmpty()) {
			owner = oList.get(0);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("owner", owner);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/updateIsViewOwner", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewOwner(@RequestBody String selection) {

		String contids[] = selection.split(",");
		
		int temp = ownerDAO.updateOwnerisViewTrue(contids);
	
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/EditOwnerSubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditOwnerSubmit(@RequestBody Owner owner) {

		int temp=ownerDAO.updateOwner(owner);
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(temp>0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getOwnerCodesList", method = RequestMethod.POST)
	public @ResponseBody Object getOwnerCodesList() {
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> bList = ownerDAO.getOwnerCodes(branchCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}*/
}
