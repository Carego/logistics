package com.mylogistics.model.app;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="appuser")
public class AppUser {

	@Id
	@Column(name="appUId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int appUId;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="appUserId",length=64)
	private String appUserId;
	
	@Column(name="appUserPaswrd",length=128)
	private String appUserPaswrd;
	
	@Column(name="appUserName",length=128)
	private String appUserName;
	
	@Column(name="appUserPhoneNo",length=15)
	private String appUserPhoneNo;
	
	@Column(name="appOTP",length=6)
	private String appOTP;
	
	@Column(name="bCode",length=4)
	private String bCode;
	
	@Column(name="appEmpCode",length=10)
	private String appEmpCode;
	
	@Column(name="appUserStatus",length=15)
	private String appUserStatus;
	
	@Column(name="appUserCode",length=15)
	private String appUserCode;

	public int getAppUId() {
		return appUId;
	}

	public void setAppUId(int appUId) {
		this.appUId = appUId;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(String appUserId) {
		this.appUserId = appUserId;
	}

	public String getAppUserPaswrd() {
		return appUserPaswrd;
	}

	public void setAppUserPaswrd(String appUserPaswrd) {
		this.appUserPaswrd = appUserPaswrd;
	}

	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getAppUserPhoneNo() {
		return appUserPhoneNo;
	}

	public void setAppUserPhoneNo(String appUserPhoneNo) {
		this.appUserPhoneNo = appUserPhoneNo;
	}

	public String getAppOTP() {
		return appOTP;
	}

	public void setAppOTP(String appOTP) {
		this.appOTP = appOTP;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getAppEmpCode() {
		return appEmpCode;
	}

	public void setAppEmpCode(String appEmpCode) {
		this.appEmpCode = appEmpCode;
	}

	public String getAppUserStatus() {
		return appUserStatus;
	}

	public void setAppUserStatus(String appUserStatus) {
		this.appUserStatus = appUserStatus;
	}

	public String getAppUserCode() {
		return appUserCode;
	}

	public void setAppUserCode(String appUserCode) {
		this.appUserCode = appUserCode;
	}
	
	
}
