package com.mylogistics.model;

//import java.util.Date;
import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hoslast")
public class HOSLast {

	@Id
	@Column(name="hOSLastId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int hOSLastId;
	
	@Column(name="hOSCnmt")
	private int hOSCnmt;
	
	@Column(name="hOSCnmtDaysLast")
	private int hOSCnmtDaysLast;
	
	@Column(name="hOSChln")
	private int hOSChln;
	
	@Column(name="hOSChlnDaysLast")
	private int hOSChlnDaysLast;
	
	@Column(name="hOSSedr")
	private int hOSSedr;
	
	@Column(name="hOSSedrDaysLast")
	private int hOSSedrDaysLast;
	
	@Column(name="hOSType")
	private String hOSType;
	
	@Column(name="hOSDate")
	private Date hOSDate;
	
	@Column(name="hOSChlnDisOrRec")
	private int hOSChlnDisOrRec;
	
	@Column(name="hOSCnmtDisOrRec")
	private int hOSCnmtDisOrRec;
	
	@Column(name="hOSSedrDisOrRec")
	private int hOSSedrDisOrRec;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	public int gethOSChlnDisOrRec() {
		return hOSChlnDisOrRec;
	}

	public void sethOSChlnDisOrRec(int hOSChlnDisOrRec) {
		this.hOSChlnDisOrRec = hOSChlnDisOrRec;
	}

	public int gethOSCnmtDisOrRec() {
		return hOSCnmtDisOrRec;
	}

	public void sethOSCnmtDisOrRec(int hOSCnmtDisOrRec) {
		this.hOSCnmtDisOrRec = hOSCnmtDisOrRec;
	}

	public int gethOSSedrDisOrRec() {
		return hOSSedrDisOrRec;
	}

	public void sethOSSedrDisOrRec(int hOSSedrDisOrRec) {
		this.hOSSedrDisOrRec = hOSSedrDisOrRec;
	}

	public int gethOSLastId() {
		return hOSLastId;
	}

	public void sethOSLastId(int hOSLastId) {
		this.hOSLastId = hOSLastId;
	}

	public int gethOSCnmt() {
		return hOSCnmt;
	}

	public void sethOSCnmt(int hOSCnmt) {
		this.hOSCnmt = hOSCnmt;
	}

	public int gethOSCnmtDaysLast() {
		return hOSCnmtDaysLast;
	}

	public void sethOSCnmtDaysLast(int hOSCnmtMonthLast) {
		this.hOSCnmtDaysLast = hOSCnmtMonthLast;
	}

	public int gethOSChln() {
		return hOSChln;
	}

	public void sethOSChln(int hOSChln) {
		this.hOSChln = hOSChln;
	}

	public int gethOSChlnDaysLast() {
		return hOSChlnDaysLast;
	}

	public void sethOSChlnDaysLast(int hOSChlnMonthLast) {
		this.hOSChlnDaysLast = hOSChlnMonthLast;
	}

	public int gethOSSedr() {
		return hOSSedr;
	}

	public void sethOSSedr(int hOSSedr) {
		this.hOSSedr = hOSSedr;
	}

	public int gethOSSedrDaysLast() {
		return hOSSedrDaysLast;
	}

	public void sethOSSedrDaysLast(int hOSSedrMonthLast) {
		this.hOSSedrDaysLast = hOSSedrMonthLast;
	}

	public String gethOSType() {
		return hOSType;
	}

	public void sethOSType(String hOSType) {
		this.hOSType = hOSType;
	}

	public Date gethOSDate() {
		return hOSDate;
	}

	public void sethOSDate(Date hOSDate) {
		this.hOSDate = hOSDate;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}


}
