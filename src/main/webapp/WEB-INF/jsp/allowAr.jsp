<style>
.button {
    background-color: #26a69a; /* Green */
    border: none;
    color: white;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 14px;
    cursor: pointer;
}


thead, tbody { display: block; font-size: 10px; }
tbody
{
 height: 400px;      
     overflow-y: auto;  
      
  
}



</style>

<div ng-show="operatorLogin || superAdminLogin">
<title>Allow Arrival Report</title>

<div class="row">
<form ng-submit="allowAr(startDate,endDate)" class="col s12 card"
style="align: center; padding-top: 40px;
 background-color: rgba(125, 125, 125, 0.3);">
		<div class="row">
			<div class="input-field col s4">
				<input type="date" id="cnmtCode"  ng-model="startDate"  required>
					<p style="color: red;font-size: 14px;font-style:italic;">{{message}}</p>
				<label>Start Date</label>
			</div>
			<div class="input-field col s4">
				<input class="validate" type ="date"  ng-model="endDate" required>
				<p style="color: red;font-size: 14px;font-style:italic;">{{message2}}</p>
       		 	<label>End Date</label>
			</div>
				<div class="input-field col s4">
				<input class="validate" type ="submit"   value="Submit">
			</div>
			</div>
			</form>
</div>
<div class="row" id="arTable", ng-hide="arTableDBFlag">
<table  id="table">
<thead>
<tr style="background-color: gray;" ><th style="width: 10px;">S. No.</th><th class="th">Bd</th><th class="th">Ch<span> No.</span> </th><th class="th">Challan<span> Date</span></th><th class="th">Lorry<span> No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th><th class="th">Ar<span>     No</span></th><th class="th">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unloading</th><th class="th">Detention</th><th class="th">Over<span> Height</span></th>
<th class="th">Extra <span>Km</span></th><th class="th">Late <span>Ack</span></th><th class="th">Late <span>Dlvry</span></th><th class="th">Wt <span>Shrtg</span></th><th class="th" >Driver Claim</th>
<th class="th">Cnmt<span> Link</span></th><th class="th">Remarks</th><th class="th">Validate</th><th class="th">New <span>Remarks</th><th class="th">Allowed</th><th class="th">Update</th></tr>
</thead>
<tbody>
<tr ng-repeat="object in arData"><td class="font">{{$index+1}}</td><td class="font">{{object[0]}}</td><td class="font">{{object[1]}}</td><td class="font">{{object[4] | date : "dd.MM.y"}}</td><td class="font">{{object[3]}}</td><td class="font">{{object[2]}}</td>
<td class="font" >{{object[5]}}<br><input ng-model="unloadingAmt" type="text"   style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[6]}}<br><input ng-model="detentionAmt" pattern="^\d*$" type="text" style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[8]}}<br><input ng-model="overheightAmt" type="text"  style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[9]}}<br><input ng-model="extraKmAmt" type="text" style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[10]}}<br><input ng-model="lateAckAmt" type="text"   style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[11]}}<br><input ng-model="lateDlvryAmt" type="text" style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[12]}}<br><input  ng-model="wtshrtgAmt" type="text" style="width: 60px; height: 30px; font-size: 10px;"></td>
<td class="font">{{object[13]}}<br><input ng-model="drClaimAmt" type="text" style="width: 60px; height: 30px; font-size: 10px;"></td>
<td ng-click="openCnmt(object[1])" class="font" ><a style="color: #26a69a;">Open Cnmt</a></td><td class="font">{{object[7]}}</td>
<td class="font"><select class="font" style="width: 60px;" ng-model="validate"><option value="Y">Yes</option><option value="N">No</option></select><p style="color: red;">{{message3[$index]}}</p></td>
<td class="font"><input type="text" ng-model="newRmrk"  style="width: 70px; height: 40px; font-size: 10px;" ><p style="color:red">{{message7[$index]}}</p></td>
<td class="font"><select class="font" style="width: 60px;"  ng-model="allowed"><option value="Y">Yes</option><option value="N">No</option></select><p style="color: red;">{{message4[$index]}}</p><p style="color:red">{{message5[$index]}}</p></td>
 <td><button class="button" ng-click="updateAr(newRmrk,validate,allowed,object[2],$index,unloadingAmt,detentionAmt,overheightAmt,extraKmAmt,lateAckAmt,lateDlvryAmt,wtshrtgAmt,drClaimAmt,object)">Update</button></td> 
</tr>
</tbody>
</table>
</div>
</div>

