package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ChqCancelVoucherDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.VoucherService;

public class ChqCancelVoucherDAOImpl implements ChqCancelVoucherDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	public ChqCancelVoucherDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@Override
	@Transactional
	public int saveChqCancelVoucher(VoucherService voucherService) {
		
		int temp = 0;
		
		int voucherNo = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String payMode = "Q";
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			java.sql.Date sqlDate = new java.sql.Date(voucherService.getCashStmtStatus().getCssDt().getTime());
			 
			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
			ProjectionList proList = Projections.projectionList();
			proList.add(Projections.property("csVouchNo"));
			cr.setProjection(proList);				
						
			List<Integer>  voucherNoList = cr.list();			
			
			
			CashStmtStatus css = (CashStmtStatus) session.get(CashStmtStatus.class,voucherService.getCashStmtStatus().getCssId());
			List<CashStmt> csList = new ArrayList<CashStmt>();	
			
			
			csList = css.getCashStmtList();			
			
			System.out.println("List Size - 2 : "+csList.size());
			
			if(!csList.isEmpty()){
				//int vNo = voucherNoList.get(voucherNoList.size() - 1);
				voucherNo = 0;				
				for(int i=0;i<csList.size();i++){					
					if(voucherNo < csList.get(i).getCsVouchNo()){
						voucherNo = csList.get(i).getCsVouchNo();
					}
				}
				voucherNo = voucherNo + 1;
			}else{
				voucherNo = 1;
			}			
			//chq leaves updation
			//update used chq
			
			System.out.println("Cheque Leaves ; ");
			ChequeLeaves usedChqFrDB = (ChequeLeaves) session.get(ChequeLeaves.class, voucherService.getUsedChq().getChqLId());
			usedChqFrDB.setChqLCancel(true);
			usedChqFrDB.setChqLChqCancelDt(sqlDate);
			session.merge(usedChqFrDB);
			
			//get all cs rows on the basis of chq no and faCode		
			
			
			Query query = session.createQuery("from CashStmt where csTvNo LIKE ? and csFaCode = ?");
			query.setString(0, usedChqFrDB.getChqLChqNo()+"%");			
			query.setString(1, usedChqFrDB.getBankMstr().getBnkFaCode());			
									
			CashStmt cashSt = (CashStmt) query.list().get(0);
						
			//get cs on the basis of bCode, csDate and csVouchNo
			cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, cashSt.getbCode()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, cashSt.getCsDt()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_VOUCH_NO, cashSt.getCsVouchNo()));
			
			List<CashStmt> cashStmtList = cr.list();
			
			System.out.println("Final List : "+cashStmtList.size());
			
			//update unused chq (chq replace)
			if (voucherService.getUnUsedChq() != null && voucherService.getUnUsedChq().getChqLId() > 0) {
				
				ChequeLeaves unUsedChqFrDB = (ChequeLeaves) session.get(ChequeLeaves.class, voucherService.getUnUsedChq().getChqLId());
				unUsedChqFrDB.setChqLChqAmt(voucherService.getUnUsedChq().getChqLChqAmt());
				unUsedChqFrDB.setChqLUsedDt(sqlDate);
				unUsedChqFrDB.setChqLUsed(true);
				session.merge(unUsedChqFrDB);
				
				//debit old chq
				CashStmt csOldChq = new CashStmt(); 
				csOldChq.setbCode(currentUser.getUserBranchCode());
				csOldChq.setUserCode(currentUser.getUserCode());
				csOldChq.setCsDescription(voucherService.getDesc()+" csId: "+cashSt.getCsId());
				csOldChq.setCsDrCr('D');
				System.out.println("chq amt: "+usedChqFrDB.getChqLChqAmt());
				csOldChq.setCsAmt(usedChqFrDB.getChqLChqAmt());
				csOldChq.setCsType(voucherService.getVoucherType());
				csOldChq.setCsVouchType("bank");
				csOldChq.setCsChequeType(usedChqFrDB.getChqLCType());
				csOldChq.setCsPayTo(cashSt.getCsPayTo());
				System.out.println("FaCode: "+usedChqFrDB.getBankMstr().getBnkFaCode());
				csOldChq.setCsFaCode(usedChqFrDB.getBankMstr().getBnkFaCode());
				//csOldChq.setCsTvNo(usedChqFrDB.getChqLChqNo()+"000");
				csOldChq.setCsTvNo(usedChqFrDB.getChqLChqNo());
				csOldChq.setCsDt(sqlDate);
				csOldChq.setCsVouchNo(voucherNo);
				csOldChq.setPayMode(payMode);
				
				//credit new chq
				CashStmt csNewChq = new CashStmt(); 
				csNewChq.setbCode(currentUser.getUserBranchCode());
				csNewChq.setUserCode(currentUser.getUserCode());
				csNewChq.setCsDescription(voucherService.getDesc()+" csId: "+cashSt.getCsId());
				csNewChq.setCsDrCr('C');
				csNewChq.setCsAmt(unUsedChqFrDB.getChqLChqAmt());
				csNewChq.setCsType(voucherService.getVoucherType());
				csNewChq.setCsVouchType("bank");
				csNewChq.setCsChequeType(unUsedChqFrDB.getChqLCType());
				csNewChq.setCsPayTo(cashSt.getCsPayTo());
				csNewChq.setCsFaCode(unUsedChqFrDB.getBankMstr().getBnkFaCode());
				//csNewChq.setCsTvNo(unUsedChqFrDB.getChqLChqNo()+"000");
				csNewChq.setCsTvNo(unUsedChqFrDB.getChqLChqNo());
				csNewChq.setCsDt(sqlDate);
				csNewChq.setCsVouchNo(voucherNo);
				csNewChq.setPayMode(payMode);
				
				
				cr = session.createCriteria(CashStmtStatus.class);
				cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,voucherService.getCashStmtStatus().getCssId()));
				cssList = cr.list();
				if(!cssList.isEmpty()){
					CashStmtStatus csStatus = cssList.get(0);
					csOldChq.setCsNo(csStatus);
					csNewChq.setCsNo(csStatus);
					csStatus.getCashStmtList().add(csOldChq);
					csStatus.getCashStmtList().add(csNewChq);
					session.merge(csStatus);
				}
				
			}else {//cancelled chq
				for (CashStmt cashStmt : cashStmtList) {
					CashStmt csReverse = new CashStmt(); 
					csReverse.setbCode(currentUser.getUserBranchCode());
					csReverse.setUserCode(currentUser.getUserCode());
					csReverse.setCsDescription(voucherService.getDesc()+" csId: "+cashStmt.getCsId());
					
					if (String.valueOf(cashStmt.getCsDrCr()).equalsIgnoreCase("D")) {
						csReverse.setCsDrCr('C');
					} else if(String.valueOf(cashStmt.getCsDrCr()).equalsIgnoreCase("C")) {
						csReverse.setCsDrCr('D');
					}
					
					csReverse.setCsAmt(cashStmt.getCsAmt());
					csReverse.setCsType(voucherService.getVoucherType());
					csReverse.setCsVouchType(cashStmt.getCsVouchType());
					csReverse.setCsChequeType(cashStmt.getCsChequeType());
					csReverse.setCsPayTo(cashStmt.getCsPayTo());
					csReverse.setCsFaCode(cashStmt.getCsFaCode());
					csReverse.setCsTvNo(cashStmt.getCsTvNo());
					csReverse.setCsDt(sqlDate);
					csReverse.setCsVouchNo(voucherNo);
					csReverse.setPayMode(payMode);
					
					cr = session.createCriteria(CashStmtStatus.class);
					cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,voucherService.getCashStmtStatus().getCssId()));
					cssList = cr.list();
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						csReverse.setCsNo(csStatus);
						csStatus.getCashStmtList().add(csReverse);
						session.merge(csStatus);
					}
					
					
				}
				
				//update bankMstr (used and unused have same bank)
				BankMstr bank = usedChqFrDB.getBankMstr();
				double balAmt = bank.getBnkBalanceAmt();			
				balAmt = balAmt + usedChqFrDB.getChqLChqAmt();
				bank.setBnkBalanceAmt(balAmt);
				
				session.merge(bank);
			}
			
			transaction.commit();
			session.flush();
			
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		
		return temp;
	}

}
