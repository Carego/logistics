package com.mylogistics.constants;

public class RemarksCNTS {
	
	public static final String REMARK_ID = "rmkId";
	
	public static final String SERIES = "docSer";
	
	public static final String DOCUMENT_NO = "docNo";
	
	public static final String HOLD_DT = "holdDt";
	
	public static final String UNHOLD_DT = "unHoldDt";
	
	public static final String REMARK = "remark";
	
	public static final String IS_ON_HOLD = "onHold";

}
