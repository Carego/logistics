'use strict';

var app = angular.module('application');

app.controller('BrChqReceiveCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("BrChqReceiveCntlr Started");
	
	$scope.bankMstrList = [];
	$scope.branchName = '';
	$scope.bankMstrDBFlag = true;
	$scope.bankMstr = {};
	$scope.issuedChqBkList = [];
	$scope.issuedChqBk = {};
	$scope.issuedChqBkFlag = false;
	
	$scope.getUsrBrBMstrList = function(){
		console.log("getUsrBrBMstrList Entered");
		var response = $http.post($scope.projectName+'/getUsrBrBMstrList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getUsrBrBMstrList Success");
				console.log("Branch Name: "+data.branchName);
				$scope.branchName = data.branchName;
				$scope.bankMstrList = data.bankMstrList;
			}else {
				$scope.alertToast("PLEASE ASSIGN BANK TO THIS BRANCH");
			}
			
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankMstrDBFlag = false;
		
		$('div#bankMstrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankMstrDBFlag = true;
			}
		});

		$('div#bankMstrDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr){
		console.log("enter into branchName----->"+bankMstr.bnkName);
		console.log("enter into branchFaCode----->"+bankMstr.bnkFaCode);
		$scope.bankMstr = bankMstr;
		$('div#bankMstrDB').dialog('close');
		
		//List of issued cheque book
		var response = $http.post($scope.projectName+'/getBrBankIssuedChqBkList', $scope.bankMstr);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getIssuedChqBkList Success");
				$scope.issuedChqBkList = data.issuedChqBkList;
				$scope.issuedChqBkFlag = true;
				for ( var i = 0; i < $scope.issuedChqBkList.length; i++) {
					console.log("from chq no: "+$scope.issuedChqBkList[i].chqBkFromChqNo);
					console.log("to chq no: "+$scope.issuedChqBkList[i].chqBkToChqNo);
				}
			}else{
				$scope.alertToast("NO CHEQUE ISSUED TO THIS BRANCH");
				$scope.issuedChqBkFlag = false;
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	//submit issued cheque book
	$scope.submitIssuedChqBk =  function(){
		console.log("enter into branchName----->submitIssuedChqBk");
		
		var response = $http.post($scope.projectName+'/rcvdChqBkByBranch', $scope.issuedChqBkList);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.alertToast("Cheque book received successfully");
				$scope.alertToast("Cheque leaves assigned to branch successfully");
				
				$scope.bankMstrDBFlag = true;	
				$scope.bankMstr = {};
				$scope.issuedChqBkList = [];
				$scope.issuedChqBk = {};
				$scope.issuedChqBkFlag = false;
				
			}else{
				$scope.alertToast("error");
				$scope.bankMstrDBFlag = true;	
				$scope.bankMstr = {};
				$scope.issuedChqBkList = [];
				$scope.issuedChqBk = {};
				$scope.issuedChqBkFlag = false;
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getUsrBrBMstrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("BrChqReceiveCntlr Ended");
}]);