<div ng-show="operatorLogin || superAdminLogin">
<title>View ATM</title>
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form ng-submit="submitAtmForm()" class="col s6 card" style="align: center; margin-top: 10px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="input-field col s6">
				<input type="text" name="atmCardNoName" ng-model="atmCardNo" required />
   				<label>Enter ATM No</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
		</form>
	<div class="col s3"> &nbsp; </div>
	
	<div ng-show="tableFlag">
	
		<div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		
		<div class="row">
			<div class="col s3 input-field">
				<input class="validate" type="text" name="bnkName" ng-model="atmCardMstr.bankMstr.bnkName" readonly>
				<label for="code">Bank</label>
			</div>

			<div class="col s3 input-field">
				<input class="validate" type="text" name="branchName" ng-model="atmCardMstr.branchName" readonly> 
				<label for="code">Branch</label>
			</div>
			
			<div class="col s3 input-field">
				<input class="validate" type="text" name="employeeName" ng-model="atmCardMstr.empName" readonly> 
				<label for="code">Employee</label>
			</div>
			
			<div class="col s3 input-field">
				<input class="validate" type="date" name="atmCardExpireDtName" ng-model="atmCardMstr.atmCardExpireDt" readonly>
				<label for="code">Expire Date</label>
			</div>
			
		</div>
		
		 <div class="row" ng-show="atmCardMstr.atmActive">
		 	<div class="col s12 center">
		 		<input type="button" value="Deactivate ATM Card" ng-click="deactiveAtmCard()">
		 	</div>
   		 </div>
   		 
   		 <div class="row" ng-hide="atmCardMstr.atmActive">
		 	<div class="col s12 center">
		 		<input type="submit" value="Activate ATM Card" ng-click="activeAtmCard()">
		 	</div>
   		 </div>
		
	</div>
	
	<div class="col s3 hide-on-med-and-down"> &nbsp;</div>
	
	
		<form name="brForm" ng-submit="transAtmToBr(brForm)" class="col s6 card" style="align: center; margin-top: 10px; padding-top:20px;  background-color: rgba(125, 125, 125, 0.3);">
			<div style="margin-bottom: 30px; font-size: 16px; font-weight: bold; font-family: arial; text-align: center; color: #26A69A;">Transfer ATM Card to Branch</div>
			<div class="input-field col s6">
				<input type="text" name="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" required readonly/>
   				<label>Enter Branch</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
		</form>
	
	</div>
	
</div>

<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>

</div>	

