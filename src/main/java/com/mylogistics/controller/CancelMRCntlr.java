package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CancelMRCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	
	@RequestMapping(value = "/gerMrCancelInfo", method = RequestMethod.POST)
	public @ResponseBody Object gerMrCancelInfo() {
		System.out.println("enter into gerMrCancelInfo function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String, Object>> brhList = branchDAO.getActiveBrNCI();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/cancelMrSubmit", method = RequestMethod.POST)
	public @ResponseBody Object cancelMrSubmit(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into cancelMrSubmit function");
		Map<String,Object> map = new HashMap<>();
		int brhId = Integer.parseInt(String.valueOf(clientMap.get("brhId")));
		String mrType = String.valueOf(clientMap.get("mrType"));
		String mrNo = String.valueOf(clientMap.get("mrNo"));
		if(brhId > 0 && mrType.equalsIgnoreCase("O")) {
			map = moneyReceiptDAO.cancelOnAcc(brhId,mrNo);
			return map;
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
}
