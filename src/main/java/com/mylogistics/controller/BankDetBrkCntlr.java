package com.mylogistics.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Access;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;


@Controller
public class BankDetBrkCntlr {

	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private SessionFactory sessionFactory;

	private Blob blob=null;

	private String brokerBnkImgPath = "/var/www/html/Erp_Image/Broker/Bnk";

	@RequestMapping(value = "/getBrkCodeByIsBnk", method = RequestMethod.POST)
	public @ResponseBody Object getBrkCodeByIsBnk() {
		System.out.println("Enter into getBrkCodeByIsBnk--->>>>>");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, String>> brokerCodeList =  new ArrayList<>();

		brokerCodeList = brokerDAO.getBrkCodesIsBnkD();
		if(!brokerCodeList.isEmpty()){


			map.put("brokerCodeList",brokerCodeList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);

		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	@RequestMapping(value = "/updateBrkBnkD", method = RequestMethod.POST)
	public @ResponseBody Object updateBrkBnkD(@RequestBody Broker brokerBnk) {
		System.out.println("Enter into updateBrkBnkD--->>>>>" +brokerBnk);
		User user=(User) httpSession.getAttribute("currentUser");
		System.out.println("user.getUserBranchCode()="+user.getUserBranchCode());
		Map<String, Object> map = new HashMap<String, Object>();
		List<Broker> brokerList = brokerDAO.getBrokerList(brokerBnk.getBrkCode());
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try{
		
		if(!brokerList.isEmpty()){
			Broker broker = brokerList.get(0);  
			
			if(blob!=null) {
				
					Path path = Paths.get(brokerBnkImgPath);
					if(! Files.exists(path))
						Files.createDirectories(path);//make new directory if not exist
					
					
					if(broker.getBrkImgId()==null) {
						BrokerImg brkImg=new BrokerImg();
						brkImg.setbCode(user.getUserBranchCode());
						brkImg.setBrkId(broker.getBrkId());
						brkImg.setUserCode(user.getUserCode());
					
						brkImg.setBrkBnkDetImgPath(brokerBnkImgPath+"/"+"brk"+broker.getBrkId()+".pdf");
						byte brokerBnkImgInBytes[] = blob.getBytes(1, (int)blob.length());
						File bnkFile = new File(brkImg.getBrkBnkDetImgPath());
						if(bnkFile.exists())
							bnkFile.delete();
						if(bnkFile.createNewFile()){
							OutputStream targetFile=  new FileOutputStream(bnkFile);
				            targetFile.write(brokerBnkImgInBytes);
				            targetFile.close();
						}
						
						int brkImgId=brokerDAO.saveBrkImg(session, brkImg);
						System.out.println("brkImgId="+brkImgId);
						broker.setBrkImgId(brkImgId);
					
						
					}else if(broker.getBrkImgId()<=0) {
						BrokerImg brkImg=new BrokerImg();
						brkImg.setbCode(user.getUserBranchCode());
						brkImg.setBrkId(broker.getBrkId());
						brkImg.setUserCode(user.getUserCode());
					
						brkImg.setBrkBnkDetImgPath(brokerBnkImgPath+"/"+"brk"+broker.getBrkId()+".pdf");
						byte brokerBnkImgInBytes[] = blob.getBytes(1, (int)blob.length());
						File bnkFile = new File(brkImg.getBrkBnkDetImgPath());
						if(bnkFile.exists())
							bnkFile.delete();
						if(bnkFile.createNewFile()){
							OutputStream targetFile=  new FileOutputStream(bnkFile);
				            targetFile.write(brokerBnkImgInBytes);
				            targetFile.close();
						}
						int brkImgId=brokerDAO.saveBrkImg(session, brkImg);
						System.out.println("brkImgId="+brkImgId);
						broker.setBrkImgId(brkImgId);
					}else {
						BrokerImg brkImg=(BrokerImg) session.get(BrokerImg.class, broker.getBrkImgId());
						brkImg.setbCode(user.getUserBranchCode());
						brkImg.setBrkId(broker.getBrkId());
						brkImg.setUserCode(user.getUserCode());
					
						brkImg.setBrkBnkDetImgPath(brokerBnkImgPath+"/"+"brk"+broker.getBrkId()+".pdf");
						byte brokerBnkImgInBytes[] = blob.getBytes(1, (int)blob.length());
						File bnkFile = new File(brkImg.getBrkBnkDetImgPath());
						if(bnkFile.exists())
							bnkFile.delete();
						if(bnkFile.createNewFile()){
							OutputStream targetFile=  new FileOutputStream(bnkFile);
				            targetFile.write(brokerBnkImgInBytes);
				            targetFile.close();
						}
						
						brokerDAO.updateBrkImg(session,brkImg);
					}
					
				
			}
			
			
			broker.setBrkAccntNo(brokerBnk.getBrkAccntNo());
			broker.setBrkIfsc(brokerBnk.getBrkIfsc());
			broker.setBrkMicr(brokerBnk.getBrkMicr());
			broker.setBrkBnkBranch(brokerBnk.getBrkBnkBranch());
			broker.setBrkAcntHldrName(brokerBnk.getBrkAcntHldrName());
			broker.setBrkBnkDet(true);
			brokerDAO.updateBroker(session,broker);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		session.flush();
		session.clear();
		transaction.commit();
		
		}catch(Exception e){
			System.out.println("Exception : "+e);
			transaction.rollback();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}finally {
			blob=null;
			session.close();
		}

		return map;
	}

	
	@RequestMapping(value = "/upldBrkChqImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldBrkChqImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		blob = null;
		try{
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	

}
