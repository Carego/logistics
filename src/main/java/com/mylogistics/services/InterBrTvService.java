package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class InterBrTvService {

	private VoucherService voucherService;
	
	private List<InterBrTvVoucher> ibtList = new ArrayList<InterBrTvVoucher>();

	public VoucherService getVoucherService() {
		return voucherService;
	}

	public void setVoucherService(VoucherService voucherService) {
		this.voucherService = voucherService;
	}

	public List<InterBrTvVoucher> getIbtList() {
		return ibtList;
	}

	public void setIbtList(List<InterBrTvVoucher> ibtList) {
		this.ibtList = ibtList;
	}
	
	
}
