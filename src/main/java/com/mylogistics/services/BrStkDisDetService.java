package com.mylogistics.services;

import java.util.List;

import com.mylogistics.model.BranchStockDispatch;

public class BrStkDisDetService {
	
	private List<Long> cnmtList;
	private List<Long> chlnList;
	private List<Long> sedrList;
	private BranchStockDispatch brStkDis;
	private int id;
	private String operatorCode;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Long> getCnmtList() {
		return cnmtList;
	}
	public void setCnmtList(List<Long> cnmtList) {
		this.cnmtList = cnmtList;
	}
	public List<Long> getChlnList() {
		return chlnList;
	}
	public void setChlnList(List<Long> chlnList) {
		this.chlnList = chlnList;
	}
	public List<Long> getSedrList() {
		return sedrList;
	}
	public void setSedrList(List<Long> sedrList) {
		this.sedrList = sedrList;
	}
	public BranchStockDispatch getBrStkDis() {
		return brStkDis;
	}
	public void setBrStkDis(BranchStockDispatch brStkDis) {
		this.brStkDis = brStkDis;
	}
	public String getOperatorCode() {
		return operatorCode;
	}
	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}
	
}
