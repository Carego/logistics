package com.mylogistics.services;

import java.util.List;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Customer;

public class MultipleLists {


	private List<Branch> brnch;
	private List<Customer> customer;
	private List<String> customerCode;
	private List<String> branchCode;
	private List<Object[]> custcontract;
	public List<Branch> getBrnch() {
		return brnch;
	}
	public void setBrnch(List<Branch> brnch) {
		this.brnch = brnch;
	}
	public List<Customer> getCustomer() {
		return customer;
	}
	public void setCustomer(List<Customer> customer) {
		this.customer = customer;
	}
	public List<String> getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(List<String> customerCode) {
		this.customerCode = customerCode;
	}
	public List<String> getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(List<String> branchCode) {
		this.branchCode = branchCode;
	}
	public List<Object[]> getCustcontract() {
		return custcontract;
	}
	public void setCustcontract(List<Object[]> custcontract) {
		this.custcontract = custcontract;
	}
	
}
