package com.mylogistics.model.webservice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cnmt_challan_app")
public class CnmtChlnApp {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cnmtChlnId")
	private Integer cnmtChlnId;
	@Column(name = "cnmtCode")
	private String cnmtCode;
	@Column(name = "chlnCode")
	private String chlnCode;
	@Column(name = "cnmtNoOfPkg")
	private String cnmtNoOfPkg;
	@Column(name = "actualWt")
	private String actualWt;
	
	public Integer getCnmtChlnId() {
		return cnmtChlnId;
	}
	public void setCnmtChlnId(Integer cnmtChlnId) {
		this.cnmtChlnId = cnmtChlnId;
	}
	public String getCnmtCode() {
		return cnmtCode;
	}
	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}
	public String getChlnCode() {
		return chlnCode;
	}
	public void setChlnCode(String chlnCode) {
		this.chlnCode = chlnCode;
	}
	public String getCnmtNoOfPkg() {
		return cnmtNoOfPkg;
	}
	public void setCnmtNoOfPkg(String cnmtNoOfPkg) {
		this.cnmtNoOfPkg = cnmtNoOfPkg;
	}
	public String getActualWt() {
		return actualWt;
	}
	public void setActualWt(String actualWt) {
		this.actualWt = actualWt;
	}
	
}