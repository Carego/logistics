package com.mylogistics.services;


public class BFService {
	
	private int bfNo;
	
	private String billNumber[];

	public int getBfNo() {
		return bfNo;
	}

	public void setBfNo(int bfNo) {
		this.bfNo = bfNo;
	}

	public String[] getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String[] billNumber) {
		this.billNumber = billNumber;
	}

	
	
}
