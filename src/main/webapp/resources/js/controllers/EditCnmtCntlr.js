'use strict';

var app = angular.module('application');

app.controller('EditCnmtCntlr',['$scope','$location','$http','$window','FileUploadService','$filter', '$log', 
                            function($scope,$location,$http,$window,FileUploadService,$filter,$log){
	
	console.log("EditCnmtStarted");

	$scope.cnmtList = [];
	$scope.cnmt = {};
	
	$scope.whShowFlag=false;
	$scope.whRequreFlag=false;
	
	$scope.branchList = [];
	$scope.branch = {};
	$scope.payAt = {};
	$scope.billAt = {};
	
	$scope.stnList = [];
	$scope.frmStn = {};
	$scope.toStn = {};
	
	$scope.vehTypeList = [];
	$scope.productTypeList = [];
	
	$scope.custList = [];
	$scope.cust = {};
	$scope.cngnor = {};
	$scope.cngnee = {};
	
	$scope.empList = [];
	$scope.emp = {};
	
	$scope.contList = [];
	$scope.cont = {};
	
	$scope.rateList = [];
	
	$scope.cnmt.cnmtInvoiceNo = [];
	
	$scope.cnmtDBFlag = true;
	$scope.branchDBFlag = true;
	$scope.custDBFlag = true;
	$scope.frmStnDBFlag = true;
	$scope.toStnDBFlag = true;
	$scope.contDBFlag = true;
	$scope.rateDBFlag = true;
	$scope.cngnorDBFlag = true;
	$scope.cngneeDBFlag = true;
	$scope.vehicleTypeDBFlag = true;
	$scope.productTypeDBFlag = true;
	$scope.payAtDBFlag = true;
	$scope.billAtDBFlag = true;
	$scope.empDBFlag = true;
	$scope.saveInvoiceNoFlag = true;
	
	$scope.showCnmtFlag = false;
	
	var cnmtImgSize = 0;
	
	$scope.cnmtImageFlag = false;
	
	/*$scope.getCnmtList = function(){
		console.log(" entered into getCnmtList------>");
		var response = $http.post($scope.projectName+'/getCnmtList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.cnmtList = data.list;
				$scope.getStnList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}*/
	
	/*$scope.openCnmtDB = function(){
		console.log("Entered into openCnmtDB");
		$scope.cnmtDBFlag = false;
		$('div#cnmtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Cnmt No",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.cnmtDBFlag = true;
			}
		});

		$('div#cnmtDB').dialog('open');
	}*/
	
	/*$scope.saveCnmtCode = function(cnmtCode){
		$scope.cnmt.cnmtCode = cnmtCode;
		$('div#cnmtDB').dialog('close');
	}*/
	
	//this method get the result from database on subbmission
	$scope.getCnmt = function() {
		console.log("getCnmt()");
		var response = $http.post($scope.projectName+'/getCnmtForEdit', $scope.cnmt.cnmtCode);
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				$log.info(data);
				$scope.cnmt = data.cnmt;
				$scope.cnmtImgId = data.cnmtImgId;
				//$scope.cont = data.cont;
				$scope.contList = data.contList;
				
				$scope.getCont();
				
				$scope.branch = data.branch;
				$scope.cust = data.cust;
				$scope.frmStn = data.frmStn;
				$scope.toStn = data.toStn;
				$scope.cngnor = data.cngnor;
				$scope.cngnee = data.cngnee;
				$scope.payAt = data.payAt;
				$scope.billAt = data.billAt;
				$scope.emp = data.emp;
				
				console.log("cnmtImgId: "+$scope.cnmtImgId)
				if(angular.isUndefined($scope.cnmtImgId) || $scope.cnmtImgId === null || $scope.cnmtImgId === ""){
					$scope.cnmtImageFlag = false;
				} else {
					$scope.cnmtImageFlag = true;
				}
				
				$scope.showCnmtFlag = true;
				//console.log("cnmt data: "+$scope.cnmt.branchCode);
				
				/*$scope.getContractList($scope.cnmt.custCode);*/
				
				
				/*$scope.getBranch();*/
				/*$scope.getCust();*/
				/*$scope.getFrmStn();*/
				/*$scope.getToStn();*/
				/*$scope.getCngnor();*/
				/*$scope.getCngnee();*/
				/*$scope.getPayAt();*/
				/*$scope.getBillAt();*/
				/*$scope.getEmp();*/
				
			}else {
				console.log(data.result);
				$scope.alertToast("Please enter valid CNMT No");
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in response: "+data);
			$scope.alertToast("Database Exception");
		});
	}
	
	/*$scope.getBranch = function() {
		console.log("getBranch()");
		for ( var i = 0; i < $scope.branchList.length; i++) {
			if ($scope.branchList[i].branchId === parseInt($scope.cnmt.branchCode)) {
				console.log("entered If: "+$scope.branchList[i].branchName);
				$scope.branch = $scope.branchList[i];
				
			}
		}
	}*/
	
	/*$scope.getCust = function() {
		console.log("getCust()")
		for ( var i = 0; i < $scope.custList.length; i++) {
			if ($scope.custList[i].custId === parseInt($scope.cnmt.custCode)) {
				$scope.cust = $scope.custList[i];
			}
		}
	}*/
	
	/*$scope.getFrmStn = function() {
		console.log("getFrmStn()");
		for ( var i = 0; i < $scope.stnList.length; i++) {
			if ($scope.stnList[i].stnId === parseInt($scope.cnmt.cnmtFromSt)) {
				$scope.frmStn = $scope.stnList[i];
			}
		}
	}*/
	
	/*$scope.getToStn = function() {
		console.log("toStn()");
		for ( var i = 0; i < $scope.stnList.length; i++) {
			if ($scope.stnList[i].stnId === parseInt($scope.cnmt.cnmtToSt)) {
				$scope.toStn = $scope.stnList[i];
			}
		}
	}*/
	
	$scope.getCont = function() {
		console.log("getCont()");
		for ( var i = 0; i < $scope.contList.length; i++) {
			if ($scope.contList[i].contCode === $scope.cnmt.contractCode) {
				$scope.cont = $scope.contList[i];
			}
		}
	}
	
	/*$scope.getCngnor = function() {
		console.log("getCngnor()");
		for ( var i = 0; i < $scope.custList.length; i++) {
			if ($scope.custList[i].custId === parseInt($scope.cnmt.cnmtConsignor)) {
				$scope.cngnor = $scope.custList[i];
			}
		}
	}*/
	
	/*$scope.getCngnee = function() {
		console.log("getCngnee");
		for ( var i = 0; i < $scope.custList.length; i++) {
			if ($scope.custList[i].custId === parseInt($scope.cnmt.cnmtConsignee)) {
				$scope.cngnee = $scope.custList[i];
			}
		}
	}*/
	
	/*$scope.getPayAt = function() {
		console.log("getPayAt()");
		for ( var i = 0; i < $scope.branchList.length; i++) {
			if ($scope.branchList[i].branchId === parseInt($scope.cnmt.cnmtPayAt)) {
				$scope.payAt = $scope.branchList[i];
			}
		}
	}*/
	
	/*$scope.getBillAt = function() {
		console.log("getBillAt()");
		for ( var i = 0; i < $scope.branchList.length; i++) {
			if ($scope.branchList[i].branchId === parseInt($scope.cnmt.cnmtBillAt)) {
				$scope.billAt = $scope.branchList[i];
			}
		}
	}*/
	
	/*$scope.getEmp = function() {
		console.log("getEmp()");
		for ( var i = 0; i < $scope.empList.length; i++) {
			if ($scope.empList[i].empId === parseInt($scope.cnmt.cnmtEmpCode)) {
				$scope.emp = $scope.empList[i];
			}
		}
	}*/
	
	$scope.getStnList = function() {
		console.log("getStnList()");
		var response = $http.post($scope.projectName+'/getStnList');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("stnList result: "+data.result);
				$scope.stnList = data.stnList;
				$scope.getVehTypeList();
			} else {
				console.log("stnList result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in stnList response: "+data);
		});
	}
	
	$scope.getVehTypeList = function() {
		console.log("getVehTypeList()");
		var response = $http.post($scope.projectName+'/getVehTypeList');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("vehicleTypeList result: "+data.result);
				$scope.vehTypeList = data.vehTypeList;
				$scope.getProductTypeList();
			} else {
				console.log("vehicleTypeList result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in vehTypeList response: "+data);
		})
	};
	
	$scope.getProductTypeList = function() {
		console.log("getProductTypeList()");
		var response = $http.post($scope.projectName+'/getProductTypeList');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("productTypeList result: "+data.result);
				$scope.productTypeList = data.productTypeList;
				$scope.getBranchList();
			} else {
				console.log("productTypeList result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in productTypeList response: "+data)
		})
	};
	
	$scope.getBranchList = function() {
		console.log("getBranch()");
		var response = $http.post($scope.projectName+'/getActiveBrNCI');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("branch List result: "+data.result);
				$scope.branchList = data.branchNCIList;
				$scope.getCustList();
			} else {
				console.log("branch List result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Erro in branchlist response: "+data);
		});
	}
	
	$scope.getCustList = function() {
		console.log("getCustList()");
		var response = $http.post($scope.projectName+'/getCustList');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getCustList result: "+data.result);
				$scope.custList = data.custList;
				$scope.getEmpList();
			} else {
				console.log("getCustList result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in custList response: "+data);
		});
	}
	
	//only client code
	$scope.getEmpList = function() {
		console.log("getEmpList()");
		var res = $http.post($scope.projectName+'/getEmpList');
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("empList result:"+data.result);
				$scope.empList = data.empList;
			} else {
				console.log("empList result:"+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in empList response"+data);
		});
	}
	
	$scope.getContractList = function(custId) {
		console.log("getContList()");
		var res = $http.post($scope.projectName+'/getContractList', custId);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getContractList result: "+data.result);
				$scope.contList = data.contList;
				$scope.getCont();
			} else {
				console.log("getContractList result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in contractList response: "+data);
		});
	}
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.openCustDB = function() {
		console.log("openCustDB()");
		$scope.custDBFlag = false;
		$('div#custDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custDBFlag = true;
			}
		});

		$('div#custDB').dialog('open');
	}
	
	$scope.saveCust = function(cust){
		console.log("saveCust()");
		$scope.cust = cust;
		$scope.cont = {};
		$scope.contList = [];
		//update contract list
		$scope.getContractList(cust.custId);
		$('div#custDB').dialog('close');
	}
	
	$scope.openFrmStnDB = function() {
		console.log("openFrmStnDB()");
		$scope.frmStnDBFlag = false;
		$('div#frmStnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.frmStnDBFlag = true;
			}
		});

		$('div#frmStnDB').dialog('open');
	}
	
	$scope.saveFrmStn = function(frmStn){
		console.log("saveFrnStn()");
		$scope.frmStn = frmStn;
		$('div#frmStnDB').dialog('close');
	}
	
	$scope.openToStnDB = function() {
		console.log("openToStnDB()");
		$scope.toStnDBFlag = false;
		$('div#toStnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.toStnDBFlag = true;
			}
		});

		$('div#toStnDB').dialog('open');
	}
	
	$scope.saveToStn = function(toStn){
		console.log("saveToStn()");
		$scope.toStn = toStn;
		$('div#toStnDB').dialog('close');
	}
	
	$scope.openContDB = function() {
		console.log("openContDB()");
		$scope.contDBFlag = false;
		$('div#contDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Contract",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.contDBFlag = true;
			}
		});

		$('div#contDB').dialog('open');
	}
	
	$scope.saveCont = function(cont){
		console.log("saveCont()");
		$scope.cont = cont;
		$('div#contDB').dialog('close');
	}
	
	$scope.getRate = function(cont, index) {
		console.log("getRate()");
		var res = $http.post($scope.projectName+'/getRate', cont.contCode);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getRate result: "+data.result);
				$scope.rateList = data.rateList;
				
				//open rateListDB
				$scope.rateDBFlag = false;
				$('div#rateDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Rate: "+cont.contCode,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.rateDBFlag = true;
					}
				});

				$('div#rateDB').dialog('open');
				
			} else {
				console.log("getRate result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in getRate response: "+data);
		});
	}
	
	$scope.openCngnorDB = function() {
		console.log("openCngnorDB()");
		$scope.cngnorDBFlag = false;
		$('div#cngnorDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Consignor",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.cngnorDBFlag = true;
			}
		});

		$('div#cngnorDB').dialog('open');
	}
	
	$scope.saveCngnor = function(cngnor){
		console.log("saveCngnor()");
		$scope.cngnor = cngnor;
		$('div#cngnorDB').dialog('close');
	}

	$scope.openCngneeDB = function() {
		console.log("openCngneeDB()");
		$scope.cngneeDBFlag = false;
		$('div#cngneeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Consignee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.cngneeDBFlag = true;
			}
		});

		$('div#cngneeDB').dialog('open');
	}
	
	$scope.saveCngnee = function(cngnee){
		console.log("saveCngnee()");
		$scope.cngnee = cngnee;
		if($scope.cngnee.custId===1754){
			$scope.cnmt.prBlCode="1754";
			$scope.cnmt.relType="P";
			$scope.getRelianceWhHouse();
			$scope.whRequreFlag=true;
		}
		if($scope.cngnee.custId===1755){
			$scope.cnmt.prBlCode="1755";
			$scope.cnmt.relType="S";
			$scope.whRequreFlag=false;
		}
		if($scope.cngnee.custId===1756){
			$scope.cnmt.prBlCode="1756";
			$scope.cnmt.relType="O";
			$scope.whRequreFlag=false;
		}
			
		$('div#cngneeDB').dialog('close');
	}
	
	$scope.openVehicleTypeDB = function() {
		console.log("openVehicleTypeDB");
		$scope.vehicleTypeDBFlag = false;
		$('div#vehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.vehicleTypeDBFlag = true;
			}
		});

		$('div#vehicleTypeDB').dialog('open');
	}
	
	$scope.saveVehicleType = function(vehicleType){
		console.log("saveVehicleType()");
		$scope.cnmt.cnmtVehicleType = vehicleType.vtCode;
		$('div#vehicleTypeDB').dialog('close');
	}
	
	$scope.openProductTypeDB = function() {
		console.log("openProductTypeDB()");
		$scope.productTypeDBFlag = false;
		$('div#productTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Product Type",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.productTypeDBFlag = true;
			}
		});

		$('div#productTypeDB').dialog('open');
	}
	
	$scope.saveProductType = function(productType) {
		console.log("saveProductType()");
		$scope.cnmt.cnmtProductType  = productType;
		$('div#productTypeDB').dialog('close');
	}
	
	$scope.calFreight = function() {
		console.log("calFreight()");
		$scope.cnmt.cnmtFreight = parseFloat(parseFloat($scope.cnmt.cnmtRate*$scope.cnmt.cnmtGuaranteeWt).toFixed(2));
		console.log("freight: "+$scope.cnmt.cnmtFreight);
	}
	
	$scope.openPayAtDB = function() {
		console.log("openPayAtDB()");
		$scope.payAtDBFlag = false;
		$('div#payAtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Pay At",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.payAtDBFlag = true;
			}
		});

		$('div#payAtDB').dialog('open');
	}
	
	$scope.savePayAt = function(payAt) {
		console.log("savePayAt()");
		$scope.payAt  = payAt;
		$('div#payAtDB').dialog('close');
	}
	
	$scope.openBillAtDB = function() {
		console.log("openBillAtDB()");
		$scope.billAtDBFlag = false;
		$('div#billAtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bill At",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.billAtDBFlag = true;
			}
		});

		$('div#billAtDB').dialog('open');
	}
	
	$scope.saveBillAt = function(billAt) {
		console.log("saveBillAt()");
		$scope.billAt  = billAt;
		$('div#billAtDB').dialog('close');
	}
	
	$scope.openEmpDB = function() {
		console.log("openBillAtDB()");
		$scope.empDBFlag = false;
		$('div#empDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.empDBFlag = true;
			}
		});

		$('div#empDB').dialog('open');
	}
	
	$scope.saveEmp = function(emp) {
		console.log("saveEmpAt()");
		$scope.emp  = emp;
		$('div#empDB').dialog('close');
	}
	
	$scope.openInvoiceNo = function(){
		console.log("Enter into openInvoiceNo");
		$scope.saveInvoiceNoFlag = false;
		$scope.invoiceNo = "";
		$('div#saveInvoiceNo').dialog({
			autoOpen: false,
			modal:true,
			title: "Save Invoice No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});
		console.log("Line no 342");
		$('div#saveInvoiceNo').dialog('open');	
	}
	
	$scope.saveInvoiceNum = function(saveInvoiceNoForm,invoiceNo,invoiceDt){
		console.log("enter into saveInvoiceNum function");
		if(saveInvoiceNoForm.$invalid){
			if(saveInvoiceNoForm.invoiceNo.$invalid){
				$scope.alertToast("Invoice number should be between 3-10 digits");
			}else if(saveInvoiceNoForm.invoiceDt.$invalid){
				$scope.alertToast("Please enter invoice date..");
			}
		}else{
			$scope.saveInvoiceNoFlag=true;
			$('div#saveInvoiceNo').dialog('close')
			if($.inArray(invoiceNo , $scope.cnmt.cnmtInvoiceNo) !== -1){
				$scope.alertToast("Invoice Number already present");
				$scope.invoiceNo = "";
			}else {
				var invList = {
					"invoiceNo" : invoiceNo,
					"date"	  : invoiceDt
				}
				if($scope.cnmt.cnmtInvoiceNo===null){
					$scope.cnmt.cnmtInvoiceNo=[];
					$scope.cnmt.cnmtInvoiceNo.push(invList);
				}else{
					$scope.cnmt.cnmtInvoiceNo.push(invList);
				}
				$scope.invoiceDt = "";
			}
		}						
	}
	
	$scope.removeCnmtInv = function(index){
		console.log("enter into removeCnmtInv function");
		$scope.cnmt.cnmtInvoiceNo.splice(index,1);
	}
	
	$scope.submitCnmt = function(cnmtForm,cnmt) {
		console.log("submitCnmt()");
		if (cnmtForm.$invalid) {
			console.log("invalidate: "+cnmtForm.$invalid);
			if (cnmtForm.branchName.$invalid) {
				$scope.alertToast("please enter valid branchName");
			} else if(cnmtForm.custName.$invalid){
				$scope.alertToast("please enter valid custName");
			} else if (cnmtForm.cnmtVOGName.$invalid) {
				$scope.alertToast("please enter valid value of goods");
			} else if (cnmtForm.cnmtDtName.$invalid) {
				$scope.alertToast("please enter valid Date");
			} else if (cnmtForm.frmStnName.$invalid) {
				$scope.alertToast("please enter valid From Station");
			} else if (cnmtForm.toStnName.$invalid) {
				$scope.alertToast("please enter valid To Station");
			} else if (cnmtForm.contCodeName.$invalid) {
				$scope.alertToast("please enter valid Contract Code");
			} else if (cnmtForm.cngnorName.$invalid) {
				$scope.alertToast("please enter valid Consignor");
			} else if (cnmtForm.cngneeName.$invalid) {
				$scope.alertToast("please enter valid Consignee");
			} else if (cnmtForm.vehicleName.$invalid) {
				$scope.alertToast("please enter valid Vehicle");
			} else if (cnmtForm.cnmtProductTypeName.$invalid) {
				$scope.alertToast("please enter valid Product Type");
			} else if (cnmtForm.cnmtActualWtName.$invalid) {
				$scope.alertToast("please enter valid Actual Weight");
			} else if (cnmtForm.cnmtGuaranteeWtName.$invalid) {
				$scope.alertToast("please enter valid Gurantee Weight");
			} else if (cnmtForm.cnmtKmName.$invalid) {
				$scope.alertToast("please enter valid Km");
			} else if (cnmtForm.cnmtStateName.$invalid) {
				$scope.alertToast("please enter valid State");
			} else if (cnmtForm.cnmtDDLName.$invalid) {
				$scope.alertToast("please enter valid DDL");
			} else if (cnmtForm.cnmtPayAtName.$invalid) {
				$scope.alertToast("please enter valid Pay On");
			} else if (cnmtForm.cnmtBillAtName.$invalid) {
				$scope.alertToast("please enter valid Bill On");
			} else if (cnmtForm.cnmtRateName.$invalid) {
				$scope.alertToast("please enter valid Rate");
			} else if (cnmtForm.cnmtFreightName.$invalid) {
				$scope.alertToast("please enter valid Freight");
			} else if (cnmtForm.cnmtTOTName.$invalid) {
				$scope.alertToast("please enter valid TOT");
			} else if (cnmtForm.cnmtDCName.$invalid) {
				$scope.alertToast("please enter valid DC");
			} else if (cnmtForm.cnmtNoOfPkgName.$invalid) {
				$scope.alertToast("please enter valid No Of Package");
			} else if (cnmtForm.cnmtCostGradeName.$invalid) {
				$scope.alertToast("please enter valid Cost Grade");
			} else if (cnmtForm.empName.$invalid) {
				$scope.alertToast("please enter valid Employee");
			} else if (cnmtForm.cnmtExtraExpName.$invalid) {
				$scope.alertToast("please enter valid Extra Expense");
			} else if (cnmtForm.cnmtDtOfDlyName.$invalid) {
				$scope.alertToast("please enter valid Date Of Delivery");
			} else if (cnmtForm.whCodeName.$invalid) {
				$scope.alertToast("please enter WhareHouse");
			}
		} else {
			console.log("invalidate: "+cnmtForm.$invalid);
			
			console.log($scope.payAt);
			
			$scope.cnmt.custCode = $scope.cust.custId;
			$scope.cnmt.cnmtFromSt = $scope.frmStn.stnId;
			$scope.cnmt.cnmtToSt = $scope.toStn.stnId;
			$scope.cnmt.contractCode = $scope.cont.contCode;
			$scope.cnmt.cnmtConsignor = $scope.cngnor.custId;
			$scope.cnmt.cnmtConsignee = $scope.cngnee.custId;
			$scope.cnmt.cnmtPayAt = $scope.payAt.branchId;
			$scope.cnmt.cnmtBillAt = $scope.billAt.branchId;
			$scope.cnmt.cnmtEmpCode = $scope.emp.empId; 
			
			$('#submitId').attr("disabled", "disabled");
			
			var res = $http.post($scope.projectName+'/saveEditCnmt', $scope.cnmt);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("saveEditCnmt result: "+data.result);
					$scope.alertToast(data.result);
					$('#submitId').removeAttr("disabled");
					
					//clear the resources
					$scope.showCnmtFlag = false;
					$scope.cnmt = {};
					
					$scope.branch = {};
					$scope.payAt = {};
					$scope.billAt = {};
					$scope.frmStn = {};
					$scope.toStn = {};
					$scope.cust = {};
					$scope.cngnor = {};
					$scope.cngnee = {};
					$scope.emp = {};
					$scope.cont = {};
					
				} else {
					$scope.alertToast(data.result);
					$('#submitId').removeAttr("disabled");
					console.log("saveEditCnmt result: "+data.result);
				}
			});
			res.error(function(data, status, headers, config) {
				$scope.alertToast(data);
				$('#submitId').removeAttr("disabled");
				console.log("error in response of SaveEditCnmt: "+data);
			});
		}
	}
	
	$scope.uploadCnmtImg = function(){
		console.log("enter into uploadCnmtImg function");
		var file = $scope.cnmtImg;
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			cnmtImgSize = file.size;
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadCnmtImg";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}
	
	//showCnmtImg
	$scope.showCnmtImg = function(){
		console.log("enter into showCnmtImg: ")
		console.log("cnmtImgId: "+$scope.cnmtImgId);
		var response = $http.post($scope.projectName+'/getCnmtImg', $scope.cnmtImgId);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log(data.cnmtImage);
				window.open("data:application/pdf;base64, "+data.cnmtImage);
			} else {
				console.log("getCnmtImg result: "+data.result);
			}
			
	    });
		response.error(function(data, status, headers, config){
			console.log("showCnmtImg Error: "+data);
		});
		
	}
	
	
	$scope.changePrBlCode=function(){
		console.log("Enter into changePrBlCode()");
		if($scope.cnmt.prBlCode === "1754"){
			$scope.cnmt.relType="P";
			$scope.whRequreFlag=true;
			$scope.cngnee.custId=1754;
		}
		if($scope.cnmt.prBlCode === "1755"){
			$scope.cnmt.relType="S";
			$scope.cnmt.whCode="";
			$scope.cngnee.custId=1755;
			$scope.whRequreFlag=false;
		}
		if($scope.cnmt.prBlCode === "1756"){
			$scope.cnmt.relType="O";
			$scope.cnmt.whCode="";
			$scope.cngnee.custId=1756;
			$scope.whRequreFlag=false;
		}
	}
	
	
	//get reliance wharehouse
	$scope.getRelianceWhHouse = function(){
		console.log("Enter into relianceWhHouse");
		var response = $http.post($scope.projectName+'/getRelWharehouse');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.warehouseList=data.whList;
				//console.log("$scope.warehouseList="+$scope.warehouseList);

					$scope.whShowFlag=true;
					$('div#warehouseId').dialog({
						autoOpen: false,
						modal:true,
						title: "Please select Warehouse",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#warehouseId').dialog('open');
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});

		}	
	
	
			// save warehouse	
			$scope.saveWarehouseCode=function(wh){
				console.log("saveWarehouseCode()"+wh[0]);
				$('div#warehouseId').dialog('close');
				$scope.cnmt.relType="P";
				$scope.cnmt.prBlCode="1754";
				$scope.cngnee.custId=1754;
				$scope.cnmt.whCode=wh[0];
			};
	
	
	console.log("EditCnmtEnded");
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getCnmtList();
		$scope.getStnList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	} 

}]);