package com.mylogistics.services;

import com.mylogistics.model.BillDetail;
import com.mylogistics.model.Cnmt;

public class BillDetService {

	private BillDetail billDetail;
	private String frStn;
	private String toStn;
	private Cnmt cnmt;
	public BillDetail getBillDetail() {
		return billDetail;
	}
	public void setBillDetail(BillDetail billDetail) {
		this.billDetail = billDetail;
	}
	public String getFrStn() {
		return frStn;
	}
	public void setFrStn(String frStn) {
		this.frStn = frStn;
	}
	public String getToStn() {
		return toStn;
	}
	public void setToStn(String toStn) {
		this.toStn = toStn;
	}
	public Cnmt getCnmt() {
		return cnmt;
	}
	public void setCnmt(Cnmt cnmt) {
		this.cnmt = cnmt;
	}
	
}
