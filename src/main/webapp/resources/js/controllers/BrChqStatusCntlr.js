'use strict';

var app = angular.module('application');

app.controller('BrChqStatusCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("BrChqStatusCntlr Started");
	
	$scope.branchNCIList = [];
	$scope.branch = {};
	
	$scope.bankNCAIList = [];
	$scope.bank = {};
	
	$scope.chequeLeaveList = [];
	$scope.chequeLeave = {};
	
	$scope.removeChqIndex = "";
	
	$scope.branchDBFlag = true;
	$scope.bankDBFlag = true;
	$scope.removeChqDBFlag = true;
	
	$scope.chqUsed = false;
	$scope.chqCancel = false;
	$scope.chqAll = true;
	$scope.chqunUsed = false;
	
	
	$scope.getActiveBrNCI = function() {
		console.log("getActiveBrNCI Entered");
		var response = $http.post($scope.projectName+'/getActiveBrNCI');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getActiveBrNCAI Success");
				$scope.branchNCIList = data.branchNCIList;
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrNCI Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getActiveBrNCI Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		
		
		
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		$scope.bank = {};
		$('#bankId').attr("disabled","disabled");
		$scope.getBankNCAI();
		
	}
	
	$scope.getBankNCAI = function() {
		console.log("getBankNCAI Entered");
		var response = $http.post($scope.projectName+'/getBankNCAI', $scope.branch.branchId);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getBankNCAI Success");
				$scope.bankNCAIList = data.bankNCAIList;
				$('#bankId').removeAttr("disabled");
			}else {
				console.log("getBankNCAI Error: "+data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankNCAI Error: "+data);
			$scope.alertToast("No bank Assigned to this branch");
		});
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankDBFlag = false;
		$('div#bankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankDBFlag = true;
			}
		});

		$('div#bankDB').dialog('open');
	}
	
	$scope.saveBank = function(bnk){
		$scope.bank = bnk;
		$('div#bankDB').dialog('close');
	}
	
	$scope.chqConditionAll = function() {
		console.log("chqCondition()");
		$scope.chqCancel = false;
		$scope.chqUsed = false;
		$scope.chqunUsed = false;
		
	}
	
	$scope.chqCondition = function() {
		console.log("chqCondition()");
		$scope.chqAll = false;
		$scope.chqunUsed = false;
	}
	
	
	$scope.chqunCondition = function() {
		console.log("chqCondition()");
		$scope.chqAll = false;
		$scope.chqCancel = false;
		$scope.chqUsed = false;
	}
	
	$scope.brChqStatusSubmit = function(brChqStatusForm){
		if(brChqStatusForm.$invalid){
			if (brChqStatusForm.branchName.$invalid) {
				$scope.alertToast("Please Enter Valid Branch");
			} else if (brChqStatusForm.bankName.$invalid) {
				$scope.alertToast("Please Enter Valid Bank");
			} 
		}else{
			
			if ($scope.chqAll || $scope.chqCancel || $scope.chqUsed || $scope.chqunUsed) {
				var chqStatusService = {
						"bnkId"		: $scope.bank.bnkId,
						"branchId"	: $scope.branch.branchId,
						"chqunUsed"	: $scope.chqunUsed,
						"chqCancel"	: $scope.chqCancel,
						"chqUsed"	: $scope.chqUsed,
						"chqAll"	: $scope.chqAll
				}
				
				var response = $http.post($scope.projectName+'/getChqForUpdate', chqStatusService);
				response.success(function(data, status, headers, config){
					
					if (data.result === "success") {
						$scope.chequeLeaveList = data.chequeLeaveList;
					}else {
						console.log("getCsForEdit Error"+data.result);
						$scope.alertToast("No cheque exist for given conditions");
						$scope.chequeLeaveList = [];
					}
				});
				response.error(function(data, status, headers, config){
					console.log("getCsForEdit Error: "+data);
				});
			} else {
				$scope.alertToast("Please Checked appropriate CheckBox");
			}
			
			
			
		}
	}
	
	$scope.updateChq = function(chequeLeave, index){
		
		var response = $http.post($scope.projectName+'/updateChqLeave', chequeLeave);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				$scope.alertToast("Cheque Updated Successfully");
			}else {
				$scope.alertToast("Error in updation");
				console.log("updateChq Error: "+data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("updateChq Error: "+data);
		});
	}
	
	
	
	$scope.removeChq = function(chequeLeave, index){
		
		$scope.chequeLeave = chequeLeave;
		$scope.removeChqIndex = index;
		
		if ($scope.chequeLeave.chqLCancel || chequeLeave.chqLUsed) {
			$scope.alertToast("You are no authorised to delete this cheque");
		} else {
			
			/*$scope.removeDBFlag = false;
			$('div#removeChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Removing Cheque",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.removeDBFlag = true;
				}
			});

			$('div#removeChqDB').dialog('open');*/
			
			$scope.removeChqDBFlag = false;
			$('div#removeChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Removing Cheque",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.removeChqDBFlag = true;
				}
			});

			$('div#removeChqDB').dialog('open');
		}
	}
	
	$scope.removeChqFinal = function(){
		$('div#removeChqDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/removeChqLeave', $scope.chequeLeave.chqLId);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				$scope.alertToast("Cheque Removed Successfully");
				$scope.chequeLeaveList.splice($scope.removeChqIndex, 1);
			}else {
				$scope.alertToast("Error in Deletion");
				console.log("Delete chq Error: "+data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Delete Chq Error: "+data);
		});
	}
	
	$scope.cancel = function() {
		$('div#removeChqDB').dialog('close');
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrNCI();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("BrChqStatusCntlr Ended");
}]);