package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

public class VehVoucherServiceImpl implements VehVoucherService{

	private List<VM_SubVMService> vmsvmList = new ArrayList<VM_SubVMService>();
	
	public void addVmAndSVm(VM_SubVMService vm_SubVMService){
		vmsvmList.add(vm_SubVMService);
	}
	
	public List<VM_SubVMService> getAllVmAndSVm(){
		return vmsvmList;
	}
	
	public void deleteAllVmAndSVM(){
		vmsvmList.clear();
	}
	
	public void remove(int index){
		vmsvmList.remove(index);
	}
}
