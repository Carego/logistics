'use strict';

var app = angular.module('application');

app.controller('UserRightsCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.ur = {};
	$scope.userList = [];
	
	$scope.userDBFlag = true;	
	
	$scope.getUser = function(){
		console.log("enter into getUser function");
		var response = $http.post($scope.projectName+'/allUserFrRght');
		   response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.userList = data.list;
			   }else{
				   $scope.alertToast("Server Error");
			   }
	    });
	    response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
	    });
	}
	
	
	$scope.openUserDB = function(){
		console.log("enter into openUserDB function");
		$scope.userDBFlag = false;
		$('div#userDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			title: "Select User",
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.userDBFlag = true;
			}
		});

		$('div#userDB').dialog('open');
	}
	
	
	$scope.saveUser = function(user){
		console.log("enter into saveUser function");
		$scope.userName = user.userName;
		$scope.ur.userId = user.userId;
		$('div#userDB').dialog('close');
		var response = $http.post($scope.projectName+'/exstngUR',user.userId);
		   response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.ur = data.ur;
				   $scope.alertToast("Already decided the rights of "+user.userName);
			   }
	    });
	    response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
	    });
	}
	
	
	$scope.submitUR = function(userRightsForm){
		console.log("enter into submitUR function = "+$scope.ur.urFT);
		if(userRightsForm.$invalid){
			$scope.alertToast("please select a User");
		}else{
			
			
			var response = $http.post($scope.projectName+'/submitUR',$scope.ur);
			   response.success(function(data, status, headers, config){
				   if(data.result === "success"){
					   $scope.ur = {};
					   $scope.userName = "";
					   $scope.alertToast(data.result);
				   }else{
					   $scope.alertToast("Server Error");
				   }
		    });
		    response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		    });
		}
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getUser();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
}]);