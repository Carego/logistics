package com.mylogistics.constants;

public class ArrivalReportCNTS {
	
	public static final String AR_ID = "arId";
	
	public static final String AR_CODE = "arCode";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String AR_DATE = "arDt";
	
	public static final String AR_RCV_WEIGHT = "arRcvWt";
	
	public static final String AR_PACKAGE = "arPkg";
	
	public static final String AR_UNLOADING = "arUnloading";
	
	public static final String AR_CLAIM = "arClaim";
	
	public static final String AR_DETENTION = "arDetention";
	
	public static final String AR_OTHER = "arOther";
	
	public static final String AR_LATE_DELIVERY = "arLateDelivery";
	
	public static final String AR_LATE_ACKNOWLEDGEMENT = "arLateAck";
	
	public static final String AR_REP_DATE = "arRepDt";
	
	public static final String AR_REP_TIME = "arRepTime";
	
	public static final String AR_EXT_KM = "arExtKm";
	
	public static final String AR_OVR_HGT = "arOvrHgt";
	
	public static final String AR_PENLTY = "arPenalty";
	
	public static final String AR_WT_SHRTG = "arWtShrtg";
	
	public static final String AR_DR_RCVR_WT = "arDrRcvrWt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String AR_CHLN_CODE = "archlnCode";
	
	public static final String AR_DESC = "arDesc";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
	
	public static final String AR_REP_TYPE = "arRepType";
	
	public static final String AR_CANCEL = "cancel";
	
	public static final String AR_SRTG_DMG = "arSrtgDmg";
	
	public static final String AR_IS_SRTG_DMG_ALW = "isSrtgDmgAlw";
}
