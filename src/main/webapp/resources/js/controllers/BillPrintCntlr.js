'use strict';

var app = angular.module('application');

app.controller('BillPrintCntlr',['$scope','$location','$http','$filter','$sce','$window','$log',
                              function($scope,$location,$http,$filter,$sce,$window,$log){
	
	
	$scope.branch = "";
	$scope.selBrh = {};
	$scope.brhId = 0;
	$scope.frBlNo = "";
	$scope.toBlNo = "";
	$scope.brhList = [];
	$scope.blList = [];
	
	$scope.openBrhFlag = true;
	$scope.printVsFlag = true;
	$scope.relBlShow = false;
	
	$scope.hindalcoBill = [107,126,290,326,477];
	
	
	$scope.getBlBranch = function(){
		console.log("enter into getBlBranch function");
		var response = $http.post($scope.projectName+'/getBlBranch');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.brhList = data.list;
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.openBrhFlag = false;
		$('div#openBrhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.openBrhFlag = true;
		    }
			});
		
		$('div#openBrhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branch = brh.branchName;
		$scope.brhId = brh.brhId;
		$scope.selBrh = brh;
		$('div#openBrhId').dialog('close');
	};
	
	$scope.changeCommaNumber=function(numberRate){
		//console.log(numberRate.replace(',', ''));
		return numberRate.replace(',', '');
	};
	
	
	
	$scope.blPrintSubmit = function(blPrintForm){
		console.log("enter into blPrintSubmit function = "+blPrintForm.$invalid);
		if(blPrintForm.$invalid){
			$scope.alertToast("please fill the correct form");
		}else{ 
			console.log("$scope.frBlNo.substring(0,2)="+$scope.frBlNo.substring(0,2));
			console.log("$scope.toBlNo.substring(0,2)="+$scope.toBlNo.substring(0,2));
			console.log("$scope.blDate="+$scope.blDate);
			if($scope.frBlNo.substring(0,2) === $scope.toBlNo.substring(0,2)){
				if(parseInt($scope.frBlNo.substring(3)) > parseInt($scope.toBlNo.substring(3))){
					$scope.alertToast("From-Bill No must be less than To-Bill No");
				}else{
					var req = { 
							"brhId"  : $scope.brhId,
							"frBlNo" : $scope.frBlNo,
							"toBlNo" : $scope.toBlNo
					};
					
					var response = $http.post($scope.projectName+'/blPrintSubmit',req);
					  response.success(function(data, status, headers, config){
						
						  if(data.result === "success"){
							 $scope.blList = data.list;
							 console.log("size of $scope.blList = "+$scope.blList.length);
							 $log.info($scope.blList);
							
							  if($scope.blList.length > 0){
								for(var i=0;i<$scope.blList.length;i++){
									if($scope.blList[i].relBillFormat == true){
										$scope.relBlShow = true;
									}else if($scope.blList[i].hmfBillFormat == true){
										$scope.hmfBillFormat = true;
									}
								} 
								
							 }
						  
							 $scope.printVsFlag = false;
						    	$('div#printVsDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
								        $(this).dialog('destroy');
								        $(this).hide();
								        $scope.printVsFlag = true;
								    }
									});
								$('div#printVsDB').dialog('open');
						  }else{
							  $scope.alertToast(data.msg);
						  }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
					   });
				}
			}else{
				$scope.alertToast("Please enter bill No of same branch");
			}
		}
	};
	
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
	};	
	
	
	$scope.downloadAnx = function(){
		console.log("enter into downloadAnx function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Annexure.xls");
	}
	
	
	$scope.downloadRelAnx = function(){
		console.log("enter into downloadAnx function");
		var blob = new Blob([document.getElementById('exportablerel').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Annexure.xls");
	}
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBlBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);