<div ng-show="operatorLogin || superAdminLogin">
<title>Daily Contract</title>


				 	<!-- Angular.js UI design start -->		
		 	
 		<div class="row">
 			
			<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name="DailyContractForm" ng-submit="submit(DailyContractForm,dlyCnt,metricType,tnc,rbkm,pbdB,pbdP,pbdD,vt,cts)">
				<div class="row">
      			<!-- 	<div class="input-field col s3">
        				<input type ="hidden" id="branchCode" name ="branchCode" ng-model="dlyCnt.branchCode" ng-click="OpenBranchCodeDB()" readonly>
        				<label>Branch Code</label>
      				</div> -->
      				<div class="input-field col s3">
      					<input type ="text" id="dlyContBLPMCodeTemp" name ="dlyContBLPMCodeTemp" ng-model="dlyContBLPMCodeTemp" ng-click="OpendlyContBLPMCodeDB()" readonly required>
      					<label>BLPM Code</label>
      					<input type ="hidden" id="dlyContBLPMCode" name ="dlyContBLPMCode" ng-model="dlyCnt.dlyContBLPMCode">
      				</div>
      				<div class="input-field col s3">
        				<input type ="text" id="dlyContCngrCodeTemp" name ="dlyContCngrCodeTemp" ng-model="dlyContCngrCodeTemp" ng-click="OpendlyContCngrCodeDB()" readonly>
        				<label>Consignor Code</label>
        				<input type ="hidden" id="dlyContCngrCode" name ="dlyContCngrCode" ng-model="dlyCnt.dlyContCngrCode">
      				</div>
      				<div class="input-field col s3">
        				<input type ="text" id="dlyContCrNameTemp" name ="dlyContCrNameTemp" ng-model="dlyCnt.dlyContCrNameTemp" ng-click="OpendlyContCrNameDB()" readonly required disabled="disabled">
        				<label>CR Name</label>
        				<input type ="hidden" id="dlyContCrName" name ="dlyContCrName" ng-model="dlyCnt.dlyContCrName" readonly>
      				</div>
      				
      				<div class="input-field col s3">
      					<select name="metricType" id="metricType" ng-model="metricType" required ng-blur="sendMetricType(metricType)">
							<option value="Ton">Ton</option>
							<option value="Kg">Kg</option>
						</select>
							<label>Metric Type</label>
					</div>
 				</div> 				
 				
 				<div class="row">
 				
 					<div class="input-field col s3">
        				<input id="dlyContStartDt" type="date" name ="dlyContStartDt" ng-model="dlyCnt.dlyContStartDt" required readonly ng-blur="setStartDate(dlyCnt.dlyContStartDt)">
        				<label>Start Date</label>
      				</div>	
      				<div class="input-field col s3">
      					<input id="dlyContEndDt" type="date" name ="dlyContEndDt" ng-model="dlyCnt.dlyContEndDt" required readonly ng-blur="setEndDate(dlyCnt.dlyContEndDt)">
      					<label>End Date</label>
      				</div>
      				
      				<div class="col s3">
      						<div class="input-field col s6">	
        						<input type="radio" id="dlyContProportionate" name ="dlyContProportionate" value="P" ng-model="dlyCnt.dlyContProportionate" ng-click="clickP()">
								<label>Proportionate</label>
							</div>
							<div class="input-field col s6">
								<input type ="radio" id="dlyContProportionate1" name ="dlyContProportionate" value="F" ng-model="dlyCnt.dlyContProportionate" ng-click="clickF()">
        						<label>Fixed</label>
        					</div>
      				</div> 
      				
      				<div class="col s3">	
      					<div class=" input-field col s3"> 
      						<input type="radio" id="dlyContType" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="W"  ng-click="clickW(metricType,dlyCnt.dlyContProportionate)">
      						<label>W</label>
      					</div>
      					<div class="input-field col s3">
							<input  type ="radio" id="dlyContType1" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="Q" ng-click="clickQ(metricType,dlyCnt.dlyContProportionate)">
							<label>Q</label>
						</div>
      					<div class="input-field col s3">
							<input type="radio" id="dlyContType2" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="K"  ng-click="clickK(metricType,dlyCnt.dlyContProportionate)">
      						<label>K</label>
      					</div>
				    </div>
 				</div>
 				
 				<div class="row">
 				     				
      				<!-- <div class="input-field col s4">
        				<input type ="number" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dlyCnt.dlyContAdditionalRate"  min="1" disabled="disabled" ng-blur="setFflagTrue(dlyCnt)">
        				<label>Additional Rate</label>
      				</div> -->
 				</div>
 				
 			
 				<div class="row">
 				
 				
					
      				<div class="input-field col s3">
        				<input type ="text" id="dlyContFromStationTemp" name ="dlyContFromStationTemp" ng-model="dlyCnt.dlyContFromStationTemp" ng-click="OpendlyContFromStationDB()" readonly required>
        				<label>From Station</label>
        				<input type ="hidden" id="dlyContFromStation" name ="dlyContFromStation" ng-model="dlyCnt.dlyContFromStation" readonly>
<!--       					<input class="col s12 btn teal white-text" type ="button"  id="addStation" name ="addStation" ng-model="addStation" ng-click="openAddNewStnDB()" value="Add New Station"> -->
      				</div>
      				<div class="input-field col s3" ng-show="onWQClick">
        				
        				<input type ="button" id="ToStations" name ="ToStations" ng-model="ToStations" ng-click="contToStnDB(dlyCnt)" value="ADD TO STATION">
        				<label>To Station</label>
      				</div>
      				
      				<div class="input-field col s3" ng-show="onKClick">
        				<input type ="text" id="dlyContToStationTemp" name ="dlyContToStationTemp" ng-model="dlyCnt.dlyContToStationTemp" ng-click="OpendlyContToStationDB()" readonly ng-blur="setToStnFlag(dlyCnt)">
        				<label>To Station</label>
        				<input type ="hidden" id="dlyContToStation" name ="dlyContToStation" ng-model="dlyCnt.dlyContToStation" readonly>
      				</div>
      				
      				<div class="col s3">
						<input type ="button" class="btn col s12 teal white-text" id="dlyContRbkmId" value="Add RBKM"  ng-click="OpendlyContRbkmIdDB(dlyCnt)" disabled="disabled">
					 </div>
					 
					 <!-- <div class="input-field col s3">
        				<input style="width: calc(100% - 65px); position: absolute; left: 10px" type ="text" id="dlyContProductType" name ="dlyContProductType" ng-model="dlyCnt.dlyContProductType" ng-click="OpendlyContProductTypeDB()" disabled="disabled" readonly>
						<label>Product type</label>
						<a style="width: 40px; position: absolute; right: -10px; padding: 0 1rem" class="btn waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpendlyContProductTypeDB1()" disabled="disabled"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a>
      				</div> -->
      				
      				
      	<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-12 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table class="tblrow mrg">
 			<caption class="coltag tblrow">Contract To Station</caption>
		    <thead>
		        <tr class="rowclr">
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">Vehicle Type</th>
		            <th class="colclr">From Weight</th>
		            <th class="colclr">To Weight</th>
		            <th class="colclr">Load</th>
		            <th class="colclr">UnLoad</th>
		            <th class="colclr">Transit Day</th>
		            <th class="colclr">ST Chg</th>
		            <th class="colclr">Rate</th>
		            <th class="colclr">Additional Rate</th>	
		            <th class="colclr">Action</th>			            
		        </tr>
		    </thead>
		    <tbody>
		    
		        <tr class="tbl" ng-repeat="cts in ctsList">
					<td class="rowcel">{{dlyContFrmStn}}</td>
				    <td class="rowcel">{{contToStnTemp[$index].ToStnTemp}}</td>
				    <td class="rowcel">{{cts.ctsVehicleType}}</td>
				    <td class="rowcel">{{cts.ctsFromWt}}</td>
		            <td class="rowcel">{{cts.ctsToWt}}</td>
		            <td class="rowcel">{{cts.ctsToLoad}}</td>
		            <td class="rowcel">{{cts.ctsToUnLoad}}</td>
		            <td class="rowcel">{{cts.ctsToTransitDay}}</td>
		            <td class="rowcel">{{cts.ctsToStatChg}}</td>
		          	<td class="rowcel">{{cts.ctsRate}}</td>
		          	<td class="rowcel">{{cts.ctsAdditionalRate}}</td>
		            
		            <td class="rowcel"><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<div class="center col s12">
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
		</div>
		</div>
   		
   		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyContFrmStn}}</td>
				    <td>{{contToStnTemp[$index].ToStnTemp}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsToLoad}}</td>
		            <td>{{cts.ctsToUnLoad}}</td>
		            <td>{{cts.ctsToTransitDay}}</td>
		            <td>{{cts.ctsToStatChg}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
		</div>
		
 				</div>
 				
 				<div class="row">
      				<!-- <div class="input-field col s3">
        				<input type ="number" id="dlyContFromWt" name ="dlyContFromWt" ng-model="dlyCnt.dlyContFromWt"  step="0.01" min="0.01" ng-required="true">
        				<label>From Weight</label>
      				</div> -->
      				
      				<!-- <div class="input-field col s3">
      			
						<select name="fromWt" id="fromWt" ng-model="fromWtPer" required>
							<option value="Ton">Ton</option>
							<option value="Kg">Kg</option>
						</select>
							<label>Ton/KG</label>
					</div> -->
      				
      				<!-- <div class="input-field col s3">
        				<input type ="number" id="dlyContToWt" name ="dlyContToWt" ng-model="dlyCnt.dlyContToWt"  step="0.01" min="0.01" ng-required="true">
        				<label>To Weight</label>
      				</div> -->
      				<!-- <div class="input-field col s3">
      			
						<select name="toWt" id="toWt" ng-model="toWtPer" required>
							<option value="Ton">Ton</option>
							<option value="Kg">Kg</option>
						</select>
							<label>Ton/KG</label>
					</div>
      				 -->
      	<!-- 			<div class="input-field col s3">
      						<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
      						<input type ="number" id="dlyContLoad" name ="dlyContLoad" ng-model="dlyCnt.dlyContLoad" ng-disabled="!checkLoad"  step="0.01" min="0.01" ng-blur="setLoadFlagTrue(dlyCnt)">
      						<label>Load</label>
      	    				<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	    		</div>
      	    		
      				<div class="input-field col s3">
							<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad"  ng-change="saveTncUnload()">
       						<input type ="number" id="dlyContUnLoad" name ="dlyContUnLoad" ng-model="dlyCnt.dlyContUnLoad" ng-disabled="!checkUnLoad"  step="0.01" min="0.01" ng-blur="setUnLoadFlagTrue(dlyCnt)">
							<label>UnLoad</label>
      						<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">
      				</div> -->
      				
      			</div>
      			<div class="row">
      			
      				<!-- <div class="input-field col s3">
        					<input style="width: calc(100% - 75px); position: absolute; left: 10px" type ="text" id="dlyContVehicleType" name ="dlyContVehicleType" ng-model="dlyCnt.dlyContVehicleType" ng-click="OpendlyContVehicleTypeDB()" readonly required>
        					<label>Vehicle Type</label>
      						<a style="width: 40px; position: absolute; right: 0px; padding: 0 1rem" class="btn waves-effect teal" id="openVehicleTypeDB" ng-model="saveVehicletype" ng-click="OpendlyContVehicleTypeDB1()"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a>
      				</div> -->
      				<!-- <div class="input-field col s3">
       						<input id ="dlyContRate" type="number" name ="dlyContRate" ng-model="dlyCnt.dlyContRate"   step="0.01" min="0.01"  ng-required="true">
       						<label> <i class="fa fa-inr"></i> Rate</label>
       				</div>
       				<div class="input-field col s3">
							
							<select name="dlyContRate1" id="dlyContRate1" ng-model="dlyContRatePer" required>
								<option value="Per Ton">Per Ton</option>
								<option value="Per Kg">Per Kg</option>
							</select>
							<label>Per Ton / KG</label>
      				</div> -->
      				<!-- <div class="input-field col s3">
							<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      						<input type ="number" id="dlyContTransitDay" name ="dlyContTransitDay" ng-model="dlyCnt.dlyContTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(dlyCnt)" >
							<label>Transit Day</label>	
							<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        			</div>
        			<div class="input-field col s3">
							<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">	
      						<input type ="number" id="dlyContStatisticalCharge" name ="dlyContStatisticalCharge" ng-model="dlyCnt.dlyContStatisticalCharge" ng-disabled="!checkStatisticalCharge" step="0.01" min="0.01" ng-blur="setSCFlagTrue(dlyCnt)">
							<label>Statistical Charge</label>
							<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      				</div> -->
      			</div>
      			
	<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		             <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Rate</th>
		            <th>Action</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkmStnList[$index].FromStn}}</td>
		            <td>{{rbkmStnList[$index].ToStn}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmLoad}}</td>
		            <td>{{rbkm.rbkmUnLoad}}</td>
		            <td>{{rbkm.rbkmTransitDay}}</td>
		            <td>{{rbkm.rbkmStatChg}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		            <td><input type ="button" ng-click="removeRbkm(rbkm)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="rbkmList.length > 1" ng-click="removeAllRbkm()" value="Remove All RBKM">
		</div>
	</div>
				<div class="row">
					<div class="input-field col s4">
        				<select name="dlyContDc" id="dlyContDc" ng-model="dlyCnt.dlyContDc" required>
							<option value="01">One bill</option>
							<option value="02">Two bill</option>
							<option value="10">Direct Payment through CNMT</option>
							<option value="20">Twice Payment</option>
							<option value="11">Partially through bill Partially through CNMT</option>
						</select>
						<label>DC</label>
      				</div>
					
 					<div class="input-field col s4">
      					<select name="dlyContCostGrade" id="dlyContCostGrade" ng-model="dlyCnt.dlyContCostGrade" required>
							<option value="A">Heavy Material</option>
							<option value="B">Natural Rubber</option>
							<option value="C">General Goods</option>
							<option value="D">Light Goods</option>
						</select>
						<label>Cost Grade</label>
      				</div>
      				
      				<div class="input-field col s4">
      					<select name="dlyContBB" id="dlyContBBId" ng-model="dlyCnt.dlyContBillBasis" ng-init="dlyCnt.dlyContBillBasis = 'chargeWt'" required>
							<option value="chargeWt">Charge Weight</option>
							<option value="receiveWt">Receive Weight</option>
							<option value="actualWt">Actual Weight</option>
						</select>
						<label>Bill Basis</label>
      				</div>

					<!-- <div class="input-field col s4">
        				<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text" type ="button" id="dlyContDetention" value="Add Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div> -->
					<!-- <div class="input-field col s4">
					<div class="col s2">
        					<input type="checkbox" disabled>
			   				<label for="check"></label>
			   		</div>
					<div class="col s10">
      					<input class="btn col s12 teal white-text" type ="button" id="dlyContRbkmId" value="Add RBKM"  ng-click="OpendlyContRbkmIdDB()" disabled="disabled">
      				</div>
      				<input type ="hidden">
					</div> -->
	   				
				
				</div>
 				
 				<div class="row">
      				<div class="input-field col s4">
      					<select name="dlyContHourOrDay" id="dlyContHourOrDay" ng-model="dlyCnt.dlyContHourOrDay" required>
							<option value="Hour">Hour</option>
							<option value="Day">Day</option>
						</select>
      					<label>Hour / Day</label>
      				</div>
      				<div class="input-field col s4">
        				<select name="dlyContDdl" id="dlyContDdl" ng-model="dlyCnt.dlyContDdl" required>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
						<label>DDL</label>
      				</div>
      				<!-- <div class="input-field col s4">
						<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
						<input class="btn teal white-text" type ="button" value="Add Penalty" id="dlyContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
			    		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
      				</div> -->
      				
 				</div>
 				<div class="row">
      				<!-- <div class="col s4">
      						<div class="input-field col s6">	
        						<input type="radio" id="dlyContProportionate" name ="dlyContProportionate" value="P" ng-model="dlyCnt.dlyContProportionate" ng-click="clickP()">
								<label>Proportionate</label>
							</div>
							<div class="input-field col s6">
								<input type ="radio" id="dlyContProportionate1" name ="dlyContProportionate" value="F" ng-model="dlyCnt.dlyContProportionate" ng-click="clickF()">
        						<label>Fixed</label>
        					</div>
      				</div>      				
      				<div class="input-field col s4">
        				<input type ="number" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dlyCnt.dlyContAdditionalRate"  min="1" disabled="disabled" ng-blur="setFflagTrue(dlyCnt)">
        				<label>Additional Rate</label>
      				</div> -->
      				<!-- <div class="input-field col s4">
        				<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
        				<input class="btn teal white-text" type ="button" id="dlyContBonus" value="Add Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
        				<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
      				</div> -->
      				
      			</div>	
	<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		            <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbdStnList[$index].FromStn}}</td>
		            <td>{{pbdStnList[$index].ToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		            <td><input type ="button" ng-click="removePbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="pbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD">
		</div>
		</div>

 				<div class="row">
      				<div class="input-field col s12">
        				<i class="mdi-editor-mode-edit prefix"></i>
        				<textarea id="textarea2" class="materialize-textarea"  rows="3" cols="92"  name ="dlyContRemark" ng-model="dlyCnt.dlyContRemark"></textarea>
        				<label>Remarks</label>
      				</div>
      				<!-- <div class="input-field col s4">
      					<div class="col s2">
        					<input type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
							<label for="checkDetention"></label>
        				</div>
						<div class="col s10">
							<input class="col s12 btn teal white-text" type ="button" id="dlyContDetention" value="Add Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
							<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
						</div>
      				</div> -->
 				</div>
      			<div class="row">
      				<div class="center col s12">
      					<input class="btn" type="submit" id="submit"value="Submit">
      				</div>
 				</div>
      				
 			</form>
 		</div> 
 		
 <!-- Angular.js UI design end -->	
	
			 	<!-- Angular.js UI design start -->		
		 	
<!--  		<div class="row" style="align: center; margin-top: 40px;">
 		<div class="col s1 hide-on-med-and-down"> &nbsp; </div>
			<form class="col s10" ng-submit="submit(dlyCnt,dlyContRatePer,tnc)">
				<div class="row">
      				<div class="input-field col s3">
        				<input class="validate "  type ="text" id="branchCode" name ="branchCode" ng-model="dlyCnt.branchCode" ng-click="OpenBranchCodeDB()" readonly>
        				<label for="branchCode">Branch Code</label>
      				</div>
      				<div class="input-field col s3">
      					<input class="validate"  type ="text" id="dlyContBLPMCode" name ="dlyContBLPMCode" ng-model="dlyCnt.dlyContBLPMCode" ng-click="OpendlyContBLPMCodeDB()" readonly>
      					<label for="dlyContBLPMCode">BLPM Code</label>
      				</div>
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id="dlyContCngrCode" name ="dlyContCngrCode" ng-model="dlyCnt.dlyContCngrCode" ng-click="OpendlyContCngrCodeDB()" readonly>
        				<label for="dlyContCngrCode">Consignor Code</label>
      				</div>
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id="dlyContCrNameTemp" name ="dlyContCrNameTemp" ng-model="dlyCnt.dlyContCrNameTemp" ng-click="OpendlyContCrNameDB()" readonly>
        				<input type ="hidden" id="regContCrName" name ="regContCrName" ng-model="regCnt.regContCrName" readonlytype ="hidden" id="dlyContCrName" name ="dlyContCrName" ng-model="dlyCnt.dlyContCrName" readonly>
        				<label for="dlyContCrNameTemp">CR Code</label>
      				</div>
 				</div>
 				
 				<div class="row">
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id="dlyContFromStationTemp" name ="dlyContFromStationTemp" ng-model="dlyCnt.dlyContFromStationTemp" ng-click="OpendlyContFromStationDB()" readonly>
        				<input type ="hidden" id="dlyContFromStation" name ="dlyContFromStation" ng-model="dlyCnt.dlyContFromStation" readonly>
        				<label for="dlyContFromStationTemp">From Station</label>
      				</div>
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id="dlyContToStationTemp" name ="dlyContToStationTemp" ng-model="dlyCnt.dlyContToStationTemp" ng-click="OpendlyContToStationDB()" readonly>
        				<input type ="hidden" id="dlyContToStation" name ="dlyContToStation" ng-model="dlyCnt.dlyContToStation" readonly>
        				<label for="dlyContToStationTemp">To Station</label>
      				</div>
      				<div class="input-field col s3">
      					<div class="col s4">&nbsp;</div>
      					<div class="col s8">
        					<input class="validate "  id="dlyContStartDt" type="date" name ="dlyContStartDt" ng-model="dlyCnt.dlyContStartDt">
        					<label>Start Date:</label>
      					</div>
      				</div>	
      				<div class="input-field col s3">
      					<div class="col s4">&nbsp;</div>
      					<div class="col s8">
      						<input class="validate"  id="dlyContEndDt" type="date" name ="dlyContEndDt" ng-model="dlyCnt.dlyContEndDt">
      						<label>End Date:</label>
      					</div>
      				</div>	
 				</div>
 				
 				<div class="row">
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id=" dlyContFromWt" name ="dlyContFromWt" ng-model="dlyCnt.dlyContFromWt">
        				<label for="dlyContFromWt">From Weight</label>
      				</div>
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id="dlyContToWt" name ="dlyContToWt" ng-model="dlyCnt.dlyContToWt">
        				<label for="dlyContToWt">To Weight</label>
      				</div>
      				<div class="col s3">
      					<div class="input-field col s4">
      						<input type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
							<label for="checkLoad"></label>
      					</div>
      					<div class="input-field col s8">
      						<input class="validate" type ="text" id="dlyContLoad" name ="dlyContLoad" ng-model="dlyCnt.dlyContLoad" ng-disabled="!checkLoad">
      						<label for="dlyContLoad">Load</label>
      	    			</div>
      	    			<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">
      	    		</div>
      	    		
      				<div class="col s3">
						<div class="input-field col s4">
							<input type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad"  ng-change="saveTncUnload()">
							<label for="checkUnLoad"></label>
						</div>
      					<div class="input-field col s8">
       						<input class="validate" type ="text" id="dlyContUnLoad" name ="dlyContUnLoad" ng-model="dlyCnt.dlyContUnLoad" ng-disabled="!checkUnLoad">
							<label for="regContUnLoad">UnLoad</label>
						</div>
      				<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">
      				</div>
      				
      			</div>
      			<div class="row">
      				<div class="input-field col s3">
       						<input class="validate" type ="text" id="dlyContRateId" name ="dlyContRate" ng-model="dlyCnt.dlyContRate">
       						<label for="dlyContRateId"> <i class="fa fa-inr"></i>  Rate</label>
       				</div>
       				<div class="input-field col s3">
							<select class="browser-default grey-text" name="dlyContRate1" id="dlyContRate1" ng-model="dlyContRatePer">
								<option value="Per Kg" disabled selected>Choose your option</option>
								<option value="Per Ton">Per Ton</option>
								<option value="">Per Kg</option>
							</select>
      				</div>
      				<div class="col s3">
						<div class="input-field col s4">	
							<input type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
							<label for="checkTransitDay"></label>
						</div>
						<div class="input-field col s8">
      						<input class="validate" type ="text" id="dlyContTransitDay" name ="dlyContTransitDay" ng-model="dlyCnt.dlyContTransitDay" ng-disabled="!checkTransitDay">
							<label for="dlyContTransitDay">Transit Day</label>	
							<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
						</div>
        			</div>
        			<div class="col s3">
        				<div class="input-field col s4">
							<input type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">	
    	  					<label for="checkStatisticalCharge"></label>
						</div>
						<div class="input-field col s8">
      						<input class="validate" type ="text" id="dlyContStatisticalCharge" name ="dlyContStatisticalCharge" ng-model="dlyCnt.dlyContStatisticalCharge" ng-disabled="!checkStatisticalCharge">
							<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
							<label for="dlyContStatisticalCharge">Statistical Charge</label>
      					</div>
      				</div>
      			</div>
      			<div class="row">
      				<div class="input-field col s6">
      					<div class="col s2">&nbsp;<label>Type:</label></div>
      					<div class="col s3"> 
      						<input class="with-gap" type="radio" id="dlyContType" name ="dlyContType" ng-model="dlyCnt.dlyContType" value="W"  ng-click="clickW()" >
      						<label for="dlyContType">W</label>
      					</div>
      					<div class="col s3">
							<input class="with-gap" type ="radio" id="dlyContType1" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="Q" ng-click="clickQ()">
							<label for="dlyContType1">Q</label>
						</div>
      					<div class="col s3">
							<input class="with-gap" type="radio" id="dlyContType2" name ="dlyContType" ng-model="dlyCnt.dlyContType" value="K"  ng-click="clickK()">
      						<label for="dlyContType2">K</label>
      					</div>
					</div>
					 <div class="input-field col s3">
					 		<div class="input-field col s4">
							<button class="btn waves-effect teal" type="button" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpendlyContProductTypeDB1()" disabled="disabled">
      						<i class="mdi-action-open-in-new"></i></button>
      						</div>
      					<div class="input-field col s8">
        					<input class="validate" type ="text" id="dlyContProductType" name ="dlyContProductType" ng-model="dlyCnt.dlyContProductType" ng-click="OpendlyContProductTypeDB()" disabled="disabled" readonly>
							<label for="regContProductType">Product type</label>
						</div>
      				</div>
      				<div class="input-field col s3">
      						<div class="input-field col s4">
      						<button class="btn waves-effect teal" type="button" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpendlyContProductTypeDB1()" disabled="disabled">
      						<i class="mdi-action-open-in-new"></i></button>
      						</div>
        				<div class="input-field col s8">
        					<input class="validate" type ="text" id="dlyContVehicleType" name ="dlyContVehicleType" ng-model="dlyCnt.dlyContVehicleType" ng-click="OpendlyContVehicleTypeDB()" readonly>
        					<label for="dlyContVehicleType">Vehicle Type</label>
        				</div>
      				</div>
 				</div>
				<div class="row">
					
					<div class="input-field col s4">
        				<select class="browser-default grey-text" name="dlyContDc" id="dlyContDc" ng-model="dlyCnt.dlyContDc">
							<option value="" disabled selected>DC</option>
							<option value="01">One bill</option>
							<option value="02">Two bill</option>
							<option value="10">Direct Payment through CNMT</option>
							<option value="20">Twice Payment</option>
							<option value="11">Partially through bill Partially through CNMT</option>
						</select>
      				</div>
					
 					<div class="input-field col s4">
      					<select class="browser-default grey-text" name="dlyContCostGrade" id="dlyContCostGrade" ng-model="dlyCnt.dlyContCostGrade">
							<option value="" disabled selected>Cost Grade</option>
							<option value="A">Heavy Material</option>
							<option value="B">Natural Rubber</option>
							<option value="C">General Goods</option>
							<option value="D">Light Goods</option>
						</select>
      				</div>

					<div class="input-field col s4">
					<div class="col s2">
        					<input type="checkbox" disabled>
			   				<label for="check"></label>
			   		</div>
					<div class="col s10">
      					<input class="btn col s12 teal white-text" type ="button" id="dlyContRbkmId" value="Add RBKM"  ng-click="OpendlyContRbkmIdDB()" disabled="disabled">
      				</div>
      				<input type ="hidden">
					</div>
	   				
				
				</div>
 				
 				<div class="row">
      				<div class="input-field col s4">
      					<select class="browser-default grey-text" name="dlyContHourOrDay" id="dlyContHourOrDay" ng-model="dlyCnt.dlyContHourOrDay">
      						<option value="" disabled selected>Hour/Day</option>
							<option value="Hour">Hour</option>
							<option value="Day">Day</option>
						</select>
      				</div>
      				<div class="input-field col s4">
        				<select class="browser-default grey-text" name="dlyContDdl" id="dlyContDdl" ng-model="dlyCnt.dlyContDdl">
							<option value="" disabled selected>DDL</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
      				</div>
      				<div class="input-field col s4">
      					<div class="col s2">
							<input type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
			    			<label for="checkPenalty"></label>
						</div>
						<div class="col s10">
							<input class="col s12 btn teal white-text" type ="button" value="Add Penalty" id="dlyContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
			    		</div>
			    		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
      				</div>
      				
 				</div>
 				<div class="row">
      				<div class="input-field col s4">
      						<div class="col s4">&nbsp;<label>Proportionate:</label></div>
      						<div class="col s4">	
        						<input class="with-gap" type="radio" id="dlyContProportionate" name ="dlyContProportionate" value="P" ng-model="dlyCnt.dlyContProportionate" ng-click="clickP()">
								<label for="dlyContProportionate">P</label>
							</div>
						
							<div class="col s4">
								<input class="with-gap" type ="radio" id="dlyContProportionate1" name ="dlyContProportionate" value="F" ng-model="dlyCnt.dlyContProportionate" ng-click="clickF()">
        						<label for="dlyContProportionate1">F</label>
        					</div>
      				</div>      				
      				<div class="input-field col s4">
        				<input class="validate" type ="text" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dlyCnt.dlyContAdditionalRate" disabled="disabled">
        				<label for="dlyContAdditionalRate">Additional Rate</label>
      				</div>
      				<div class="input-field col s4">
        				<div class="col s2">
        					<input type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
			   				<label for="checkBonus"></label>
			   			</div>
        				<div class="col s10">
        					<input class="col s12 btn teal white-text" type ="button" id="dlyContBonus" value="Add Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
        					<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        				</div>
      				</div>
      				
      			</div>	
      			
 				<div class="row">
      				<div class="input-field col s8">
        				<input class="validate" type ="text" id="dlyContCngrCode" name ="dlyContCngrCode" ng-model="dlyCnt.dlyContCngrCode" ng-click="OpendlyContCngrCodeDB()" readonly>
        				<label for="dlyContCngrCode">Remarks</label>
      				</div>
      				<div class="input-field col s4">
      					<div class="col s2">
        					<input type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
							<label for="checkDetention"></label>
        				</div>
						<div class="col s10">
							<input class="col s12 btn teal white-text" type ="button" id="dlyContDetention" value="Add Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
							<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
						</div>
      				</div>
 				</div>
      			
      			<div class="row">
      			</div>
      			<div class="row">
      				<div class="center col s12">
      					<input class="btn waves-effect waves-light" type="submit" id="submit"value="Submit">
      				</div>
 				</div>
      				
 			</form>
 		</div> -->
 		
 <!-- Angular.js UI design end -->	
	
<!-- 	<form name="DailyContractForm" ng-submit="submit(DailyContractForm,dlyCnt,dlyContRatePer,tnc,rbkm,pbdP,pbdB,pbdD,vt)">
		<table>
			<tr>
				<td>Branch Code *</td>
				<td><input type ="text" id="branchCode" name ="branchCode" ng-model="dlyCnt.branchCode" ng-click="OpenBranchCodeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td>Daily Contract BLPM Code *</td>
				<td><input type ="text" id="dlyContBLPMCode" name ="dlyContBLPMCode" ng-model="dlyCnt.dlyContBLPMCode" ng-click="OpendlyContBLPMCodeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td>Daily Contract Consignor Code *</td>
				<td><input type ="text" id="dlyContCngrCode" name ="dlyContCngrCode" ng-model="dlyCnt.dlyContCngrCode" ng-click="OpendlyContCngrCodeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td>Daily Contract CR Name *</td>
				<td><input type ="text" id="dlyContCrNameTemp" name ="dlyContCrNameTemp" ng-model="dlyCnt.dlyContCrNameTemp" ng-click="OpendlyContCrNameDB()" readonly required></td>
				<td><input type ="hidden" id="dlyContCrName" name ="dlyContCrName" ng-model="dlyCnt.dlyContCrName" readonly></td>
			</tr>
			
			<tr>
				<td>Daily Contract Start Date *</td>
				<td><input  id="dlyContStartDt" type="date" name ="dlyContStartDt" ng-model="dlyCnt.dlyContStartDt" required></td>
			</tr>
			
			<tr>
				<td>Daily Contract End Date *</td>
				<td><input  id="dlyContEndDt" type="date" name ="dlyContEndDt" ng-model="dlyCnt.dlyContEndDt" required></td>
			</tr>
			
			<tr>
				<td>Daily Contract From Station *</td>
				<td><input type ="text" id="dlyContFromStationTemp" name ="dlyContFromStationTemp" ng-model="dlyCnt.dlyContFromStationTemp" ng-click="OpendlyContFromStationDB()" readonly required></td>
				<td><input type ="hidden" id="dlyContFromStation" name ="dlyContFromStation" ng-model="dlyCnt.dlyContFromStation" readonly></td>
			</tr>
			
			<tr>
				<td>Daily Contract To Station *</td>
				<td><input type ="text" id="dlyContToStationTemp" name ="dlyContToStationTemp" ng-model="dlyCnt.dlyContToStationTemp" ng-click="OpendlyContToStationDB()" readonly required></td>
				<td><input type ="hidden" id="dlyContToStation" name ="dlyContToStation" ng-model="dlyCnt.dlyContToStation" readonly></td>
			</tr>
			
			<tr>
				<td>Daily Contract Rate *</td>
				<td>Rs.<input type ="number" name ="dlyContRate" ng-model="dlyCnt.dlyContRate"  ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true">
				<select name="dlyContRate1" id="dlyContRate1" ng-model="dlyContRatePer" required>
				<option value="Per Ton">Per Ton</option>
				<option value="Per Kg">Per Kg</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Daily Contract From Weight *</td>
				<td><input type ="number" id=" dlyContFromWt" name ="dlyContFromWt" ng-model="dlyCnt.dlyContFromWt" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Daily Contract To Weight *</td>
				<td><input type ="number" id="dlyContToWt" name ="dlyContToWt" ng-model="dlyCnt.dlyContToWt" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Daily Contract Type</td>
				<td><input type="radio" id="dlyContType" name ="dlyContType" ng-model="dlyCnt.dlyContType" value="W"  ng-click="clickW()" >W
				<input type ="radio" id="dlyContType1" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="Q" ng-click="clickQ()">Q
				<input type="radio" id="dlyContType2" name ="dlyContType" ng-model="dlyCnt.dlyContType" value="K"  ng-click="clickK()">K</td>
			</tr>
			
			<tr>
				<td>Daily Contract Product Type</td>
				<td><input type ="text" id="dlyContProductType" name ="dlyContProductType" ng-model="dlyCnt.dlyContProductType" ng-click="OpendlyContProductTypeDB()" disabled="disabled" readonly>	
				<input type="button" id="-" ng-model="savProducttype" ng-click="OpendlyContProductTypeDB1()" disabled="disabled"></td>
			</tr>
			
			<tr>
				<td>Daily Contract RBKM ID</td>
				<td><input type ="button" id="dlyContRbkmId" value="Add RBKM"  ng-click="OpendlyContRbkmIdDB()" disabled="disabled"></td>
			</tr>
			
			<tr>
				<td>Daily Contract Vehicle Type *</td>
				<td><input type ="text" id="dlyContVehicleType" name ="dlyContVehicleType" ng-model="dlyCnt.dlyContVehicleType" ng-click="OpendlyContVehicleTypeDB()" readonly required>
					<input type="button" id="openVehicleTypeDB" ng-model="saveVehicletype" ng-click="OpendlyContVehicleTypeDB1()"></td>
			</tr>
			
			<tr>
				<td>Daily Contract DC *</td>
				<td><select name="dlyContDc" id="dlyContDc" ng-model="dlyCnt.dlyContDc" required>
					<option value="01">One bill</option>
					<option value="02">Two bill</option>
					<option value="10">Direct Payment through CNMT</option>
					<option value="20">Twice Payment</option>
					<option value="11">Partially through bill Partially through CNMT</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Daily Contract Cost Grade *</td>
				<td><select name="dlyContCostGrade" id="dlyContCostGrade" ng-model="dlyCnt.dlyContCostGrade" required>
					<option value="A">Heavy Material</option>
					<option value="B">Natural Rubber</option>
					<option value="C">General Goods</option>
					<option value="D">Light Goods</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Daily Contract Transit Day</td>
				<td><input type ="number" id="dlyContTransitDay" name ="dlyContTransitDay" ng-model="dlyCnt.dlyContTransitDay" ng-disabled="!checkTransitDay" ng-pattern="/[1-9]/" min="1" ng-blur="setTransitFlagTrue()" ng-minlength="1" ng-maxlength="7">
				<input type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()"></td>
				<td><input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" ></td>
			</tr>
			
			<tr>
				<td>Daily Contract Statistical Charge</td>
				<td><input type ="number" id="dlyContStatisticalCharge" name ="dlyContStatisticalCharge" ng-model="dlyCnt.dlyContStatisticalCharge" ng-disabled="!checkStatisticalCharge" min="1" ng-blur="setSCFlagTrue()" ng-pattern="/[1-9]/" ng-minlength="1" ng-maxlength="7">
				<input type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()"></td>
				<td><input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" ></td>
			</tr>
				
				<tr>
					<td>Daily Contract Load</td>
					<td><input type ="number" id="dlyContLoad" name ="dlyContLoad" ng-model="dlyCnt.dlyContLoad" ng-disabled="!checkLoad" ng-pattern="/[1-9]/" min="1" ng-blur="setLoadFlagTrue()" ng-minlength="1" ng-maxlength="7">
					<input type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()"></td>
			   		<td><input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" ></td>
			    </tr>
			    
			     <tr>
					<td>Daily Contract UnLoad</td>
					<td><input type ="number" id="dlyContUnLoad" name ="dlyContUnLoad" ng-model="dlyCnt.dlyContUnLoad" ng-disabled="!checkUnLoad" ng-pattern="/[1-9]/" min="1" ng-blur="setUnLoadFlagTrue()" ng-minlength="1" ng-maxlength="7">
					<input type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad"  ng-change="saveTncUnload()"></td>
					<td><input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'"></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract Penalty</td>
					<td><input type ="button" value="Add Penalty" id="dlyContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
					<input type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()"></td>
			    	<td><input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'"></td>
			    </tr> 
			   
			    <tr>
					<td>Daily Contract Bonus</td>
					<td><input type ="button" id="dlyContBonus" value="Add Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
					<input type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()"></td>
			   		<td><input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'"></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract Detention</td>
					<td><input type ="button" id="dlyContDetention" value="Add Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
					<input type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()"></td>
			    	<td><input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'"></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract Hour Or Day *</td>
					<td><select name="dlyContHourOrDay" id="dlyContHourOrDay" ng-model="dlyCnt.dlyContHourOrDay" required>
							<option value="Hour">Hour</option>
							<option value="Day">Day</option>
						</select></td>
				</tr>
			    
				
				<tr>
					<td>Daily Contract Proportionate</td>
					<td><input type="radio" id="dlyContProportionate" name ="dlyContProportionate" value="P" ng-model="dlyCnt.dlyContProportionate" ng-click="clickP()">P
					<input type ="radio" id="dlyContProportionate1" name ="dlyContProportionate" value="F" ng-model="dlyCnt.dlyContProportionate" ng-click="clickF()">F
					</td>
				</tr>
			    
			     <tr>
					<td>Daily Contract Additional Rate</td>
					<td><input type ="number" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dlyCnt.dlyContAdditionalRate" ng-pattern="/[1-9]/" min="1" disabled="disabled" ng-minlength="1" ng-maxlength="7" ng-blur="setFflagTrue()"></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract DDL *</td>
					<td><select name="dlyContDdl" id="dlyContDdl" ng-model="dlyCnt.dlyContDdl" required>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select></td>
				</tr>
			    
			    <tr>
					<td>Daily Contract Remarks</td>
					<td><textarea rows="4" cols="50"  name ="dlyContRemark" ng-model="dlyCnt.dlyContRemark"></textarea></td>
			    </tr>
			    
			    <tr>
					<td><input type="submit" value="Submit"></td>
				</tr>
			    
			 </table>
			 </form> -->
 	   
	<div id="dlyContRbkmIdDB" ng-hide = "RbkmIdFlag">	
	<form name="RbkmForm" ng-submit="saveRbkmId(RbkmForm,rbkm,metricType)"> 
	
		<table>
			<tr>
				<!-- <td>From Station *</td> -->
				<td><input type ="hidden" id="rbkmFromStationTemp" name ="rbkmFromStationTemp" ng-model="rbkm.rbkmFromStationTemp" ng-click="OpenrbkmFromStationDB()" readonly></td>
				<td><input type ="hidden" id="rbkmFromStation" name ="rbkmFromStation" ng-model="rbkm.rbkmFromStation" ></td>
			</tr>
			
			<tr>
				<td>To Station </td>
				<td><input type ="text"  id="rbkmToStationTemp" name ="rbkmToStationTemp" ng-model="rbkm.rbkmToStationTemp" ng-click="OpenrbkmToStationDB()" readonly></td>
			<td><input type ="hidden"  id="rbkmToStation" name ="rbkmToStation" ng-model="rbkm.rbkmToStation" ></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input type ="text"  id="rbkmStateCodeTemp" name ="rbkmStateCodeTemp" ng-model="rbkm.rbkmStateCodeTemp" ng-click="OpenrbkmStateCodeDB()" readonly required></td>
				<td><input type ="hidden"  id="rbkmStateCode" name ="rbkmStateCode" ng-model="rbkm.rbkmStateCode"></td>
			</tr>
			
			<tr>
				<td>From Kilometer *</td>
				<td><input type ="number"  id="rbkmFromKm" name ="rbkmFromKm" ng-model="rbkm.rbkmFromKm" ng-minlength="1" ng-maxlength="7" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Kilometer *</td>
				<td><input type ="number"  id="rbkmToKm" name ="rbkmToKm" ng-model="rbkm.rbkmToKm" ng-minlength="1" ng-maxlength="7" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text"  id="rbkmVehicleType" name ="rbkmVehicleType" ng-model="rbkm.rbkmVehicleType" ng-click="OpenRbkmVehicleTypeDB()" required readonly></td>
				<td><input type="button" ng-model="addVTForRbkm" ng-click="OpenAddVehicleTypeDB()"></td>
			</tr>
			
			<tr>
				<td> <div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoadRbkm" ng-model="checkLoadRbkm" ng-change="saveTncLoadRbkm()">
					<input class="validate" type ="number" id="rbkmLoad" name ="rbkmLoad" ng-model="rbkm.rbkmLoad" ng-disabled="!checkLoadRbkm"  step="0.00001" min="0.00001"  ng-blur="setLoadFlagTrueRbkm(rbkm)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div></td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoadRbkm" ng-model="checkUnLoadRbkm" ng-change="saveTncUnloadRbkm()">
					<input class="validate" type ="number" id="rbkmtUnLoad" name ="rbkmUnLoad" ng-model="rbkm.rbkmUnLoad" ng-disabled="!checkUnLoadRbkm"  step="0.00001" min="0.00001"  ng-blur="setUnLoadFlagTrueRbkm(rbkm)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDayRbkm" ng-model="checkTransitDayRbkm" ng-change="saveTncTransitDayRbkm()">
      				<input class="validate" type ="number" id="rbkmTransitDay" name ="rbkmTransitDay" ng-model="rbkm.rbkmTransitDay" ng-disabled="!checkTransitDayRbkm"  min="1" ng-blur="setTransitFlagTrueRbkm(rbkm)" >
					<label for="rbkmTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalChargeRbkm" ng-model="checkStatisticalChargeRbkm" ng-change="saveTncStatisticalChargeRbkm()">
      				<input class="validate" type ="number" id="rbkmStatChg" name ="rbkmStatChg" ng-model="rbkm.rbkmStatChg" ng-disabled="!checkStatisticalChargeRbkm"  step="0.00001" min="0.00001"  ng-blur="setSCFlagTrueRbkm(rbkm)">
					<label for="rbkmStstChg">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>Rate(Per Km) *</td>
				<td><input type ="number"  id="rbkmRate" name ="rbkmRate" ng-model="rbkm.rbkmRate"   step="0.00001" min="0.00001" required></td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
			
		 	<tr>
				<td colspan="2" class="center"><input type="submit" id="submit3" value="Submit" ></td>
			</tr>
		</table>
	</form>
	</div>	
	
	<!-- <div id="addVTForRbkmDB" ng-hide = "RbkmAddVTFlag">
	<form name="RbkmVehicleForm" ng-submit="saveVTForRbkm(RbkmVehicleForm,vt)">
		<table>
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType"  required></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  required></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit"  min ="1"  required></td>
			</tr>
			
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt"  step="0.01" min="0.01" required></td>
			</tr>
			
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	 -->
	
	<div id="AddProductTypeDB" ng-hide = "AddProductTypeFlag">
	<form name="ProductTypeDB1Form" ng-submit="saveproducttype(ProductTypeDB1Form,pt)">
		<table>
			<tr>
				<td>Product Name</td>
				<td><input type ="text" id="ptName" name ="ptName" ng-model="pt.ptName" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="addVehicleTypeDB1" ng-hide = "VehicleTypeFlag1">
	<form name="VehicleForm" ng-submit="savevehicletype(VehicleForm,vt)">
		<table>
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType"  required></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  required></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit"  min ="1"  required></td>
			</tr>
			
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt"  step="0.00001" min="0.00001" required></td>
			</tr>
			
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
		
		<div id="PenaltyDB" ng-hide="PenaltyFlag">
	<form name="PenaltyForm" ng-submit="savePbdForPenalty(PenaltyForm,pbdP)"> 
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempP" name ="pbdFromStnTempP" ng-model="pbdP.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdFromStnP" name ="pbdFromStnP" ng-model="pbdP.pbdFromStn"  ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempP" name ="pbdToStnTempP" ng-model="pbdP.pbdToStnTemp" ng-click="OpenPenaltyToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnP" name ="pbdToStnP" ng-model="pbdP.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtP" name ="pbdStartDtP" ng-model="pbdP.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtP" name ="pbdEndDtP" ng-model="pbdP.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day*</td>
				<td><input type ="number" id="pbdFromDayP" name ="pbdFromDayP" ng-model="pbdP.pbdFromDay" required  min="1" ></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayP" name ="pbdToDayP" ng-model="pbdP.pbdToDay" required  min="1" ></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeP" name ="pbdVehicleTypeP" ng-model="pbdP.pbdVehicleType" ng-click="OpenPenaltyVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayP" id="pbdHourNDayP" ng-model="pbdP.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtP" name ="pbdAmtP" ng-model="pbdP.pbdAmt" required  step="0.00001" min="0.00001" ></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Submit" id="savePenalty"  ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="BonusDB" ng-hide="BonusFlag">
	<form name="BonusForm" ng-submit="savePbdForBonus(BonusForm,pbdB)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempB" name ="pbdFromStnTempB" ng-model="pbdB.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdFromStnB" name ="pbdFromStnB" ng-model="pbdB.pbdFromStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempB" name ="pbdToStnTempB" ng-model="pbdB.pbdToStnTemp" ng-click="OpenBonusToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnB" name ="pbdToStnB" ng-model="pbdB.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtB" name ="pbdStartDtB" ng-model="pbdB.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtB" name ="pbdEndDtB" ng-model="pbdB.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayB" name ="pbdFromDayB" ng-model="pbdB.pbdFromDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayB" name ="pbdToDayB" ng-model="pbdB.pbdToDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeB" name ="pbdVehicleTypeB" ng-model="pbdB.pbdVehicleType" ng-click="OpenBonusVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayB" id="pbdHourNDayB" ng-model="pbdB.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtB" name ="pbdAmtB" ng-model="pbdB.pbdAmt"  step="0.00001" min="0.00001" required></td>
			</tr>
			
		 	<tr>
				<td colspan="2" class="center"><input type="submit" value="Submit"  id="saveBonus" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="DetentionDB" ng-hide="DetentionFlag">
	<form name="DetentionForm"  ng-submit="savePbdForDetention(DetentionForm,pbdD)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempD" name ="pbdFromStnTempD" ng-model="pbdD.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempD" name ="pbdToStnTempD" ng-model="pbdD.pbdToStnTemp" ng-click="OpenDetentionToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtD" name ="pbdStartDtD" ng-model="pbdD.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtD" name ="pbdEndDtD" ng-model="pbdD.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayD" name ="pbdFromDayD" ng-model="pbdD.pbdFromDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayD" name ="pbdToDayD" ng-model="pbdD.pbdToDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeD" name ="pbdVehicleTypeD" ng-model="pbdD.pbdVehicleType" ng-click="OpenDetentionVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayD" id="pbdHourNDayD" ng-model="pbdD.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtD" name ="pbdAmtD" ng-model="pbdD.pbdAmt" required  step="0.00001" min="0.00001"></td>
			</tr>
			
		 	<tr>
				<td colspan="2" class="center"><input type="submit" value="Submit" id="saveDetention" ></td>
			</tr>
		</table>
		</form>
	</div>
		
		
	<div id="contToStationW" ng-hide = "contToStationFlagW">
	<form name="ContToStationFormW" ng-submit="saveContToStnW(ContToStationFormW,cts,dlyCnt,metricType)">
		<table>
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
				<td><a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
			</tr>
			
			<tr>
				<!-- <td>Product Type*</td> -->
				<td><input type ="hidden" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType"  readonly></td>
			</tr> 
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWt" name ="ctsFromWt" ng-model="cts.ctsFromWt"  step="0.00001" min="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWt" name ="ctsToWt" ng-model="cts.ctsToWt" step="0.00001" min="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required></td>
				<td><input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRate" name ="ctsRate" ng-model="cts.ctsRate" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>Additional Rate *</td>
				<td><input type ="number" id="ctsAdditionalRate" name ="ctsAdditionalRate" ng-model="cts.ctsAdditionalRate" step="0.00001" required disabled="disabled"></td>
			</tr>
			
			<tr>
				<td>
				</td>
			</tr>
			
			<tr>
				<td> <div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
					<input class="validate" type ="number" id="ctsToLoad" name ="ctsToLoad" ng-model="cts.ctsToLoad" ng-disabled="!checkLoad"  step="0.00001" min="0.00001"  ng-blur="setLoadFlagTrue(cts)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div></td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad" ng-change="saveTncUnload()">
					<input class="validate" type ="number" id="ctsToUnLoad" name ="ctsToUnLoad" ng-model="cts.ctsToUnLoad" ng-disabled="!checkUnLoad"  step="0.00001" min="0.00001"  ng-blur="setUnLoadFlagTrue(cts)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      				<input class="validate" type ="number" id="ctsToTransitDay" name ="ctsToTransitDay" ng-model="cts.ctsToTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(cts)" >
					<label for="regContTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">
      				<input class="validate" type ="number" id="ctsToStatChg" name ="ctsToStatChg" ng-model="cts.ctsToStatChg" ng-disabled="!checkStatisticalCharge"  step="0.00001" min="0.00001"  ng-blur="setSCFlagTrue(cts)">
					<label for="regContStatisticalCharge">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
			
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
	
	<div id="contToStationQ" ng-hide = "contToStationFlagQ">
	<form name="ContToStationFormQ" ng-submit="saveContToStnQ(ContToStationFormQ,cts,dlyCnt)">
		<table>
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required></td>
				<td><input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
				<td><a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
			</tr>
			
			<tr>
				<td>Product Type*</td>
				<td><input type ="text" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" ng-click="openProductTypeDB()" readonly required></td>
				<td><a style="width: 40px; position: absolute; right: -10px; padding: 0 1rem" class="btn waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpenProductTypeDB1()" disabled="disabled"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a></td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWtQ" name ="ctsFromWt" ng-model="cts.ctsFromWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWtQ" name ="ctsToWt" ng-model="cts.ctsToWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRateQ" name ="ctsRate" ng-model="cts.ctsRate"  step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>Additional Rate *</td>
				<td><input type ="number" id="ctsAdditionalRate" name ="ctsAdditionalRate" ng-model="cts.ctsAdditionalRate" step="0.00001" required disabled="disabled"></td>
			</tr>
			
			<tr>
				<td> <div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
					<input class="validate" type ="number" id="ctsToLoad" name ="ctsToLoad" ng-model="cts.ctsToLoad" ng-disabled="!checkLoad"  step="0.00001" min="0.00001"  ng-blur="setLoadFlagTrue(cts)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div></td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad" ng-change="saveTncUnload()">
					<input class="validate" type ="number" id="ctsToUnLoad" name ="ctsToUnLoad" ng-model="cts.ctsToUnLoad" ng-disabled="!checkUnLoad"  step="0.00001" min="0.00001"  ng-blur="setUnLoadFlagTrue(cts)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      				<input class="validate" type ="number" id="ctsToTransitDay" name ="ctsToTransitDay" ng-model="cts.ctsToTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(cts)" >
					<label for="regContTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">
      				<input class="validate" type ="number" id="ctsToStatChg" name ="ctsToStatChg" ng-model="cts.ctsToStatChg" ng-disabled="!checkStatisticalCharge"  step="0.00001" min="0.00001"  ng-blur="setSCFlagTrue(cts)">
					<label for="regContStatisticalCharge">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
		
	<div id="addStation" ng-hide="addStationFlag">
	<form name="StationForm" ng-submit="saveNewStation(StationForm,station)">
		<table>
			<tr>
				<td>Station Name*</td>
				<td><input id="stnName" type ="text" name ="stnName" ng-model="station.stnName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>Station District*</td>
				<td><input class="validate" id="district" type ="text" name ="stnDistrict" ng-model="station.stnDistrict" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input class="validate" id="code" type ="text" name ="stateCode"  ng-model="station.stateCode" readonly ng-click="openStateCodeDB()" required></td>
			</tr>
			<tr>
				<td>Pin</td>
				<td><input class="validate"  type ="text" id="stnPin" name ="stnPin" ng-model="station.stnPin" ng-minlength="6" ng-maxlength="6" ng-required="true" ></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
		
		
		
 	  <div id ="dlyContBLPMCodeDB" ng-hide = "blpmCodeFlag">
 	  
 	    <input type="text" name="filterBlpm" ng-model="filterBlpm.custCode" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer FaCode</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
			</tr>
 	  
		  
		  <tr ng-repeat="customer in customerList | filter:filterBlpm">
		 	  <td><input type="radio"  name="customerName" class="custCode"  value="{{ customer }}" ng-model="blpmCode" ng-click="savBLPMCode(customer)"></td>
              <td>{{ customer.custFaCode }}</td>
              <td>{{ customer.custName }}</td>
              <td>{{ customer.custPin }}</td>
              <td>{{ customer.custIsDailyContAllow }}</td>
          </tr>
      </table>   
	</div>

	<div id ="dlyContCngrCodeDB" ng-hide="CngrCodeFlag">
	
	<input type="text" name="filterCngr" ng-model="filterCngr.custCode" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer Code</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="cngrCode in customerList  | filter:filterCngr">
		 	  <td><input type="radio"  name="cngrCodeName"  class="custCode"  value="{{ cngrCode }}" ng-model="custCode1" ng-click="savCngrCode(cngrCode)"></td>
              <td>{{ cngrCode.custCode }}</td>
              <td>{{ cngrCode.custName }}</td>
              <td>{{ cngrCode.custPin }}</td>
               <td>{{cngrCode.custIsDailyContAllow }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchCodeDB" ng-hide = "branchCodeFlag">
	
	<input type="text" name="filterBranch" ng-model="filterBranch.branchCode" placeholder="Search by Branch Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
			</tr>
 	 
		  
		  <tr ng-repeat="branch in branchList  | filter:filterBranch">
		 	  <td><input type="radio"  name="branchCode" class="branchCode"  value="{{ branch}}" ng-model="branchCode" ng-click="savBranchCode(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
          </tr>
         </table>
         
	</div>
	
	
	<div id ="dlyContFromStationDB" ng-hide = "FromStationFlag">
	
	<input type="text" name="filterFromStn" ng-model="filterFromStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  <tr ng-repeat="fromStation in stationList  | filter:filterFromStn">
		 	  <td><input type="radio"  name="fromStationName"  class="stnName"  value="{{ fromStation }}" ng-model="stnName"  ng-click="savFrmStnCode(fromStation)"></td>
              <td>{{ fromStation.stnCode }}</td>
              <td>{{ fromStation.stnName }}</td>
              <td>{{ fromStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="dlyContToStationDB" ng-hide = "ToStationFlag">
	
	<input type="text" name="filterToStn" ng-model="filterToStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="toStation in stationList  | filter:filterToStn">
		 	  <td><input type="radio"  name="toStationName"  class="stnName"  value="{{toStation}}" ng-model="stnName1" ng-click="savToStnCode(toStation)"></td>
              <td>{{ toStation.stnCode }}</td>
              <td>{{ toStation.stnName }}</td>
              <td>{{ toStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="dlyContCrNameDB" ng-hide = "CrNameFlag">
	
	<input type="text" name="filterCr" ng-model="filterCr.custRepCode" placeholder="Search by CR Code">
 	  
 	   	  <table>
 	 		<tr>
 	 			<th></th>
 	 			
 	 			<th>CR Code</th>
 	 			
				<th>Customer Name</th>
				
				<th>CR Name</th>
			
				<th>CR Designation</th>
			</tr>
 	 
		  
		  <tr ng-repeat="cr in list  | filter:filterCr">
		 	  <td><input type="radio"  name="crName"  class="custRepCode"  value="{{cr }}" ng-model="custRepCode" ng-click="savCrName(cr)"></td>
              <td>{{ cr.custRepCode }}</td>
              <td>{{ cr.custName }}</td>
              <td>{{ cr.custRepName }}</td>
              <td>{{ cr.custRepDesig }}</td>
          </tr>
         </table>
    
         
	</div>
	
	<div id ="ctsVehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		  <tr ng-repeat="VT in vtList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="VTName" class="vtCode"  value="{{ VT }}" ng-model="vtCode" ng-click="savVehicleType(VT)"></td>
              <td>{{VT.vtCode}}</td>
              <td>{{VT.vtVehicleType}}</td>
              <td>{{VT.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="productTypeDB" ng-hide = "ProductTypeFlag"> 
	
	<input type="text" name="filterProduct" ng-model="filterProduct" placeholder="Search by Product Name">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>Product Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="pt in ptList  | filter:filterProduct">
		 	  <td><input type="radio"  name="ptName" class="ptName"  value="{{ pt }}" ng-model="ptName" ng-click="savProductName(pt)"></td>
              <td>{{ pt }}</td>
          </tr>
         </table>
   </div>

	<div id ="rbkmFromStationDB" ng-hide = "rbkmFromStationFlag">
 	  
 	  <input type="text" name="filterRbkmFStn" ng-model="filterRbkmFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmFStation in stationList  | filter:filterRbkmFStn">
		 	  <td><input type="radio"  name="rbkmFStationName"  class="station"  value="{{rbkmFStation}}" ng-model="stnCode2" ng-click="savRbkmFromStnCode(rbkmFStation)"></td>
              	<td>{{ rbkmFStation.stnCode }}</td>
              	<td>{{ rbkmFStation.stnName }}</td>
              	<td>{{ rbkmFStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmToStationDB" ng-hide = "rbkmToStationFlag">
	
	<input type="text" name="filterRbkmTStn" ng-model="filterRbkmTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmTStation in stationList  | filter:filterRbkmTStn">
		 	  <td><input type="radio"  name="rbkmTStationName" value="{{rbkmTStation}}" ng-model="stnCode3" ng-click="saveRbkmToStnCode(rbkmTStation)"></td>
          		<td>{{ rbkmTStation.stnCode }}</td>
              	<td>{{ rbkmTStation.stnName }}</td>
              	<td>{{ rbkmTStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmStateCodeDB" ng-hide = "rbkmStateCodeFlag">
	
	<input type="text" name="filterRbkmState" ng-model="filterRbkmState.stateCode" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
				<th>State Name</th>
			</tr>
 	 	<tr ng-repeat="state in listState  | filter:filterRbkmState">
		 	  <td><input type="radio"  name="state"  value="{{ state }}" ng-model="stateCode" ng-click="saveStateCode(state)"></td>
               <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmVehicleTypeDB" ng-hide = "rbkmVehicleTypeFlag">
	
	<input type="text" name="filterRbkmVehicle" ng-model="filterRbkmVehicle.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			
				
			</tr>
 	  
		  
		 <tr ng-repeat="rbkmVType in vtList  | filter:filterRbkmVehicle">
		 	  <td><input type="radio"  name="rbkmVTypeName"  value="{{rbkmVType}}" ng-model="rbkmVtCode" ng-click="savRbkmVehicleType(rbkmVType)"></td>
              <td>{{ rbkmVType.vtCode }}</td>
              <td>{{ rbkmVType.vtVehicleType }}</td>
              <td>{{rbkmVType.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="PenaltyFromStationDB" ng-hide = "PenaltyFromStationFlag">
	
	<input type="text" name="filterPenaltyFStn" ng-model="filterPenaltyFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="pFromstation in stationList  | filter:filterPenaltyFStn">
		 	  <td><input type="radio"  name="pFromstationName" class="station"  value="{{ pFromstation }}" ng-model="PFStnCode" ng-click="savPenaltyFromStn(pFromstation)"></td>
              <td>{{ pFromstation.stnCode }}</td>
              <td>{{ pFromstation.stnName }}</td>
              <td>{{ pFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="PenaltyToStationDB" ng-hide = "PenaltyToStationFlag">
	
	<input type="text" name="filterPenaltyTStn" ng-model="filterPenaltyTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				<th>Station Name</th>
				
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="pTostation in stationList  | filter:filterPenaltyTStn">
		 	  <td><input type="radio"  name="pTostationName"   value="{{ pTostation }}" ng-model="PTStnCode" ng-click="savPenaltyToStn(pTostation)"></td>
              <td>{{ pTostation.stnCode }}</td>
              <td>{{ pTostation.stnName }}</td>
              <td>{{ pTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	

	<div id ="PenaltyVehicleTypeDB" ng-hide = "PenaltyVehicleTypeFlag">
	
	<input type="text" name="filterPenaltyVehicle" ng-model="filterPenaltyVehicle.vtCode" placeholder="Search by Vehicle Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
				
			</tr>
 	  
		  
		 <tr ng-repeat="vtPenalty in vtList  | filter:filterPenaltyVehicle">
		 	  <td><input type="radio"  name="vtPenaltyName" class="vtCode"  value="{{vtPenalty}}" ng-model="pVtCode" ng-click="savPenaltyVehicleType(vtPenalty)"></td>
              <td>{{ vtPenalty.vtCode }}</td>
              <td>{{ vtPenalty.vtVehicleType }}</td>
              <td>{{vtPenalty.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
		<div id ="BonusFromStationDB" ng-hide = "BonusFromStationFlag">
		
		<input type="text" name="filterBonusFStn" ng-model="filterBonusFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bFromstation in stationList  | filter:filterBonusFStn">
		 	  <td><input type="radio"  name="bFromstationName" class="station"  value="{{bFromstation }}" ng-model="BFStnCode" ng-click="savBonusFromStn(bFromstation)"></td>
              <td>{{ bFromstation.stnCode }}</td>
              <td>{{ bFromstation.stnName }}</td>
              <td>{{ bFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusToStationDB" ng-hide = "BonusToStationFlag">
	
	<input type="text" name="filterBonusTStn" ng-model="filterBonusTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bTostation in stationList  | filter:filterBonusTStn">
		 	  <td><input type="radio"  name="bTostationName"   value="{{ bTostation }}" ng-model="BTStnCode" ng-click="savBonusToStn(bTostation)"></td>
              <td>{{ bTostation.stnCode }}</td>
              <td>{{ bTostation.stnName }}</td>
              <td>{{ bTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusVehicleTypeDB" ng-hide = "BonusVehicleTypeFlag">
	
	<input type="text" name="filterBonusVehicle" ng-model="filterBonusVehicle.vtCode" placeholder="Search by Vehicle Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
			
				<th>Service Type</th>
				
			</tr>
 	  
		  
		 <tr ng-repeat="vtBonus in vtList  | filter:filterBonusVehicle">
		 	  <td><input type="radio"  name="vtBonusName" class="vtCode"  value="{{ vtBonus }}" ng-model="BVtCode" ng-click="savBonusVehicleType(vtBonus)"></td>
              <td>{{ vtBonus.vtCode }}</td>
              <td>{{ vtBonus.vtVehicleType }}</td>
              <td>{{vtBonus.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
			<div id ="DetentionFromStationDB" ng-hide = "DetentionFromStationFlag">
			
			<input type="text" name="filterDetentionFStn" ng-model="filterDetentionFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="dFromstation in stationList  | filter:filterDetentionFStn">
		 	  <td><input type="radio"  name="dFromstationName"  class="station"  value="{{ dFromstation }}" ng-model="DFStnCode" ng-click="savDetentionFromStn(dFromstation)"></td>
             <td>{{ dFromstation.stnCode }}</td>
              <td>{{ dFromstation.stnName }}</td>
              <td>{{ dFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionToStationDB" ng-hide = "DetentionToStationFlag">
	
	<input type="text" name="filterDetentionTStn" ng-model="filterDetentionTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="dTostation in stationList  | filter:filterDetentionTStn">
		 	  <td><input type="radio"  name="dTostationName"  value="{{ dTostation }}" ng-model="DTStnCode" ng-click="savDetentionToStn(dTostation)"></td>
             <td>{{ dTostation.stnCode }}</td>
              <td>{{ dTostation.stnName }}</td>
              <td>{{ dTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionVehicleTypeDB" ng-hide = "DetentionVehicleTypeFlag">
	
	<input type="text" name="filterDetentionVehicle" ng-model="filterDetentionVehicle.vtCode" placeholder="Search by Vehicle Code">
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
			
				<th>Service Type</th>
			</tr>
		 <tr ng-repeat="vtDetention in vtList  | filter:filterDetentionVehicle">
		 	  <td><input type="radio"  name="vtDetentionName" class="vtCode"  value="{{ vtDetention }}" ng-model="DVtCode" ng-click="savDetentionVehicleType(vtDetention)"></td>
              <td>{{ vtDetention.vtCode }}</td>
              <td>{{ vtDetention.vtVehicleType }}</td>
              <td>{{vtDetention.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="ToStationsDB" ng-hide ="ToStationsDBFlag">
 	  	<input type="text" name="filterToStns" ng-model="filterToStns.stnCode" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="toStations in stationList | filter:filterToStns">
		 	  <td><input type="radio"  name="toStations"  class="station"  value="{{ toStations }}" ng-model="toStns" ng-click="saveToStations(toStations)"></td>
             <td>{{ toStations.stnCode }}</td>
              <td>{{ toStations.stnName }}</td>
              <td>{{ toStations.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="StateCodeDB" ng-hide="StateCodeFlag">
	
	<input type="text" name="filterState" ng-model="filterState.stateCode" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
			</tr>
 	 	<tr ng-repeat="statecode in listState  | filter:filterState">
		 	  <td><input type="radio"  name="statecode"  value="{{ statecode }}" ng-model="stateCode1" ng-click="savStateCode(statecode)"></td>
               <td>{{ statecode.stateCode }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id="dlyContDB" ng-hide = "dlyContFlag">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div> 
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<table class="table-hover table-bordered table-bordered">
		<tr>
			<td><h3>Daily Contract Details</h3></td>
		</tr>
		<tr>
			<td>BLPM Code</td>
			<td>{{dlyCnt.dlyContBLPMCode}}</td>
		</tr>
		<tr>
			<td>Consignor Code</td>
			<td>{{dlyCnt.dlyContCngrCode}}</td>
		</tr>
		<tr>
			<td>CR Name</td>
			<td>{{dlyCnt.dlyContCrNameTemp}}</td>
		</tr>
		<tr>
			<td>From Station</td>
			<td>{{dlyCnt.dlyContFromStation}}</td>
		</tr>
		<tr ng-show="dlyCnt.dlyContToStation">
			<td>To Station</td>
			<td>{{dlyCnt.dlyContToStation}}</td>
		</tr>
		<tr>
			<td>Start Date</td>
			<td>{{dlyCnt.dlyContStartDt}}</td>
		</tr>
		<tr>
			<td>End Date</td>
			<td>{{dlyCnt.dlyContEndDt}}</td>
		</tr>
		
		<tr>
			<td>DC</td>
			<td>{{dlyCnt.dlyContDc}}</td>
		</tr>
		<tr>
			<td>Cost Grade</td>
			<td>{{dlyCnt.dlyContCostGrade}}</td>
		</tr>
		<tr>
			<td>Hour/Day</td>
			<td>{{dlyCnt.dlyContHourOrDay}}</td>
		</tr>
		<tr>
			<td>DDL</td>
			<td>{{dlyCnt.dlyContDdl}}</td>
		</tr>
		<tr ng-show="dlyCnt.dlyContAdditionalRate">
			<td>Additional Rate</td>
			<td>{{dlyCnt.dlyContAdditionalRate}}</td>
		</tr>
		<!-- <tr>
			<td>Product Type</td>
			<td>{{dlyCnt.dlyContProductType}}</td>
		</tr> -->
	
		<tr ng-show="dlyCnt.dlyContLoad">
			<td>Load</td>
			<td>{{dlyCnt.dlyContLoad}}</td>
		</tr>
		<tr ng-show="dlyCnt.dlyContUnLoad">
			<td>Unload</td>
			<td>{{dlyCnt.dlyContUnLoad}}</td>
		</tr>
		<tr ng-show="dlyCnt.dlyContTransitDay">
			<td>Transit Day</td>
			<td>{{dlyCnt.dlyContTransitDay}}</td>
		</tr>
		<tr ng-show="dlyCnt.dlyContStatisticalCharge">
			<td>Statistical Charge</td>
			<td>{{dlyCnt.dlyContStatisticalCharge}}</td>
		</tr>
		
		<tr ng-show="dlyCnt.dlyContRemark">
			<td>Remarks</td>
			<td>{{dlyCnt.dlyContRemark}}</td>
		</tr> 
	</table>
	
		<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		     <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmLoad}}</td>
		            <td>{{rbkm.rbkmUnLoad}}</td>
		            <td>{{rbkm.rbkmTransitDay}}</td>
		            <td>{{rbkm.rbkmStatChg}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
		
	
		<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
			<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
			<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	 <tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		             <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Rate</th>
		            <th>Additional Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsToLoad}}</td>
		            <td>{{cts.ctsToUnLoad}}</td>
		            <td>{{cts.ctsToTransitDay}}</td>
		            <td>{{cts.ctsToStatChg}}</td>
		          	<td>{{cts.ctsRate}}</td>
		          	<td>{{cts.ctsAdditionalRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
   		
   		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		             <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsToLoad}}</td>
		            <td>{{cts.ctsToUnLoad}}</td>
		            <td>{{cts.ctsToTransitDay}}</td>
		            <td>{{cts.ctsToStatChg}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		<input type="button" value="Cancel" ng-click="back()">
		<input type="button" value="Save" id="saveBtnId" ng-click="saveContract(dlyCnt,tnc,metricType)">
	</div>
	</div>
	
	<div id="showFaCodeId" ng-hide="showFaCode">
		<table>
			<tr>
				<td>DailyContract Code : </td>
				<td>{{ dlyFaCode }}</td>
			</tr>
			<tr>
				<td>
					<input type="button" value="OK" ng-click="exit()"/>
				</td>
			</tr>
		</table>
	</div>
</div>