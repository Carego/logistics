package com.mylogistics.services.bank;

import com.mylogistics.model.Address;
import com.mylogistics.model.Employee;
import com.mylogistics.model.bank.BankMstr;

public class BankMstrService {

	private Address bnkAdd;
	
	private Employee bnkEmpCode1;
	
	private Employee bnkEmpCode2;
	
	private BankMstr bankMstr;

	public Address getBnkAdd() {
		return bnkAdd;
	}

	public void setBnkAdd(Address bnkAdd) {
		this.bnkAdd = bnkAdd;
	}

	public Employee getBnkEmpCode1() {
		return bnkEmpCode1;
	}

	public void setBnkEmpCode1(Employee bnkEmpCode1) {
		this.bnkEmpCode1 = bnkEmpCode1;
	}

	public Employee getBnkEmpCode2() {
		return bnkEmpCode2;
	}

	public void setBnkEmpCode2(Employee bnkEmpCode2) {
		this.bnkEmpCode2 = bnkEmpCode2;
	}

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}
}
