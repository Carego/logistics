package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ContToStnService;
import com.mylogistics.services.ContToStnServiceImpl;
import com.mylogistics.services.PBDService;
import com.mylogistics.services.PBDServiceImpl;

@Controller
public class AddContToStnCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;
	
	private PBDService pbdService = new PBDServiceImpl();
	
	private ContToStnService contToStnService = new ContToStnServiceImpl();

	
	@RequestMapping(value = "/getCCFrToStn", method = RequestMethod.POST)
	public @ResponseBody Object getCCFrToStn() {
		System.out.println("enter into getCCFrToStn function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> finalList = new ArrayList<String>();
		List<String> regFaList = regularContractDAO.getRegFaCode();
		List<String> dlyFaList = dailyContractDAO.getDlyFaCode();
		List<VehicleType> vehList = vehicleTypeDAO.getVehicleType();
		List<Station> stnList = stationDAO.getStationData();
		if(!regFaList.isEmpty()){
			for(int i=0;i<regFaList.size();i++){
				finalList.add(regFaList.get(i));
			}
		}

		if(!dlyFaList.isEmpty()){
			for(int i=0;i<dlyFaList.size();i++){
				finalList.add(dlyFaList.get(i));
			}
		}
		
		if(!finalList.isEmpty()){
			map.put("list",finalList);
			map.put("vehList",vehList);
			map.put("stnList",stnList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getToStnFrChng", method = RequestMethod.POST)
	public @ResponseBody Object getToStnFrChng(@RequestBody String contFaCode)throws Exception {
		System.out.println("enter into getCCFrToStn function");
		Map<String,Object> map = new HashMap<String,Object>();
		contToStnService.deleteAllCTS();
		pbdService.deleteAllPBD();
		if(contFaCode != null){
			String subStr = contFaCode.substring(0,2);
			System.out.println("subStrFaCode="+subStr);
			if(subStr.equalsIgnoreCase("05")){
				List<RegularContract> regList = regularContractDAO.getRegContByFaCode(contFaCode);
				List<Customer> custList = customerDAO.getCustomer(regList.get(0).getRegContBLPMCode());
				String branchName = branchDAO.getBrNameByBrCode(regList.get(0).getBranchCode());
				List<Station> stnList = stationDAO.retrieveStation(regList.get(0).getRegContFromStation());
				
				Map<String,Object> contract = new HashMap<String,Object>();
				contract.put("contFaCode",regList.get(0).getRegContFaCode());
				contract.put("contCode",regList.get(0).getRegContCode());
				if(!custList.isEmpty()){
					contract.put("custName",custList.get(0).getCustName());
					contract.put("custFaCode",custList.get(0).getCustFaCode());
				}
				contract.put("contBB",regList.get(0).getRegContBillBasis());
				contract.put("contBranch",branchName);
				contract.put("contFrStn",stnList.get(0).getStnName());
				contract.put("contFrStnCode",regList.get(0).getRegContFromStation());
				contract.put("contFrDt",regList.get(0).getRegContFromDt());
				contract.put("contToDt",regList.get(0).getRegContToDt());
				contract.put("contType",regList.get(0).getRegContType());
				contract.put("contValue",regList.get(0).getRegContValue());
				contract.put("contWeight",regList.get(0).getRegContWt());
				contract.put("contDDL",regList.get(0).getRegContDdl());
				contract.put("contInsBy",regList.get(0).getRegContInsuredBy());
				contract.put("contRemarks",regList.get(0).getRegContRemark());
				
				
				if(!regList.isEmpty()){
					java.util.Date utilDate=new java.util.Date();
					Date sqlDate = new Date(utilDate.getTime());
					List<ContToStn> contToList = contToStnDAO.getContToStn(regList.get(0).getRegContCode(),sqlDate);
					if(!contToList.isEmpty()){
						List<Map<String,Object>> ctsList = new ArrayList<>();
						for(int i=0;i<contToList.size();i++){
					
							Map<String,Object> ctsMap = new HashMap<String,Object>();
							List<Station> stnList1 = stationDAO.retrieveStation(contToList.get(i).getCtsToStn());
							System.out.println("size of List<Station> stnList1  "+i+" = "+stnList1.size());
							ctsMap.put("ctsId",contToList.get(i).getCtsId());
							ctsMap.put("frStn",stnList.get(0).getStnName());
							ctsMap.put("frStnCode",regList.get(0).getRegContFromStation());
							ctsMap.put("toStn",stnList1.get(0).getStnName());
							ctsMap.put("toStnCode",contToList.get(i).getCtsToStn());
							ctsMap.put("ctsFrDt",contToList.get(i).getCtsFrDt());
							ctsMap.put("ctsToDt",contToList.get(i).getCtsToDt());
							ctsMap.put("vehType",contToList.get(i).getCtsVehicleType());
							ctsMap.put("frWt",contToList.get(i).getCtsFromWt());
							ctsMap.put("toWt",contToList.get(i).getCtsToWt());
							ctsMap.put("rate",contToList.get(i).getCtsRate());
							
							ctsList.add(ctsMap);
						}
						
						
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						map.put("cotract",contract);
						map.put("list",ctsList);
					}else{
						map.put("msg","There is no to station avaliable for this contract");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					System.out.println("invalid regular contract facode");
					map.put("msg","invalid regular contract facode");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else if(subStr.equalsIgnoreCase("04")){
				List<DailyContract> dlyList = dailyContractDAO.getDlyContByFaCode(contFaCode);
				List<Customer> custList = customerDAO.getCustomer(dlyList.get(0).getDlyContBLPMCode());
				String branchName = branchDAO.getBrNameByBrCode(dlyList.get(0).getBranchCode());
				List<Station> stnList = stationDAO.retrieveStation(dlyList.get(0).getDlyContFromStation());
				
				Map<String,Object> contract = new HashMap<String,Object>();
				contract.put("contFaCode",dlyList.get(0).getDlyContFaCode());
				contract.put("contCode",dlyList.get(0).getDlyContCode());
				if(!custList.isEmpty()){
					contract.put("custName",custList.get(0).getCustName());
					contract.put("custFaCode",custList.get(0).getCustFaCode());
				}
				contract.put("contBB",dlyList.get(0).getDlyContBillBasis());
				contract.put("contBranch",branchName);
				contract.put("contFrStn",stnList.get(0).getStnName());
				contract.put("contFrStnCode",dlyList.get(0).getDlyContFromStation());
				contract.put("contFrDt",dlyList.get(0).getDlyContStartDt());
				contract.put("contToDt",dlyList.get(0).getDlyContEndDt());
				contract.put("contType",dlyList.get(0).getDlyContType());
				//contract.put("contValue",dlyList.get(0).getDlyContValue());
				//contract.put("contWeight",regList.get(0).getRegContWt());
				contract.put("contDDL",dlyList.get(0).getDlyContDdl());
				//contract.put("contInsBy",dlyList.get(0).getDlyContInsuredBy());
				contract.put("contRemarks",dlyList.get(0).getDlyContRemark());
				
				if(!dlyList.isEmpty()){
					List<ContToStn> contToList = contToStnDAO.getContToStn(dlyList.get(0).getDlyContCode());
					if(!contToList.isEmpty()){
						List<Map<String,Object>> ctsList = new ArrayList<>();
						for(int i=0;i<contToList.size();i++){
							Map<String,Object> ctsMap = new HashMap<String,Object>();
							List<Station> stnList1 = stationDAO.retrieveStation(contToList.get(i).getCtsToStn());
							ctsMap.put("ctsId",contToList.get(i).getCtsId());
							ctsMap.put("frStn",stnList.get(0).getStnName());
							ctsMap.put("frStnCode",dlyList.get(0).getDlyContFromStation());
							ctsMap.put("toStn",stnList1.get(0).getStnName());
							ctsMap.put("toStnCode",contToList.get(i).getCtsToStn());
							ctsMap.put("ctsFrDt",contToList.get(i).getCtsFrDt());
							ctsMap.put("ctsToDt",contToList.get(i).getCtsToDt());
							ctsMap.put("vehType",contToList.get(i).getCtsVehicleType());
							ctsMap.put("frWt",contToList.get(i).getCtsFromWt());
							ctsMap.put("toWt",contToList.get(i).getCtsToWt());
							ctsMap.put("rate",contToList.get(i).getCtsRate());
							
							ctsList.add(ctsMap);
						}
						
						
						
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						map.put("cotract",contract);
						map.put("list",ctsList);
					}else{
						map.put("msg","There is no to station avaliable for this contract");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					System.out.println("invalid daily contract facode");
					map.put("msg","invalid daily contract facode");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				System.out.println("invalid contract code");
				map.put("msg","invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","contract code is not valid");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value="/fetchPbdListForACTS",method = RequestMethod.GET)
	public @ResponseBody Object fetchPbdListForACTS(){
		
		List<PenaltyBonusDetention> pList = pbdService.getAllPBD();
	
		Map<String,Object> map = new HashMap<String,Object>();
		if(!pList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", pList);	
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value="/addPbdForACTS",method = RequestMethod.POST)
	public @ResponseBody Object addPbdForACTS(@RequestBody PenaltyBonusDetention penaltyBonusDetention){
		pbdService.addPBD(penaltyBonusDetention);
	
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}

	@RequestMapping(value="/removePbdForACTS",method = RequestMethod.POST)
	public @ResponseBody Object removePbdForACTS(@RequestBody PenaltyBonusDetention penaltyBonusDetention){
		pbdService.deletePBD(penaltyBonusDetention);

		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}

	@RequestMapping(value="/removeAllPbdForACTS",method = RequestMethod.POST)
	public @ResponseBody Object removeAllPbdForACTS(){
		pbdService.deleteAllPBD();
	
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}	
	

	@RequestMapping(value = "/fetchCTSListForACTS", method = RequestMethod.GET)
	public @ResponseBody Object fetchCTSListForACTS() {
		System.out.println("enter into fetchCTSListForRC function");

		List<ContToStn> cList = contToStnService.getAllCTS();

		Map<String, Object> map = new HashMap<String, Object>();
		if(!cList.isEmpty()){
			map.put("list", cList);	
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put("list", cList);	
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/addCTSForACTS", method = RequestMethod.POST)
	public @ResponseBody Object addCTSForACTS(@RequestBody ContToStn contToStn) {

		System.out.println("enter into addCTSForRC function");
		System.out.println("contToStn.ctsFrDt = "+contToStn.getCtsFrDt());
		contToStnService.addCTS(contToStn);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeCTSForACTS", method = RequestMethod.POST)
	public @ResponseBody Object removeCTSForACTS(@RequestBody ContToStn contToStn) {

		System.out.println("enter into removeCTSForRC function");
		contToStnService.deleteCTS(contToStn);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}

	@RequestMapping(value = "/removeAllCTSForACTS", method = RequestMethod.POST)
	public @ResponseBody Object removeAllCTSForACTS() {

		System.out.println("enter into removeAllCTSForRC function");
		contToStnService.deleteAllCTS();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	
	@RequestMapping(value = "/saveCtsFrACTS", method = RequestMethod.POST)
	public @ResponseBody Object saveCtsFrACTS(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into saveCtsFrACTS function");
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			User currentUser = (User)httpSession.getAttribute("currentUser");
			List<Map<String,Object>> chgRateList = new ArrayList<>();
			String contCode = (String) clientMap.get("contCode");
			String billBase = (String) clientMap.get("billBasis");
			System.out.println("billBase ====> "+billBase);
			System.out.println("(String) clientMap.get(rentDt) = "+(String) clientMap.get("renDt"));
			String rentDtStr = (String) clientMap.get("renDt");
			String productTypeName=(String) clientMap.get("ptName");
			
			if(rentDtStr != null && !rentDtStr.equalsIgnoreCase("")){
				Date renDt = Date.valueOf((String) clientMap.get("renDt"));
				System.out.println("renDt = "+renDt);
				chgRateList = (List<Map<String, Object>>) clientMap.get("chgRateList");
				System.out.println("chgRateList = "+chgRateList.size());
			}else{
				System.out.println("***********************");
			}
			    
			System.out.println("contCode = "+contCode);
			
			if(!chgRateList.isEmpty()){
				for(int i=0;i<chgRateList.size();i++){
					System.out.println("chgRateList["+i+"].ctsId = "+chgRateList.get(i).get("ctsId"));
					System.out.println("chgRateList["+i+"].newRate = "+chgRateList.get(i).get("newRate"));
				}
			}
			
			
			if(contCode != null){
				
				if(billBase != null){
					int res = 0;
					if(contCode.substring(0,3).equalsIgnoreCase("reg")){
						System.out.println("************** 1");
						res = regularContractDAO.updateBillBase(contCode , billBase);
					}else{
						System.out.println("************** 2");
						res = dailyContractDAO.updateBillBase(contCode , billBase);
					}
				}
				
				List<PenaltyBonusDetention> pbdList = pbdService.getAllPBD();
				List<ContToStn> cList = contToStnService.getAllCTS();
				
				if(rentDtStr != null && !rentDtStr.equalsIgnoreCase("")){
					Date renDt = Date.valueOf((String) clientMap.get("renDt"));
					if(!chgRateList.isEmpty()){
						int res = 0;
						if(contCode.substring(0,3).equalsIgnoreCase("reg")){
							res = contToStnDAO.chgStnRate(renDt,chgRateList);
						}else{
							res = contToStnDAO.chgDlyStnRate(renDt,chgRateList,contCode);
						}
						
						if(res > 0){
							System.out.println("successfully save the chgRateList");
						}else{
							System.out.println("error in saving chgRateList");
						}
					}
				}
				
				if(!pbdList.isEmpty()){
					for (int i = 0; i < pbdList.size(); i++) {
						pbdList.get(i).setPbdContCode(contCode);
						pbdList.get(i).setbCode(currentUser.getUserBranchCode());
						pbdList.get(i).setUserCode(currentUser.getUserCode());
						int temp = penaltyBonusDetentionDAO.savePBD(pbdList.get(i));
					}
				}
				
				if(!cList.isEmpty()){
					for(int i = 0; i < cList.size();i++) {
						double ctsRate=0;
						double ctsFromWt=0;
						double ctsToWt=0;
						
						ctsRate = cList.get(i).getCtsRate();
						ctsRate = ctsRate/ConstantsValues.kgInTon;
						cList.get(i).setCtsRate(ctsRate);
						
						ctsFromWt = cList.get(i).getCtsFromWt();
						ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
						cList.get(i).setCtsFromWt(ctsFromWt);
						
						ctsToWt = cList.get(i).getCtsToWt();
						ctsToWt = ctsToWt*ConstantsValues.kgInTon;
						cList.get(i).setCtsToWt(ctsToWt);
						
						cList.get(i).setCtsProductType(productTypeName);
						
						if(cList.get(i).getCtsProductType() == null || cList.get(i).getCtsProductType() == ""){
							cList.get(i).setCtsProductType("-100");
						}
						
						System.out.println("Product Type ="+productTypeName);
						cList.get(i).setCtsContCode(contCode);
						cList.get(i).setbCode(currentUser.getUserBranchCode());
						cList.get(i).setUserCode(currentUser.getUserCode());
						int temp=contToStnDAO.saveContToStn(cList.get(i));
						
					}
				}
				pbdService.deleteAllPBD();
				contToStnService.deleteAllCTS();
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put("msg","Invalid Contract Code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		//map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value="/updateToWt", method=RequestMethod.POST)
	public @ResponseBody Object updateToWt(@RequestBody ContToStn contToStn){
		System.out.println("AddContToStnCntlr.updateToWt()");
		Map<String, Object> map = new HashMap<>();
		//System.out.println("Extended Date ="+contToStn.getCtsToDt());
		int temp = contToStnDAO.updateToWt(contToStn);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
}
