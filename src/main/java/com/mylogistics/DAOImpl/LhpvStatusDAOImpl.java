package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.LhpvAdvCNTS;
import com.mylogistics.constants.LhpvBalCNTS;
import com.mylogistics.constants.LhpvCashSmryCNTS;
import com.mylogistics.constants.LhpvStatusCNTS;
import com.mylogistics.constants.LhpvSupCNTS;
import com.mylogistics.constants.MoneyReceiptCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvCashSmry;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.model.lhpv.LhpvSup;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

public class LhpvStatusDAOImpl implements LhpvStatusDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private HttpSession httSession;
	
	
	public static Logger logger = Logger.getLogger(LhpvStatusDAOImpl.class);

	@Autowired
	public LhpvStatusDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public LhpvStatus getLastLhpvStatus(String bCode) {
		System.out.println("enter into getLastLhpvStatus function");
		List<LhpvStatus> lhstList = new ArrayList<LhpvStatus>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Query query = session
					.createQuery("from LhpvStatus where bCode= :bCode and lsBackDt= :lsBackDt and lsDt >= '2017-11-01' order by lsId DESC");
			query.setString("bCode", bCode);
			query.setBoolean("lsBackDt", false);
			lhstList = query.setMaxResults(1).list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		if (lhstList.isEmpty()) {
			return null;
		} else {
			return lhstList.get(0);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public LhpvStatus getLastLhpvStatus(String bCode,Session session) {
		System.out.println("enter into getLastLhpvStatus function");
		List<LhpvStatus> lhstList = new ArrayList<LhpvStatus>();
			Query query = session
					.createQuery("from LhpvStatus where bCode= :bCode and lsBackDt= :lsBackDt and lsDt >= '2017-11-01' order by lsId DESC");
			query.setString("bCode", bCode);
			query.setBoolean("lsBackDt", false);
			lhstList = query.setMaxResults(1).list();
			session.flush();
		if (lhstList.isEmpty()) {
			return null;
		} else {
			return lhstList.get(0);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveLhpvAdv(VoucherService voucherService) {
		System.out.println("enter into saveLhpvAdv function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvAdv> lhpvAdvList = voucherService.getLhpvAdvList();
		System.out.println("size of lhpvAdvList = " + lhpvAdvList.size());
		int lhpvAdvNo = 0;
		Session session=null;
		Transaction transaction=null;
		if (!lhpvAdvList.isEmpty()) {
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
						.getTime());
				Criteria cr = session.createCriteria(LhpvAdv.class);
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_DT, sqlDate));
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_BCODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("laNo"));
				cr.setProjection(proList);

				List<Integer> lhpvAdvNoList = cr.list();

				if (!lhpvAdvNoList.isEmpty()) {
					int laNo = lhpvAdvNoList.get(lhpvAdvNoList.size() - 1);
					lhpvAdvNo = laNo + 1;
				} else {
					lhpvAdvNo = 1;
				}

				char payBy = voucherService.getPayBy();
				String payToStf =  voucherService.getPayToStf();
				String stafCode = voucherService.getStafCode();
				String loryCode = voucherService.getLoryCode();
				System.out.println("value of payBy = " + payBy);
				System.out.println("value of payTo = " + payToStf);
				System.out.println("value of stafCode = " + stafCode);
				double amount = 0;
				System.out.println("Paid Amount is=" + amount);

				if (payBy == 'C') {
					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

					}
				} else if (payBy == 'Q') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBankCode(voucherService.getBankCode());
						lhpvAdv.setLaChqType(voucherService.getChequeType());
						lhpvAdv.setLaChqNo(voucherService.getChequeLeaves()
								.getChqLChqNo());
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvAdv.getLaFinalTot();
					}

					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = " + chequeType);
					String bankCode = voucherService.getBankCode();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();

					if(!bankMstrList.isEmpty()) {
						// It will cancel the cheque if lhpv is reversed
						if (totPayAmt < 0) {
							
							
							List<ChequeLeaves> chqL =new ArrayList<>();
							for(int i=0;i<bankMstrList.get(0).getChequeLeavesList().size();i++){
								if(bankMstrList.get(0).getChequeLeavesList().get(i).getChqLChqNo().equalsIgnoreCase(voucherService.getChequeLeaves().getChqLChqNo()))
									chqL.add(bankMstrList.get(0).getChequeLeavesList().get(i));
							}
							System.out.println("chqId= "+chqL.get(0).getChqLChqNo());

							ChequeLeaves chq = chqL.get(0);
							chq.setChqLChqCancelDt(new Date(new java.util.Date()
									.getTime()));
							chq.setChqLCancel(true);
							
							session.merge(chq);

							
						} else {
							List<ChequeLeaves> chqL = new ArrayList<>();
							ChequeLeaves chequeLeaves = voucherService
									.getChequeLeaves();
							cr = session.createCriteria(ChequeLeaves.class);
							cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
									chequeLeaves.getChqLId()));
							chqL = cr.list();

							ChequeLeaves chq = chqL.get(0);
							chq.setChqLChqAmt(totPayAmt);
							chq.setChqLUsedDt(new Date(new java.util.Date()
									.getTime()));
							chq.setChqLUsed(true);

							session.merge(chq);

						}

						BankMstr bank = bankMstrList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totPayAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = " + balAmt);
						System.out.println("dedAmt = " + dedAmt);
						System.out.println("newBalAmt = " + newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
					}

				} else if (payBy == 'R') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBankCode(voucherService.getBankCode());
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						if (payToStf.equals("S")) {
							System.out.println(payToStf);
							lhpvAdv.setPayToStf(payToStf);
							lhpvAdv.setStafCode(stafCode);
						}else if(payToStf.equals("L")){
							System.out.println(payToStf);
							lhpvAdv.setPayToStf(payToStf);
							lhpvAdv.setStafCode(loryCode);
						}
						
						System.out.println(lhpvAdv.getPayToStf());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvAdv.getLaFinalTot();
					}

					/*
					 * List<BankMstr> bankMstrList = new ArrayList<>(); char
					 * chequeType = voucherService.getChequeType();
					 * System.out.println("chequeType = "+chequeType); String
					 * bankCode = voucherService.getBankCode();
					 * cr=session.createCriteria(BankMstr.class);
					 * cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,
					 * bankCode)); bankMstrList = cr.list();
					 * 
					 * 
					 * if(!bankMstrList.isEmpty()){
					 * 
					 * BankMstr bank = bankMstrList.get(0); double balAmt =
					 * bank.getBnkBalanceAmt(); double dedAmt = totPayAmt;
					 * double newBalAmt = balAmt - dedAmt;
					 * System.out.println("balAmt = "+balAmt);
					 * System.out.println("dedAmt = "+dedAmt);
					 * System.out.println("newBalAmt = "+newBalAmt);
					 * bank.setBnkBalanceAmt(newBalAmt); session.merge(bank); }
					 */

				}else if (payBy == 'P') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBankCode(voucherService.getBankCode());
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						//if (payToStf.equals("S")) {
							System.out.println(voucherService.getPayToPetro());
							System.out.println(voucherService.getCardCode());
							lhpvAdv.setPayToStf(voucherService.getPayToPetro());
							lhpvAdv.setStafCode(voucherService.getCardCode());
						/*}else if(payToStf.equals("L")){
							System.out.println(payToStf);
							lhpvAdv.setPayToStf(payToStf);
							lhpvAdv.setStafCode(loryCode);
						}*/
						
						System.out.println(lhpvAdv.getPayToStf());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvAdv.getLaFinalTot();
					}


				} else {
					System.out.println("invalid payment method");
				}
				session.flush();
				session.clear();
				transaction.commit();

			} catch (Exception e) {
				transaction.rollback();
				e.printStackTrace();
			}

			session.close();
		} else {
			lhpvAdvNo = 0;
		}
		return lhpvAdvNo;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveLhpvAdvTemp(VoucherService voucherService) {
		System.out.println("enter into saveLhpvAdvTemp function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvAdv> lhpvAdvList = voucherService.getLhpvAdvList();
		System.out.println("size of lhpvAdvList = " + lhpvAdvList.size());
		int lhpvAdvNo = 0;
		if (!lhpvAdvList.isEmpty()) {
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
						.getTime());
				Criteria cr = session.createCriteria(LhpvAdv.class);
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_DT, sqlDate));
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_BCODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("laNo"));
				cr.setProjection(proList);

				List<Integer> lhpvAdvNoList = cr.list();

				if (!lhpvAdvNoList.isEmpty()) {
					int laNo = lhpvAdvNoList.get(lhpvAdvNoList.size() - 1);
					lhpvAdvNo = laNo + 1;
				} else {
					lhpvAdvNo = 1;
				}

				char payBy = voucherService.getPayBy();
				System.out.println("value of payBy = " + payBy);

				if (payBy == 'C') {
					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

						LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();
						lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
						lhpvCashSmry.setLaId(laId);
						lhpvCashSmry.setLbId(0);
						lhpvCashSmry.setLcsBrhCode(brFaCode);
						lhpvCashSmry.setLcsAdvEntr(false);
						lhpvCashSmry.setLcsBrkOwn(voucherService
								.getBrkOwnFaCode());
						lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
						lhpvCashSmry.setLcsChlnCode(actChln.getChlnCode());
						lhpvCashSmry.setLcsCsEntr(true);
						lhpvCashSmry.setLcsLhpvDt(lhpvAdv.getLaDt());
						lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
						lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
						lhpvCashSmry.setLcsPayAmt(lhpvAdv.getLaLryAdvP());
						lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
						lhpvCashSmry.setLcsToday(true);
						lhpvCashSmry.setLspId(0);
						lhpvCashSmry.setUserCode(currentUser.getUserCode());

						session.save(lhpvCashSmry);

					}
				} else if (payBy == 'Q') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBankCode(voucherService.getBankCode());
						lhpvAdv.setLaChqType(voucherService.getChequeType());
						lhpvAdv.setLaChqNo(voucherService.getChequeLeaves()
								.getChqLChqNo());
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvAdv.getLaFinalTot();
					}

					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = " + chequeType);
					String bankCode = voucherService.getBankCode();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();

					if (!bankMstrList.isEmpty()) {

						List<ChequeLeaves> chqL = new ArrayList<>();
						ChequeLeaves chequeLeaves = voucherService
								.getChequeLeaves();
						cr = session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
								chequeLeaves.getChqLId()));
						chqL = cr.list();

						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totPayAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date()
								.getTime()));
						chq.setChqLUsed(true);

						session.merge(chq);

						BankMstr bank = bankMstrList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totPayAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = " + balAmt);
						System.out.println("dedAmt = " + dedAmt);
						System.out.println("newBalAmt = " + newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
					}

				} else if (payBy == 'R') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvAdvList.size(); i++) {
						LhpvAdv lhpvAdv = lhpvAdvList.get(i);
						lhpvAdv.setLaDt(sqlDate);
						lhpvAdv.setLaNo(lhpvAdvNo);
						lhpvAdv.setLaPayMethod(payBy);
						lhpvAdv.setLaBrhCode(brFaCode);
						lhpvAdv.setLaBankCode(voucherService.getBankCode());
						lhpvAdv.setLaBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvAdv.setUserCode(currentUser.getUserCode());
						lhpvAdv.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvAdv.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLaList().add(lhpvAdv);
						lhpvAdv.setLhpvStatus(actLhpvSt);
						// actChln.getLhpvAdv().add(lhpvAdv);
						double rem = actChln.getChlnRemAdv();
						actChln.setChlnRemAdv(rem - lhpvAdv.getLaLryAdvP());
						session.merge(actChln);
						lhpvAdv.setChallan(actChln);
						// actChln.setLhpvAdv(lhpvAdv);
						int laId = (Integer) session.save(lhpvAdv);
						System.out.println("laId = " + laId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvAdv.getLaFinalTot();
					}

					/*
					 * List<BankMstr> bankMstrList = new ArrayList<>(); char
					 * chequeType = voucherService.getChequeType();
					 * System.out.println("chequeType = "+chequeType); String
					 * bankCode = voucherService.getBankCode();
					 * cr=session.createCriteria(BankMstr.class);
					 * cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,
					 * bankCode)); bankMstrList = cr.list();
					 * 
					 * 
					 * if(!bankMstrList.isEmpty()){
					 * 
					 * BankMstr bank = bankMstrList.get(0); double balAmt =
					 * bank.getBnkBalanceAmt(); double dedAmt = totPayAmt;
					 * double newBalAmt = balAmt - dedAmt;
					 * System.out.println("balAmt = "+balAmt);
					 * System.out.println("dedAmt = "+dedAmt);
					 * System.out.println("newBalAmt = "+newBalAmt);
					 * bank.setBnkBalanceAmt(newBalAmt); session.merge(bank); }
					 */

				} else {
					System.out.println("invalid payment method");
				}
				transaction.commit();
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.clear();
			session.close();
		} else {
			lhpvAdvNo = 0;
		}
		return lhpvAdvNo;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveLhpvBal(VoucherService voucherService) {
		System.out.println("enter into saveLhpvBal function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		// Map<String, Object> map = new HashMap<String, Object>();
		LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvBal> lhpvBalList = voucherService.getLhpvBalList();
		System.out.println("size of lhpvBalList = " + lhpvBalList.size());
		int lhpvBalNo = 0;
		if (!lhpvBalList.isEmpty()) {
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
						.getTime());
				Criteria cr = session.createCriteria(LhpvBal.class);
				cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, sqlDate));
				cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BCODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("lbNo"));
				cr.setProjection(proList);

				List<Integer> lhpvBalNoList = cr.list();

				if (!lhpvBalNoList.isEmpty()) {
					int laNo = lhpvBalNoList.get(lhpvBalNoList.size() - 1);
					lhpvBalNo = laNo + 1;
				} else {
					lhpvBalNo = 1;
				}

				char payBy = voucherService.getPayBy();
				String payToStf =  voucherService.getPayToStf();
				String stafCode = voucherService.getStafCode();
				System.out.println("value of payBy = " + payBy);
				System.out.println("value of payTo = " + payToStf);
				System.out.println("value of stafCode = " + stafCode);

				if (payBy == 'C') {
					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

					}
				} else if (payBy == 'Q') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBankCode(voucherService.getBankCode());
						lhpvBal.setLbChqType(voucherService.getChequeType());
						lhpvBal.setLbChqNo(voucherService.getChequeLeaves()
								.getChqLChqNo());
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
					}

					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = " + chequeType);
					String bankCode = voucherService.getBankCode();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();

					if (!bankMstrList.isEmpty()) {
						
						// It will cancel the cheque if lhpv is reversed
						if (totPayAmt < 0) {
							List<ChequeLeaves> chqL = new ArrayList<>();
							ChequeLeaves chequeLeaves = voucherService
									.getChequeLeaves();
							String bnkId = Integer.toString(bankMstrList.get(0)
									.getBnkId());
							String chqNo = voucherService.getChequeLeaves()
									.getChqLChqNo();
							System.out.println("BankId=" + bnkId);
							System.out.println("ChequeLeavesId=" + chqNo);
							/*
							 * cr=session.createCriteria(ChequeLeaves.class);
							 * cr.
							 * add(Restrictions.eq(ChequeLeavesCNTS.BANK_MSTR,
							 * bnkId ));
							 * cr.add(Restrictions.eq(ChequeLeavesCNTS.
							 * CHQ_L_CHQ_NO,chqNo)); chqL = cr.list();
							 */
							try {
								SQLQuery query = session
										.createSQLQuery("UPDATE chequeLeaves cl SET cl.chqLCancel=1 ,cl.chqlChqCancelDt =:date where cl.bnkId = :bnkId AND cl.chqLChqNo = :chqNo ");
								query.setParameter("bnkId", bnkId);
								query.setParameter("chqNo", chqNo);
								query.setParameter("date", new Date(
										new java.util.Date().getTime()));
								System.out.println(query);
								int result = query.executeUpdate();
								/*
								 * chqL=cr.list(); ChequeLeaves chq
								 * =chqL.get(0); System.out.println("Chq="+chq);
								 * chq.setChqLCancel(true); session.merge(chq);
								 */
							} catch (Exception e) {
								e.printStackTrace();

							}
						}else{

						List<ChequeLeaves> chqL = new ArrayList<>();
						ChequeLeaves chequeLeaves = voucherService
								.getChequeLeaves();
						cr = session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
								chequeLeaves.getChqLId()));
						chqL = cr.list();

						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totPayAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date()
								.getTime()));
						chq.setChqLUsed(true);
						
						

						session.merge(chq);
						
						}

						BankMstr bank = bankMstrList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totPayAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = " + balAmt);
						System.out.println("dedAmt = " + dedAmt);
						System.out.println("newBalAmt = " + newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
					}

				} else if (payBy == 'R') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBankCode(voucherService.getBankCode());
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());
						
						if (payToStf.equals("S")) {
							System.out.println(payToStf);
							lhpvBal.setPayToStf(payToStf);
							lhpvBal.setStafCode(stafCode);
						}
						System.out.println(lhpvBal.getPayToStf());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
					}

					/*
					 * List<BankMstr> bankMstrList = new ArrayList<>(); char
					 * chequeType = voucherService.getChequeType();
					 * System.out.println("chequeType = "+chequeType); String
					 * bankCode = voucherService.getBankCode();
					 * cr=session.createCriteria(BankMstr.class);
					 * cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,
					 * bankCode)); bankMstrList = cr.list();
					 * 
					 * 
					 * if(!bankMstrList.isEmpty()){
					 * 
					 * BankMstr bank = bankMstrList.get(0); double balAmt =
					 * bank.getBnkBalanceAmt(); double dedAmt = totPayAmt;
					 * double newBalAmt = balAmt - dedAmt;
					 * System.out.println("balAmt = "+balAmt);
					 * System.out.println("dedAmt = "+dedAmt);
					 * System.out.println("newBalAmt = "+newBalAmt);
					 * bank.setBnkBalanceAmt(newBalAmt); session.merge(bank); }
					 */

				} else {
					System.out.println("invalid payment method");
				}
				session.flush();
				session.clear();
				transaction.commit();
				
			} catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
			}
			
			session.close();
		} else {
			lhpvBalNo = 0;
		}
		return lhpvBalNo;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public int saveLhpvBal(VoucherService voucherService,Session session) {
		System.out.println("enter into saveLhpvBal function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		// Map<String, Object> map = new HashMap<String, Object>();
		LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvBal> lhpvBalList = voucherService.getLhpvBalList();
		System.out.println("size of lhpvBalList = " + lhpvBalList.size());
		int lhpvBalNo = 0;
		if (!lhpvBalList.isEmpty()) {

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
						.getTime());
				Criteria cr = session.createCriteria(LhpvBal.class);
				cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, sqlDate));
				cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BCODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("lbNo"));
				cr.setProjection(proList);

				List<Integer> lhpvBalNoList = cr.list();

				if (!lhpvBalNoList.isEmpty()) {
					int laNo = lhpvBalNoList.get(lhpvBalNoList.size() - 1);
					lhpvBalNo = laNo + 1;
				} else {
					lhpvBalNo = 1;
				}

				char payBy = voucherService.getPayBy();
				String payToStf =  voucherService.getPayToStf();
				String stafCode = voucherService.getStafCode();
				String loryCode = voucherService.getLoryCode();
				System.out.println("value of payBy = " + payBy);
				System.out.println("value of payTo = " + payToStf);
				System.out.println("value of stafCode = " + stafCode);

				if (payBy == 'C') {
					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

					}
				} else if (payBy == 'Q') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBankCode(voucherService.getBankCode());
						lhpvBal.setLbChqType(voucherService.getChequeType());
						lhpvBal.setLbChqNo(voucherService.getChequeLeaves()
								.getChqLChqNo());
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
					}

					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = " + chequeType);
					String bankCode = voucherService.getBankCode();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();

					if (!bankMstrList.isEmpty()) {
						
						// It will cancel the cheque if lhpv is reversed
						if(totPayAmt < 0) {
								List<ChequeLeaves> chqL =new ArrayList<>();
								for(int i=0;i<bankMstrList.get(0).getChequeLeavesList().size();i++){
									if(bankMstrList.get(0).getChequeLeavesList().get(i).getChqLChqNo().equalsIgnoreCase(voucherService.getChequeLeaves().getChqLChqNo()))
										chqL.add(bankMstrList.get(0).getChequeLeavesList().get(i));
								}
								System.out.println("chqId= "+chqL.get(0).getChqLChqNo());

								ChequeLeaves chq = chqL.get(0);
								chq.setChqLChqCancelDt(new Date(new java.util.Date()
										.getTime()));
								chq.setChqLCancel(true);
								
								session.merge(chq);
								
						}else{

						List<ChequeLeaves> chqL ;
						ChequeLeaves chequeLeaves = voucherService
								.getChequeLeaves();
						cr = session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
								chequeLeaves.getChqLId()));
						chqL = cr.list();

						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totPayAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date()
								.getTime()));
						chq.setChqLUsed(true);
						
						session.merge(chq);
						
						}

						BankMstr bank = bankMstrList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totPayAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = " + balAmt);
						System.out.println("dedAmt = " + dedAmt);
						System.out.println("newBalAmt = " + newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
					}

				} else if (payBy == 'R') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBankCode(voucherService.getBankCode());
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());
						
						if (payToStf.equals("S")) {
							System.out.println(payToStf);
							lhpvBal.setPayToStf(payToStf);
							lhpvBal.setStafCode(stafCode);
						}else if(payToStf.equals("L")){
							lhpvBal.setPayToStf(payToStf);
							lhpvBal.setStafCode(loryCode);
						}
						System.out.println(lhpvBal.getPayToStf());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
					}


				} else {
					System.out.println("invalid payment method");
				}
		} else {
			lhpvBalNo = 0;
		}
		return lhpvBalNo;
	}

	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int saveLhpvBalTemp(VoucherService voucherService) {
		System.out.println("enter into saveLhpvBalTemp function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		// Map<String, Object> map = new HashMap<String, Object>();
		LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvBal> lhpvBalList = voucherService.getLhpvBalList();
		System.out.println("size of lhpvBalList = " + lhpvBalList.size());
		int lhpvBalNo = 0;
		if (!lhpvBalList.isEmpty()) {
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
						.getTime());
				Criteria cr = session.createCriteria(LhpvBal.class);
				cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, sqlDate));
				cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BCODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("lbNo"));
				cr.setProjection(proList);

				List<Integer> lhpvBalNoList = cr.list();

				if (!lhpvBalNoList.isEmpty()) {
					int laNo = lhpvBalNoList.get(lhpvBalNoList.size() - 1);
					lhpvBalNo = laNo + 1;
				} else {
					lhpvBalNo = 1;
				}

				char payBy = voucherService.getPayBy();
				System.out.println("value of payBy = " + payBy);

				if (payBy == 'C') {
					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						if (actChln.getChlnArId() < 0) {
							lhpvBal.setLbIsArRept(false);
						} else {
							lhpvBal.setLbIsArRept(true);
						}

						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();
						lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
						lhpvCashSmry.setLaId(0);
						lhpvCashSmry.setLbId(lbId);
						lhpvCashSmry.setLcsBrhCode(brFaCode);
						lhpvCashSmry.setLcsAdvEntr(false);
						lhpvCashSmry.setLcsBrkOwn(voucherService
								.getBrkOwnFaCode());
						lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
						lhpvCashSmry.setLcsChlnCode(actChln.getChlnCode());
						lhpvCashSmry.setLcsCsEntr(true);
						lhpvCashSmry.setLcsLhpvDt(lhpvBal.getLbDt());
						lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
						lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
						lhpvCashSmry.setLcsPayAmt(lhpvBal.getLbLryBalP());
						lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
						lhpvCashSmry
								.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
						lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
						lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
						lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
						lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
						lhpvCashSmry
								.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
						lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
						lhpvCashSmry
								.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());
						lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
						lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
						lhpvCashSmry.setLcsToday(true);
						lhpvCashSmry.setLspId(0);
						lhpvCashSmry.setUserCode(currentUser.getUserCode());

						session.save(lhpvCashSmry);

					}
				} else if (payBy == 'Q') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBankCode(voucherService.getBankCode());
						lhpvBal.setLbChqType(voucherService.getChequeType());
						lhpvBal.setLbChqNo(voucherService.getChequeLeaves()
								.getChqLChqNo());
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
					}

					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = " + chequeType);
					String bankCode = voucherService.getBankCode();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();

					if (!bankMstrList.isEmpty()) {

						List<ChequeLeaves> chqL = new ArrayList<>();
						ChequeLeaves chequeLeaves = voucherService
								.getChequeLeaves();
						cr = session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
								chequeLeaves.getChqLId()));
						chqL = cr.list();

						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totPayAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date()
								.getTime()));
						chq.setChqLUsed(true);

						session.merge(chq);

						BankMstr bank = bankMstrList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totPayAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = " + balAmt);
						System.out.println("dedAmt = " + dedAmt);
						System.out.println("newBalAmt = " + newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
					}

				} else if (payBy == 'R') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvBalList.size(); i++) {
						LhpvBal lhpvBal = lhpvBalList.get(i);
						lhpvBal.setLbDt(sqlDate);
						lhpvBal.setLbNo(lhpvBalNo);
						lhpvBal.setLbPayMethod(payBy);
						lhpvBal.setLbBrhCode(brFaCode);
						lhpvBal.setLbBankCode(voucherService.getBankCode());
						lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvBal.setUserCode(currentUser.getUserCode());
						lhpvBal.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvBal.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,
								chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(
								LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLbList().add(lhpvBal);
						lhpvBal.setLhpvStatus(actLhpvSt);

						double rem = actChln.getChlnRemBal();
						actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
						session.merge(actChln);
						// actChln.setLhpvBal(lhpvBal);

						lhpvBal.setChallan(actChln);
						int lbId = (Integer) session.save(lhpvBal);
						System.out.println("lbId ===> " + lbId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
					}

					/*
					 * List<BankMstr> bankMstrList = new ArrayList<>(); char
					 * chequeType = voucherService.getChequeType();
					 * System.out.println("chequeType = "+chequeType); String
					 * bankCode = voucherService.getBankCode();
					 * cr=session.createCriteria(BankMstr.class);
					 * cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,
					 * bankCode)); bankMstrList = cr.list();
					 * 
					 * 
					 * if(!bankMstrList.isEmpty()){
					 * 
					 * BankMstr bank = bankMstrList.get(0); double balAmt =
					 * bank.getBnkBalanceAmt(); double dedAmt = totPayAmt;
					 * double newBalAmt = balAmt - dedAmt;
					 * System.out.println("balAmt = "+balAmt);
					 * System.out.println("dedAmt = "+dedAmt);
					 * System.out.println("newBalAmt = "+newBalAmt);
					 * bank.setBnkBalanceAmt(newBalAmt); session.merge(bank); }
					 */

				} else {
					System.out.println("invalid payment method");
				}
				transaction.commit();
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.clear();
			session.close();
		} else {
			lhpvBalNo = 0;
		}
		return lhpvBalNo;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveLhpvSup(VoucherService voucherService) {
		System.out.println("enter into saveLhpvSup function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		// Map<String, Object> map = new HashMap<String, Object>();
		LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvSup> lhpvSupList = voucherService.getLhpvSupList();
		System.out.println("size of lhpvSupList = " + lhpvSupList.size());
		int lhpvSupNo = 0;
		if (!lhpvSupList.isEmpty()) {
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt().getTime());
				Criteria cr = session.createCriteria(LhpvSup.class);
				cr.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_DT, sqlDate));
				cr.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_BCODE,currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("lspNo"));
				cr.setProjection(proList);

				List<Integer> lhpvSupNoList = cr.list();

				if (!lhpvSupNoList.isEmpty()) {
					int lspNo = lhpvSupNoList.get(lhpvSupNoList.size() - 1);
					lhpvSupNo = lspNo + 1;
				} else {
					lhpvSupNo = 1;
				}

				char payBy = voucherService.getPayBy();
				System.out.println("value of payBy = " + payBy);

				if (payBy == 'C') {
					for (int i = 0; i < lhpvSupList.size(); i++) {
						LhpvSup lhpvSup = lhpvSupList.get(i);
						lhpvSup.setLspDt(sqlDate);
						lhpvSup.setLspNo(lhpvSupNo);
						lhpvSup.setLspPayMethod(payBy);
						lhpvSup.setLspBrhCode(brFaCode);
						lhpvSup.setLspBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvSup.setUserCode(currentUser.getUserCode());
						lhpvSup.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvSup.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLspList().add(lhpvSup);
						lhpvSup.setLhpvStatus(actLhpvSt);

						lhpvSup.setChallan(actChln);
						int lspId = (Integer) session.save(lhpvSup);
						System.out.println("lspId ===> " + lspId);
						session.update(actLhpvSt);

					}
				} else if (payBy == 'Q') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvSupList.size(); i++) {
						LhpvSup lhpvSup = lhpvSupList.get(i);
						lhpvSup.setLspDt(sqlDate);
						lhpvSup.setLspNo(lhpvSupNo);
						lhpvSup.setLspPayMethod(payBy);
						lhpvSup.setLspBrhCode(brFaCode);
						lhpvSup.setLspBankCode(voucherService.getBankCode());
						lhpvSup.setLspChqType(voucherService.getChequeType());
						lhpvSup.setLspChqNo(voucherService.getChequeLeaves().getChqLChqNo());
						lhpvSup.setLspBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvSup.setUserCode(currentUser.getUserCode());
						lhpvSup.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvSup.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLspList().add(lhpvSup);
						lhpvSup.setLhpvStatus(actLhpvSt);

						lhpvSup.setChallan(actChln);
						int lspId = (Integer) session.save(lhpvSup);
						System.out.println("lspId ===> " + lspId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvSup.getLspFinalTot();
					}

					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = " + chequeType);
					String bankCode = voucherService.getBankCode();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();

					if (!bankMstrList.isEmpty()) {

						List<ChequeLeaves> chqL = new ArrayList<>();
						ChequeLeaves chequeLeaves = voucherService
								.getChequeLeaves();
						cr = session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
						chqL = cr.list();

						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totPayAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date().getTime()));
						chq.setChqLUsed(true);

						session.merge(chq);

						BankMstr bank = bankMstrList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totPayAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = " + balAmt);
						System.out.println("dedAmt = " + dedAmt);
						System.out.println("newBalAmt = " + newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
					}

				} else if (payBy == 'R') {

					double totPayAmt = 0.0;

					for (int i = 0; i < lhpvSupList.size(); i++) {
						LhpvSup lhpvSup = lhpvSupList.get(i);
						lhpvSup.setLspDt(sqlDate);
						lhpvSup.setLspNo(lhpvSupNo);
						lhpvSup.setLspPayMethod(payBy);
						lhpvSup.setLspBrhCode(brFaCode);
						lhpvSup.setLspBankCode(voucherService.getBankCode());
						lhpvSup.setLspBrkOwn(voucherService.getBrkOwnFaCode());
						lhpvSup.setUserCode(currentUser.getUserCode());
						lhpvSup.setbCode(currentUser.getUserBranchCode());

						Challan chln = lhpvSup.getChallan();
						Challan actChln = (Challan) session.get(Challan.class,chln.getChlnId());

						LhpvStatus actLhpvSt = (LhpvStatus) session.get(LhpvStatus.class, lhpvStatus.getLsId());
						actLhpvSt.getLspList().add(lhpvSup);
						lhpvSup.setLhpvStatus(actLhpvSt);

						lhpvSup.setChallan(actChln);
						int lspId = (Integer) session.save(lhpvSup);
						System.out.println("lspId ===> " + lspId);
						session.update(actLhpvSt);

						totPayAmt = totPayAmt + lhpvSup.getLspFinalTot();
					}

					/*
					 * List<BankMstr> bankMstrList = new ArrayList<>(); char
					 * chequeType = voucherService.getChequeType();
					 * System.out.println("chequeType = "+chequeType); String
					 * bankCode = voucherService.getBankCode();
					 * cr=session.createCriteria(BankMstr.class);
					 * cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,
					 * bankCode)); bankMstrList = cr.list();
					 * 
					 * 
					 * if(!bankMstrList.isEmpty()){
					 * 
					 * BankMstr bank = bankMstrList.get(0); double balAmt =
					 * bank.getBnkBalanceAmt(); double dedAmt = totPayAmt;
					 * double newBalAmt = balAmt - dedAmt;
					 * System.out.println("balAmt = "+balAmt);
					 * System.out.println("dedAmt = "+dedAmt);
					 * System.out.println("newBalAmt = "+newBalAmt);
					 * bank.setBnkBalanceAmt(newBalAmt); session.merge(bank); }
					 */

				} else {
					System.out.println("invalid payment method");
				}
				transaction.commit();
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.clear();
			session.close();
		} else {
			lhpvSupNo = 0;
		}
		return lhpvSupNo;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvAdv> getLhpvAdvList(int lsId) {
		System.out.println("enter into getLhpvAdvList funciton = " + lsId);
		List<LhpvAdv> lhpvAdvList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();
			/*
			 * if(!lsList.isEmpty()){ if(lsList.get(0).isLsClose() == false){
			 * lhpvAdvList = lsList.get(0).getLaList(); } }
			 */
			if (!lsList.isEmpty()) {
				if (lsList.get(0).isLsClose() == false) {
					LhpvStatus lhpvStatus = lsList.get(0);
					List<LhpvAdv> list = lhpvStatus.getLaList();
					System.out.println(lhpvStatus.getLaList());
					Hibernate.initialize(lhpvStatus.getLaList());
					lhpvAdvList = lhpvStatus.getLaList();
				}
			}
			System.out.println("Size of lhpvAdvList = " + lhpvAdvList.size());
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvAdvList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvAdv> getLhpvAdv(int lsId) {
		System.out.println("enter into getLhpvAdv funciton = " + lsId);
		List<LhpvAdv> lhpvAdvList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				/* Hibernate.initialize(lsList.get(0).getLaList()); */
				lhpvAdvList = lsList.get(0).getLaList();
			}

			System.out.println("Size of lhpvAdvList = " + lhpvAdvList.size());
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvAdvList;
	}
	
	
	@Override
	public List<LhpvAdv> getLhpvAdv(int lsId,Session session) {
		System.out.println("enter into getLhpvAdv funciton = " + lsId);
		List<LhpvAdv> lhpvAdvList = new ArrayList<>();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				/* Hibernate.initialize(lsList.get(0).getLaList()); */
				lhpvAdvList = lsList.get(0).getLaList();
			}

			System.out.println("Size of lhpvAdvList = " + lhpvAdvList.size());
		return lhpvAdvList;
	}

	
	
	
	

	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvBal> getLhpvBalList(int lsId) {
		System.out.println("enter into getLhpvBalList funciton = " + lsId);
		List<LhpvBal> lhpvBalList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();
			if (!lsList.isEmpty()) {
				if (lsList.get(0).isLsClose() == false) {
					lhpvBalList = lsList.get(0).getLbList();
				}
			}
			System.out.println("Size of lhpvBalList = " + lhpvBalList.size());
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvBalList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvBal> getLhpvBal(int lsId) {
		System.out.println("enter into getLhpvBal funciton = " + lsId);
		List<LhpvBal> lhpvBalList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				lhpvBalList = lsList.get(0).getLbList();
			}

			System.out.println("Size of lhpvBalList = " + lhpvBalList.size());
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvBalList;
	}
	
	
	
	public List<LhpvBal> getLhpvBal(int lsId,Session session) {
		System.out.println("enter into getLhpvBal funciton = " + lsId);
		List<LhpvBal> lhpvBalList = new ArrayList<>();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				lhpvBalList = lsList.get(0).getLbList();
			}

			System.out.println("Size of lhpvBalList = " + lhpvBalList.size());
		return lhpvBalList;
	}

	@Override
	public List<LhpvBal> getLhpvBal(Date date,String bCode) {
		System.out.println("enter into getLhpvBal funciton = " + date);
		Session session=sessionFactory.openSession();
		Branch branch=(Branch) session.createCriteria(Branch.class)
				.add(Restrictions.eq(BranchCNTS.BRANCH_CODE, bCode)).list().get(0);
		List<LhpvBal> lhpvBalList = new ArrayList<>();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvBal.class);
			cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, date));
			cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BRH_FACODE, branch.getBranchFaCode()));
			cr.add(Restrictions.isNull(LhpvBalCNTS.LHPV_BAL_STATUS));
			lhpvBalList = cr.list();

			session.clear();
			session.close();
			System.out.println("Size of lhpvBalList = " + lhpvBalList.size());
		return lhpvBalList;
	}
	
	
	@Override
	public List<LhpvAdv> getLhpvAdv(Date date,String bCode) {
		System.out.println("enter into getLhpvBal funciton = " + date);
		Session session=sessionFactory.openSession();
		Branch branch=(Branch) session.createCriteria(Branch.class)
				.add(Restrictions.eq(BranchCNTS.BRANCH_CODE, bCode)).list().get(0);
		List<LhpvAdv> lhpvAdvList = new ArrayList<>();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvAdv.class);
			cr.add(Restrictions.le(LhpvAdvCNTS.LHPV_ADV_DT, date));
			cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_BCODE, branch.getBranchCode()));
			cr.add(Restrictions.isNull("lhpvStatus"));
			lhpvAdvList = cr.list();

			session.clear();
			session.close();
			System.out.println("Size of lhpvBalList = " + lhpvAdvList.size());
		return lhpvAdvList;
	}
	
	
	
	@Override
	public List<LhpvBal> getLhpvBal(Date date,String bCode,Session session) {
		System.out.println("enter into getLhpvBal funciton = " + date);
		Branch branch=(Branch) session.createCriteria(Branch.class)
				.add(Restrictions.eq(BranchCNTS.BRANCH_CODE, bCode)).list().get(0);
		List<LhpvBal> lhpvBalList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvBal.class);
			cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, date));
			cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BRH_FACODE, branch.getBranchFaCode()));
			cr.add(Restrictions.isNull(LhpvBalCNTS.LHPV_BAL_STATUS));
			lhpvBalList = cr.list();
			
			

			System.out.println("Size of lhpvBalList = " + lhpvBalList.size());
		return lhpvBalList;
	}


	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvSup> getLhpvSupList(int lsId) {
		System.out.println("enter into getLhpvSupList funciton = " + lsId);
		List<LhpvSup> lhpvSupList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();
			if (!lsList.isEmpty()) {
				if (lsList.get(0).isLsClose() == false) {
					lhpvSupList = lsList.get(0).getLspList();
				}
			}
			System.out.println("Size of lhpvSupList = " + lhpvSupList.size());
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvSupList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvSup> getLhpvSup(int lsId) {
		System.out.println("enter into getLhpvSup funciton = " + lsId);
		List<LhpvSup> lhpvSupList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				lhpvSupList = lsList.get(0).getLspList();
			}

			System.out.println("Size of lhpvSupList = " + lhpvSupList.size());
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvSupList;
	}

	
	
	
	@Override
	public List<LhpvSup> getLhpvSup(int lsId,Session session) {
		System.out.println("enter into getLhpvSup funciton = " + lsId);
		List<LhpvSup> lhpvSupList = new ArrayList<>();
			List<LhpvStatus> lsList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				lhpvSupList = lsList.get(0).getLspList();
			}

			System.out.println("Size of lhpvSupList = " + lhpvSupList.size());
		return lhpvSupList;
	}

	
	
	
	
//	@SuppressWarnings("unchecked")
//	@Transactional
//	public int closeLhpvIntoCS(LhpvStatus lhpvStatus) {
//		System.out.println("enter into closeLhpv funciton");
//		User currentUser = (User) httpSession.getAttribute("currentUser");
//		Date date = new Date(new java.util.Date().getTime());
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(date);
//		System.out.println("unique id =====>" + calendar.getTimeInMillis());
//		long milliSec = calendar.getTimeInMillis();
//		String tvNo = String.valueOf(milliSec);
//		int res = 0;
//
//		int lsId = lhpvStatus.getLsId();
//		if (lsId > 0) {
//			try {
//				session = this.sessionFactory.openSession();
//				transaction = session.beginTransaction();
//				List<LhpvStatus> lsList = new ArrayList<>();
//				Criteria cr = session.createCriteria(LhpvStatus.class);
//				cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
//				lsList = cr.list();
//
//				Branch curBrh = (Branch) session.get(Branch.class,
//						Integer.parseInt(currentUser.getUserBranchCode()));
//
//				String lryFaCode = "";
//				String csDisFaCode = "";
//				String tdsFaCode = "";
//				String munsFaCode = "";
//				String clmFaCode = "";
//				String lryAdvOth = "";
//				List<FAMaster> famList = new ArrayList<>();
//				/*
//				 * cr = session.createCriteria(FAMaster.class);
//				 * cr.add(Restrictions
//				 * .or(FAMasterCNTS.FA_MFA_NAME,FAMasterCNTS.FA_MFA_NAME
//				 * ));//(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LORRY_HIRE));
//				 */
//				famList = session
//						.createCriteria(FAMaster.class)
//						.add(Restrictions.disjunction().add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,
//										ConstantsValues.LORRY_HIRE))
//										.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.CASH_DIS_LH))
//								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TDS_LRY_HIRE))
//								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.MUNS_LRY_HIRE))
//								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.CLAIM_LRY_HIRE))
//								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LRYADV_AND_OTH)))
//						.list();
//
//				if (!famList.isEmpty()) {
//					for (int i = 0; i < famList.size(); i++) {
//						if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.LORRY_HIRE)) {
//							lryFaCode = famList.get(i).getFaMfaCode();
//						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.CASH_DIS_LH)) {
//							csDisFaCode = famList.get(i).getFaMfaCode();
//						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.TDS_LRY_HIRE)) {
//							tdsFaCode = famList.get(i).getFaMfaCode();
//						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.MUNS_LRY_HIRE)) {
//							munsFaCode = famList.get(i).getFaMfaCode();
//						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.CLAIM_LRY_HIRE)) {
//							clmFaCode = famList.get(i).getFaMfaCode();
//						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.LRYADV_AND_OTH)) {
//							lryAdvOth = famList.get(i).getFaMfaCode();
//						} else {
//							System.out.println("invalid result");
//						}
//					}
//				}
//
//				if (!lsList.isEmpty()) {
//
//					LhpvStatus actLhpvStatus = lsList.get(0);
//					List<LhpvAdv> lhpvAdvList = actLhpvStatus.getLaList();
//					List<LhpvBal> lhpvBalList = actLhpvStatus.getLbList();
//					List<LhpvSup> lhpvSupList = actLhpvStatus.getLspList();
//
//					double sameBrhLhPay_C = 0.0;
//					double sameBrhCD_C = 0.0;
//					double sameBrhTds_C = 0.0;
//					double sameBrhMuns_C = 0.0;
//					double sameBrhClm_C = 0.0;
//
//					double sameBrhLhPay_Q = 0.0;
//					double sameBrhCD_Q = 0.0;
//					double sameBrhTds_Q = 0.0;
//					double sameBrhMuns_Q = 0.0;
//					double sameBrhClm_Q = 0.0;
//
//					double sameBrhLhPay_R = 0.0;
//					double sameBrhCD_R = 0.0;
//					double sameBrhTds_R = 0.0;
//					double sameBrhMuns_R = 0.0;
//					double sameBrhClm_R = 0.0;
//
//					List<CashStmtStatus> cashStmtStatusList = new ArrayList<>();
//					Query query = session
//							.createQuery("from CashStmtStatus where bCode= :type and cssDt = :date");
//					query.setParameter("type", currentUser.getUserBranchCode());
//					query.setParameter("date", lsList.get(0).getLsDt());
//					cashStmtStatusList = query.setMaxResults(1).list();
//					java.sql.Date sqlDate = new java.sql.Date(
//							cashStmtStatusList.get(0).getCssDt().getTime());
//
//					if (!cashStmtStatusList.isEmpty()) {
//						CashStmtStatus cashStmtStatus = cashStmtStatusList
//								.get(0);
//						int voucherNo = 0;
//						List<CashStmt> csVouchList = new ArrayList<CashStmt>();
//						csVouchList = cashStmtStatus.getCashStmtList();
//
//						if (!csVouchList.isEmpty()) {
//							for (int i = 0; i < csVouchList.size(); i++) {
//								if (voucherNo < csVouchList.get(i).getCsVouchNo()) {
//									voucherNo = csVouchList.get(i).getCsVouchNo();
//								}
//							}
//
//							voucherNo = voucherNo + 1;
//						} else {
//							voucherNo = 1;
//						}
//						/*
//						 * cr = session.createCriteria(CashStmt.class);
//						 * cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,
//						 * sqlDate));
//						 * cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE
//						 * ,currentUser.getUserBranchCode())); ProjectionList
//						 * proList = Projections.projectionList();
//						 * proList.add(Projections.property("csVouchNo"));
//						 * cr.setProjection(proList);
//						 * 
//						 * List<Integer> voucherNoList = cr.list(); int
//						 * voucherNo = 0; if (!voucherNoList.isEmpty()) { int
//						 * vNo = voucherNoList.get(voucherNoList.size() - 1);
//						 * voucherNo = vNo + 1; } else { voucherNo = 1; }
//						 */
//
//						if (!lhpvAdvList.isEmpty()) {
//							for (int i = 0; i < lhpvAdvList.size(); i++) {
//								LhpvAdv lhpvAdv = lhpvAdvList.get(i);
//								double tempLaTotPayAmt;
//								// char payTo=lhpvAdv.getPayToStf();
//								String stfCode = lhpvAdv.getStafCode();
//
//								if (lhpvAdv.getLaTotPayAmt() < 0) {
//									tempLaTotPayAmt = (-1)* (lhpvAdv.getLaTotPayAmt());
//								} else {
//									tempLaTotPayAmt = lhpvAdv.getLaTotPayAmt();
//								}
//								// Challan challan = lhpvAdv.getChallan();
//								System.out.println("tempLaTotPayAmt="
//										+ tempLaTotPayAmt);
//								// if(challan.getBranchCode().equalsIgnoreCase(actLhpvStatus.getbCode())){
//								// Lhpv Adv for Challan of same branch
//								if (lhpvAdv.getLaPayMethod() == 'C') {
//									sameBrhLhPay_C = sameBrhLhPay_C	+ lhpvAdv.getLaTotPayAmt();
//									sameBrhCD_C = sameBrhCD_C + lhpvAdv.getLaCashDiscR();
//									sameBrhTds_C = sameBrhTds_C	+ lhpvAdv.getLaTdsR();
//									sameBrhMuns_C = sameBrhMuns_C + lhpvAdv.getLaMunsR();
//									System.out.println("Kamal1");
//								} else if (lhpvAdv.getLaPayMethod() == 'Q') {
//									System.out.println("Kamal2");
//									sameBrhLhPay_Q = sameBrhLhPay_Q	+ lhpvAdv.getLaTotPayAmt();
//									sameBrhCD_Q = sameBrhCD_Q + lhpvAdv.getLaCashDiscR();
//									sameBrhTds_Q = sameBrhTds_Q	+ lhpvAdv.getLaTdsR();
//									sameBrhMuns_Q = sameBrhMuns_Q + lhpvAdv.getLaMunsR();
//
//									CashStmt mainCashStmt = new CashStmt();
//									mainCashStmt.setbCode(currentUser.getUserBranchCode());
//									mainCashStmt.setUserCode(currentUser.getUserCode());
//									// mainCashStmt.setCsDescription("LHPV ADVANCE PAY BY "+lhpvAdv.getLaBankCode()+" BANK");
//									mainCashStmt.setCsDescription("as per lhpv");
//									if (lhpvAdv.getLaTotPayAmt() < 0) {
//										mainCashStmt.setCsDrCr('D');
//										mainCashStmt.setCsAmt((-1)*(lhpvAdv.getLaFinalTot()));
//									} else {
//										mainCashStmt.setCsDrCr('C');
//										mainCashStmt.setCsAmt(lhpvAdv.getLaFinalTot());
//									}
//
//									//mainCashStmt.setCsAmt(tempLaTotPayAmt);
//									mainCashStmt.setCsType("LHPV PAYMENT");
//									mainCashStmt.setCsVouchType("BANK");
//									mainCashStmt.setCsChequeType(lhpvAdv.getLaChqType());
//									mainCashStmt.setCsFaCode(lhpvAdv.getLaBankCode());
//									// mainCashStmt.setCsTvNo(tvNo);
//									mainCashStmt.setCsDt(sqlDate);
//									mainCashStmt.setCsVouchNo(voucherNo);
//									mainCashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//									mainCashStmt.setPayMode("Q");
//
//									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//									mainCashStmt.setCsNo(actCss);
//									actCss.getCashStmtList().add(mainCashStmt);
//									session.merge(actCss);
//
//									if (lhpvAdv.getLaTotPayAmt() > 0) {
//										System.out.println("Kamal3");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(tempLaTotPayAmt);
//										// cashStmt.setCsDescription("LHPV Payment By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaTotPayAmt() < 0) {
//										System.out.println("Kamal5");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										// double
//										// tempLaTotPayAmt=(-1)*(lhpvAdv.getLaTotPayAmt());
//										cashStmt.setCsAmt(tempLaTotPayAmt);
//										// cashStmt.setCsDescription("LHPV Payment By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//
//									}
//
//									if (lhpvAdv.getLaCashDiscR() > 0) {
//										System.out.println("Kamal6");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvAdv.getLaCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaCashDiscR() < 0) {
//										System.out.println("Kamal7");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1) * lhpvAdv.getLaCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvAdv.getLaTdsR() > 0) {
//										System.out.println("Kamal8");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvAdv.getLaTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaTdsR() < 0) {
//										System.out.println("Kamal9");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1)
//												* lhpvAdv.getLaTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvAdv.getLaMunsR() > 0) {
//										System.out.println("Kamal10");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvAdv.getLaMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaMunsR() < 0) {
//										System.out.println("Kamal11");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1) * lhpvAdv.getLaMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									voucherNo = voucherNo + 1;
//
//								} else if (lhpvAdv.getLaPayMethod() == 'R') {
//									System.out.println("Kamal12");
//									sameBrhLhPay_R = sameBrhLhPay_R	+ lhpvAdv.getLaTotPayAmt();
//									sameBrhCD_R = sameBrhCD_R + lhpvAdv.getLaCashDiscR();
//									sameBrhTds_R = sameBrhTds_R	+ lhpvAdv.getLaTdsR();
//									sameBrhMuns_R = sameBrhMuns_R + lhpvAdv.getLaMunsR();
//
//									CashStmt mainCashStmt = new CashStmt();
//									mainCashStmt.setbCode(currentUser.getUserBranchCode());
//									mainCashStmt.setUserCode(currentUser.getUserCode());
//									// mainCashStmt.setCsDescription("LHPV ADVANCE PAY BY "+lhpvAdv.getLaBankCode()+" BANK");
//									mainCashStmt.setCsDescription("as per lhpv");
//									// mainCashStmt.setCsDrCr('C');
//									Double tempFinlTot;
//									if (lhpvAdv.getLaTotPayAmt() < 0) {
//										mainCashStmt.setCsDrCr('D');
//										tempFinlTot = (-1) * (lhpvAdv.getLaFinalTot());
//									} else {
//										mainCashStmt.setCsDrCr('C');
//										tempFinlTot = lhpvAdv.getLaFinalTot();
//									}
//									mainCashStmt.setCsAmt(tempFinlTot);
//									mainCashStmt.setCsType("LHPV PAYMENT");
//									mainCashStmt.setCsVouchType("RTGS");
//									mainCashStmt.setCsChequeType(lhpvAdv.getLaChqType());
//									// mainCashStmt.setCsFaCode(lhpvAdv.getLaBankCode());
//									if (stfCode != null && stfCode != "") {
//										mainCashStmt.setCsFaCode(stfCode);
//										System.out.println("setCsFaCode=" + stfCode);
//									} else {
//										mainCashStmt.setCsFaCode(lhpvAdv.getLaBrkOwn());
//										System.out.println("setCsFaCode=" + lhpvAdv.getLaBrkOwn());
//									}
//
//									mainCashStmt.setCsTvNo(tvNo);
//									mainCashStmt.setPayMode("R");
//									mainCashStmt.setCsDt(sqlDate);
//									mainCashStmt.setCsVouchNo(voucherNo);
//
//									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//									mainCashStmt.setCsNo(actCss);
//									actCss.getCashStmtList().add(mainCashStmt);
//									session.merge(actCss);
//
//									if (lhpvAdv.getLaTotPayAmt() > 0) {
//										System.out.println("Kamal13");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(tempLaTotPayAmt);
//										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaTotPayAmt() < 0) {
//										System.out.println("Kamal14");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(tempLaTotPayAmt);
//										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvAdv.getLaCashDiscR() > 0) {
//										System.out.println("Kamal15");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvAdv.getLaCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaCashDiscR() < 0) {
//										System.out.println("Kamal16");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1) * lhpvAdv.getLaCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvAdv.getLaTdsR() > 0) {
//										System.out.println("Kamal17");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvAdv.getLaTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaTdsR() < 0) {
//										System.out.println("Kamal18");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)
//												* lhpvAdv.getLaTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvAdv.getLaMunsR() > 0) {
//										System.out.println("Kamal19");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvAdv.getLaMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvAdv.getLaMunsR() < 0) {
//										System.out.println("Kamal20");
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)
//												* lhpvAdv.getLaMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									voucherNo = voucherNo + 1;
//								} else {
//									System.out
//											.println("invalid payment method for lhpv advance");
//								}
//
//								// }else{
//								// Lhpv Adv for Challan of other branch
//
//								// }
//
//								lhpvAdv.setLaIsClose(true);
//								session.merge(lhpvAdv);
//							}
//						}
//
//						if (!lhpvBalList.isEmpty()) {
//							for (int i = 0; i < lhpvBalList.size(); i++) {
//								LhpvBal lhpvBal = lhpvBalList.get(i);
//								// Challan challan = lhpvBal.getChallan();
//								String stfCode = lhpvBal.getStafCode();
//								double tempLbTotPayAmt;
//								// char payTo=lhpvAdv.getPayToStf();
//								//String stfCode = lhpvbal.getStafCode();
//
//								if (lhpvBal.getLbFinalTot() < 0) {
//									tempLbTotPayAmt = (-1)* (lhpvBal.getLbFinalTot());
//								} else {
//									tempLbTotPayAmt = lhpvBal.getLbFinalTot();
//								}
//								// Challan challan = lhpvAdv.getChallan();
//								System.out.println("tempLbTotPayAmt="
//										+ tempLbTotPayAmt);
//
//								// if(challan.getBranchCode().equalsIgnoreCase(actLhpvStatus.getbCode())){
//								// Lhpv Adv for Challan of same branch
//								if (lhpvBal.getLbPayMethod() == 'C') {
//									sameBrhLhPay_C = sameBrhLhPay_C + lhpvBal.getLbTotPayAmt();
//									sameBrhCD_C = sameBrhCD_C + lhpvBal.getLbCashDiscR();
//									sameBrhTds_C = sameBrhTds_C	+ lhpvBal.getLbTdsR();
//									sameBrhMuns_C = sameBrhMuns_C + lhpvBal.getLbMunsR();
//									sameBrhClm_C = sameBrhClm_C	+ lhpvBal.getLbWtShrtgCR()+ lhpvBal.getLbLateDelCR()+ lhpvBal.getLbLateAckCR()+ lhpvBal.getLbOthMiscP()+ lhpvBal.getLbDrRcvrWtCR();
//								} else if (lhpvBal.getLbPayMethod() == 'Q') {
//									double claim = 0.0;
//									sameBrhLhPay_Q = sameBrhLhPay_Q + lhpvBal.getLbTotPayAmt();
//									sameBrhCD_Q = sameBrhCD_Q + lhpvBal.getLbCashDiscR();
//									sameBrhTds_Q = sameBrhTds_Q	+ lhpvBal.getLbTdsR();
//									sameBrhMuns_Q = sameBrhMuns_Q + lhpvBal.getLbMunsR();
//									claim = lhpvBal.getLbWtShrtgCR()+ lhpvBal.getLbLateDelCR()+ lhpvBal.getLbLateAckCR()+ lhpvBal.getLbOthMiscP()+ lhpvBal.getLbDrRcvrWtCR();
//									sameBrhClm_Q = sameBrhClm_Q	+ lhpvBal.getLbWtShrtgCR()+ lhpvBal.getLbLateDelCR()+ lhpvBal.getLbLateAckCR()+ lhpvBal.getLbOthMiscP()+ lhpvBal.getLbDrRcvrWtCR();
//
//									CashStmt mainCashStmt = new CashStmt();
//									mainCashStmt.setbCode(currentUser.getUserBranchCode());
//									mainCashStmt.setUserCode(currentUser.getUserCode());
//									// mainCashStmt.setCsDescription("LHPV BALANCE PAY BY "+lhpvBal.getLbBankCode()+" BANK");
//									mainCashStmt.setCsDescription("as per lhpv");
//									//mainCashStmt.setCsDrCr('C');
//									if (lhpvBal.getLbFinalTot() < 0) {
//										mainCashStmt.setCsDrCr('D');
//									} else {
//										mainCashStmt.setCsDrCr('C');
//									}
//									//mainCashStmt.setCsAmt(lhpvBal.getLbFinalTot());
//									mainCashStmt.setCsAmt(tempLbTotPayAmt);
//									mainCashStmt.setCsType("LHPV PAYMENT");
//									mainCashStmt.setCsVouchType("BANK");
//									mainCashStmt.setCsChequeType(lhpvBal.getLbChqType());
//									mainCashStmt.setCsFaCode(lhpvBal.getLbBankCode());
//									mainCashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//									mainCashStmt.setPayMode("Q");
//									mainCashStmt.setCsDt(sqlDate);
//									mainCashStmt.setCsVouchNo(voucherNo);
//
//									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//									mainCashStmt.setCsNo(actCss);
//									actCss.getCashStmtList().add(mainCashStmt);
//									session.merge(actCss);
//
//									if (lhpvBal.getLbTotPayAmt() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbTotPayAmt());
//										// cashStmt.setCsDescription("LHPV Payment By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbTotPayAmt() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbTotPayAmt());
//										// cashStmt.setCsDescription("LHPV Payment By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvBal.getLbCashDiscR() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbCashDiscR() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvBal.getLbTdsR() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbTdsR() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvBal.getLbMunsR() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbMunsR() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (claim > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(claim);
//										// cashStmt.setCsDescription("LHPV Claim By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(clmFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (claim < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*claim);
//										// cashStmt.setCsDescription("LHPV Claim By "+lhpvBal.getLbBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(clmFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									voucherNo = voucherNo + 1;
//
//								} else if (lhpvBal.getLbPayMethod() == 'R') {
//									double claim = 0.0;
//									sameBrhLhPay_R = sameBrhLhPay_R + lhpvBal.getLbTotPayAmt();
//									sameBrhCD_R = sameBrhCD_R + lhpvBal.getLbCashDiscR();
//									sameBrhTds_R = sameBrhTds_R + lhpvBal.getLbTdsR();
//									sameBrhMuns_R = sameBrhMuns_R + lhpvBal.getLbMunsR();
//									claim = lhpvBal.getLbWtShrtgCR() + lhpvBal.getLbLateDelCR() + lhpvBal.getLbLateAckCR() + lhpvBal.getLbOthMiscP() + lhpvBal.getLbDrRcvrWtCR();
//									sameBrhClm_R = sameBrhClm_R + lhpvBal.getLbWtShrtgCR() + lhpvBal.getLbLateDelCR() + lhpvBal.getLbLateAckCR() + lhpvBal.getLbOthMiscP() + lhpvBal.getLbDrRcvrWtCR();
//
//									CashStmt mainCashStmt = new CashStmt();
//									mainCashStmt.setbCode(currentUser.getUserBranchCode());
//									mainCashStmt.setUserCode(currentUser.getUserCode());
//									// mainCashStmt.setCsDescription("LHPV BALANCE PAY BY "+lhpvBal.getLbBankCode()+" BANK");
//									mainCashStmt.setCsDescription("as per lhpv");
//									if(lhpvBal.getLbFinalTot()>0){
//										mainCashStmt.setCsDrCr('C');
//										mainCashStmt.setCsAmt(lhpvBal.getLbFinalTot());
//									}else{
//										mainCashStmt.setCsDrCr('D');
//										mainCashStmt.setCsAmt((-1)*(lhpvBal.getLbFinalTot()));
//									}
//									mainCashStmt.setCsType("LHPV PAYMENT");
//									mainCashStmt.setCsVouchType("RTGS");
//									mainCashStmt.setCsChequeType(lhpvBal.getLbChqType());
//									/*
//									 * mainCashStmt.setCsFaCode(lhpvBal.
//									 * getLbBankCode());
//									 */
//									if (stfCode != null && stfCode != "") {
//										mainCashStmt.setCsFaCode(stfCode);
//										System.out.println("setCsFaCode="
//												+ stfCode);
//									} else {
//										mainCashStmt.setCsFaCode(lhpvBal
//												.getLbBrkOwn());
//										System.out.println("setCsFaCode="
//												+ lhpvBal
//												.getLbBrkOwn());
//									}
//									//mainCashStmt.setCsFaCode(lhpvBal.getLbBrkOwn());
//									mainCashStmt.setCsTvNo(tvNo);
//									mainCashStmt.setPayMode("R");
//									mainCashStmt.setCsDt(sqlDate);
//									mainCashStmt.setCsVouchNo(voucherNo);
//
//									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
//									mainCashStmt.setCsNo(actCss);
//									actCss.getCashStmtList().add(mainCashStmt);
//									session.merge(actCss);
//
//									if (lhpvBal.getLbTotPayAmt() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbTotPayAmt());
//										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbTotPayAmt() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*(lhpvBal.getLbTotPayAmt()));
//										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvBal.getLbCashDiscR() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbCashDiscR() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbCashDiscR());
//										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(csDisFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV CASH DISCOUNT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//
//									if (lhpvBal.getLbTdsR() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									} else if (lhpvBal.getLbTdsR() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbTdsR());
//										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(tdsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV TDS");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvBal.getLbMunsR() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvBal.getLbMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setPayMode("R");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (lhpvBal.getLbMunsR() < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*lhpvBal.getLbMunsR());
//										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(munsFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setCsType("LHPV MUNSIANA");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setPayMode("R");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (claim > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(claim);
//										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(clmFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}else if (claim < 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt((-1)*claim);
//										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(clmFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									voucherNo = voucherNo + 1;
//								} else {
//									System.out
//											.println("invalid payment method for lhpv balance");
//								}
//								// }else{
//								// Lhpv Adv for Challan of other branch
//
//								// }
//								lhpvBal.setLbIsClose(true);
//								session.merge(lhpvBal);
//							}
//						}
//
//						if (!lhpvSupList.isEmpty()) {
//							for (int i = 0; i < lhpvSupList.size(); i++) {
//								LhpvSup lhpvSup = lhpvSupList.get(i);
//								// Challan challan = lhpvSup.getChallan();
//
//								// if(challan.getBranchCode().equalsIgnoreCase(actLhpvStatus.getbCode())){
//								// Lhpv Adv for Challan of same branch
//								if (lhpvSup.getLspPayMethod() == 'C') {
//									sameBrhLhPay_C = sameBrhLhPay_C
//											+ lhpvSup.getLspTotPayAmt();
//									sameBrhClm_C = sameBrhClm_C
//											+ lhpvSup.getLspTotRcvrAmt();
//								} else if (lhpvSup.getLspPayMethod() == 'Q') {
//									sameBrhLhPay_Q = sameBrhLhPay_Q
//											+ lhpvSup.getLspTotPayAmt();
//									sameBrhClm_Q = sameBrhClm_Q
//											+ lhpvSup.getLspTotRcvrAmt();
//
//									CashStmt mainCashStmt = new CashStmt();
//									mainCashStmt.setbCode(currentUser
//											.getUserBranchCode());
//									mainCashStmt.setUserCode(currentUser
//											.getUserCode());
//									// mainCashStmt.setCsDescription("LHPV SUPPLEMENTARY PAY BY "+lhpvSup.getLspBankCode()+" BANK");
//									mainCashStmt
//											.setCsDescription("as per lhpv");
//									mainCashStmt.setCsDrCr('C');
//									mainCashStmt.setCsAmt(lhpvSup
//											.getLspFinalTot());
//									mainCashStmt.setCsType("LHPV PAYMENT");
//									mainCashStmt.setCsVouchType("BANK");
//									mainCashStmt.setCsChequeType(lhpvSup
//											.getLspChqType());
//									mainCashStmt.setCsFaCode(lhpvSup
//											.getLspBankCode());
//									mainCashStmt.setCsTvNo(lhpvSup
//											.getLspChqNo());
//									mainCashStmt.setPayMode("Q");
//									mainCashStmt.setCsDt(sqlDate);
//									mainCashStmt.setCsVouchNo(voucherNo);
//
//									CashStmtStatus actCss = (CashStmtStatus) session
//											.get(CashStmtStatus.class,
//													cashStmtStatus.getCssId());
//									mainCashStmt.setCsNo(actCss);
//									actCss.getCashStmtList().add(mainCashStmt);
//									session.merge(actCss);
//
//									if (lhpvSup.getLspTotPayAmt() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvSup
//												.getLspTotPayAmt());
//										// cashStmt.setCsDescription("LHPV Payment By "+lhpvSup.getLspBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvSup
//												.getLspChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvSup.getLspTotRcvrAmt() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvSup
//												.getLspTotRcvrAmt());
//										// cashStmt.setCsDescription("LHPV Claim By "+lhpvSup.getLspBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(clmFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(lhpvSup
//												.getLspChqNo());
//										cashStmt.setPayMode("Q");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("BANK");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									voucherNo = voucherNo + 1;
//
//								} else if (lhpvSup.getLspPayMethod() == 'R') {
//									sameBrhLhPay_R = sameBrhLhPay_R
//											+ lhpvSup.getLspTotPayAmt();
//									sameBrhClm_R = sameBrhClm_R
//											+ lhpvSup.getLspTotRcvrAmt();
//
//									CashStmt mainCashStmt = new CashStmt();
//									mainCashStmt.setbCode(currentUser
//											.getUserBranchCode());
//									mainCashStmt.setUserCode(currentUser
//											.getUserCode());
//									// mainCashStmt.setCsDescription("LHPV SUPPLEMENTARY PAY BY "+lhpvSup.getLspBankCode()+" BANK");
//									mainCashStmt
//											.setCsDescription("as per lhpv");
//									mainCashStmt.setCsDrCr('C');
//									mainCashStmt.setCsAmt(lhpvSup
//											.getLspFinalTot());
//									mainCashStmt.setCsType("LHPV PAYMENT");
//									mainCashStmt.setCsVouchType("RTGS");
//									mainCashStmt.setCsChequeType(lhpvSup
//											.getLspChqType());
//									/*
//									 * mainCashStmt.setCsFaCode(lhpvSup.
//									 * getLspBankCode());
//									 */
//									mainCashStmt.setCsFaCode(lhpvSup
//											.getLspBrkOwn());
//									mainCashStmt.setCsTvNo(tvNo);
//									mainCashStmt.setPayMode("R");
//									mainCashStmt.setCsDt(sqlDate);
//									mainCashStmt.setCsVouchNo(voucherNo);
//
//									CashStmtStatus actCss = (CashStmtStatus) session
//											.get(CashStmtStatus.class,
//													cashStmtStatus.getCssId());
//									mainCashStmt.setCsNo(actCss);
//									actCss.getCashStmtList().add(mainCashStmt);
//									session.merge(actCss);
//
//									if (lhpvSup.getLspTotPayAmt() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvSup
//												.getLspTotPayAmt());
//										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvSup.getLspBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('D');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(lryFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									if (lhpvSup.getLspTotRcvrAmt() > 0) {
//										CashStmt cashStmt = new CashStmt();
//										cashStmt.setbCode(currentUser
//												.getUserBranchCode());
//										cashStmt.setCsAmt(lhpvSup
//												.getLspTotRcvrAmt());
//										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvSup.getLspBankCode()+" Bank");
//										cashStmt.setCsDescription("as per lhpv");
//										cashStmt.setCsDrCr('C');
//										cashStmt.setCsDt(sqlDate);
//										cashStmt.setCsFaCode(clmFaCode);
//										cashStmt.setCsIsClose(false);
//										cashStmt.setCsTvNo(tvNo);
//										cashStmt.setPayMode("R");
//										cashStmt.setCsType("LHPV PAYMENT");
//										cashStmt.setCsVouchNo(voucherNo);
//										cashStmt.setCsVouchType("RTGS");
//										cashStmt.setUserCode(currentUser
//												.getUserCode());
//
//										CashStmtStatus actCss1 = (CashStmtStatus) session
//												.get(CashStmtStatus.class,
//														cashStmtStatus
//																.getCssId());
//										cashStmt.setCsNo(actCss1);
//										actCss1.getCashStmtList().add(cashStmt);
//										session.merge(actCss1);
//									}
//
//									voucherNo = voucherNo + 1;
//								} else {
//									System.out
//											.println("invalid payment method for lhpv Supplementary");
//								}
//								// }else{
//								// Lhpv Adv for Challan of other branch
//
//								// }
//
//								lhpvSup.setLspIsClose(true);
//								session.merge(lhpvSup);
//							}
//						}
//
//						System.out.println("Kamal21");
//						// Same Branch entry in Cash Statement
//						List<LhpvCashSmry> lcsList = new ArrayList<>();
//						cr = session.createCriteria(LhpvCashSmry.class);
//						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,
//								actLhpvStatus.getLsDt()));
//						cr.add(Restrictions.eq(LhpvCashSmryCNTS.B_CODE,
//								actLhpvStatus.getbCode()));
//						lcsList = cr.list();
//
//						double todayPay = 0;
//						double advLHPay = 0;
//						if (!lcsList.isEmpty()) {
//							System.out.println("Kamal22");
//							for (int i = 0; i < lcsList.size(); i++) {
//								if (lcsList.get(i).isLcsToday() == true
//										&& lcsList.get(i).isLcsCsEntr() == false
//										&& lcsList.get(i).isLcsAdvEntr() == false) {
//									todayPay = todayPay
//											+ lcsList.get(i).getLcsPayAmt();
//									System.out.println("Kamal23");
//								} else if (lcsList.get(i).isLcsToday() == false
//										&& lcsList.get(i).isLcsCsEntr() == false
//										&& lcsList.get(i).isLcsAdvEntr() == true) {
//									advLHPay = advLHPay
//											+ lcsList.get(i).getLcsPayAmt();
//									System.out.println("Kamal24");
//								} else {
//									System.out.println("Kamal25");
//								}
//
//								LhpvCashSmry lhpvCashSmry = lcsList.get(i);
//								lhpvCashSmry.setLcsCsEntr(true);
//								session.merge(lhpvCashSmry);
//							}
//							System.out.println("Kamal26");
//						}
//
//						if (advLHPay > 0) {
//							System.out.println("advLHPay>0=" + advLHPay);
//							CashStmt cashStmt = new CashStmt();
//							cashStmt.setbCode(currentUser.getUserBranchCode());
//							cashStmt.setCsAmt(advLHPay);
//							// cashStmt.setCsDescription("LHPV Payment By Cash");
//							cashStmt.setCsDescription("as per lhpv");
//							cashStmt.setCsDrCr('D');
//							cashStmt.setCsDt(sqlDate);
//							cashStmt.setCsFaCode(lryFaCode);
//							cashStmt.setCsIsClose(false);
//							cashStmt.setCsTvNo(tvNo);
//							cashStmt.setPayMode("C");
//							cashStmt.setCsType("LHPV PAYMENT");
//							cashStmt.setCsVouchNo(voucherNo);
//							cashStmt.setCsVouchType("CASH");
//							cashStmt.setUserCode(currentUser.getUserCode());
//
//							CashStmtStatus actCss = (CashStmtStatus) session
//									.get(CashStmtStatus.class,
//											cashStmtStatus.getCssId());
//							cashStmt.setCsNo(actCss);
//							actCss.getCashStmtList().add(cashStmt);
//							session.merge(actCss);
//
//							CashStmt cashStmt1 = new CashStmt();
//							cashStmt1.setbCode(currentUser.getUserBranchCode());
//							cashStmt1.setCsAmt(advLHPay);
//							// cashStmt1.setCsDescription("LHPV Other Payment By Cash");
//							cashStmt1.setCsDescription("as per lhpv");
//							cashStmt1.setCsDrCr('C');
//							cashStmt1.setCsDt(sqlDate);
//							cashStmt1.setCsFaCode(lryAdvOth);
//							cashStmt1.setCsIsClose(false);
//							cashStmt1.setCsTvNo(tvNo);
//							cashStmt1.setPayMode("C");
//							cashStmt1.setCsType("LHPV PAYMENT");
//							cashStmt1.setCsVouchNo(voucherNo);
//							cashStmt1.setCsVouchType("CASH");
//							cashStmt1.setUserCode(currentUser.getUserCode());
//
//							CashStmtStatus actCss1 = (CashStmtStatus) session
//									.get(CashStmtStatus.class,
//											cashStmtStatus.getCssId());
//							cashStmt1.setCsNo(actCss1);
//							actCss1.getCashStmtList().add(cashStmt1);
//							session.merge(actCss1);
//						}
//						System.out.println("Kamal26");
//						if (sameBrhLhPay_C > 0) {
//							System.out.println("Kamal27");
//							double lhOthAmt = 0;
//							lhOthAmt = sameBrhLhPay_C - todayPay;
//
//							if (todayPay > 0) {
//								System.out.println("todayPay>0=" + todayPay);
//								CashStmt cashStmt = new CashStmt();
//								cashStmt.setbCode(currentUser
//										.getUserBranchCode());
//								cashStmt.setCsAmt(todayPay);
//								// cashStmt.setCsDescription("LHPV Payment By Cash");
//								cashStmt.setCsDescription("as per lhpv");
//								cashStmt.setCsDrCr('D');
//								cashStmt.setCsDt(sqlDate);
//								cashStmt.setCsFaCode(lryFaCode);
//								cashStmt.setCsIsClose(false);
//								cashStmt.setCsTvNo(tvNo);
//								cashStmt.setPayMode("C");
//								cashStmt.setCsType("LHPV PAYMENT");
//								cashStmt.setCsVouchNo(voucherNo);
//								cashStmt.setCsVouchType("CASH");
//								cashStmt.setUserCode(currentUser.getUserCode());
//
//								CashStmtStatus actCss = (CashStmtStatus) session
//										.get(CashStmtStatus.class,
//												cashStmtStatus.getCssId());
//								cashStmt.setCsNo(actCss);
//								actCss.getCashStmtList().add(cashStmt);
//								session.merge(actCss);
//							}
//
//							if (lhOthAmt > 0) {
//								System.out.println("lhOthAmt>0=" + lhOthAmt);
//								CashStmt cashStmt = new CashStmt();
//								cashStmt.setbCode(currentUser
//										.getUserBranchCode());
//								cashStmt.setCsAmt(lhOthAmt);
//								// cashStmt.setCsDescription("LHPV Other Payment By Cash");
//								cashStmt.setCsDescription("as per lhpv");
//								cashStmt.setCsDrCr('D');
//								cashStmt.setCsDt(sqlDate);
//								cashStmt.setCsFaCode(lryAdvOth);
//								cashStmt.setCsIsClose(false);
//								cashStmt.setCsTvNo(tvNo);
//								cashStmt.setPayMode("C");
//								cashStmt.setCsType("LHPV PAYMENT");
//								cashStmt.setCsVouchNo(voucherNo);
//								cashStmt.setCsVouchType("CASH");
//								cashStmt.setUserCode(currentUser.getUserCode());
//
//								CashStmtStatus actCss = (CashStmtStatus) session
//										.get(CashStmtStatus.class,
//												cashStmtStatus.getCssId());
//								cashStmt.setCsNo(actCss);
//								actCss.getCashStmtList().add(cashStmt);
//								session.merge(actCss);
//							}
//
//						}
//
//						if (sameBrhCD_C > 0) {
//							System.out.println("sameBrhCD_C>0=" + sameBrhCD_C);
//							CashStmt cashStmt = new CashStmt();
//							cashStmt.setbCode(currentUser.getUserBranchCode());
//							cashStmt.setCsAmt(sameBrhCD_C);
//							// cashStmt.setCsDescription("LHPV Cash Discount By Cash");
//							cashStmt.setCsDescription("as per lhpv");
//							cashStmt.setCsDrCr('C');
//							cashStmt.setCsDt(sqlDate);
//							cashStmt.setCsFaCode(csDisFaCode);
//							cashStmt.setCsIsClose(false);
//							cashStmt.setCsTvNo(tvNo);
//							cashStmt.setPayMode("C");
//							cashStmt.setCsType("LHPV CASH DISCOUNT");
//							cashStmt.setCsVouchNo(voucherNo);
//							cashStmt.setCsVouchType("CASH");
//							cashStmt.setUserCode(currentUser.getUserCode());
//
//							CashStmtStatus actCss = (CashStmtStatus) session
//									.get(CashStmtStatus.class,
//											cashStmtStatus.getCssId());
//							cashStmt.setCsNo(actCss);
//							actCss.getCashStmtList().add(cashStmt);
//							session.merge(actCss);
//						}
//
//						if (sameBrhTds_C > 0) {
//							System.out
//									.println("sameBrhTds_C>0=" + sameBrhTds_C);
//							CashStmt cashStmt = new CashStmt();
//							cashStmt.setbCode(currentUser.getUserBranchCode());
//							cashStmt.setCsAmt(sameBrhTds_C);
//							// cashStmt.setCsDescription("LHPV TDS By Cash");
//							cashStmt.setCsDescription("as per lhpv");
//							cashStmt.setCsDrCr('C');
//							cashStmt.setCsDt(sqlDate);
//							cashStmt.setCsFaCode(tdsFaCode);
//							cashStmt.setCsIsClose(false);
//							cashStmt.setCsTvNo(tvNo);
//							cashStmt.setPayMode("C");
//							cashStmt.setCsType("LHPV TDS");
//							cashStmt.setCsVouchNo(voucherNo);
//							cashStmt.setCsVouchType("CASH");
//							cashStmt.setUserCode(currentUser.getUserCode());
//
//							CashStmtStatus actCss = (CashStmtStatus) session
//									.get(CashStmtStatus.class,
//											cashStmtStatus.getCssId());
//							cashStmt.setCsNo(actCss);
//							actCss.getCashStmtList().add(cashStmt);
//							session.merge(actCss);
//						}
//
//						if (sameBrhMuns_C > 0) {
//							System.out.println("sameBrhMuns_C>0="
//									+ sameBrhMuns_C);
//							CashStmt cashStmt = new CashStmt();
//							cashStmt.setbCode(currentUser.getUserBranchCode());
//							cashStmt.setCsAmt(sameBrhMuns_C);
//							// cashStmt.setCsDescription("LHPV Munsiana By Cash");
//							cashStmt.setCsDescription("as per lhpv");
//							cashStmt.setCsDrCr('C');
//							cashStmt.setCsDt(sqlDate);
//							cashStmt.setCsFaCode(munsFaCode);
//							cashStmt.setCsIsClose(false);
//							cashStmt.setCsTvNo(tvNo);
//							cashStmt.setPayMode("C");
//							cashStmt.setCsType("LHPV MUNSIANA");
//							cashStmt.setCsVouchNo(voucherNo);
//							cashStmt.setCsVouchType("CASH");
//							cashStmt.setUserCode(currentUser.getUserCode());
//
//							CashStmtStatus actCss = (CashStmtStatus) session
//									.get(CashStmtStatus.class,
//											cashStmtStatus.getCssId());
//							cashStmt.setCsNo(actCss);
//							actCss.getCashStmtList().add(cashStmt);
//							session.merge(actCss);
//						}
//
//						if (sameBrhClm_C > 0) {
//							System.out.println("sameBrhClm_C>0" + sameBrhClm_C);
//							CashStmt cashStmt = new CashStmt();
//							cashStmt.setbCode(currentUser.getUserBranchCode());
//							cashStmt.setCsAmt(sameBrhClm_C);
//							// cashStmt.setCsDescription("LHPV Claim By Cash");
//							cashStmt.setCsDescription("as per lhpv");
//							cashStmt.setCsDrCr('C');
//							cashStmt.setCsDt(sqlDate);
//							cashStmt.setCsFaCode(clmFaCode);
//							cashStmt.setCsIsClose(false);
//							cashStmt.setCsTvNo(tvNo);
//							cashStmt.setPayMode("C");
//							cashStmt.setCsType("LHPV PAYMENT");
//							cashStmt.setCsVouchNo(voucherNo);
//							cashStmt.setCsVouchType("CASH");
//							cashStmt.setUserCode(currentUser.getUserCode());
//
//							CashStmtStatus actCss = (CashStmtStatus) session
//									.get(CashStmtStatus.class,
//											cashStmtStatus.getCssId());
//							cashStmt.setCsNo(actCss);
//							actCss.getCashStmtList().add(cashStmt);
//							session.merge(actCss);
//						}
//
//						actLhpvStatus.setLsClose(true);
//						session.merge(actLhpvStatus);
//						res = voucherNo;
//					}
//
//				}
//				session.flush();
//				session.clear();
//				transaction.commit();
//
//			} catch (Exception e) {
//				transaction.rollback();
//				e.printStackTrace();
//				return 0;
//			}finally{
//				session.close();	
//			}
//			
//		} else {
//			res = 0;
//		}
//		return res;
//	}

	
	
	
	
	
	
	//TODO start intoCs
	public int closeLhpvIntoCS(LhpvStatus lhpvStatus,Session session) {
		System.out.println("enter into closeLhpv funciton");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		int res = 0;

		int lsId = lhpvStatus.getLsId();
		if (lsId > 0) {

			List<LhpvStatus> lsList = new ArrayList<>();
				Criteria cr = session.createCriteria(LhpvStatus.class);
				cr.add(Restrictions.eq(LhpvStatusCNTS.LS_ID, lsId));
				lsList = cr.list();

				Branch curBrh = (Branch) session.get(Branch.class,
						Integer.parseInt(currentUser.getUserBranchCode()));

				String lryFaCode = "";
				String csDisFaCode = "";
				String tdsFaCode = "";
				String munsFaCode = "";
				String clmFaCode = "";
				String lryAdvOth = "";
				List<FAMaster> famList = new ArrayList<>();
				/*
				 * cr = session.createCriteria(FAMaster.class);
				 * cr.add(Restrictions
				 * .or(FAMasterCNTS.FA_MFA_NAME,FAMasterCNTS.FA_MFA_NAME
				 * ));//(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LORRY_HIRE));
				 */
				famList = session
						.createCriteria(FAMaster.class)
						.add(Restrictions.disjunction().add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,
										ConstantsValues.LORRY_HIRE))
										.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.CASH_DIS_LH))
								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TDS_LRY_HIRE))
								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.MUNS_LRY_HIRE))
								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.CLAIM_LRY_HIRE))
								.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LRYADV_AND_OTH)))
						.list();

				if (!famList.isEmpty()) {
					for (int i = 0; i < famList.size(); i++) {
						if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.LORRY_HIRE)) {
							lryFaCode = famList.get(i).getFaMfaCode();
						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.CASH_DIS_LH)) {
							csDisFaCode = famList.get(i).getFaMfaCode();
						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.TDS_LRY_HIRE)) {
							tdsFaCode = famList.get(i).getFaMfaCode();
						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.MUNS_LRY_HIRE)) {
							munsFaCode = famList.get(i).getFaMfaCode();
						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.CLAIM_LRY_HIRE)) {
							clmFaCode = famList.get(i).getFaMfaCode();
						} else if (famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.LRYADV_AND_OTH)) {
							lryAdvOth = famList.get(i).getFaMfaCode();
						} else {
							System.out.println("invalid result");
						}
					}
				}

				if (!lsList.isEmpty()) {

					LhpvStatus actLhpvStatus = lsList.get(0);
					List<LhpvAdv> lhpvAdvList = actLhpvStatus.getLaList();
					List<LhpvBal> lhpvBalList = actLhpvStatus.getLbList();
					List<LhpvSup> lhpvSupList = actLhpvStatus.getLspList();

					double sameBrhLhPay_C = 0.0;
					double sameBrhCD_C = 0.0;
					double sameBrhTds_C = 0.0;
					double sameBrhMuns_C = 0.0;
					double sameBrhClm_C = 0.0;

					double sameBrhLhPay_Q = 0.0;
					double sameBrhCD_Q = 0.0;
					double sameBrhTds_Q = 0.0;
					double sameBrhMuns_Q = 0.0;
					double sameBrhClm_Q = 0.0;

					double sameBrhLhPay_R = 0.0;
					double sameBrhCD_R = 0.0;
					double sameBrhTds_R = 0.0;
					double sameBrhMuns_R = 0.0;
					double sameBrhClm_R = 0.0;

					List<CashStmtStatus> cashStmtStatusList = new ArrayList<>();
					Query query = session
							.createQuery("from CashStmtStatus where bCode= :type and cssDt = :date");
					query.setParameter("type", currentUser.getUserBranchCode());
					query.setParameter("date", lsList.get(0).getLsDt());
					cashStmtStatusList = query.setMaxResults(1).list();
					java.sql.Date sqlDate = new java.sql.Date(
							cashStmtStatusList.get(0).getCssDt().getTime());

					if (!cashStmtStatusList.isEmpty()) {
						CashStmtStatus cashStmtStatus = cashStmtStatusList
								.get(0);
						int voucherNo = 0;
						List<CashStmt> csVouchList = new ArrayList<CashStmt>();
						csVouchList = cashStmtStatus.getCashStmtList();

						if (!csVouchList.isEmpty()) {
							for (int i = 0; i < csVouchList.size(); i++) {
								if (voucherNo < csVouchList.get(i).getCsVouchNo()) {
									voucherNo = csVouchList.get(i).getCsVouchNo();
								}
							}

							voucherNo = voucherNo + 1;
						} else {
							voucherNo = 1;
						}
						/*
						 * cr = session.createCriteria(CashStmt.class);
						 * cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,
						 * sqlDate));
						 * cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE
						 * ,currentUser.getUserBranchCode())); ProjectionList
						 * proList = Projections.projectionList();
						 * proList.add(Projections.property("csVouchNo"));
						 * cr.setProjection(proList);
						 * 
						 * List<Integer> voucherNoList = cr.list(); int
						 * voucherNo = 0; if (!voucherNoList.isEmpty()) { int
						 * vNo = voucherNoList.get(voucherNoList.size() - 1);
						 * voucherNo = vNo + 1; } else { voucherNo = 1; }
						 */

						if (!lhpvAdvList.isEmpty()) {
							for (int i = 0; i < lhpvAdvList.size(); i++) {
								LhpvAdv lhpvAdv = lhpvAdvList.get(i);
								double tempLaTotPayAmt;
								// char payTo=lhpvAdv.getPayToStf();
								String stfCode = lhpvAdv.getStafCode();

								if (lhpvAdv.getLaTotPayAmt() < 0) {
									tempLaTotPayAmt = (-1)* (lhpvAdv.getLaTotPayAmt());
								} else {
									tempLaTotPayAmt = lhpvAdv.getLaTotPayAmt();
								}
								// Challan challan = lhpvAdv.getChallan();
								System.out.println("tempLaTotPayAmt="
										+ tempLaTotPayAmt);
								// if(challan.getBranchCode().equalsIgnoreCase(actLhpvStatus.getbCode())){
								// Lhpv Adv for Challan of same branch
								if (lhpvAdv.getLaPayMethod() == 'C') {
									sameBrhLhPay_C = sameBrhLhPay_C	+ lhpvAdv.getLaTotPayAmt();
									sameBrhCD_C = sameBrhCD_C + lhpvAdv.getLaCashDiscR();
									sameBrhTds_C = sameBrhTds_C	+ lhpvAdv.getLaTdsR();
									sameBrhMuns_C = sameBrhMuns_C + lhpvAdv.getLaMunsR();
									System.out.println("Kamal1");
								} else if (lhpvAdv.getLaPayMethod() == 'Q') {
									System.out.println("Kamal2");
									sameBrhLhPay_Q = sameBrhLhPay_Q	+ lhpvAdv.getLaTotPayAmt();
									sameBrhCD_Q = sameBrhCD_Q + lhpvAdv.getLaCashDiscR();
									sameBrhTds_Q = sameBrhTds_Q	+ lhpvAdv.getLaTdsR();
									sameBrhMuns_Q = sameBrhMuns_Q + lhpvAdv.getLaMunsR();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser.getUserCode());
									// mainCashStmt.setCsDescription("LHPV ADVANCE PAY BY "+lhpvAdv.getLaBankCode()+" BANK");
									mainCashStmt.setCsDescription("as per lhpv");
									if (lhpvAdv.getLaTotPayAmt() < 0) {
										mainCashStmt.setCsDrCr('D');
										mainCashStmt.setCsAmt((-1)*(lhpvAdv.getLaFinalTot()));
									} else {
										mainCashStmt.setCsDrCr('C');
										mainCashStmt.setCsAmt(lhpvAdv.getLaFinalTot());
									}

									//mainCashStmt.setCsAmt(tempLaTotPayAmt);
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("BANK");
									mainCashStmt.setCsChequeType(lhpvAdv.getLaChqType());
									mainCashStmt.setCsFaCode(lhpvAdv.getLaBankCode());
									// mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);
									mainCashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
									mainCashStmt.setPayMode("Q");

									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvAdv.getLaTotPayAmt() > 0) {
										System.out.println("Kamal3");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(tempLaTotPayAmt);
										// cashStmt.setCsDescription("LHPV Payment By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaTotPayAmt() < 0) {
										System.out.println("Kamal5");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										// double
										// tempLaTotPayAmt=(-1)*(lhpvAdv.getLaTotPayAmt());
										cashStmt.setCsAmt(tempLaTotPayAmt);
										// cashStmt.setCsDescription("LHPV Payment By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);

									}

									if (lhpvAdv.getLaCashDiscR() > 0) {
										System.out.println("Kamal6");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaCashDiscR() < 0) {
										System.out.println("Kamal7");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1) * lhpvAdv.getLaCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaTdsR() > 0) {
										System.out.println("Kamal8");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaTdsR());
										// cashStmt.setCsDescription("LHPV TDS By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaTdsR() < 0) {
										System.out.println("Kamal9");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)
												* lhpvAdv.getLaTdsR());
										// cashStmt.setCsDescription("LHPV TDS By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaMunsR() > 0) {
										System.out.println("Kamal10");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaMunsR() < 0) {
										System.out.println("Kamal11");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1) * lhpvAdv.getLaMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvAdv.getLaChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;

								} else if (lhpvAdv.getLaPayMethod() == 'R') {
									System.out.println("Kamal12");
									sameBrhLhPay_R = sameBrhLhPay_R	+ lhpvAdv.getLaTotPayAmt();
									sameBrhCD_R = sameBrhCD_R + lhpvAdv.getLaCashDiscR();
									sameBrhTds_R = sameBrhTds_R	+ lhpvAdv.getLaTdsR();
									sameBrhMuns_R = sameBrhMuns_R + lhpvAdv.getLaMunsR();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser.getUserCode());
									// mainCashStmt.setCsDescription("LHPV ADVANCE PAY BY "+lhpvAdv.getLaBankCode()+" BANK");
									mainCashStmt.setCsDescription("as per lhpv");
									// mainCashStmt.setCsDrCr('C');
									Double tempFinlTot;
									if (lhpvAdv.getLaTotPayAmt() < 0) {
										mainCashStmt.setCsDrCr('D');
										tempFinlTot = (-1) * (lhpvAdv.getLaFinalTot());
									} else {
										mainCashStmt.setCsDrCr('C');
										tempFinlTot = lhpvAdv.getLaFinalTot();
									}
									mainCashStmt.setCsAmt(tempFinlTot);
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("RTGS");
									mainCashStmt.setCsChequeType(lhpvAdv.getLaChqType());
									// mainCashStmt.setCsFaCode(lhpvAdv.getLaBankCode());
									if (stfCode != null && stfCode != "") {
										mainCashStmt.setCsFaCode(stfCode);
										System.out.println("setCsFaCode=" + stfCode);
									} else if(lhpvAdv.getLaBankCode() != null && !lhpvAdv.getLaBankCode().isEmpty()) {
										mainCashStmt.setCsFaCode(lhpvAdv.getLaBankCode());
										System.out.println("setCsFaCode=" + lhpvAdv.getLaBankCode());
									} else {
										mainCashStmt.setCsFaCode(lhpvAdv.getLaBrkOwn());
										System.out.println("setCsFaCode=" + lhpvAdv.getLaBrkOwn());
									}

									mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setPayMode("R");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvAdv.getLaTotPayAmt() > 0) {
										System.out.println("Kamal13");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(tempLaTotPayAmt);
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaTotPayAmt() < 0) {
										System.out.println("Kamal14");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(tempLaTotPayAmt);
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaCashDiscR() > 0) {
										System.out.println("Kamal15");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaCashDiscR() < 0) {
										System.out.println("Kamal16");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1) * lhpvAdv.getLaCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaTdsR() > 0) {
										System.out.println("Kamal17");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaTdsR() < 0) {
										System.out.println("Kamal18");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)
												* lhpvAdv.getLaTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaMunsR() > 0) {
										System.out.println("Kamal19");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaMunsR() < 0) {
										System.out.println("Kamal20");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)
												* lhpvAdv.getLaMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;
								}else if (lhpvAdv.getLaPayMethod() == 'P') {
									System.out.println("Kamalp12");
									sameBrhLhPay_R = sameBrhLhPay_R	+ lhpvAdv.getLaTotPayAmt();
									sameBrhCD_R = sameBrhCD_R + lhpvAdv.getLaCashDiscR();
									sameBrhTds_R = sameBrhTds_R	+ lhpvAdv.getLaTdsR();
									sameBrhMuns_R = sameBrhMuns_R + lhpvAdv.getLaMunsR();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser.getUserCode());
									// mainCashStmt.setCsDescription("LHPV ADVANCE PAY BY "+lhpvAdv.getLaBankCode()+" BANK");
									mainCashStmt.setCsDescription("as per lhpv Petro");
									// mainCashStmt.setCsDrCr('C');
									Double tempFinlTot;
									if (lhpvAdv.getLaTotPayAmt() < 0) {
										mainCashStmt.setCsDrCr('D');
										tempFinlTot = (-1) * (lhpvAdv.getLaFinalTot());
									} else {
										mainCashStmt.setCsDrCr('C');
										tempFinlTot = lhpvAdv.getLaFinalTot();
									}
									mainCashStmt.setCsAmt(tempFinlTot);
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("PETRO");
									mainCashStmt.setCsChequeType(lhpvAdv.getLaChqType());
									// mainCashStmt.setCsFaCode(lhpvAdv.getLaBankCode());
									//if (stfCode != null && stfCode != "") {
										mainCashStmt.setCsFaCode(stfCode);
										System.out.println("setCsFaCode=" + stfCode);
									/*} else {
										mainCashStmt.setCsFaCode(lhpvAdv.getLaBrkOwn());
										System.out.println("setCsFaCode=" + lhpvAdv.getLaBrkOwn());
									}*/

									mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setPayMode("P");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvAdv.getLaTotPayAmt() > 0) {
										System.out.println("Kamal13");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(tempLaTotPayAmt);
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaTotPayAmt() < 0) {
										System.out.println("Kamal14");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(tempLaTotPayAmt);
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaCashDiscR() > 0) {
										System.out.println("Kamal15");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaCashDiscR() < 0) {
										System.out.println("Kamal16");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1) * lhpvAdv.getLaCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaTdsR() > 0) {
										System.out.println("Kamal17");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaTdsR() < 0) {
										System.out.println("Kamal18");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)
												* lhpvAdv.getLaTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvAdv.getLaMunsR() > 0) {
										System.out.println("Kamal19");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvAdv.getLaMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvAdv.getLaMunsR() < 0) {
										System.out.println("Kamal20");
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)
												* lhpvAdv.getLaMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvAdv.getLaBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv Petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;
								} else {
									System.out
											.println("invalid payment method for lhpv advance");
								}

								// }else{
								// Lhpv Adv for Challan of other branch

								// }

								lhpvAdv.setLaIsClose(true);
								session.merge(lhpvAdv);
							}
						}

						if (!lhpvBalList.isEmpty()) {
							for (int i = 0; i < lhpvBalList.size(); i++) {
								LhpvBal lhpvBal = lhpvBalList.get(i);
								// Challan challan = lhpvBal.getChallan();
								String stfCode = lhpvBal.getStafCode();
								double tempLbTotPayAmt;
								// char payTo=lhpvAdv.getPayToStf();
								//String stfCode = lhpvbal.getStafCode();

								if (lhpvBal.getLbFinalTot() < 0) {
									tempLbTotPayAmt = (-1)* (lhpvBal.getLbFinalTot());
								} else {
									tempLbTotPayAmt = lhpvBal.getLbFinalTot();
								}
								// Challan challan = lhpvAdv.getChallan();
								System.out.println("tempLbTotPayAmt="
										+ tempLbTotPayAmt);

								// if(challan.getBranchCode().equalsIgnoreCase(actLhpvStatus.getbCode())){
								// Lhpv Adv for Challan of same branch
								if (lhpvBal.getLbPayMethod() == 'C') {
									sameBrhLhPay_C = sameBrhLhPay_C + lhpvBal.getLbTotPayAmt();
									sameBrhCD_C = sameBrhCD_C + lhpvBal.getLbCashDiscR();
									sameBrhTds_C = sameBrhTds_C	+ lhpvBal.getLbTdsR();
									sameBrhMuns_C = sameBrhMuns_C + lhpvBal.getLbMunsR();
									sameBrhClm_C = sameBrhClm_C	+ lhpvBal.getLbWtShrtgCR()+ lhpvBal.getLbLateDelCR()+ lhpvBal.getLbLateAckCR()+ lhpvBal.getLbOthMiscP()+ lhpvBal.getLbDrRcvrWtCR();
								} else if (lhpvBal.getLbPayMethod() == 'Q') {
									double claim = 0.0;
									sameBrhLhPay_Q = sameBrhLhPay_Q + lhpvBal.getLbTotPayAmt();
									sameBrhCD_Q = sameBrhCD_Q + lhpvBal.getLbCashDiscR();
									sameBrhTds_Q = sameBrhTds_Q	+ lhpvBal.getLbTdsR();
									sameBrhMuns_Q = sameBrhMuns_Q + lhpvBal.getLbMunsR();
									claim = lhpvBal.getLbWtShrtgCR()+ lhpvBal.getLbLateDelCR()+ lhpvBal.getLbLateAckCR()+ lhpvBal.getLbOthMiscP()+ lhpvBal.getLbDrRcvrWtCR();
									sameBrhClm_Q = sameBrhClm_Q	+ lhpvBal.getLbWtShrtgCR()+ lhpvBal.getLbLateDelCR()+ lhpvBal.getLbLateAckCR()+ lhpvBal.getLbOthMiscP()+ lhpvBal.getLbDrRcvrWtCR();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser.getUserCode());
									// mainCashStmt.setCsDescription("LHPV BALANCE PAY BY "+lhpvBal.getLbBankCode()+" BANK");
									mainCashStmt.setCsDescription("as per lhpv");
									//mainCashStmt.setCsDrCr('C');
									if (lhpvBal.getLbFinalTot() < 0) {
										mainCashStmt.setCsDrCr('D');
									} else {
										mainCashStmt.setCsDrCr('C');
									}
									//mainCashStmt.setCsAmt(lhpvBal.getLbFinalTot());
									mainCashStmt.setCsAmt(tempLbTotPayAmt);
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("BANK");
									mainCashStmt.setCsChequeType(lhpvBal.getLbChqType());
									mainCashStmt.setCsFaCode(lhpvBal.getLbBankCode());
									mainCashStmt.setCsTvNo(lhpvBal.getLbChqNo());
									mainCashStmt.setPayMode("Q");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvBal.getLbTotPayAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbTotPayAmt() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbCashDiscR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbCashDiscR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbTdsR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbTdsR());
										// cashStmt.setCsDescription("LHPV TDS By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbTdsR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbTdsR());
										// cashStmt.setCsDescription("LHPV TDS By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbMunsR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbMunsR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (claim > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(claim);
										// cashStmt.setCsDescription("LHPV Claim By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (claim < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*claim);
										// cashStmt.setCsDescription("LHPV Claim By "+lhpvBal.getLbBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvBal.getLbChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;

								} else if (lhpvBal.getLbPayMethod() == 'R') {
									double claim = 0.0;
									sameBrhLhPay_R = sameBrhLhPay_R + lhpvBal.getLbTotPayAmt();
									sameBrhCD_R = sameBrhCD_R + lhpvBal.getLbCashDiscR();
									sameBrhTds_R = sameBrhTds_R + lhpvBal.getLbTdsR();
									sameBrhMuns_R = sameBrhMuns_R + lhpvBal.getLbMunsR();
									claim = lhpvBal.getLbWtShrtgCR() + lhpvBal.getLbLateDelCR() + lhpvBal.getLbLateAckCR() + lhpvBal.getLbOthMiscP() + lhpvBal.getLbDrRcvrWtCR();
									sameBrhClm_R = sameBrhClm_R + lhpvBal.getLbWtShrtgCR() + lhpvBal.getLbLateDelCR() + lhpvBal.getLbLateAckCR() + lhpvBal.getLbOthMiscP() + lhpvBal.getLbDrRcvrWtCR();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser.getUserCode());
									// mainCashStmt.setCsDescription("LHPV BALANCE PAY BY "+lhpvBal.getLbBankCode()+" BANK");
									mainCashStmt.setCsDescription("as per lhpv");
									if(lhpvBal.getLbFinalTot()>0){
										mainCashStmt.setCsDrCr('C');
										mainCashStmt.setCsAmt(lhpvBal.getLbFinalTot());
									}else{
										mainCashStmt.setCsDrCr('D');
										mainCashStmt.setCsAmt((-1)*(lhpvBal.getLbFinalTot()));
									}
									mainCashStmt.setCsType("LHPV PAYMENT");
									if(lhpvBal.getLbBankName() != null)//for ho payments
										mainCashStmt.setCsVouchType("BANK");
									else
										mainCashStmt.setCsVouchType("RTGS");
									mainCashStmt.setCsChequeType(lhpvBal.getLbChqType());
									/*
									 * mainCashStmt.setCsFaCode(lhpvBal.
									 * getLbBankCode());
									 */
									if (stfCode != null && stfCode != "") {
										mainCashStmt.setCsFaCode(stfCode);
										System.out.println("setCsFaCode="
												+ stfCode);
									}else if(lhpvBal.getLbBankName() != null){//by kamal for ho payments
										mainCashStmt.setCsFaCode(lhpvBal
												.getLbBankCode());
									}else {
										mainCashStmt.setCsFaCode(lhpvBal
												.getLbBrkOwn());
										System.out.println("setCsFaCode="
												+ lhpvBal
												.getLbBrkOwn());
									}
									//mainCashStmt.setCsFaCode(lhpvBal.getLbBrkOwn());
									mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setPayMode("R");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvBal.getLbTotPayAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbTotPayAmt() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*(lhpvBal.getLbTotPayAmt()));
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbCashDiscR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbCashDiscR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}


									if (lhpvBal.getLbTdsR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvBal.getLbTdsR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbMunsR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setPayMode("R");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbMunsR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setPayMode("R");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (claim > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(claim);
										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (claim < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*claim);
										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else
											cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;
								}else if (lhpvBal.getLbPayMethod() == 'P') {
									double claim = 0.0;
									sameBrhLhPay_R = sameBrhLhPay_R + lhpvBal.getLbTotPayAmt();
									sameBrhCD_R = sameBrhCD_R + lhpvBal.getLbCashDiscR();
									sameBrhTds_R = sameBrhTds_R + lhpvBal.getLbTdsR();
									sameBrhMuns_R = sameBrhMuns_R + lhpvBal.getLbMunsR();
									claim = lhpvBal.getLbWtShrtgCR() + lhpvBal.getLbLateDelCR() + lhpvBal.getLbLateAckCR() + lhpvBal.getLbOthMiscP() + lhpvBal.getLbDrRcvrWtCR();
									sameBrhClm_R = sameBrhClm_R + lhpvBal.getLbWtShrtgCR() + lhpvBal.getLbLateDelCR() + lhpvBal.getLbLateAckCR() + lhpvBal.getLbOthMiscP() + lhpvBal.getLbDrRcvrWtCR();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser.getUserCode());
									// mainCashStmt.setCsDescription("LHPV BALANCE PAY BY "+lhpvBal.getLbBankCode()+" BANK");
									mainCashStmt.setCsDescription("as per lhpv petro");
									if(lhpvBal.getLbFinalTot()>0){
										mainCashStmt.setCsDrCr('C');
										mainCashStmt.setCsAmt(lhpvBal.getLbFinalTot());
									}else{
										mainCashStmt.setCsDrCr('D');
										mainCashStmt.setCsAmt((-1)*(lhpvBal.getLbFinalTot()));
									}
									mainCashStmt.setCsType("LHPV PAYMENT");
									/*if(lhpvBal.getLbBankName() != null)//for ho payments
										mainCashStmt.setCsVouchType("BANK");
									else*/
										mainCashStmt.setCsVouchType("PETRO");
									mainCashStmt.setCsChequeType(lhpvBal.getLbChqType());
									/*
									 * mainCashStmt.setCsFaCode(lhpvBal.
									 * getLbBankCode());
									 */
									//if (stfCode != null && stfCode != "") {
										mainCashStmt.setCsFaCode(stfCode);
										System.out.println("setCsFaCode="
												+ stfCode);
									/*}else if(lhpvBal.getLbBankName() != null){//by kamal for ho payments
										mainCashStmt.setCsFaCode(lhpvBal
												.getLbBankCode());
									}else {
										mainCashStmt.setCsFaCode(lhpvBal
												.getLbBrkOwn());
										System.out.println("setCsFaCode="
												+ lhpvBal
												.getLbBrkOwn());
									}*/
									//mainCashStmt.setCsFaCode(lhpvBal.getLbBrkOwn());
									mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setPayMode("P");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvBal.getLbTotPayAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbTotPayAmt() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*(lhpvBal.getLbTotPayAmt()));
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										
										cashStmt.setUserCode(currentUser.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,	cashStmtStatus.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbCashDiscR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbCashDiscR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbCashDiscR());
										// cashStmt.setCsDescription("LHPV Cash Discount By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(csDisFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV CASH DISCOUNT");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}


									if (lhpvBal.getLbTdsR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									} else if (lhpvBal.getLbTdsR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbTdsR());
										// cashStmt.setCsDescription("LHPV TDS By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(tdsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV TDS");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvBal.getLbMunsR() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvBal.getLbMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setPayMode("P");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (lhpvBal.getLbMunsR() < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*lhpvBal.getLbMunsR());
										// cashStmt.setCsDescription("LHPV Munsiana By Rtgs Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(munsFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setCsType("LHPV MUNSIANA");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setPayMode("P");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (claim > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(claim);
										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}else if (claim < 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt((-1)*claim);
										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvBal.getLbBankCode()+" BANK");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										/*if(lhpvBal.getLbBankName() != null)//for ho payments
											cashStmt.setCsVouchType("BANK");
										else*/
											cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;
								} else {
									System.out
											.println("invalid payment method for lhpv balance");
								}
								// }else{
								// Lhpv Adv for Challan of other branch

								// }
								lhpvBal.setLbIsClose(true);
								session.merge(lhpvBal);
							}
						}

						if (!lhpvSupList.isEmpty()) {
							for (int i = 0; i < lhpvSupList.size(); i++) {
								LhpvSup lhpvSup = lhpvSupList.get(i);
								// Challan challan = lhpvSup.getChallan();

								// if(challan.getBranchCode().equalsIgnoreCase(actLhpvStatus.getbCode())){
								// Lhpv Adv for Challan of same branch
								if (lhpvSup.getLspPayMethod() == 'C') {
									sameBrhLhPay_C = sameBrhLhPay_C
											+ lhpvSup.getLspTotPayAmt();
									sameBrhClm_C = sameBrhClm_C
											+ lhpvSup.getLspTotRcvrAmt();
								} else if (lhpvSup.getLspPayMethod() == 'Q') {
									sameBrhLhPay_Q = sameBrhLhPay_Q
											+ lhpvSup.getLspTotPayAmt();
									sameBrhClm_Q = sameBrhClm_Q
											+ lhpvSup.getLspTotRcvrAmt();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser
											.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser
											.getUserCode());
									// mainCashStmt.setCsDescription("LHPV SUPPLEMENTARY PAY BY "+lhpvSup.getLspBankCode()+" BANK");
									mainCashStmt
											.setCsDescription("as per lhpv");
									mainCashStmt.setCsDrCr('C');
									mainCashStmt.setCsAmt(lhpvSup
											.getLspFinalTot());
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("BANK");
									mainCashStmt.setCsChequeType(lhpvSup
											.getLspChqType());
									mainCashStmt.setCsFaCode(lhpvSup
											.getLspBankCode());
									mainCashStmt.setCsTvNo(lhpvSup
											.getLspChqNo());
									mainCashStmt.setPayMode("Q");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session
											.get(CashStmtStatus.class,
													cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvSup.getLspTotPayAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvSup
												.getLspTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By "+lhpvSup.getLspBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvSup
												.getLspChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvSup.getLspTotRcvrAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvSup
												.getLspTotRcvrAmt());
										// cashStmt.setCsDescription("LHPV Claim By "+lhpvSup.getLspBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(lhpvSup
												.getLspChqNo());
										cashStmt.setPayMode("Q");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("BANK");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;

								} else if (lhpvSup.getLspPayMethod() == 'R') {
									sameBrhLhPay_R = sameBrhLhPay_R
											+ lhpvSup.getLspTotPayAmt();
									sameBrhClm_R = sameBrhClm_R
											+ lhpvSup.getLspTotRcvrAmt();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser
											.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser
											.getUserCode());
									// mainCashStmt.setCsDescription("LHPV SUPPLEMENTARY PAY BY "+lhpvSup.getLspBankCode()+" BANK");
									mainCashStmt
											.setCsDescription("as per lhpv");
									mainCashStmt.setCsDrCr('C');
									mainCashStmt.setCsAmt(lhpvSup
											.getLspFinalTot());
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("RTGS");
									mainCashStmt.setCsChequeType(lhpvSup
											.getLspChqType());
									/*
									 * mainCashStmt.setCsFaCode(lhpvSup.
									 * getLspBankCode());
									 */
									mainCashStmt.setCsFaCode(lhpvSup
											.getLspBrkOwn());
									mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setPayMode("R");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session
											.get(CashStmtStatus.class,
													cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvSup.getLspTotPayAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvSup
												.getLspTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvSup.getLspBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvSup.getLspTotRcvrAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvSup
												.getLspTotRcvrAmt());
										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvSup.getLspBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("R");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("RTGS");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;
								} else if (lhpvSup.getLspPayMethod() == 'P') {
									sameBrhLhPay_R = sameBrhLhPay_R
											+ lhpvSup.getLspTotPayAmt();
									sameBrhClm_R = sameBrhClm_R
											+ lhpvSup.getLspTotRcvrAmt();

									CashStmt mainCashStmt = new CashStmt();
									mainCashStmt.setbCode(currentUser
											.getUserBranchCode());
									mainCashStmt.setUserCode(currentUser
											.getUserCode());
									// mainCashStmt.setCsDescription("LHPV SUPPLEMENTARY PAY BY "+lhpvSup.getLspBankCode()+" BANK");
									mainCashStmt
											.setCsDescription("as per lhpv petro");
									mainCashStmt.setCsDrCr('C');
									mainCashStmt.setCsAmt(lhpvSup
											.getLspFinalTot());
									mainCashStmt.setCsType("LHPV PAYMENT");
									mainCashStmt.setCsVouchType("PETRO");
									mainCashStmt.setCsChequeType(lhpvSup
											.getLspChqType());
									/*
									 * mainCashStmt.setCsFaCode(lhpvSup.
									 * getLspBankCode());
									 */
									mainCashStmt.setCsFaCode(lhpvSup
											.getLspBrkOwn());
									mainCashStmt.setCsTvNo(tvNo);
									mainCashStmt.setPayMode("P");
									mainCashStmt.setCsDt(sqlDate);
									mainCashStmt.setCsVouchNo(voucherNo);

									CashStmtStatus actCss = (CashStmtStatus) session
											.get(CashStmtStatus.class,
													cashStmtStatus.getCssId());
									mainCashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(mainCashStmt);
									session.merge(actCss);

									if (lhpvSup.getLspTotPayAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvSup
												.getLspTotPayAmt());
										// cashStmt.setCsDescription("LHPV Payment By RTGS Through "+lhpvSup.getLspBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('D');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(lryFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									if (lhpvSup.getLspTotRcvrAmt() > 0) {
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser
												.getUserBranchCode());
										cashStmt.setCsAmt(lhpvSup
												.getLspTotRcvrAmt());
										// cashStmt.setCsDescription("LHPV Claim By RTGS Through "+lhpvSup.getLspBankCode()+" Bank");
										cashStmt.setCsDescription("as per lhpv petro");
										cashStmt.setCsDrCr('C');
										cashStmt.setCsDt(sqlDate);
										cashStmt.setCsFaCode(clmFaCode);
										cashStmt.setCsIsClose(false);
										cashStmt.setCsTvNo(tvNo);
										cashStmt.setPayMode("P");
										cashStmt.setCsType("LHPV PAYMENT");
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsVouchType("PETRO");
										cashStmt.setUserCode(currentUser
												.getUserCode());

										CashStmtStatus actCss1 = (CashStmtStatus) session
												.get(CashStmtStatus.class,
														cashStmtStatus
																.getCssId());
										cashStmt.setCsNo(actCss1);
										actCss1.getCashStmtList().add(cashStmt);
										session.merge(actCss1);
									}

									voucherNo = voucherNo + 1;
								} else {
									System.out
											.println("invalid payment method for lhpv Supplementary");
								}
								// }else{
								// Lhpv Adv for Challan of other branch

								// }

								lhpvSup.setLspIsClose(true);
								session.merge(lhpvSup);
							}
						}

						System.out.println("Kamal21");
						// Same Branch entry in Cash Statement
						List<LhpvCashSmry> lcsList = new ArrayList<>();
						cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,
								actLhpvStatus.getLsDt()));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.B_CODE,
								actLhpvStatus.getbCode()));
						lcsList = cr.list();

						double todayPay = 0;
						double advLHPay = 0;
						if (!lcsList.isEmpty()) {
							System.out.println("Kamal22");
							for (int i = 0; i < lcsList.size(); i++) {
								if (lcsList.get(i).isLcsToday() == true
										&& lcsList.get(i).isLcsCsEntr() == false
										&& lcsList.get(i).isLcsAdvEntr() == false) {
									todayPay = todayPay
											+ lcsList.get(i).getLcsPayAmt();
									System.out.println("Kamal23");
								} else if (lcsList.get(i).isLcsToday() == false
										&& lcsList.get(i).isLcsCsEntr() == false
										&& lcsList.get(i).isLcsAdvEntr() == true) {
									advLHPay = advLHPay
											+ lcsList.get(i).getLcsPayAmt();
									System.out.println("Kamal24");
								} else {
									System.out.println("Kamal25");
								}

								LhpvCashSmry lhpvCashSmry = lcsList.get(i);
								lhpvCashSmry.setLcsCsEntr(true);
								session.merge(lhpvCashSmry);
							}
							System.out.println("Kamal26");
						}

						if (advLHPay > 0) {
							System.out.println("advLHPay>0=" + advLHPay);
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setCsAmt(advLHPay);
							// cashStmt.setCsDescription("LHPV Payment By Cash");
							cashStmt.setCsDescription("as per lhpv");
							cashStmt.setCsDrCr('D');
							cashStmt.setCsDt(sqlDate);
							cashStmt.setCsFaCode(lryFaCode);
							cashStmt.setCsIsClose(false);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setPayMode("C");
							cashStmt.setCsType("LHPV PAYMENT");
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsVouchType("CASH");
							cashStmt.setUserCode(currentUser.getUserCode());

							CashStmtStatus actCss = (CashStmtStatus) session
									.get(CashStmtStatus.class,
											cashStmtStatus.getCssId());
							cashStmt.setCsNo(actCss);
							actCss.getCashStmtList().add(cashStmt);
							session.merge(actCss);

							CashStmt cashStmt1 = new CashStmt();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setCsAmt(advLHPay);
							// cashStmt1.setCsDescription("LHPV Other Payment By Cash");
							cashStmt1.setCsDescription("as per lhpv");
							cashStmt1.setCsDrCr('C');
							cashStmt1.setCsDt(sqlDate);
							cashStmt1.setCsFaCode(lryAdvOth);
							cashStmt1.setCsIsClose(false);
							cashStmt1.setCsTvNo(tvNo);
							cashStmt1.setPayMode("C");
							cashStmt1.setCsType("LHPV PAYMENT");
							cashStmt1.setCsVouchNo(voucherNo);
							cashStmt1.setCsVouchType("CASH");
							cashStmt1.setUserCode(currentUser.getUserCode());

							CashStmtStatus actCss1 = (CashStmtStatus) session
									.get(CashStmtStatus.class,
											cashStmtStatus.getCssId());
							cashStmt1.setCsNo(actCss1);
							actCss1.getCashStmtList().add(cashStmt1);
							session.merge(actCss1);
						}
						System.out.println("Kamal26");
						if (sameBrhLhPay_C > 0) {
							System.out.println("Kamal27");
							double lhOthAmt = 0;
							lhOthAmt = sameBrhLhPay_C - todayPay;

							if (todayPay > 0) {
								System.out.println("todayPay>0=" + todayPay);
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser
										.getUserBranchCode());
								cashStmt.setCsAmt(todayPay);
								// cashStmt.setCsDescription("LHPV Payment By Cash");
								cashStmt.setCsDescription("as per lhpv");
								cashStmt.setCsDrCr('D');
								cashStmt.setCsDt(sqlDate);
								cashStmt.setCsFaCode(lryFaCode);
								cashStmt.setCsIsClose(false);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setPayMode("C");
								cashStmt.setCsType("LHPV PAYMENT");
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsVouchType("CASH");
								cashStmt.setUserCode(currentUser.getUserCode());

								CashStmtStatus actCss = (CashStmtStatus) session
										.get(CashStmtStatus.class,
												cashStmtStatus.getCssId());
								cashStmt.setCsNo(actCss);
								actCss.getCashStmtList().add(cashStmt);
								session.merge(actCss);
							}

							if (lhOthAmt > 0) {
								System.out.println("lhOthAmt>0=" + lhOthAmt);
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser
										.getUserBranchCode());
								cashStmt.setCsAmt(lhOthAmt);
								// cashStmt.setCsDescription("LHPV Other Payment By Cash");
								cashStmt.setCsDescription("as per lhpv");
								cashStmt.setCsDrCr('D');
								cashStmt.setCsDt(sqlDate);
								cashStmt.setCsFaCode(lryAdvOth);
								cashStmt.setCsIsClose(false);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setPayMode("C");
								cashStmt.setCsType("LHPV PAYMENT");
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsVouchType("CASH");
								cashStmt.setUserCode(currentUser.getUserCode());

								CashStmtStatus actCss = (CashStmtStatus) session
										.get(CashStmtStatus.class,
												cashStmtStatus.getCssId());
								cashStmt.setCsNo(actCss);
								actCss.getCashStmtList().add(cashStmt);
								session.merge(actCss);
							}

						}

						if (sameBrhCD_C > 0) {
							System.out.println("sameBrhCD_C>0=" + sameBrhCD_C);
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setCsAmt(sameBrhCD_C);
							// cashStmt.setCsDescription("LHPV Cash Discount By Cash");
							cashStmt.setCsDescription("as per lhpv");
							cashStmt.setCsDrCr('C');
							cashStmt.setCsDt(sqlDate);
							cashStmt.setCsFaCode(csDisFaCode);
							cashStmt.setCsIsClose(false);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setPayMode("C");
							cashStmt.setCsType("LHPV CASH DISCOUNT");
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsVouchType("CASH");
							cashStmt.setUserCode(currentUser.getUserCode());

							CashStmtStatus actCss = (CashStmtStatus) session
									.get(CashStmtStatus.class,
											cashStmtStatus.getCssId());
							cashStmt.setCsNo(actCss);
							actCss.getCashStmtList().add(cashStmt);
							session.merge(actCss);
						}

						if (sameBrhTds_C > 0) {
							System.out
									.println("sameBrhTds_C>0=" + sameBrhTds_C);
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setCsAmt(sameBrhTds_C);
							// cashStmt.setCsDescription("LHPV TDS By Cash");
							cashStmt.setCsDescription("as per lhpv");
							cashStmt.setCsDrCr('C');
							cashStmt.setCsDt(sqlDate);
							cashStmt.setCsFaCode(tdsFaCode);
							cashStmt.setCsIsClose(false);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setPayMode("C");
							cashStmt.setCsType("LHPV TDS");
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsVouchType("CASH");
							cashStmt.setUserCode(currentUser.getUserCode());

							CashStmtStatus actCss = (CashStmtStatus) session
									.get(CashStmtStatus.class,
											cashStmtStatus.getCssId());
							cashStmt.setCsNo(actCss);
							actCss.getCashStmtList().add(cashStmt);
							session.merge(actCss);
						}

						if (sameBrhMuns_C > 0) {
							System.out.println("sameBrhMuns_C>0="
									+ sameBrhMuns_C);
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setCsAmt(sameBrhMuns_C);
							// cashStmt.setCsDescription("LHPV Munsiana By Cash");
							cashStmt.setCsDescription("as per lhpv");
							cashStmt.setCsDrCr('C');
							cashStmt.setCsDt(sqlDate);
							cashStmt.setCsFaCode(munsFaCode);
							cashStmt.setCsIsClose(false);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setPayMode("C");
							cashStmt.setCsType("LHPV MUNSIANA");
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsVouchType("CASH");
							cashStmt.setUserCode(currentUser.getUserCode());

							CashStmtStatus actCss = (CashStmtStatus) session
									.get(CashStmtStatus.class,
											cashStmtStatus.getCssId());
							cashStmt.setCsNo(actCss);
							actCss.getCashStmtList().add(cashStmt);
							session.merge(actCss);
						}

						if (sameBrhClm_C > 0) {
							System.out.println("sameBrhClm_C>0" + sameBrhClm_C);
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setCsAmt(sameBrhClm_C);
							// cashStmt.setCsDescription("LHPV Claim By Cash");
							cashStmt.setCsDescription("as per lhpv");
							cashStmt.setCsDrCr('C');
							cashStmt.setCsDt(sqlDate);
							cashStmt.setCsFaCode(clmFaCode);
							cashStmt.setCsIsClose(false);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setPayMode("C");
							cashStmt.setCsType("LHPV PAYMENT");
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsVouchType("CASH");
							cashStmt.setUserCode(currentUser.getUserCode());

							CashStmtStatus actCss = (CashStmtStatus) session
									.get(CashStmtStatus.class,
											cashStmtStatus.getCssId());
							cashStmt.setCsNo(actCss);
							actCss.getCashStmtList().add(cashStmt);
							session.merge(actCss);
						}

						actLhpvStatus.setLsClose(true);
						session.merge(actLhpvStatus);
						res = voucherNo;
					}

				}
			
		} else {
			res = 0;
		}
		return res;
	}
//TODO End into cs
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public int closeLhpvSmry(LhpvStatus lhpvStatus) {
		System.out.println("enter into closeLhpvSmry function");
		int res = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int lsId = lhpvStatus.getLsId();
		Session session=null;
		Transaction transaction=null;
		if (lsId > 0) {
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt().getTime());

				LhpvStatus actLhpvStatus = (LhpvStatus) session.get(LhpvStatus.class, lsId);
				Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(lhpvStatus.getbCode()));
				/*
				 * List<LhpvCashSmry> lcsList = new ArrayList<>(); Criteria cr =
				 * session.createCriteria(LhpvCashSmry.class);
				 * cr.add(Restrictions
				 * .eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate)); lcsList =
				 * cr.list();
				 */

				List<LhpvAdv> lhpvAdvList = new ArrayList<>();
				List<LhpvAdv> actLAList = new ArrayList<>();
				lhpvAdvList = actLhpvStatus.getLaList();
				for (int i = 0; i < lhpvAdvList.size(); i++) {
					if (lhpvAdvList.get(i).getLaPayMethod() == 'C') {
						actLAList.add(lhpvAdvList.get(i));
					}
				}

				System.out.println("size of actLAList = " + actLAList.size());

				List<LhpvBal> lhpvBalList = new ArrayList<>();
				List<LhpvBal> actLBList = new ArrayList<>();
				lhpvBalList = actLhpvStatus.getLbList();
				for (int i = 0; i < lhpvBalList.size(); i++) {
					if (lhpvBalList.get(i).getLbPayMethod() == 'C') {
						actLBList.add(lhpvBalList.get(i));
					}
				}

				System.out.println("size of actLBList = " + actLBList.size());

				List<LhpvSup> lhpvSupList = new ArrayList<>();
				List<LhpvSup> actLSList = new ArrayList<>();
				lhpvSupList = actLhpvStatus.getLspList();
				for (int i = 0; i < lhpvSupList.size(); i++) {
					if (lhpvSupList.get(i).getLspPayMethod() == 'C') {
						actLSList.add(lhpvSupList.get(i));
					}
				}

				if (!actLAList.isEmpty()) {
					for (int i = 0; i < actLAList.size(); i++) {
						LhpvAdv lhpvAdv = actLAList.get(i);
						String chlnCode = lhpvAdv.getChallan().getChlnCode();
						double todayTotPay = lhpvAdv.getLaTotPayAmt();
						double alrdyPayAmt = 0;

						List<LhpvCashSmry> lcsList = new ArrayList<>();
						Criteria cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_BRH_CODE,branch.getBranchFaCode()));
						lcsList = cr.list();

						if (!lcsList.isEmpty()) {
							// Check the already paid amt for the same
							// broker/owner
							for (int j = 0; j < lcsList.size(); j++) {
								if (lhpvAdv.getLaBrkOwn().equalsIgnoreCase(lcsList.get(j).getLcsBrkOwn())) {
									alrdyPayAmt = alrdyPayAmt + lcsList.get(j).getLcsPayAmt();
								}
							}
							System.out.println("Already Pay Amount="+ alrdyPayAmt);
							System.out.println("Today Pay Amount="+ todayTotPay);
							if (alrdyPayAmt < 20000) {
								// enter into today and rest of the amt into
								// next date

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
								lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
								if (todayTotPay <= (20000 - alrdyPayAmt)) {
									lhpvCashSmry.setLcsPayAmt(todayTotPay);
								} else {
									lhpvCashSmry.setLcsPayAmt(20000 - alrdyPayAmt);
								}
								lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
								lhpvCashSmry.setLaId(lhpvAdv.getLaId());
								System.out.println("1 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - (20000 - alrdyPayAmt);
								System.out.println("remaining Total Pay" + remTotPay);
								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
									lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
									System.out.println("2 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
										lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
										lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										System.out.println("3 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}

								}

							} else {
								// advance entry for this broker/owner
								// get the last entry of this broker/owner
								double lastPayAmt = 0;
								List<LhpvCashSmry> lastLCSList = new ArrayList<>();
								Query query = session.createQuery("from LhpvCashSmry where lcsBrkOwn= :type order by lcsId DESC");
								query.setParameter("type",lhpvAdv.getLaBrkOwn());
								lastLCSList = query.setMaxResults(1).list();

								if (!lastLCSList.isEmpty()) {
									LhpvCashSmry lastLCS = lastLCSList.get(0);
									Date lastDt = lastLCS.getLcsLhpvDt();
									// check last time amt paid to this
									// broker/owner
									if (lastLCS.getLcsPayAmt() < 20000) {
										// enter into last date and rest of the
										// amt into next date

										LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

										lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry.setUserCode(currentUser.getUserCode());
										lhpvCashSmry.setLcsToday(false);
										lhpvCashSmry.setLcsAdvEntr(true);
										lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
										lhpvCashSmry.setLcsChlnCode(chlnCode);
										lhpvCashSmry.setLcsCsEntr(false);
										lhpvCashSmry.setLcsLhpvDt(lastDt);
										lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
										lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
										if (todayTotPay <= (20000 - lastLCS.getLcsPayAmt())) {
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
										} else {
											lhpvCashSmry.setLcsPayAmt(20000 - lastLCS.getLcsPayAmt());
										}
										lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
										lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
										lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
										lhpvCashSmry.setLaId(lhpvAdv.getLaId());
										System.out.println("4 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

										session.save(lhpvCashSmry);

										double remTotPay = todayTotPay - (20000 - lastLCS.getLcsPayAmt());

										if (remTotPay <= 20000 && remTotPay > 0) {
											Date nextDt = CodePatternService.getNextDt(lastDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
											lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
											lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
											System.out.println("5 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

											session.save(lhpvCashSmry1);
										} else {
											Date nextAdvDt = lastDt;
											while (remTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
												lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
												lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
												if (remTotPay >= 20000) {
													lhpvCashSmry1.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry1.setLcsPayAmt(remTotPay);
												}
												System.out.println("6 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

												session.save(lhpvCashSmry1);
												remTotPay = remTotPay - 20000;
											}

										}

									} else {
										// entery start from next day
										Date nextDt = CodePatternService.getNextDt(lastDt);
										if (todayTotPay <= 20000 && todayTotPay > 0) {
											// Simple one entry into next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(nextDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
											lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
											lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
											lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
											lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
											lhpvCashSmry.setLaId(lhpvAdv.getLaId());
											System.out.println("7 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

											session.save(lhpvCashSmry);
										} else {
											// multiple advance entries for this
											// broker/owner
											Date nextAdvDt = lastDt;
											boolean flag = true;
											while (todayTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

												lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry.setUserCode(currentUser.getUserCode());
												lhpvCashSmry.setLcsToday(false);
												lhpvCashSmry.setLcsAdvEntr(true);
												lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
												lhpvCashSmry.setLcsChlnCode(chlnCode);
												lhpvCashSmry.setLcsCsEntr(false);
												lhpvCashSmry.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
												lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
												lhpvCashSmry.setLaId(lhpvAdv.getLaId());
												if (todayTotPay >= 20000) {
													lhpvCashSmry.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry.setLcsPayAmt(todayTotPay);
												}

												if (flag) {
													lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
													lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
													lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());

													flag = false;
												}
												System.out.println("8 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

												session.save(lhpvCashSmry);
												todayTotPay = todayTotPay - 20000;
											}
										}
									}
								}
							}

						} else {
							// Simple enter LhpvADV to LhpvCashSmry in sqlDate

							if (todayTotPay <= 20000 && todayTotPay > 0) {

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
								lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
								lhpvCashSmry.setLcsPayAmt(todayTotPay);
								lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
								lhpvCashSmry.setLaId(lhpvAdv.getLaId());
								System.out.println("9 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

								session.save(lhpvCashSmry);

							} else {

								// multiple advance entries for this
								// broker/owner and date start from sqlDate

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
								lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
								if (todayTotPay < 0) {// This code is for lhpv
														// reversal
									lhpvCashSmry.setLcsPayAmt(todayTotPay);
								} else {
									lhpvCashSmry.setLcsPayAmt(20000);
								}
								// lhpvCashSmry.setLcsPayAmt(20000);
								lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
								lhpvCashSmry.setLaId(lhpvAdv.getLaId());
								System.out.println("10 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - 20000;

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextAdvDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
									lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
									System.out.println("11 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
										lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
										lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										System.out.println("12 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}
								}

							}
						}
					}
				}

				if (!actLBList.isEmpty()) {
					for (int i = 0; i < actLBList.size(); i++) {
						LhpvBal lhpvBal = actLBList.get(i);
						String chlnCode = lhpvBal.getChallan().getChlnCode();
						double todayTotPay = lhpvBal.getLbTotPayAmt();
						double othTotPay = todayTotPay - lhpvBal.getLbLryBalP();
						double alrdyPayAmt = 0;

						List<LhpvCashSmry> lcsList = new ArrayList<>();
						Criteria cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_BRH_CODE,branch.getBranchFaCode()));
						lcsList = cr.list();

						if (!lcsList.isEmpty()) {
							// Check the already paid amt for the same
							// broker/owner
							for (int j = 0; j < lcsList.size(); j++) {
								if (lhpvBal.getLbBrkOwn().equalsIgnoreCase(lcsList.get(j).getLcsBrkOwn())) {
									alrdyPayAmt = alrdyPayAmt + lcsList.get(j).getLcsPayAmt();
								}
							}

							if (alrdyPayAmt < 20000) {
								// enter into today and rest of the amt into
								// next date

								if ((20000 - alrdyPayAmt) >= othTotPay) {
									// other payment condition true--->same date
									// + next date
									LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

									lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry.setUserCode(currentUser.getUserCode());
									lhpvCashSmry.setLcsToday(true);
									lhpvCashSmry.setLcsAdvEntr(false);
									lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
									lhpvCashSmry.setLcsChlnCode(chlnCode);
									lhpvCashSmry.setLcsCsEntr(false);
									lhpvCashSmry.setLcsLhpvDt(sqlDate);
									lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
									lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
									lhpvCashSmry.setLbId(lhpvBal.getLbId());
									if (todayTotPay <= (20000 - alrdyPayAmt)) {
										lhpvCashSmry.setLcsPayAmt(todayTotPay);
									} else {
										lhpvCashSmry.setLcsPayAmt(20000 - alrdyPayAmt);
									}

									lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
									lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
									lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
									lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
									lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
									lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
									lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
									lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
									lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
									lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
									lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
									lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
									lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());
									lhpvCashSmry.setLbId(lhpvBal.getLbId());

									session.save(lhpvCashSmry);

									double remTotPay = todayTotPay	- (20000 - alrdyPayAmt);

									if (remTotPay <= 20000 && remTotPay > 0) {
										Date nextDt = CodePatternService.getNextDt(sqlDate);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
										lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
										lhpvCashSmry1.setLcsPayAmt(remTotPay);
										lhpvCashSmry1.setLbId(lhpvBal.getLbId());

										session.save(lhpvCashSmry1);
									} else {
										Date nextAdvDt = sqlDate;
										while (remTotPay > 0) {
											nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry1.setLbId(lhpvBal.getLbId());
											if (remTotPay >= 20000) {
												lhpvCashSmry1.setLcsPayAmt(20000);
											} else {
												lhpvCashSmry1.setLcsPayAmt(remTotPay);
											}

											session.save(lhpvCashSmry1);
											remTotPay = remTotPay - 20000;
										}

									}

								} else {
									// other payment condition false--->next
									// date

									if (todayTotPay <= 20000 && todayTotPay > 0) {
										Date nextDt = CodePatternService.getNextDt(sqlDate);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
										lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
										lhpvCashSmry1.setLcsPayAmt(todayTotPay);
										lhpvCashSmry1.setLbId(lhpvBal.getLbId());

										lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
										lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
										lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
										lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
										lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
										lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
										lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
										lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
										lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
										lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
										lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
										lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
										lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

										session.save(lhpvCashSmry1);
									} else {
										Date nextAdvDt = sqlDate;
										boolean flag = true;

										while (todayTotPay > 0) {
											nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry1.setLbId(lhpvBal.getLbId());
											if (todayTotPay >= 20000) {
												lhpvCashSmry1.setLcsPayAmt(20000);
											} else {
												lhpvCashSmry1.setLcsPayAmt(todayTotPay);
											}

											if (flag) {
												lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
												lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
												lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
												lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
												lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
												lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
												lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
												lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
												lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
												lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
												lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
												lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
												lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

												flag = false;
											}

											session.save(lhpvCashSmry1);
											todayTotPay = todayTotPay - 20000;
										}
									}
								}

							} else {
								// advance entry for this broker/owner
								// get the last entry of this broker/owner
								double lastPayAmt = 0;
								List<LhpvCashSmry> lastLCSList = new ArrayList<>();
								Query query = session.createQuery("from LhpvCashSmry where lcsBrkOwn= :type order by lcsId DESC");
								query.setParameter("type",lhpvBal.getLbBrkOwn());
								lastLCSList = query.setMaxResults(1).list();

								if (!lastLCSList.isEmpty()) {
									LhpvCashSmry lastLCS = lastLCSList.get(0);
									Date lastDt = lastLCS.getLcsLhpvDt();
									// check last time amt paid to this
									// broker/owner
									if (lastLCS.getLcsPayAmt() < 20000) {
										// enter into last date and rest of the
										// amt into next date

										if ((20000 - lastLCS.getLcsPayAmt()) >= othTotPay) {
											// other payment condition
											// true--->same date + next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(lastDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry.setLbId(lhpvBal.getLbId());
											if (todayTotPay <= (20000 - lastLCS.getLcsPayAmt())) {
												lhpvCashSmry.setLcsPayAmt(todayTotPay);
											} else {
												lhpvCashSmry.setLcsPayAmt(20000 - lastLCS.getLcsPayAmt());
											}

											lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
											lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
											lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
											lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
											lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
											lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
											lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
											lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
											lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
											lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
											lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
											lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
											lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

											session.save(lhpvCashSmry);

											double remTotPay = todayTotPay	- (20000 - lastLCS.getLcsPayAmt());

											if (remTotPay <= 20000	&& remTotPay > 0) {
												Date nextDt = CodePatternService.getNextDt(lastDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
												lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
												lhpvCashSmry1.setLcsPayAmt(remTotPay);
												lhpvCashSmry1.setLbId(lhpvBal.getLbId());

												session.save(lhpvCashSmry1);
											} else {
												Date nextAdvDt = lastDt;
												while (remTotPay > 0) {
													nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
													LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

													lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
													lhpvCashSmry1.setUserCode(currentUser.getUserCode());
													lhpvCashSmry1.setLcsToday(false);
													lhpvCashSmry1.setLcsAdvEntr(true);
													lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
													lhpvCashSmry1.setLcsChlnCode(chlnCode);
													lhpvCashSmry1.setLcsCsEntr(false);
													lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
													lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
													lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
													lhpvCashSmry1.setLbId(lhpvBal.getLbId());
													if (remTotPay >= 20000) {
														lhpvCashSmry1.setLcsPayAmt(20000);
													} else {
														lhpvCashSmry1.setLcsPayAmt(remTotPay);
													}

													session.save(lhpvCashSmry1);
													remTotPay = remTotPay - 20000;
												}

											}
										} else {
											// other payment condition
											// false--->next date

											if (todayTotPay <= 20000 && todayTotPay > 0) {
												Date nextDt = CodePatternService.getNextDt(lastDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
												lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
												lhpvCashSmry1.setLcsPayAmt(todayTotPay);
												lhpvCashSmry1.setLbId(lhpvBal.getLbId());

												lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
												lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
												lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
												lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
												lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
												lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
												lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
												lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
												lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
												lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
												lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
												lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
												lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

												session.save(lhpvCashSmry1);
											} else {
												Date nextAdvDt = lastDt;
												boolean flag = true;

												while (todayTotPay > 0) {
													nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
													LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

													lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
													lhpvCashSmry1.setUserCode(currentUser.getUserCode());
													lhpvCashSmry1.setLcsToday(false);
													lhpvCashSmry1.setLcsAdvEntr(true);
													lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
													lhpvCashSmry1.setLcsChlnCode(chlnCode);
													lhpvCashSmry1.setLcsCsEntr(false);
													lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
													lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
													lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
													lhpvCashSmry1.setLbId(lhpvBal.getLbId());
													if (todayTotPay >= 20000) {
														lhpvCashSmry1.setLcsPayAmt(20000);
													} else {
														lhpvCashSmry1.setLcsPayAmt(todayTotPay);
													}

													if (flag) {
														lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
														lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
														lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
														lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
														lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
														lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
														lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
														lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
														lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
														lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
														lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
														lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
														lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

														flag = false;
													}

													session.save(lhpvCashSmry1);
													todayTotPay = todayTotPay - 20000;
												}
											}
										}

									} else {
										// entery start from next day
										Date nextDt = CodePatternService
												.getNextDt(lastDt);
										if (todayTotPay <= 20000&& todayTotPay > 0) {
											// Simple one entry into next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(nextDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
											lhpvCashSmry.setLbId(lhpvBal.getLbId());

											lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
											lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
											lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
											lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
											lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
											lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
											lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
											lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
											lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
											lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
											lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
											lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
											lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

											session.save(lhpvCashSmry);
										} else {
											// multiple advance entries for this
											// broker/owner
											Date nextAdvDt = lastDt;
											boolean flag = true;

											while (todayTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

												lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry.setUserCode(currentUser.getUserCode());
												lhpvCashSmry.setLcsToday(false);
												lhpvCashSmry.setLcsAdvEntr(true);
												lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
												lhpvCashSmry.setLcsChlnCode(chlnCode);
												lhpvCashSmry.setLcsCsEntr(false);
												lhpvCashSmry.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
												lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
												lhpvCashSmry.setLbId(lhpvBal.getLbId());
												if (todayTotPay >= 20000) {
													lhpvCashSmry.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry.setLcsPayAmt(todayTotPay);
												}

												if (flag) {
													lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
													lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
													lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
													lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
													lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
													lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
													lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
													lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
													lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
													lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
													lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
													lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
													lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

													flag = false;
												}

												session.save(lhpvCashSmry);
												todayTotPay = todayTotPay - 20000;
											}
										}
									}
								}
							}

						} else {
							// Simple enter LhpvBAL to LhpvCashSmry in sqlDate

							if (todayTotPay <= 20000) {

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
								lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
								lhpvCashSmry.setLcsPayAmt(todayTotPay);
								lhpvCashSmry.setLbId(lhpvBal.getLbId());

								lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
								lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
								lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
								lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

								session.save(lhpvCashSmry);

							} else {

								// multiple advance entries for this
								// broker/owner and date start from sqlDate

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
								lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
								lhpvCashSmry.setLcsPayAmt(20000);
								lhpvCashSmry.setLbId(lhpvBal.getLbId());

								lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
								lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
								lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
								lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - 20000;

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextAdvDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
									lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLbId(lhpvBal.getLbId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
										lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
										lhpvCashSmry1.setLbId(lhpvBal.getLbId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}
								}

							}
						}
					}
				}

				if (!actLSList.isEmpty()) {
					for (int i = 0; i < actLSList.size(); i++) {
						LhpvSup lhpvSup = actLSList.get(i);
						String chlnCode = lhpvSup.getChallan().getChlnCode();
						double todayTotPay = lhpvSup.getLspTotPayAmt();
						double alrdyPayAmt = 0;

						List<LhpvCashSmry> lcsList = new ArrayList<>();
						Criteria cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_BRH_CODE,branch.getBranchFaCode()));
						lcsList = cr.list();

						if (!lcsList.isEmpty()) {
							// Check the already paid amt for the same
							// broker/owner
							for (int j = 0; j < lcsList.size(); j++) {
								if (lhpvSup.getLspBrkOwn().equalsIgnoreCase(lcsList.get(j).getLcsBrkOwn())) {
									alrdyPayAmt = alrdyPayAmt + lcsList.get(j).getLcsPayAmt();
								}
							}

							if (alrdyPayAmt < 20000) {
								// enter into today and rest of the amt into
								// next date

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
								lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());

								if (todayTotPay <= (20000 - alrdyPayAmt)) {
									lhpvCashSmry.setLcsPayAmt(todayTotPay);
								} else {
									lhpvCashSmry.setLcsPayAmt(20000 - alrdyPayAmt);
								}

								lhpvCashSmry.setLspId(lhpvSup.getLspId());

								lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
								lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
								lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - (20000 - alrdyPayAmt);

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
									lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());

									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLspId(lhpvSup.getLspId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
										lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
										lhpvCashSmry1.setLspId(lhpvSup.getLspId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}

										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}

								}

							} else {
								// advance entry for this broker/owner
								// get the last entry of this broker/owner
								double lastPayAmt = 0;
								List<LhpvCashSmry> lastLCSList = new ArrayList<>();
								Query query = session.createQuery("from LhpvCashSmry where lcsBrkOwn= :type order by lcsId DESC");
								query.setParameter("type",lhpvSup.getLspBrkOwn());
								lastLCSList = query.setMaxResults(1).list();

								if (!lastLCSList.isEmpty()) {
									LhpvCashSmry lastLCS = lastLCSList.get(0);
									Date lastDt = lastLCS.getLcsLhpvDt();
									// check last time amt paid to this
									// broker/owner
									if (lastLCS.getLcsPayAmt() < 20000) {
										// enter into last date and rest of the
										// amt into next date

										LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

										lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry.setUserCode(currentUser.getUserCode());
										lhpvCashSmry.setLcsToday(false);
										lhpvCashSmry.setLcsAdvEntr(true);
										lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
										lhpvCashSmry.setLcsChlnCode(chlnCode);
										lhpvCashSmry.setLcsCsEntr(false);
										lhpvCashSmry.setLcsLhpvDt(lastDt);
										lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
										lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());

										if (todayTotPay <= (20000 - lastLCS.getLcsPayAmt())) {
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
										} else {
											lhpvCashSmry.setLcsPayAmt(20000 - lastLCS.getLcsPayAmt());
										}

										lhpvCashSmry.setLspId(lhpvSup.getLspId());

										lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
										lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
										lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
										lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
										lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
										lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
										lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
										lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
										lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
										lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

										session.save(lhpvCashSmry);

										double remTotPay = todayTotPay- (20000 - lastLCS.getLcsPayAmt());

										if (remTotPay <= 20000 && remTotPay > 0) {
											Date nextDt = CodePatternService.getNextDt(lastDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
											lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
											lhpvCashSmry1.setLspId(lhpvSup.getLspId());

											session.save(lhpvCashSmry1);
										} else {
											Date nextAdvDt = lastDt;
											while (remTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
												lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
												lhpvCashSmry1.setLspId(lhpvSup.getLspId());
												if (remTotPay >= 20000) {
													lhpvCashSmry1.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry1.setLcsPayAmt(remTotPay);
												}

												session.save(lhpvCashSmry1);
												remTotPay = remTotPay - 20000;
											}

										}

									} else {
										// entery start from next day
										Date nextDt = CodePatternService.getNextDt(lastDt);
										if (todayTotPay <= 20000 && todayTotPay > 0) {
											// Simple one entry into next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(nextDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
											lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
											lhpvCashSmry.setLspId(lhpvSup.getLspId());

											lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
											lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
											lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
											lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
											lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
											lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
											lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
											lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
											lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
											lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

											session.save(lhpvCashSmry);
										} else {
											// multiple advance entries for this
											// broker/owner
											Date nextAdvDt = lastDt;
											boolean flag = true;

											while (todayTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

												lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry.setUserCode(currentUser.getUserCode());
												lhpvCashSmry.setLcsToday(false);
												lhpvCashSmry.setLcsAdvEntr(true);
												lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
												lhpvCashSmry.setLcsChlnCode(chlnCode);
												lhpvCashSmry.setLcsCsEntr(false);
												lhpvCashSmry.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
												lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());
												lhpvCashSmry.setLspId(lhpvSup.getLspId());
												if (todayTotPay >= 20000) {
													lhpvCashSmry.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry.setLcsPayAmt(todayTotPay);
												}

												if (flag) {
													lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
													lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
													lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
													lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
													lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
													lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
													lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
													lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
													lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
													lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());
												}

												session.save(lhpvCashSmry);
												todayTotPay = todayTotPay - 20000;
											}
										}
									}
								}
							}

						} else {
							// Simple enter LhpSUP to LhpvCashSmry in sqlDate

							//if (todayTotPay <= 20000 && todayTotPay > 0) {
							if (todayTotPay <= 20000) {//this code is replaced by kamal (from commented condition to this condition)

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
								lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());
								lhpvCashSmry.setLcsPayAmt(todayTotPay);
								lhpvCashSmry.setLspId(lhpvSup.getLspId());

								lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
								lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
								lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

								session.save(lhpvCashSmry);

							} else {

								// multiple advance entries for this
								// broker/owner and date start from sqlDate

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
								lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());

								lhpvCashSmry.setLcsPayAmt(20000);
								lhpvCashSmry.setLspId(lhpvSup.getLspId());

								lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
								lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
								lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - 20000;

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextAdvDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
									lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLspId(lhpvSup.getLspId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
										lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
										lhpvCashSmry1.setLspId(lhpvSup.getLspId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}
								}

							}
						}
					}
				}

				res = 1;
		//		session.flush();
		//		session.clear();
				transaction.commit();

			} catch (Exception e) {
				transaction.rollback();
				e.printStackTrace();
				return 0;
			}finally{
				session.close();
			}

		} else {
			res = 0;
		}
		return res;
	}

	
	
	
	
	
	
	
	
	//TODO Start
	@Override
	public int closeLhpvSmry(LhpvStatus lhpvStatus,Session session) {
		System.out.println("enter into closeLhpvSmry function");
		int res = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int lsId = lhpvStatus.getLsId();
		if (lsId > 0) {

				java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt().getTime());

				LhpvStatus actLhpvStatus = (LhpvStatus) session.get(LhpvStatus.class, lsId);
				Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(lhpvStatus.getbCode()));
				/*
				 * List<LhpvCashSmry> lcsList = new ArrayList<>(); Criteria cr =
				 * session.createCriteria(LhpvCashSmry.class);
				 * cr.add(Restrictions
				 * .eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate)); lcsList =
				 * cr.list();
				 */

				List<LhpvAdv> lhpvAdvList = new ArrayList<>();
				List<LhpvAdv> actLAList = new ArrayList<>();
				lhpvAdvList = actLhpvStatus.getLaList();
				for (int i = 0; i < lhpvAdvList.size(); i++) {
					if (lhpvAdvList.get(i).getLaPayMethod() == 'C') {
						actLAList.add(lhpvAdvList.get(i));
					}
				}

				System.out.println("size of actLAList = " + actLAList.size());

				List<LhpvBal> lhpvBalList = new ArrayList<>();
				List<LhpvBal> actLBList = new ArrayList<>();
				lhpvBalList = actLhpvStatus.getLbList();
				for (int i = 0; i < lhpvBalList.size(); i++) {
					if (lhpvBalList.get(i).getLbPayMethod() == 'C') {
						actLBList.add(lhpvBalList.get(i));
					}
				}

				System.out.println("size of actLBList = " + actLBList.size());

				List<LhpvSup> lhpvSupList = new ArrayList<>();
				List<LhpvSup> actLSList = new ArrayList<>();
				lhpvSupList = actLhpvStatus.getLspList();
				for (int i = 0; i < lhpvSupList.size(); i++) {
					if (lhpvSupList.get(i).getLspPayMethod() == 'C') {
						actLSList.add(lhpvSupList.get(i));
					}
				}

				if (!actLAList.isEmpty()) {
					for (int i = 0; i < actLAList.size(); i++) {
						LhpvAdv lhpvAdv = actLAList.get(i);
						String chlnCode = lhpvAdv.getChallan().getChlnCode();
						double todayTotPay = lhpvAdv.getLaTotPayAmt();
						double alrdyPayAmt = 0;

						List<LhpvCashSmry> lcsList = new ArrayList<>();
						Criteria cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_BRH_CODE,branch.getBranchFaCode()));
						lcsList = cr.list();

						if (!lcsList.isEmpty()) {
							// Check the already paid amt for the same
							// broker/owner
							for (int j = 0; j < lcsList.size(); j++) {
								if (lhpvAdv.getLaBrkOwn().equalsIgnoreCase(lcsList.get(j).getLcsBrkOwn())) {
									alrdyPayAmt = alrdyPayAmt + lcsList.get(j).getLcsPayAmt();
								}
							}
							System.out.println("Already Pay Amount="+ alrdyPayAmt);
							System.out.println("Today Pay Amount="+ todayTotPay);
							if (alrdyPayAmt < 20000) {
								// enter into today and rest of the amt into
								// next date

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
								lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
								if (todayTotPay <= (20000 - alrdyPayAmt)) {
									lhpvCashSmry.setLcsPayAmt(todayTotPay);
								} else {
									lhpvCashSmry.setLcsPayAmt(20000 - alrdyPayAmt);
								}
								lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
								lhpvCashSmry.setLaId(lhpvAdv.getLaId());
								System.out.println("1 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - (20000 - alrdyPayAmt);
								System.out.println("remaining Total Pay" + remTotPay);
								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
									lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
									System.out.println("2 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
										lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
										lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										System.out.println("3 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}

								}

							} else {
								// advance entry for this broker/owner
								// get the last entry of this broker/owner
								double lastPayAmt = 0;
								List<LhpvCashSmry> lastLCSList = new ArrayList<>();
								Query query = session.createQuery("from LhpvCashSmry where lcsBrkOwn= :type order by lcsId DESC");
								query.setParameter("type",lhpvAdv.getLaBrkOwn());
								lastLCSList = query.setMaxResults(1).list();

								if (!lastLCSList.isEmpty()) {
									LhpvCashSmry lastLCS = lastLCSList.get(0);
									Date lastDt = lastLCS.getLcsLhpvDt();
									// check last time amt paid to this
									// broker/owner
									if (lastLCS.getLcsPayAmt() < 20000) {
										// enter into last date and rest of the
										// amt into next date

										LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

										lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry.setUserCode(currentUser.getUserCode());
										lhpvCashSmry.setLcsToday(false);
										lhpvCashSmry.setLcsAdvEntr(true);
										lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
										lhpvCashSmry.setLcsChlnCode(chlnCode);
										lhpvCashSmry.setLcsCsEntr(false);
										lhpvCashSmry.setLcsLhpvDt(lastDt);
										lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
										lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
										if (todayTotPay <= (20000 - lastLCS.getLcsPayAmt())) {
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
										} else {
											lhpvCashSmry.setLcsPayAmt(20000 - lastLCS.getLcsPayAmt());
										}
										lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
										lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
										lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
										lhpvCashSmry.setLaId(lhpvAdv.getLaId());
										System.out.println("4 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

										session.save(lhpvCashSmry);

										double remTotPay = todayTotPay - (20000 - lastLCS.getLcsPayAmt());

										if (remTotPay <= 20000 && remTotPay > 0) {
											Date nextDt = CodePatternService.getNextDt(lastDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
											lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
											lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
											System.out.println("5 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

											session.save(lhpvCashSmry1);
										} else {
											Date nextAdvDt = lastDt;
											while (remTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
												lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
												lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
												if (remTotPay >= 20000) {
													lhpvCashSmry1.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry1.setLcsPayAmt(remTotPay);
												}
												System.out.println("6 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

												session.save(lhpvCashSmry1);
												remTotPay = remTotPay - 20000;
											}

										}

									} else {
										// entery start from next day
										Date nextDt = CodePatternService.getNextDt(lastDt);
										if (todayTotPay <= 20000 && todayTotPay > 0) {
											// Simple one entry into next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(nextDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
											lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
											lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
											lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
											lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
											lhpvCashSmry.setLaId(lhpvAdv.getLaId());
											System.out.println("7 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

											session.save(lhpvCashSmry);
										} else {
											// multiple advance entries for this
											// broker/owner
											Date nextAdvDt = lastDt;
											boolean flag = true;
											while (todayTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

												lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry.setUserCode(currentUser.getUserCode());
												lhpvCashSmry.setLcsToday(false);
												lhpvCashSmry.setLcsAdvEntr(true);
												lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
												lhpvCashSmry.setLcsChlnCode(chlnCode);
												lhpvCashSmry.setLcsCsEntr(false);
												lhpvCashSmry.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
												lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
												lhpvCashSmry.setLaId(lhpvAdv.getLaId());
												if (todayTotPay >= 20000) {
													lhpvCashSmry.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry.setLcsPayAmt(todayTotPay);
												}

												if (flag) {
													lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
													lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
													lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());

													flag = false;
												}
												System.out.println("8 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

												session.save(lhpvCashSmry);
												todayTotPay = todayTotPay - 20000;
											}
										}
									}
								}
							}

						} else {
							// Simple enter LhpvADV to LhpvCashSmry in sqlDate

							if (todayTotPay <= 20000 && todayTotPay > 0) {

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
								lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
								lhpvCashSmry.setLcsPayAmt(todayTotPay);
								lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
								lhpvCashSmry.setLaId(lhpvAdv.getLaId());
								System.out.println("9 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

								session.save(lhpvCashSmry);

							} else {

								// multiple advance entries for this
								// broker/owner and date start from sqlDate

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_ADV);
								lhpvCashSmry.setLcsBrhCode(lhpvAdv.getLaBrhCode());
								if (todayTotPay < 0) {// This code is for lhpv
														// reversal
									lhpvCashSmry.setLcsPayAmt(todayTotPay);
								} else {
									lhpvCashSmry.setLcsPayAmt(20000);
								}
								// lhpvCashSmry.setLcsPayAmt(20000);
								lhpvCashSmry.setLcsCashDiscR(lhpvAdv.getLaCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvAdv.getLaMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvAdv.getLaTdsR());
								lhpvCashSmry.setLaId(lhpvAdv.getLaId());
								System.out.println("10 lhpvAdv.getLaId() = "+ lhpvAdv.getLaId());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - 20000;

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextAdvDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
									lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
									System.out.println("11 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvAdv.getLaBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_ADV);
										lhpvCashSmry1.setLcsBrhCode(lhpvAdv.getLaBrhCode());
										lhpvCashSmry1.setLaId(lhpvAdv.getLaId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										System.out.println("12 lhpvAdv.getLaId() = " + lhpvAdv.getLaId());

										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}
								}

							}
						}
					}
				}

				if (!actLBList.isEmpty()) {
					for (int i = 0; i < actLBList.size(); i++) {
						LhpvBal lhpvBal = actLBList.get(i);
						String chlnCode = lhpvBal.getChallan().getChlnCode();
						double todayTotPay = lhpvBal.getLbTotPayAmt();
						double othTotPay = todayTotPay - lhpvBal.getLbLryBalP();
						double alrdyPayAmt = 0;

						List<LhpvCashSmry> lcsList = new ArrayList<>();
						Criteria cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_BRH_CODE,branch.getBranchFaCode()));
						lcsList = cr.list();

						if (!lcsList.isEmpty()) {
							// Check the already paid amt for the same
							// broker/owner
							for (int j = 0; j < lcsList.size(); j++) {
								if (lhpvBal.getLbBrkOwn().equalsIgnoreCase(lcsList.get(j).getLcsBrkOwn())) {
									alrdyPayAmt = alrdyPayAmt + lcsList.get(j).getLcsPayAmt();
								}
							}

							if (alrdyPayAmt < 20000) {
								// enter into today and rest of the amt into
								// next date

								if ((20000 - alrdyPayAmt) >= othTotPay) {
									// other payment condition true--->same date
									// + next date
									LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

									lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry.setUserCode(currentUser.getUserCode());
									lhpvCashSmry.setLcsToday(true);
									lhpvCashSmry.setLcsAdvEntr(false);
									lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
									lhpvCashSmry.setLcsChlnCode(chlnCode);
									lhpvCashSmry.setLcsCsEntr(false);
									lhpvCashSmry.setLcsLhpvDt(sqlDate);
									lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
									lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
									lhpvCashSmry.setLbId(lhpvBal.getLbId());
									if (todayTotPay <= (20000 - alrdyPayAmt)) {
										lhpvCashSmry.setLcsPayAmt(todayTotPay);
									} else {
										lhpvCashSmry.setLcsPayAmt(20000 - alrdyPayAmt);
									}

									lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
									lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
									lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
									lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
									lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
									lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
									lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
									lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
									lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
									lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
									lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
									lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
									lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());
									lhpvCashSmry.setLbId(lhpvBal.getLbId());

									session.save(lhpvCashSmry);

									double remTotPay = todayTotPay	- (20000 - alrdyPayAmt);

									if (remTotPay <= 20000 && remTotPay > 0) {
										Date nextDt = CodePatternService.getNextDt(sqlDate);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
										lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
										lhpvCashSmry1.setLcsPayAmt(remTotPay);
										lhpvCashSmry1.setLbId(lhpvBal.getLbId());

										session.save(lhpvCashSmry1);
									} else {
										Date nextAdvDt = sqlDate;
										while (remTotPay > 0) {
											nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry1.setLbId(lhpvBal.getLbId());
											if (remTotPay >= 20000) {
												lhpvCashSmry1.setLcsPayAmt(20000);
											} else {
												lhpvCashSmry1.setLcsPayAmt(remTotPay);
											}

											session.save(lhpvCashSmry1);
											remTotPay = remTotPay - 20000;
										}

									}

								} else {
									// other payment condition false--->next
									// date

									if (todayTotPay <= 20000 && todayTotPay > 0) {
										Date nextDt = CodePatternService.getNextDt(sqlDate);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
										lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
										lhpvCashSmry1.setLcsPayAmt(todayTotPay);
										lhpvCashSmry1.setLbId(lhpvBal.getLbId());

										lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
										lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
										lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
										lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
										lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
										lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
										lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
										lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
										lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
										lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
										lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
										lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
										lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

										session.save(lhpvCashSmry1);
									} else {
										Date nextAdvDt = sqlDate;
										boolean flag = true;

										while (todayTotPay > 0) {
											nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry1.setLbId(lhpvBal.getLbId());
											if (todayTotPay >= 20000) {
												lhpvCashSmry1.setLcsPayAmt(20000);
											} else {
												lhpvCashSmry1.setLcsPayAmt(todayTotPay);
											}

											if (flag) {
												lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
												lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
												lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
												lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
												lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
												lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
												lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
												lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
												lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
												lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
												lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
												lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
												lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

												flag = false;
											}

											session.save(lhpvCashSmry1);
											todayTotPay = todayTotPay - 20000;
										}
									}
								}

							} else {
								// advance entry for this broker/owner
								// get the last entry of this broker/owner
								double lastPayAmt = 0;
								List<LhpvCashSmry> lastLCSList = new ArrayList<>();
								Query query = session.createQuery("from LhpvCashSmry where lcsBrkOwn= :type order by lcsId DESC");
								query.setParameter("type",lhpvBal.getLbBrkOwn());
								lastLCSList = query.setMaxResults(1).list();

								if (!lastLCSList.isEmpty()) {
									LhpvCashSmry lastLCS = lastLCSList.get(0);
									Date lastDt = lastLCS.getLcsLhpvDt();
									// check last time amt paid to this
									// broker/owner
									if (lastLCS.getLcsPayAmt() < 20000) {
										// enter into last date and rest of the
										// amt into next date

										if ((20000 - lastLCS.getLcsPayAmt()) >= othTotPay) {
											// other payment condition
											// true--->same date + next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(lastDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry.setLbId(lhpvBal.getLbId());
											if (todayTotPay <= (20000 - lastLCS.getLcsPayAmt())) {
												lhpvCashSmry.setLcsPayAmt(todayTotPay);
											} else {
												lhpvCashSmry.setLcsPayAmt(20000 - lastLCS.getLcsPayAmt());
											}

											lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
											lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
											lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
											lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
											lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
											lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
											lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
											lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
											lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
											lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
											lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
											lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
											lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

											session.save(lhpvCashSmry);

											double remTotPay = todayTotPay	- (20000 - lastLCS.getLcsPayAmt());

											if (remTotPay <= 20000	&& remTotPay > 0) {
												Date nextDt = CodePatternService.getNextDt(lastDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
												lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
												lhpvCashSmry1.setLcsPayAmt(remTotPay);
												lhpvCashSmry1.setLbId(lhpvBal.getLbId());

												session.save(lhpvCashSmry1);
											} else {
												Date nextAdvDt = lastDt;
												while (remTotPay > 0) {
													nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
													LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

													lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
													lhpvCashSmry1.setUserCode(currentUser.getUserCode());
													lhpvCashSmry1.setLcsToday(false);
													lhpvCashSmry1.setLcsAdvEntr(true);
													lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
													lhpvCashSmry1.setLcsChlnCode(chlnCode);
													lhpvCashSmry1.setLcsCsEntr(false);
													lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
													lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
													lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
													lhpvCashSmry1.setLbId(lhpvBal.getLbId());
													if (remTotPay >= 20000) {
														lhpvCashSmry1.setLcsPayAmt(20000);
													} else {
														lhpvCashSmry1.setLcsPayAmt(remTotPay);
													}

													session.save(lhpvCashSmry1);
													remTotPay = remTotPay - 20000;
												}

											}
										} else {
											// other payment condition
											// false--->next date

											if (todayTotPay <= 20000 && todayTotPay > 0) {
												Date nextDt = CodePatternService.getNextDt(lastDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
												lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
												lhpvCashSmry1.setLcsPayAmt(todayTotPay);
												lhpvCashSmry1.setLbId(lhpvBal.getLbId());

												lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
												lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
												lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
												lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
												lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
												lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
												lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
												lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
												lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
												lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
												lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
												lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
												lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

												session.save(lhpvCashSmry1);
											} else {
												Date nextAdvDt = lastDt;
												boolean flag = true;

												while (todayTotPay > 0) {
													nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
													LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

													lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
													lhpvCashSmry1.setUserCode(currentUser.getUserCode());
													lhpvCashSmry1.setLcsToday(false);
													lhpvCashSmry1.setLcsAdvEntr(true);
													lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
													lhpvCashSmry1.setLcsChlnCode(chlnCode);
													lhpvCashSmry1.setLcsCsEntr(false);
													lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
													lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
													lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
													lhpvCashSmry1.setLbId(lhpvBal.getLbId());
													if (todayTotPay >= 20000) {
														lhpvCashSmry1.setLcsPayAmt(20000);
													} else {
														lhpvCashSmry1.setLcsPayAmt(todayTotPay);
													}

													if (flag) {
														lhpvCashSmry1.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
														lhpvCashSmry1.setLcsMunsR(lhpvBal.getLbMunsR());
														lhpvCashSmry1.setLcsTdsR(lhpvBal.getLbTdsR());
														lhpvCashSmry1.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
														lhpvCashSmry1.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
														lhpvCashSmry1.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
														lhpvCashSmry1.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
														lhpvCashSmry1.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
														lhpvCashSmry1.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
														lhpvCashSmry1.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
														lhpvCashSmry1.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
														lhpvCashSmry1.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
														lhpvCashSmry1.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

														flag = false;
													}

													session.save(lhpvCashSmry1);
													todayTotPay = todayTotPay - 20000;
												}
											}
										}

									} else {
										// entery start from next day
										Date nextDt = CodePatternService
												.getNextDt(lastDt);
										if (todayTotPay <= 20000&& todayTotPay > 0) {
											// Simple one entry into next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(nextDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
											lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
											lhpvCashSmry.setLbId(lhpvBal.getLbId());

											lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
											lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
											lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
											lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
											lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
											lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
											lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
											lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
											lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
											lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
											lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
											lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
											lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

											session.save(lhpvCashSmry);
										} else {
											// multiple advance entries for this
											// broker/owner
											Date nextAdvDt = lastDt;
											boolean flag = true;

											while (todayTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

												lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry.setUserCode(currentUser.getUserCode());
												lhpvCashSmry.setLcsToday(false);
												lhpvCashSmry.setLcsAdvEntr(true);
												lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
												lhpvCashSmry.setLcsChlnCode(chlnCode);
												lhpvCashSmry.setLcsCsEntr(false);
												lhpvCashSmry.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
												lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
												lhpvCashSmry.setLbId(lhpvBal.getLbId());
												if (todayTotPay >= 20000) {
													lhpvCashSmry.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry.setLcsPayAmt(todayTotPay);
												}

												if (flag) {
													lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
													lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
													lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
													lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
													lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
													lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
													lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
													lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
													lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
													lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
													lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
													lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
													lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

													flag = false;
												}

												session.save(lhpvCashSmry);
												todayTotPay = todayTotPay - 20000;
											}
										}
									}
								}
							}

						} else {
							// Simple enter LhpvBAL to LhpvCashSmry in sqlDate

							if (todayTotPay <= 20000) {

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
								lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
								lhpvCashSmry.setLcsPayAmt(todayTotPay);
								lhpvCashSmry.setLbId(lhpvBal.getLbId());

								lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
								lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
								lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
								lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

								session.save(lhpvCashSmry);

							} else {

								// multiple advance entries for this
								// broker/owner and date start from sqlDate

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_BAL);
								lhpvCashSmry.setLcsBrhCode(lhpvBal.getLbBrhCode());
								lhpvCashSmry.setLcsPayAmt(20000);
								lhpvCashSmry.setLbId(lhpvBal.getLbId());

								lhpvCashSmry.setLcsCashDiscR(lhpvBal.getLbCashDiscR());
								lhpvCashSmry.setLcsMunsR(lhpvBal.getLbMunsR());
								lhpvCashSmry.setLcsTdsR(lhpvBal.getLbTdsR());
								lhpvCashSmry.setLcsWtShrtgCR(lhpvBal.getLbWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvBal.getLbDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvBal.getLbLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvBal.getLbLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvBal.getLbOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvBal.getLbOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvBal.getLbOthPnltyP());
								lhpvCashSmry.setLcsOthMiscP(lhpvBal.getLbOthMiscP());
								lhpvCashSmry.setLcsUnpDetP(lhpvBal.getLbUnpDetP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvBal.getLbUnLoadingP());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - 20000;

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextAdvDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
									lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLbId(lhpvBal.getLbId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvBal.getLbBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_BAL);
										lhpvCashSmry1.setLcsBrhCode(lhpvBal.getLbBrhCode());
										lhpvCashSmry1.setLbId(lhpvBal.getLbId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}
								}

							}
						}
					}
				}

				if (!actLSList.isEmpty()) {
					for (int i = 0; i < actLSList.size(); i++) {
						LhpvSup lhpvSup = actLSList.get(i);
						String chlnCode = lhpvSup.getChallan().getChlnCode();
						double todayTotPay = lhpvSup.getLspTotPayAmt();
						double alrdyPayAmt = 0;

						List<LhpvCashSmry> lcsList = new ArrayList<>();
						Criteria cr = session.createCriteria(LhpvCashSmry.class);
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT,sqlDate));
						cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_BRH_CODE,branch.getBranchFaCode()));
						lcsList = cr.list();

						if (!lcsList.isEmpty()) {
							// Check the already paid amt for the same
							// broker/owner
							for (int j = 0; j < lcsList.size(); j++) {
								if (lhpvSup.getLspBrkOwn().equalsIgnoreCase(lcsList.get(j).getLcsBrkOwn())) {
									alrdyPayAmt = alrdyPayAmt + lcsList.get(j).getLcsPayAmt();
								}
							}

							if (alrdyPayAmt < 20000) {
								// enter into today and rest of the amt into
								// next date

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
								lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());

								if (todayTotPay <= (20000 - alrdyPayAmt)) {
									lhpvCashSmry.setLcsPayAmt(todayTotPay);
								} else {
									lhpvCashSmry.setLcsPayAmt(20000 - alrdyPayAmt);
								}

								lhpvCashSmry.setLspId(lhpvSup.getLspId());

								lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
								lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
								lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - (20000 - alrdyPayAmt);

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
									lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());

									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLspId(lhpvSup.getLspId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
										lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
										lhpvCashSmry1.setLspId(lhpvSup.getLspId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}

										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}

								}

							} else {
								// advance entry for this broker/owner
								// get the last entry of this broker/owner
								double lastPayAmt = 0;
								List<LhpvCashSmry> lastLCSList = new ArrayList<>();
								Query query = session.createQuery("from LhpvCashSmry where lcsBrkOwn= :type order by lcsId DESC");
								query.setParameter("type",lhpvSup.getLspBrkOwn());
								lastLCSList = query.setMaxResults(1).list();

								if (!lastLCSList.isEmpty()) {
									LhpvCashSmry lastLCS = lastLCSList.get(0);
									Date lastDt = lastLCS.getLcsLhpvDt();
									// check last time amt paid to this
									// broker/owner
									if (lastLCS.getLcsPayAmt() < 20000) {
										// enter into last date and rest of the
										// amt into next date

										LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

										lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry.setUserCode(currentUser.getUserCode());
										lhpvCashSmry.setLcsToday(false);
										lhpvCashSmry.setLcsAdvEntr(true);
										lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
										lhpvCashSmry.setLcsChlnCode(chlnCode);
										lhpvCashSmry.setLcsCsEntr(false);
										lhpvCashSmry.setLcsLhpvDt(lastDt);
										lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
										lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());

										if (todayTotPay <= (20000 - lastLCS.getLcsPayAmt())) {
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
										} else {
											lhpvCashSmry.setLcsPayAmt(20000 - lastLCS.getLcsPayAmt());
										}

										lhpvCashSmry.setLspId(lhpvSup.getLspId());

										lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
										lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
										lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
										lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
										lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
										lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
										lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
										lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
										lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
										lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

										session.save(lhpvCashSmry);

										double remTotPay = todayTotPay- (20000 - lastLCS.getLcsPayAmt());

										if (remTotPay <= 20000 && remTotPay > 0) {
											Date nextDt = CodePatternService.getNextDt(lastDt);
											LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

											lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry1.setUserCode(currentUser.getUserCode());
											lhpvCashSmry1.setLcsToday(false);
											lhpvCashSmry1.setLcsAdvEntr(true);
											lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
											lhpvCashSmry1.setLcsChlnCode(chlnCode);
											lhpvCashSmry1.setLcsCsEntr(false);
											lhpvCashSmry1.setLcsLhpvDt(nextDt);
											lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
											lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
											lhpvCashSmry1.setLspId(lhpvSup.getLspId());

											session.save(lhpvCashSmry1);
										} else {
											Date nextAdvDt = lastDt;
											while (remTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

												lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry1.setUserCode(currentUser.getUserCode());
												lhpvCashSmry1.setLcsToday(false);
												lhpvCashSmry1.setLcsAdvEntr(true);
												lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
												lhpvCashSmry1.setLcsChlnCode(chlnCode);
												lhpvCashSmry1.setLcsCsEntr(false);
												lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
												lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
												lhpvCashSmry1.setLspId(lhpvSup.getLspId());
												if (remTotPay >= 20000) {
													lhpvCashSmry1.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry1.setLcsPayAmt(remTotPay);
												}

												session.save(lhpvCashSmry1);
												remTotPay = remTotPay - 20000;
											}

										}

									} else {
										// entery start from next day
										Date nextDt = CodePatternService.getNextDt(lastDt);
										if (todayTotPay <= 20000 && todayTotPay > 0) {
											// Simple one entry into next date
											LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

											lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
											lhpvCashSmry.setUserCode(currentUser.getUserCode());
											lhpvCashSmry.setLcsToday(false);
											lhpvCashSmry.setLcsAdvEntr(true);
											lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
											lhpvCashSmry.setLcsChlnCode(chlnCode);
											lhpvCashSmry.setLcsCsEntr(false);
											lhpvCashSmry.setLcsLhpvDt(nextDt);
											lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
											lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());
											lhpvCashSmry.setLcsPayAmt(todayTotPay);
											lhpvCashSmry.setLspId(lhpvSup.getLspId());

											lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
											lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
											lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
											lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
											lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
											lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
											lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
											lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
											lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
											lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

											session.save(lhpvCashSmry);
										} else {
											// multiple advance entries for this
											// broker/owner
											Date nextAdvDt = lastDt;
											boolean flag = true;

											while (todayTotPay > 0) {
												nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
												LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

												lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
												lhpvCashSmry.setUserCode(currentUser.getUserCode());
												lhpvCashSmry.setLcsToday(false);
												lhpvCashSmry.setLcsAdvEntr(true);
												lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
												lhpvCashSmry.setLcsChlnCode(chlnCode);
												lhpvCashSmry.setLcsCsEntr(false);
												lhpvCashSmry.setLcsLhpvDt(nextAdvDt);
												lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
												lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());
												lhpvCashSmry.setLspId(lhpvSup.getLspId());
												if (todayTotPay >= 20000) {
													lhpvCashSmry.setLcsPayAmt(20000);
												} else {
													lhpvCashSmry.setLcsPayAmt(todayTotPay);
												}

												if (flag) {
													lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
													lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
													lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
													lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
													lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
													lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
													lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
													lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
													lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
													lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());
												}

												session.save(lhpvCashSmry);
												todayTotPay = todayTotPay - 20000;
											}
										}
									}
								}
							}

						} else {
							// Simple enter LhpSUP to LhpvCashSmry in sqlDate

							//if (todayTotPay <= 20000 && todayTotPay > 0) {
							if (todayTotPay <= 20000) {//this code is replaced by kamal (from commented condition to this condition)

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
								lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());
								lhpvCashSmry.setLcsPayAmt(todayTotPay);
								lhpvCashSmry.setLspId(lhpvSup.getLspId());

								lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
								lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
								lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

								session.save(lhpvCashSmry);

							} else {

								// multiple advance entries for this
								// broker/owner and date start from sqlDate

								LhpvCashSmry lhpvCashSmry = new LhpvCashSmry();

								lhpvCashSmry.setbCode(currentUser.getUserBranchCode());
								lhpvCashSmry.setUserCode(currentUser.getUserCode());
								lhpvCashSmry.setLcsToday(true);
								lhpvCashSmry.setLcsAdvEntr(false);
								lhpvCashSmry.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
								lhpvCashSmry.setLcsChlnCode(chlnCode);
								lhpvCashSmry.setLcsCsEntr(false);
								lhpvCashSmry.setLcsLhpvDt(sqlDate);
								lhpvCashSmry.setLcsLhpvType(ConstantsValues.LHPV_SUP);
								lhpvCashSmry.setLcsBrhCode(lhpvSup.getLspBrhCode());

								lhpvCashSmry.setLcsPayAmt(20000);
								lhpvCashSmry.setLspId(lhpvSup.getLspId());

								lhpvCashSmry.setLcsWtShrtgCR(lhpvSup.getLspWtShrtgCR());
								lhpvCashSmry.setLcsDrRcvrWtCR(lhpvSup.getLspDrRcvrWtCR());
								lhpvCashSmry.setLcsLateDelCR(lhpvSup.getLspLateDelCR());
								lhpvCashSmry.setLcsLateAckCR(lhpvSup.getLspLateAckCR());
								lhpvCashSmry.setLcsOthExtKmP(lhpvSup.getLspOthExtKmP());
								lhpvCashSmry.setLcsOthOvrHgtP(lhpvSup.getLspOthOvrHgtP());
								lhpvCashSmry.setLcsOthPnltyP(lhpvSup.getLspOthPnltyP());
								lhpvCashSmry.setLcsUnLoadingP(lhpvSup.getLspUnLoadingP());
								lhpvCashSmry.setLcsUnpDetP(lhpvSup.getLspUnpDetP());
								lhpvCashSmry.setLcsOthMiscP(lhpvSup.getLspOthMiscP());

								session.save(lhpvCashSmry);

								double remTotPay = todayTotPay - 20000;

								if (remTotPay <= 20000 && remTotPay > 0) {
									Date nextAdvDt = CodePatternService.getNextDt(sqlDate);
									LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

									lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
									lhpvCashSmry1.setUserCode(currentUser.getUserCode());
									lhpvCashSmry1.setLcsToday(false);
									lhpvCashSmry1.setLcsAdvEntr(true);
									lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
									lhpvCashSmry1.setLcsChlnCode(chlnCode);
									lhpvCashSmry1.setLcsCsEntr(false);
									lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
									lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
									lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
									lhpvCashSmry1.setLcsPayAmt(remTotPay);
									lhpvCashSmry1.setLspId(lhpvSup.getLspId());

									session.save(lhpvCashSmry1);
								} else {
									Date nextAdvDt = sqlDate;
									while (remTotPay > 0) {
										nextAdvDt = CodePatternService.getNextDt(nextAdvDt);
										LhpvCashSmry lhpvCashSmry1 = new LhpvCashSmry();

										lhpvCashSmry1.setbCode(currentUser.getUserBranchCode());
										lhpvCashSmry1.setUserCode(currentUser.getUserCode());
										lhpvCashSmry1.setLcsToday(false);
										lhpvCashSmry1.setLcsAdvEntr(true);
										lhpvCashSmry1.setLcsBrkOwn(lhpvSup.getLspBrkOwn());
										lhpvCashSmry1.setLcsChlnCode(chlnCode);
										lhpvCashSmry1.setLcsCsEntr(false);
										lhpvCashSmry1.setLcsLhpvDt(nextAdvDt);
										lhpvCashSmry1.setLcsLhpvType(ConstantsValues.LHPV_SUP);
										lhpvCashSmry1.setLcsBrhCode(lhpvSup.getLspBrhCode());
										lhpvCashSmry1.setLspId(lhpvSup.getLspId());
										if (remTotPay >= 20000) {
											lhpvCashSmry1.setLcsPayAmt(20000);
										} else {
											lhpvCashSmry1.setLcsPayAmt(remTotPay);
										}
										session.save(lhpvCashSmry1);
										remTotPay = remTotPay - 20000;
									}
								}

							}
						}
					}
				}

				res = 1;

		} else {
			res = 0;
		}
		return res;
	}

	//TODO End
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvCashSmry> getLhpvCash(Date date, String bCode) {
		System.out.println("enter into getLhpvCash function");
		List<LhpvCashSmry> lhpvCashList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
//			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(LhpvCashSmry.class);
			cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT, date));
			cr.add(Restrictions.eq(LhpvCashSmryCNTS.B_CODE, bCode));
			lhpvCashList = cr.list();

	//		session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvCashList;
	}

	
	
	
	
	
	public List<LhpvCashSmry> getLhpvCash(Date date, String bCode,Session session) {
		System.out.println("enter into getLhpvCash function");
		List<LhpvCashSmry> lhpvCashList = new ArrayList<>();
			Criteria cr = session.createCriteria(LhpvCashSmry.class);
			cr.add(Restrictions.eq(LhpvCashSmryCNTS.LCS_LHPV_DT, date));
			cr.add(Restrictions.eq(LhpvCashSmryCNTS.B_CODE, bCode));
			lhpvCashList = cr.list();

		return lhpvCashList;
	}

	
	
	
	@SuppressWarnings("unchecked")
	public LhpvStatus getLssByDt(String bCode, Date date) {
		System.out.println("enter into getLssByDt function");
		List<LhpvStatus> lsList = new ArrayList<>();
		LhpvStatus lhpvStatus = null;
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, date));
			cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, bCode));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				lhpvStatus = lsList.get(0);
			}
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvStatus;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> chkBackDt() {
		System.out.println("enter into chkBackDt function");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			session = this.sessionFactory.openSession();

			/*
			 * Criteria cr = session.createCriteria(MoneyReceipt.class);
			 * cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,
			 * "Payment Detail"));
			 * 
			 * List<MoneyReceipt> moneyReceiptlList = cr.list();
			 * 
			 * System.out.println("moneyReceiptlList size: "+moneyReceiptlList.size
			 * ());
			 * 
			 * if (!moneyReceiptlList.isEmpty()) { for (MoneyReceipt mr :
			 * moneyReceiptlList) { System.out.println("mrId: "+mr.getMrId());
			 * 
			 * List<Map<String, Object>> mrFrPdList = mr.getMrFrPDList();
			 * 
			 * for (Map<String, Object> mrFrPdMap : mrFrPdList) {
			 * System.out.println("mrFrPdMap: "+mrFrPdMap.entrySet()); if
			 * (String.valueOf(mrFrPdMap.get("brhId")).equalsIgnoreCase("2") &&
			 * String
			 * .valueOf(mrFrPdMap.get("onAccMr")).equalsIgnoreCase("MR000006"))
			 * { System.err.println("we got mr: "+mr.getMrId()); }
			 * 
			 * if (String.valueOf(mrFrPdMap.get("brhId")).equalsIgnoreCase("2")
			 * &&
			 * String.valueOf(mrFrPdMap.get("onAccMr")).equalsIgnoreCase("MR000008"
			 * )) { System.err.println("we got mr: "+mr.getMrId()); }
			 * 
			 * if (String.valueOf(mrFrPdMap.get("brhId")).equalsIgnoreCase("4")
			 * &&
			 * String.valueOf(mrFrPdMap.get("onAccMr")).equalsIgnoreCase("MR000001"
			 * )) { System.err.println("we got mr: "+mr.getMrId()); } } }
			 * 
			 * }
			 */

			// transaction = session.beginTransaction();

			// wrong lhpv
			/*
			 * int counter = 0; List<Branch> brhList = new ArrayList<Branch>();
			 * Criteria cr = session.createCriteria(Branch.class); brhList =
			 * cr.list();
			 * 
			 * for(int i=0;i<brhList.size();i++){ List<CashStmtStatus> cssList =
			 * new ArrayList<CashStmtStatus>(); cr =
			 * session.createCriteria(CashStmtStatus.class); cr =
			 * session.createCriteria
			 * (CashStmtStatusCNTS.B_Code,String.valueOf(brhList
			 * .get(i).getBranchId())); cssList = cr.list();
			 * if(!cssList.isEmpty()){ for(int j=0;j<cssList.size();j++){
			 * List<CashStmt> csList = new ArrayList<CashStmt>(); csList =
			 * cssList.get(j).getCashStmtList(); double lhpvAmt = 0; for(int
			 * k=0;k<csList.size();k++){
			 * if(csList.get(j).getCsType().equalsIgnoreCase(anotherString)) } }
			 * double lhpvAmt = 0;
			 * 
			 * } }
			 */

			/*
			 * List<LhpvAdv> laList = new ArrayList<LhpvAdv>(); Criteria cr =
			 * session.createCriteria(LhpvAdv.class);
			 * cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_PAY_BY,'C')); laList
			 * = cr.list(); int count = 0; for(int i=0;i<laList.size();i++){
			 * List<LhpvCashSmry> lcsList = new ArrayList<LhpvCashSmry>(); cr =
			 * session.createCriteria(LhpvCashSmry.class);
			 * cr.add(Restrictions.eq
			 * (LhpvCashSmryCNTS.LA_ID,laList.get(i).getLaId())); lcsList =
			 * cr.list(); if(!lcsList.isEmpty()){ for(int
			 * j=0;j<lcsList.size();j++){
			 * if(lcsList.get(j).getLcsLhpvDt().before
			 * (laList.get(i).getLaDt())){ count = count + 1;
			 * 
			 * } } } } System.out.println("count = "+count);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> checkBackDtLhpv(Date date) {
		System.out.println("enter into checkBackDtLhpv function");
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try {
			session = this.sessionFactory.openSession();
			List<LhpvStatus> lsList = new ArrayList<LhpvStatus>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE,
					currentUser.getUserBranchCode()));
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, date));
			lsList = cr.list();
			if (!lsList.isEmpty()) {
				LhpvStatus lhpvStatus = lsList.get(0);
				System.out
						.println("IS LS BACK DT : " + lhpvStatus.isLsBackDt());
				if (lhpvStatus.isLsBackDt()) {
					if (lhpvStatus.isLsClose()) {
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						map.put("msg", "Already close the LHPV of " + date);
					} else {
						map.put("lhpv", lhpvStatus);
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						map.put("msg", "right lhpv");
					}
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					map.put("msg", "wrong date for LHPV");
				}
			} else {
				List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
				cr = session.createCriteria(CashStmtStatus.class);
				cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,
						currentUser.getUserBranchCode()));
				cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, date));
				cssList = cr.list();
				if (!cssList.isEmpty()) {
					LhpvStatus lhpvStatus = new LhpvStatus();
					lhpvStatus.setbCode(currentUser.getUserBranchCode());
					lhpvStatus.setLsClose(false);
					lhpvStatus.setLsBackDt(true);
					lhpvStatus.setLsDt(date);
					lhpvStatus.setLsNo(cssList.get(0).getCssCsNo());
					lhpvStatus.setUserCode(currentUser.getUserCode());

					session.save(lhpvStatus);
					map.put("lhpv", lhpvStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					map.put("msg", "New lhpvstatus created");
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					map.put("msg", "There is no CS for date " + date);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int closeLhpvTemp(Date date, int brhId) {
		System.out.println("enter into closeLhpvTemp function");
		int res = 0;
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();

			List<LhpvStatus> lssList = new ArrayList<LhpvStatus>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, String.valueOf(brhId)));
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, date));
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_BACK, true));
			lssList = cr.list();

			if (!lssList.isEmpty()) {
				LhpvStatus lhpvStatus = lssList.get(0);
				lhpvStatus.setLsClose(true);

				session.update(lhpvStatus);
				transaction.commit();
				res = 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.clear();
		return res;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> chkTempLhpv(Date date, int brhId) {
		System.out.println("enter into chkTempLhpv function");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			session = this.sessionFactory.openSession();
			List<LhpvStatus> lssList = new ArrayList<LhpvStatus>();
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, String.valueOf(brhId)));
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, date));
			lssList = cr.list();

			if (!lssList.isEmpty()) {
				if (lssList.get(0).isLsBackDt()) {
					if (lssList.get(0).isLsClose()) {
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						map.put("msg", "You already close the LHPV of " + date);
					} else {
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					}
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					map.put("msg", "You can't close the LHPV of " + date);
				}
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				map.put("msg", "wrong Date");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public LhpvStatus getLssByDt(String bCode, Date date, Session session) {
		System.out.println("enter into getLssByDt function");
		List<LhpvStatus> lsList = new ArrayList<>();
		LhpvStatus lhpvStatus = null;
		try {
			Criteria cr = session.createCriteria(LhpvStatus.class);
			cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, date));
			cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, bCode));
			lsList = cr.list();

			if (!lsList.isEmpty()) {
				lhpvStatus = lsList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lhpvStatus;
	}
	
	@Override
	public LhpvStatus getLastLhpvStatusBack(String bCode) {
		System.out.println("enter into getLastLhpvStatus function");
		List<LhpvStatus> lhstList = new ArrayList<LhpvStatus>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Query query = session
					.createQuery("from LhpvStatus where bCode= :bCode and lsBackDt= :lsBackDt and lsDt <= '2017-10-31' order by lsId DESC");
			query.setString("bCode", bCode);
			query.setBoolean("lsBackDt", false);
			lhstList = query.setMaxResults(1).list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		if (lhstList.isEmpty()) {
			return null;
		} else {
			return lhstList.get(0);
		}
	}

	//TODO new lhpv program
	@SuppressWarnings("unchecked")
	public int saveLhpvBalByBrh(VoucherService voucherService,Session session){
		System.out.println("Enter into saveLhpvBalByBrh()");
		int j=0;
		if(!voucherService.getLhpvBalList().isEmpty()){
			for(int i=0;i<voucherService.getLhpvBalList().size();i++){
				Challan challan=voucherService.getLhpvBalList().get(i).getChallan();
				challan.setBankName(voucherService.getActBankName());
				challan.setAccountNo(voucherService.getActAccountNo());
				challan.setIfscCode(voucherService.getActIfscCode());
				challan.setPayeeName(voucherService.getActPayeeName());
				challan.setCardFaCode(voucherService.getCardCode());
				session.update(challan);
				
				/*if(voucherService.getActIfscCode() != null) {
					List<ChallanDetail> chdList=session.createCriteria(ChallanDetail.class)
							.add(Restrictions.eq("chdChlnCode", challan.getChlnCode())).list();
					if(!chdList.isEmpty()) {
						for(int k=0;k<chdList.size();k++) {
							ChallanDetail chd=chdList.get(k);
							chd.setChdPanHdrType(voucherService.getAcntHldr());
							session.update(chd);
						}
					}
				}*/
				
			}
			j=1;
		}
		
		return j;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public int saveLhpvBalHo(VoucherService voucherService,Session session) {
		System.out.println("enter into saveLhpvBal function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		// Map<String, Object> map = new HashMap<String, Object>();
		//LhpvStatus lhpvStatus = voucherService.getLhpvStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<LhpvBal> lhpvBalList = voucherService.getLhpvBalList();
		System.out.println("size of lhpvBalList = " + lhpvBalList.size());
		
		
		LhpvStatus lhpvStatus=getLhpvStatusByDateBcode(voucherService.getBranch().getBranchCode(),voucherService.getCurrentDate(),session);

		int lhpvBalNo = 0;
		
		if(lhpvStatus!= null){
			
			if (!lhpvBalList.isEmpty()) {

					java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
							.getTime());
					Criteria cr = session.createCriteria(LhpvBal.class);
					cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, sqlDate));
					cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BCODE,
							voucherService.getBranch().getBranchCode()));
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property("lbNo"));
					cr.setProjection(proList);

					List<Integer> lhpvBalNoList = cr.list();

					if (!lhpvBalNoList.isEmpty()) {
						int laNo = lhpvBalNoList.get(lhpvBalNoList.size() - 1);
						lhpvBalNo = laNo + 1;
					} else {
						lhpvBalNo = 1;
					}

					char payBy = voucherService.getPayBy();
					String payToStf =  voucherService.getPayToStf();
					String stafCode = voucherService.getStafCode();
					System.out.println("value of payBy = " + payBy);
					System.out.println("value of payTo = " + payToStf);
					System.out.println("value of stafCode = " + stafCode);

//					if (payBy == 'C') {
//						for (int i = 0; i < lhpvBalList.size(); i++) {
//							LhpvBal lhpvBal = lhpvBalList.get(i);
//							lhpvBal.setLbDt(sqlDate);
//							lhpvBal.setLbNo(lhpvBalNo);
//							lhpvBal.setLbPayMethod(payBy);
//							lhpvBal.setLbBrhCode(brFaCode);
//							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
//							lhpvBal.setUserCode(currentUser.getUserCode());
//							lhpvBal.setbCode(currentUser.getUserBranchCode());
	//
//							Challan chln = lhpvBal.getChallan();
//							Challan actChln = (Challan) session.get(Challan.class,
//									chln.getChlnId());
	//
//							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
//									LhpvStatus.class, lhpvStatus.getLsId());
//							actLhpvSt.getLbList().add(lhpvBal);
//							lhpvBal.setLhpvStatus(actLhpvSt);
	//
//							double rem = actChln.getChlnRemBal();
//							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
//							session.merge(actChln);
//							// actChln.setLhpvBal(lhpvBal);
	//
//							lhpvBal.setChallan(actChln);
//							int lbId = (Integer) session.save(lhpvBal);
//							System.out.println("lbId ===> " + lbId);
//							session.update(actLhpvSt);
	//
//						}
//					} else 
					if (payBy == 'Q') {

						double totPayAmt = 0.0;

						for (int i = 0; i < lhpvBalList.size(); i++) {
							LhpvBal lhpvBal = lhpvBalList.get(i);
							lhpvBal.setLbDt(sqlDate);
							lhpvBal.setLbNo(lhpvBalNo);
							lhpvBal.setLbPayMethod(payBy);
							lhpvBal.setLbBrhCode(brFaCode);
							lhpvBal.setLbBankCode(voucherService.getBankCode());
							lhpvBal.setLbChqType(voucherService.getChequeType());
							lhpvBal.setLbChqNo(voucherService.getChequeLeaves()
									.getChqLChqNo());
							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
							lhpvBal.setUserCode(currentUser.getUserCode());
							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());

							Challan chln = lhpvBal.getChallan();
							Challan actChln = (Challan) session.get(Challan.class,
									chln.getChlnId());
							lhpvBal.setLbBankName(actChln.getBankName());
							lhpvBal.setLbIfscCode(actChln.getIfscCode());
							lhpvBal.setLbAccountNo(actChln.getAccountNo());
							lhpvBal.setLbPayeeName(actChln.getPayeeName());
							lhpvBal.setLbIsPaid(false);

							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
									LhpvStatus.class, lhpvStatus.getLsId());
							actLhpvSt.getLbList().add(lhpvBal);
							lhpvBal.setLhpvStatus(actLhpvSt);

							double rem = actChln.getChlnRemBal();
							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
							session.merge(actChln);

							lhpvBal.setChallan(actChln);
							int lbId = (Integer) session.save(lhpvBal);
							System.out.println("lbId ===> " + lbId);
							session.update(actLhpvSt);

							totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
						}

						List<BankMstr> bankMstrList = new ArrayList<>();
						char chequeType = voucherService.getChequeType();
						System.out.println("chequeType = " + chequeType);
						String bankCode = voucherService.getBankCode();
						cr = session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
						bankMstrList = cr.list();

						if (!bankMstrList.isEmpty()) {
							
							// It will cancel the cheque if lhpv is reversed
							if(totPayAmt < 0) {
									List<ChequeLeaves> chqL =new ArrayList<>();
									for(int i=0;i<bankMstrList.get(0).getChequeLeavesList().size();i++){
										if(bankMstrList.get(0).getChequeLeavesList().get(i).getChqLChqNo().equalsIgnoreCase(voucherService.getChequeLeaves().getChqLChqNo()))
											chqL.add(bankMstrList.get(0).getChequeLeavesList().get(i));
									}
									System.out.println("chqId= "+chqL.get(0).getChqLChqNo());

									ChequeLeaves chq = chqL.get(0);
									chq.setChqLChqCancelDt(new Date(new java.util.Date()
											.getTime()));
									chq.setChqLCancel(true);
									
									session.merge(chq);
									
							}else{

							List<ChequeLeaves> chqL ;
							ChequeLeaves chequeLeaves = voucherService
									.getChequeLeaves();
							cr = session.createCriteria(ChequeLeaves.class);
							cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
									chequeLeaves.getChqLId()));
							chqL = cr.list();

							ChequeLeaves chq = chqL.get(0);
							chq.setChqLChqAmt(totPayAmt);
							chq.setChqLUsedDt(new Date(new java.util.Date()
									.getTime()));
							chq.setChqLUsed(true);
							
							session.merge(chq);
							
							}

							BankMstr bank = bankMstrList.get(0);
							double balAmt = bank.getBnkBalanceAmt();
							double dedAmt = totPayAmt;
							double newBalAmt = balAmt - dedAmt;
							System.out.println("balAmt = " + balAmt);
							System.out.println("dedAmt = " + dedAmt);
							System.out.println("newBalAmt = " + newBalAmt);
							bank.setBnkBalanceAmt(newBalAmt);
							session.merge(bank);
						}

					} else if (payBy == 'R') {

						double totPayAmt = 0.0;

						for (int i = 0; i < lhpvBalList.size(); i++) {
							LhpvBal lhpvBal = lhpvBalList.get(i);
							lhpvBal.setLbDt(sqlDate);
							lhpvBal.setLbNo(lhpvBalNo);
							lhpvBal.setLbPayMethod(payBy);
							lhpvBal.setLbBrhCode(brFaCode);
							lhpvBal.setLbBankCode(voucherService.getBankCode());
							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
							lhpvBal.setUserCode(currentUser.getUserCode());
							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());
							
							if (payToStf.equals("S")) {
								System.out.println(payToStf);
								lhpvBal.setPayToStf(payToStf);
								lhpvBal.setStafCode(stafCode);
							}
							System.out.println(lhpvBal.getPayToStf());

							Challan chln = lhpvBal.getChallan();
							Challan actChln = (Challan) session.get(Challan.class,
									chln.getChlnId());
							
							lhpvBal.setLbBankName(actChln.getBankName());
							lhpvBal.setLbIfscCode(actChln.getIfscCode());
							lhpvBal.setLbAccountNo(actChln.getAccountNo());
							lhpvBal.setLbPayeeName(actChln.getPayeeName());
							lhpvBal.setLbIsPaid(false);

							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
									LhpvStatus.class, lhpvStatus.getLsId());
							actLhpvSt.getLbList().add(lhpvBal);
							lhpvBal.setLhpvStatus(actLhpvSt);

							double rem = actChln.getChlnRemBal();
							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
							session.merge(actChln);
							// actChln.setLhpvBal(lhpvBal);

							lhpvBal.setChallan(actChln);
							int lbId = (Integer) session.save(lhpvBal);
							System.out.println("lbId ===> " + lbId);
							session.update(actLhpvSt);

							totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
						}


					}else if (payBy == 'P') {

						double totPayAmt = 0.0;

						for (int i = 0; i < lhpvBalList.size(); i++) {
							LhpvBal lhpvBal = lhpvBalList.get(i);
							lhpvBal.setLbDt(sqlDate);
							lhpvBal.setLbNo(lhpvBalNo);
							lhpvBal.setLbPayMethod(payBy);
							lhpvBal.setLbBrhCode(brFaCode);
							lhpvBal.setLbBankCode(voucherService.getBankCode());
							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
							lhpvBal.setUserCode(currentUser.getUserCode());
							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());
							
//							
							System.out.println(lhpvBal.getPayToStf());

							Challan chln = lhpvBal.getChallan();
							Challan actChln = (Challan) session.get(Challan.class,
									chln.getChlnId());
							
							lhpvBal.setLbBankName(actChln.getBankName());
							lhpvBal.setLbIfscCode(actChln.getIfscCode());
							lhpvBal.setLbAccountNo(actChln.getAccountNo());
							lhpvBal.setLbPayeeName(actChln.getPayeeName());
							lhpvBal.setPayToStf(actChln.getBankName());
							lhpvBal.setStafCode(actChln.getCardFaCode());
							lhpvBal.setLbIsPaid(false);

							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
									LhpvStatus.class, lhpvStatus.getLsId());
							actLhpvSt.getLbList().add(lhpvBal);
							lhpvBal.setLhpvStatus(actLhpvSt);

							double rem = actChln.getChlnRemBal();
							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
							session.merge(actChln);
							// actChln.setLhpvBal(lhpvBal);

							lhpvBal.setChallan(actChln);
							int lbId = (Integer) session.save(lhpvBal);
							System.out.println("lbId ===> " + lbId);
							session.update(actLhpvSt);

							totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
						}


					} else {
						System.out.println("invalid payment method");
					}
			} else {
				lhpvBalNo = 0;
			}
			
		}else{
			if (!lhpvBalList.isEmpty()) {

//					java.sql.Date sqlDate = new java.sql.Date(lhpvStatus.getLsDt()
//							.getTime());
					Criteria cr = session.createCriteria(LhpvBal.class);
					cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, voucherService.getCurrentDate()));
					cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_BCODE,
							voucherService.getBranch().getBranchCode()));
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property("lbNo"));
					cr.setProjection(proList);

					List<Integer> lhpvBalNoList = cr.list();

					if (!lhpvBalNoList.isEmpty()) {
						int laNo = lhpvBalNoList.get(lhpvBalNoList.size() - 1);
						lhpvBalNo = laNo + 1;
					} else {
						lhpvBalNo = 1;
					}

					char payBy = voucherService.getPayBy();
					String payToStf =  voucherService.getPayToStf();
					String stafCode = voucherService.getStafCode();
					System.out.println("value of payBy = " + payBy);
					System.out.println("value of payTo = " + payToStf);
					System.out.println("value of stafCode = " + stafCode);

//					if (payBy == 'C') {
//						for (int i = 0; i < lhpvBalList.size(); i++) {
//							LhpvBal lhpvBal = lhpvBalList.get(i);
//							lhpvBal.setLbDt(sqlDate);
//							lhpvBal.setLbNo(lhpvBalNo);
//							lhpvBal.setLbPayMethod(payBy);
//							lhpvBal.setLbBrhCode(brFaCode);
//							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
//							lhpvBal.setUserCode(currentUser.getUserCode());
//							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());
	//
//							Challan chln = lhpvBal.getChallan();
//							Challan actChln = (Challan) session.get(Challan.class,
//									chln.getChlnId());
	//
//							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
//									LhpvStatus.class, lhpvStatus.getLsId());
//							actLhpvSt.getLbList().add(lhpvBal);
//							lhpvBal.setLhpvStatus(actLhpvSt);
	//
//							double rem = actChln.getChlnRemBal();
//							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
//							session.merge(actChln);
//							// actChln.setLhpvBal(lhpvBal);
	//
//							lhpvBal.setChallan(actChln);
//							int lbId = (Integer) session.save(lhpvBal);
//							System.out.println("lbId ===> " + lbId);
//							session.update(actLhpvSt);
	//
//						}
//					} else 
					if (payBy == 'Q') {

						double totPayAmt = 0.0;

						for (int i = 0; i < lhpvBalList.size(); i++) {
							LhpvBal lhpvBal = lhpvBalList.get(i);
							lhpvBal.setLbDt(voucherService.getCurrentDate());
							lhpvBal.setLbNo(lhpvBalNo);
							lhpvBal.setLbPayMethod(payBy);
							lhpvBal.setLbBrhCode(brFaCode);
							lhpvBal.setLbBankCode(voucherService.getBankCode());
							lhpvBal.setLbChqType(voucherService.getChequeType());
							lhpvBal.setLbChqNo(voucherService.getChequeLeaves()
									.getChqLChqNo());
							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
							lhpvBal.setUserCode(currentUser.getUserCode());
							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());

							Challan chln = lhpvBal.getChallan();
							Challan actChln = (Challan) session.get(Challan.class,
									chln.getChlnId());
							lhpvBal.setLbBankName(actChln.getBankName());
							lhpvBal.setLbIfscCode(actChln.getIfscCode());
							lhpvBal.setLbAccountNo(actChln.getAccountNo());
							lhpvBal.setLbPayeeName(actChln.getPayeeName());
							lhpvBal.setLbIsPaid(false);

//							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
//									LhpvStatus.class, lhpvStatus.getLsId());
//							actLhpvSt.getLbList().add(lhpvBal);
//							lhpvBal.setLhpvStatus(actLhpvSt);
							//session.saveOrUpdate(lhpvBal);
							double rem = actChln.getChlnRemBal();
							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
							session.merge(actChln);

							lhpvBal.setChallan(actChln);
							int lbId = (Integer) session.save(lhpvBal);
							System.out.println("lbId ===> " + lbId);
							//session.update(actLhpvSt);

							totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
						}

						List<BankMstr> bankMstrList = new ArrayList<>();
						char chequeType = voucherService.getChequeType();
						System.out.println("chequeType = " + chequeType);
						String bankCode = voucherService.getBankCode();
						cr = session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
						bankMstrList = cr.list();

						if (!bankMstrList.isEmpty()) {
							
							// It will cancel the cheque if lhpv is reversed
							if(totPayAmt < 0) {
									List<ChequeLeaves> chqL =new ArrayList<>();
									for(int i=0;i<bankMstrList.get(0).getChequeLeavesList().size();i++){
										if(bankMstrList.get(0).getChequeLeavesList().get(i).getChqLChqNo().equalsIgnoreCase(voucherService.getChequeLeaves().getChqLChqNo()))
											chqL.add(bankMstrList.get(0).getChequeLeavesList().get(i));
									}
									System.out.println("chqId= "+chqL.get(0).getChqLChqNo());

									ChequeLeaves chq = chqL.get(0);
									chq.setChqLChqCancelDt(new Date(new java.util.Date()
											.getTime()));
									chq.setChqLCancel(true);
									
									session.merge(chq);
									
							}else{

							List<ChequeLeaves> chqL ;
							ChequeLeaves chequeLeaves = voucherService
									.getChequeLeaves();
							cr = session.createCriteria(ChequeLeaves.class);
							cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,
									chequeLeaves.getChqLId()));
							chqL = cr.list();

							ChequeLeaves chq = chqL.get(0);
							chq.setChqLChqAmt(totPayAmt);
							chq.setChqLUsedDt(new Date(new java.util.Date()
									.getTime()));
							chq.setChqLUsed(true);
							
							session.merge(chq);
							
							}

							BankMstr bank = bankMstrList.get(0);
							double balAmt = bank.getBnkBalanceAmt();
							double dedAmt = totPayAmt;
							double newBalAmt = balAmt - dedAmt;
							System.out.println("balAmt = " + balAmt);
							System.out.println("dedAmt = " + dedAmt);
							System.out.println("newBalAmt = " + newBalAmt);
							bank.setBnkBalanceAmt(newBalAmt);
							session.merge(bank);
						}

					} else if (payBy == 'R') {

						double totPayAmt = 0.0;

						for (int i = 0; i < lhpvBalList.size(); i++) {
							LhpvBal lhpvBal = lhpvBalList.get(i);
							lhpvBal.setLbDt(voucherService.getCurrentDate());
							lhpvBal.setLbNo(lhpvBalNo);
							lhpvBal.setLbPayMethod(payBy);
							lhpvBal.setLbBrhCode(brFaCode);
							lhpvBal.setLbBankCode(voucherService.getBankCode());
							lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
							lhpvBal.setUserCode(currentUser.getUserCode());
							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());
							
							if (payToStf.equals("S")) {
								System.out.println(payToStf);
								lhpvBal.setPayToStf(payToStf);
								lhpvBal.setStafCode(stafCode);
							}
							System.out.println(lhpvBal.getPayToStf());

							Challan chln = lhpvBal.getChallan();
							Challan actChln = (Challan) session.get(Challan.class,
									chln.getChlnId());
							
							lhpvBal.setLbBankName(actChln.getBankName());
							lhpvBal.setLbIfscCode(actChln.getIfscCode());
							lhpvBal.setLbAccountNo(actChln.getAccountNo());
							lhpvBal.setLbPayeeName(actChln.getPayeeName());
							lhpvBal.setLbIsPaid(false);

//							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
//									LhpvStatus.class, lhpvStatus.getLsId());
//							actLhpvSt.getLbList().add(lhpvBal);
//							lhpvBal.setLhpvStatus(actLhpvSt);
							
							//session.saveOrUpdate(lhpvBal);
							double rem = actChln.getChlnRemBal();
							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
							session.merge(actChln);
							// actChln.setLhpvBal(lhpvBal);

							lhpvBal.setChallan(actChln);
							int lbId = (Integer) session.save(lhpvBal);
							System.out.println("lbId ===> " + lbId);
							//session.update(actLhpvSt);

							totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
						}


					} else if (payBy == 'P') {

						double totPayAmt = 0.0;

						for (int i = 0; i < lhpvBalList.size(); i++) {
							LhpvBal lhpvBal = lhpvBalList.get(i);
							lhpvBal.setLbDt(voucherService.getCurrentDate());
							lhpvBal.setLbNo(lhpvBalNo);
							lhpvBal.setLbPayMethod(payBy);
							lhpvBal.setLbBrhCode(brFaCode);
							lhpvBal.setLbBankCode(voucherService.getBankCode());
							//lhpvBal.setLbBrkOwn(voucherService.getBrkOwnFaCode());
							lhpvBal.setLbBrkOwn(lhpvBalList.get(i).getLbBrkOwn());
							lhpvBal.setUserCode(currentUser.getUserCode());
							lhpvBal.setbCode(voucherService.getBranch().getBranchCode());
							

							Challan chln = lhpvBal.getChallan();
							Challan actChln = (Challan) session.get(Challan.class,
									chln.getChlnId());
							
							lhpvBal.setPayToStf(actChln.getBankName());
							lhpvBal.setStafCode(actChln.getCardFaCode());
							lhpvBal.setLbBankName(actChln.getBankName());
							lhpvBal.setLbIfscCode(actChln.getIfscCode());
							lhpvBal.setLbAccountNo(actChln.getAccountNo());
							lhpvBal.setLbPayeeName(actChln.getPayeeName());
							lhpvBal.setLbIsPaid(false);

//							LhpvStatus actLhpvSt = (LhpvStatus) session.get(
//									LhpvStatus.class, lhpvStatus.getLsId());
//							actLhpvSt.getLbList().add(lhpvBal);
//							lhpvBal.setLhpvStatus(actLhpvSt);
							
							//session.saveOrUpdate(lhpvBal);
							double rem = actChln.getChlnRemBal();
							actChln.setChlnRemBal(rem - lhpvBal.getLbLryBalP());
							session.merge(actChln);
							// actChln.setLhpvBal(lhpvBal);

							lhpvBal.setChallan(actChln);
							int lbId = (Integer) session.save(lhpvBal);
							System.out.println("lbId ===> " + lbId);
							//session.update(actLhpvSt);

							totPayAmt = totPayAmt + lhpvBal.getLbFinalTot();
						}


					} else {
						System.out.println("invalid payment method");
					}
			} else {
				lhpvBalNo = 0;
			}
		}
		

		return lhpvBalNo;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LhpvBal> getLhpvBal(Date fromDt, Date toDt) {
		// TODO Auto-generated method stub
	  Session session= this.sessionFactory.openSession();
	  List<LhpvBal> lhpvBals=null;
	  if(session!=null)
	  {
		lhpvBals=  session.createCriteria(LhpvBal.class).add(Restrictions.ge(LhpvBalCNTS.LHPV_BAL_DT, fromDt))
				.add(Restrictions.le(LhpvBalCNTS.LHPV_BAL_DT, toDt))
				.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_BANK_NAME, ""))
				.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_ACCOUNT_NO, ""))
				.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_IS_PAID, true)).list();
	  session.close();
	  }
		return lhpvBals;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LhpvBal> getLhpvBalFrPtro(Date date){
		// TODO Auto-generated method stub
		System.out.println("PetroCard");
		  Session session= this.sessionFactory.openSession();
		  List<LhpvBal> lhpvBals=null;
		  if(session!=null)
		  {
			lhpvBals=  session.createCriteria(LhpvBal.class)
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_BANK_NAME, ""))
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_ACCOUNT_NO, ""))
					.add(Restrictions.isNull(LhpvBalCNTS.LHPV_BAL_IFSC_CODE))
					.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, date))
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_IS_PAID, true))
					.addOrder(Order.asc(LhpvBalCNTS.LHPV_BAL_BCODE))
					.addOrder(Order.asc(LhpvBalCNTS.LHPV_BAL_ACCOUNT_NO)).list();
		  session.close();
		  }
			return lhpvBals;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LhpvBal> getLhpvBalFrBank(Date date){
		// TODO Auto-generated method stub
		System.out.println("Bank");
		  Session session= this.sessionFactory.openSession();
		  List<LhpvBal> lhpvBals=null;
		  if(session!=null)
		  {
			lhpvBals=  session.createCriteria(LhpvBal.class)
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_BANK_NAME, ""))
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_ACCOUNT_NO, ""))
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_IFSC_CODE, ""))
					.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, date))
					.add(Restrictions.neOrIsNotNull(LhpvBalCNTS.LHPV_BAL_IS_PAID, true))
					.addOrder(Order.asc(LhpvBalCNTS.LHPV_BAL_BCODE))
					.addOrder(Order.asc(LhpvBalCNTS.LHPV_BAL_ACCOUNT_NO)).list();
		  session.close();
		  }
			return lhpvBals;

	}
	
	
	@Override
	public int updateLhpvBal(List<LhpvBal> lhpvBals)
	{
		int i=0;
	Session session=this.sessionFactory.openSession();
	Transaction tx = session.beginTransaction();
	int count = 0;
		try{
			for (LhpvBal lhpvBal : lhpvBals) {
				lhpvBal.setLbIsPaid(true);
				  session.update(lhpvBal); 
				  /* if ( ++count % 50 == 0 ) {
				      session.flush();
				      session.clear();
				   }*/
			}	
			 session.flush();
		      session.clear();
			tx.commit();
			i=1;
		}
		catch(Exception e)
		{
			tx.rollback();
			e.printStackTrace();
			i=0;
		}
		finally
		{
			session.close();
		}
		return i;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public LhpvStatus getLhpvStatusByDateBcode(String bCode,Date date,Session session){
		System.out.println("enter into getLastLhpvStatus function");
		List<LhpvStatus> lhstList = new ArrayList<LhpvStatus>();
			Query query = session
					.createQuery("from LhpvStatus where bCode= :bCode and lsBackDt= :lsBackDt and lsDt = :lsDt");
			query.setString("bCode", bCode);
			query.setBoolean("lsBackDt", false);
			query.setDate("lsDt", date);
			lhstList = query.setMaxResults(1).list();
			session.flush();
		if (lhstList.isEmpty()) {
			return null;
		} else {
			return lhstList.get(0);
		}
	}
	
	
	@Override
	public List<LhpvBal> setLhpvBal(LhpvStatus ls,List<LhpvBal> lbList) {
		System.out.println("enter into getLhpvBal funciton = " );
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
		LhpvStatus lhpvStatus=(LhpvStatus) session.get(LhpvStatus.class,ls.getLsId());
		
		for(int i=0;i<lbList.size();i++){
			if(lbList.get(i).getLhpvStatus()==null)
				lbList.get(i).setLhpvStatus(lhpvStatus);
		}
		
		lhpvStatus.setLbList(lbList);
		session.update(lhpvStatus);
		session.flush();
			session.clear();
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in adding lhpvbal"+e);
			transaction.rollback();
		}finally{
			session.close();
		}
			
			System.out.println("Size of lhpvBalList = " + lbList.size());
		return lbList;
	}
	
	
	@Override
	public List<LhpvAdv> setLhpvAdv(LhpvStatus ls,List<LhpvAdv> laList) {
		System.out.println("enter into setLhpvAdv funciton = " );
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
		LhpvStatus lhpvStatus=(LhpvStatus) session.get(LhpvStatus.class,ls.getLsId());
		
		for(int i=0;i<laList.size();i++){
			if(laList.get(i).getLhpvStatus()==null)
				laList.get(i).setLhpvStatus(lhpvStatus);
		}
		
		lhpvStatus.setLaList(laList);
		session.update(lhpvStatus);
		session.flush();
			session.clear();
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in adding lhpvbal"+e);
			transaction.rollback();
		}finally{
			session.close();
		}
			
			System.out.println("Size of lhpvBalList = " + laList.size());
		return laList;
	}

	@Override
	public int saveLhpvAdv(LhpvAdv la,Session session) {
		int laId=(int) session.save(la);
		return laId;
	}
	
	
	
}
