'use strict';

var app = angular.module('application');

app.controller('MrPrintCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	$scope.brhList = [];
	$scope.brhId = 0;
	$scope.selBrh = {};
	$scope.branch = "";
	$scope.mrList = [];
	
	$scope.openBrhFlag = true;
	$scope.printVsFlag = true;
	
	$scope.getMrBranch = function(){
		console.log("enter into getMrBranch function");
		var response = $http.post($scope.projectName+'/getMrBranch');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.brhList = data.list;
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.openBrhFlag = false;
		$('div#openBrhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.openBrhFlag = true;
		    }
			});
		
		$('div#openBrhId').dialog('open');	
	}
	
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branch = brh.branchName;
		$scope.brhId = brh.brhId;
		$scope.selBrh = brh;
		$('div#openBrhId').dialog('close');
	}
	
	
	$scope.mrPrintSubmit = function(mrPrintForm){
		console.log("enter into blPrintSubmit function = "+mrPrintForm.$invalid);
		if(mrPrintForm.$invalid){
			$scope.alertToast("please fill the correct form");
		}else{
			
				if(parseInt($scope.frMrNo.substring(2)) > parseInt($scope.toMrNo.substring(2))){
					$scope.alertToast("From-M.R. No must be less than To-M.R. No");
				}else{
					var req = {
							"brhId"  : $scope.brhId,
							"frMrNo" : $scope.frMrNo,
							"toMrNo" : $scope.toMrNo
					};
					
					var response = $http.post($scope.projectName+'/mrPrintSubmit',req);
					  response.success(function(data, status, headers, config){
						  if(data.result === "success"){
							 $scope.mrList = data.list;
							 
							 $scope.printVsFlag = false;
						    	$('div#printVsDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
								        $(this).dialog('destroy');
								        $(this).hide();
								        $scope.printVsFlag = true;
								    }
									});
								$('div#printVsDB').dialog('open');
						  }else{
							  $scope.alertToast(data.msg);
						  }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
					   });
				}
		}
	}
	
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
	}	
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getMrBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);