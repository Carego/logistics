package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.PenaltyBonusDetention;

@Repository
public interface PenaltyBonusDetentionDAO {
	
	//Sanjana: saves the penaltybonusdetention to database
	public int savePBD(PenaltyBonusDetention penaltyBonusDetention);
	
	//Sanjana : gets PBD from database on basis of entered contractCode 
	public List<PenaltyBonusDetention> getPenaltyBonusDetention(String code);

	//Sanjana :delete PBD from database
	public PenaltyBonusDetention deletePBD(String code);
}
