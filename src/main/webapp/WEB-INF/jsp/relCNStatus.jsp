<div ng-show="operatorLogin || superAdminLogin">
<title>Reliance CNStatus</title>
<div class="row">

<form ng-submit="relianceCnStatus(cnStatus)" class="col s12 card"
style="align: center; padding-top: 40px;
 background-color: rgba(125, 125, 125, 0.3);">
		<div class="row">
			<div class="input-field col s4">
				<input type="text" id="cnmtCode" name="cnmtCode" ng-model="cnStatus.cnmtCode" ng-keyup="OpenCnmtCodeDB($event.keyCode)"  required>
					<p style="color: red;font-size: 14px;font-style:italic;">{{message9}}</p>
				<label>CNMT Code</label>
			</div>
			<div class="input-field col s4">
				<input class="validate" type ="text" name ="fromStation" ng-model="cnStatus.fromStation" readOnly>
       		 	<label>From Station</label>
			</div>
			<div class="input-field col s4">
				<input type="text"  ng-model="cnStatus.toStation"
			 ng-required="true" readOnly> <label>To Station</label>
			</div>
			</div>
			<div class="row">
			<div class="input-field col s4">
				<input type="date"  ng-model="cnStatus.indentDate" ng-blur="checkingIndentDate(cnStatus)">
						<p style="color: red;font-size: 14px;font-style:italic;">{{message8}}</p>
				 <label>Indent Date</label>
			</div>
			<div class="input-field col s4">
				<input type="date"  ng-model="cnStatus.placementDate" ng-blur="compareIndentPlacementDate(cnStatus)">
							<p style="color: red;font-size: 14px;font-style:italic;">{{message}}</p>
				 <label>Placement Date</label>
			</div>
			<div class="input-field col s4">
				<input type="date"  ng-model="cnStatus.tripStartDate" ng-blur="comparePlacementTripStartDate(cnStatus)"> 
<p style="color: red;font-size: 14px;font-style:italic;">{{message2}}</p>
				<label>Trip Start Date</label>
			</div>
			</div>
			<div class="row">
				<div class="input-field col s3" >
				<input type="text"  ng-model="cnStatus.challanno" ng-click="challanData(cnStatus.cnmtCode)">
				<p style="color: red;font-size: 14px;font-style:italic;">{{message3}}</p>
				 <label>Challan No.</label>
			</div>
			<div class="input-field col s3" >
				<input type="text"  ng-model="cnStatus.lorryNo" readonly="readonly">		
				 <label>Lorry No</label>
			</div>
			<div class="input-field col s3" >
				<input type="text"  ng-model="cnStatus.carryType" readonly="readonly">
				<label>Lorry Type</label>
			</div>
				<div class="input-field col s3">
				<input type="text"  ng-model="cnStatus.trasittime" readonly="readonly">
				<label>Transit Time</label>
			</div>
		</div>
		
	 	<div class="row">
				<div class="input-field col s4">
				<select ng-model="cnStatus.currentStatus" ng-change="currentStatusValue(cnStatus.currentStatus);">
				<option value="Intransit">Intransit</option>
				<option value="Delivered">Delivered</option>
				</select>
				 <label>Current Status</label>
			</div>
			</div> 
				<div class="row" id="Ardiv" ng-hide="ArDBFlag">
				<div class="input-field col s4">
				<input type="text"  ng-model="cnStatus.arNo" ng-click="arrivalData(cnStatus.challanno)">
				<p style="color: red;font-size: 14px;font-style:italic;">{{message4}}</p>
				<p style="color: red;font-size: 14px;font-style:italic;">{{message6}}</p>
				<label>AR No.</label>
			</div>
			<div class="input-field col s4">
				<input class="validate" type ="date"  ng-model="cnStatus.reportingDate" >
       		 	<label>Reporting Date</label>
			</div>
			<div class="input-field col s4">
				<input type="date"  ng-model="cnStatus.unloadingDate"
			 > <label>Unloading Date</label>
			</div>
				</div>
				
				<div class="row">
				<div class="input-field col s4">
			    <textarea rows="10" cols="20" ng-model="cnStatus.deviation"></textarea>
				<label>Reason Deviations</label>
				</div>
				</div>
			
			<div class="row">
			<div class="input-field col s12 center">
				<input type="submit" value="Submit">
			</div>

		</div>

</form>
</div>

<div id="cnmtCodeDB" ng-hide="CnmtCodeDBFlag">

	<input type="text" name="filterCnmtCode"
		ng-model="filterCnmtCode.cnmtCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>Cnmt Code</th>
		</tr>
		<tr ng-repeat="cnmt in cnmtCodeList | filter:filterCnmtCode">
			<td><input type="radio" name="cnmtCode" id="cnmtId"
				value="{{ cnmt.cnmtCode }}" ng-model="cnmtCode"
				ng-click="saveCnmtCode(cnmt.cnmtCode,cnmt.cnmtToSt,cnmt.cnmtFromSt)"></td>
			<td>{{ cnmt.cnmtCode }}</td>
		</tr>
	</table>
</div>

<div id="challanCodeDB" ng-hide="ChallanCodeDBFlag">

	<input type="text" name="filterChallanCode"
		ng-model="filterChallanCode.challanCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>Challan Code</th>
		</tr>
		<tr ng-repeat="challan in challanList | filter:filterChallanCode">
			<td><input type="radio" name="challanCode" id="challanId"
				value="{{ challan.chlnCode }}" ng-model="chlnCode"
				ng-click="challanDataInField(challan)"></td> 
			<td>{{ challan.chlnCode }}</td>
		</tr>
	</table>
</div>

<div id="ArCodeDB" ng-hide="ArCodeDBFlag">

	<input type="text" name="filterArCode"
		ng-model="filterArCode.arCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>Arrival Code</th>
		</tr>
		<tr ng-repeat="ar in arList | filter:filterArCode">
			<td><input type="radio" 
				value="{{ ar.arCode }}" ng-model="arCode"
				ng-click="arDataInField(ar)"></td> 
			<td>{{ ar.arCode }}</td>
		</tr>
	</table>
</div>
</div>