'use strict';

var app = angular.module('application');

app.controller('BillTempCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	console.log("BillTempCntlr Started");
	
	/*$scope.btDetailList = [];
	$scope.btDetail = {};
	$scope.btMain = {};
	
	
	$scope.billTableFlag = false;
	
	$scope.billDetailDBFlag = true;
	
	
	$scope.addBill = function() {
		console.log("addBill()");
		$scope.billDetailDBFlag = false;
		$('div#billDetailDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bill Detail",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.billDetailDBFlag = true;
			}
		});

		$('div#billDetailDB').dialog('open');
	}
	
	$scope.weigth = function() {
		console.log("enter into: weigth()");
		if (parseFloat($scope.btDetail.btdWeightMt)>0 && parseFloat($scope.btDetail.btdRate)>0) {
			$scope.btDetail.btdFreight = parseFloat($scope.btDetail.btdWeightMt)*parseFloat($scope.btDetail.btdRate);
			$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight); + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);  
		}else {
			$scope.btDetail.btdFreight = "";
			$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight); + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);
		}
	}
	
	$scope.rate = function() {
		console.log("enter into: rate()");
		if (parseFloat($scope.btDetail.btdWeightMt)>0 && parseFloat($scope.btDetail.btdRate)>0) {
			$scope.btDetail.btdFreight = parseFloat($scope.btDetail.btdWeightMt)*parseFloat($scope.btDetail.btdRate);
			$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight) + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);
		} else {
			$scope.btDetail.btdFreight = "";
			$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight) + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);
		}
	}
	
	$scope.detention = function() {
		console.log("enter into: detention()");
		$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight) + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);
	}
	
	$scope.bonus = function() {
		console.log("enter into: bonus()");
		$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight) + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);
	}
	
	$scope.ldngNUnldng = function() {
		console.log("enter into: ldngNUnldng()");
		$scope.btDetail.btdTotal = parseFloat($scope.btDetail.btdFreight) + parseFloat($scope.btDetail.btdDetention) + parseFloat($scope.btDetail.btdBonus) + parseFloat($scope.btDetail.btdLdngNUnldng);
	}
	
	$scope.billDetailSubmit = function() {
		console.log("billDetailSubmit()");
		$scope.billTableFlag = true;
		$scope.btDetailList.push($scope.btDetail);
		$('div#billDetailDB').dialog('close');
		$scope.btDetail = {};
		$scope.btDetail.btdDetention=0;
		$scope.btDetail.btdBonus=0;
		$scope.btDetail.btdLdngNUnldng=0;
		$scope.btMain.btmSubTotal = 0;
		if ($scope.btDetailList.length>0) {
			for ( var i = 0; i < $scope.btDetailList.length; i++) {
				$scope.btMain.btmSubTotal = $scope.btMain.btmSubTotal + $scope.btDetailList[i].btdTotal;
			}
		} else {
			$scope.btMain.btmSubTotal = "";
		}
		
	}
	
	$scope.removebillTempDet = function(btDet, index){
		console.log("removebillTempDet: "+index);
		$scope.btDetailList.splice(index, 1);
		$scope.btMain.btmSubTotal = 0;
		if ($scope.btDetailList.length>0) {
			for ( var i = 0; i < $scope.btDetailList.length; i++) {
				$scope.btMain.btmSubTotal = $scope.btMain.btmSubTotal + $scope.btDetailList[i].btdTotal;
			}
		} else {
			$scope.btMain.btmSubTotal = 0;
		}
	}
	
	$scope.billMainSubmit = function() {
		console.log("enter into: billMainSubmit()");
		
		for ( var i = 0; i < $scope.btDetailList.length; i++) {
			console.log("detail total: "+$scope.btDetailList[i].btdTotal);
			
			$scope.btDetailList[i].btdTotal = parseFloat($scope.btDetailList[i].btdTotal);
		}
		
		var billTempService = {
			"btDetailList"		:	$scope.btDetailList,
			"btMain"			:	$scope.btMain
		}
		
		var response = $http.post($scope.projectName+'/saveBillTemp', billTempService);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log(data.result);
				$scope.alertToast("Success");
				//clean all data
				$scope.btDetailList = [];
				$scope.btDetail = {};
				$scope.btMain = {};
				
				//initialise intial values
				$scope.btDetail.btdDetention=0;
				$scope.btDetail.btdBonus=0;
				$scope.btDetail.btdLdngNUnldng=0;
				$scope.btMain.btmSubTotal = 0;
			}else {
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$('#btdCnmtNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#btdCnmtNoId').keypress(function(e) {
		if (this.value.length == 20) {
			e.preventDefault();
		}
	});
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getBankMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}*/
	
	console.log("BillTempCntlr Ended");
	
}]);