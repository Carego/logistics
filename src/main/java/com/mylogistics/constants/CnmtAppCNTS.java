package com.mylogistics.constants;

public class CnmtAppCNTS {

	public static final String CNMT_ID = "cnmtId";
	public static final String CNMT_CODE = "cnmtCode";
	public static final String CNMT_CUST_CODE = "cnmtCustCode";
	public static final String CNMT_VOG = "cnmtVOG";
	public static final String CNMT_DT = "cnmtDt";
	public static final String CNMT_FROM_ST = "cnmtFromSt";
	public static final String CNMT_TO_ST = "cnmtToSt";
	public static final String CNMT_CONSIGNOR = "cnmtConsignor";
	public static final String CNMT_CONSIGNEE = "cnmtConsignee";
	public static final String CNMT_VEHICLE_TYPE = "cnmtVehicleType";
	public static final String CNMT_PRODUCT_TYPE = "cnmtProductType";
	public static final String CNMT_ACTUAL_WT = "cnmtActualWt";
	public static final String CNMT_GUARANTEE_WT = "cnmtGuaranteeWt";
	public static final String CNMT_KM = "cnmtKm";
	public static final String CNMT_STATE = "cnmtState";
	public static final String CNMT_PAY_AT = "cnmtPayAt";
	public static final String CNMT_BILL_AT = "cnmtBillAt";
	public static final String CNMT_RATE = "cnmtRate";
	public static final String CNMT_NO_OF_PKG = "cnmtNoOfPkg";
	public static final String CNMT_FREIGHT = "cnmtFreight";
	public static final String CNMT_TOT = "cnmtTOT";
	public static final String CNMT_EMP_CODE = "cnmtEmpCode";
	public static final String CNMT_EXTRA_EXP = "cnmtExtraExp";
	public static final String CNMT_DT_OF_DLY = "cnmtDtOfDly";
	public static final String CNMT_CONT_CODE = "cnmtContCode";
	public static final String CNMT_DTD_DLY = "cnmtDtdDly";
	public static final String CNMT_DC = "cnmtDc";
	public static final String CNMT_COST_GRADE = "cnmtCostGrade";
	public static final String CNMT_INVOICE_NO = "cnmtInvoiceNo";
	public static final String CNMT_INVOICE_DT = "cnmtInvoiceDt";
	public static final String CNMT_USER_CODE = "cnmtUserCode";
	public static final String CNMT_B_CODE = "cnmtBCode";
	public static final String CNMT_BRANCH_CODE = "cnmtBranchCode";
	public static final String CNMT_IS_PENDING = "cnmtIsPending";
	public static final String CNMT_IS_CANCEL = "cnmtIsCancel";
	public static final String CNMT_CREATION_TIME = "creationTS";
	
}