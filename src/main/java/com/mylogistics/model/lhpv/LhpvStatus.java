package com.mylogistics.model.lhpv;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mylogistics.model.Challan;

@Entity
@Table(name = "lhpvstatus")
public class LhpvStatus {

	@Id
	@Column(name="lsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int lsId;

	/*@JsonIgnore
	@JsonManagedReference(value = "lschln")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvstatus_challan",
	joinColumns=@JoinColumn(name="lsId"),
	inverseJoinColumns=@JoinColumn(name="chlnId"))
	private List<Challan> chlnList = new ArrayList<Challan>();*/
	
	@JsonIgnore
	@JsonManagedReference(value = "lsla")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvstatus_lhpvadv",
	joinColumns=@JoinColumn(name="lsId"),
	inverseJoinColumns=@JoinColumn(name="laId"))
	private List<LhpvAdv> laList = new ArrayList<LhpvAdv>();
	
	@JsonIgnore
	@JsonManagedReference(value = "lslb")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvstatus_lhpvbal",
	joinColumns=@JoinColumn(name="lsId"),
	inverseJoinColumns=@JoinColumn(name="lbId"))
	private List<LhpvBal> lbList = new ArrayList<LhpvBal>();
	
	@JsonIgnore
	@JsonManagedReference(value = "lslsp")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvstatus_lhpvsup",
	joinColumns=@JoinColumn(name="lsId"),
	inverseJoinColumns=@JoinColumn(name="lspId"))
	private List<LhpvSup> lspList = new ArrayList<LhpvSup>();

	@Column(name="lsDt")
	private Date lsDt;
	
	@Column(name="lsNo")
	private int lsNo;
	
	@Column(name="lsCrAmt")
	private double lsCrAmt;
	
	@Column(name="lsDbAmt")
	private double lsDbAmt;
	
	@Column(name="lsClose" , columnDefinition="boolean default false")
	private boolean lsClose;
	
	@Column(name="lsBackDt" , columnDefinition="boolean default false")
	private boolean lsBackDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	public int getLsId() {
		return lsId;
	}

	public void setLsId(int lsId) {
		this.lsId = lsId;
	}

	public List<LhpvAdv> getLaList() {
		return laList;
	}

	public void setLaList(List<LhpvAdv> laList) {
		this.laList = laList;
	}
	
	/*public List<Challan> getChlnList() {
		return chlnList;
	}

	public void setChlnList(List<Challan> chlnList) {
		this.chlnList = chlnList;
	}*/

	public Date getLsDt() {
		return lsDt;
	}

	public void setLsDt(Date lsDt) {
		this.lsDt = lsDt;
	}

	public int getLsNo() {
		return lsNo;
	}

	public void setLsNo(int lsNo) {
		this.lsNo = lsNo;
	}

	public double getLsCrAmt() {
		return lsCrAmt;
	}

	public void setLsCrAmt(double lsCrAmt) {
		this.lsCrAmt = lsCrAmt;
	}

	public double getLsDbAmt() {
		return lsDbAmt;
	}

	public void setLsDbAmt(double lsDbAmt) {
		this.lsDbAmt = lsDbAmt;
	}

	public boolean isLsClose() {
		return lsClose;
	}

	public void setLsClose(boolean lsClose) {
		this.lsClose = lsClose;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public List<LhpvBal> getLbList() {
		return lbList;
	}

	public void setLbList(List<LhpvBal> lbList) {
		this.lbList = lbList;
	}

	public List<LhpvSup> getLspList() {
		return lspList;
	}

	public void setLspList(List<LhpvSup> lspList) {
		this.lspList = lspList;
	}

	public boolean isLsBackDt() {
		return lsBackDt;
	}

	public void setLsBackDt(boolean lsBackDt) {
		this.lsBackDt = lsBackDt;
	}
	
	
}
