package com.mylogistics.services;

import java.sql.Date;

import org.springframework.stereotype.Service;

@Service
public class PanService {

	private String panName;
	
	private String panNo;
	
	private Date panDOB;
	
	private Date panDt;
	
	private int ownId;
	
	private int brkId;

	public String getPanName() {
		return panName;
	}

	public void setPanName(String panName) {
		this.panName = panName;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public Date getPanDOB() {
		return panDOB;
	}

	public void setPanDOB(Date panDOB) {
		this.panDOB = panDOB;
	}

	public Date getPanDt() {
		return panDt;
	}

	public void setPanDt(Date panDt) {
		this.panDt = panDt;
	}

	public int getOwnId() {
		return ownId;
	}

	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}

	public int getBrkId() {
		return brkId;
	}

	public void setBrkId(int brkId) {
		this.brkId = brkId;
	}

	
}
