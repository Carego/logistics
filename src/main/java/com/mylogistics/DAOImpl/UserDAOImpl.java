package com.mylogistics.DAOImpl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.UserDAO;
import com.mylogistics.constants.BranchMUStatsCNTS;
import com.mylogistics.constants.UserCNTS;
import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.User;

public class UserDAOImpl implements UserDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(UserDAOImpl.class);
	
	@Autowired
	public UserDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/*
	 This function is used to save a User Object in User table
	*/
	 @Transactional
	 public void saveUser(User user){
		 System.out.println("****enter into saveUser---->"+user.getUserCode());
		
		try{
			 session = this.sessionFactory.openSession();//fixed
			 transaction = session.beginTransaction();//fixed
			 session.save(user);
			 transaction.commit();//fixed
			 System.out.println("user data inserted successfully");
			 session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("exit from saveUser");
		session.clear();
		session.close();
	 }
	 
	 /*
	 This function is used to retrieve a User Object from User table
	*/
	@SuppressWarnings("unchecked")
	@Transactional
	 public List<User> retrieveUser(String userCode){
		 System.out.println("****enter into retrieveUser---->");
		 List<User> results = new ArrayList<User>();
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(User.class);
			 cr.add(Restrictions.eq(UserCNTS.USER_CODE, userCode));
			 results  = cr.list();
			 session.flush();
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 System.out.println("****exit from retrieveUser---->");
		 session.clear();
		 session.close();
		return results; 
		
	 }
	
	@SuppressWarnings("unchecked")
	@Transactional
	 public List<User> getUserByUN(String userName , String password){
		 System.out.println("****enter into getUserByUN---->");
		 List<User> results = new ArrayList<User>();
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(User.class);
			 cr.add(Restrictions.eq(UserCNTS.USER_NAME, userName));
			 results  = cr.list();			 
			 //SQLQuery query = session.createSQLQuery(userName);
			 //query.executeUpdate();
			 session.flush();
			 session.clear();
			 session.close();
		 }		
		 catch(Exception e){
			 System.out.println("Error : "+e);
		 }
		 System.out.println("****exit from retrieveUser---->");
		 
		return results; 
		
	 }
	
	/*
	 This function is used to Update userCode of a User Object
	*/
	/* @SuppressWarnings("unchecked")
	 @Transactional
	 public User updateUserByUserCode(String userCode , String userName){
		 System.out.println("****enter into updateUserByUserCode---->");
		 try{
			 session = this.sessionFactory.openSession();//fixed
			 transaction = session.beginTransaction();//fixed
			 Criteria cr=session.createCriteria(User.class);//fixed
			 cr.add(Restrictions.eq("userName", userName));//fixed
			 
			 user =(User) cr.list().get(0);
			 user.setUserCode(userCode);
			 session.saveOrUpdate(user);
			 
			 transaction.commit();//fixed
			 session.flush();
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 System.out.println("****exit from updateUserByUserCode---->");
		 session.close();
		 return user;
	 }*/
	 
	 
	 /*
	 This function is used to add a new user by superAdmin 
	 */
	 @Transactional
	 public String addNewUser(User user){
		 System.out.println("enter into addNewUser of UserDAOImpl");
		 String result = "";
		 boolean flag = true;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(User.class);
			 List<User> userList = cr.list();
			 if(!userList.isEmpty()){
				 for(int i=0;i<userList.size();i++){
					 if(userList.get(i).getUserName().equalsIgnoreCase(user.getUserName())){
						 result = "USERNAME ALREADY EXIST";
						 flag = false;
						 break;
					 }
				 }
				 
				 if(flag == true){
					 session.save(user);
					 transaction.commit();
					 result = "success";
				 }
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 result = "failure";
		 }
		 session.clear();
		 session.close();
		return result;
	 }
	 
	 /*
	 This function is used to get all the User Objects from user table
	*/
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<User> getAllUser(){
		 System.out.println("enter into getAllUser function");
		 List<User> userList = new ArrayList<User>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 userList = session.createCriteria(User.class).list();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		}
		 session.clear();
		 session.close();
		 return userList;
	 }
	 
	 /*
	 This function is used to Update a list of User Object
	*/
	 @Transactional
	 public String updateUserList(List<User> userList){
		 System.out.println("enter into updateUserList function");
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 if (!userList.isEmpty()) {
				 for(int i=0;i<userList.size();i++){
					 User user = userList.get(i);
					 session.update(user);
				 }
			 }
			 //userList = session.createCriteria(User.class).list();
			 transaction.commit();
			 session.flush();
			 
		 }catch(Exception e){
			 e.printStackTrace();
			 return "failure";
		 }
		 session.clear();
		 session.close();
		 return "success";
	 }
	 
	 /*
	 This function is used to Update a User Object
	*/
	 @Transactional
	 public String updateUser(User user){
		 System.out.println("enter into updateUser function");
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.update(user);
			 transaction.commit();
			 session.flush();
			
		 }catch(Exception e){
			 e.printStackTrace();
			 return "failure";
		 }
		 session.clear();
		 session.close();
		 return "success";
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<User> getHBOperator(String branchCode){
		 System.out.println("enter into getHBOperator function");
		 List<User> userList = new ArrayList<User>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(User.class);
			 cr.add(Restrictions.eq(UserCNTS.USER_BRANCH_CODE,branchCode));
			 cr.add(Restrictions.eq(UserCNTS.USER_ROLE,33));
			 userList = cr.list();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 
		 return userList;
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<User> getSubBranchOperator(String branchCode){
		 logger.info("Enter into getSubBranchOperator() : BranchCode = "+branchCode);		 
		 List<User> userList = new ArrayList<User>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(User.class);
			 cr.add(Restrictions.eq(UserCNTS.USER_BRANCH_CODE,branchCode));			 
			 cr.add(Restrictions.eq(UserCNTS.USER_ROLE,33));
			 cr.addOrder(Order.asc(UserCNTS.USER_BRANCH_CODE));
			 userList = cr.list();
			 if(userList.isEmpty())
				 logger.info("Users not found !");
			 else
				 logger.info("Users found ! Size = "+userList.size());
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 logger.error("Exception : "+e);
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 logger.info("Exit from getSubBranchOperator()");
		 return userList;
	 }
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public int updateUser(String employeeCode,String branchCode){
		 
		  System.out.println("Employee code to be updated in userDAOImpl---"+employeeCode);
		  int tmp,i;
		  Date date = new Date();
		  Calendar calendar = Calendar.getInstance();
		  calendar.setTime(date);
	      
	      List<User> list = new ArrayList<User>();
	      try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(User.class);
			  cr.add(Restrictions.eq(UserCNTS.USER_CODE,employeeCode));
			  list =cr.list();
			  System.out.println("Size of list in updateUser---->"+list.size());
			  for(i=0;i<list.size();i++){
				  User user = list.get(i);
				  user.setUserBranchCode(branchCode);
				  //user.setUserCodeTemp(empCodeTemp);
				  user.setCreationTS(calendar);
				  session.update(user); 
			  }
			  transaction.commit();
			  session.flush();
			  tmp=1;
		  }
		  catch (Exception e) {
			  e.printStackTrace();
			  tmp=-1;
		  }
	      session.clear();
		  session.close();
		  return tmp;
		 
	 }
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public int setUserCode(int userId, int empId){
		 System.out.println("enter into setUserCode funciton");
		 int temp = 0; 
		  try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(User.class);
			  cr.add(Restrictions.eq(UserCNTS.USER_ID,userId));
			  List<User> userList = cr.list();
			  if(!userList.isEmpty()){
				  User user = userList.get(0);
				  user.setUserCode(String.valueOf(empId));
				  session.update(user);
				  transaction.commit();
				  temp = 1;
			  }else{
				  temp = -1;
			  }
			  session.flush();
		  }
		  catch (Exception e) {
			  e.printStackTrace();
			  temp=-1;
		  }
		  session.clear();
		  session.close();
		  return temp;
	 }

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public int changePass(User user) {
		int temp;
		 try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(User.class);
			  cr.add(Restrictions.eq(UserCNTS.USER_ID,user.getUserId()));
			  List<User> userList = cr.list();
			  if(!userList.isEmpty()){
				  User existingUsers = userList.get(0);
				  existingUsers.setPassword(user.getPassword());
				  session.merge(existingUsers);
				  transaction.commit();
				  temp = 1;
			  }else{
				  temp = -1;
			  }
			  session.flush();
		  }
		  catch (Exception e) {
			  e.printStackTrace();
			  temp=-1;
		  }
		  session.clear();
		  session.close();
		  return temp;
		
	}
	 
	 
}
