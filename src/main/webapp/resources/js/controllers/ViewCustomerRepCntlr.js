'use strict';

var app = angular.module('application');

app.controller('ViewCustomerRepCntlr',['$scope','$location','$http','$filter',
                                       function($scope,$location,$http,$filter){

	$scope.CustomerRepCodeDBFlag=true;
	$scope.show = "true";

	$scope.OpenCustomerRepCodeDB = function(){
		$scope.CustomerRepCodeDBFlag=false;
		$('div#customerRepCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Customer Representative Codes",
			show: UDShow,
			hide: UDHide,
			position: UDPos,			
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerRepCodeDB').dialog('open');
	}

	$scope.saveCustomerRepCode = function(crep){
		$scope.crCode = crep.crCode ;
		$scope.crCodeTemp = crep.crFaCode;
		$('div#customerRepCodeDB').dialog('close');
		$scope.CustomerRepCodeDBFlag=true;
	}

	$scope.getCustomerRepCodeList = function(){
		console.log("getCustomerRepData------>");
		var response = $http.post($scope.projectName+'/getCustomerRepCodeList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custRepCodeList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}

	$scope.SubmitCR = function(crCode){
		console.log("enter into submit function--->");
		$scope.code=$('#custrepCode').val();
		if($scope.code === ""){
			$scope.alertToast("Please enter customer rep code....");
		}else{
			$scope.show = "false";
			var response = $http.post($scope.projectName+'/submitcustomerrepresentative', crCode);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.customer= data.customer;	
					$scope.customerRepresentative = data.customerRepresentative;
					$scope.customer.creationTS =  $filter('date')($scope.customer.creationTS, 'MM/dd/yyyy hh:mm:ss');
					$scope.customerRepresentative.creationTS =  $filter('date')($scope.customerRepresentative.creationTS, 'MM/dd/yyyy hh:mm:ss');
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}

			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	}

	$scope.getCustomerRepCodeList();

}]);