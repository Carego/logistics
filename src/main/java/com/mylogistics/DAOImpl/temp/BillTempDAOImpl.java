package com.mylogistics.DAOImpl.temp;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.temp.BillTempDAO;
import com.mylogistics.constants.AddressCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.User;
import com.mylogistics.model.temp.BillTempDetail;
import com.mylogistics.model.temp.BillTempMain;
import com.mylogistics.services.temp.BillTempService;

public class BillTempDAOImpl implements BillTempDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;

	@Autowired
	public BillTempDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public int saveBillTemp(BillTempService billTempService) {
		
		int temp = 0;
		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			List<BillTempDetail> billTempDetailList = billTempService.getBtDetailList();
			BillTempMain billTempMain = billTempService.getBtMain();
			
			for (BillTempDetail billTempDetail : billTempDetailList) {
				billTempDetail.setbCode(currentUser.getUserBranchCode());
				billTempDetail.setUserCode(currentUser.getUserCode());
				billTempDetail.setBillTempMain(billTempMain);
				billTempMain.getBillTempDetailList().add(billTempDetail);
			}
			
			billTempMain.setbCode(currentUser.getUserBranchCode());
			billTempMain.setUserCode(currentUser.getUserCode());
			
			session.save(billTempMain);
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}
	

	/*@Transactional
	@SuppressWarnings("unchecked")
	public List<Address> getAddress(String brokerCode, String type) {
		System.out.println("Enter into get Address function--" + type);
		List<Address> addresses = new ArrayList<Address>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(Address.class);
			cr.add(Restrictions.eq(AddressCNTS.TYPE, type));
			cr.add(Restrictions.eq(AddressCNTS.REF_CODE, brokerCode));
			addresses = cr.list();
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return addresses;
	}*/

}
