<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="trialForm" ng-submit=trialSubmit(trialForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
	     		<div class="col s3 input-field">
	       				<input class="validate" type ="text" id="finYrId" name ="finYrName" ng-model="finYr" ng-required="true" readonly="readonly" ng-click="openFinYrDB()">
	       		<label for="code">Financial Year</label>	
	       		</div>
	       		
	       		<div class="col s3 input-field">
	       				<input class="validate" type ="date" id="uptoDtId" name ="uptoDtName" ng-model="uptoDt" ng-required="true">
	       			<label for="code">Upto Date</label>	
	       		</div>
	       		
	       		<div class="col s3 input-field">
	       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" disabled="disabled" readonly="readonly">
	       			<label for="code">Branch</label>	
	       		</div>
	       		
	       		<div class="col s2 input-field">
					<input class="validate" type="radio" id="consolidateRBId" name="isRBName" ng-click="consolidateRB()" > 
					<label for="code">Consolidate</label>
				</div>
				
				<div class="col s1 input-field">
					<input class="validate" type="radio" id="branchRBId" name="isRBName" ng-click="branchRB()"> 
					<label for="code">Branch</label>
				</div>
				
				
	       	</div>
		    
		    <div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" id="trialSubmitId" value="Submit">
      			 		<input type="button" id="printTrialId" value="Print Pdf" ng-click="printTrialPdf()" ng-show="showTableFlag" disabled="disabled">
      			 		<input type="button" id="printTrialXlsId" value="Print Xls" ng-click="printTrialXls()" ng-show="showTableFlag" disabled="disabled">
      			 	</div>
      		</div>
	       	 
		  </form>
    </div>
    
    <div id ="finYrDB" ng-hide="finYrDBFlag" >
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Financial Year </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="finYr in finYearList | filter:filterTextbox">
		 	  <td><input type="radio"  name="finYr" value="{{ finYr }}" ng-click="saveFinYr(finYr)"></td>
              <td>{{finYr}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<!-- print trial report -->
	<div class="row" ng-if="showTableFlag" id="exportable">
		<div class="container" style="width: 100% !important;">
			<table class="tblrow" style="width: 100% !important;">
	       		
	       		<!--
	       		if ((boolean) trialRepMap.get("isBranch")) {
					subChunk = new Chunk("("+String.valueOf(trialRepMap.get("branchName"))+")");
					
				} else {
					subChunk = new Chunk("(Consolidated)");
				}
	       		
	       		  -->
	       		
	       		<tr>
	       			<th class="coltag tblrow" colspan="4" ng-if="isBranch">Trial Report: ({{branch.branchName}})</th>
	       			<th class="coltag tblrow" colspan="4" ng-if="isConsolidate">Trial Report: Consolidated</th>
	       		</tr>
	       		
	       		<tr>
	       			<th class="tblrow" colspan="2">Financial Year: {{finYr}}</th>
	       			<th class="tblrow" colspan="2">Upto Date: {{uptoDt| date:'dd-MM-yyyy'}}</th>
	       		</tr>
	       		
	       		<tr class="rowclr">
		            <th class="colclr">A/C Code</th>
		            <th class="colclr" style="width: 100px">A/C Head</th>
		            <th class="colclr">Debit</th>
		            <th class="colclr">Credit</th>
		        </tr>
	       		
	       		<tr ng-repeat="faCodeName in faCodeNameList" ng-if="cashStmt.csFaCode == faCode.csFaCode">
		            <td class="rowcel">{{faCodeName.csFaCode}}</td>
		            <td class="rowcel">{{faCodeName.csFaName}}</td>
		            <td class="rowcel" ng-if="faCodeName.csDrCrBalance === 'D'">{{faCodeName.csAmtBalance | number : 2}}</td>
		            <td class="rowcel" ng-if="faCodeName.csDrCrBalance === 'D'"></td>
		            <td class="rowcel" ng-if="faCodeName.csDrCrBalance === 'C'"></td>
		            <td class="rowcel" ng-if="faCodeName.csDrCrBalance === 'C'">{{faCodeName.csAmtBalance | number : 2}}</td>
		        </tr>
		        
		        <tr>
       				<td class="rowcel" colspan="2">CLOSING BALANCE (cash in branch)</td>
       				<td class="rowcel">{{cashInClosing | number : 2}}</td>
		            <td class="rowcel"></td>
		        </tr>
		        
		        <tr>
       				<td class="colclr" colspan="2">Total</td>
       				<td class="colclr">{{totalDr | number : 2}}</td>
		            <td class="colclr">{{totalCr | number : 2}}</td>
		        </tr>
	       	    
	       	</table>
	    </div>
	          
	</div>
	
</div>