package com.mylogistics.services;

import com.mylogistics.model.Stationary;

public class StationaryRecService {
	
	private Stationary stationary;
	private String cnmtSeq;
	private String chlnSeq;
	private String sedrSeq;
	
	public void remove(){
		this.stationary = null;
		this.chlnSeq = "";
		this.cnmtSeq = "";
		this.sedrSeq = "";
	}
	
	public Stationary getStationary() {
		return stationary;
	}
	public void setStationary(Stationary stationary) {
		this.stationary = stationary;
	}
	public String getCnmtSeq() {
		return cnmtSeq;
	}
	public void setCnmtSeq(String cnmtSeq) {
		this.cnmtSeq = cnmtSeq;
	}
	public String getChlnSeq() {
		return chlnSeq;
	}
	public void setChlnSeq(String chlnSeq) {
		this.chlnSeq = chlnSeq;
	}
	public String getSedrSeq() {
		return sedrSeq;
	}
	public void setSedrSeq(String sedrSeq) {
		this.sedrSeq = sedrSeq;
	}
	
	
}
