package com.mylogistics.constants;

public class StationaryCNTS {

	public static final String ST_ID = "stId";
	
	public static final String ST_IS_REC = "stIsRec";
	
	public static final String ST_CODE = "stCode";
	
	public static final String ST_IS_CRT = "stIsCrt";
	
	public static final String ST_ODR_DT = "stOdrDt";
	
	public static final String ST_REC_DT = "stRecDt";
	
	public static final String ST_ODR_CNMT = "stOdrCnmt";
	
	public static final String ST_ODR_CHLN = "stOdrChln";
	
	public static final String ST_ODR_SEDR = "stOdrSedr";
	
	public static final String ST_REC_CNMT = "stRecCnmt";
	
	public static final String ST_REC_CHLN = "stRecChln";
	
	public static final String ST_REC_SEDR = "stRecSedr";
	
	public static final String ST_SUP_INO = "stSupInvcNo";
	
	public static final String ST_SUP_CODE = "stSupCode";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
