<div ng-show="operatorLogin || superAdminLogin">
<title>View Broker</title>
<div class="row">
	<div class="col s1 hidden-xs hidden-sm"> &nbsp; </div>
		<form class="col s10 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="brokerView(brkCode)">
	<div id ="empCode">
			<div class="input-field col s6">
   				<input type="text" id="brokercode" name="brkCode" ng-model="brkCode" ng-click="openBrokerCodeDB()" readonly/>
   				<label>Enter Broker Code</label>
 			</div>
			
      			 	<div class="input-field col s6 center">
      			 		<input type="submit" value="Submit"/>
      			 	</div>
	</div>
		</form>
		<div class="col s3"> &nbsp; </div>
</div>	

		<div class="row" ng-hide = "brokerDetails" >
	<div class="col s1 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s10 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >			
		<form name="ViewBrokerForm" ng-submit="EditBrokerSubmit(ViewBrokerForm)">
		<h5>Here's the detail of <span class="teal-text text-lighten-2">{{broker.brkName}}</span>:</h5>
		<table class="table-bordered table-condensed">
				<tr>
	                <td>Broker Code:</td>
	                <td>{{broker.brkCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>Name:</td>
	                <td>{{ broker.brkName }}<input type ="text" name ="brkName" ng-model="broker.brkName" value="{{ broker.brkName }}" ></td>
	            </tr>
	           
	            <tr>
	                <td>PAN Name:</td>
	                <td>{{ broker.brkPanName }}<input type ="text" id="brkPanName" name ="brkPanName" ng-model="broker.brkPanName" value="{{ broker.brkPanName }}"></td>
	            </tr>
	            
	             <tr>
	                <td>PAN No.:</td>
	                <td>{{ broker.brkPanNo }}<input type ="text" id="brkPanNo" name ="brkPanNo" ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/" ng-model="broker.brkPanNo" value="{{ broker.brkPanNo }}"></td>
	            </tr>
	            
	             <tr>
	                <td>Firm Name:</td>
	                <td>{{ broker.brkFirmName }}<input type ="text" id="brkFirmName" name ="brkFirmName" ng-model="broker.brkFirmName" value="{{ broker.brkFirmName }}"></td>
	            </tr>
	            
	           
	            <tr>
	                <td>Firm Type:</td>
	                <td>{{broker.brkFirmType}}<select name="brkFirmType" id="brkFirmType" ng-model="broker.brkFirmType" ng-init="broker.brkFirmType">
                    <option value="Proprietor">Proprietor</option>
                    <option value="Partner">Partner</option>
                    <option value="Pvt Ltd">Pvt Ltd</option>
                    <option value="Ltd">Limited</option>
                </select></td>
	            </tr>
	            
	             <tr>
	                <td>Phone No.:</td>
	                <td>{{ broker.brkPhNoList }}<input type ="text" name ="brkPhNoList" ng-model="broker.brkPhNoList[0]"></td>
	            </tr>
	            
	            <tr>
	                <td>Email Id:</td>
	                <td>{{ broker.brkEmailId }}<input type ="text" name ="brkEmailId" ng-model="broker.brkEmailId" value="{{ broker.brkEmailId }}"></td>
	            </tr>
	            
	            
	          <td><h5>Bank Detail</h5></td>
					<tr>
						<td>Account Holder Name:</td>
						<td>{{ broker.brkAcntHldrName}}</td>
					</tr>

					<tr>
						<td>Bank Name:</td>
						<td>{{ broker.brkBnkBranch}}</td>
					</tr>
					<tr>
						<td>IFSC Code:</td>
						<td>{{ broker.brkIfsc}}</td>
					</tr>
					<tr>
						<td>Account Number:</td>
						<td>{{ broker.brkAccntNo}}</td>
					</tr>


	          
	          <table class="table-hover table-bordered table-condensed">
					<td><h5>View Image</h5></td>
					<tr>
						<td><input type="button" value="PAN Image"
							ng-click="showPanImage(broker)"></td>
						<td><input type="button" value="DEC Image"
							ng-click="showDecImage(broker)"></td>
						<td><input type="button" value="BANK Image"
							ng-click="showBankImage(broker)"></td>
					</tr>
				</table>

				<table class="table-hover table-bordered table-condensed" ng-if="currentBranch=='Gurgaon (H.O)'">
					<td><h5>Upload Image</h5></td>
					<tr>
						<td>

							<form class="col s12 card"
								style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
								name="brkPANUpldForm">

								<div class="input-field col s9">
									<input type="file" class="teal white-text btn" name="brkPanImageName"
										file-model="brkPanImage"> <label>Choose PAN
										File for Upload</label>
								</div>
								<div class="col s3">
									<a class="btn waves-effect waves-light white-text col s12"
										type="button" ng-click="uploadBrkPanImage(brkPanImage)">Upload
										PAN</a>
								</div>

							</form>
						</td>
					</tr>

					<tr>
						<td>

							<form class="col s12 card"
								style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
								name="brkDecUpldForm">

								<div class="input-field col s9">
									<input type="file" class="teal white-text btn" name="brkDecImageName"
										file-model="brkDecImage"> <label>Choose DEC
										File for Upload</label>
								</div>
								<div class="col s3">
									<a class="btn waves-effect waves-light white-text col s12"
										type="button" ng-click="uploadBrkDecImage(brkDecImage)">Upload Dec.</a>
								</div>

							</form>
						</td>
					</tr>
				</table>
	          
	          
	          
	          
	          <table  class="table-bordered table-condensed">
	          
	          <tr>
	          		<td><h5>Current Address</h5></td>
	          </tr>
	          
				
				 <tr>
						<td>Address</td>
						<td>{{ add.completeAdd}}<input id="completeAddId" type ="text" name ="completeAddName"  ng-model="add.completeAdd"  ></td>
					</tr>

					<tr>
						<td>Pin</td>
						<td>{{add.addPin}}<input id="addPinId" type ="text" name ="addPinName"  ng-model="add.addPin" ng-keyup="getStnByPin()" minlength="6" maxlength="6"  >
        			</td>
					</tr> 
					<tr>
						<td>State</td>
						<td>{{add.addState}}<input id="addStateId" type ="text" name ="addStateName"  ng-model="add.addState" ng-click="getState()" readonly >
        			</td>
					</tr>
					
					<tr>
						<td>District</td>
						<td>{{add.addDist}}<input id="districtId" type ="text" name ="districtName"  ng-model="add.addDist" ng-click="getState()" readonly  >
        			</td>
					</tr>
					
					<tr>
						<td>City</td>
						<td>{{add.addCity}}<input id="addCityId" type ="text" name ="addCityName"  ng-model="add.addCity" ng-click="getState()" readonly  >
        			</td>
					</tr>
					
					<tr>
						<td>Location</td>
						<td>{{add.addPost}}<input id="locationId" type ="text" name ="locationName"  ng-model="add.addPost" ng-click="getState()" readonly  >
        			</td>
					</tr>
				
				
	          </table>
	           <input type="submit" value="Submit"  ng-if="currentBranch=='Gurgaon (H.O)'">
	          </form>
	
		<div id ="brokerCodesDB" ng-hide = "brokerCodesDBFlag">
		
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Broker Code">
 	   	 <table>
 	   	 <tr>
 	   	 	<th></th>
			<th>Code</th>
			<th>Name</th>
			<th>FaCode</th> 	   	 
 	   	 </tr>
		 <tr ng-repeat="codes in codeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="brokerCode" value="{{ codes }}" ng-model="Code" ng-click="saveCode(codes)"></td>
              <td>{{ codes.code }}</td>
              <td>{{ codes.name }}</td>
              <td>{{ codes.faCode }}</td>
          </tr>
         </table>       
	</div>
</body>
</div>
<div id="viewBrokerDB" ng-show="viewBrokerFlag"> 

	<table class="table-hover table-bordered table-bordered">
		<tr>
			<td>Broker Code:</td>
			<td>{{broker.brkCode}}</td>
		</tr>
		
				
		<tr>
			<td>Name</td>
			<td>{{broker.brkName}}</td>
		</tr>
		
		<tr>
			<td>Pan Name</td>
			<td>{{broker.brkPanName}}</td>
		</tr>
		
		<tr>
			<td>Pan No.</td>
			<td>{{broker.brkPanNo}}</td>
		</tr>
		
		<tr>
			<td>Phone No.</td>
			<td>{{broker.brkPhNoList}}</td>
		</tr>
		
		<tr>
			<td>Email-ID</td>
			<td>{{broker.brkEmailId}}</td>
		</tr>
		
		<tr>
			<td>Firm Type</td>
			<td>{{broker.brkFirmType}}</td>
		</tr>
		
	</table>
	
	
		<table  class="table-hover table-bordered table-bordered">
		<h4>Current Address</h4>
		<tr>
			<td>Address</td>
			<td>{{add.completeAdd}}</td>
		</tr>
		
		<tr>
			<td>City</td>
			<td>{{add.addCity}}</td>
		</tr>
		
		<tr>
			<td>State</td>
			<td>{{add.addState}}</td>
		</tr>
		
		<tr>
			<td>Pin</td>
			<td>{{add.addPin}}</td>
		</tr>
		</table>
		
					
			<input type="button" value="Cancel" ng-click="back()">
			<input type="button" value="Save" ng-click="saveBroker()">
</div>


<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnName">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.pinCode }}</td>
				<td>{{ stn.stationName }}</td>
				<td>{{ stn.city }}</td>
				<td>{{ stn.district }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
