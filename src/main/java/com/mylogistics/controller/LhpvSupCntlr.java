package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Challan;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class LhpvSupCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private ArrivalReportDAO arrivalReportDAO;
	
	
	@RequestMapping(value = "/getLhpvSupDet" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvSupDet() {  
		System.out.println("Enter into getLhpvBalDet---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<Map<String,Object>> bnkList = new ArrayList<>();
				bnkList = bankMstrDAO.getBnkNameAndCode(branchCode);
				LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatus(branchCode);
				if(lhpvStatus != null){
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put("lhpvStatus",lhpvStatus);
					map.put("bnkList",bnkList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	
	@RequestMapping(value = "/getChlnAndBrOwnLSP" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnAndBrOwnLSP() {  
		System.out.println("Enter into getChlnAndBrOwnLSP---->");
		Map<String, Object> map = new HashMap<>();	
		
		List<String> chlnList = challanDAO.getChlnCodeFrLHSP();
		
		if(!chlnList.isEmpty()){
			for(int i=0;i<chlnList.size();i++){
				for(int j=i+1;j<chlnList.size();j++){
					if(chlnList.get(i).equalsIgnoreCase(chlnList.get(j))){
						chlnList.remove(j);
						j--;
					}
				}
			}
		}
		
		List<Map<String,String>> brkList = brokerDAO.getBrkNCF();
		List<Map<String,String>> ownList = ownerDAO.getOwnNCF();
		
		List<Map<String,String>> actBOList = new ArrayList<>();
		
		if(!brkList.isEmpty()){
			/*for(int i=0;i<brkList.size();i++){
				if(Integer.parseInt(brkList.get(i).get("panImgId")) <= 0){
					brkList.remove(i);
					i = i-1;
				}
			}*/
			actBOList = brkList;
		}
		
		if(!ownList.isEmpty()){
			for(int i=0;i<ownList.size();i++){
				//if(Integer.parseInt(ownList.get(i).get("panImgId")) > 0){
					actBOList.add(ownList.get(i));
				//}
			}
		}
		
		if(!chlnList.isEmpty() && !actBOList.isEmpty()){
			map.put("chlnList", chlnList);
			map.put("actBOList",actBOList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	
	
	@RequestMapping(value = "/getChequeNoFrLHSP" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrLHSP(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNoFrLHSP---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		ChequeLeaves chequeLeaves= bankMstrDAO.getChqListByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getChqLByBnk(bankCode,CType);
		if(!chqList.isEmpty()){
			System.out.println("***************** 1");
			map.put("list",chqList);
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getChlnFrLSP" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnFrLSP(@RequestBody String chlnCode) {  
		System.out.println("Enter into getChlnFrLSP---->");	
		Map<String, Object> map = new HashMap<>();	
		
		if(chlnCode != null){
			List<Challan> chlnList = challanDAO.getChallanList(chlnCode);
			if(!chlnList.isEmpty()){
				map.put("arHoAlw",false);
				int res = challanDAO.checkLSP(chlnList.get(0).getChlnId());
				
				if(res > 0){
					map.put("msg","you already genrate the LHPV SUPPLEMENTARY for this challan");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}else{
					if(chlnList.get(0).getChlnArId() > 0){
						//LhpvBal lhpvBal = chlnList.get(0).getLhpvBal();
						List<LhpvBal> lhpvBalList = challanDAO.getLBOfChln(chlnList.get(0).getChlnId());
						
						map.put("drRcvrWtF",false);
						map.put("wtShrtgF",false);
						map.put("ltDelF",false);
						map.put("ltAckF",false);
						map.put("exKmF",false);
						map.put("ovrHgtF",false);
						map.put("penaltyF",false);
						map.put("othF",false);
						map.put("detF",false);
						map.put("unLdgF",false);
						
						if(!lhpvBalList.isEmpty()){
							for(int i=0;i<lhpvBalList.size();i++){
								
								LhpvBal lhpvBal = lhpvBalList.get(i);
								ArrivalReport arrivalReport = arrivalReportDAO.getArById(chlnList.get(0).getChlnArId());
								if(arrivalReport.isArIsHOAlw() == true){
									map.put("arHoAlw",true);
									System.out.println("arrivalReport.getArWtShrtg() = "+arrivalReport.getArWtShrtg());
									
									if((boolean)map.get("drRcvrWtF") == false){
										if(/*lhpvBal.getLbDrRcvrWtCR() <= 0 &&*/ arrivalReport.getArRemDrRcvrWt()>0){
											map.put("drRcvrWt",arrivalReport.getArRemDrRcvrWt());
											map.put("drRcvrWtF",true);
										}else{
											map.put("drRcvrWtF",false);
										}
									}
									
									
									if((boolean)map.get("wtShrtgF") == false){
										if(/*lhpvBal.getLbWtShrtgCR() <= 0 &&*/ arrivalReport.getArRemWtShrtg()>0){
											map.put("wtShrtg",arrivalReport.getArRemWtShrtg());
											map.put("wtShrtgF",true);
										}else{
											map.put("wtShrtgF",false);
										}
									}
									
									
									if((boolean)map.get("ltDelF") == false){
										if(/*lhpvBal.getLbLateDelCR() <= 0 &&*/ arrivalReport.getArRemLateDelivery()>0){
											map.put("ltDel",arrivalReport.getArRemLateDelivery());
											map.put("ltDelF",true);
										}else{
											map.put("ltDelF",false);
										}
									}
									
									
									if((boolean)map.get("ltAckF") == false){
										if(/*lhpvBal.getLbLateAckCR() <= 0 &&*/ arrivalReport.getArRemLateAck()>0){
											map.put("ltAck",arrivalReport.getArRemLateAck());
											map.put("ltAckF",true);
										}else{
											map.put("ltAckF",false);
										}
									}
									
									
									if((boolean)map.get("exKmF") == false){
										if(/*lhpvBal.getLbOthExtKmP() <= 0 &&*/ arrivalReport.getArRemExtKm()>0){
											map.put("exKm",arrivalReport.getArRemExtKm());
											map.put("exKmF",true);
										}else{
											map.put("exKmF",false);
										}
									}
									
									
									if((boolean)map.get("ovrHgtF") == false){
										if(/*lhpvBal.getLbOthOvrHgtP() <= 0 &&*/ arrivalReport.getArRemOvrHgt()>0){
											map.put("ovrHgt",arrivalReport.getArRemOvrHgt());
											map.put("ovrHgtF",true);
										}else{
											map.put("ovrHgtF",false);
										}
									}
									
									
									if((boolean)map.get("penaltyF") == false){
										if(/*lhpvBal.getLbOthPnltyP() <= 0 &&*/ arrivalReport.getArPenalty()>0){
											map.put("penalty",arrivalReport.getArPenalty());
											map.put("penaltyF",true);
										}else{
											map.put("penaltyF",false);
										}
									}
									
									
									if((boolean)map.get("othF") == false){
										if(/*lhpvBal.getLbOthMiscP() <= 0 &&*/ arrivalReport.getArOther()>0){
											map.put("oth",arrivalReport.getArOther());
											map.put("othF",true);
										}else{
											map.put("othF",false);
										}
									}
									
									
									if((boolean)map.get("detF") == false){
										if(/*lhpvBal.getLbUnpDetP() <= 0 &&*/ arrivalReport.getArRemDetention()>0){
											map.put("det",arrivalReport.getArRemDetention());
											map.put("detF",true);
										}else{
											map.put("detF",false);
										}
									}
									
									
									if((boolean)map.get("unLdgF") == false){
										if(/*lhpvBal.getLbUnLoadingP() <= 0 &&*/ arrivalReport.getArRemUnloading()>0){
											map.put("unLdg",arrivalReport.getArRemUnloading());
											map.put("unLdgF",true);
										}else{
											map.put("unLdgF",false);
										}
									}
									
									
								}else{
									map.put("arHoAlw",false);
								}

							}
						}else{
							System.out.println("Bp Lhpv Bal generated for this challan");
						}
											
						
					}
					
					map.put("chln",chlnList.get(0));
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}							
				
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	
	@RequestMapping(value = "/submitLhpvSup" , method = RequestMethod.POST)  
	public @ResponseBody Object submitLhpvSup(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitLhpvSup---->");	
		Map<String, Object> map = new HashMap<>();	
		//int laNo = lhpvStatusDAO.saveLhpvAdv(voucherService);
		int i = arrivalReportDAO.saveRemBalSup(voucherService);
		int lspNo = lhpvStatusDAO.saveLhpvSup(voucherService);
		if(lspNo > 0  && i>0){
			map.put("lspNo",lspNo);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	
		return map;
	}	
	
	@RequestMapping(value = "/verifyChlnForLhpvSup" , method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnForLhpvSup(@RequestBody String chlnCode){
		System.out.println("LhpvAdvCntlr.verifyChlnForLhpvSup()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.verifyChlnForLhpvSup(chlnCode);
		return map;
	}
	
	//TODO Back Data 
	@RequestMapping(value = "/getLhpvSupDetBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvSupDetBack() {  
		System.out.println("Enter into getLhpvBalDetBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<Map<String,Object>> bnkList = new ArrayList<>();
				bnkList = bankMstrDAO.getBnkNameAndCode(branchCode);
				LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatusBack(branchCode);
				if(lhpvStatus != null){
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put("lhpvStatus",lhpvStatus);
					map.put("bnkList",bnkList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	

	
}
