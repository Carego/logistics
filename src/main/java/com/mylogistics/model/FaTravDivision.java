package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "fatravdivision")
public class FaTravDivision {

	@Id
	@Column(name="ftdId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ftdId;
	
	/*@Column(name="ftdSvmId")
	private int ftdSvmId;*/
	
	@Column(name="ftdTravType")
	private String ftdTravType;
	
	@Column(name="ftdTravAmt")
	private double ftdTravAmt;
	
	@Column(name="ftdHName")
	private String ftdHName;
	
	@Column(name="ftdHRate")
	private double ftdHRate;
	
	@Column(name="ftdPlace")
	private String ftdPlace;
	
	@Column(name="ftdFrDt")
	private Date ftdFrDt;
	
	@Column(name="ftdToDt")
	private Date ftdToDt;
	
	@Column(name="ftdCsId", columnDefinition = "int default -1")
	private int ftdCsId;
	
	//Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name = "empId") 
	private Employee employee;
	
	@Column(name="ftdDesc" , columnDefinition="TEXT")
	private String ftdDesc;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getFtdId() {
		return ftdId;
	}

	public void setFtdId(int ftdId) {
		this.ftdId = ftdId;
	}

	/*public int getFtdSvmId() {
		return ftdSvmId;
	}

	public void setFtdSvmId(int ftdSvmId) {
		this.ftdSvmId = ftdSvmId;
	}*/

	public String getFtdTravType() {
		return ftdTravType;
	}

	public void setFtdTravType(String ftdTravType) {
		this.ftdTravType = ftdTravType;
	}

	public double getFtdTravAmt() {
		return ftdTravAmt;
	}

	public void setFtdTravAmt(double ftdTravAmt) {
		this.ftdTravAmt = ftdTravAmt;
	}

	public String getFtdHName() {
		return ftdHName;
	}

	public void setFtdHName(String ftdHName) {
		this.ftdHName = ftdHName;
	}

	public double getFtdHRate() {
		return ftdHRate;
	}

	public void setFtdHRate(double ftdHRate) {
		this.ftdHRate = ftdHRate;
	}

	public String getFtdPlace() {
		return ftdPlace;
	}

	public void setFtdPlace(String ftdPlace) {
		this.ftdPlace = ftdPlace;
	}

	public Date getFtdFrDt() {
		return ftdFrDt;
	}

	public void setFtdFrDt(Date ftdFrDt) {
		this.ftdFrDt = ftdFrDt;
	}

	public Date getFtdToDt() {
		return ftdToDt;
	}

	public void setFtdToDt(Date ftdToDt) {
		this.ftdToDt = ftdToDt;
	}

	public String getFtdDesc() {
		return ftdDesc;
	}

	public void setFtdDesc(String ftdDesc) {
		this.ftdDesc = ftdDesc;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getFtdCsId() {
		return ftdCsId;
	}

	public void setFtdCsId(int ftdCsId) {
		this.ftdCsId = ftdCsId;
	}
}
