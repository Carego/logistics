<div ng-show="operatorLogin || superAdminLogin">
<title>GST Update</title>
<div class="row">
<div class="col s2 hide-on-med-and-down">&nbsp;</div>
	<form name=CustomerForm class="col s12 m12 l8 card"
		style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		ng-submit="submitCustomer(CustomerForm)">
		
		
		<div class="row">
		
		<div class="input-field col s3">
			<input type="text" id="customerCode" name="custCodeTemp"
				ng-model="custCodeTemp" required readonly
				ng-click="OpenCustomerCodeDB()" /> 
				<input class="validate" type ="hidden" ng-model="custCode" >
				<label>Enter Customer
				Code</label>
		</div>
		
			 <div class="input-field col s5">
    	   			<input class="validate" type ="text" id="gstId" name ="GSTName" ng-model="custGstNo" ng-pattern="/^[0-3][0-9][A-Z]{5}[0-9]{4}[A-Z]{1}[A-Z0-9]{1}[Z]{1}[A-Z0-9]{1}$/" ng-minlength="15" ng-maxlength="15"  ng-required="true">
        			<label>GST No.</label>
      		</div>
      		
      		 <div class="input-field col s3">
    	   			<input class="validate" type ="text" id="sacId" name ="SACCode" ng-model="custSAC"  ng-minlength="6"   ng-required="true">
        			<label>SAC Code</label>
      		</div>
	
		</div>
		<div class="row">
			<div class="col s12 center">
				<input class="btn waves-effect waves-light" type="submit"
					value="submit">
			</div>
		</div>
	</form>
	
	
	<div id="customerCodeDB" ng-hide="CustomerCodeDBFlag">
	<input type="text" name="filterTextbox" ng-model="filterTextbox"
		placeholder="Search">
	<table>
		<tr>
			<th></th>
			<th>customer Code</th>
			<th>customer Name</th>
			<th>Branch Code</th>
			<th>customer FACode</th>
		</tr>
	
		<tr ng-repeat="cust in custCodeList | filter:filterTextbox">
			<td><input type="radio" name="custCode" value="{{ cust }}"
				ng-model="$parent.custCode" ng-change="saveCustomerCode(cust)"></td>
			<td>{{ cust.custCode }}</td>	
			<td>{{ cust.custName }}</td>
			<td>{{ cust.bCode }}</td>
			<td>{{ cust.custFaCode }}</td>
		</tr>
	</table>
</div>

</div>
		