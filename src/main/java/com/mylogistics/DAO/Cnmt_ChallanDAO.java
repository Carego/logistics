package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;

import com.mylogistics.model.Cnmt_Challan;

public interface Cnmt_ChallanDAO {

	public int saveCnmt_Challan(Cnmt_Challan cnmt_Challan);
	
	public int saveCnmt_Challan(Cnmt_Challan cnmt_Challan,Session session);
	
	public List<Cnmt_Challan> getCnmt_ChallanByCnmtId(int cnmtId);
	
	public List<Cnmt_Challan> getCnmt_ChallanByChlnId(int chlnId);
	
	public List<Integer> getCnmtIdByChlnId(int chlnId);
	
	public List<Integer> getChlnIdByCnmtId(int cnmtId);
	
	public List<Cnmt_Challan> getAllCnChln();
}
