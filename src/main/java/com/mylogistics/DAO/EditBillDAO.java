package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;

import com.mylogistics.model.Bill;
import com.mylogistics.model.Branch;
import com.mylogistics.model.ServiceTax;
import com.mylogistics.services.BillService;

public interface EditBillDAO {

	public List<Branch>getAllBranch(Session session);
	public List<Bill>getAllBill(Session session,Integer brhId);
	public Object getBillFromBranch(Session session,String blBillNO,Integer brhId,ServiceTax serviceTax);
	public Object modifyBill(Session session,BillService billService);
	public Object modifyBillN(Session session,BillService billService);
	public Object modifyBillGST(Session session,BillService billService);
	
}