package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;

@Repository
public interface StateDAO {
	
	//Satarupa: retrieves the list of state code from database
	public List<String> getStateCode();

	//Satarupa: saves state to database
	public int saveStateToDB(State state);
	
	//Satarupa: saves state to database
	public int updateState(State state);
	
	//Satarupa: retrieves the complete database of state table
	public List<State> getStateData();
	
	public List<State> getStateDataByStaNameCode(String stnNameCode);
	
	//Satarupa: retrieves the details of state on the basis of stateCode
	 public List<State> retrieveState(String stateCode);
	
	public int savePanIssueState(State state);
	    
	//Shikha: retrieves state lorry prefix
	public List<String> getStateLryPrefix();
	
	//Satarupa: 
	public List<State> getLastStateRow();
	
	//Satarupa:
	public long totalCount();
	
	public List<String> getStateName();
	
	public List<State> getStateNameByCode(String stateCode);

	public List<State> getStateGSTName(Session session, User user);

}
