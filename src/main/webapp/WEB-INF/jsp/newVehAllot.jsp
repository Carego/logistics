<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newVehAllotForm" ng-submit=submitVehAllot(newVehAllotForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="vms.branch.branchFaCode" ng-click="openBranchDB()" readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vhNoId" name ="vhNo" ng-model="vms.vehicleMstr.vehNo" ng-required="true" >
		       			<label for="code">Vehicle Number</label>	
		       		</div>
		       	  
		       	   <div class="col s4 input-field">
		       				<input class="validate" type ="number" id="vehModelId" name ="vehModel" ng-model="vms.vehicleMstr.vehModel" ng-required="true" >
		       			<label for="code">Vehicle Model</label>	
		       		</div>
		    </div>
		    <div class="row">
		    
					 <div class="col s4 input-field">
			    		<select name="vehType" id="vehTypeId" ng-model="vms.vehicleMstr.vehType" ng-change="selectVType()" ng-required="true"">
							<option value='C'>Car</option>
							<option value='M'>Motor Cycle</option>
							<option value='S'>Scooter</option>
						</select>
						<label>Vehicle Type</label>
					</div>
							    
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehOwnNameId" name ="vehOwnName" ng-model="vms.vehicleMstr.vehOwnName" ng-required="true" >
		       			<label for="code">Owner Name</label>	
		       		</div>
		       			
					<div class="col s2 input-field">
		       			<input class="validate" type ="text" id="ownAddId" name ="ownAdd" ng-model="vms.ownAdd.addId" ng-click="selectOwnAdd()" ng-required="true" >
		       			<label for="code">Owner Address</label>
		       		</div>	
		       		<div class="col s2 input-field">
		       			<input type="button" value="New Add" ng-click="addNewOwnAdd()"/>
		       		</div>	    
		 
		    </div>
		    
		     <div ng-show="showOwnAddFlag">
    		 	<table>
    		 		<caption>Owner Address</caption>
    		 		<tr>
						<th>address</th>
						<th>city</th>
						<th>Pin</th>
						<th>State</th>    		 		
    		 		</tr>
    		 		<tr>
    		 			<td>{{vms.ownAdd.completeAdd}}</td>
    		 			<td>{{vms.ownAdd.addCity}}</td>
    		 			<td>{{vms.ownAdd.addPin}}</td>
    		 			<td>{{vms.ownAdd.addState}}</td>
    		 		</tr>
    		 	</table>
    		 </div>
		    
	      	<div class="row">
	      	 
				 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="empId" name ="empName" ng-model="vms.employee.empFaCode" ng-click="openEmpDB()" readonly>
		       			<label for="code">Employee Code</label>	
		       	</div>
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehEmpLNoId" name ="vehEmpLNo" ng-model="vms.vehicleMstr.vehEmpLNo" ng-required="true" >
		       			<label for="code">Employee License No.</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="vehEmpLUptoId" name ="vehEmpLUpto" ng-model="vms.vehicleMstr.vehEmpLUpto" ng-required="true" >
		       			<label for="code">License UpTo</label>	
		       	</div>
		     
	   		</div>
	   	
	   		 <div class="row">
	   		 
	   		 	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehPolicyNoId" name ="vehPolicyNo" ng-model="vms.vehicleMstr.vehPolicyNo" ng-required="true" >
		       			<label for="code">Policy Number</label>	
		       	</div>
		       	
	     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="vehInsUptoId" name ="vehInsUpto" ng-model="vms.vehicleMstr.vehInsUpto" ng-required="true" >
		       			<label for="code">Insurance UpTo</label>	
		       	</div>
	   		 
				<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="vehInsPremId" name ="vehInsPrem" ng-model="vms.vehicleMstr.vehInsPrem">
		       			<label for="code">Insurance Premium</label>	
		       	</div>

    		 </div>	
    		 
    		 <div class="row">
    		 
    		 	<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="vehTaxUptoId" name ="vehTaxUpto" ng-model="vms.vehicleMstr.vehTaxUpto" >
		       			<label for="code">Vehicle Tax UpTo</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehTaxPlaceId" name ="vehTaxPlace" ng-model="vms.vehicleMstr.vehTaxPlace">
		       			<label for="code">Vehicle Tax Place</label>	
		       	</div>
				<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="vehTaxAmtId" name ="vehTaxAmt" ng-model="vms.vehicleMstr.vehTaxAmt">
		       			<label for="code">Vehicle Tax Amount</label>	
		       	</div>	
    		 </div>
    		  
    		 <div class="row">
    		 	
    		 	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="addressId" name ="address" ng-model="vms.address.addId" ng-click="selectRTO()">
		       			<label for="code">Vehicle RTO Address</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       			<input type="button" value="New RTO Address" ng-click="newRTOAdd()"/>
		       	</div>
		   
				<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="vehFitUptoId" name ="vehFitUpto" ng-model="vms.vehicleMstr.vehFitUpto">
		       			<label for="code">Vehicle Fitness UpTo</label>	
		       	</div>
    		 	
		       
    		 </div>
    		
    		  <div class="row scrl hi" ng-show="showRTOAdd">
    		 	<table class="tblrow">
    		 		<caption class="coltag tblrow">RTO Address</caption>
    		 		<tr class="rowclr ">
						<th class="colclr">Address</th>
						<th class="colclr">City</th>
						<th class="colclr">Pin</th>
						<th class="colclr" >State</th> 
					
					
						
    		 		</tr>
    		 	
    		 		<tr>
   		 			  <td class="rowcel">{{vms.address.completeAdd}}</td>
    		 			<td class="rowcel">{{vms.address.addCity}}</td>
    		 			<td class="rowcel">{{vms.address.addPin}}</td>
    		 			<td class="rowcel">{{vms.address.addState}}</td>
                        
    		 		</tr>
               
    		 	</table>
    		 </div>
    		
    		 
    		  <div class="row">
	
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehFitPlaceId" name ="vehFitPlace" ng-model="vms.vehicleMstr.vehFitPlace">
		       			<label for="code">Vehicle Fitness Place</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehEngineNoId" name ="vehEngineNo" ng-model="vms.vehicleMstr.vehEngineNo">
		       			<label for="code">Vehicle Engine Number</label>	
		       	</div>
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="vehChesisNoId" name ="vehChesisNo" ng-model="vms.vehicleMstr.vehChesisNo">
		       			<label for="code">Vehicle Chesis Number</label>	
		       	</div>
    		 	
    		 </div>
     		 
     		 
     		  <div class="row">
     		  	<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="vehWEFDtId" name ="vehWEFDt" ng-model="vms.vehicleMstr.vehWEFDt">
		       			<label for="code">With Effect From Date</label>	
		       	</div>
     		  </div>

			 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vms.vehicleMstr.vehComment"></textarea>
 					<label>Comment</label>
				</div>
    		 </div>	


	   		  <div class="row">
	     		<div class="col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    <div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	 	  <th>Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch"   value="{{ branch }}" ng-model="brCode" ng-click="saveBrCode(branch)"></td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchFaCode }}</td>
          </tr>
      </table> 
	</div>

	<div id ="empCodeDB" ng-hide="empCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Employee Name</th>
 	  	 	  <th>Employee FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="emp in empList | filter:filterTextbox">
		 	  <td><input type="radio"  name="emp"   value="{{ emp }}" ng-model="empFaCode" ng-click="saveEmpCode(emp)"></td>
              <td>{{ emp.empName }}</td>
              <td>{{ emp.empFaCode }}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="ownAddDB" ng-hide="ownAddDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>address</th>
 	  	 	  <th>City</th>
 	  	 	  <th>Pin</th>
 	  	 	  <th>State</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="ownAdd in ownAddList | filter:filterTextbox">
		 	  <td><input type="radio"  name="ownAdd"   value="{{ ownAdd }}" ng-model="ownAddId" ng-click="saveOwnAdd(ownAdd)"></td>
              <td>{{ ownAdd.completeAdd	 }}</td>
              <td>{{ ownAdd.addCity	}}</td>
              <td>{{ ownAdd.addPin	}}</td>
              <td>{{ ownAdd.addState }}</td>
          </tr>
      </table> 
	</div>
	
	
	
	<div id ="rtoAddDB" ng-hide="rtoAddDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>address</th>
 	  	 	  <th>City</th>
 	  	 	  <th>Pin</th>
 	  	 	  <th>State</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="rtoAdd in rtoAddList | filter:filterTextbox">
		 	  <td><input type="radio"  name="rtoAdd"   value="{{ rtoAdd }}" ng-model="rtoAddId" ng-click="saveRTO(rtoAdd)"></td>
              <td>{{ rtoAdd.completeAdd	 }}</td>
              <td>{{ rtoAdd.addCity	}}</td>
              <td>{{ rtoAdd.addPin	}}</td>
              <td>{{ rtoAdd.addState }}</td>
          </tr>
      </table> 
	</div>
	
	<div id="newRtoAddDB" ng-hide="newRtoAddDBFlag">
		<form name="newRTOForm" ng-submit="submitNewRto(newRTOForm,add)">
			<table>
					<tr>
						<td>Address</td>
						<td><input type ="text" name="completeAddName" id="completeAddId" ng-model="add.completeAdd" ng-required="true" ng-minlength="3" ng-minlength="255"></td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type ="text" name="addStateName" id="addStateId" ng-model="add.addState" ng-required="true" ng-minlength="3" ng-minlength="15"></td>
					</tr>
					<tr>
						<td>City</td>
						<td><input type ="text" name="addCityName" id="addCityId" ng-model="add.addCity" ng-required="true" ng-minlength="3" ng-minlength="40"></td>
					</tr>
				
					<tr>
						<td>Pin No</td>
						<td><input type ="number" name="addPinName" id="addPinId" ng-model="add.addPin"  ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
					</tr>	
					<tr><td colspan="2"><input type ="submit" value="Submit"></td></tr>	
			</table>	
		</form>
	</div>
	
	
	<div id="newOwnAddDB" ng-hide="newOwnAddDBFlag">
		<form name="newOwnAddForm" ng-submit="submitNewOwnAdd(newOwnAddForm,add1)">
			<table>
					<tr>
						<td>Address</td>
						<td><input type ="text" name="completeAddName" id="completeAddId" ng-model="add1.completeAdd" ng-required="true" ng-minlength="3" ng-minlength="255"></td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type ="text" name="addStateName" id="addStateId" ng-model="add1.addState" ng-required="true" ng-minlength="3" ng-minlength="15"></td>
					</tr>
					<tr>
						<td>City</td>
						<td><input type ="text" name="addCityName" id="addCityId" ng-model="add1.addCity" ng-required="true" ng-minlength="3" ng-minlength="40"></td>
					</tr>
				
					<tr>
						<td>Pin No</td>
						<td><input type ="number" name="addPinName" id="addPinId" ng-model="add1.addPin"  ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
					</tr>	
					<tr><td colspan="2"><input type ="submit" value="Submit"></td></tr>	
			</table>	
		</form>
	</div>
		
	<div id="submitVhDB" ng-hide="submitVhFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vms.branch.branchName}} ( {{vms.branch.branchFaCode}} )</td>
				</tr>
				
				<tr>
					<td>Vehicle Number</td>
					<td>{{vms.vehicleMstr.vehNo}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Model</td>
					<td>{{vms.vehicleMstr.vehModel}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Type</td>
					<td>{{vms.vehicleMstr.vehType}}</td>
				</tr>
				
				<tr>
					<td>Owner Name</td>
					<td>{{vms.vehicleMstr.vehOwnName}}</td>
				</tr>
				
				<tr>
					<td>Owner Address</td>
					<td>
						<table>
							<tr>
								<th>Address</th>
								<th>City</th>
								<th>Pin</th>
								<th>State</th>
							</tr>
							<tr>
								<td>{{vms.ownAdd.completeAdd}}</td>
								<td>{{vms.ownAdd.addCity}}</td>
								<td>{{vms.ownAdd.addPin}}</td>
								<td>{{vms.ownAdd.addState}}</td>
							</tr>
						</table>				
					</td>
				</tr>
				
				
				<tr>
					<td>Employee</td>
					<td>{{vms.employee.empName}} ( {{vms.employee.empFaCode}} )</td>
				</tr>
				
				<tr>
					<td>Policy Number</td>
					<td>{{vms.vehicleMstr.vehPolicyNo}}</td>
				</tr>
				
				<tr>
					<td>Insurance UpTo</td>
					<td>{{vms.vehicleMstr.vehInsUpto}}</td>
				</tr>
				
				<tr>
					<td>Insurance Premium</td>
					<td>{{vms.vehicleMstr.vehInsPrem}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Tax UpTo</td>
					<td>{{vms.vehicleMstr.vehTaxUpto}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Tax Place</td>
					<td>{{vms.vehicleMstr.vehTaxPlace}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Tax Amount</td>
					<td>{{vms.vehicleMstr.vehTaxAmt}}</td>
				</tr>
				
				<tr>
					<td>Vehicle RTO Address</td>
					<td>
						<table>
							<tr>
								<th>Address</th>
								<th>City</th>
								<th>Pin</th>
								<th>State</th>
							</tr>
							<tr>
								<td>{{vms.address.completeAdd}}</td>
								<td>{{vms.address.addCity}}</td>
								<td>{{vms.address.addPin}}</td>
								<td>{{vms.address.addState}}</td>
							</tr>
						</table>				
					</td>
				</tr>
				
				<tr>
					<td>Vehicle Fitness UpTo</td>
					<td>{{vms.vehicleMstr.vehFitUpto}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Fitness Place</td>
					<td>{{vms.vehicleMstr.vehFitPlace}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Engine Number</td>
					<td>{{vms.vehicleMstr.vehEngineNo}}</td>
				</tr>
				
				<tr>
					<td>Vehicle Chesis Number</td>
					<td>{{vms.vehicleMstr.vehChesisNo}}</td>
				</tr>
				
				<tr>
					<td>With Effect From Date</td>
					<td>{{vms.vehicleMstr.vehWEFDt}}</td>
				</tr>
				
				<tr>
					<td>Comment</td>
					<td>{{vms.vehicleMstr.vehComment}}</td>
				</tr>

			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveVms()"/>
		</div>
	</div>

</div>
