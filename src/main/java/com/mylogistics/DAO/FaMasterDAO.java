package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
@Repository
public interface FaMasterDAO {

	//Satarupa: saves fAMaster to database
	public int saveFaMaster(FAMaster fAMaster);
	
	public int saveFaMaster(FAMaster fAMaster,Session session);
	
	//Satarupa: updates fAMaster to database
	public int updateFaMaster(FAMaster fAMaster);
	
	public void updateFaMaster(FAMaster fAMaster,Session session);
	
	//Satarupa: retrieve the details of FAMaster table on the basis of faMfaCode
	public List<FAMaster> retrieveFAMaster(String faMfaCode);
	
	public List<FAMaster> retrieveFAMaster(String faMfaCode,Session session);
	
	public List<FAMaster> getMultiFaM(String faMfaType);
	
	public List<Map<String, Object>> getMultiFaMByNameOrCode(String faName);
	
	public List<FAMaster> getFAMByFaName(String faMfaName);
	
	public List<FAMaster> getAllTdsFAM();
	
	public List<FAMaster> getAllFaMstr();
	
	public List<FAMaster> getAllFaMstrByFaNameOrCode(String faNameCode);
	
	public List<FAMaster> getAllFAM();
	
	public List<FAMaster> getAllFAM(Session session);
	
	public List<Map<String, Object>> getFaCodeName();
	
	public Object getFaCodeFrLdr(String faMfaCode);
	
	public Map<String, String> getAllFAM(Session session, User user, Set<String> faCodes);

	public List<FAMaster> getFAMByFaNameN(Session session, String faMfaName);

	public List<FAMaster> getFAMByFaName(Session session, String faMfaName);
	
}
