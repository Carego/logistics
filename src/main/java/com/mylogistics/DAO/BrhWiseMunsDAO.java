package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.lhpv.BrhWiseMuns;

@Repository
public interface BrhWiseMunsDAO {

	public int saveBrhMuns(BrhWiseMuns brhWiseMuns);
	
	public BrhWiseMuns getBrhMuns(int brId);
}
