<div ng-show="operatorLogin || superAdminLogin">
<title>Customer</title>
<div class="row">
	<div class="col s2 hide-on-med-and-down">&nbsp;</div>
	<form name=CustomerForm class="col s12 m12 l8 card"
		style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		ng-submit="submitCustomer(CustomerForm,customer,custrep)">
		<div class="row">
			<div class="input-field col s12 m4 l4">
			<input class="validate" type ="text" name ="branchFaCode" ng-model="branchFaCode" readonly ng-click="OpenBranchCodeDB()" required>
        <input class="validate" type ="hidden" name ="branchCode" ng-model="customer.branchCode" >
				<!-- <input type="text" name="branchCode" id="branchCode"
					ng-model="customer.branchCode" ng-click="OpenBranchCodeDB()"
					required readonly>  --><label>Branch Code</label>
			</div>
			<div class="input-field col s12 m8 l8">
				<input id="fname" type="text" name="custName"
					ng-model="customer.custName" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label> Name</label>
			</div>


		</div>
		
		<div class="row">
			<div class="input-field col s2">
      		 	<input class="validate" type ="text" name ="stateGSTName" ng-model="stateGST"  ng-click="openStateGSTCodeDB()" ng-required="true" readonly>
        		<label>State Code GST</label>
      		</div>
            <div class="input-field col s3">
       			<input class="validate" type ="text" name ="stateName" ng-model="stateName"  ng-click="openStateGSTCodeDB()" ng-required="true" readonly>
        		<label>State Name</label>
      		</div>
      		
      		 <div class="input-field col s4">
       			<input class="validate" type ="text" name ="GSTName" ng-model="customer.custGstNo" ng-pattern="/^[0-3][0-9][A-Z]{5}[0-9]{4}[A-Z]{1}[A-Z0-9]{3}$/" ng-minlength="15" ng-maxlength="15"  ng-required="true">
        		<label>GST No.</label>
      		</div>
      		
      		 <div class="input-field col s3">
       			<input class="validate" type ="text" name ="SACCode" ng-model="customer.custSAC"  ng-minlength="6"  ng-required="true">
        		<label>SAC Code</label>
      		</div>
		
		</div>
		
		<div class="row">
			<div class="input-field col s12">
				<input id="add" type="text" name="custAdd"
					ng-model="customer.custAdd" ng-minlength="3" ng-maxlength="255"
					ng-required="true"> <label>Address</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m4 l4">
				<input id="city" type="text" name="custCity"
					ng-model="customer.custCity" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label>City</label>
			</div>

			<div class="input-field col s12 m4 l4">
				<input id="state" type="text" name="custState"
					ng-model="customer.custState" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label>State</label>
			</div>
			<div class="input-field col s12 m4 l4">
				<input id="pin" type="text" name="custPin"
					ng-model="customer.custPin" ng-minlength="6" ng-maxlength="6"
					ng-required="true"> <label>Pin</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12 m6 l3">
				<input id="circle" type="text" name="custTdsCircle"
					ng-model="customer.custTdsCircle" ng-minlength="3"
					ng-maxlength="40"> <label>TDS Circle</label>
			</div>
			<div class="input-field col s12 m6 l3">
				<input id="tdscity" type="text" name="custTdsCity"
					ng-model="customer.custTdsCity" ng-minlength="3" ng-maxlength="40">
				<label>TDS City</label>
			</div>
			<div class="input-field col s12 m6 l3">
				<input type="text" id="custPanNo" name="custPanNo"
					ng-model="customer.custPanNo" ng-minlength="10" ng-maxlength="10"
					ng-pattern="/[a-zA-Z0-9]/"> <label>PAN
					Number</label>
			</div>
			<div class="input-field col s12 m6 l3">
				<input id="custTinNo" type="text" name="custTinNo"
					ng-model="customer.custTinNo" ng-minlength="8" ng-maxlength="12"
					min="0"> <label>TIN Number</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12 m6 l6">
				<select name="custStatus" ng-model="customer.custStatus" required>
					<option value="Private Limited">Private Limited</option>
					<option value="Limited">Limited</option>
					<option value="Partnership">Partnership</option>
					<option value="Others">Others</option>
				</select> <label>Customer Status</label>
			</div>

			<div class="input-field col col s12 m6 l6">
				<input type="hidden" ng-model="customer.custIsDailyContAllow"
					ng-init="customer.custIsDailyContAllow='no'"> <input
					type="checkbox" ng-model="checkcustIsDailyContAllow"
					ng-change="savecustIsDailyContAllow()"> <label>Daily
					Contract Allowed</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12 m6 l4">
				<input id="custDirector" type="text" name="custDirector"
					ng-model="temp.custDirector" ng-click="OpenDirectorDB()"
					readonly> <label>Director</label>
			</div>
			<div class="input-field col s12 m6 l4">
				<input id="custMarketingHead" type="text" name="custMarketingHead"
					ng-model="temp.custMarketingHead" ng-click="OpenMarketingDB()"
					readonly> <label>Marketing Head</label>
			</div>
			<div class="input-field col s12 m6 l4">
				<input type="text" id="custCorpExecutive" name="custCorpExecutive"
					ng-model="temp.custCorpExecutive" ng-click="OpenExecutiveDB()"
					readonly> <label>Corporate Executive</label>
			</div>
			<div class="input-field col s12 m6 l4">
				<input id="custBranchPerson" type="text" name="custBranchPerson"
					ng-model="temp.custBranchPerson" ng-click="OpenBranchPersonDB()"
					readonly> <label>Branch Head</label>
			</div>
			<div class="input-field col s12 m6 l4">
				<input id="custCRPeriod" type="number" name="custCRPeriod"
					ng-model="customer.custCrPeriod"> <label>CR_Period</label>
			</div>
			<div class="input-field col s12 m6 l4">
				<select name="custCrBase" ng-model="customer.custCrBase">
					<option value="BF">BF</option>
					<option value="BL">BL</option>
					<option value="CN">CN</option>
				</select> <label>CR_Base</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m6 l6">
				<select name="custSrvTaxBy" ng-model="customer.custSrvTaxBy"
					>
					<option value="C">Customer</option>
					<option value="S">TCGPPL</option>
				</select> <label>Srv_Tax_By</label>
			</div>
			<div class="input-field col s12 m6 l6">
				<select name="custBillCycle" ng-model="customer.custBillCycle"
					ng-change="setCustBillValue()">
					<option value='D'>D</option>
					<option value='W'>W</option>
					<option value='M'>M</option>
					<option value='F'>F</option>
				</select> <label>Bill Cycle</label>
			</div>
			<div class="input-field col s12 m6 l6">
				<input type="number" name="custBillValueName" id="custBillValueId"
					ng-model="customer.custBillValue" disabled="disabled"
					/ ng-blur="checkBillValue()"> <label>Customer Bill
					Value</label>
			</div>
			<div class="input-field col s12 m6 l6">
				<input class="btn col s12" type="button" name="acrd" id="acrd"
					value="Add CR Details" ng-model="custRepDetail"
					ng-click="OpenCRDB()"> <label>Customer Rep Details</label>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center">
				<input class="btn waves-effect waves-light" type="submit"
					value="submit">
			</div>
		</div>
	</form>

</div>

<div class="row" id="crdCb" ng-hide="CrdCbFlag">
	<form class="col s12 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="AddCustomerRepForm" form
		ng-submit="nextAddCR(AddCustomerRepForm,custrep)">
		<div class="row">
			<div class="col s3 hide-on-med-and-down">&nbsp;</div>
			<div class="row">
				<!-- <div class="input-field col s12 m6 l6">
        				<input type ="text" id="text2" name ="custCode" ng-model="custrep.custCode" readonly>
        				<label>Customer Code</label>
      				</div> -->
				<div class="input-field col s12 m6 l6">
					<input type="text" name="custRefNo" id="custRefNo"
						ng-model="custrep.custRefNo" ng-minlength="3" ng-maxlength="40">
					<label>Customer Reference Number</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<input type="text" id="crFirstName" name="crFirstName"
						ng-model="custrep.crFirstName" ng-minlength="3" ng-maxlength="40"
						> <label>First Name</label>
				</div>

				<div class="input-field col s12">
					<input type="text" id="crLastName" name="crLastName"
						ng-model="custrep.crLastName" ng-minlength="3" ng-maxlength="40"
						> <label>Last Name</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m4 l4">
					<input type="text" name="crDesignation" id="crDesignation"
						ng-model="custrep.crDesignation" ng-minlength="3"
						ng-maxlength="40" ng-model-options="{ debounce: 200 }"
						ng-keyup="getCustDesigList(custrep)" ng-blur="fillCustDesigVal()"
						> <label>Designation</label>
				</div>
				<div class="input-field col s12 m4 l4">
					<input type="text" name="crMobileNo" id="crMobileNo"
						ng-model="custrep.crMobileNo" ng-minlength="4" ng-maxlength="15"
						> <label>Mobile No</label>
				</div>
				<div class="input-field col s12 m4 l4">
					<input type="email" name="crEmailId" ng-model="custrep.crEmailId"
						ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/"
						ng-maxlength="40" > <label>Email
						ID</label>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
			</div>
		</div>
	</form>
</div>

<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

	<input type="text" name="filterTextbox"
		ng-model="filterTextbox" placeholder="Search">
	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
			
			<th>Branch FACode</th>
		</tr>
		<tr ng-repeat="branch in branchList | filter:filterTextbox"">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ branch.branchCode }}" ng-model="branchFaCode"
				ng-click="saveBranchCode(branch)"></td>
			<td>{{ branch.branchCode }}</td>
			<td>{{ branch.branchName }}</td>
			<td>{{ branch.branchPin }}</td>
			<td>{{ branch.branchFaCode }}</td>
		</tr>
	</table>

</div>

<div id="custDirectorDB" ng-hide="custDirectorDBFlag">
	<input type="text" name="filterDirector"
		ng-model="filterDirector"
		placeholder="Search by Employee Code">
	<table>
		<tr>
			<th></th>
			<th>Employee Name</th>
			<th>Employee Code</th>
			<!-- <th>Branch Code</th> -->
			<th>FACode</th>

		</tr>

		<tr
			ng-repeat="empCustDirector in employeeList | filter:filterDirector">
			<td><input type="radio" name="empCustDirectorName"
				class="empCustDirectorCls" value="{{ empCustDirector }}"
				ng-model="custDirector" ng-click="saveCustDirector(empCustDirector)"></td>
			<td>{{ empCustDirector.empName }}</td>
			<td>{{ empCustDirector.empCodeTemp }}</td>
			<!-- <td>{{ empCustDirector.branchCode }}</td> -->
			<td>{{ empCustDirector.empFaCode }}</td>
		</tr>
	</table>

</div>

<div id="custMktHeadDB" ng-hide="custMktHeadDBFlag">
	<input type="text" name="filterMktHead"
		ng-model="filterMktHead" placeholder="Search by Employee Code">
	<table>
		<tr>
			<th></th>
			<th>Employee Name</th>
			<th>Employee Code</th>
			<!-- <th>Branch Code</th> -->
			<th>FACode</th>

		</tr>

		<tr ng-repeat="empCustMktHead in employeeList | filter:filterMktHead">
			<td><input type="radio" name="empCustMktHeadName"
				class="empCustMktHeadCls" value="{{ empCustMktHead }}"
				ng-model="custMktHead" ng-click="saveCustMktHead(empCustMktHead)"></td>
			<td>{{ empCustMktHead.empName }}</td>
			<td>{{ empCustMktHead.empCodeTemp }}</td>
			<!-- <td>{{ empCustMktHead.branchCode }}</td> -->
			<td>{{ empCustMktHead.empFaCode }}</td>
		</tr>
	</table>

</div>

<div id="custCorpExcDB" ng-hide="custCorpExcDBFlag">
	<input type="text" name="filterCorpExc"
		ng-model="filterCorpExc" placeholder="Search by Employee Code">
	<table>
		<tr>
			<th></th>
			<th>Employee Name</th>
			<th>Employee Code</th>
			<!-- <th>Branch Code</th> -->
			<th>FACode</th>

		</tr>

		<tr ng-repeat="empCustCorpExc in employeeList | filter:filterCorpExc">
			<td><input type="radio" name="empCustCorpExcName"
				class="empCustCorpExcCls" value="{{ empCustCorpExc }}"
				ng-model="custCorpExc" ng-click="saveCustCorpExc(empCustCorpExc)"></td>
			<td>{{ empCustCorpExc.empName }}</td>
			<td>{{ empCustCorpExc.empCode }}</td>
			<!-- <td>{{ empCustCorpExc.branchCode }}</td> -->
			<td>{{ empCustCorpExc.empFaCode }}</td>
		</tr>
	</table>

</div>


	  
	  <div id ="stateCodeDB" ng-hide="stateCodeDBFlag">
 	  <input type="text" name="filterstateCode" ng-model="filterStateCode.stateName" placeholder="Search by State Name">
 	  <table>
 	 		<tr>
 	 		    <th></th>				
				<th>State GST Code</th>
				<th>State Name</th>
			</tr>
 	  	  
		  <tr ng-repeat="state in stateList | filter:filterStateCode">
		 	  <td><input type="radio"  name="stateName"  class="stateCls"  value="{{ state }}" ng-model="stateGST" ng-click="saveStateCode(state)"></td>
              <td>{{state.stateGST}}</td>
              <td>{{state.stateName}}</td>
          </tr>
         </table>
         
	</div>



<div id="custBrPersonDB" ng-hide="custBrPersonDBFlag">
	<input type="text" name="filterBrPerson"
		ng-model="filterBrPerson"
		placeholder="Search by Employee Code">
	<table>
		<tr>
			<th></th>

			<!-- <th>Employee Code</th> -->

			<th>Employee Name</th>

			<th>Employee Code</th>
			<!-- <th>Branch Code</th> -->
			
			<th>FACode</th>

		</tr>

		<tr ng-repeat="empCustBrPerson in employeeList | filter:filterBrPerson">
			<td><input type="radio" name="empCustBrPersonName"
				class="empCustempCustBrPersonCls" value="{{ empCustBrPerson }}"
				ng-model="custBrPerson" ng-click="saveCustBrPerson(empCustBrPerson)"></td>
			<!-- <td>{{ empCustBrPerson.empCode }}</td> -->
			<td>{{ empCustBrPerson.empName }}</td>
			<td>{{ empCustBrPerson.empCodeTemp }}</td>
			<!-- <td>{{ empCustBrPerson.branchCode }}</td> -->
			<td>{{ empCustBrPerson.empFaCode }}</td>
		</tr>
	</table>

</div>

<div id="viewCustDetailsDB" ng-hide="viewCustDetailsFlag">

	<div class="row">

		<div class="col s12 card"
			style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<h4>
				Here's the details of Customer <span
					class="teal-text text-lighten-2">{{customer.custName}}</span>:
			</h4>
			<table class="table-bordered table-hover table-condensed">

				<tr>
					<td>Customer Name:</td>
					<td>{{customer.custName}}</td>
				</tr>
				<tr>
					<td>Customer Address:</td>
					<td>{{customer.custAdd}}</td>
				</tr>
				<tr>
					<td>Customer City:</td>
					<td>{{customer.custCity}}</td>
				</tr>

				<tr>
					<td>Customer State:</td>
					<td>{{customer.custState}}</td>
				</tr>

				<tr>
					<td>Customer Pin:</td>
					<td>{{customer.custPin}}</td>
				</tr>

				<tr>
					<td>Customer TDS Circle:</td>
					<td>{{customer.custTdsCircle}}</td>
				</tr>

				<tr>
					<td>Customer TDS City:</td>
					<td>{{customer.custTdsCity}}</td>
				</tr>

				<tr>
					<td>Customer PAN Number:</td>
					<td>{{customer.custPanNo}}</td>
				</tr>

				<tr>
					<td>Customer TIN Number:</td>
					<td>{{customer.custTinNo}}</td>
				</tr>

				<tr>
					<td>Customer Status:</td>
					<td>{{customer.custStatus}}</td>
				</tr>

				<tr>
					<td>Daily Contract Allowed:</td>
					<td>{{customer.custIsDailyContAllow}}</td>
				</tr>

				<tr>
					<td>Director:</td>
					<td>{{customer.custDirector}}</td>
				</tr>

				<tr>
					<td>Marketing Head:</td>
					<td>{{customer.custMktHead}}</td>
				</tr>

				<tr>
					<td>Corp. Executive:</td>
					<td>{{customer.custCorpExc}}</td>
				</tr>

				<tr>
					<td>Branch Person:</td>
					<td>{{customer.custBrPerson}}</td>
				</tr>

				<tr>
					<td>CR Period</td>
					<td>{{customer.custCrPeriod}}</td>
				</tr>

				<tr>
					<td>CR Base:</td>
					<td>{{customer.custCrBase}}</td>
				</tr>

				<tr>
					<td>Service Tax By:</td>
					<td>{{customer.custSrvTaxBy}}</td>
				</tr>

				<tr>
					<td>State GST Code:</td>
					<td>{{customer.stateGST}}</td>
				</tr>
				
				<tr>
					<td>GST No.:</td>
					<td>{{customer.custGstNo}}</td>
				</tr>
				<tr>
				<td>SAC Code.:</td>
					<td>{{customer.custSAC}}</td>
				</tr>
				
				<tr>
					<td>Bill Cycle:</td>
					<td>{{customer.custBillCycle}}</td>
				</tr>

				<tr>
					<td>Bill Value:</td>
					<td>{{customer.custBillValue}}</td>
				</tr>

				<tr>
					<td>Customer Referance No.:</td>
					<td>{{custrep.custRefNo}}</td>
				</tr>

				<tr>
					<td>Customer Representative Name:</td>
					<td>{{custrep.crName}}</td>
				</tr>


				<tr>
					<td>Customer Representative Designation:</td>
					<td>{{custrep.crDesignation}}</td>
				</tr>

				<tr>
					<td>Customer Representative Mobile No.:</td>
					<td>{{custrep.crMobileNo}}</td>
				</tr>
				
				

				<tr>
					<td>Customer Representative Email Id:</td>
					<td>{{custrep.crEmailId}}</td>
				</tr>
				<tr ng-repeat="CRCode in custRepList">
					<td>Customer Representative Codes:</td>
					<td>{{CRCode}}</td>
				</tr>
			</table>

			<input type="button" value="Save" id="saveBtnId" ng-click="saveCustomer(customer)">
			<input type="button" value="Cancel"
				ng-click="closeViewCustDetailsDB()">


		</div>
	</div>
</div>

<div id="afterSaveDB" ng-hide="afterSaveDBFlag"> 
	<table>
		<tr>
			<td>Customer Code : </td>
			<td>{{ afterSaveCustCode }} </td>
		</tr>
		<tr>
			<td>CustomerRepresentative Code : </td>
			<td>{{ afterSaveCRepCode }} </td>
		</tr>
		<tr>
			<td>
				<input type="button" value="OK" ng-click="afterSaveClose()"/>
			</td>
		</tr>
	</table>
</div>



</div>

<!-- 		<form name="CustomerForm" ng-submit="next(CustomerForm,customer)">
			<table>
				<tr>
					<td>Customer Name: *</td>
					<td><input type ="text" name ="custName" ng-model="customer.custName" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer Address: *</td>
					<td><input type ="text" name ="custAdd" ng-model="customer.custAdd" ng-minlength="3" ng-maxlength="255" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer City: *</td>
					<td><input type ="text" name ="custCity" ng-model="customer.custCity" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer State: *</td>
					<td><input type ="text" name ="custState" ng-model="customer.custState" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer Pin: *</td>
					<td><input type ="text" name ="custPin" ng-model="customer.custPin" ng-minlength="6" ng-maxlength="6" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer TDS Circle: *</td>
					<td><input type ="text" name ="custTdsCircle" ng-model="customer.custTdsCircle" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer TDS City: *</td>
					<td><input type ="text" name ="custTdsCity" ng-model="customer.custTdsCity" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer PAN Number: *</td>
					<td><input type ="text" name ="custPanNo" ng-model="customer.custPanNo" ng-minlength="10" ng-maxlength="10" ng-pattern="/[a-zA-Z0-9]/" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Customer TIN Number: *</td>
					<td><input type ="number" name ="custTinNo" ng-model="customer.custTinNo" ng-minlength="8" ng-maxlength="12" ng-required="true" min="0"></td>
				</tr>
				
				<tr>
					<td>Customer Status: *</td>
					<td>
						<select name="custStatus" ng-model="customer.custStatus" required>
							<option value="Private Limited">Private Limited</option>
							<option value="Limited">Limited</option>
							<option value="Partnership">Partnership</option>
							<option value="Others">Others</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td>Daily Contract Allowed</td>
					<td><input type ="hidden" ng-model="customer.custIsDailyContAllow" ng-init="customer.custIsDailyContAllow='no'">	
					 <input type="checkbox" ng-model="checkcustIsDailyContAllow" ng-change="savecustIsDailyContAllow()"></td>	
				</tr>
	
				<tr>
					<td><input type="submit" value="next"></td>
				</tr>
			</table>
			
		</form> -->
