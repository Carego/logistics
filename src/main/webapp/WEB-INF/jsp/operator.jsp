<title>Operator</title>
<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row" ng-if="hboList.length > 0 || oprtrNotList.length > 0 || totalHBONList.length > 0 || hboList.length > 0 || HBOSNList.length > 0" >
        <div class="col s12 m12 l12">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <table ng-repeat="hbo in hboList" class="noborder">
              	
              <tr>
              	<td></td>
				<td>Operator Code</td>
				<td>Cnmt Number</td>
				<td>Chln Number</td>
				<td>Sedr Number</td>
				<td>Branch Code</td>
				<td>Time</td>
              </tr>
              
              <tr>
              <td><input type="checkbox" name="check" ></td>
				<td>{{hbo.operatorCode}}</td>
				<td>{{hbo.noCnmt}}</td>
				<td>{{hbo.noChln}}</td>
				<td>{{hbo.noSedr}}</td>
				<td>{{hbo.disBranchCode}}</td>
				<td>{{hbo.creationTS | date : format : timezone}}</td>
				<td><input type="button" value="Create Dispatch Order" ng-click="createOrder(hbo)"></td>
              </tr>
              
            </table>
            
            <table ng-repeat="oprtrNot in oprtrNotList" class="noborder">
              	
              <tr>
				<td>Dispatch Code</td>
				<td>Date</td>
				<td>Operator Code</td>
				<td>Is Received</td>
              </tr>
              
              <tr>
				<td>{{oprtrNot.dispatchCode}}</td>
				<td>{{oprtrNot.dispatchDate}}</td>
				<td>{{oprtrNot.operatorCode}}</td>
				<td>{{oprtrNot.isRecieved}}</td>
				
				<td><input type="button" value="Show Dispatch Order" ng-click="getBrStDisDetData(oprtrNot.dispatchCode)"></td>
              </tr>
              
            </table>
              
              
            <table ng-repeat="HBON in totalHBONList">
              	
              <tr>
              	<td></td>
				<td>Operator Code</td>
				<td>Cnmt Number</td>
				<td>Chln Number</td>
				<td>Sedr Number</td>
				<td>Branch Code</td>
				<td>Time</td>
              </tr>
              
              <tr>
              <td><input type="checkbox" name="check" ></td>
				<td>{{HBON.operatorCode}}</td>
				<td>{{HBON.noCnmt}}</td>
				<td>{{HBON.noChln}}</td>
				<td>{{HBON.noSedr}}</td>
				<td>{{HBON.disBranchCode}}</td>
				<td>{{HBON.creationTS | date : format : timezone}}</td>
				<!-- <td><input type="button" value="Create Dispatch Order" ng-click="createOrder(hbo.bCode)"></td> -->
              </tr>
              
              <tr>
              	<td><input type ="date" ng-model="dispatchDate"></td>
              	 <td><input type="button" ng-click="getListWithDate(dispatchDate)"></td>
              </tr>
              
            </table>
              
              
            <span class="card-title">Stationary Notification</span>
             
              	<table ng-repeat="HBOSN in HBOSNList">
              	
	              <tr>
	              	<td></td>
					<td>Cnmt Number</td>
					<td>Chln Number</td>
					<td>Sedr Number</td>
					<td>Send To Vendor</td>
	              </tr>
	              
	              <tr>
	              <td><input type="checkbox" name="check" ></td>
					<td>{{HBOSN.hBOSN_cnmtNo}}</td>
					<td>{{HBOSN.hBOSN_chlnNo}}</td>
					<td>{{HBOSN.hBOSN_sedrNo}}</td>
					<td>{{HBOSN.hBOSN_isCreate}}</td>
					<!-- <td>{{HBOSN.creationTS | date : format : timezone}}</td> -->
					<td ng-if="HBOSN.hBOSN_isCreate === 'yes'">
						<input type="button" id="createSO_id" value="Create Stationary Order" disabled="disabled">
					</td>
					<td ng-if="HBOSN.hBOSN_isCreate === 'no'">
						<input type="button" id="createSO_id" value="Create Stationary Order" ng-click="createStnOrder(HBOSN)">
					</td>
					<td ng-if="HBOSN.hBOSN_isCreate === 'yes'">
						<input type="button" id="recSO_id" value="Receive Stationary Order" ng-click="recStnOrder(HBOSN)">
					</td>
					<td ng-if="HBOSN.hBOSN_isCreate === 'no'">
						<input type="button" id="recSO_id" value="Receive Stationary Order" disabled="disabled">
					</td>
					<!-- <td><input type="button" value="Create Dispatch Order" ng-click="createOrder(hbo.bCode)"></td> -->
	              </tr>
	              
	            <!--   <tr>
	              	<td><input type ="date" ng-model="dispatchDate"></td>
	              	 <td><input type="button" ng-click="getListWithDate(dispatchDate)"></td>
	              </tr> -->
	              
           	 </table>
        
            </div>
           
          </div>
        </div>
        
        
      <div id="createStnOrderDB" ng-hide="createStnOrderFlag" style="margin-top:50px;">
    	 <form name="StationaryOrderForm" ng-submit="submitStationaryOrder(StationaryOrderForm,st)">
	 	  		<div class="row">
      				<div class="input-field col s6">
        				<input class="validate" type ="date" name="stOdrDt" ng-model="st.stOdrDt" required>
						<label>Stationary Order Date*</label>
      				</div>
      			   <div class="input-field col s6">
        				<input class="validate" type ="text" name="stSupCode" ng-model="st.stSupCode" ng-click="openStationarySupplierDB()" readonly required>
        				<label>Supplier Code</label>	
      			   </div>
      			</div>
      			 
      			<div class="row">
      			 	<div class="input-field col s4">
      					<input class="validate" type ="number" name="stOdrCnmt" ng-model="st.stOdrCnmt"  readonly>
      					<label>Number Of Books For CNMT</label>
      				</div>	
      			 
	      			<div class="input-field col s4">
	      					<input class="validate" type ="number" name="stOdrChln" ng-model="st.stOdrChln" readonly>
	      					<label>Number Of Books For Challan</label>
	      			</div>
	      			 
	      			<div class="input-field col s4">	
	      			 		<input class="validate" type ="number" name="stOdrSedr" ng-model="st.stOdrSedr"  readonly>
	      			 		<label>Number Of Books For SEDR</label>
	      			</div>	
      			</div>
      			
      			<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
      			</div>
 	    </form>    
    
    </div> 
     
     
     
    <div id ="StationarySupplierDB" ng-hide = "StationarySupplierFlag">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Supplier Code</th>
			</tr>
 	 
		  
		  <tr ng-repeat="supplier in supplierCodeList">
		 	  <td><input type="radio"  name="supplier" id="supplier"  value="{{ supplier }}" ng-model="stnCode3" ng-click="saveSupplierCode(supplier)"></td>
             <td>{{ supplier.stSupCode }}</td>
              
          </tr>
       </table>
         
	</div>      
<!--         <div class="col s12 m6 l4">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Employee</span>
              <p> content </p>
            </div>
            <div class="card-action">
              <a href="#">link</a>
              <a href='#'>link</a>
            </div>
          </div>
        </div> -->
     </div>
     
	 
     <div id="dispatchDetailsDB" ng-hide="dispatchDetailsDBFlag">
     <form name="dispatchDetailForm" ng-submit="submitDisDetForm(dispatchDetailForm, brsDisData.brsDisDt, receiveDate, invoiceNo)">
		<table>
			<tr>
				<td>Dispatch Code</td>
				<td><input type="text" name="dispatchCode" ng-model="dispatchCode" readonly> </td>
			</tr>
			
			<tr>
				<td>CNMT
					<div ng-repeat="cnmtStartNo in cnmtStartNoList">
						<input type="checkbox" name="checkCnmts" class="abc">
						{{cnmtStartNo}}
						<br>
					</div>
				</td>
				<td>CHALLAN
					<div ng-repeat="chlnStartNo in chlnStartNoList">
					<input type="checkbox" name="checkChlns" class="abc">
						{{chlnStartNo}}
						<br>
					</div>
				</td>
				<td>SEDR
					<div ng-repeat="sedrStartNo in sedrStartNoList">
					<input type="checkbox" name="checkSedrs" class="abc">
						{{sedrStartNo}}
						<br>
					</div>
				</td>
				
			</tr>
			
			<tr>
			 	<td>Dispatch Date</td>
			 	<td><input type="date" name="dispatchDate" ng-model="brsDisData.brsDisDt" readonly></td>
			</tr>
			
			<tr>
			 	<td>Receive Date</td>
			 	<td><input type="date" name="receiveDate" ng-model="receiveDate" ng-blur="compareDate()"></td>
			</tr>
			
			<tr>
			 	<td>Dispatch Invoice Number</td>
			 	<td><input type="text" name="invoiceNo" ng-model="invoiceNo"></td>
			</tr>
			
			<tr>
				<td><input type="submit" name="submit" value="Submit"></td>
			</tr>
			
		</table>
	</form>
     
     </div>
     
     
     <div id="RecStationaryDB" ng-hide="RecStationaryFlag">
     	<form name="recStationaryForm" ng-submit="recStationary(date,nowCnmtSeq,nowChlnSeq,nowSedrSeq)">
     		<table>
				<!-- <tr>
					<td>Stationary Code</td>
					<td><input type="text" name="stnCode" ng-model="Stationary.stCode" readonly> </td>
				</tr> -->
				<tr>
					<td>Supplier Code</td>
					<td><input type="text" name="supCode" ng-model="Stationary.stSupCode" readonly> </td>
				</tr>
				<tr>
					<td>Date</td>
					<td><input type="date" name="stnDate" ng-model="date" ng-required="true"> </td>
				</tr>
				<tr>
					<td>No Of Cnmt</td>
					<td><input type="text" name="stnDate" ng-model="Stationary.stOdrCnmt" readonly> </td>
					<td>Sequence no.</td>
					<td><input type="text" name="stnDate" ng-model="nowCnmtSeq" ng-required="true"> </td>
				</tr>
				<tr>
					<td>No Of Chln</td>
					<td><input type="text" name="stnDate" ng-model="Stationary.stOdrChln" readonly> </td>
					<td>Sequence no.</td>
					<td><input type="text" name="stnDate" ng-model="nowChlnSeq" ng-required="true"> </td>
				</tr>
				<tr>
					<td>No Of Sedr</td>
					<td><input type="text" name="stnDate" ng-model="Stationary.stOdrSedr" readonly> </td>
					<td>Sequence no.</td>
					<td><input type="text" name="stnDate" ng-model="nowSedrSeq" ng-required="true"> </td>
				</tr>
				<tr>
					<td>Supplier Invoice No.</td>
					<td><input type="text" name="invoiceNo" ng-model="Stationary.stSupInvcNo" ng-required="true"> </td>
				</tr>
				<tr>
					<td><input type="submit" value="submit" > </td>
				</tr>
			</table>	
        </form>
     </div>
     
     
     
     <div id ="operatorCodeDB" ng-hide = "operatorCodeFlag"> 	  
 	  		<table class="noborder">
 	 		<tr>
				<th></th>
				<th>Operator Code</th>
			</tr> 	 
		  	<tr ng-repeat="opcode in opCodeList">
		 	  <td><input type="radio"  name="opcode"  class="opcode"  value="{{ opcode.userCode }}" ng-model="operatorCode" ng-click="savOperatorCode(opcode.userCode)"></td>
              <td>{{ opcode.userCode }}</td>
          </tr>
         </table>         
		</div>
		
     <div id ="createDispatchID" ng-hide="createDispatchFlag">
     
			     <form name ="createDispatchOrderForm" ng-submit="saveBranchStockDispatch(createDispatchOrderForm,brsd,modelCnmt,modelChln,modelSedr)">
			<table >
					<tr>
					<td>ID</td>
					<td>{{HBON.hbonId}}</td>
					</tr>
					
					<tr>
						<td>Operator Code</td>
						<td><input type ="text" name="operatorCode" ng-model="brsd.operatorCode" ng-click="openOperatorCodeDB()" required readonly></td>
					</tr>
					
				
					<tr>
						<td>Branch Code</td>
						<td><input type ="text" name="brsDisBranchCode" ng-model="brsd.brsDisBranchCode" required readonly></td>
					</tr>
					
					<tr>
						<td>No of Cnmt</td>
						<td><input type ="text" name="brsDisCnmt" ng-model="brsd.brsDisCnmt" readonly></td>
					</tr>
					
					<tr ng-show="brsd.brsDisCnmt > 0">
					
					<td>
						<div ng-repeat="cnmt in cnmtTextboxList">
							<input type="text" ng-model="modelCnmt[cnmt]"  ng-blur="checkCnmtNo(modelCnmt)" />
						</div>
					</td>
					<!-- <td><input type="button" value="Save CNMT" ng-click="saveNoCnmt(modelCnmt,brsd)"></td> -->
					</tr>
					
					<tr >
						<td>No of Chln</td>
						<td><input type ="text" name="brsDisChln" ng-model="brsd.brsDisChln"   readonly></td>
					</tr>
					
					<tr ng-show="brsd.brsDisChln > 0">
					<td>
					<div ng-repeat="chln in chlnTextboxList">
							<input type="text" ng-model="modelChln[chln]" ng-blur="checkChlnNo(modelChln)"/>
						</div>
						</td>
					<!-- <td><input type="button" value="Save Chln" ng-click="saveNoChln(modelChln,brsd)"></td> -->
					</tr>
					
					<tr>
						<td>No of Sedr</td>
						<td><input type ="text" name="brsDisSedr" ng-model="brsd.brsDisSedr"   readonly></td>
					</tr>
					
					<tr ng-show="brsd.brsDisSedr > 0">
					<td>
					<div ng-repeat="sedr in sedrTextboxList">
							<input type="text" ng-model="modelSedr[sedr]" ng-blur="checkSedrNo(modelSedr)"/>
						</div>
						</td>
					<!-- <td><input type="button" value="Save Sedr" ng-click="saveNoSedr(modelSedr,brsd)"></td> -->
					</tr>
					
					<tr>
						<td>Date</td>
						<td><input type ="date" name="brsDisDt" ng-model="brsd.brsDisDt" required></td>
					</tr>
					
				
					<tr>
						<td><input type="submit" id="submitBSD" value="Submit"></td>
					</tr>
			</table>
			</form>
     </div>
     
     
</div>     