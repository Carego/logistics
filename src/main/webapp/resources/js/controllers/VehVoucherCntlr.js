'use strict';

var app = angular.module('application');

app.controller('VehVoucherCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.vs = {};
	$scope.vm_svm = {};
	$scope.vm_svm.faCarList = [];
	$scope.vm_svm.sVehMstr = {};
	
	$scope.vmAndSvmList = [];
	$scope.chqList = [];
	
	var temp;
	$scope.svmTotAmtSum=0;
	$scope.vehNoDBFlag = true;
	$scope.bankCodeDBFlag = true;
	$scope.vehVoucherDBFlag = true;
	$scope.petExpDBFlag = true;
	$scope.repExpDBFlag = true;
	$scope.rTaxDBFlag = true;
	$scope.insuDBFlag = true;
	$scope.serDBFlag = true;
	$scope.othDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.tdsCodeFlag = true;
	$scope.selChqDBFlag = true;
	
	$scope.faCar1 = {};
	$scope.faCar2 = {};
	$scope.faCar3 = {};
	$scope.faCar4 = {};
	$scope.faCar5 = {};
	$scope.faCar6 = {};
	
	
	var max = 15;
	
	$('#fuelId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	 $('#fuelId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	 
	 $('#readingId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	 });
		
	 $('#readingId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	 
	 $('#amountId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	 });
		
	 $('#amountId').keypress(function(e) {
	  	 if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	          e.preventDefault();
	      } 
     });
	 
	 
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		//$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVDetFrVehV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.VEH_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getVehicleMstr();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.getVehicleMstr = function(){
		console.log("enter into $scope.getVehicleMstr funciton");
		var response = $http.post($scope.projectName+'/getVehMstrFrVehV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vehMstrList = data.list;
				$scope.getTdsCode();
				//$scope.fetchVMEDetails();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	} 	
	
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.payTo = "";
			$('#payToId').attr("disabled","disabled");
			$scope.vehNo = "";
			$('#vehNoId').attr("disabled","disabled");	
		}else if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			
			$('#payToId').removeAttr("disabled");
			$('#vehNoId').removeAttr("disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}	
			
			
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	
	 $scope.saveBankCode =  function(bankCode){
			console.log("enter into saveBankCode----->"+bankCode);
			$scope.vs.bankCode = bankCode;
			$('div#bankCodeDB').dialog('close');
			$scope.bankCodeDBFlag = true;
			
			if($scope.vs.payBy === 'Q'){
				
				var chqDet = {
						"bankCode" : bankCode,
						"CType"    : $scope.vs.chequeType
				};
				
				var response = $http.post($scope.projectName+'/getChequeNoFrVehV',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$('#payToId').removeAttr("disabled");
						$('#vehNoId').removeAttr("disabled");
						$scope.chqList = data.list;
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						console.log("Error in bringing data from getChequeNo");
						$scope.alertToast("Branch does not have any "+$scope.vs.chequeType+" cheque of "+bankCode+" bank");
					}

				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}else{
				$('#payToId').removeAttr("disabled");
				$('#vehNoId').removeAttr("disabled");
			}		
	 }
	 
	 
	 $scope.openChqDB = function(){
		 console.log("enter into openChqDB function");
		    $scope.selChqDBFlag = false;
	    	$('div#selChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select Cheque No.",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selChqDBFlag = true;
			    }
				});
			$('div#selChqDB').dialog('open');
	 }
	
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function");
		 $scope.vs.chequeLeaves = chq;
		 $('div#selChqDB').dialog('close');
	 }
	
	 
	 
	 $scope.openVehNoDB = function(){
		 console.log("enter into openVehNoDB function");
		 
		 $scope.petrol = false;
		 $scope.repair = false;
		 $scope.roadTax = false;
		 $scope.insurance = false;
		 $scope.service = false;
		 $scope.other = false;
		
		 $scope.vehNoDBFlag = false;
			$('div#vehNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Bank Code",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#vehNoDB').dialog('open');	
	 }
	 
	 
	 $scope.saveVehMstr = function(vehMstr){
		 console.log("enter into saveVehMstr function");
		 $scope.vehNo = vehMstr.vehNo;
		 $scope.vehNoDBFlag = true;
		 $('div#vehNoDB').dialog('close');
		 
		 var countVehNo = 0;
		 for(var i=0;i<$scope.vmAndSvmList.length;i++){
			 if($scope.vmAndSvmList[i].vehMstr.vehNo === vehMstr.vehNo){
				 countVehNo = countVehNo +1;
			 }
		 }
		 
		 if(countVehNo > 0){
			$scope.alertToast("you already create the voucher of "+vehMstr.vehNo);
		 }else{
			 $scope.vm_svm.vehMstr = vehMstr
			 $scope.vehVoucherDBFlag = false;
			 $('div#vehVoucherDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					title: "Voucher Details for vehicle "+vehMstr.vehNo,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				
				$('div#vehVoucherDB').dialog('open');
		 }
			
	 }
	 
	 
	 
	 $scope.savePetrol = function(){
		 console.log("enter into savePetrol function = "+$scope.petrol);
		 if($scope.petrol){
			 $('#petDetId').removeAttr("disabled");
		 }else{
			 $('#petDetId').attr("disabled","disabled");
		 }
	 } 
	 
	 
	 $scope.openPetDet = function(){
		 console.log("enter into openPetDet function");
		 $scope.petExpDBFlag = false;
		 $('div#petExpDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Petrol Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#petExpDB').dialog('open');	
	 }
	 
	 
	 
	 $scope.submitPetDet = function(petDetForm){
		 console.log("enter into submitPetDet function");
		 if(petDetForm.$invalid){
			 $scope.alertToast("please fill correct Details of Petrol Expense");
		 }else{
			 $scope.petExpDBFlag = true;
			 $('div#petExpDB').dialog('close');	
			 $scope.faCar1.fcdExpType = "PETROL";
			 
			 for(var i=0;i<$scope.vm_svm.faCarList.length;i++){
				 if($scope.vm_svm.faCarList[i].fcdExpType === "PETROL"){
					 $scope.vm_svm.faCarList.splice(i,1);
					 $scope.alertToast("UPDATE  PETROL  EXP")	
				 }
			 }
			 
			 $scope.vm_svm.faCarList.push($scope.faCar1);
		 }
	 }
	 
	 
	 $scope.saveRepair = function(){
		 console.log("enter into saveRepair function");
		 if($scope.repair){
			 $('#repDetId').removeAttr("disabled");
		 }else{
			 $('#repDetId').attr("disabled","disabled");
		 }
	 }
	 
	 
	 
	 $scope.openRepDet = function(){
		 console.log("enter into openRepDet function");
		 $scope.repExpDBFlag = false;
		 $('div#repExpDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Repair Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#repExpDB').dialog('open');	
	 }
	 
	 
	 
	 $scope.submitRepDet = function(repDetForm){
		 console.log("enter into submitRepDet function");
		 if(repDetForm.$index){
			 $scope.alertToast("please fill correct details of Repair Expense");
		 }else{
			 $scope.repExpDBFlag = true;
			 $('div#repExpDB').dialog('close');	
			 $scope.faCar2.fcdExpType = "REPAIR";
			 
			 for(var i=0;i<$scope.vm_svm.faCarList.length;i++){
				 if($scope.vm_svm.faCarList[i].fcdExpType === "REPAIR"){
					 $scope.vm_svm.faCarList.splice(i,1);
					 $scope.alertToast("UPDATE  REPAIR  EXP")	
				 }
			 }
			 
			 $scope.vm_svm.faCarList.push($scope.faCar2);
		 }
	 }
	 
	 
	 $scope.saveRoadTax = function(){
		 console.log("enter into saveRoadTax function");
		 if($scope.roadTax){
			 $('#roadTaxDetId').removeAttr("disabled");
		 }else{
			 $('#roadTaxDetId').attr("disabled","disabled");
		 }
	 }
	 
	 
	 $scope.openRTaxDet = function(){
		 console.log("enter into openRTaxDet function");
		 $scope.rTaxDBFlag = false;
		 $('div#rTaxDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Road Tax Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#rTaxDB').dialog('open');	
	 }
	 
	 
	 $scope.submitRTaxDet = function(rTaxDetForm){
		 console.log("enter into submitRTaxDet function");
		 if(rTaxDetForm.$index){
			 $scope.alertToast("please fill correct details of RoadTax Expense");
		 }else{
			 $scope.rTaxDBFlag = true;
			 $('div#rTaxDB').dialog('close');	
			 $scope.faCar3.fcdExpType = "ROADTAX";
			 
			 for(var i=0;i<$scope.vm_svm.faCarList.length;i++){
				 if($scope.vm_svm.faCarList[i].fcdExpType === "ROADTAX"){
					 $scope.vm_svm.faCarList.splice(i,1);
					 $scope.alertToast("UPDATE  ROADTAX  EXP")	
				 }
			 }
			 
			 $scope.vm_svm.faCarList.push($scope.faCar3);
		 }
	 }
	 
	 
	 $scope.saveInsu = function(){
		 console.log("enter into saveInsu function");
		 if($scope.insurance){
			 $('#roadTaxDetId').removeAttr("disabled");
		 }else{
			 $('#roadTaxDetId').attr("disabled","disabled");
		 }
	 }
	 
	 
	 $scope.openInsuDet = function(){
		 console.log("enter into openInsuDet function");
		 $scope.insuDBFlag = false;
		 $('div#insuDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Insurance Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#insuDB').dialog('open');	
	 }
	 
	 
	 $scope.submitInsuDet = function(insuDetForm){
		 console.log("enter into submitInsuDet function");
		 if(insuDetForm.$invalid){
			 $scope.alertToast("please fill correct details of Insurance Expense");
		 }else{
			 $scope.insuDBFlag = true;
			 $('div#insuDB').dialog('close');	
			 $scope.faCar4.fcdExpType = "INSURANCE";
			 
			 for(var i=0;i<$scope.vm_svm.faCarList.length;i++){
				 if($scope.vm_svm.faCarList[i].fcdExpType === "INSURANCE"){
					 $scope.vm_svm.faCarList.splice(i,1);
					 $scope.alertToast("UPDATE  INSURANCE  EXP")	
				 }
			 }
			 
			 $scope.vm_svm.faCarList.push($scope.faCar4);
		 }
	 }
	 
	 
	 $scope.saveService = function(){
		 console.log("enter into saveService function");
		 if($scope.service){
			 $('#serDetId').removeAttr("disabled");
		 }else{
			 $('#serDetId').attr("disabled","disabled");
		 }
	 }
	 
	 
	 $scope.openSerDet = function(){
		 console.log("enter into openSerDet function");
		 $scope.serDBFlag = false;
		 $('div#serDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Service Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#serDB').dialog('open');	
	 }
	 
	 
	 $scope.submitSerDet = function(serDetForm){
		 console.log("enter into submitSerDet function");
		 if(serDetForm.$invalid){
			 $scope.alertToast("please fill correct details of Service Expense");
		 }else{
			 $scope.serDBFlag = true;
			 $('div#serDB').dialog('close');	
			 $scope.faCar5.fcdExpType = "SERVICE";
			 
			 for(var i=0;i<$scope.vm_svm.faCarList.length;i++){
				 if($scope.vm_svm.faCarList[i].fcdExpType === "SERVICE"){
					 $scope.vm_svm.faCarList.splice(i,1);
					 $scope.alertToast("UPDATE  SERVICE  EXP")	
				 }
			 }
			 
			 $scope.vm_svm.faCarList.push($scope.faCar5);
		 }
	 }
	 
	 
	 
	 $scope.saveOther = function(){
		 console.log("enter into saveOther function");
		 if($scope.other){
			 $('#othDetId').removeAttr("disabled");
		 }else{
			 $('#othDetId').attr("disabled","disabled");
		 }
	 }
	 
	 
	 
	 $scope.openOthDet = function(){
		 console.log("enter into openOthDet function");
		 $scope.othDBFlag = false;
		 $('div#othDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Other Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#othDB').dialog('open');	
	 }
	 
	 
	 $scope.submitOthDet = function(othDetForm){
		 console.log("enter into submitOthDet function");
		 if(othDetForm.$index){
			 $scope.alertToast("please fill correct details of Other Expense");
		 }else{
			 $scope.othDBFlag = true;
			 $('div#othDB').dialog('close');	
			 $scope.faCar6.fcdExpType = "OTHER";
			 
			 for(var i=0;i<$scope.vm_svm.faCarList.length;i++){
				 if($scope.vm_svm.faCarList[i].fcdExpType === "OTHER"){
					 $scope.vm_svm.faCarList.splice(i,1);
					 $scope.alertToast("UPDATE  OTHER  EXP")	
				 }
			 }
			 
			 $scope.vm_svm.faCarList.push($scope.faCar6);
		 }
	 }
	 
	 
	 
	 $scope.submitVMVoucher = function(vmVoucherForm){
		 console.log("enter into submitVMVoucher function = "+vmVoucherForm.$invalid);
		 if(vmVoucherForm.$invalid){
			 $scope.alertToast("please enter correct details of Vehicle Expense");
		 }else{
			 $scope.vehVoucherDBFlag = true;
			 $('div#vehVoucherDB').dialog('close');
			 $scope.vm_svm.sVehMstr.svmTotAmt = 0 ;
			 for(var i=0 ; i< $scope.vm_svm.faCarList.length ; i++){
				 $scope.vm_svm.sVehMstr.svmTotAmt =  $scope.vm_svm.sVehMstr.svmTotAmt + $scope.vm_svm.faCarList[i].fcdExpAmt;
			 }
			 $scope.addVMEDetails();
		 }
	 }
	 
	 
	 $scope.addVMEDetails = function(){
		 console.log("enter into addVMDetails function");
		 var response = $http.post($scope.projectName+'/addVMEDetails',$scope.vm_svm);
			response.success(function(data, status, headers, config){
				$scope.vm_svm = {};
				$scope.vm_svm.faCarList = [];
				$scope.vm_svm.sVehMstr = {};
				
				$scope.faCar1 = {};
				$scope.faCar2 = {};
				$scope.faCar3 = {};
				$scope.faCar4 = {};
				$scope.faCar5 = {};
				$scope.faCar6 = {};
				
				$scope.fetchVMEDetails();
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 $scope.fetchVMEDetails = function(){
		 console.log("enter into fetchVMEDetails function");
		 var response = $http.post($scope.projectName+'/fetchVMEDetails');
			response.success(function(data, status, headers, config){
				$scope.vmAndSvmList = data.list;
				
				temp=0;
				for(var i=0; i<data.list.length; i++){
					temp=temp+data.list[i].sVehMstr.svmTotAmt;
				}
				$scope.svmTotAmtSum=temp;
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 $scope.removeVME = function(index,vmAndSvm){
		 console.log("enter into removeVME function = "+index);
		 $scope.svmTotAmtSum=$scope.svmTotAmtSum-vmAndSvm.sVehMstr.svmTotAmt;
		 	var remIndex = {
					"index" : index
			};
			var response = $http.post($scope.projectName+'/removeVMEDetails',remIndex);
			response.success(function(data, status, headers, config){
				$scope.fetchVMEDetails();
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 
	 $scope.voucherSubmit = function(newVoucherForm){
		 console.log("enter into voucherSubmit function = "+newVoucherForm.$index);
		 if(newVoucherForm.$index){
			$scope.alertToast("please enter correct vouher details") 
		 }else{
			 console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }		 
	 }
	 
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
	 }
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
		 $('#saveId').attr("disabled","disabled");
		 var response = $http.post($scope.projectName+'/submitVehVoucher',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("success");
					$('#saveId').removeAttr("disabled");
					$scope.vs = {};
					$scope.vm_svm = {};
					$scope.vm_svm.faCarList = [];
					$scope.vm_svm.sVehMstr = {};
					$scope.vmAndSvmList = [];
					$scope.vehNo = "";
					
					$scope.faCar1 = {};
					$scope.faCar2 = {};
					$scope.faCar3 = {};
					$scope.faCar4 = {};
					$scope.faCar5 = {};
					$scope.faCar6 = {};
					$scope.getVoucherDetails();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 $scope.getTdsCode = function(){
			console.log("enter into getTdsCode function");
			var response = $http.post($scope.projectName+'/getTdsCode');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.fsMTdsList = data.list;
					$scope.fetchVMEDetails();
				}else{
					$scope.fetchVMEDetails();
					console.log(data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	}
	 
	 
	 
	 $scope.selectTds = function(){
			console.log("enter into selectTds function");
			$scope.tdsCodeFlag = false;
			$('div#tdsCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#tdsCodeDB').dialog('open');
	}
	 
	 
	 $scope.saveTdsCode = function(fsMTds){
			console.log("enter into saveTdsCode function");
			$scope.vs.tdsCode = fsMTds.faMfaCode;
			$scope.tdsCodeFlag = true;
			$('div#tdsCodeDB').dialog('close');
	} 
	 
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);