'use strict';

var app = angular.module('application');

app.controller('NewVehicleVendorCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("NewVehicleVendorCntlr Started");
	
	$scope.ownerList = [];
	$scope.brokerList = [];
	$scope.vtList = [];
	$scope.stateList = [];
	
	$scope.owner = {};
	$scope.broker = {};
	$scope.vehVenMstr = {};
	
	$scope.vehVenMstrFormFlag = false;
	
	$scope.ownerDBFlag = true;
	$scope.brokerDBFlag = true;
	$scope.vehTypeDBFlag = true;
	$scope.stateDBFlag = true;
	$scope.fitStateDBFlag = true;
	$scope.vehVenMstrDBFlag = true;

	$scope.getOwnerByNameCode = function(keyCode){		
		console.log("Enter into getOwnerByNameCode");
		if(keyCode === 8 || parseInt($scope.owner.ownName.length) < 2)
			return;
		
		var response = $http.post($scope.projectName+'/getOwnerByNameCode', $scope.owner.ownName);
		response.success(function(data, status, headers, config){
			console.log(data);
			if (data.result === "success"){ 				
				$scope.ownerList = data.ownerList;	
				$scope.openOwnerDB();
			}else{ 
				$scope.alertToast(data.msg);
				$scope.ownerList = [];
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getOwnerByNameCode");
		});
		console.log("Exit from getOwnerByNameCode()");
	}
	
	$scope.getBrokerByNameCode = function(keyCode){		
		console.log("Enter into getBrokerByNameCode");
		if(keyCode === 8 || parseInt($scope.broker.brkName.length) < 2)
			return;
		
		var response = $http.post($scope.projectName+'/getBrokerByNameCode', $scope.broker.brkName);
		response.success(function(data, status, headers, config){
			console.log(data);
			if (data.result === "success"){ 				
				$scope.brokerList = data.brokerList;	
				$scope.openBrokerDB();
			}else{ 
				$scope.alertToast(data.msg);
				$scope.brokerList = [];
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getOwnerByNameCode");
		});
		console.log("Exit from getOwnerByNameCode()");
	}
	
	$scope.openOwnerDB = function(){
		console.log("Entered into openOwnerDB");
		$scope.ownerDBFlag = false;
		$('div#ownerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Owner",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownerDBFlag = true;
			}
		});

		$('div#ownerDB').dialog('open');
	}
	
	$scope.saveOwner = function(owner){
		$scope.owner = owner;
		$('div#ownerDB').dialog('close');
	}
	
	/*$scope.closeOwnDb =  function(){
		console.log("OwnerName----->"+$scope.owner.ownName);
		$('div#ownerDB').dialog('close');
	}*/
	
	
	
	$scope.openBrokerDB = function(){
		console.log("Entered into openBrokerDB");
		$scope.brokerDBFlag = false;
		$('div#brokerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brokerDBFlag = true;
			}
		});

		$('div#brokerDB').dialog('open');
	}
	
	$scope.saveBroker = function(broker){
		$scope.broker = broker;
		$('div#brokerDB').dialog('close');
	}
	
	/*$scope.closeBrkDb =  function(){
		console.log("brokerName----->"+$scope.broker.brkName);
		$('div#brokerDB').dialog('close');
	}*/
	
	$scope.vehVenValid = function(vehVenValidForm){
		if(vehVenValidForm.$invalid){
			if (vehVenValidForm.ownerNm.$invalid) {
				$scope.alertToast("Please Enter Owner Name");
			} else if (vehVenValidForm.brokerNm.$invalid) {
				$scope.alertToast("Please Enter Broker Name");
			} else if (vehVenValidForm.rcNoNm.$invalid) {
				$scope.alertToast("Please Enter Rc No.");
			} 
		}else{
			console.log("final submission");
			var vehVendorService = {
					"ownId"		:	$scope.owner.ownId,
					"brkId"		:	$scope.broker.brkId,
					"vvRcNo"	:	$scope.vehVenMstr.vvRcNo
			}
			
			var response = $http.post($scope.projectName+'/vehVenValid', vehVendorService);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					$scope.alertToast("You can enter Vehicle mstr");
					$scope.vehVenMstrFormFlag = true;
					
					if ($scope.vtList.length < 1) {
						$scope.getVehicleType();
						console.log("getVehicleType() called: ")
					}else{
						console.log("getVehicleType() not called: ")
					}
					
					
				}else {
					$scope.alertToast(data.errorResult);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("vehVenValid Error: "+data);
			});
		}
	}
	
	$scope.openVehicleTypeDB = function(){
		$scope.vehTypeDBFlag = false;
    	$('div#vehTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Vehicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.vehTypeDBFlag = true;
		    }
			});
		
		$('div#vehTypeDB').dialog('open');
		}
	
	$scope.saveVehicleType = function(vt){
		$scope.vehVenMstr.vvType = vt.vtCode;
		$('div#vehTypeDB').dialog('close');
		
	}
	
	$scope.getVehicleType = function(){
		console.log("getVehicleType()");
		var response = $http.post($scope.projectName+'/getVehicleTypeCodeForOwner');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vtList = data.list;
				
				if ($scope.stateList.length < 1) {
					$scope.getStateList();
					console.log("getStateList() called: ")
				}else{
					console.log("getStateList() not called: ")
				}
				
				
			}else{
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.getStateList = function() {
		console.log("getStateList()");
		var response = $http.post($scope.projectName+'/getStateList');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stateList = data.stateList;
				/*for ( var i = 0; i < $scope.stateList.length; i++) {
					console.log($scope.stateList[i]);
				}*/
			}else{
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.openStateDB = function(){
		console.log("openStateDB()");
		$scope.stateDBFlag = false;
    	$('div#stateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "States",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.stateDBFlag = true;
		    }
			});
		
		$('div#stateDB').dialog('open');
		}
	
	$scope.saveState = function(state){
		console.log("saveState()");
		$scope.vehVenMstr.vvPerState = state;
		$('div#stateDB').dialog('close');
		
	}
	
	$scope.openFitStateDB = function(){
		console.log("openFitStateDB()");
		$scope.fitStateDBFlag = false;
    	$('div#fitStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "States",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.fitStateDBFlag = true;
		    }
			});
		
		$('div#fitStateDB').dialog('open');
		}
	
	$scope.saveFitState = function(state){
		console.log("saveFitState()");
		$scope.vehVenMstr.vvFitState = state;
		$('div#fitStateDB').dialog('close');
		
	}
	
	
	$scope.vehVenMstrSubmit = function(vehVenMstrForm){
		if(vehVenMstrForm.$invalid){
			if (vehVenMstrForm.vvDriverName.$invalid) {
				$scope.alertToast("Please Enter Driver Name");
			} else if (vehVenMstrForm.vvDriverDLNoName.$invalid) {
				$scope.alertToast("Please Enter Valid DL No");
			} else if (vehVenMstrForm.vvDriverMobNoName.$invalid) {
				$scope.alertToast("Please Enter Valid Mobile No");
			}
		}else{
			
			console.log("final submittion");
			$scope.vehVenMstrDBFlag = false;
			$('div#vehVenMstrDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Vehicle Detail",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.vehVenMstrDBFlag = true;
				}
			});
			$('div#vehVenMstrDB').dialog('open');
			
		}
	};
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#vehVenMstrDB').dialog('close');
	}
	
	
	$scope.saveVehVenMstr = function(){
		$('div#vehVenMstrDB').dialog('close');
		console.log("saveVehVenMstr()");
		
		var vehVendorService = {
				"ownId"			:	$scope.owner.ownId,
				"brkId"			:	$scope.broker.brkId,
				"vehVenMstr"	:	$scope.vehVenMstr
		}
		
		$('#saveVehVenMstrId').attr("disabled","disabled");
		
		var response = $http.post($scope.projectName+'/saveVehVenMstr', vehVendorService);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.alertToast("Success");
				
				$('#saveVehVenMstrId').removeAttr("disabled");
				
				//empty the resources
				
				$scope.owner = {};
				$scope.broker = {};
				$scope.vehVenMstr = {};
				
				$scope.vehVenMstrFormFlag = false;
				
			}else {
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("saveVehVenMstr Error: "+data);
		});
	}
	
	$('#vvDriverMobNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#vvDriverMobNoId').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});
	
	$('#rcNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvDriverNameId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvDriverDLNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPerNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvFitNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvFinanceBankId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPolicyNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPolicyCompId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvTransitPassNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("NewVehicleVendorCntlr Ended");
}]);