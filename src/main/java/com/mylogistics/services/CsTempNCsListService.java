package com.mylogistics.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtTemp;

@Service
public class CsTempNCsListService {

	private CashStmtTemp cashStmtTemp;
	
	private List<CashStmt> cashStmtList;

	public List<CashStmt> getCashStmtList() {
		return cashStmtList;
	}

	public void setCashStmtList(List<CashStmt> cashStmtList) {
		this.cashStmtList = cashStmtList;
	}

	public CashStmtTemp getCashStmtTemp() {
		return cashStmtTemp;
	}

	public void setCashStmtTemp(CashStmtTemp cashStmtTemp) {
		this.cashStmtTemp = cashStmtTemp;
	}
}
