package com.mylogistics.controller.webservices;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jws.soap.InitParam;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.servlet.http.HttpSession;
import javax.swing.text.DateFormatter;

import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.log.SysoCounter;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.werservice.ChallanWebServiceDao;
import com.mylogistics.DAO.werservice.WebServiceDAO;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.CnmtApp;
import com.mylogistics.model.webservice.VehicleApp;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.InvAndDateService;
import com.mylogistics.services.InvoiceService;
import com.mylogistics.services.InvoiceServiceImpl;
import com.mylogistics.services.ModelService;

@RestController
public class WebService {

	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;
	
	@Autowired
	private WebServiceDAO webServiceDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private ChallanWebServiceDao challanWebServiceDao;
	
	@Autowired
	private BranchDAO branchDAO;
	
	public OwnerDAO getOwnerDAO() {
		return ownerDAO;
	}

	public void setOwnerDAO(OwnerDAO ownerDAO) {
		this.ownerDAO = ownerDAO;
	}

	public BrokerDAO getBrokerDAO() {
		return brokerDAO;
	}

	public void setBrokerDAO(BrokerDAO brokerDAO) {
		this.brokerDAO = brokerDAO;
	}

	public StationDAO getStationDAO() {
		return stationDAO;
	}

	public void setStationDAO(StationDAO stationDAO) {
		this.stationDAO = stationDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public BranchStockLeafDetDAO getBranchStockLeafDetDAO() {
		return branchStockLeafDetDAO;
	}

	public void setBranchStockLeafDetDAO(BranchStockLeafDetDAO branchStockLeafDetDAO) {
		this.branchStockLeafDetDAO = branchStockLeafDetDAO;
	}

	public WebServiceDAO getWebServiceDAO() {
		return webServiceDAO;
	}

	public void setWebServiceDAO(WebServiceDAO webServiceDAO) {
		this.webServiceDAO = webServiceDAO;
	}

	public HttpSession getHttpSession() {
		return httpSession;
	}

	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}

	public ChallanWebServiceDao getChallanWebServiceDao() {
		return challanWebServiceDao;
	}

	public void setChallanWebServiceDao(ChallanWebServiceDao challanWebServiceDao) {
		this.challanWebServiceDao = challanWebServiceDao;
	}

	public ModelService getModelService() {
		return modelService;
	}

	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}

	public InvoiceService getInvoiceService() {
		return invoiceService;
	}

	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	private ModelService modelService = new ModelService();
	private InvoiceService invoiceService = new InvoiceServiceImpl();
	
	@RequestMapping(value = "/getOwnNameIdWs", method = RequestMethod.GET)
	public @ResponseBody Object getOwnNameIdWs() {
	
		List<Map<String, String>> ownList = ownerDAO.getOwnNCF();
		
		Map<String, Object> map = new HashMap<>();
		map.put("ownDet", ownList);
		
		return map;
	}
	
	@RequestMapping(value = "/getBrkNameIdWs", method = RequestMethod.GET)
	public @ResponseBody Object getBrkNameIdWs() {
	
		List<Map<String, String>> brkList = brokerDAO.getBrkNCF();
		
		Map<String, Object> map = new HashMap<>();
		map.put("brkDet", brkList);
		
		return map;
	}
	
	@RequestMapping(value = "/getStnNameIdWs", method = RequestMethod.GET)
	public @ResponseBody Object getStnNameIdWs() {
	
		List<Map<String, Object>> stnNameCodePinList = stationDAO.getStnNameCodePin();
		
		Map<String, Object> map = new HashMap<>();
		map.put("stnDet", stnNameCodePinList);
		
		return map;
	}
	
	@RequestMapping(value = "/loginWs", method = RequestMethod.POST)
	public @ResponseBody Object login(@RequestParam("userName") String userName, @RequestParam("password") String password) {
		Map<String, Object> map = new HashMap<>();
		map = getUserByUN(userName, password);
		String status = String.valueOf(map.get(ConstantsValues.STATUS));
		if(status.equalsIgnoreCase("success")){
			User user = (User) map.get("user");			
			map.remove("user");
			map.put("userName", user.getUserName());
			map.put("userCode", user.getUserCode());
			map.put("branchCode", user.getUserBranchCode());
			map.put("branchName", branchDAO.getBrNameByBrCode(user.getUserBranchCode()));			
		}
		System.err.println("Result : "+map);
		return map;
	}
	
	public Map getUserByUN(String userName, String password){
		System.out.println("getUserByUN()......");
		List<User> userList = new ArrayList<User>();
		Map<String, Object> map = new HashMap<>();
		try{
			//convert pass to md5
			String pass = null;
	        try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
			    byte[] digest = md.digest();
			    StringBuffer sb = new StringBuffer();
			    for (byte b : digest) {
		            sb.append(String.format("%02x", b & 0xff));
		        }
			    pass = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        userList = userDAO.getUserByUN(userName, password);
	        if (!userList.isEmpty()) {
					User user = userList.get(0);
				if (user.getPassword().equalsIgnoreCase(pass)) {				
					map.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					map.put("msg", "valid password");
					map.put("user", user);
				} else {
					map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					map.put("msg", "invalid password");
				}
			} else {
				map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				map.put("msg", "user not exist");
			}
		}catch(Exception e){
			System.out.println("Error in getUserByUN - WebService : "+e);
		}
		return map;
	}
	
	@RequestMapping(value = "/getFCNMTWs", method = RequestMethod.POST)
	public @ResponseBody Object getFCNMTWs(@RequestParam Map<String, Object> initParam) {
		System.out.println("getFCNMTWs().....");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{			
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				// CNMT Code List
				List<Map<String, Object>> cnmtCodeList = getCnmtCodeList(user.getUserBranchCode());
				//if(! cnmtCodeList.isEmpty())
					resultMap.put("cnmtCodeList", cnmtCodeList);
				// Consignee List
				List<Map<String, Object>> consigneeList = getConsigneeList();
				//if(! consigneeList.isEmpty())
					resultMap.put("consigneeList", consigneeList);
				// Customer List
				List<Map<String, Object>> customerList = getCustomerList(user.getUserBranchCode());
				//if(! customerList.isEmpty())
					resultMap.put("customerList", customerList);
				// Active Employee List
				List<Map<String, Object>> employeeList = getAllEmployeeList();
				//if(! employeeList.isEmpty())
					resultMap.put("employeeList", employeeList);
				// State List
				List<Map<String, Object>> stateList = getStateList();
				//if(! stateList.isEmpty())
					resultMap.put("stateList", stateList);
				// Delete All Inv
				deleteAllInv();
				List<InvAndDateService> cnmtInvList = getAllInvoice();
				//if(! cnmtInvList.isEmpty())
					resultMap.put("cnmtInvList", cnmtInvList);
				List<Map<String, Object>> stationDataList = getStationDataList();
				//if(! stationDataList.isEmpty())
					resultMap.put("stationDataList", stationDataList);
				List<String> ptList = getProjectTypeList();
				//if(! ptList.isEmpty())
					resultMap.put("ptList", ptList);
				List<Map<String, Object>> vtList = getVehicleTypeList();
				//if(! vtList.isEmpty())
					resultMap.put("vtList", vtList);
				List<Map<String, Object>> branchList = getBranchList();
				//if(! branchList.isEmpty())
					resultMap.put("branchList", branchList);
				
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/checkContWs", method = RequestMethod.POST)
	public @ResponseBody Object checkContWs(@RequestParam Map<String, Object> initParam) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String custCode = String.valueOf(initParam.get("custCode"));
				String cnmtDate = String.valueOf(initParam.get("cnmtDate"));
				String cnmtFromSt = String.valueOf(initParam.get("cnmtFromSt"));
				
				System.out.println("CustCode : "+custCode);
				System.out.println("CNMTDate : "+cnmtDate);
				System.out.println("CNMTFromSt : "+cnmtFromSt);
				
				Date cnmtDt = null;
				java.sql.Date sqlDate = null;				
				DateFormat formatter = null;
				try {
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					cnmtDt = new Date(formatter.parse(cnmtDate).getTime()); // birtDate is a string
					sqlDate = new java.sql.Date(cnmtDt.getTime());
				}catch (Exception e) {
					System.out.println("Exception :" + e);
				}
				System.out.println("Sql Date : "+sqlDate);
				Map<String, Object> regContList = getRegContList(custCode, sqlDate, cnmtFromSt);			
				Map<String, Object> dailyContList = getDailyContList(custCode, sqlDate, cnmtFromSt);
								
				List test = new ArrayList();
				
				if(! regContList.isEmpty()){				
					test.add(regContList);
				}else{					
					test.add(dailyContList);
				}
					
				
				if(regContList.isEmpty() && dailyContList.isEmpty()){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "No such Contract !");
				}else{
					resultMap.put("contList", test);
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				}
				/*
				if(! regContList.isEmpty()){
					resultMap.put("contList", regContList);
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					resultMap.put("msg", "Relevant contract exists ! Enter contract code");
				}else{
					List<Map<String, Object>> dailyContList = getDailyContList(custCode, sqlDate, cnmtFromSt);
					if(! regContList.isEmpty()){
						resultMap.put("contList", dailyContList);
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
						resultMap.put("msg", "Relevant contract exists ! Enter contract code");
					}else{
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
						resultMap.put("msg", "Relevant contact does not exists !");
					}
				}
				*/
			}
		}catch(Exception e){
			System.out.println("Error in checkContWs - WebService : "+e);
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/addRateForCnmtByWWs", method = RequestMethod.POST)
	public @ResponseBody Object addRateForCnmtByW(@RequestParam Map<String, Object> initParam) {
		System.out.println("addRateForCnmtByW().........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String cnmtToSta = String.valueOf(initParam.get("cnmtToSta"));
				String cnmtVehicleType = String.valueOf(initParam.get("cnmtVehicleType"));
				String contCode = String.valueOf(initParam.get("contCode"));
				String cnmtDt = String.valueOf(initParam.get("cnmtDt"));
				
				System.out.println("cnmtToSta : "+cnmtToSta);
				System.out.println("cnmtVehicleType : "+cnmtVehicleType);
				System.out.println("contCode : "+contCode);
				System.out.println("cnmtDt : "+cnmtDt);
				
				Date date = null;
				java.sql.Date sqlDate = null;				
				DateFormat formatter = null;
				try {
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					date = new Date(formatter.parse(cnmtDt).getTime()); // birtDate is a string
					sqlDate = new java.sql.Date(date.getTime());
					resultMap =  addRateForCnmtByW(contCode, cnmtToSta, cnmtVehicleType, sqlDate);
					if( !resultMap.isEmpty()){
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);						
					}else{
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record found !");
					}
				}catch (Exception e) {
					System.out.println("Exception :" + e);
				}
				System.out.println("Sql Date : "+sqlDate);				
			}
		}catch(Exception e){
			System.out.println("Error in addRateForCnmtByW - WebService : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/addRateForCnmtByQWs", method = RequestMethod.POST)
	public @ResponseBody Object addRateForCnmtByQ(@RequestParam Map<String, Object> initParam) {
		System.out.println("addRateForCnmtByW().........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String cnmtToSta = String.valueOf(initParam.get("cnmtToSta"));
				String cnmtProductType = String.valueOf(initParam.get("cnmtProductType"));
				String contCode = String.valueOf(initParam.get("contCode"));
				String cnmtDt = String.valueOf(initParam.get("cnmtDt"));
				
				System.out.println("cnmtToSta : "+cnmtToSta);
				System.out.println("cnmtProductType : "+cnmtProductType);
				System.out.println("contCode : "+contCode);
				System.out.println("cnmtDt : "+cnmtDt);
				
				Date date = null;
				java.sql.Date sqlDate = null;				
				DateFormat formatter = null;
				try {
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					date = new Date(formatter.parse(cnmtDt).getTime()); // birtDate is a string
					sqlDate = new java.sql.Date(date.getTime());
					resultMap =  addRateForCnmtByQ(contCode, cnmtToSta, cnmtProductType, sqlDate);
					if( !resultMap.isEmpty()){
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);						
					}else{
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record found !");
					}
				}catch (Exception e) {
					System.out.println("Exception :" + e);
				}
				System.out.println("Sql Date : "+sqlDate);				
			}
		}catch(Exception e){
			System.out.println("Error in addRateForCnmtByW - WebService : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/addRateForCnmtByKmWs", method = RequestMethod.POST)
	public @ResponseBody Object addRateForCnmtByKg(@RequestParam Map<String, Object> initParam) {
		System.out.println("addRateForCnmtByW().........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String cnmtKm = String.valueOf(initParam.get("cnmtKm"));
				String contCode = String.valueOf(initParam.get("contCode"));
				String cnmtState = String.valueOf(initParam.get("cnmtState"));
				
				
				System.out.println("cnmtKm : "+cnmtKm);			
				System.out.println("cnmtState : "+cnmtState);
				System.out.println("contCode : "+contCode);
				
				resultMap =  addRateForCnmtByKm(Integer.parseInt(cnmtKm), cnmtState, contCode);
				
				if( !resultMap.isEmpty()){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);						
				}else{
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}								
			}
		}catch(Exception e){
			System.out.println("Error in addRateForCnmtByW - WebService : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getAddRateFrCnmtWs", method = RequestMethod.POST)
	public @ResponseBody Object getAddRateFrCnmtWs(@RequestParam Map<String, Object> initParam) {
		System.out.println("addRateForCnmtByW().........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String toStn = String.valueOf(initParam.get("toStn"));
				String contCode = String.valueOf(initParam.get("contCode"));
				String vType = String.valueOf(initParam.get("vType"));
				String cnmtDt = String.valueOf(initParam.get("cnmtDt"));				
				
				System.out.println("toStn : "+toStn);			
				System.out.println("contCode : "+contCode);
				System.out.println("vType : "+vType);
				System.out.println("cnmtDt : "+cnmtDt);
				
				resultMap =  getAddRateFrCnmtWs(toStn, contCode, vType, cnmtDt);									
			}
		}catch(Exception e){
			System.out.println("Error in addRateForCnmtByW - WebService : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/submitCnmtWs", method = RequestMethod.POST)
	public @ResponseBody Object submitCnmtWs(@RequestParam Map<String, String> initParam) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String status = "";
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");								
				
				Boolean already = webServiceDAO.isCnmtAvailable(initParam.get("cnmtCode"));
				if(! already){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "CNMT already exists !");
					return resultMap;
				}
				CnmtApp cnmtApp = new CnmtApp();
				cnmtApp.setCnmtCode(initParam.get("cnmtCode"));
				cnmtApp.setCnmtCustCode(initParam.get("custCode"));
				cnmtApp.setCnmtVOG(Double.parseDouble(initParam.get("cnmtVOG")));
				
				java.util.Date cnmtDt = null;
				//java.util.Date cnmtDtOfDly1 = null;
				java.util.Date cnmtInvoiceDt = null;
				
				java.sql.Date cnmtDate = null;
				//java.sql.Date cnmtDtOfDly = null;
				java.sql.Date cnmtInvoiceDate = null;
				
				DateFormat formatter = null;
				
				try {
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					String d1;
					String d2;
					d1 = initParam.get("cnmtDt");
					d2 = initParam.get("cnmtInvoiceDt");
					if(d1 != null){
						cnmtDt = new Date(formatter.parse(d1).getTime()); // birtDate is a string
						cnmtDate = new java.sql.Date(cnmtDt.getTime());
					}
					if(d2 != null){
						cnmtInvoiceDt = new Date(formatter.parse(d2).getTime()); // birtDate is a string					
						cnmtInvoiceDate = new java.sql.Date(cnmtInvoiceDt.getTime());
					}
				}catch (Exception e) {
					System.out.println("Exception in converting date :" + e);
				}
				
				cnmtApp.setCnmtDt(cnmtDate);							
				cnmtApp.setCnmtFromStCode(initParam.get("cnmtFromSt"));
				cnmtApp.setCnmtToStCode(initParam.get("cnmtToSt"));
				cnmtApp.setCnmtConsignorCode(initParam.get("cnmtConsignor"));
				cnmtApp.setCnmtConsigneeCode(initParam.get("cnmtConsignee"));
				cnmtApp.setCnmtVehicleTypeCode(initParam.get("cnmtVehicleType"));
				cnmtApp.setCnmtProductType(initParam.get("cnmtProductType"));
				cnmtApp.setCnmtActualWt(Double.parseDouble(initParam.get("cnmtActualWt")));
				cnmtApp.setCnmtGuaranteeWt(Double.parseDouble(initParam.get("cnmtGuaranteeWt")));				
				if(! String.valueOf(initParam.get("cnmtKm")).equalsIgnoreCase(""))
					cnmtApp.setCnmtKm(Double.parseDouble(initParam.get("cnmtKm")));				
				if(! String.valueOf(initParam.get("cnmtState")).equalsIgnoreCase(""))
					cnmtApp.setCnmtStateCode(initParam.get("cnmtState"));
				if(! String.valueOf(initParam.get("cnmtPayAt")).equalsIgnoreCase(""))
					cnmtApp.setCnmtPayAtCode(initParam.get("cnmtPayAt"));
				if(! String.valueOf(initParam.get("cnmtBillAt")).equalsIgnoreCase(""))
					cnmtApp.setCnmtBillAtCode(initParam.get("cnmtBillAt"));
				cnmtApp.setCnmtRate(Double.parseDouble(initParam.get("cnmtRate"))/1000);
				cnmtApp.setCnmtNoOfPkg(Integer.parseInt(initParam.get("cnmtNoOfPkg")));
				cnmtApp.setCnmtFreight(Double.parseDouble(initParam.get("cnmtFreight")));
				cnmtApp.setCnmtTOT(Double.parseDouble(initParam.get("cnmtTOT")));
				cnmtApp.setCnmtEmpCode(initParam.get("cnmtEmpCode"));
				if(! String.valueOf(initParam.get("cnmtExtraExp")).equalsIgnoreCase(""))
					cnmtApp.setCnmtExtraExp(Double.parseDouble(initParam.get("cnmtExtraExp")));
				cnmtApp.setCnmtContCode(initParam.get("contCode"));
				cnmtApp.setCnmtDtdDly(initParam.get("dtdDly"));
				cnmtApp.setCnmtDc(initParam.get("dc"));
				cnmtApp.setCnmtCostGrade(initParam.get("costGrade"));
				
				String cnmtInvNo = initParam.get("cnmtInvoiceNo");
				String cnmtInvDate = initParam.get("cnmtInvoiceDate");
				
				if(cnmtInvNo != null && !cnmtInvNo.equalsIgnoreCase(""))
					cnmtInvNo = cnmtInvNo.substring(0, cnmtInvNo.lastIndexOf(":"));
				if(cnmtInvDate != null && !cnmtInvNo.equalsIgnoreCase(""))
					cnmtInvDate = cnmtInvDate.substring(0, cnmtInvDate.lastIndexOf(":"));
				cnmtApp.setCnmtInvoiceNo(cnmtInvNo);
				cnmtApp.setCnmtInvoiceDate(cnmtInvDate);
				//cnmtApp.setCnmtInvoiceDt(cnmtInvoiceDate);
												
				cnmtApp.setCnmtUserCode(user.getUserCode());
				cnmtApp.setCnmtBCode(user.getUserBranchCode());
				cnmtApp.setCnmtBranchCode(user.getUserBranchCode());
				
				cnmtApp.setCnmtIsPending(true);
				cnmtApp.setCnmtIsCancel(false);
				
				resultMap = webServiceDAO.saveCnmtToDB(cnmtApp);
				status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
				if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					resultMap.put("msg", "CNMT is saved !");
				}else if(status.equalsIgnoreCase(ConstantsValues.ERROR)){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					resultMap.put("msg", "CNMT is not saved !");
				}else{
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Something is going wrong !");
				}
			}
		}catch(Exception e){
			System.out.println("Error in submitCnmtWs1 - WebService : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/checkAvaCnmtCode", method = RequestMethod.POST)
	public @ResponseBody Object checkAvaCnmtCode(@RequestParam Map<String, String> initParam) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String cnmtCount = String.valueOf(initParam.get("cnmtCode"));
		resultMap = webServiceDAO.checkAvaCnmtCode(cnmtCount);
		return resultMap;
	}
	
	public List<Map<String, Object>> getCnmtCodeList(String branchCode){		
		List<Map<String, Object>> cnmtCodeList = webServiceDAO.getCodeList(branchCode,"cnmt");
		System.out.println("CNMTCode List Size : "+cnmtCodeList.size());		
		return cnmtCodeList;
	}
	
	public List<Map<String, Object>> getConsigneeList(){
		List<Map<String, Object>> consigneeList = webServiceDAO.getConsigneeList();
		System.out.println("Consignee List Size : "+ consigneeList.size());
		return consigneeList;
	}
	
	public List<Map<String, Object>> getCustomerList(String branchCode){
		List<Map<String, Object>> customerList = webServiceDAO.getCustomerList();
		System.out.println("Customer List Size : "+ customerList.size());
		return customerList;
	}
	
	public List<Map<String, Object>> getAllEmployeeList(){
		List<Map<String, Object>> employeeList = webServiceDAO.getAllEmployeeList();		
		System.out.println("Employee List Size : "+employeeList.size());
		return employeeList;
	}
	
	public List<Map<String, Object>> getStateList(){
		List<Map<String, Object>> stateList = webServiceDAO.getStateList();
		System.out.println("State List Size : "+stateList.size());
		return stateList;
	}
	
	public void deleteAllInv(){
		InvoiceService invoiceService = new InvoiceServiceImpl();		
		invoiceService.deleteAll();
		System.out.println("Delete All Inv");
	}
	
	public List<InvAndDateService> getAllInvoice(){
		InvoiceService invoiceService = new InvoiceServiceImpl();
		List<InvAndDateService> cnmtInvList = invoiceService.getAllInvoice();
		System.out.println("CnmtInv List Size : "+cnmtInvList.size());
		return cnmtInvList;
	}
	
	public List<Map<String, Object>> getStationDataList(){
		List<Map<String, Object>> stationDataList = webServiceDAO.getStationDataList();
		System.out.println("StationData List Size : "+stationDataList.size());
		return stationDataList;
	}
	
	public List<String> getProjectTypeList(){
		List<String> ptList = webServiceDAO.getProductTypeList();
		System.out.println("ProductType List Size : "+ptList.size());
		return ptList;
	}
	
	public List<Map<String, Object>> getVehicleTypeList(){
		List<Map<String, Object>> vtList = webServiceDAO.getVehicleTypeList();
		System.out.println("VehicleType List Size : "+vtList.size());
		return vtList;
	}
	
	public List<Map<String, Object>> getBranchList(){
		List<Map<String, Object>> branchList = webServiceDAO.getBranchList();
		System.out.println("Branch List size : "+branchList.size());
		return branchList;
	}
	public Map<String, Object> getRegContList(String custCode, Date cnmtDate, String cnmtFromSt){
		Map<String, Object> regContList = webServiceDAO.getRegContList(custCode, cnmtDate, cnmtFromSt);
		System.out.println("Regular Contract List Size : "+regContList.size());
		return regContList;
	}
	
	public Map<String, Object> getDailyContList(String custCode, Date cnmtDate, String cnmtFromSt){
		Map<String, Object> dailyContList = webServiceDAO.getDailyContList(custCode, cnmtDate, cnmtFromSt);
		System.out.println("Daily Contract List Size : "+dailyContList.size());
		return dailyContList;
	}
	
	public Map addRateForCnmtByW(String contCode, String cnmtToSta, String cnmtVehicleType, Date cnmtDate) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(contCode.contains("reg")){										
			List<ContToStn> contToStnList = webServiceDAO.getContToStnRateCnmtW(contCode, cnmtToSta, cnmtVehicleType, cnmtDate);
			double rate = 0.0;
			double garWt = 0.0;
			for(ContToStn contToStn : contToStnList){
				rate = contToStn.getCtsRate();
				garWt = contToStn.getCtsToWt();
			}
			double actualRate = rate;
			resultMap.put("rate", actualRate);
			resultMap.put("garWt", garWt);								
		}else if(contCode.contains("dly")){
			List<ContToStn> contToStnList = webServiceDAO.getContToStnRateCnmtW(contCode, cnmtToSta, cnmtVehicleType, cnmtDate);
			double rate = 0.0;
			double garWt = 0.0;
			for(ContToStn contToStn : contToStnList){
				rate = contToStn.getCtsRate();
				garWt = contToStn.getCtsToWt();
			}
			double actualRate = rate;
			resultMap.put("rate", actualRate);
			resultMap.put("garWt", garWt);				
		}
		return resultMap;
	}
	
	public Map addRateForCnmtByQ(String contCode, String cnmtToSta, String cnmtProductType, Date cnmtDate) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(contCode.contains("reg")){										
			List<ContToStn> contToStnList = webServiceDAO.getContToStnRateCnmtQ(contCode, cnmtToSta, cnmtProductType, cnmtDate);
			double rate = 0.0;
			double garWt = 0.0;
			for(ContToStn contToStn : contToStnList){
				rate = contToStn.getCtsRate();
				garWt = contToStn.getCtsToWt();
				System.out.println("GarWt : "+contToStn.getCtsToWt());
			}
			double actualRate = rate;
			resultMap.put("rate", actualRate);
			resultMap.put("garWt", garWt);								
		}else if(contCode.contains("dly")){
			List<ContToStn> contToStnList = webServiceDAO.getContToStnRateCnmtQ(contCode, cnmtToSta, cnmtProductType, cnmtDate);
			double rate = 0.0;
			double garWt = 0.0;
			for(ContToStn contToStn : contToStnList){
				rate = contToStn.getCtsRate();
				garWt = contToStn.getCtsToWt();
				System.out.println("GarWt : "+contToStn.getCtsToWt());
			}
			double actualRate = rate;
			resultMap.put("rate", actualRate);
			resultMap.put("garWt", garWt);				
		}
		return resultMap;
	}
	
	public Map addRateForCnmtByKm(Integer cnmtKm, String cnmtState, String contCode) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(contCode.contains("reg")){										
			//List<RegularContract> rgContList = webServiceDAO.getRegContractData(contCode);
			//RegularContract rgContract = rgContList.get(0);
			List<RateByKm> rbkmList = webServiceDAO.getRbkmRate(cnmtState, contCode);
			double rate = 0.0;
			String vehTypeCode = "";
			if(!rbkmList.isEmpty()){
				for(int i=0;i<rbkmList.size();i++){
					if(rbkmList.get(i).getRbkmFromKm() <= cnmtKm && rbkmList.get(i).getRbkmToKm() >= cnmtKm){
						rate = rbkmList.get(i).getRbkmRate();
						vehTypeCode = rbkmList.get(i).getRbkmVehicleType();
					}
				}
			}
			double actualRate = cnmtKm * rate;
			resultMap.put("rate", actualRate);
			resultMap.put("vtCode", vehTypeCode);									
		}else if(contCode.contains("dly")){
			//List<DailyContract> rgContList = webServiceDAO.getDailyContractData(contCode);
			//DailyContract dlyCont = rgContList.get(0);
			List<RateByKm> rbkmList = webServiceDAO.getRbkmRate(cnmtState, contCode);
			double rate = 0.0;
			String vehTypeCode = "";
			for(int i=0;i<rbkmList.size();i++){
				if(rbkmList.get(i).getRbkmFromKm() <= cnmtKm && rbkmList.get(i).getRbkmToKm() >= cnmtKm){
					rate = rbkmList.get(i).getRbkmRate();
					vehTypeCode = rbkmList.get(i).getRbkmVehicleType();
				}
			}	
			double actualRate = cnmtKm * rate;
			resultMap.put("rate", actualRate);
			resultMap.put("vtCode", vehTypeCode);	
		}
		return resultMap;
	}
	
	public Map getAddRateFrCnmtWs(String toStn, String contCode, String vType, String cnmtDt){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Date date = null;
		java.sql.Date sqlDate = null;				
		DateFormat formatter = null;
		try {
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			date = new Date(formatter.parse(cnmtDt).getTime()); // birtDate is a string
			sqlDate = new java.sql.Date(date.getTime());			
		}catch (Exception e) {
			System.out.println("Exception :" + e);
		}		
		
		if(contCode.contains("reg")){			
			List<ContToStn> ContToStnList = webServiceDAO.getContToStnRateCnmtW(contCode, toStn, vType, sqlDate);
			double addRate = 0.0;	
			for(int i=0;i<ContToStnList.size();i++){
				addRate = addRate + ContToStnList.get(i).getCtsAdditionalRate();
			}
			resultMap.put("aRate", addRate);
			resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else if(contCode.contains("dly")){			
			List<ContToStn> ContToStnList = webServiceDAO.getContToStnRateCnmtW(contCode, toStn, vType, sqlDate);
			double addRate = 0.0;
			if(!ContToStnList.isEmpty()){			
				for(int i=0;i<ContToStnList.size();i++){
					addRate = addRate + ContToStnList.get(i).getCtsAdditionalRate();
				}
			}	
			resultMap.put("aRate",addRate);
			resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		return resultMap;
	}
	
	public  double getCnmtDaysLast(User user) {
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar =Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		Date endDate = new Date(calendar.getTime().getTime());
		double cnmtLeft=0;
		double cnmtPerDayUsed = webServiceDAO.getLastMonthPerDayUsed(date,endDate,"cnmt");
		double daysLeft = 0;
		if(cnmtPerDayUsed>0){
			cnmtLeft = branchStockLeafDetDAO.getLeftStationary(user.getUserBranchCode(),"cnmt");
			daysLeft=cnmtLeft/cnmtPerDayUsed;
		}

		return daysLeft;
	}
	
	public double getAvgMonthlyUsageForCnmt() {

		System.out.println("------Enter into getAvgMonthlyUsage function");

		Date todayDate = new Date(new java.util.Date().getTime());
		Date firstEntryDate = webServiceDAO.getFirstEntryDate("cnmt");
		System.out.println("**************firstEntryDate = "+firstEntryDate);
		double average=0.00;
		if(firstEntryDate != null){
			Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = new Date(calendar.getTime().getTime());

			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));


			double cnmtForOneMnth = webServiceDAO.getNoOfStationary(todayDate, oneMonthPrevDate,"cnmt");
			double cnmtForScndMnth = webServiceDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate,"cnmt");
			double cnmtForThirdMnth = webServiceDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate,"cnmt");
			double cnmtBwFrstAndScndMnth = webServiceDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate,"cnmt");
			double cnmtBwFrstAndOneMnth = webServiceDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate,"cnmt");
			double cnmtBwTodayAndFrstDate = webServiceDAO.getNoOfStationary(todayDate, firstEntryDate,"cnmt");


			if(days>=90){
				average = ((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + cnmtForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		

				average = (((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + (cnmtBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){

				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				average = (((cnmtForOneMnth*2) +  (cnmtBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				if(daysBwFrstAndToday == 0){
					average = cnmtBwTodayAndFrstDate*30;
				}else{
					average = ((cnmtBwTodayAndFrstDate/daysBwFrstAndToday)*30);
				}
			}
		}	

		return average;
	}	
	
}
