package com.mylogistics.DAO.werservice;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.ChallanApp;
import com.mylogistics.model.webservice.ChallanDTApp;
import com.mylogistics.model.webservice.CnmtChlnApp;

public interface ChallanWebServiceDao {
	
	public Map<String, Object> getCnmtCodeForChallan(String  branchCode);
	public String getBranchNameByCode(String branchCode);
	public Map<String, Object> getCnmtDetailForChallan(String cnmtCode);
	public Map<String, Object> getVehicleNoList();
	public Map<String, Object> getChlnCodeForChallan(String branchCode);	
	public List<Cnmt> getCnmtByCnmtCode(String cnmtCode, String type);
	public List<Integer> getCnmt_ChallanByCnmtId(int cnmtId);
	public List<String> getChallanToStationById(int chlnId);
	public String saveChallanAndChallanDTAndCnmtChlnApp(ChallanApp challanApp, ChallanDTApp challanDTApp, List<CnmtChlnApp> cnmtChlnApp);
	public Map<String, Object> getVehicleForChln();
	public Map<String, Object> getChlnDtByVehicleNo(String vehicleNo);
	public Map<String, Object> getVehicleDtWe(String vehicleNo);
	public Map<String, Object> uploadOwnPanDecImage(OwnerImg ownerImg, Owner owner);
	public Map<String, Object> uploadBrkPanDecImage(BrokerImg brokerImg, Broker broker);
	public Owner getOwnerById(Integer ownId);
	public Broker getBrokerById(Integer brkId);
	public List<String> getPendingChallanCode();
	public Map<String, Object> getPendingChallanDetail(String chlnCode);
	public Map<String, Object> updateChlnApp(Challan challan, List<Map<String, Object>> cnmtList);
	public List<Customer> getCustomerListByCnmtList(List<Map<String, Object>> cnmtList);	
	public Map<String, Object> uploadImage(String code, String type, String ownBrkId, String panName,String panNo,String panDOB,String panDate,MultipartFile panImg, MultipartFile decImg, User user);
	public Boolean isChlnAvailable(String chlnCode);
}