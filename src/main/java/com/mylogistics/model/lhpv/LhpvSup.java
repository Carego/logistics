package com.mylogistics.model.lhpv;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mylogistics.model.Challan;

@Entity
@Table(name = "lhpvsup")
public class LhpvSup {

	@Id
	@Column(name="lspId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int lspId;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonBackReference(value = "lslb")
	@JoinColumn(name = "lsId") 
	private LhpvStatus lhpvStatus;
	
	//One To One
	@JsonIgnore
	@JsonManagedReference(value = "lspchln")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvsup_challan",
	joinColumns=@JoinColumn(name="lspId"),
	inverseJoinColumns=@JoinColumn(name="chlnId"))
	private Challan challan;
	
	@Column(name="lspNo")
	private int lspNo;
	
	@Column(name="lspDt")
	private Date lspDt;
	
	@Column(name="lspIsClose" , columnDefinition="boolean default false")
	private boolean lspIsClose;
	
	@Column(name="lspBrhCode")
	private String lspBrhCode;
	
	@Column(name="lspBrkOwn")
	private String lspBrkOwn;
	
	@Column(name="lspPayMethod")
	private char lspPayMethod;
	
	@Column(name="lspBankCode")
	private String lspBankCode;
	
	@Column(name="lspChqType")
	private char lspChqType;
	
	@Column(name="lspChqNo")
	private String lspChqNo;
	
	@Column(name="lspWtShrtgCR" , columnDefinition="double default 0")
	private double lspWtShrtgCR;
	
	@Column(name="lspDrRcvrWtCR" , columnDefinition="double default 0")
	private double lspDrRcvrWtCR;
	
	@Column(name="lspLateAckCR" , columnDefinition="double default 0")
	private double lspLateAckCR;
	
	@Column(name="lspLateDelCR" , columnDefinition="double default 0")
	private double lspLateDelCR;
	
	@Column(name="lspOthExtKmP"  , columnDefinition="double default 0")
	private double lspOthExtKmP;
	
	@Column(name="lspOthOvrHgtP" , columnDefinition="double default 0")
	private double lspOthOvrHgtP;
	
	@Column(name="lspOthPnltyP" , columnDefinition="double default 0")
	private double lspOthPnltyP;
	
	@Column(name="lspOthMisctP" , columnDefinition="double default 0")
	private double lspOthMiscP;
	
	@Column(name="lspUnLoadingP" , columnDefinition="double default 0")
	private double lspUnLoadingP;
	
	@Column(name="lspUnpDetP" , columnDefinition="double default 0")
	private double lspUnpDetP;
	
	@Column(name="lspTotPayAmt" , columnDefinition="double default 0")
	private double lspTotPayAmt;
	
	@Column(name="lspTotRcvrAmt" , columnDefinition="double default 0")
	private double lspTotRcvrAmt;
	
	@Column(name="lspFinalTot" , columnDefinition="double default 0")
	private double lspFinalTot;
	
	@Column(name="lspDesc")
	private String lspDesc;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Transient
	private String sheetNo;

	public int getLspId() {
		return lspId;
	}

	public void setLspId(int lspId) {
		this.lspId = lspId;
	}

	public LhpvStatus getLhpvStatus() {
		return lhpvStatus;
	}

	public void setLhpvStatus(LhpvStatus lhpvStatus) {
		this.lhpvStatus = lhpvStatus;
	}

	public int getLspNo() {
		return lspNo;
	}

	public void setLspNo(int lspNo) {
		this.lspNo = lspNo;
	}

	public Date getLspDt() {
		return lspDt;
	}

	public void setLspDt(Date lspDt) {
		this.lspDt = lspDt;
	}

	public boolean isLspIsClose() {
		return lspIsClose;
	}

	public void setLspIsClose(boolean lspIsClose) {
		this.lspIsClose = lspIsClose;
	}

	public String getLspBrkOwn() {
		return lspBrkOwn;
	}

	public void setLspBrkOwn(String lspBrkOwn) {
		this.lspBrkOwn = lspBrkOwn;
	}

	public double getLspWtShrtgCR() {
		return lspWtShrtgCR;
	}

	public void setLspWtShrtgCR(double lspWtShrtgCR) {
		this.lspWtShrtgCR = lspWtShrtgCR;
	}

	public double getLspLateAckCR() {
		return lspLateAckCR;
	}

	public void setLspLateAckCR(double lspLateAckCR) {
		this.lspLateAckCR = lspLateAckCR;
	}

	public double getLspLateDelCR() {
		return lspLateDelCR;
	}

	public void setLspLateDelCR(double lspLateDelCR) {
		this.lspLateDelCR = lspLateDelCR;
	}

	public double getLspOthExtKmP() {
		return lspOthExtKmP;
	}

	public void setLspOthExtKmP(double lspOthExtKmP) {
		this.lspOthExtKmP = lspOthExtKmP;
	}

	public double getLspOthOvrHgtP() {
		return lspOthOvrHgtP;
	}

	public void setLspOthOvrHgtP(double lspOthOvrHgtP) {
		this.lspOthOvrHgtP = lspOthOvrHgtP;
	}

	public double getLspOthPnltyP() {
		return lspOthPnltyP;
	}

	public void setLspOthPnltyP(double lspOthPnltyP) {
		this.lspOthPnltyP = lspOthPnltyP;
	}

	public double getLspOthMiscP() {
		return lspOthMiscP;
	}

	public void setLspOthMiscP(double lspOthMiscP) {
		this.lspOthMiscP = lspOthMiscP;
	}

	public double getLspUnLoadingP() {
		return lspUnLoadingP;
	}

	public void setLspUnLoadingP(double lspUnLoadingP) {
		this.lspUnLoadingP = lspUnLoadingP;
	}

	public double getLspUnpDetP() {
		return lspUnpDetP;
	}

	public void setLspUnpDetP(double lspUnpDetP) {
		this.lspUnpDetP = lspUnpDetP;
	}

	public double getLspTotPayAmt() {
		return lspTotPayAmt;
	}

	public void setLspTotPayAmt(double lspTotPayAmt) {
		this.lspTotPayAmt = lspTotPayAmt;
	}

	public double getLspTotRcvrAmt() {
		return lspTotRcvrAmt;
	}

	public void setLspTotRcvrAmt(double lspTotRcvrAmt) {
		this.lspTotRcvrAmt = lspTotRcvrAmt;
	}

	public double getLspFinalTot() {
		return lspFinalTot;
	}

	public void setLspFinalTot(double lspFinalTot) {
		this.lspFinalTot = lspFinalTot;
	}

	public String getLspDesc() {
		return lspDesc;
	}

	public void setLspDesc(String lspDesc) {
		this.lspDesc = lspDesc;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public Challan getChallan() {
		return challan;
	}

	public void setChallan(Challan challan) {
		this.challan = challan;
	}

	public char getLspPayMethod() {
		return lspPayMethod;
	}

	public void setLspPayMethod(char lspPayMethod) {
		this.lspPayMethod = lspPayMethod;
	}

	public String getLspBankCode() {
		return lspBankCode;
	}

	public void setLspBankCode(String lspBankCode) {
		this.lspBankCode = lspBankCode;
	}

	public char getLspChqType() {
		return lspChqType;
	}

	public void setLspChqType(char lspChqType) {
		this.lspChqType = lspChqType;
	}

	public String getLspChqNo() {
		return lspChqNo;
	}

	public void setLspChqNo(String lspChqNo) {
		this.lspChqNo = lspChqNo;
	}

	public String getLspBrhCode() {
		return lspBrhCode;
	}

	public void setLspBrhCode(String lspBrhCode) {
		this.lspBrhCode = lspBrhCode;
	}

	public double getLspDrRcvrWtCR() {
		return lspDrRcvrWtCR;
	}

	public void setLspDrRcvrWtCR(double lspDrRcvrWtCR) {
		this.lspDrRcvrWtCR = lspDrRcvrWtCR;
	}

	public String getSheetNo() {
		return sheetNo;
	}

	public void setSheetNo(String sheetNo) {
		this.sheetNo = sheetNo;
	}
	
	
}
