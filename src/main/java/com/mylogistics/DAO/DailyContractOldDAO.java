package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.DailyContractOld;

public interface DailyContractOldDAO {

	public int saveDlyContOld(DailyContractOld dailyContractOld);
	
	//Sanjana: gets dailyContractOld data of particular contractCode
	public List<DailyContractOld> getOldDailyContract(String contractCode);
}
