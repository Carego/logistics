package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.ChqPayVoucherDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.CPVoucherService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class ChqPayVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private ChqPayVoucherDAO chqPayVoucherDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	private static Logger logger = Logger.getLogger(ChqPayVoucherCntlr.class);
	
	
	@RequestMapping(value = "/getVDetFrCPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrCPV() {  
		System.out.println("Enter into getVDetFrCPV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getAllFaCodeFrCPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllFaCodeFrCPV() {  
		System.out.println("Enter into getAllFaCodeFrCPV---->");
		Map<String,Object> map = new HashMap<String,Object>();
		//List<FAMaster> faMList = faMasterDAO.getAllFaMstr();
		List<FAMaster> faMList = faMasterDAO.getAllFAM();
		List<FAMaster> faTList = faMasterDAO.getAllTdsFAM();
		if(!faMList.isEmpty()){
			for(int i=0;i<faMList.size();i++){
				if(faMList.get(i).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
					faMList.remove(i);
					i = i-1;
				}
			}
			map.put("faMList",faMList);
			map.put("faTList",faTList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/getAllFaCodeFrCPVByFaName" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllFaCodeFrCPVByFaName(@RequestBody String faNameCode){ 
		logger.info("Enter into getAllFaCodeFrCPVByFaCode() : faNameCode = "+faNameCode);
		Map<String,Object> map = new HashMap<String,Object>();
		List<FAMaster> faMList = faMasterDAO.getAllFaMstrByFaNameOrCode(faNameCode);
		
		List<FAMaster> faTList = faMasterDAO.getAllTdsFAM();		
		
		if(!faMList.isEmpty()){			
			map.put("faMList",faMList);
			map.put("faTList",faTList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from getAllFaCodeFrCPVByFaCode");
		return map;
	}	
	
	@RequestMapping(value = "/getAllTdsForChqPay" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllTdsForChqPay(){ 
		logger.info("Enter into getAllTdsForChqPay()");
		Map<String,Object> map = new HashMap<String,Object>();		
		List<FAMaster> faTList = faMasterDAO.getAllTdsFAM();		
		
		if(! faTList.isEmpty()){		
			map.put("faTList",faTList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from getAllTdsForChqPay");
		return map;
	}	
	
	
	@RequestMapping(value = "/getCustBrFrCPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getCustBrFrCPV(@RequestBody String custFaCode) {  
		System.out.println("Enter into getCustBrFrCPV---->");
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> custBrList = customerDAO.getAllCustBrCode(custFaCode);
		map.put("list",custBrList);
		return map;
	}
	
	
	@RequestMapping(value = "/getEmpBrFrCPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getEmpBrFrCPV(@RequestBody String empFaCode) {  
		System.out.println("Enter into getEmpBrFrCPV---->");
		Map<String,Object> map = new HashMap<String,Object>();
		//List<String> custBrList = customerDAO.getAllCustBrCode(custFaCode);
		List<String> empBrList = employeeDAO.getAllEmpBrh(empFaCode);
		map.put("list",empBrList);
		return map;
	}
	
	
	@RequestMapping(value = "/getAllBrFrCPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllBrFrCPV() {  
		System.out.println("Enter into getAllBrFrCPV---->");
		Map<String,Object> map = new HashMap<String,Object>();
		//List<String> custBrList = customerDAO.getAllCustBrCode(custFaCode);
		List<String> brhList = new ArrayList<>();
		List<Branch> brList = branchDAO.getAllActiveBranches();
		if(!brList.isEmpty()){
			for(int i=0;i<brList.size();i++){
				brhList.add(brList.get(i).getBranchFaCode());
			}
			map.put("list",brhList);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitCPVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object submitCPVoucher(@RequestBody CPVoucherService cpVoucherService) {  
		System.out.println("Enter into submitCPVoucher---->");
		Map<String,Object> map = new HashMap<String,Object>();
		VoucherService voucherService = cpVoucherService.getVoucherService();
		List<Map<String,Object>> faList = cpVoucherService.getFaList();
		System.out.println("size of faList = "+faList.size());
		
		for(int i=0;i<faList.size();i++){
			Map<String,Object> clientMap = faList.get(i);
			System.out.println(i+" record faCode = "+clientMap.get("faCode") + "class = "+clientMap.get("faCode").getClass());
			System.out.println(i+" record amt = "+clientMap.get("amt") + "class = "+clientMap.get("amt").getClass());
		}
		
		if(voucherService.getVoucherType().equalsIgnoreCase("Cheque Payment")){
			 map = chqPayVoucherDAO.saveChqPayVoucher(voucherService , faList);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		
		return map;
	}	
	
	
	//TODO Back Data
	@RequestMapping(value = "/getVDetFrCPVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrCPVBack() {  
		System.out.println("Enter into getVDetFrCPVBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
}
