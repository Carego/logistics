package com.mylogistics.services;

public class JourVouchDRCR {
	
	private String faCode;
	
	private double crAmt;
	
	private double dbAmt;
	
	private String desc;
	
	private String chqNo;

	public String getFaCode() {
		return faCode;
	}

	public void setFaCode(String faCode) {
		this.faCode = faCode;
	}

	public double getCrAmt() {
		return crAmt;
	}

	public void setCrAmt(double crAmt) {
		this.crAmt = crAmt;
	}

	public double getDbAmt() {
		return dbAmt;
	}

	public void setDbAmt(double dbAmt) {
		this.dbAmt = dbAmt;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getChqNo() {
		return chqNo;
	}

	public void setChqNo(String chqNo) {
		this.chqNo = chqNo;
	}
}
