package com.mylogistics.model;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "multiplestate")

public class MultipleState {
	@Id
	@Column(name="mulStateId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mulStateId;
	
	@Column(name="mulStateStateCode")
	private String mulStateStateCode;
	
	@Column(name="mulStateRefCode")
	private String mulStateRefCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getMulStateId() {
		return mulStateId;
	}

	public void setMulStateId(int mulStateId) {
		this.mulStateId = mulStateId;
	}

	public String getMulStateStateCode() {
		return mulStateStateCode;
	}

	public void setMulStateStateCode(String mulStateStateCode) {
		this.mulStateStateCode = mulStateStateCode;
	}

	public String getMulStateRefCode() {
		return mulStateRefCode;
	}

	public void setMulStateRefCode(String mulStateRefCode) {
		this.mulStateRefCode = mulStateRefCode;
	}

}
