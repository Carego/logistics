'use strict';

var app = angular.module('application');

app.controller('ViewVehicleTypeCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.getVehicleTypeDetails = function(){
		console.log("Entered into getVehicleTypeDetails function------>");
		var response = $http.post($scope.projectName+'/getVehicleTypeDetails');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vehicletypeList = data.list;	
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
		
	 $scope.update = function(VehicleTypeForm,vehicletype){
		console.log("Entered into update function---");
	
		if(VehicleTypeForm.$invalid){
			if(VehicleTypeForm.vtVehicleType.$invalid){
				$scope.alertToast("Please enter vehicle type between 3-40 characters.....");
			}else if(VehicleTypeForm.vtCode.$invalid){
				$scope.alertToast("Please enter vehicle  type code of one character.....");
			}else if(VehicleTypeForm.vtServiceType.$invalid){
				$scope.alertToast("Please enter service type between 3-40 characters.....");
			}else if(VehicleTypeForm.vtLoadLimit.$invalid){
				$scope.alertToast("Please enter valid load limit that is number of digits before decimal can be atmost seven.....");
			}else if(VehicleTypeForm.vtGuaranteeWt.$invalid){
				$scope.alertToast("Please enter guarentee weight that is number of digits before decimal can be atmost seven.....");
			}
		}else{
			var response = $http.post($scope.projectName+'/updateVehicleType', vehicletype);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	 } 
	 
	 $('VehicleTypeCode').keypress(function(e) {
	       if (this.value.length == 1) {
	           e.preventDefault();
	       }
	});
	 
	/* $('#LoadLimit[$index]').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });

	$('#LoadLimit[$index]').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	});*/
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
			$scope.getVehicleTypeDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	

	
}]);