package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.util.List;
import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "cashstmtstatus")
public class CashStmtStatus {

	@Id
	@Column(name="cssId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cssId;
	
	//One To Many with CashStmt
	@JsonIgnore
	@JsonManagedReference(value = "cssCs")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="cashstmtstatus_cashstmt",
	joinColumns=@JoinColumn(name="cssId"),
	inverseJoinColumns=@JoinColumn(name="csId"))
	@OrderBy("csVouchNo ASC")
	private List<CashStmt> cashStmtList = new ArrayList<CashStmt>();
	
	@Column(name="cssDt")
	private Date cssDt;
	
	@Column(name="cssCsNo")
	private int cssCsNo;

	@Column(name="cssOpenBal")
	private double cssOpenBal;
	
	@Column(name="cssCloseBal")
	private double cssCloseBal;
	
	@Column(name="cssBkOpenBal" , columnDefinition="double default 0.0")
	private double cssBkOpenBal;
	
	@Column(name="cssBkCloseBal" , columnDefinition="double default 0.0")
	private double cssBkCloseBal;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getCssId() {
		return cssId;
	}

	public void setCssId(int cssId) {
		this.cssId = cssId;
	}

	public List<CashStmt> getCashStmtList() {
		return cashStmtList;
	}

	public void setCashStmtList(List<CashStmt> cashStmtList) {
		this.cashStmtList = cashStmtList;
	}

	public Date getCssDt() {
		return cssDt;
	}

	public void setCssDt(Date cssDt) {
		this.cssDt = cssDt;
	}

	public double getCssOpenBal() {
		return cssOpenBal;
	}

	public void setCssOpenBal(double cssOpenBal) {
		this.cssOpenBal = cssOpenBal;
	}

	public double getCssCloseBal() {
		return cssCloseBal;
	}

	public void setCssCloseBal(double cssCloseBal) {
		this.cssCloseBal = cssCloseBal;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getCssCsNo() {
		return cssCsNo;
	}

	public void setCssCsNo(int cssCsNo) {
		this.cssCsNo = cssCsNo;
	}

	public double getCssBkOpenBal() {
		return cssBkOpenBal;
	}

	public void setCssBkOpenBal(double cssBkOpenBal) {
		this.cssBkOpenBal = cssBkOpenBal;
	}

	public double getCssBkCloseBal() {
		return cssBkCloseBal;
	}

	public void setCssBkCloseBal(double cssBkCloseBal) {
		this.cssBkCloseBal = cssBkCloseBal;
	}
	
	
}
