'use strict';

var app = angular.module('application');

app.controller('DisplayBrokerCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.BrokerCodeFlag = true;
	$scope.isViewNo=false;
	$scope.selection=[];
	$scope.availableTags = [];
	$scope.showBrokerDetails= true;
	$scope.broker={};
	$scope.brkCodeByModal=false;
	$scope.brkCodeByAuto=false;
	
	$scope.OpenBrkCodeDB = function(){
		$scope.BrokerCodeFlag = false;
    	$('div#BrokerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
			$('div#BrokerCodeDB').dialog('open');
		}
	
	$scope.enableModalTextBox = function(){
		$('#brkCodeByMId').removeAttr("disabled");
		 $('#ok').attr("disabled","disabled");
		 $('#brkCodeByAId').attr("disabled","disabled");
		 $scope.brkCodeByA="";
	}
	
	$scope.enableAutoTextBox = function(){
		$('#brkCodeByAId').removeAttr("disabled");
		 $('#brkCodeByMId').attr("disabled","disabled");
		 $('#ok').removeAttr("disabled");
		 $scope.brkCodeByM="";
	}
	
	$scope.getBrkIsViewNo = function(){
		   var response = $http.post($scope.projectName+'/getBrkIsViewNo');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				   $scope.successToast(data.result);
				   $scope.BrokerList = data.list;
				   for(var i=0;i<$scope.BrokerList.length;i++){
				   $scope.BrokerList[i].creationTS = $filter('date')($scope.BrokerList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				   }
				  
			   }else{
				   $scope.alertToast(data.result);
				   $scope.isViewNo=true;
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	$scope.saveBrkCode = function(brokerCodes){
		
		$scope.brkCodeByM=brokerCodes;
		$('div#BrokerCodeDB').dialog('close');
		$scope.showBrokerDetails= false;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/brokerDetails',brokerCodes);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.broker = data.broker;	
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.toggleSelection = function toggleSelection(brkCode) {
		   var idx = $scope.selection.indexOf(brkCode);
		   
		   // is currently selected
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
			   }
		   // is newly selected
		   else {
			   $scope.selection.push(brkCode);
		   }
	   }	  
	
	$scope.verifyBroker = function(){
		if(!$scope.selection.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
		 var response = $http.post($scope.projectName+'/updateIsViewBroker',$scope.selection.toString());
		 response.success(function(data, status, headers, config){
			 if(data.result==="success"){
				$scope.successToast(data.result);
				$scope.getBrkIsViewNo();	 
			 }else{
				 console.log(result.data);
			 }
			
	    });	 
		 response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
			});
	 }
	}
	
	$scope.getBrkCodeList=function(){
	      $( "#brkCodeByAId" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	}
	
	$scope.getBrokerCode = function(){
		var response = $http.get($scope.projectName+'/getBrokerCode');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			$scope.getBrkIsViewNo();
			//$scope.getBrokerCodesList();
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getBrokerCodesList = function(){
		console.log("Enetr into $scope.getBrokerCodesList function");
		var response = $http.post($scope.projectName+'/getBrokerCodesList');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.alertToast(data.result);
				$scope.brokerCodeList = data.list;
				$scope.getBrokerCode();
				console.log("---"+$scope.brokerCodeList);	
			}else{
				console.log(data.result);
			}
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.getBrokerList = function(){
		$scope.code = $( "#brkCodeByAId" ).val();
		
		if($scope.code===""){
			$scope.alertToast("Please enter code");
		}else if($scope.code.substring(0,3)==="brk"){
			$scope.showBrokerDetails= false;
			$scope.isViewNo=true;
			var response = $http.post($scope.projectName+'/brokerDetails',$scope.code);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.broker = data.broker;		
				}else{
					console.log(data.result);
				}
					
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		else{
			$scope.alertToast("Enter correct code");
		}
	}
	
		$scope.backToList = function(){
			 $scope.showBrokerDetails= true;
			 $scope.isViewNo=false;
			 $scope.brkCodeByM="";
			 $scope.brkCodeByA="";
			 $scope.brkCodeByModal=false;
			 $scope.brkCodeByAuto=false;
			 $scope.getBrkIsViewNo();
			 $('#ok').attr("disabled","disabled");
			 $('#brkCodeByAId').attr("disabled","disabled");
			 $('#brkCodeByMId').attr("disabled","disabled");
		 }
		
		 $scope.EditBrokerSubmit = function(broker){
				console.log("enter into EditBrokerSubmit function--->"+broker.branchCode);
				var response = $http.post($scope.projectName+'/EditBrokerSubmit', broker);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.alertToast(data.result);
						$scope.broker="";
						$scope.brkCodeByM="";
						$scope.brkCodeByA="";	
					}else{
						console.log(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		 
		 if($scope.adminLogin === true || $scope.superAdminLogin === true){
			 $scope.getBrokerCodesList();
		}else if($scope.logoutStatus === true){
				 $location.path("/");
		}else{
				 console.log("****************");
		}
		
	//$scope.getBrkIsViewNo();
	
}]);
