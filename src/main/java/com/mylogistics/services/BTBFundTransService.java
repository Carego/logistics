package com.mylogistics.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class BTBFundTransService {

	private String frBrCode;
	private String frBrName;
	private String frBkCode;
	private String frBkAccNo;
	private String frBkName;
	private Date fundDt;
	private double frBkAmt;
	private String desc;
	
	private List<Map<String,Object>> toFundList = new ArrayList<>();

	public String getFrBrCode() {
		return frBrCode;
	}

	public void setFrBrCode(String frBrCode) {
		this.frBrCode = frBrCode;
	}

	public String getFrBrName() {
		return frBrName;
	}

	public void setFrBrName(String frBrName) {
		this.frBrName = frBrName;
	}

	public String getFrBkCode() {
		return frBkCode;
	}

	public void setFrBkCode(String frBkCode) {
		this.frBkCode = frBkCode;
	}

	public String getFrBkAccNo() {
		return frBkAccNo;
	}

	public void setFrBkAccNo(String frBkAccNo) {
		this.frBkAccNo = frBkAccNo;
	}

	public String getFrBkName() {
		return frBkName;
	}

	public void setFrBkName(String frBkName) {
		this.frBkName = frBkName;
	}

	public List<Map<String, Object>> getToFundList() {
		return toFundList;
	}

	public void setToFundList(List<Map<String, Object>> toFundList) {
		this.toFundList = toFundList;
	}

	public double getFrBkAmt() {
		return frBkAmt;
	}

	public void setFrBkAmt(double frBkAmt) {
		this.frBkAmt = frBkAmt;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getFundDt() {
		return fundDt;
	}

	public void setFundDt(Date fundDt) {
		this.fundDt = fundDt;
	}
	
	
	
}
