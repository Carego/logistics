'use strict';

var app = angular.module('application');

app.controller('AddContToStnCntlr',['$scope','$location','$http','$window','$log',
                                 function($scope,$location,$http,$window,$log){

	$scope.contFaList = [];
	$scope.ctosList = [];
	$scope.contract = {};
	$scope.ctsList = [];
	$scope.vehList = [];
	$scope.stnList = [];
	$scope.contToStnTemp=[];
	$scope.pbdStnList=[];
	$scope.pbdP = {};
	$scope.pbdB = {};
	$scope.pbdD = {};
	$scope.cts = {};
	$scope.pt = {};
	$scope.vt = {};
	$scope.tnc={};
	$scope.type =[];
	$scope.productTypeList=[];
	$scope.productType="";
	$scope.station={};
	$scope.prodctTypFlag=false;	
	//$scope.newDtshow = false;
	
	$scope.ctos = {};
	
	
	$scope.contCodeDBFlag = true;
	$scope.contToStationFlagW = true;
	$scope.VehicleTypeFlag = true;
	$scope.ProductTypeFlag = true;
	$scope.ToStationsDBFlag = true;
	$scope.checkLoadFlag=true;
	$scope.checkUnLoadFlag=true;
	$scope.checkTransitFlag=true;
	$scope.checkStatisticalChargeFlag=true;
	$scope.checkPenaltyFlag=true;
	$scope.PenaltyFlag = true;
	$scope.PenaltyToStationFlag = true;
	$scope.PenaltyVehicleTypeFlag = true;
	$scope.checkBonusFlag=true;
	$scope.BonusFlag = true;
	$scope.checkDetentionFlag=true;
	$scope.DetentionFlag = true;
	$scope.chgBB = false;
	
	$scope.chgRate = [];
	
	$('#cnmtVOG').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#cnmtVOG').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	  $scope.getContCode = function(){
		  console.log("enter into getContCode function");
		  var response = $http.post($scope.projectName+'/getCCFrToStn');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.contFaList = data.list;
					$scope.vehList = data.vehList;
					$scope.stnList = data.stnList;
				}else{
					$scope.alertToast("Contract are not avaliable");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	  }
	
	  
	  $scope.openCodesDB = function(){
		  console.log("enter into openCodesDB function");
		  $scope.contCodeDBFlag = false;
			$('div#contCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title:"select Contract Code",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.contCodeDBFlag = true;
			    }
				});
			$('div#contCodeDB').dialog('open');
	  }
	  
	  
	  $scope.saveContCode = function(code){
		  console.log("enter into saveContCode function");
		  $scope.contCode = code;
		  $('div#contCodeDB').dialog('close');
	  }
	  
	  $scope.viewContract = function(code,contForm){
		  console.log("enter into viewContract function");
		  if(contForm.$invalid){
			$scope.alertToast("please select a contract code");  
		  }else{
			  var response = $http.post($scope.projectName+'/getToStnFrChng',code);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("success");
						$scope.contract = data.cotract;
						$scope.ctosList = data.list;
						for(var i=0;i<$scope.ctosList.length;i++){
							console.log(i);
						}
						$log.info($scope.contract);
						$log.info($scope.ctosList);
						console.log("ctosList.length = "+$scope.ctosList.length);
					}else{
						console.log("failure");
						$scope.alertToast(data.msg);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
		  }
	  }
	  
	  
	  $scope.contToStnDB = function(){
		  console.log("enter into contToStnDB function");
		  if($scope.contract.contType=='Q'){
			  $scope.prodctTypFlag=true;
			  console.log("hiiiiiiiiii"+$scope.contract.contType);
		  }else{
			  $scope.prodctTypFlag=false;
		  }
		  $scope.contToStationFlagW = false;
		  $('div#contToStationW').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.contToStationFlagW = true;
			    }
				});
				
		 $('div#contToStationW').dialog('open');
	  }
	  
	  
	  
	  $scope.openCtsVehicleType = function(){
		  console.log("enter into openCtsVehicleType function");
			$scope.VehicleTypeFlag = false;
			$('div#ctsVehicleTypeDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Vehicle Type",
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.VehicleTypeFlag = true;
			    }
				});
			
			$('div#ctsVehicleTypeDB').dialog('open');		
		}
	  
	  
	  
	  $scope.savVehicleType = function(VT){
			$scope.cts.ctsVehicleType = VT.vtCode;
			$scope.pbdP.pbdVehicleType = VT.vtCode;
			$scope.pbdB.pbdVehicleType = VT.vtCode;
			$scope.pbdD.pbdVehicleType = VT.vtCode;
			$('div#ctsVehicleTypeDB').dialog("close");
			$scope.VehicleTypeFlag = true;
	  }
	  
	  $scope.openCtsProductType = function(){
		  console.log("enter into openCtsVehicleType function");
			$scope.ProductTypeFlag = false;
			
			var res = $http.post($scope.projectName+'/getProductTypeList');
			res.success(function(data, status, headers, config) {
				$scope.productTypeList=data.productTypeList;
				if(data.result === "success"){
					$('div#ctsProductTypeDB').dialog({
						autoOpen: false,
						modal:true,
						title: "Product Type",
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.ProductTypeFlag = true;
					    }
						});
					
					$('div#ctsProductTypeDB').dialog('open');		
				} else{
					$scope.alertToast("No Record found");
				}
			});
			
		res.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
			
			
		
	  
	  $scope.savProductType = function(ptName){
			$scope.productType =ptName ;
			$('div#ctsProductTypeDB').dialog("close");
			$scope.ProductTypeFlag = true;
	  }
	  
	  $scope.openToStationsDB = function(){	
		  console.log("enter into openToStationsDB function");
			$scope.ToStationsDBFlag = false;
			$('div#ToStationsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "To Stations",
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.ToStationsDBFlag = true;
			    }
				});
				
				$('div#ToStationsDB').dialog('open');
	 }
	  
	  
	  
	$scope.saveToStations = function(toStations){
		console.log("enter into saveToStations function = "+toStations.stnName);
		$scope.cts.ctsToStnTemp=toStations.stnName;
		$scope.cts.ctsToStn=toStations.stnCode;
		$scope.pbdP.pbdToStnTemp = toStations.stnName;
		$scope.pbdP.pbdToStn = toStations.stnCode;
		$scope.pbdB.pbdToStnTemp = toStations.stnName;
		$scope.pbdB.pbdToStn = toStations.stnCode;
		$scope.pbdD.pbdToStnTemp = toStations.stnName;
		$scope.pbdD.pbdToStn = toStations.stnCode;
		$('div#ToStationsDB').dialog("close");
		$scope.ToStationsDBFlag = true;
	}	 
	
	
	$scope.saveTncLoad= function(){
		console.log("enter into saveTncLoad --- >"+$scope.checkLoad);
		if($scope.checkLoad){
			$scope.checkLoadFlag=false;
			$scope.tnc.tncLoading="yes";
		}
		if(!($scope.checkLoad)){
			$scope.cts.ctsToLoad="";
			
			$scope.checkLoadFlag=true;
			$scope.tnc.tncLoading="no";
		}	
	}
	
	
	$scope.setLoadFlagTrue = function(cts){
		if(cts.ctsToLoad ===null || cts.ctsToLoad==="" || angular.isUndefined(cts.ctsToLoad)){
			$scope.checkLoadFlag=false;	
		}else{
			$scope.checkLoadFlag=true;
		}
	}
	
	
	$scope.saveTncUnload = function(){
		if($scope.checkUnLoad){
			$scope.checkUnLoadFlag=false;
			$scope.tnc.tncUnLoading="yes";
		}
		if(!($scope.checkUnLoad)){
			$scope.cts.ctsToUnLoad="";
			$scope.checkUnLoadFlag=true;
			$scope.tnc.tncUnLoading="no";
		}	
	}
	  
	$scope.setUnLoadFlagTrue = function(cts){
		if(cts.ctsToUnLoad ===null || cts.ctsToUnLoad==="" || angular.isUndefined(cts.ctsToUnLoad)){
			$scope.checkUnLoadFlag=false;	
		}else{
			$scope.checkUnLoadFlag=true;
		}
	}
	
	
	$scope.saveTncTransitDay = function(){
		if($scope.checkTransitDay){
			$scope.checkTransitFlag=false;
			$scope.tnc.tncTransitDay="yes";
		}
		if(!($scope.checkTransitDay)){
			$scope.cts.ctsToTransitDay="";
			$scope.checkTransitFlag=true;
			$scope.tnc.tncTransitDay="no";
		}	
	}
	
	
	$scope.setTransitFlagTrue = function(cts){
		if(cts.ctsToTransitDay ===null || cts.ctsToTransitDay==="" || angular.isUndefined(cts.ctsToTransitDay)){
			$scope.checkTransitFlag=false;
		}else{
			$scope.checkTransitFlag=true;
		}	
	}
	
	
	$scope.saveTncStatisticalCharge = function(){
		if($scope.checkStatisticalCharge){
			$scope.checkStatisticalChargeFlag=false;
			$scope.tnc.tncStatisticalCharge="yes";
		}
		if(!($scope.checkStatisticalCharge)){
			$scope.cts.ctsToStatChg="";
			$scope.checkStatisticalChargeFlag=true;
			$scope.tnc.tncStatisticalCharge="no";
		}	
	}
	
	$scope.setSCFlagTrue = function(cts){
		if(cts.ctsToStatChg ===null || cts.ctsToStatChg==="" || angular.isUndefined(cts.ctsToStatChg)){
			$scope.checkStatisticalChargeFlag=false;
		}else{
			$scope.checkStatisticalChargeFlag=true;
		}
	}
	
	
	$scope.saveTncPenalty = function(){
		if($scope.checkPenalty){
			$scope.checkPenaltyFlag=false;
			$scope.tnc.tncPenalty="yes";
		}
		if(!($scope.checkPenalty)){
			$scope.checkPenaltyFlag=true;
			$scope.tnc.tncPenalty="no";
		}	
	}
	
	$scope.OpenPenaltyDB = function(){
		$scope.PenaltyFlag = false;
		$scope.pbdP.pbdPenBonDet = "P";
		$scope.pbdP.pbdFromStnTemp = $scope.contract.contFrStn;
		$scope.pbdP.pbdFromStn = $scope.contract.contFrStnCode;
		$scope.pbdP.pbdStartDt = $scope.contract.contFrDt;
		$scope.pbdP.pbdEndDt = $scope.contract.contToDt;
		$('div#PenaltyDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Penalty",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.PenaltyFlag = true;
		    }
		});
		
		$('div#PenaltyDB').dialog('open');
	}
	

	
    $scope.savePbdForPenalty = function(PenaltyForm,pbdP){
		$('div#PenaltyDB').dialog("close");
		if(PenaltyForm.$invalid){
			if(PenaltyForm.pbdFromStnTempP.$invalid){
				$scope.alertToast("please Enter From Station");
			}else if(PenaltyForm.pbdToStnTempP.$invalid){
				$scope.alertToast("please Enter to Station");
			}else if(PenaltyForm.pbdVehicleTypeP.$invalid){
				$scope.alertToast("please Enter vehicle type");
			}else if(PenaltyForm.pbdToDayP.$invalid){
				$scope.alertToast("please Enter correct todlay");
			}else if(PenaltyForm.pbdFromDayP.$invalid){
				$scope.alertToast("please Enter correct fromdlay");
			}else if(PenaltyForm.pbdHourNDayP.$invalid){
				$scope.alertToast("please Enter Hour/Day");
			}else if(PenaltyForm.pbdAmtP.$invalid){
				$scope.alertToast("please Enter correct amount");
			}
		}else{
			if($scope.pbdP.pbdFromDay>$scope.pbdP.pbdToDay){
				$scope.alertToast("From day cannot be greater than to day");
		     }else if($scope.pbdP.pbdStartDt>$scope.pbdP.pbdEndDt){
					$scope.alertToast("Start date cannot be greater than end date");
			     }else {
			    	 
			    	 var temp = {
			    			"ToStn" : 	pbdP.pbdToStnTemp,
							"ToStnTemp" : 	pbdP.pbdToStn,
							"FromStn" : pbdP.pbdFromStnTemp,
							"FromStnTemp" : pbdP.pbdFromStn,
							"pbd"	  :pbdP.pbdPenBonDet
					};
					$scope.pbdStnList.push(temp);
					console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
					pbdP.pbdContCode = $scope.contract.contCode; 
				var response = $http.post($scope.projectName+'/addPbdForACTS', pbdP);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.checkPenaltyFlag=true;
						console.log("from server--<<<"+data.result);
						$scope.fetchPbdList();
						
						$('input[name=penaltyFromstationName]').attr('checked',false);
						$('input[name=penaltyTostationName]').attr('checked',false);
						$('input[name="VtPenaltyName"]').attr('checked',false);
						
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}
		}
	
    
    $scope.fetchPbdList = function(){
		console.log("enter into fetchPbdList function");
		 var response = $http.get($scope.projectName+'/fetchPbdListForACTS');
		 response.success(function(data, status, headers, config){
			if(data.result==="success"){
			 	$scope.pbdList = data.list;
			 	$scope.pbdFlag=true;
			 	$scope.checkPenaltyFlag=true;
			}else{
				console.log(data);
				$scope.checkPenalty=false;
		 		$scope.checkBonus=false;
		 		$scope.checkDetention=false;
		 		$scope.pbdFlag=false;
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
    
    
    $scope.removePbd = function(pbd) {
		console.log("enter into removePbd function");
		var response = $http.post($scope.projectName+'/removePbdForACTS',pbd);
		response.success(function(data, status, headers, config){
			for(var i=0;i<$scope.pbdStnList.length;i++){
				if(($scope.pbdStnList[i].ToStnTemp === pbd.pbdToStn) && ($scope.pbdStnList[i].FromStnTemp === pbd.pbdFromStn) && ($scope.pbdStnList[i].pbd === pbd.pbdPenBonDet)){
					$scope.pbdStnList.splice(i,1);
					i--;
				}
			}
			console.log($scope.pbdStnList.length);
			$scope.fetchPbdList();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
    }
	
    
    $scope.removeAllPbd = function() {
		var response = $http.post($scope.projectName+'/removeAllPbdForRegCont');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.pbdStnList = [];
				$scope.fetchPbdList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
    }
    
    
    
    $scope.saveTncBonus = function(){
		if($scope.checkBonus){
			$scope.checkBonusFlag=false;
			$scope.tnc.tncBonus="yes";
		}
		if(!($scope.checkBonus)){
			$scope.checkBonusFlag=true;
			$scope.tnc.tncBonus="no";
		}	
	}
    
    
    $scope.OpenBonusDB = function(){
		$scope.BonusFlag = false;
		$scope.pbdB.pbdFromStnTemp = $scope.contract.contFrStn;
		$scope.pbdB.pbdFromStn = $scope.contract.contFrStnCode;
		$scope.pbdB.pbdStartDt = $scope.contract.contFrDt;
		$scope.pbdB.pbdEndDt = $scope.contract.contToDt;
		$scope.pbdB.pbdPenBonDet = "B";
		$('div#BonusDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Bonus",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.BonusFlag = true;
		    }
		});
			
		$('div#BonusDB').dialog('open');
	}
    
    
    
    $scope.savePbdForBonus = function(BonusForm,pbdB){
		$('div#BonusDB').dialog("close");
		$scope.BonusFlag=true;
		if(BonusForm.$invalid){
			if(BonusForm.pbdFromStnTempB.$invalid){
				$scope.alertToast("please Enter From Station");
			}else if(BonusForm.pbdToStnTempB.$invalid){
				$scope.alertToast("please Enter to Station");
			}else if(BonusForm.pbdVehicleTypeB.$invalid){
				$scope.alertToast("please Enter vehicle type");
			}else if(BonusForm.pbdToDayB.$invalid){
				$scope.alertToast("please Enter correct todlay");
			}else if(BonusForm.pbdFromDayB.$invalid){
				$scope.alertToast("please Enter correct fromdlay");
			}else if(BonusForm.pbdHourNDayB.$invalid){
				$scope.alertToast("please Enter Hour/Day");
			}else if(BonusForm.pbdAmtB.$invalid){
				$scope.alertToast("please Enter correct amount");
			}
		}else{
			console.log(pbdB.pbdFromDay+" "+pbdB.pbdToDay);
			if($scope.pbdB.pbdFromDay > $scope.pbdB.pbdToDay){
				$scope.alertToast("From day cant be greater than to day");
		     }else if(pbdB.pbdStartDt>pbdB.pbdEndDt){
					$scope.alertToast("Start date cannot be greater than end date");
		     }else{
		    	 
		    	 var temp = {
		    			 "ToStn" 		: 	pbdB.pbdToStnTemp,
						 "ToStnTemp"    : 	pbdB.pbdToStn,
						 "FromStn"      :   pbdB.pbdFromStnTemp,
						 "FromStnTemp"  :   pbdB.pbdFromStn,
						 "pbd"	        :   pbdB.pbdPenBonDet
						};
					$scope.pbdStnList.push(temp);
					console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
					pbdB.pbdContCode = $scope.contract.contCode;
				var response = $http.post($scope.projectName+'/addPbdForACTS', pbdB);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
					$scope.checkBonusFlag=true;
					$scope.fetchPbdList();
					$('input[name=bonusFromstationName]').attr('checked',false);
					$('input[name=bonusTostationName]').attr('checked',false);
					$('input[name="vtBonusName"]').attr('checked',false);
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}
		}
    
    
    $scope.saveTncDetention = function(){
		if($scope.checkDetention){
			$scope.checkDetentionFlag=false;
			$scope.tnc.tncDetention="yes";
		}
		if(!($scope.checkDetention)){
			$scope.checkDetentionFlag=true;
			$scope.tnc.tncDetention="no";
		}	
	}
    
    
    $scope.OpenDetentionDB = function(){
		$scope.DetentionFlag = false;
		$scope.pbdD.pbdFromStnTemp = $scope.contract.contFrStn;
		$scope.pbdD.pbdFromStn = $scope.contract.contFrStnCode;
		$scope.pbdD.pbdStartDt = $scope.contract.contFrDt;
		$scope.pbdD.pbdEndDt = $scope.contract.contToDt;
		$scope.pbdD.pbdPenBonDet = "D";
		$('div#DetentionDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Detention",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.DetentionFlag = true;
		    }
		});
		
		$('div#DetentionDB').dialog('open');
	}
    
    
    $scope.savePbdForDetention = function(DetentionForm,pbdD){
		$('div#DetentionDB').dialog("destroy");
		$scope.DetentionFlag=true;
		console.log("enter into savePbd function--->");
		if(DetentionForm.$invalid){
			
			if(DetentionForm.pbdFromStnTempD.$invalid){
				$scope.alertToast("please Enter From Station");
			}else if(DetentionForm.pbdToStnTempD.$invalid){
				$scope.alertToast("please Enter to Station");
			}else if(DetentionForm.pbdVehicleTypeD.$invalid){
				$scope.alertToast("please Enter vehicle type");
			}else if(DetentionForm.pbdToDayD.$invalid){
				$scope.alertToast("please Enter correct todlay");
			}else if(DetentionForm.pbdFromDayD.$invalid){
				$scope.alertToast("please Enter correct fromdlay");
			}else if(DetentionForm.pbdHourNDayD.$invalid){
				$scope.alertToast("please Enter Hour/Day");
			}else if(DetentionForm.pbdAmtD.$invalid){
				$scope.alertToast("please Enter correct amount");
			}
			}else{
			 if($scope.pbdD.pbdFromDay>$scope.pbdD.pbdToDay){
				$scope.alertToast("From day cannot be greater than to day");
		     }else if($scope.pbdD.pbdStartDt>$scope.pbdD.pbdEndDt){
					$scope.alertToast("Start date cannot be greater than end date");
			     }else{
			    	 
			    	 var temp = {
			    			    "ToStn" : 	pbdD.pbdToStnTemp,
								"ToStnTemp" : 	pbdD.pbdToStn,
								"FromStn" : pbdD.pbdFromStnTemp,
								"FromStnTemp" : pbdD.pbdFromStn,
								"pbd"	  :pbdD.pbdPenBonDet
							};
						$scope.pbdStnList.push(temp);
						console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
				pbdD.pbdContCode = $scope.contract.contCode;		
				var response = $http.post($scope.projectName+'/addPbdForRegCont', pbdD);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
					$scope.checkDetentionFlag=true;
					$scope.fetchPbdList();
					$('input[name=detentionFromstationName]').attr('checked',false);
					$('input[name=detentionTostationName]').attr('checked',false);
					$('input[name="vtDetentionName"]').attr('checked',false);
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		}
	}
	
    
    
    
    $scope.saveContToStnW = function(ContToStationFormW,cts,regCnt,metricType){
		console.log("Enter into saveContToStnW function"+ContToStationFormW.$invalid);
		$('div#contToStationW').dialog("close");
		$scope.contToStationFlagW = true;
		$scope.ctsflagW=true;
		if($scope.ctsList.length>0){
			console.log("if list exists"+$scope.ctsList.length);
			if(ContToStationFormW.ctsToStnTemp.$invalid){
				$scope.alertToast("Please enter To Station");
			}else if(ContToStationFormW.ctsVehicleType.$invalid){
				$scope.alertToast("Please enter Vehicle Type");
			} else if(cts.ctsFromWt>cts.ctsToWt){
				$scope.alertToast("From Weight cannot be greater than To Weight");
			}else {
				cts.ctsToStn=cts.ctsToStn.toUpperCase();
				cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
				$scope.ctsTemp= cts.ctsToStn+cts.ctsVehicleType;
				
				for(var i=0;i<$scope.ctsList.length;i++){
					console.log($scope.ctsList[i].ctsToStn);
					//$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType;
					$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType);
				}
				if($.inArray($scope.ctsTemp,$scope.type)!==-1){
					$scope.alertToast("Already exists");
				}else{
					var temp = {
							"ToStn" : 	cts.ctsToStn,
							"ToStnTemp" : cts.ctsToStnTemp,
							"vehicleType" :cts.ctsVehicleType,
							"ptName" :$scope.productType
						};
					$scope.contToStnTemp.push(temp);
					console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
					console.log("$scope.contract.contFrDt = "+$scope.contract.contFrDt);
					console.log("$scope.contract.contToDt = "+$scope.contract.contToDt);
					cts.ctsFrDt = $scope.contract.contFrDt;
					cts.ctsToDt = $scope.contract.contToDt;
					
					var response = $http.post($scope.projectName+'/addCTSForRC', cts);	   
					response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();	
								$scope.ContToStnFlag=true;
								console.log($scope.ContToStnFlag);
								
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}			
				}
		}else{
			console.log("if list does not exists"+cts.ctsFromWt+""+cts.ctsToWt);
			if(ContToStationFormW.ctsToStnTemp.$invalid){
				$scope.alertToast("Please enter To Station");
			}else if(ContToStationFormW.ctsVehicleType.$invalid){
				$scope.alertToast("Please enter Vehicle Type");
			} else if(cts.ctsFromWt>cts.ctsToWt){
				$scope.alertToast("From Weight cannot be greater than To Weight");
			}else{
				cts.ctsToStn=cts.ctsToStn.toUpperCase();
				cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
				
				var temp = {
						"ToStn" : 	cts.ctsToStn,
						"ToStnTemp" : cts.ctsToStnTemp,
						"vehicleType" :cts.ctsVehicleType,
						"ptName" :$scope.productType
					};
				$scope.contToStnTemp.push(temp);
				
				console.log("$scope.contract.contFrDt = "+$scope.contract.contFrDt);
				console.log("$scope.contract.contToDt = "+$scope.contract.contToDt);
				cts.ctsFrDt = $scope.contract.contFrDt;
				cts.ctsToDt = $scope.contract.contToDt;
				
				console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
				var response = $http.post($scope.projectName+'/addCTSForACTS',cts);	   
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							console.log("from server ---->"+data.result);
							$scope.fetchCTSList();	
							$scope.ContToStnFlag=true;
							console.log("saved In W"+$scope.ContToStnFlag);
							$('input[name=toStations]').attr('checked',false);
							$('input[name=VTName]').attr('checked',false);
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});	
				}	
			}
		}
    
    
    $scope.fetchCTSList = function(){
		   console.log("Entr into fetchCTSList function");
			 var response = $http.get($scope.projectName+'/fetchCTSListForACTS');
			 response.success(function(data, status, headers, config){
				 if(data.result==="success"){
					 $scope.ctsList = data.list;
					 if($scope.contract.contType==='W'){
						 $scope.ctsFlagW=true;	 
					 }else if($scope.contract.contType==='Q'){
						 $scope.ctsFlagQ=true;	 
					 }
				 }else{
					 console.log(data.result);
					 $scope.ctsList = data.list;
					 $scope.ctsFlagW=false;
					 $scope.ctsFlagQ=false;
					 if($scope.regCnt.regContType==='W' || $scope.regCnt.regContType==='Q'){
						 console.log($scope.regCnt.regContType);
						 $scope.ContToStnFlag=false; 
					 }
					 }	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
    
    
    $scope.removeCTS = function(cts) {
		console.log("Entr into removeCTS function");
		var response = $http.post($scope.projectName+'/removeCTSForACTS',cts);
		response.success(function(data, status, headers, config) {
			console.log(cts.ctsToStn);
			for(var i=0;i<$scope.contToStnTemp.length;i++){
				if(($scope.contToStnTemp[i].ToStn === cts.ctsToStn) && ($scope.contToStnTemp[i].vehicleType === cts.ctsVehicleType)){
					$scope.contToStnTemp.splice(i,1);
					i--;
				}
			}
			console.log($scope.contToStnTemp.length);
			$scope.fetchCTSList();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
    
    
    $scope.removeAllCTS = function() {
		console.log("Entr into removeAllCts function");
		var response = $http.post($scope.projectName+'/removeAllCTSForACTS');
		response.success(function(data, status, headers, config) {
			if($scope.contToStnTemp.length > 0){
				for(var i=0;i<$scope.contToStnTemp.length;i++){
					$scope.contToStnTemp.splice(i,1);
					i--;
				}
			}
			console.log($scope.contToStnTemp.length);
			$scope.fetchCTSList();
			$scope.ctsflagW=false;
			$scope.ctsflagQ=false;
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
    }
    
    
    $scope.chgBillBasis = function(billBase){
    	console.log("enter into chgBillBasis function = "+billBase);
    	$scope.chgBB = true;
    }
    
    
    
    $scope.saveCts = function(){
    	console.log("enter into saveCts function");
    	console.log("size of $scope.chgRate = "+$scope.chgRate.length);
    	if($scope.chgRate.length > 0){
    		for(var i=0;i<$scope.chgRate.length;i++){
    			console.log("$scope.chgRate["+i+"].ctsId = "+$scope.chgRate[i].ctsId);
    			console.log("$scope.chgRate["+i+"].newRate = "+$scope.chgRate[i].newRate);
    			console.log("*****************");
    		}
    	}
    	
    	
    	var req = {
    			"contCode"    : $scope.contract.contCode,
    			"renDt"       : $scope.renDt,
    			"billBasis"   : $scope.contract.contBB,
    			"chgRateList" : $scope.chgRate,
    			"ptName" :$scope.productType
    	};
    	
    	var response = $http.post($scope.projectName+'/saveCtsFrACTS',req);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.getContCode();
				$scope.contFaList = [];
				$scope.ctosList = [];
				$scope.contract = {};
				$scope.ctsList = [];
				$scope.pbdList = [];
				$scope.vehList = [];
				$scope.stnList = [];
				$scope.contToStnTemp=[];
				$scope.pbdStnList=[];
				$scope.pbdP = {};
				$scope.pbdB = {};
				$scope.pbdD = {};
				$scope.cts = {};
				$scope.pt = {};
				$scope.vt = {};
				$scope.tnc={};
				$scope.type =[];
				$scope.station={};
				$scope.chgRate = [];
				$scope.renDt = '';
				$scope.productType="";
				$scope.chgBB = false;
			}else{
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
    }
    
    
    $scope.chkRenDt = function(){
    	console.log("enter into chkRenDt funcition = "+$scope.renDt);
    	if(angular.isUndefined($scope.renDt) || $scope.renDt === "" || $scope.renDt === null){
    		console.log("**********************");
    		$scope.chgRate = [];
    		//$('#newRateId').attr("disabled","disabled");
    		if($scope.ctosList.length > 0){
    			for(var i=0;i<$scope.ctosList.length;i++){
    				$('#newRateId'+i).attr("disabled","disabled");
    			}
    		}
    	}else{
    		console.log("#######################");
    		//$('#newRateId').removeAttr("disabled");
    		if($scope.contract.contFrDt > $scope.renDt){
    			$scope.alertToast("Please enter renew date greater than "+$scope.contract.contFrDt);
    		}else if($scope.contract.contToDt < $scope.renDt){
    			$scope.alertToast("Please enter renew date less than "+$scope.contract.contToDt);
    		}else{
    			if($scope.ctosList.length > 0){
        			for(var i=0;i<$scope.ctosList.length;i++){
        				if($scope.renDt > $scope.ctosList[i].ctsFrDt && $scope.renDt < $scope.ctosList[i].ctsToDt){
        					$('#newRateId'+i).removeAttr("disabled");
        				}
        			}
        		}
    		}
    	}
    }
    
    
    $scope.chgCtsRate = function(id,rate){
    	console.log("enter into chgCtsRate function = "+id);
    	
    	if(rate > 0){
    		if($scope.chgRate.length > 0){
        		var duplicate = false;
        		for(var i=0;i<$scope.chgRate.length;i++){
        			if($scope.chgRate[i].ctsId === id){
        				duplicate = true;
        				break;
        			}
        		}
        		
        		if(duplicate){
        			$scope.alertToast("you already entered the new rate for this station");
        		}else{
        			var map = {
            				"ctsId"   : id,
            				"newRate" : rate
            		};
            		$scope.chgRate.push(map);
        		}
        	}else{
        		var map = {
        				"ctsId"   : id,
        				"newRate" : rate
        		};
        		$scope.chgRate.push(map);
        	}
    	}else{
    		console.log("%%%%%%%%%%%%%%%");
    		if($scope.chgRate.length > 0){
        		for(var i=0;i<$scope.chgRate.length;i++){
        			if($scope.chgRate[i].ctsId === id){
        				$scope.chgRate.splice(i,1);
        				break;
        			}
        		}
        	}
    	}	
    }
    
    $scope.changeToWt = function(ctos, index) {
		console.log("changeToWt()");
		$log.info(ctos);
		$scope.ctos = ctos;
		$scope.ctos.ctsToWt = ctos.toWt; 
		$scope.ctos.ctsFromWt = ctos.frWt;
		//if($scope.contract.contToDt >= ctos.ctsToDt){

			//	$scope.ctos.ctsToDt = ctos.extendDate;
			//	console.log("Date"+$scope.ctos.ctsToDt);
				console.log(ctos);
				var res = $http.post($scope.projectName+'/updateToWt', $scope.ctos);
				res.success(function(data, status, headers, config) {
					if (data.result === "success") {
						console.log("response of updateToWt "+data.result);
						$scope.alertToast(data.result);
					} else {
						console.log("response of updateToWt "+data.result);
						$scope.alertToast(data.result);
					}
				});
				res.error(function(data, status, headers, config) {
					console.log("error in response of updateToWt "+data);
					$scope.alertToast(data.result);
				})	
		/*}else{
			$scope.alertToast("Extend Date can't be greater than "+$scope.contract.contToDt);
		}*/
	}
    
    
  /*  $scope.chkExtndDt=function(){
    	console.log("chkExtndDt") ;   	
    	$scope.newDtshow = ! $scope.newDtshow;
    	if($scope.contract.contToDt>){
    		$scope.newDtshow=true;
    	}
    		
    }*/
    
    /* $scope.chgCtsDate=function(ctos,ctsNewToDt){
    	 
    	 
     }*/
    
	 if($scope.adminLogin === true || $scope.superAdminLogin === true){
		  $scope.getContCode();
	 }else if($scope.logoutStatus === true){
			 $location.path("/");
	 }else{
			 console.log("****************");
	 } 
}]);