package com.mylogistics.constants;

public class CustomerGroupCNTS {
	
	public static final String GROUP_ID = "groupId";
	public static final String GROUP_NAME = "groupName";
	public static final String DEFAULT_CUST_GROUP_ID="no";

}
