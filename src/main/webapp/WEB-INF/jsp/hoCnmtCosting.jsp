 <div ng-show=" superAdminLogin"> 
<title>CNMT COSTING</title>
	
	<div class="row" >
	<form name="cnmtCostForm" ng-submit="costing()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);" >
	
	<div class="row">
		    
		    	<div class="col s3 input-field" style="width: 8%;">
					<input class="validate" type="radio" id="consolidateRBId" name="isRBName" ng-model="cnmtCosting.isConBrh" value="con" ng-click="consolidateRB()" > 
					<label for="code">Consolidate</label>
				</div>
				
				<div class="col s3 input-field" style="width: 8%;">
					<input class="validate" type="radio" id="branchRBId" name="isRBName" ng-model="cnmtCosting.isConBrh" value="brh" ng-click="branchRB()"> 
					<label for="code">Branch</label>
				</div>
				
				<div class="col s3 input-field" style="width: 20%;">
	       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="brhName" ng-click="openBranchDB()" disabled="disabled" readonly="readonly">
	       			<label for="code">Select Branch</label>	
	       		</div>
	       		
	       		<div class="col s3 input-field" style="width: 20%;">
	       				<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="custName" ng-click="openCustDB()"  readonly="readonly">
	       			<label for="code">Select Customer</label>	
	       		</div>
	       		<div class="input-field col s4" style="width: 20%;">
					<input type="date" name="frmDt" ng-model="cnmtCosting.frmDt" ng-required=true>
					<label>From Date</label>
				</div>
				<div class="input-field col s4" style="width: 20%;">
					<input type="date" name="toDt" ng-model="cnmtCosting.toDt" ng-required=true>
					<label>To Date</label>
				</div>		
		    
		    </div>		
	
	 		<div class="row">				 
				<input class="btn" type="submit" id="submitId" value="Generate Cost">				 
				<input type="button" id="printXlsId" value="Take Report" ng-click="getCostingReport()">				
				<input type="button" id="downloadReport" value="Download" ng-click="downloadReport()">
			 </div>
			 
	</form>
	
	</div>
	
	<div id ="brhOpenDB" ng-hide="brhOpenFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Id</th>
 	  	  	  <th>Bank Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in brhList | filter:filterTextbox">
		 	  <td><input type="radio"  name="brhName"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBrhCode(brh)"></td>
              <td>{{brh.branchId}}</td>
              <td>{{brh.branchName}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="custOpenDB" ng-hide="custOpenFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Code</th>
 	  	  	  <th>Customer Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in customerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="custName"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustCode(cust)"></td>
              <td>{{cust.custCode}}</td>
              <td>{{cust.custName}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="cnmtCostingTable" class="myTable">
		<table>
 	    	<caption>Cnmt Costing</caption> 	    
 	  	  	<tr>
 	  	  		<th>S.No.</th> 	  	  	  
 	  	  		<th >CNMT No.</th>
 	  	  		<th >CNMT Dt.</th>
 	  	  		<th >BCode</th>
 	  	  		<th >Consignee</th> 	  	  		
 	  	  		<th >Consignor</th> 	  	  	  
 	  	  		<th >Frm. Sta.</th>
 	  	  		<th >To Sta.</th>
 	  	  		<th >Gra. Wt.</th>
 	  	  		<th >CNMT Amt.</th> 	  	  		
 	  	  		<th >Bill No.</th> 	  	  	  
 	  	  		<th >Bill Amt.</th>
 	  	  		<th >Chln. No.</th>
 	  	  		<th >Chln. Dt.</th>
 	  	  		<th >Chln. Frt.</th>
 	  	  		<th >LHPV Amt.</th>
 	  	  		<th >Unloading Amt.</th>
 	  	  		<th >Detention Amt.</th>
 	  	  		<th >Ov. Ht. Amt.</th>
 	  	  		<th >Penelty Amt.</th>
 	  	  		<th >Extra Km.</th>
 	  	  		<th >Rcv. Amt.</th>
 	  	  		<th >Cost</th>
 	  	  		<th >Net Mrg.</th>
 	  	  		<th >Mrg. %</th> 	  	  		
 	  	  </tr>
 	  	  <tr ng-repeat="cnmt in cnmtCostingList">	 	
				<td >{{$index + 1}}</td>
				<td >{{cnmt.cnmtCode}}</td>
				<td >{{cnmt.cnmtDt}}</td>
				<td >{{cnmt.cnmtBCode}}</td>
				<td >{{cnmt.cnmtConsignee}}</td>
				<td >{{cnmt.cnmtConsignor}}</td>
				<td >{{cnmt.cnmtFrmStn}}</td>
				<td >{{cnmt.cnmtToStn}}</td>
				<td >{{cnmt.cnmtGuaWt / 1000}}</td>
				<td >{{cnmt.cnmtAmt}}</td>
				<td >{{cnmt.cnmtBillNo}}</td>
				<td >{{cnmt.cnmtBillAmt}}</td>
				<td >{{cnmt.cnmtChlnCode}}</td>
				<td >{{cnmt.cnmtChlnDt}}</td>
				<td >{{cnmt.cnmtChlnFrt}}</td>
				<td >{{cnmt.cnmtLhpvAmt}}</td>
				<td >{{cnmt.cnmtUnl}}</td>
				<td >{{cnmt.cnmtDetention}}</td>
				<td >{{cnmt.cnmtOverHeight}}</td>
				<td >{{cnmt.cnmtPenelty}}</td>
				<td >{{cnmt.cnmtExtraKm}}</td>
				<td >{{cnmt.cnmtRecovery}}</td>
				<td >{{cnmt.cnmtCost}}</td>
				<td >{{cnmt.cnmtNetMer}}</td>
				<td >{{cnmt.cnmtNetMerPer}}</td>			
          </tr>
      </table> 
	</div>	
	
	

 </div>