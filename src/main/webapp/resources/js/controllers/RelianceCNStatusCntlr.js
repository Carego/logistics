'use strict';

var app = angular.module('application');

app.controller('RelianceCNStatusCntlr',['$scope','$location','$http','$window','FileUploadService','$filter',
                            function($scope,$location,$http,$window,FileUploadService,$filter){


	$scope.CnmtCodeDBFlag=true;
	$scope.ChallanCodeDBFlag=true;
	$scope.message="";
	$scope.message2="";
	$scope.ArDBFlag=true;
	$scope.ArCodeDBFlag=true;
	$scope.printXlsFlag = false;
	
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "relCNStatus.xls");
	};

	$scope.OpenCnmtCodeDB = function(keyCode){
		console.log("Enter into OpenCnmtCodeDB()");
		
		if(keyCode ===8  || parseInt($scope.cnStatus.cnmtCode.length) < 3)
			return;
			
			var response = $http.post($scope.projectName + '/getCnmtCodeByCodeN', $scope.cnStatus.cnmtCode);
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){
					
					$scope.cnStatus.cnmtCode = ""; 
					$scope.cnmtCodeList = data.cnmtCodeList;
					
					$scope.CnmtCodeDBFlag=false;
					$('div#cnmtCodeDB').dialog({
						autoOpen: false,
						modal:true,
						title: "CNMT Code",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#cnmtCodeDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
					$scope.cnStatus.cnmtCode = ""; 
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCnmtCodeByCode");
			});
		console.log("Exit from OpenCnmtCodeDB()");
	}
	
	
	
	$scope.saveCnmtCode = function(cnmtCode,cnmtToSt,cnmtFromSt){
		$scope.cnStatus.cnmtCode=cnmtCode;
	var stationData={toStation : cnmtToSt,fromStation : cnmtFromSt };
		console.log(cnmtCode+" To Station "+cnmtToSt+" From Station  "+cnmtFromSt);
		console.log("Cnmt Code for global variable is "+$scope.cnStatus.cnmtCode);
		
		$('div#cnmtCodeDB').dialog('close');
		$scope.CnmtCodeDBFlag=true; 
		
	var response = $http.post($scope.projectName + '/getDataFromRelianceCNStatus', cnmtCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.cnList==0)
				{
				var response = $http.post($scope.projectName + '/getStationByCode', stationData);
				response.success(function(data, status, headers, config){
					console.log(data);
					if(data.result === "success"){
						console.log(data.toStName);
						$scope.tostName = data.toStName;
						$scope.frStName=data.frStName;
						console.log("To Station Name " +$scope.tostName);
		                $scope.cnStatus.fromStation=$scope.frStName;
		                $scope.cnStatus.toStation=$scope.tostName;
				}});
				response.error(function(data, status, headers, config){
					console.log("Error in hitting /getCnmtCodeByCode");
				});
				
			}
			else
				{
				$scope.cnList=data.cnList;
				console.log(" I am in else condition");
				console.log("Scope object");
				console.log($scope.cnList);
				console.log(data.cnList.fromStation);
				$scope.cnStatus.fromStation=data.cnList.fromStation;
				$scope.cnStatus.toStation=$scope.cnList.toStation;
				$scope.cnStatus.indentDate=$scope.cnList.indentDate;
				$scope.cnStatus.placementDate=$scope.cnList.placementDate;
				$scope.cnStatus.tripStartDate=$scope.cnList.tripStartDate;
				$scope.cnStatus.challanno=$scope.cnList.challanno;
				$scope.cnStatus.lorryNo=$scope.cnList.lorryNo;
				$scope.cnStatus.carryType=$scope.cnList.carryType;
				$scope.cnStatus.trasittime=$scope.cnList.trasittime;
				$scope.cnStatus.currentStatus=$scope.cnList.currentStatus;
				$scope.cnStatus.arNo=$scope.cnList.arNo;
				$scope.cnStatus.reportingDate=$scope.cnList.reportingDate;
				$scope.cnStatus.unloadingDate=$scope.cnList.unloadingDate;
				$scope.cnStatus.deviation=$scope.cnList.deviation;
				if($scope.cnStatus.currentStatus=="Delivered")
					{
					$scope.ArDBFlag=false;
					}
				else if($scope.cnStatus.currentStatus=="Intransit")
					{
					$scope.ArDBFlag=true;
					}
				
				
				}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCnmtCodeByCode");
		});
/*		var response = $http.post($scope.projectName + '/getStationByCode', stationData);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				console.log(data.toStName);
				$scope.tostName = data.toStName;
				$scope.frStName=data.frStName;
				console.log("To Station Name " +$scope.tostName);
                $scope.cnStatus.fromStation=$scope.frStName;
                $scope.cnStatus.toStation=$scope.tostName;
		}});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCnmtCodeByCode");
		});*/
	}
    $scope.message="";
	$scope.checkingIndentDate=function(cnStatus)
	{
		var date=new Date();
	console.log(date);
	if(angular.isDefined(cnStatus.indentDate))
		{
	var dat2=new Date(cnStatus.indentDate);
		console.log(cnStatus.indentDate);
	if(dat2>date)
		{
		console.log(" i am here");
		$scope.message8="Date Should be less than or equal to Today's Date";
		}
	else
		{
		console.log("Fine");
		$scope.message8="";
		}
		}
	}
	
	$scope.compareIndentPlacementDate=function(cnStatus)
	{
		if(angular.isDefined(cnStatus.indentDate) && angular.isDefined(cnStatus.placementDate))
			{
			if(cnStatus.indentDate>cnStatus.placementDate)
			{
			console.log("I am in the date condition");
			$scope.message="Placement Date should be greater than or equal to Indent Date";
			}
			else
				{
				$scope.message="";
				}
			}
		
	}
	$scope.comparePlacementTripStartDate=function(cnStatus)
	{
		if(angular.isDefined(cnStatus.indentDate) && angular.isDefined(cnStatus.placementDate))
			{
			if(cnStatus.placementDate>cnStatus.tripStartDate)
			{
			console.log("I am in the date condition");
			$scope.message2="Trip Start Date should be greater than or equal to Placement Date";
	
			}
			else
				{
				$scope.message2="";
				}
			}
		
	}
	
$scope.message6="";
	$scope.relianceCnStatus=function(cnStatus)
	{
		console.log(cnStatus.indentDate);
		console.log(cnStatus.currentStatus);
		console.log(cnStatus.arNo);
		console.log(cnStatus.trasittime);
		if(cnStatus.indentDate>cnStatus.placementDate)
			{
			console.log("I am in the date condition");
			$scope.message="Placement Date should be greater than or equal to Indent Date";
			$scope.message2="";
			}
		else if(cnStatus.placementDate>cnStatus.tripStartDate)
			{
			console.log("I am in the date condition");
			$scope.message2="Trip Start Date should be greater than or equal to Placement Date";
			$scope.message="";
			}
	
	   if(cnStatus.cnmtCode.length>=3)
			   {
			if(cnStatus.currentStatus=="Delivered")
				{
/*				if(cnStatus.arNo=="" || cnStatus.arNo==null || angular.isUndefined(cnStatus.arNo))
					{
				$scope.message6="Ar No. is required";
				console.log("I am in error condition");
					}
				else{
*/					$scope.message="";
					$scope.message2="";
					$scope.message6="";
					console.log(" I am in else condition in delivered condition");
					var response=$http.post($scope.projectName + '/saveRelianceCNStatus', cnStatus);
					response.success(function(data, status, headers, config){
					$scope.cnStatus.cnmtCode="";
					$scope.cnStatus.fromStation="";
					$scope.cnStatus.toStation="";
					$scope.cnStatus.indentDate="";
					$scope.cnStatus.placementDate="";
					$scope.cnStatus.tripStartDate="";
					$scope.cnStatus.challanno="";
					$scope.cnStatus.lorryNo="";
					$scope.cnStatus.carryType="";
					$scope.cnStatus.trasittime="";
					$scope.cnStatus.currentStatus="";
					$scope.cnStatus.arNo="";
					$scope.cnStatus.reportingDate="";
					$scope.cnStatus.unloadingDate="";
					$scope.cnStatus.deviation="";
					
				});
					response.error(function(data, status, headers, config){
						console.log("Error in hitting /getCnmtCodeByCode");
					});
				}
//				}
			else 
				{
				$scope.message="";
				$scope.message2="";
				$scope.message6="";
				$scope.cnStatus.arNo="";
				$scope.cnStatus.reportingDate="";
				$scope.cnStatus.unloadingDate="";
				console.log("I am in submission condition");
			var response=$http.post($scope.projectName + '/saveRelianceCNStatus', cnStatus);
			response.success(function(data, status, headers, config){
			$scope.cnStatus.cnmtCode="";
			$scope.cnStatus.fromStation="";
			$scope.cnStatus.toStation="";
			$scope.cnStatus.indentDate="";
			$scope.cnStatus.placementDate="";
			$scope.cnStatus.tripStartDate="";
			$scope.cnStatus.challanno="";
			$scope.cnStatus.lorryNo="";
			$scope.cnStatus.carryType="";
			$scope.cnStatus.trasittime="";
			$scope.cnStatus.currentStatus="";
			$scope.cnStatus.arNo="";
			$scope.cnStatus.reportingDate="";
			$scope.cnStatus.unloadingDate="";
			$scope.cnStatus.deviation="";
			
		});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCnmtCodeByCode");
			});
				}
	}
	   else
		   {
		   $scope.message9="Cnmt Code is too Short";
		   }
			
			
	}
	$scope.message3="";
	$scope.challanData=function(cnmtCode)
	{
		console.log("I am in challan Data Method");
		console.log(cnmtCode);
		if(cnmtCode==null || cnmtCode=="" || angular.isUndefined(cnmtCode)){
			
		$scope.message3="Fill out the Cnmt Code first";
		}
		else
			{
			$scope.message3="";
			var response = $http.post($scope.projectName + '/getChallan', cnmtCode);
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){
					console.log("Hi i am in success");
					
					//$scope.cnStatus.cnmtCode = ""; 
					$scope.cnStatus.challanno=""
					$scope.challanList = data.challanList;
					console.log($scope.challanList);
					$scope.ChallanCodeDBFlag=false;
					$('div#challanCodeDB').dialog({
						autoOpen: false,
						modal:true,
						title: "Challan Code",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#challanCodeDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
					//$scope.cnStatus.cnmtCode = ""; 
					$scope.cnStatus.challanno=""
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCnmtCodeByCode");
			});
			}
	}
	
	$scope.challanDataInField=function(challan)
	{
		console.log("I am in challan field method");
		console.log(challan);
		console.log("Challan Time Allow  "+challan.chlnTimeAllow);
		$('div#challanCodeDB').dialog('close');
		$scope.ChallanCodeDBFlag=true;
		$scope.cnStatus.challanno=challan.chlnCode;
		$scope.cnStatus.lorryNo=challan.chlnLryNo;
		$scope.cnStatus.carryType=challan.chlnVehicleType;
		$scope.cnStatus.trasittime=challan.chlnTimeAllow;
		console.log("Exit from challan id field method");
	}
	
	
	$scope.currentStatusValue=function(currentStatus)
	{
	console.log("Current Status is "+currentStatus);
	$scope.cnStatus.currentStatus=currentStatus;
	console.log("     "+$scope.cnStatus.currentStatus);
		if(currentStatus==="Delivered")
			{
			$scope.ArDBFlag=false;
			}
		else
			{
			$scope.ArDBFlag=true;
			}
	}
	
	
	$scope.message4="";
	$scope.arrivalData=function(challanno)
	{
		console.log("Challan No is "+challanno);
		if(challanno==null || challanno=="" || angular.isUndefined(challanno)){
			
			$scope.message4="Fill out the Challan No first";
			}
		else
			{
			$scope.message4="";
			var response = $http.post($scope.projectName + '/getArrival', challanno);
			
			
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){
					console.log("Hi i am in success");
					
					//$scope.cnStatus.cnmtCode = ""; 
					$scope.cnStatus.arNo=""
					$scope.arList = data.arrivalReportList;
					console.log($scope.arList);
					$scope.ArCodeDBFlag=false;
					$('div#ArCodeDB').dialog({
						autoOpen: false,
						modal:true,
						title: "Arrival Code",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#ArCodeDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
					//$scope.cnStatus.cnmtCode = ""; 
					$scope.cnStatus.arNo=""
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCnmtCodeByCode");
			});
			}
	}
	
	$scope.arDataInField=function(ar)
	{
		console.log("I am in Arrival field method");
		console.log(ar);
		$('div#ArCodeDB').dialog('close');
		$scope.ArCodeDBFlag=true;
	$scope.cnStatus.arNo=ar.arCode;
	$scope.cnStatus.reportingDate=ar.arRepDt;
	$scope.cnStatus.unloadingDate=ar.arDt;
		console.log("Exit from Arrival id field method");
	}
	$scope.message5="";
	
	$scope.getrelianceCnStatus=function(toindentDate,fromindentDate)
	{
		if(toindentDate<fromindentDate)
			{
			$scope.message5="From Indent Date Should be less than or equal to To Indent Date";
			}
		else
			{
			$scope.message5="";
			var indentData={fromIndent : fromindentDate,toIndent : toindentDate };
		var response=	$http.post($scope.projectName + '/fetchRelianceCNStatus', indentData);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				$scope.printXlsFlag = true;
			
		}});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCnmtCodeByCode");
		});
			}
		
	}
	$scope.printCnStatus=function()
	{
		var response=	$http.post($scope.projectName + '/printRelianceCNStatus');
	}
}]);