'use strict';

var app = angular.module('application');

app.controller('CnmtFrmToDtlCntlr',['$scope','$location','$http','$window','$filter',
                            function($scope,$location,$http,$window,$filter){
	
	$scope.CustomerCodeDBFlag=true;
	$scope.toStnCodeDBFlag=true;
	$scope.frmStnCodeDBFlag=true;
	$scope.cnListDBFlag=true;
	$scope.custCode="";
	$scope.frmStnCode="";
	$scope.toStnCode="";
	$scope.frmDt="";
	$scope.toDt="";
	$scope.obj = {};
	$scope.printXLSFlg=false;
	
	$scope.getStation = function(){
		console.log("getStation------>Kamal");
		var response = $http.get($scope.projectName+'/getStationList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("hiiii");
				$scope.stationList = data.list;
				//console.log($scope.stationList);
			}else{
				$scope.alertToast("you don't have any Station");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
			$scope.alertToast("you have exception");
		});
	}();
	
	$scope.getCustomerList = function(){
		console.log("getCustomerData------>Kamal");
		var response = $http.get($scope.projectName+'/getCustomerList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.customerList = data.list;
				//console.log($scope.customerList);
			}else{
				$scope.alertToast("you don't have any cutomer");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}();//it will call automatic itself without calling explicitly 
	
	
	$scope.compaireFY=function(){
		console.log("compaireFY------>Kamal");
	
		var date=$scope.obj.frmDt;
		var response = $http.post($scope.projectName+'/isInCurntFYear' , date);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("Entered Date in current financial year");
			}else{
				$scope.obj.frmDt="";
				$scope.alertToast("Date should be in current Financial year");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.alertToast("Please Enter valid date");
		});
	}
	
	
	
	$scope.OpenCustomerCodeDB = function(){
		$scope.CustomerCodeDBFlag=false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer ",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerCodeDB').dialog('open');
	}
	
	$scope.saveCustCode = function(cust){
		$('div#customerCodeDB').dialog('close');
		$scope.customerName=cust.custName;
		$scope.custCode=cust.custCode;
	}
	
	
	$scope.OpenCnmtToStationDB = function(){
		$scope.toStnCodeDBFlag=false;
		$('div#toStnCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Station ",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#toStnCodeDB').dialog('open');
	}
	
	$scope.saveToStnCode = function(stn,index){
		$('div#toStnCodeDB').dialog('close');
		$scope.toStationName=stn[1];
		$scope.toStnCode=stn[0];
	}
	
	
	$scope.OpenCnmtFromStationDB = function(){
		$scope.frmStnCodeDBFlag=false;
		$scope.getStation
		$('div#frmStnCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Station ",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#frmStnCodeDB').dialog('open');
	}
	
	$scope.saveFrmStnCode = function(stn,index){
		$('div#frmStnCodeDB').dialog('close');
		$scope.frmStationName=stn[1];
		$scope.frmStnCode=stn[0];
	}
	
	
	$scope.getCnmtList = function(){
		console.log($scope.frmStnCode);
		console.log($scope.toStnCode);
		console.log($scope.custCode);
		console.log($scope.obj.frmDt);
		console.log($scope.obj.toDt);
		$scope.cnList=[];
		var userInput={
				"custCode" : $scope.custCode,
				"frmStnCode" : $scope.frmStnCode,
				"toStnCode" : $scope.toStnCode,
				"frmDt" : $scope.obj.frmDt,
				"toDt"  : $scope.obj.toDt
		}
		
		$('#submitId').attr("disabled", "disabled");
		  var response = $http.post($scope.projectName+'/getCnmtFrmToCwise', userInput);
		  response.success(function(data, status, headers, config){
			  $('#submitId').removeAttr("disabled");
			  if(data.result === "success"){
				console.log(data.list);
				$scope.cnList = data.list;
				$scope.cnListDBFlag=false;
				$scope.printXLSFlg=true;
				$scope.alertToast("Success");
			  }else{
				  $scope.printXLSFlg=false;
				  $scope.alertToast("No record found");
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
			  $scope.printXLSFlg=false;
			  $('#submitId').removeAttr("disabled");
		  });
		
	}
	
	
	$scope.clear=function(){
		$scope.toStnCode="";
		$scope.toStationName="";
		$scope.frmStationName="";
		$scope.frmStnCode="";
	}
	
	
	
}]);