package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.PetroCardDAO;
import com.mylogistics.model.PetroCard;

public class PetroCardDAOImpl implements PetroCardDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	/*private Session session;
	private Transaction transaction;*/
	
	
	public PetroCardDAOImpl(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	Logger logger = Logger.getLogger(FYearDAOImpl.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PetroCard> getAllPetroCardByBrhType(String cardType,String bCode){
		List<PetroCard> list=new ArrayList<>();
		
		Session session=this.sessionFactory.openSession();
		try {
			list=session.createCriteria(PetroCard.class)
					.add(Restrictions.eq("cardType", cardType))
					.add(Restrictions.eq("branch", bCode)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PetroCard> getAllPetroCardByType(String cardType){
		List<PetroCard> list=new ArrayList<>();
		
		Session session=this.sessionFactory.openSession();
		try {
			list=session.createCriteria(PetroCard.class)
					.add(Restrictions.eq("cardType", cardType)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PetroCard> getAllPetroCardByBrh(String bCode){
		List<PetroCard> list=new ArrayList<>();
		
		Session session=this.sessionFactory.openSession();
		try {
			list=session.createCriteria(PetroCard.class)
					.add(Restrictions.eq("branch", bCode)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return list;
	}

}
