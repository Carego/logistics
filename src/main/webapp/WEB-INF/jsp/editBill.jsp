<div ng-show="operatorLogin || superAdminLogin">

<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>


	<div class="noprint">
		<form name="editBillForm" ng-submit="editBillSubmit(editBillForm)" class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;">
			<div class="row">
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch" ng-required="true" ng-click="openBrhDB()" readonly>
		       			<label for="code" style="color:black;">Branch</label>	
		       		</div>
		       		
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="billDtId" name ="billName" ng-model="blBillNo"  ng-required="true" ng-click="openBillDB()"  readonly>
		       			<label for="code" style="color:black;">Bill No</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" id="firstSubmit" type ="submit" value="submit" >	
		       		</div>
		    </div>
	</form>
	
	<div id="brhId" ng-hide="openBrhDBFlag">
	    <input  type="text" name="filterBrh" ng-model="filterBrh" placeholder="search....">
	   <table>
	   		<tr>
	   			<th></th>
	   			<th>Branch Name</th>
	   			<th>FaCode</th>
	   		</tr>
	   		<tr ng-repeat="brh in branchList | filter:filterBrh">
	   		    <td><input type="radio" name="brh" value="{{brh}}" ng-model="brhCode" ng-click="saveBranch(brh)"> </td>
	   			 <td>{{brh.branchName}}</td>
	   			 <td>{{brh.branchFaCode}}</td>
	   		</tr>
	   </table>
	</div>
	
 	<div id ="openBillId" ng-hide="openBillDBFlag">
		  <input type="text" name="filterBill" ng-model="filterBill" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bill No.</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bill in billList | filter:filterBill">
		 	  <td><input type="radio"  name="billName"   value="{{bill}}" ng-model="billNoCode" ng-click="saveBill(bill)"></td>
              <td>{{bill.blBillNo}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="openCnmtId" ng-hide="openCnmtDBFlag">
	   <input type="text" name="filterfCnmt" ng-model="filterCnmt" placeholder="Search....."/>
	   <table ng-show="bill.blType == 'N'">
	     <tr>
	     <th></th>
	     <th>Cnmt No.</th>
	     </tr>
	     <tr ng-repeat="cnmt in cnmtList | filter:filterCnmt">
	       <td><input type="radio" name="cnmt"   value="{{ cnmt }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt)"></td>
	       <td>{{cnmt}}</td>	       
	     </tr>
	   </table>
	
	</div>
	
 	
	<div class="noprint" ng-hide="editBillFlag">
		<form name="billForm" ng-submit=billSubmit(billForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
			ng-class = "{'form-error': billForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row">
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="cnmtId" name ="cnmtName" ng-model="cnmtCode" ng-click="selectBuilty()"  readonly>
		       			<label for="code" style="color:black;">Add Builty </label>	
		       		</div>
		    </div>
			    <div ng-show="billDetailSerialList.length > 0">
		    	<div class="row">
					<table class="tblrow">
					<caption class="coltag tblrow">Bill For {{customer.custName}}</caption>
						<tr class="rowclr">
							<th class="colclr">Cn No</th>
							<th class="colclr">Cn Dt.</th>
							<th class="colclr">Lry No</th>
							<th class="colclr">Fr Stn</th>
							<th class="colclr">To Stn</th>
							<th class="colclr">Weight MT</th>
							<th class="colclr">Rate</th>
							<th class="colclr">Freight</th>
							<th class="colclr">Detention</th>
							<th class="colclr">Bonus</th>
							<th class="colclr">Ldng</th>
							<th class="colclr">UnLdng</th>
							<th class="colclr">Other</th>
							<th class="colclr">Total</th>
							<th class="colclr"></th>
						</tr>
						<tr class="tbl" ng-repeat="blDetList in billDetSerList">
							<td class="rowcel" ng-click="editBillDetail($index)" style="cursor: pointer;">{{blDetList.cnmt.cnmtCode}}</td>
							<td class="rowcel">{{blDetList.cnmt.cnmtDt | date : format : timezone}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdLryNo}}</td>
							<td class="rowcel">{{blDetList.frStn}}</td>
							<td class="rowcel">{{blDetList.toStn}}</td>
							<td class="rowcel">
								<div ng-show="blDetList.billDetail.bdBlBase == 'chargeWt'"> 
									{{blDetList.billDetail.bdChgWt / 1000}}
								</div>
								<div ng-show="blDetList.billDetail.bdBlBase == 'actualWt'"> 
									{{blDetList.billDetail.bdActWt / 1000}}
								</div>
								<div ng-show="blDetList.billDetail.bdBlBase =='receiveWt'"> 
									{{blDetList.billDetail.bdRecWt / 1000}}
								</div>
							</td>
							<td class="rowcel">{{blDetList.billDetail.bdRate * 1000 | number:2}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdFreight}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdDetAmt}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdBonusAmt}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdLoadAmt}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdUnloadAmt}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdOthChgAmt}}</td>
							<td class="rowcel">{{blDetList.billDetail.bdTotAmt}}</td>
							<td class="rowcel">
				            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeBillDet($index)" >
									<i class="mdi-action-delete white-text"></i>
								</a>
							</td>
						</tr>
					</table>
				</div>
		    </div>
		    
		    <div ng-show="billDetSerList.length > 0">
		    	<div class="row">
		    	</div>
		    </div>
		    
		    <div class="row">
		   				       		
		     		<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="subTotId" name ="subTotName" ng-model="bill.blSubTot" ng-required="true">
		       			<label for="code" style="color:black;">Sub Total</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.subTotName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Sub total is required.</span>
							</div>	
		       		</div>

					<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="txbSerId" name ="txbSerName" ng-model="bill.blTaxableSerTax" ng-required="true">
		       			<label for="code" style="color:black;">Taxable Service Tax</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.txbSerName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Taxable service tax is required.</span>
							</div>		
		       		</div>
		       		
		       		<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="serId" name ="serName" ng-model="bill.blSerTax" ng-required="true">
		       			<label for="code" style="color:black;">Service Tax</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.serName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Service tax is required.</span>
							</div>	
		       		</div>
		       		
		       		<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="sbcId" name ="sbcName" ng-model="bill.blSwachBhCess" ng-required="true">
		       			<label for="code" style="color:black;">Swachh Bharat Cess</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.sbcName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Swachh Bharat is required.</span>
							</div>	
		       		</div>

					<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="kcId" name ="kkcName" ng-model="bill.blKisanKalCess" ng-required="true">
		       			<label for="code" style="color:black;">Kisan kalyan Cess</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.kkcName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Kisan kalyan Cess is required.</span>
							</div>		
		       		</div>
		       		
					<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="fTotId" name ="fTotName" ng-model="bill.blFinalTot" ng-required="true">
		       			<label for="code" style="color:black;">Final Total</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.fTotName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Final total is required.</span>
							</div>		
		       		</div>
		    </div>
		    
		    <div class="row">
		       		
		       		<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="remAmtId" name ="remAmt" ng-model="bill.blRemAmt" ng-required="true">
		       			<label for="code" style="color:black;">OutStandingAmount</label>
		       			<div class = "text-left errorMargin" ng-messages = "billForm.remAmt.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Outstandingamount is required.</span>
							</div>		
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" id = "secondSubmit" type ="submit" value="submit" >	
		       		</div>
		       		
		    </div>
	     
	</form>
	</div>  
		    
<div id="editcnmtBillId" ng-hide="editcnmtBillFlag" class="noprint">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="cnmtBillForm" ng-submit="editCnBillForm(cnmtBillForm)"
		ng-class = "{'form-error': cnmtBillForm.$invalid && editFormSubmitted}" novalidate = "novalidate">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="date" name="cnmtDt" id="cnmtDtId" ng-model="actCnmt.cnmtDt" ng-required="true" readonly>
					<label style="color:black;">Cnmt Date</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.cnmtDt.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Cnmt date is required.</span>
							</div>
				</div>
				
				<div class="col s4 input-field">
					<input type ="text" name="frStnName" id="frStnId" ng-model="frStn" ng-required="true" readonly />
					<label style="color:black;">From Station</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.frStnName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  From station is required.</span>
							</div>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="text" name="toStnName" id="toStnId" ng-model="toStn" ng-required="true" readonly>
					<label style="color:black;">To Station</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.toStnName .$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  To station is required.</span>
							</div>
				</div>
	     </div>
	     
	     <div class="row">
	     		
				<div class="col s4 input-field">
					<input class="validate" type ="number" name="actWtName" id="actWtId" ng-model="billDet.bdActWt" ng-keyup="calBillTot()" step="0.001" min="0.000" ng-required="true" readonly="billBasisRead">
					<label style="color:black;">Actual Wt(kg)</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.actWtName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Actual wt(kg) is required.</span>
							</div>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type ="number" name="recWtName" id="recWtId" ng-model="billDet.bdRecWt" ng-keyup="calBillTot()" step="0.001" min="0.000"  readonly>
					<label style="color:black;">Receiving Wt(kg)</label>
				</div>
				
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="wtName" id="wtId" ng-model="billDet.bdChgWt" ng-keyup="calBillTot()" step="0.001" min="0.000" ng-required="billBasisRead">
					<label style="color:black;">Charge Wt(kg)</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.wtName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Charge wt(kg) is required.</span>
							</div>
				</div>
		
	     </div>
	     
	     <div class="row">
	     		
	     		<div class="col s4 input-field">
					<input type ="text" name="lryName" id="lryId" ng-model="billDet.bdLryNo" readonly />
					<label style="color:black;">Lorry No</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="rateName" id="rateId" ng-model="billDet.bdRate" ng-keyup="calBillTot()" step="0.000001" min="0.000000" ng-requierd="true" >
					<label style="color:black;">Rate per kg</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.rateName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Rate per kg is required.</span>
							</div>
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="frgName" id="frgId" ng-model="billDet.bdFreight" ng-keyup="calBillTot()" ng-required="true" step="0.001" min="0.000" readonly />
					<label style="color:black;">Freight</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.frgName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Freight is required.</span>
							</div>
				</div>
				
	     </div>
	     
	     
	     <div class="row">
	     
	     		<div class="col s4 input-field">
					<input class="validate" type ="number" name="ldName" id="ldId" ng-model="billDet.bdLoadAmt" ng-keyup="calBillTot()" ng-init="billDet.bdLoadAmt = 0" ng-required="true" >
					<label style="color:black;">Loading</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.ldName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Loading is required.</span>
							</div>
				</div>
			
				 <div class="col s4 input-field">
					<input class="validate" type ="number" name="unldName" id="unldId" ng-model="billDet.bdUnloadAmt" ng-keyup="calBillTot()" ng-init="billDet.bdUnloadAmt = 0" ng-required="true" >
					<label style="color:black;">UnLoading</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.unldName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Loading is required.</span>
							</div>
				</div>
				
	     		<div class="col s4 input-field">
					<input class="validate" type ="number" name="bnsName" id="bnsId" ng-model="billDet.bdBonusAmt" ng-keyup="calBillTot()" ng-init="billDet.bdBonusAmt = 0" ng-required="true">
					<label style="color:black;">Bonus</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.bnsName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Loading is required.</span>
							</div>
				</div>
		
	     </div>
	     
	     <div class="row">
	     
	     		 <div class="col s4 input-field">
					<input class="validate" type ="number" name="detName" id="detId" ng-model="billDet.bdDetAmt" ng-keyup="calBillTot()" ng-init="billDet.bdDetAmt = 0" ng-required="true" >
					<label style="color:black;">Detention</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.detName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Loading is required.</span>
							</div>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type ="number" name="totName" id="totId" ng-model="billDet.bdTotAmt" ng-init="billDet.bdTotAmt = 0" ng-required="true" readonly step="0.001" min="0.000">
					<label style="color:black;">Total Amt</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtBillForm.totName.$error" ng-if = "editFormSubmitted">
								<span ng-message = "required">*  Loading is required.</span>
							</div>
				</div>
				
		  </div>
	     
	    
	    <div class="row"> 
				<div class="col s4 input-field">
					<input type ="button" name="othChgName" id="othChgId" value="Other Charges" ng-click="otChg()">
				</div>
				
	     		<div class="input-field col s4 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	    </div>
	     <div class="row">
	     	<div class="col s12 input-field" ng-show="billDet.bdOthChgList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">OTHER CHARGES</caption>
						<tr>
							<th class="colclr">Type</th>
							<th class="colclr">Value</th>
							<th class="colclr">Action</th>
						</tr>
						<tr ng-repeat="otChg in billDet.bdOthChgList">
							<td class="rowcel">{{otChg.otChgType}}</td>
							<td class="rowcel">{{otChg.otChgValue}}</td>
							<td class="rowcel">
								<input type="button" name="rmvName" id="rmvId" value="REMOVE" ng-click="removeOth($index)"/>
							</td>
						</tr>
					</table>
				</div>
	     </div>

	</form>
		
		<div id="otChgDB" ng-hide="otChgFlag" class="noprint">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="OtherChargeForm" ng-submit="saveOtChg(OtherChargeForm,ot)"
			ng-class = "{'form-error': OtherChargeForm.$invalid && otherChgIsFormSubmitted}" novalidate = "novalidate">
			 <div class="row">
				    <div class="col s6 input-field">
						<select name="otChgTypeName" id="otChgTypeId" ng-model="ot.otChgType" ng-options="otChgType as otChgType for otChgType in otChgTypeList" required></select>
						<label style="color:black;">Type</label>
						<div class = "text-left errorMargin" ng-messages = "OtherChargeForm.otChgTypeName.$error" ng-if = "otherChgIsFormSubmitted">
								<span ng-message = "required">*  Type is required.</span>
							</div>
					</div>
					
					<div class="col s6 input-field">
						<input type="number" name="otChgValueName" id="otChgValueId" ng-model="ot.otChgValue" required>
						<label style="color:black;">Value</label>
						<div class = "text-left errorMargin" ng-messages = "OtherChargeForm.otChgValueName.$error" ng-if = "otherChgIsFormSubmitted">
								<span ng-message = "required">*  Value is required.</span>
							</div>
					</div>
		     </div>
		     
		     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>	
	     </form>
	</div>
		
	</div>
		    
	
	</div>  

</div>