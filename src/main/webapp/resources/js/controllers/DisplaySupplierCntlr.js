'use strict';

var app = angular.module('application');

app.controller('DisplaySupplierCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.showSupplierDetails = true;
	$scope.supplierCodeFlag = true;
	$scope.supplierListFlag = false;
	$scope.availableTags = [];
	$scope.currentSup = {};
	$scope.selection=[];
	$scope.supList = [];
	$scope.supplierModalRadio = false ;
	$scope.supplierAutoRadio = false;
	var idx;
	$scope.sup = {};
	
	$scope.getSupplierForAdmin = function(){
		console.log("Entered into getSupplierForAdmin function------>");
		var response = $http.post($scope.projectName+'/getSupplierForAdmin');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.supList = data.supList;
				$scope.supplierListFlag = false;
				for(var i=0;i<$scope.supList.length;i++){
					$scope.supList[i].creationTS =  $filter('date')($scope.supList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);				
			}else{
				$scope.supplierListFlag = true;
				$scope.alertToast(data.result);
			}	
			$scope.getSupplierCodesForAdmin();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.enableSupCodeModal = function(){
		console.log("enter into enableSupCodeModal function");
		$('#supplierCodeId').removeAttr("disabled");
		$('#ok').attr("disabled","disabled");
		$('#supCodeId').attr("disabled","disabled");
		$scope.supCode="";
	}
	
	$scope.enableAutoSupCodeModal = function(){
		console.log("enter into enableAutoSupCodeModal function");
		$('#supCodeId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		$('#supplierCodeId').attr("disabled","disabled");
		$scope.supplierCode="";
	}
	
	$scope.OpenSupCodeDB = function(){
		console.log("enter into OpenSupCodeDB funtion");
		$scope.supplierCodeFlag = false;
    	$('div#supplierCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#supplierCodeDB').dialog('open');
	}
	
	$scope.saveSupCode = function(sup){
		console.log("enter into saveSupCode function");
		$scope.supplierCode = sup.stSupCode;
		console.log("Value selected is--->"+$scope.supplierCode);
		$('div#supplierCodeDB').dialog('close');
		$scope.supplierCodeFlag = true;
		
		var response = $http.post($scope.projectName+'/stSupplierDetails', $scope.supplierCode);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.supplierListFlag = true;
				$scope.showSupplierDetails = false;
				$scope.currentSup = data.stsupplier;
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}				
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.getSupCodeList = function(){
		console.log("Enter into getSupCodeList function");
	      $( "#supCodeId" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	}
	
	$scope.getSupCodeDetails = function(supCode){
		
		if(angular.isUndefined(supCode) || supCode === null || supCode === ""){
			$scope.alertToast("Please enter supplier code...")
		}else{
			$scope.supplierListFlag=true;
				
			$scope.code = $( "#supCodeId" ).val();
			console.log("$scope.code---------"+$scope.code);
				
			var response = $http.post($scope.projectName+'/stSupplierDetails',$scope.code);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.currentSup = data.stsupplier;
					$scope.showSupplierDetails = false;
				}else if(data.result==="error"){
					$scope.showSupplierDetails = true;
					$scope.alertToast("There is no such branch...")
				}	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	$scope.backToList = function(){
		$scope.showSupplierDetails = true;
		$scope.currentSup = "";
		$scope.supplierCode="";
		$scope.supCode="";
		$scope.supplierModalRadio = false ;
		$scope.supplierAutoRadio = false;
		if(!$scope.supList.length){
			$scope.supplierListFlag = true;
		}else{
			$scope.supplierListFlag = false;
		}
		$('#supplierCodeId').attr("disabled","disabled");
		$('#supCodeId').attr("disabled","disabled");
	}
	
$scope.editSupplier = function(currentSup){
	
	console.log("enter into editSupplier function--->");
	var response = $http.post($scope.projectName+'/editSupplier', currentSup);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.successToast(data.result);
			$scope.currentSup="";
			}else{
			console.log(data);
			}		
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.toggleSelection = function toggleSelection(stSupCode) {
	   idx = $scope.selection.indexOf(stSupCode);
	   
	   // is currently selected
	   if (idx > -1) {
		   $scope.selection.splice(idx, 1);
		   }
	   // is newly selected
	   else {
		   $scope.selection.push(stSupCode);
	   }
	}	  

	$scope.verifyStSupplier = function(){   
		console.log("Entered into verifyStSupplier function---");
		if(!$scope.selection.length){
			$scope.alertToast("You have not selected anything");
		}else{
			var response = $http.post($scope.projectName+'/updateIsViewStSupplier',$scope.selection.toString());
			response.success(function(data, status, headers, config){
				$scope.successToast(data.result);
				$scope.getSupplierForAdmin();
			});     
			response.error(function(data, status, headers, config) {
	           $scope.errorToast(data.result);
			});
		}
	
	}
	
	$scope.getSupplierCodesForAdmin = function(){
		console.log("entered into getSupplierCodesForAdmin function------>");
		var response = $http.post($scope.projectName+'/getSupplierCodesForAdmin');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.supplierCodeList = data.supplierCodeList;
				$scope.availableTags = data.supplierCodeList;
				console.log("******************");
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getSupplierForAdmin();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
	
}]);