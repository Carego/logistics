'use strict';

var app = angular.module('application');

app.controller('CloseVoucherCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){
	$scope.vs = {};
	$scope.penCsList = [];
	$scope.csList = [];
	$scope.acHdList = [];
	$scope.actCsList = [];
	
	$scope.cashCsList = [];
	$scope.bankCsList = [];
	$scope.bankCsTempList = [];
	$scope.contraCsList = [];
	$scope.bkCsMapList = [];
	
	$scope.date = new Date();
	
	$scope.printVsFlag = true;
	$scope.openBal = 0;
	$scope.closeBal = 0;
	$scope.bkOpenBal = 0;
	$scope.bkCloseBal = 0;
	
	$scope.date = new Date();
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVoucherDetFrCls');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				
				$scope.openBal = $scope.vs.cashStmtStatus.cssOpenBal;
				$scope.bkOpenBal = $scope.vs.cashStmtStatus.cssBkOpenBal;
				$scope.brhCode = $scope.vs.branch.branchFaCode;
				$scope.brhName = $scope.vs.branch.branchName;
				$scope.sheetNo = $scope.vs.cashStmtStatus.cssCsNo;
				
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				$scope.getPendingCS();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.closeVoucher = function(){ 
		console.log("enter into closeVoucher function");
		$('#closeVHId').attr("disabled","disabled");
		if(!angular.isUndefined($scope.vs.cashStmtStatus)){
			$scope.vs.penCsId = [];
			if($scope.penCsList.length > 0){
				for(var i=0;i<$scope.penCsList.length;i++){
					$scope.vs.penCsId.push($scope.penCsList[i].csId);	
				}
			}
			var response = $http.post($scope.projectName+'/closeVoucherN',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('#closeVHId').removeAttr("disabled");
					$scope.alertToast("status closed");
					$scope.vs.penCsId = [];
					$scope.penCsList = [];
					$scope.vs = {};
					$scope.penCsList = [];
					
					$scope.actCsList = data.list;
					$scope.acHdList = data.acHdList;
					$scope.bkCsMapList = data.bkCsMapList;
					
					console.log("size of $scope.acHdList = "+$scope.acHdList.length);
					if($scope.acHdList.length > 0){
						for(var i=0;i<$scope.acHdList.length;i++){
							for(var j=i+1;j<$scope.acHdList.length;j++){
								if($scope.acHdList[i].faCode === $scope.acHdList[j].faCode){
									$scope.acHdList.splice(j,1);
									j = j-1;
								}
							}
						}
					}
					console.log("new size of $scope.acHdList = "+$scope.acHdList.length);
					
			
					if($scope.actCsList.length > 0){
						for(var i=0;i<$scope.actCsList.length;i++){
							if($scope.actCsList[i].csVouchType === "CASH" || $scope.actCsList[i].csVouchType === "cash" ){
								$scope.cashCsList.push($scope.actCsList[i]);
							}else if($scope.actCsList[i].csVouchType === "BANK" || $scope.actCsList[i].csVouchType === "bank"){
								var subStr = $scope.actCsList[i].csFaCode.substr(0,2);
								if(subStr !== "07"){
									$scope.bankCsList.push($scope.actCsList[i]);
								}else{
									/*if($scope.actCsList[i].csType === "Fund Transfer"){
										$scope.bankCsList.push($scope.actCsList[i]);
									}else{
										$scope.bankCsTempList.push($scope.actCsList[i]);
									}*/
									
									$scope.bankCsList.push($scope.actCsList[i]);
									
									/*if($scope.actCsList[i].csType === "CashWithdraw(ATM)" || $scope.actCsList[i].csType === "CashWithdraw" || $scope.actCsList[i].csType === "CashDeposite"){
										$scope.bankCsTempList.push($scope.actCsList[i]);
									}else{
										$scope.bankCsList.push($scope.actCsList[i]);
										//$scope.bankCsTempList.push($scope.actCsList[i]);
									}*/
									//$scope.bankCsTempList.push($scope.actCsList[i]);
								}
							}else if($scope.actCsList[i].csVouchType === "CONTRA" || $scope.actCsList[i].csVouchType === "contra" || $scope.actCsList[i].csVouchType === "RTGS" || $scope.actCsList[i].csVouchType === "rtgs"){
								$scope.contraCsList.push($scope.actCsList[i]);
							}
						}
					}
					
					
					if($scope.cashCsList.length > 0){
						$scope.recAmt = $scope.openBal;
						$scope.payAmt = $scope.closeBal;
						for(var i=0;i<$scope.cashCsList.length;i++){
							if($scope.cashCsList[i].csDrCr === 'C'){
								$scope.recAmt = $scope.recAmt + $scope.cashCsList[i].csAmt;
							}else if($scope.cashCsList[i].csDrCr === 'D'){
								$scope.payAmt = $scope.payAmt + $scope.cashCsList[i].csAmt;
							}else{
								
							}
						}
					}else{
						$scope.recAmt = $scope.openBal;
						$scope.payAmt = $scope.closeBal;
					}
					
					
					
					if($scope.bankCsList.length > 0){
						$scope.recBnkAmt = $scope.bkOpenBal;
						$scope.payBnkAmt = $scope.bkCloseBal;
						for(var i=0;i<$scope.bankCsList.length;i++){
							if($scope.bankCsList[i].csDrCr === 'C'){
								$scope.recBnkAmt = $scope.recBnkAmt + $scope.bankCsList[i].csAmt;
							}else if($scope.bankCsList[i].csDrCr === 'D'){
								$scope.payBnkAmt = $scope.payBnkAmt + $scope.bankCsList[i].csAmt;
							}else{
								
							}
						}
					}else{
						$scope.recBnkAmt = $scope.bkOpenBal;
						$scope.payBnkAmt = $scope.bkCloseBal;
					}
					
					
					$scope.printVsFlag = false;
			    	$('div#printVsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.printVsFlag = true;
					    }
						});
					$('div#printVsDB').dialog('open');
					
					//$scope.getVoucherDetails();
				}else{
					console.log(data.result);
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
		$scope.getVoucherDetails();
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
		$('div#printVsDB').dialog('close');
		//alert("holddd..............");
		$scope.getVoucherDetails();
		//alert("holddd..............*****");
	}
	
	
	
	$scope.getPendingCS = function(){
		console.log("enter into getPendingCS function");
		var response = $http.post($scope.projectName+'/getPendingCS',$scope.vs.cashStmtStatus);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.penCsList = data.list;
				$scope.getClosingBal();
			}else{
				$scope.getClosingBal();
				console.log("there is no pending cs");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.getClosingBal = function(){
		console.log("enter into getClosingBal function");
		var response = $http.post($scope.projectName+'/getClosingBal',$scope.vs.cashStmtStatus);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.csList = data.list;
				$scope.closeBal = data.closeBal;
				$scope.bkCloseBal = data.bkCloseBal;
				
				if($scope.closeBal < 0){
					$scope.alertToast("Closing balance must be greater than 0");
					$('#closeVHId').attr("disabled","disabled");
				}else{
					$('#closeVHId').removeAttr("disabled");
				}
			
			}else{
				console.log("error in fetching closing bal");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$('#closeVHId').attr("disabled","disabled"); 
		$scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);