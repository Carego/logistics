package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "hbo_stnnotification")
public class HBO_StnNotification {

	@Id
	@Column(name="hBOSN_Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int hBOSN_Id;
	
	@Column(name="hBOSN_cnmtNo")
	private int hBOSN_cnmtNo;
	
	@Column(name="hBOSN_chlnNo")
	private int hBOSN_chlnNo;
	
	@Column(name="hBOSN_sedrNo")
	private int hBOSN_sedrNo;
	
	@Column(name="hBOSN_hasRec")
	private String hBOSN_hasRec;
	
	@Column(name="hBOSN_isCreat")
	private String hBOSN_isCreate;
	
	@Column(name="hBOSN_opCode")
	private String hBOSN_opCode;
	
	@Column(name="hBOSN_stnId")
	private int hBOSN_stnId;
	
	@Column(name="hBOSN_isView")
	private String hBOSN_isView;
	

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	

	public String gethBOSN_isCreate() {
		return hBOSN_isCreate;
	}

	public void sethBOSN_isCreate(String hBOSN_isCreat) {
		this.hBOSN_isCreate = hBOSN_isCreat;
	}

	public String gethBOSN_isView() {
		return hBOSN_isView;
	}

	public void sethBOSN_isView(String hBOSN_isView) {
		this.hBOSN_isView = hBOSN_isView;
	}

	public String gethBOSN_opCode() {
		return hBOSN_opCode;
	}

	public void sethBOSN_opCode(String hBOSN_opCode) {
		this.hBOSN_opCode = hBOSN_opCode;
	}

	public int gethBOSN_Id() {
		return hBOSN_Id;
	}

	public void sethBOSN_Id(int hBOSN_Id) {
		this.hBOSN_Id = hBOSN_Id;
	}

	public int gethBOSN_cnmtNo() {
		return hBOSN_cnmtNo;
	}

	public void sethBOSN_cnmtNo(int hBOSN_cnmtNo) {
		this.hBOSN_cnmtNo = hBOSN_cnmtNo;
	}

	public int gethBOSN_chlnNo() {
		return hBOSN_chlnNo;
	}

	public void sethBOSN_chlnNo(int hBOSN_chlnNo) {
		this.hBOSN_chlnNo = hBOSN_chlnNo;
	}

	public int gethBOSN_sedrNo() {
		return hBOSN_sedrNo;
	}

	public void sethBOSN_sedrNo(int hBOSN_sedrNo) {
		this.hBOSN_sedrNo = hBOSN_sedrNo;
	}

	public String gethBOSN_hasRec() {
		return hBOSN_hasRec;
	}

	public void sethBOSN_hasRec(String hBOSN_hasRec) {
		this.hBOSN_hasRec = hBOSN_hasRec;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int gethBOSN_stnId() {
		return hBOSN_stnId;
	}

	public void sethBOSN_stnId(int hBOSN_stnId) {
		this.hBOSN_stnId = hBOSN_stnId;
	}
	
	
}
