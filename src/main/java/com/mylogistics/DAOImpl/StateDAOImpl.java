package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.StateDAO;
import com.mylogistics.constants.StateCNTS;
import com.mylogistics.model.Challan;
import com.mylogistics.model.State;
import com.mylogistics.model.User;

public class StateDAOImpl implements StateDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public  StateDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<String> getStateCode(){
		List<String> stateList = new ArrayList<String>();
		try{
		     session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(State.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(StateCNTS.STATE_CODE));
			 cr.setProjection(projList);
			 stateList = cr.list();
			 transaction.commit();
			 session.flush();
		     System.out.println("------->"+stateList);
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return stateList;
	 }
	
	 @Transactional
	 public int saveStateToDB(State state){
		 int temp;
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  
			  List<State> stList = session.createCriteria(State.class)
					 .add(Restrictions.eq(StateCNTS.STATE_NAME, state.getStateName()))
					.list();
			  if(stList.isEmpty()){			  
				  session.save(state);
				  transaction.commit();
				  temp=1;
			  }else{
				  temp=0;
			  }
			  session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		 session.clear();
		session.close();
		return temp;
	}
	 
	 @Transactional
	 public int updateState(State state){
		 System.out.println("Entered into updateState function of stateDAOImpl");
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);	 	
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  state.setCreationTS(calendar);
			  session.update(state);
			  transaction.commit();
			  session.flush();
			  temp=1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		 session.clear();
		session.close();
		return temp;		 
	 }
	   
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<State> getStateData(){
		 List<State> stateList = new ArrayList<State>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         stateList = session.createCriteria(State.class).list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return stateList;
	}
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<State> getStateDataByStaNameCode(String stnNameCode){
		 List<State> stateList = new ArrayList<State>();
		 try{
			 session = this.sessionFactory.openSession();
	         Criteria cr = session.createCriteria(State.class);
	         if(NumberUtils.isNumber(stnNameCode))
	        	 cr.add(Restrictions.like(StateCNTS.STATE_CODE, stnNameCode+"%"));
	         else
	        	 cr.add(Restrictions.like(StateCNTS.STATE_NAME, stnNameCode+"%"));
	         stateList = cr.list();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return stateList;
	}
	 
	 @Transactional
	    public int savePanIssueState(State state){
	        int temp;
	        try{
	            session = this.sessionFactory.openSession();
	            transaction =  session.beginTransaction();
	            session.save(state);
	            transaction.commit();
	            session.flush();
	            temp=1;
	       }catch(Exception e){
	           e.printStackTrace();
	           temp=-1;
	       }
	        session.clear();
	       session.close();
	       return temp;
	}
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getStateLryPrefix(){
		 List<String> stateList = new ArrayList<String>();
		 try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(State.class);
			  ProjectionList projList = Projections.projectionList();
			  projList.add(Projections.property(StateCNTS.STATE_LRY_PREFIX));
			  cr.setProjection(projList);
			  stateList = cr.list();
			  transaction.commit();
			  session.flush();
		}catch(Exception e){
			  e.printStackTrace();
		} 
		 session.clear();
		session.close();
		return stateList;
	}
	 
	 @SuppressWarnings("unchecked")
	 public List<State> retrieveState(String stateCode){
		 List<State> list = new ArrayList<State>();
		 Session session=null;
		 try{
			   session = this.sessionFactory.openSession();
			   Criteria cr=session.createCriteria(State.class);
			   cr.add(Restrictions.eq(StateCNTS.STATE_CODE,stateCode));
			   list =cr.setMaxResults(1).list();
		}
		catch(Exception e){
				e.printStackTrace();
		}
		 session.clear();
		session.close();
		return list; 
	 }
	 
	 @Transactional
	 public long totalCount(){
		 long num = -1;
		 System.out.println("Entered into totalCount function");
		 try{
		     session = this.sessionFactory.openSession();
		     num = (Long) session.createCriteria(State.class).setProjection(Projections.rowCount()).uniqueResult();
		     session.flush();
		 } 
		 catch(Exception e){
	 		 e.printStackTrace();
	 	}
		 session.clear();
	 	session.close();
		return num;
		
	}
	 
	  @Transactional
	  @SuppressWarnings("unchecked")
	  public List<State> getLastStateRow(){
		  List<State> last = new ArrayList<State>();
		  try{
			   session = this.sessionFactory.openSession();
			   last = session.createQuery("from State order by stateId DESC ").setMaxResults(1).list();
			   session.flush();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  session.clear();
		  session.close(); 
		  return last;
	  }

	  @Transactional
	  @Override
	  public List<String> getStateName() {
		  List<String> stateName = new ArrayList<String>();
		  try {
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr = session.createCriteria(State.class);
			  ProjectionList projList = Projections.projectionList();
			  projList.add(Projections.property(StateCNTS.STATE_NAME));
			  cr.setProjection(projList);
			  stateName = cr.list();
			
			  session.flush();
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
		  session.clear();
		  session.close();
		  return stateName;
	  }
	  
	  @Override
	  public List<State> getStateNameByCode(String stateCode) {
		  
		  List<State> state=null ;
		  Session session=null;
		  try {
			  session = this.sessionFactory.openSession();
			  Criteria cr = session.createCriteria(State.class)
					  .add(Restrictions.eq("stateCode", stateCode));
			  ProjectionList projList = Projections.projectionList();
			  projList.add(Projections.property(StateCNTS.STATE_NAME),StateCNTS.STATE_NAME);
			  projList.add(Projections.property(StateCNTS.STATE_GST_CODE),StateCNTS.STATE_GST_CODE);
			  cr.setProjection(projList);
			  cr.setResultTransformer(Transformers.aliasToBean(State.class));
			  state = cr.list();
			
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
		  session.clear();
		  session.close();
		  return state;
	  }
	  
	  
	  @Override
	  public List<State> getStateGSTName(Session session, User user){
	
		  System.out.println("Enter into getStateGSTCodeData()" + user.getUserName());
		  List<State> list=new ArrayList<>();
		  
		  try{
			  Criteria cr=session.createCriteria(State.class)
					  .add(Restrictions.isNotNull(StateCNTS.STATE_GST_CODE));
			  list=cr.list();
		  }catch(Exception e){
			  System.out.println("Exception in getStateGSTName()"+e);
		  }
		  
		  return list;
	  }
}
