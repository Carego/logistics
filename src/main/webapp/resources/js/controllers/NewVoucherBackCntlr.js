'use strict';

var app = angular.module('application');

app.controller('NewVoucherBackCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){

	console.log('NewVoucherCntlr started --->>>');
	
	$scope.bankCodeDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.printVsFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.bankCodeList =[];
	$scope.chqList = [];
	$scope.maxChqAmnt  = 0.0;
	$scope.vs ={};
	$scope.cNo = "";
	
	$scope.voucherSubmit = function(newVoucherForm){
		if(newVoucherForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			if($scope.vs.amount <= 0){
				$scope.alertToast("please enter a valid cheque amount");
			}else{
				console.log("final submittion");
				$scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
			}
		}
	};
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveVsDB').dialog('close');
		$scope.saveVsFlag = true;
	}
	
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$('div#saveVsDB').dialog('close');
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitVoucher',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("success");
				$('#saveId').removeAttr("disabled");
				$scope.alertToast("voucher generated successfully");
				$scope.vs.vhNo = data.vhNo;
				$scope.vs.tvNo = data.tvNo;
				$scope.vs.bnkName = data.bnkName;
				console.log("$scope.vs.bnkName = "+$scope.vs.bnkName);
				
				$scope.printVsFlag = false;
		    	$('div#printVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#printVsDB').dialog('open');
				
				
				/*$scope.vs = {};
				$scope.cNo = "";*/
				//$scope.getVoucherDetails();
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}

	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
		$scope.printVsFlag = true;
		$scope.vs = {};
		$scope.cNo = "";
		$scope.getVoucherDetails();
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
		$('div#printVsDB').dialog('close');
		$scope.printVsFlag = true;
		$scope.vs = {};
		$scope.cNo = "";
		$scope.getVoucherDetails();
	}
	
	
	
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVoucherDetailsBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){				
				$scope.chqList = [];
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.CASH_WITHDRAW;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				console.log("Brach Fa Code === " + data.branch.branchFaCode);
				console.log("Brach Name === " + data.branch.branchName);
				console.log("Sheet No === " + data.cashStmtStatus.cssCsNo);
				console.log("Date === " + data.cashStmtStatus.cssDt);
				console.log("VS Date === " + $scope.vs.cashStmtStatus.cssDt);
				console.log("List of Bank Codes ---->"+data.bankCodeList.length);
				console.log("Bank Code 1 ----->>>"+ data.bankCodeList[0]);
				
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}

	
	
	/*$scope.closeVoucher = function(){
		console.log("enter into closeVoucher function");
		if(!angular.isUndefined($scope.vs.cashStmtStatus)){
			var response = $http.post($scope.projectName+'/closeVoucher',$scope.vs.cashStmtStatus);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("status closed");
					$scope.getVoucherDetails();
				}else{
					copnsole.log(data.result);
					$scope.alertToast("status not close due to internal server error");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}*/
	
	
	
	$scope.openBankCodeDB = function(){
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank Code",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankCodeDBFlag = true;
			}
		});

		$('div#bankCodeDB').dialog('open');
	}  
	
	 $scope.saveBankCode =  function(bankCode){
			console.log("enter into saveBankCode----->"+bankCode);
			$scope.vs.bankCode = bankCode;
			$('div#bankCodeDB').dialog('close');
			$scope.bankCodeDBFlag = true;
			
			var chqDet = {
					"bankCode" : bankCode,
					"CType"    : $scope.vs.chequeType
			}
			
			//if($scope.vs.chequeType === 'C'){
				var response = $http.post($scope.projectName+'/getChequeNo',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.cNo = $scope.vs.chequeLeaves.chqLChqNo;
						$scope.chqList = data.list;
						/*$scope.vs.chequeNo = data.chequeLeaves.chqLChqNo;*/
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						console.log("Error in bringing data from getChequeNo");
						$scope.alertToast("Bank "+bankCode+" does not have any '"+$scope.vs.chequeType+"' type Cheque");
						$scope.vs.bankCode = "";
					}

				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			/*}else{
				console.log("***********************");
			}*/
	}
	 
	 
	 $scope.openChqNoDB = function(){
		 console.log("enter into openChqNoDB function");
		 $scope.chqNoDBFlag = false;
	    	$('div#chqNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.chqNoDBFlag = true;
			    }
				});
			$('div#chqNoDB').dialog('open');
	 }
	 
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function = "+chq.chqLChqNo);
		 $scope.vs.chequeLeaves = chq;
		 $scope.cNo = $scope.vs.chequeLeaves.chqLChqNo;
		 $('div#chqNoDB').dialog('close');
	 }
	 
	 $scope.checkAmount = function(){
		 console.log("enter into checkAmount function--->"+$scope.vs.amount);
		 if(angular.isUndefined($scope.cNo) || $scope.cNo === "" || $scope.cNo === null){
			console.log("****************"); 
		 }else{
			 if($scope.vs.amount >  $scope.vs.chequeLeaves.chqLChqMaxLt){
				 $scope.alertToast("Amount Cannot be greater than " +$scope.maxChqAmnt );
				 $scope.vs.amount = 0;
			 }	
		 } 
	 }
	 
	 $scope.selectChqType = function(){
		 console.log("enter into selectChqType function");
		 if($scope.vs.chequeType === 'M'){
			 $('#chequeNoId').attr("disabled","disabled");
			 $scope.vs.chequeNo = "";
		 }
	 }
	 
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);