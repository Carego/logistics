package com.mylogistics.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.Challan;
import com.mylogistics.services.ConstantsValues;

@Controller
public class PayableRptCntlr {
	
	@Autowired
	ReportDAO reportDao;
	
	List<Challan> challanList=new ArrayList<>();

	@RequestMapping(value="/getPayableData", method=RequestMethod.POST)
	public @ResponseBody Map<String, String> getPayableRptData(@RequestBody Map<String, String> requestMap){
		Map<String, String> responseMap=new HashMap<>();
		Date fromDt=Date.valueOf(requestMap.get("fromDt"));
		Date toDt=Date.valueOf(requestMap.get("toDt"));
		List<Challan> chlnList=reportDao.getPayableReport(fromDt,toDt);
		if(!chlnList.isEmpty()) {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			challanList=chlnList;
		}else
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		
		return responseMap;
	}
	
	
	@RequestMapping(value="/getpPayableRptXLSX", method=RequestMethod.POST)
	public void printPayableRpt(HttpServletResponse response){
		
		XSSFWorkbook workbook=new XSSFWorkbook();
		XSSFSheet sheet=workbook.createSheet("payableRpt");
		sheet.setColumnWidth(0, 10*200);
		sheet.setColumnWidth(1, 35*200);
		sheet.setColumnWidth(2, 35*200);
		sheet.setColumnWidth(3, 35*200);
		sheet.setColumnWidth(4, 35*200);
		
		XSSFRow row=sheet.createRow(0);
		XSSFCell cell=null;
		cell=row.createCell(0);
		cell.setCellValue("BCode");
		
		cell=row.createCell(1);
		cell.setCellValue("Challan Code");
		
		cell=row.createCell(2);
		cell.setCellValue("Challan Date");
		
		cell=row.createCell(3);
		cell.setCellValue("Chln Rem Adv");
		
		cell=row.createCell(4);
		cell.setCellValue("Chln Rem Bal");
		
		int rowToCreate=1;
		XSSFCell xssfCell=null;
		XSSFRow xssfRow=null;
		
		double chlnRemAdv=0.0;
		double chlnRemBal=0.0;
		for (Challan challan: challanList) {
			
			xssfRow=sheet.createRow(rowToCreate);
			xssfCell=xssfRow.createCell(0);
			xssfCell.setCellValue(challan.getbCode());
			
			xssfCell=xssfRow.createCell(1);
			xssfCell.setCellValue(challan.getChlnCode());
			
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			
			xssfCell=xssfRow.createCell(2);
			xssfCell.setCellValue(dateFormat.format(challan.getChlnDt()));
			
			chlnRemAdv=chlnRemAdv+challan.getChlnRemAdv();
			xssfCell=xssfRow.createCell(3);
			xssfCell.setCellValue(challan.getChlnRemAdv());
			
			chlnRemBal=chlnRemBal+challan.getChlnRemBal();
			xssfCell=xssfRow.createCell(4);
			xssfCell.setCellValue(challan.getChlnRemBal());
			
			rowToCreate++;
		}
		
		XSSFCellStyle cellStyle=workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		xssfRow=sheet.createRow(rowToCreate);
		xssfCell=xssfRow.createCell(0);
		sheet.addMergedRegion(new CellRangeAddress(rowToCreate,rowToCreate,0,2));
		xssfCell.setCellValue("Total:");
		xssfCell.setCellStyle(cellStyle);
		
		xssfCell=xssfRow.createCell(3);
		xssfCell.setCellValue(chlnRemAdv);
		
		xssfCell=xssfRow.createCell(4);
		xssfCell.setCellValue(chlnRemBal);
		
		response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=payableRpt.xlsx");
		ServletOutputStream out;
		try {
			out = response.getOutputStream();
			workbook.write(out);
			out.flush();
	        out.close();
	        challanList.clear();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
}
