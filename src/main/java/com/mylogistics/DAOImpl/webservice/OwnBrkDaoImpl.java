package com.mylogistics.DAOImpl.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Blob;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.FetchType;
import javax.persistence.Version;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import sun.security.krb5.internal.crypto.crc32;

import com.itextpdf.text.Image;
import com.mylogistics.DAO.werservice.OwnBrkDao;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.webservice.OwnBrkAppCNTS;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.ImageApp;
import com.mylogistics.model.webservice.OwnBrkApp;
import com.mylogistics.services.ConstantsValues;

public class OwnBrkDaoImpl implements OwnBrkDao {

	@Autowired
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(OwnBrkDaoImpl.class);
	
	private String ownerPanImgPath = "/var/www/html/Erp_Image/Owner/Pan";
	private String ownerDecImgPath = "/var/www/html/Erp_Image/Owner/Dec";
	
	private String brokerPanImgPath = "/var/www/html/Erp_Image/Broker/Pan";
	private String brokerDecImgPath = "/var/www/html/Erp_Image/Broker/Dec";
		
	public OwnBrkDaoImpl(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	@Override
	public Boolean findOwnBrk(OwnBrkApp ownBrkApp){
		Boolean already = true;
		try{
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(OwnBrkApp.class)
					.add(Restrictions.eq(OwnBrkAppCNTS.TYPE, ownBrkApp.getType()))
					.add(Restrictions.eq(OwnBrkAppCNTS.OWN_BRK_NAME, ownBrkApp.getOwnBrkName()));
			if(! ownBrkApp.getOwnBrkPhoneNo().equalsIgnoreCase(""))
				cr.add(Restrictions.eq(OwnBrkAppCNTS.OWN_BRK_PHONE_NO, ownBrkApp.getOwnBrkPhoneNo()));
			List<OwnBrkApp> ownBrkList = cr.list();			
			if(ownBrkList.isEmpty())
				already = false;
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return already;
	}
	
	@Override
	public Map<String, Object> saveOwnBrk(OwnBrkApp ownBrkApp) {
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		logger.info("Enter into saveOwnBrk()...");
		try{
			// Find Already Exist OR NOT
			Boolean already = findOwnBrk(ownBrkApp);
			if(already){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				if(ownBrkApp.getType().equalsIgnoreCase("owner"))
					resultMap.put("msg", "Owner already exists !");
				else
					resultMap.put("msg", "Broker already exists !");
			}else{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();			
				ownBrkApp.setIsPending(true);
				ownBrkApp.setIsCancel(false);
				session.save(ownBrkApp);
				transaction.commit();
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);			
				resultMap.put("msg", "Recorded Successfully !");
			}
		}catch(Exception e){
			logger.error("Error in saveOwnBrk()...");
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");			
		}
		logger.info("Exit from saveOwnBrk()...");
		return resultMap;
	}
	
	@Override
	public Map<String, Object> getPendingOwnBrk(String type){
		logger.info("Enter into getPendingOwners()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			Criteria pendingOwnBrkCriteria = session.createCriteria(OwnBrkApp.class);
			pendingOwnBrkCriteria.add(Restrictions.eq(OwnBrkAppCNTS.IS_PENDING, true));
			pendingOwnBrkCriteria.add(Restrictions.eq(OwnBrkAppCNTS.TYPE, type));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(OwnBrkAppCNTS.OWN_BRK_ID), OwnBrkAppCNTS.OWN_BRK_ID);
			projectionList.add(Projections.property(OwnBrkAppCNTS.OWN_BRK_NAME), OwnBrkAppCNTS.OWN_BRK_NAME);
			projectionList.add(Projections.property(OwnBrkAppCNTS.OWN_BRK_PHONE_NO), OwnBrkAppCNTS.OWN_BRK_PHONE_NO);
			projectionList.add(Projections.property(OwnBrkAppCNTS.TYPE), OwnBrkAppCNTS.TYPE);
			pendingOwnBrkCriteria.setProjection(projectionList);
			pendingOwnBrkCriteria.setResultTransformer(Transformers.aliasToBean(OwnBrkApp.class));
			List<OwnBrkApp> pendingOwnBrkList = pendingOwnBrkCriteria.list();
			if(pendingOwnBrkList.isEmpty()){
				logger.info("No pending Owne !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No pending records !");
			}else{
				logger.info("Pending  : "+pendingOwnBrkList.size());
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("pendingOwnBrkList", pendingOwnBrkList);
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from getPendingOwners()...");
		return resultMap;
	}
	
	@Override
	public Map<String, Object> getOwnBrkDtById(OwnBrkApp ownBrkApp){
		logger.info("Enter into getOwnBrkDtById()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			System.out.println("ID : "+ownBrkApp.getOwnBrkId());
			session = this.sessionFactory.openSession();
			List<OwnBrkApp> ownBrkAppList = session.createCriteria(OwnBrkApp.class)
					.add(Restrictions.eq(OwnBrkAppCNTS.OWN_BRK_ID, ownBrkApp.getOwnBrkId()))
					.setFetchMode(OwnBrkAppCNTS.ADDRESS_LIST, FetchMode.EAGER)
					.list();		
			if(ownBrkAppList.isEmpty()){
				logger.info("No such record found !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such record found !");
			}else{
				logger.info("Owner/Broker found !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("pendingOwnBrk", ownBrkAppList.get(0));
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from getOwnBrkDtById()...");
		return resultMap;
	}
	@Override
	public ImageApp findOwnBrkImg(Integer ownBrkId, String type){
		logger.info("Enter into findOwnBrkImg() ID : "+ownBrkId);
		System.out.println("Img ID : "+ownBrkId);
		ImageApp imageApp = null;
		try{
			session = this.sessionFactory.openSession();
			List<ImageApp> imgAppList = session.createCriteria(ImageApp.class)
					.add(Restrictions.eq("ownBrkId", ownBrkId))
					.add(Restrictions.eq("type", type))
					.list();
			if(imgAppList.isEmpty())
				imageApp = null;
			else
				imageApp = imgAppList.get(0);
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.clear();
		session.close();		
		logger.info("Exit from findOwnBrkImg()...");
		return imageApp;
	}

	@Override
	public void copyImagesOrDeleteOwn(ImageApp imgApp, String ownerCode, Blob panImg, Blob decImg, User user){
		logger.info("Enter into copyImages()...");
		try{
			session = this.sessionFactory.openSession();
			List<Owner> ownList = session.createCriteria(Owner.class)
					.add(Restrictions.eq(OwnerCNTS.OWN_CODE, ownerCode))
					.list();
			
			if(! ownList.isEmpty()){
				
				Owner owner = (Owner) ownList.get(0);
				OwnerImg ownerImg = null;
				Integer imgId = owner.getOwnImgId();
				
				if(imgId != null && imgId > 0)
					ownerImg = (OwnerImg) session.get(OwnerImg.class, imgId);
				else{
					ownerImg = new OwnerImg();
					ownerImg.setbCode(user.getUserBranchCode());
					ownerImg.setUserCode(user.getUserCode());
					ownerImg.setOwnId(owner.getOwnId());
				}
							
				ImageApp img = (ImageApp) session.get(ImageApp.class, imgApp.getId());	
					
				if(panImg == null){
					if(img.getPanPath() != null){
						File readFile = new File(imgApp.getPanPath());
						if(readFile.exists()){
								
							FileInputStream input = new FileInputStream(imgApp.getPanPath());
							FileOutputStream output = new FileOutputStream(ownerPanImgPath+"/"+ownerCode+".pdf");
							byte[] bFile = new byte[(int) readFile.length()];
							input.read(bFile);
							input.close();
							output.write(bFile);
							output.close();
							readFile.delete();
							
							ownerImg.setOwnPanImgPath(ownerPanImgPath+"/"+ownerCode+".pdf");
							owner.setOwnIsPanImg(true);
						}
					}
				}else{
					if(img.getPanPath() != null){
						File file = new File(img.getPanPath());
						if(file.exists())
							file.delete();
					}
				}
					
				if(decImg == null){
					if(img.getDecPath() != null){							
						File readFile = new File(imgApp.getDecPath());		
						
						if(readFile.exists()){
							FileInputStream input = new FileInputStream(imgApp.getDecPath());
							FileOutputStream output = new FileOutputStream(ownerDecImgPath+"/"+ownerCode+".pdf");
							byte[] bFile = new byte[(int) readFile.length()];
							input.read(bFile);
							input.close();
							output.write(bFile);
							output.close();
							readFile.delete();
							
							ownerImg.setOwnDecImgPath(ownerDecImgPath+"/"+ownerCode+".pdf");
							owner.setOwnIsDecImg(true);
						}	
					}
				}else{
					if(img.getDecPath() != null){
						File file = new File(img.getDecPath());
						if(file.exists())
							file.delete();
					}
				}
				
				if(imgId == null || imgId < 0){
					imgId = (Integer) session.save(ownerImg);
					owner.setOwnImgId(imgId);
				}else			
					session.update(ownerImg);
				
				session.update(owner);
					
				if(panImg != null && decImg != null)
					session.delete(img);
					imgApp = null;
				}
					
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from copyImages()...");			
	}
	
	@Override
	public void copyImagesOrDeleteBrk(ImageApp imgApp, String brokerCode, Blob panImg, Blob decImg, User user){
		logger.info("Enter into copyImages()...");
		try{
			session = this.sessionFactory.openSession();
			List<Broker> brkList = session.createCriteria(Broker.class)
					.add(Restrictions.eq(BrokerCNTS.BRK_CODE, brokerCode))
					.list();
			
			if(! brkList.isEmpty()){
				
				Broker broker = (Broker) brkList.get(0);
				BrokerImg brokerImg = null;
				Integer imgId = broker.getBrkImgId();
				
				if(imgId != null && imgId > 0)
					brokerImg = (BrokerImg) session.get(BrokerImg.class, imgId);
				else{
					brokerImg = new BrokerImg();
					brokerImg.setbCode(user.getUserBranchCode());
					brokerImg.setUserCode(user.getUserCode());
					brokerImg.setBrkId(broker.getBrkId());
				}
							
				ImageApp img = (ImageApp) session.get(ImageApp.class, imgApp.getId());	
					
				if(panImg == null){
					if(img.getPanPath() != null){
						File readFile = new File(imgApp.getPanPath());
						if(readFile.exists()){
								
							FileInputStream input = new FileInputStream(imgApp.getPanPath());
							FileOutputStream output = new FileOutputStream(brokerPanImgPath+"/"+brokerCode+".pdf");
							byte[] bFile = new byte[(int) readFile.length()];
							input.read(bFile);
							input.close();
							output.write(bFile);
							output.close();
							readFile.delete();
							
							brokerImg.setBrkPanImgPath(brokerPanImgPath+"/"+brokerCode+".pdf");
							broker.setBrkIsPanImg(true);
						}
					}
				}else{
					if(img.getPanPath() != null){
						File file = new File(img.getPanPath());
						if(file.exists())
							file.delete();
					}
				}
					
				if(decImg == null){
					if(img.getDecPath() != null){							
						File readFile = new File(imgApp.getDecPath());		
						
						if(readFile.exists()){
							FileInputStream input = new FileInputStream(imgApp.getDecPath());
							FileOutputStream output = new FileOutputStream(brokerPanImgPath+"/"+brokerCode+".pdf");
							byte[] bFile = new byte[(int) readFile.length()];
							input.read(bFile);
							input.close();
							output.write(bFile);
							output.close();
							readFile.delete();
							
							brokerImg.setBrkDecImgPath(brokerDecImgPath+"/"+brokerCode+".pdf");
							broker.setBrkIsDecImg(true);
						}	
					}
				}else{
					if(img.getDecPath() != null){
						File file = new File(img.getDecPath());
						if(file.exists())
							file.delete();
					}
				}
				
				if(imgId == null || imgId < 0){
					imgId = (Integer) session.save(brokerImg);
					broker.setBrkImgId(imgId);
				}else			
					session.update(brokerImg);
				
				session.update(broker);
					
				if(panImg != null && decImg != null)
					session.delete(img);
					imgApp = null;
				}
					
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from copyImages()...");		
	}
	
	@Override
	public void updateOwnBrkRecords(Integer ownBrkId, String code){
		logger.info("Enter in udpateOwnRecords()...");		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			System.out.println("ID : "+ownBrkId);
			OwnBrkApp ownBrkApp = (OwnBrkApp) session.get(OwnBrkApp.class, ownBrkId);			
			if(ownBrkApp != null){
				if(ownBrkApp.getType().equalsIgnoreCase("owner")){
					session.createQuery("UPDATE VehicleApp SET vOwnCode = :ownerCode WHERE vOwnName = :ownName")
						.setParameter("ownerCode", code)
						.setParameter("ownName", ownBrkApp.getOwnBrkName())
						.executeUpdate();
					session.createQuery("UPDATE ChallanDTApp SET chdOwnCode = :chdOwnCode WHERE chdOwnName = :chdOwnName")
						.setParameter("chdOwnCode", code)
						.setParameter("chdOwnName", ownBrkApp.getOwnBrkName())
						.executeUpdate();					
				}else if(ownBrkApp.getType().equalsIgnoreCase("broker")){
					session.createQuery("UPDATE VehicleApp SET vBrkCode = :brokerCode WHERE vBrkName = :brkName")
					.setParameter("brokerCode", code)
					.setParameter("brkName", ownBrkApp.getOwnBrkName())
					.executeUpdate();
				session.createQuery("UPDATE ChallanDTApp SET chdBrkCode = :chdBrkCode WHERE chdBrkName = :brkName")
					.setParameter("chdBrkCode", code)
					.setParameter("brkName", ownBrkApp.getOwnBrkName())
					.executeUpdate();
				}
				ownBrkApp.setIsPending(false);
				session.update(ownBrkApp);								
				//session.delete(ownBrkApp);
				transaction.commit();
			}			
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from updateOwnRecords()...");
	}
	
	
}