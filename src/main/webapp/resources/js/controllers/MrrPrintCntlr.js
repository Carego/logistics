'use strict';

var app = angular.module('application');

app.controller('MrrPrintCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	$scope.brhList = [];
	$scope.brhId = 0;
	$scope.selBrh = {};
	$scope.branch = "";
	$scope.mrList = [];
	
	$scope.openBrhFlag = true;
	$scope.printVsFlag = true;
	
	$scope.getMrrBranch = function(){
		console.log("enter into getMrBranch function");
		var response = $http.post($scope.projectName+'/getMrrBranch');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.brhList = data.list;
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.openBrhFlag = false;
		$('div#openBrhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.openBrhFlag = true;
		    }
			});
		
		$('div#openBrhId').dialog('open');	
	}
	
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branch = brh.branchName;
		$scope.brhId = brh.brhId;
		$scope.selBrh = brh;
		$('div#openBrhId').dialog('close');
	}
	
	
	$scope.mrrPrintSubmit = function(mrrPrintForm){
		console.log("enter into mrrPrintSubmit function = "+mrrPrintForm.$invalid);
		if(mrrPrintForm.$invalid){
			$scope.alertToast("please fill the correct form");
		}else{
				var req = {
						"brhId" : $scope.brhId,
						"mrDt"  : $scope.mrDt
				};
				
				var response = $http.post($scope.projectName+'/mrrPrintSubmit',req);
				  response.success(function(data, status, headers, config){
					  if(data.result === "success"){
						 $scope.mrList = data.list;
						 $scope.totAmt = 0;
						 if($scope.mrList.length > 0){
							 for(var i=0;i<$scope.mrList.length;i++){
								 console.log("$scope.totAmt + $scope.mrList[i].mr.mrNetAmt = "+$scope.mrList[i].mr.mrNetAmt);
								 $scope.totAmt = $scope.totAmt + $scope.mrList[i].mr.mrNetAmt; 
							 } 
						 }
						 console.log("$scope.totAmt = "+$scope.totAmt);
						 $scope.printVsFlag = false;
					    	$('div#printVsDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								show: UDShow,
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.printVsFlag = true;
							    }
								});
							$('div#printVsDB').dialog('open');
					  }else{
						  $scope.alertToast(data.msg);
					  }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
				   });
		}
	}
	
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
	}	
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getMrrBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);