package com.mylogistics.constants;

public class PenaltyBonusDetentonCNTS {

		public static final String PBD_ID="pbdId";
		
		public static final String PBD_CONT_CODE="pbdContCode";
		
		public static final String PBD_FROM_STATION="pbdFromStn";
		
		public static final String PBD_TO_STATION="pbdToStn";
		
		public static final String PBD_START_DT="pbdStartDt";
		
		public static final String PBD_END_DT="pbdEndDt";
		
		public static final String PBD_FROM_DLAY="pbdFromDlay";
		
		public static final String PBD_TO_DLAY="pbdToDlay";
		
		public static final String PBD_VEHICLE_TYPE="pbdVehicleType";
		
		public static final String PBD_PEN_BON_DET="pbdPenBonDet";
		
		public static final String PBD_HOUR_N_DAY="pbdHourNDay";
		
		public static final String PBD_AMT="pbdAmt";
}
