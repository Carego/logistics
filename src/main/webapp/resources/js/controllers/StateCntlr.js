'use strict';

var app = angular.module('application');

app.controller('StateCntlr',['$scope','$location','$http','$window','$filter',
	                                 function($scope,$location,$http,$window,$filter){	
	
	 $scope.state = {};
	 $scope.viewStateFlag = true;
	 $scope.stateList =[];
	 var code;
	 $scope.type=[];

	
	$scope.Submit = function(StateForm,state,addstateLryPrefix1,addstateLryPrefix2,addstateLryPrefix3){
		console.log("enter into submit function");
		$scope.state.stateName = state.stateName.toUpperCase();
		$scope.code = state.stateName+state.stateSTD;
	
		if(StateForm.$invalid){
			if(StateForm.stateName.$invalid){
				$scope.alertToast("Please enter State Name between 3-40 characters.... ");
			}else if(StateForm.addstateLryPrefix1.$invalid){
				$scope.alertToast("State Lorry Prefix should be of minimum 2 characters... ");
			}else if(StateForm.addstateLryPrefix2.$invalid){
				$scope.alertToast("State Lorry Prefix should be of minimum 2 characters... ");
			}else if(StateForm.addstateLryPrefix3.$invalid){
				$scope.alertToast("State Lorry Prefix should be of minimum 2 characters... ");
			}else if(StateForm.stateSTD.$invalid){
				$scope.alertToast("Please enter State STD of 2-5 digits... ");
			}
		}else{
			if($.inArray($scope.code,$scope.type)!==-1){
				$scope.alertToast("State already exists");
			}else{
				if(angular.isUndefined(addstateLryPrefix3) || addstateLryPrefix3 === null || addstateLryPrefix3 === ""){
			    	if(angular.isUndefined(addstateLryPrefix2) || addstateLryPrefix2 === null || addstateLryPrefix2 === ""){
			    		state.stateLryPrefix = addstateLryPrefix1;
				    	$scope.sendStateData(state);
			    	}
			    	else if(!(angular.isUndefined(addstateLryPrefix2)) || !(addstateLryPrefix2 === null) || !(addstateLryPrefix2 === "")){
			    		state.stateLryPrefix = addstateLryPrefix1+"/"+addstateLryPrefix2;
			    		$scope.sendStateData(state);
			    	}
			     }else if(!(angular.isUndefined(addstateLryPrefix3)) || !(addstateLryPrefix3 === null) || !(addstateLryPrefix3 === "")){
						if(angular.isUndefined(addstateLryPrefix2) || addstateLryPrefix2 === null || addstateLryPrefix2 === ""){
							$scope.alertToast("please fill correct entry in previous state lorry textbox of maximum 3 characters...");
						}else if(!(addstateLryPrefix3 === "")){
							state.stateLryPrefix = addstateLryPrefix1+"/"+addstateLryPrefix2+"/"+addstateLryPrefix3;
							$scope.sendStateData(state);
					    }else if(addstateLryPrefix3 === ""){
					    	state.stateLryPrefix = addstateLryPrefix1+"/"+addstateLryPrefix2;
							$scope.sendStateData(state);
					    }
				  }else if(!(angular.isUndefined(addstateLryPrefix2)) || !(addstateLryPrefix2 === null) || !(addstateLryPrefix2 === "")){
				    	if(angular.isUndefined(addstateLryPrefix3) || addstateLryPrefix3 === null || addstateLryPrefix3 === ""){
				    		state.stateLryPrefix = addstateLryPrefix1+"/"+addstateLryPrefix2;
				    		$scope.sendStateData(state);
				    	}else if(!(addstateLryPrefix2 === "")){
				    		state.stateLryPrefix = addstateLryPrefix1+"/"+addstateLryPrefix2+"/"+addstateLryPrefix3;
				    		$scope.sendStateData(state);
				    	}else if(addstateLryPrefix2 === ""){
				    		state.stateLryPrefix = addstateLryPrefix1+"/"+addstateLryPrefix3;
				    		$scope.sendStateData(state);
				    	}
				   }	
			}
			
		}
	}
	
	
	$scope.sendStateData = function(state){
		console.log("enter into sendStateData fucntion");
		
		$scope.viewStateFlag = false;
		$('div#viewStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "View State Details",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
			}
		});
		$('div#viewStateDB').dialog('open');
	}
	
	$scope.saveState = function(state){
		console.log("Entered into saveState function----> ");
		
		var response = $http.post($scope.projectName+'/submitState', state);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewStateDB').dialog('close');
				$scope.successToast(data.result);
				$scope.state.stateName="";
				$scope.addstateLryPrefix1="";
				$scope.addstateLryPrefix2="";
				$scope.addstateLryPrefix3="";
				$scope.state.stateSTD="";
			    $scope.getAllStateDetails();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.closeStateDB = function(){
		$('div#viewStateDB').dialog('close');
	}
		    
	    $('#std').keypress(function(key) {
		       if(key.charCode < 48 || key.charCode > 57)
		           return false;
	   });

	    $('#std').keypress(function(e) {
		       if (this.value.length == 5) {
		           e.preventDefault();
		       }
		});
	    
	    $('#stateName').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		});
	    
	    $('#addstateLryPrefix1').keypress(function(e) {
		       if (this.value.length == 3) {
		           e.preventDefault();
		       }
		});
	    
	    $('#addstateLryPrefix2').keypress(function(e) {
		       if (this.value.length == 3) {
		           e.preventDefault();
		       }
		});
	    
	    $('#addstateLryPrefix3').keypress(function(e) {
		       if (this.value.length == 3) {
		           e.preventDefault();
		       }
		});
	    
	    $scope.getAllStateDetails = function(){
	    	console.log("Entered into getAllStateDetails function");
	    	var response = $http.post($scope.projectName+'/getStateDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stateList = data.list;
					for(var i=0;i<$scope.stateList.length;i++){
							$scope.type[i] = $scope.stateList[i].stateName+$scope.stateList[i].stateSTD;
							$scope.type.push($scope.stateList[i].stateName+$scope.stateList[i].stateSTD);
							console.log($scope.type[i]);
					}
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	    } 
	    
	   
	    if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	    	 $scope.getAllStateDetails();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		   		    
	   
	
}]);