'use strict';

var app = angular.module('application');

app.controller('EditCashStmtCntlr',['$scope','$location','$http','$filter','$window', 'FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){

	console.log("EditCashStmtCntlr");
	
	$scope.faCodeNameList = [];
	$scope.faCodeNameListForBnk = [];
	$scope.branchNCIList = [];
	$scope.branch = {};
	$scope.cashStmtList = [];
	$scope.cashStmt = {};
	
	//edited cs
	$scope.csList = [];
	$scope.cs = {};
	
	$scope.showCSTableFlag = false;
	
	$scope.faMstrDBFlag = true;
	$scope.branchDBFlag = true;
	$scope.cashStmtDBFlag = true;
	$scope.faMstrForBnkDBFlag = true;
	
	$scope.getActiveBrNCI = function() {
		console.log("getActiveBrNCI Entered");
		var response = $http.post($scope.projectName+'/getActiveBrNCI');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getActiveBrNCI Success");
				$scope.branchNCIList = data.branchNCIList;
				$scope.getFaMstr();
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrNCI Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getActiveBrNCI Error: "+data);
		});
	}
	
	$scope.getFaMstr = function(){
		console.log("getFaMstr Entered");
		var response = $http.post($scope.projectName+'/getFaMstr');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getFaMstr Success");
				$scope.faCodeNameList = data.faCodeNameList;
				$scope.faCodeNameListForBnk = data.faCodeNameListForBnk;
				/*for ( var i = 0; i < $scope.faCodeNameList.length; i++) {
					console.log("code: "+$scope.faCodeNameList[i].faMfaCode);
					console.log("Name: "+$scope.faCodeNameList[i].faMfaName);
				}*/
			}else {
				$scope.alertToast("faMstr: "+dara.result);
				console.log("getFaMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getFaMstr Error: "+data);
		});
	}
	
	//open different dialog for bank 
	$scope.openFaMstrDB = function(faCode){
		console.log("Entered into openFaMstrDB");
		console.log("faCode: "+faCode);
		
		if (faCode.substring(0, 2) === "07") {
			//open bank db
			$scope.faMstrForBnkDBFlag = false;
			$('div#faMstrForBnkDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Financial Codes",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.faMstrForBnkDBFlag = true;
				}
			});

			$('div#faMstrForBnkDB').dialog('open');
		} else {
			//other db
			$scope.faMstrDBFlag = false;
			$('div#faMstrDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Financial Codes",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.faMstrDBFlag = true;
				}
			});

			$('div#faMstrDB').dialog('open');
		}
	}
	
	$scope.saveFaMstr = function(faCodeName){
		$scope.faCode = faCodeName.faMfaCode;
		$('div#faMstrDB').dialog('close');
	}
	
	$scope.saveFaMstrForBnk = function(faCodeNameForBnk){
		$scope.faCode = faCodeNameForBnk.faMfaCode;
		$('div#faMstrForBnkDB').dialog('close');
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		$scope.branch = branch;
	}
	
	$scope.closeBranchDB =  function(){
		$('div#branchDB').dialog('close');
	}
	
	$scope.getCSSubmit = function(editCSForm){
		if(editCSForm.$invalid){
			if (editCSForm.cssDtName.$invalid) {
				$scope.alertToast("Please Enter Valid Date");
			} else if (editCSForm.csVouchNoName.$invalid) {
				$scope.alertToast("Please Enter Valid Voucher No");
			} else if (editCSForm.branchName.$invalid) {
				$scope.alertToast("Please Enter Branch");
			}
		}else{
			
			var editCsService = {
					"cssDt"		: $scope.cssDt,
					"csVouchNo"	: $scope.csVouchNo,
					"bCode"		: $scope.branch.branchId
			}
			
			var response = $http.post($scope.projectName+'/getCsForEdit', editCsService);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					console.log("getCsForEdit Success");
					$scope.cashStmtList = data.cashStmtList;
					$scope.cashStmt = {};
					$scope.showCSTableFlag = false;
					$scope.openCashStmtDB();
				}else {
					console.log("getCsForEdit Error"+data.result);
					$scope.alertToast(data.resultFromDb);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getCsForEdit Error: "+data);
			});
			
		}
	};
	
	$scope.openCashStmtDB = function(){
		console.log("Entered into openCashStmtDB");
		$scope.cashStmtDBFlag = false;
		$('div#cashStmtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Cash Statement",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.cashStmtDBFlag = true;
			}
		});

		$('div#cashStmtDB').dialog('open');
	}
	
	$scope.saveCashStmt = function(cashStmt){
		$scope.cashStmt = cashStmt;
		$('div#cashStmtDB').dialog('close');
		$scope.showCSTableFlag = true;
		
		$scope.faCode = $scope.cashStmt.csFaCode; 
		$scope.description = $scope.cashStmt.csDescription;
		$scope.amt = $scope.cashStmt.csAmt;
		$scope.drCr = $scope.cashStmt.csDrCr;
		
	}
	
	$scope.editCS = function() {
		console.log("editCS()");
		$scope.cs.csFaCode = $scope.faCode;  
		$scope.cs.csDescription = $scope.description;
		$scope.cs.csAmt = $scope.amt;
		$scope.cs.csDrCr = $scope.drCr;
		
		if(angular.isUndefined($scope.amt) || $scope.amt === "" || $scope.amt === null || $scope.amt < 0){
			$scope.alertToast("Enter valid amount");
		}else{
			$scope.csList.push($scope.cs);
		}
		$scope.cs = {};
	}
	
	$scope.removeCs = function (cs, index){
		console.log("removeCs()");
		$scope.csList.splice(index, 1);
	}
	
	$scope.submitCs = function() {
		console.log("submitCs()");
		
		var tempEditCSSum = 0;
		
		for ( var i = 0; i < $scope.csList.length; i++) {
			tempEditCSSum = tempEditCSSum + $scope.csList[i].csAmt; 
		}
		
		console.log("tempEditCSSum: "+parseInt(tempEditCSSum).toFixed(2));
		console.log("cashStmt.csAmt: "+parseFloat($scope.cashStmt.csAmt).toFixed(2))
		
		if (parseFloat(tempEditCSSum).toFixed(2) === parseFloat($scope.cashStmt.csAmt).toFixed(2)) {
			var csService = {
					"cashStmtList"	: $scope.csList,
					"cashStmt"		: $scope.cashStmt,
					"cssDt"			: $scope.cssDt,
					"csVouchNo"		: $scope.csVouchNo,
					"bCode"			: $scope.branch.branchId
			}
				
			
			$('#submitCsId').attr("disabled","disabled");
			
			var response = $http.post($scope.projectName+'/saveCsForEdit', csService);
			response.success(function(data, status, headers, config){
					
				if (data.result === "success") {
					console.log("getCsForEdit Success");
					
					$('#submitCsId').removeAttr("disabled");
					
					//emptied the resources
					$scope.branch = {};
					$scope.cashStmtList = [];
					$scope.cashStmt = {};
					
					$scope.csList = [];
					$scope.cs = {};
					
					$scope.cssDt = "";
					$scope.csVouchNo = "";
					
					$scope.showCSTableFlag = false;
					
					$scope.faMstrDBFlag = true;
					$scope.branchDBFlag = true;
					$scope.cashStmtDBFlag = true;
					
					$scope.alertToast("Updated Successfully");
					
				}else {
					console.log("getCsForEdit Error"+data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getCsForEdit Error: "+data);
			});
		} else {
			$scope.alertToast("CashStatement's Amount Sum in not valid");
		}
		
		
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrNCI();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("EditCashStmtCntlr Ended");
}]);