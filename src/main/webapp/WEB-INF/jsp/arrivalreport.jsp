<!-- <style>
.ui-dialog {
    overflow-y: hidden !important;
}
</style> -->
<div ng-show="operatorLogin || superAdminLogin">
<title>Arrival Report</title>
<div class="row">
	<!-- <div class="col s2 hide-on-med-and-down">&nbsp;</div> -->
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="ArrivalReportForm"
		ng-submit="submitAR(ArrivalReportForm,ar,manualArCode,compArCode)">
		<div class="row">

			<div class="input-field col s3">
				<select name="arRepType" ng-model="ar.arRepType" required
					ng-change="fillArCode(ar.arRepType)">
					<option value="Manual">Manual</option>
					<option value="Computerized">Computerized</option>
				</select> <label for="arRepType">Arrival Report Type</label>
			</div>

			<!-- <div class="input-field col s3">
				<input type="text" name="branchCode" id="branchCode"
					ng-model="arbrCodeTemp" ng-click="OpenBranchCodeDB()" required
					readonly> <label>Branch Code</label>
			</div>
			<div class="input-field col s3" ng-hide="manualArCodeFlag">
				<input class="validate" id="arId" type="text" name="manualArCode"
					ng-model="ar.arCode" ng-click="OpenARCodeDB()" readonly> 
					<label for="StateName">Arrival
					Report Code</label>
			</div> -->
			<!-- <div class="input-field col s3" ng-hide="compArCodeFlag">
				<input class="validate" id="arId" type="text" name="compArCode"
					ng-model="ar.compArCode" ng-click="OpenARCodeDB()" readonly>
				<label for="StateName">Arrival Report Code</label>
			</div> -->
			<div class="input-field col s3" ng-hide="issueDtHideFlg">
					<input id="issueDtId" type="date" name="arIssueDtName"
					ng-model="ar.arIssueDt" ng-blur="isInFY()" ng-required="issuDtRqrFlg"> 
					<label >Arrival Issue Date</label>
			</div>
			
		</div>
		<div class="row">
			<div class="input-field col s3">
				<input type="text" name="archlnCodeName" id="archlnCodeId" ng-model="ar.archlnCode"
					ng-blur="getArChlnCode(ar.archlnCode)" required > <label
					for="StateName">Challan Code</label>
			</div>
			<div class="input-field col s3">
				<input id="StateName" type="date" name="arRepDt"
					ng-model="ar.arRepDt" ng-blur="calLateDel()" ng-required="true"> <label>Arrival
					Report Date</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" id="arRcvWt" type="number" name="arRcvWt"
					ng-model="ar.arRcvWt" ng-required="true" ng-minlength="1"
					ng-maxlength="9" step="0.00001" min="0.00000"
					ng-blur="checkRecWt(recWtPer)"> <label for="StateName">
					Received Weight
				</label>
			</div>
			<div class="input-field col s3">
				<select name="recWtPerName" id="recWtPerId" ng-model="recWtPer"
					ng-init="recWtPer = 'Ton'" ng-change="checkRecWt(recWtPer)"
					required>
					<option value="Ton">Ton</option>
					<option value="Kg">Kg</option>
				</select>
			</div>
		</div>
		
		
		<div class="row">
			<div class="input-field col s3">
					<input class="validate" id="arPkg" type="number" name="arPkg"
						ng-model="ar.arPkg" ng-minlength="1" ng-maxlength="9"
						ng-required="true" ng-blur="checkArPkg()" min="0" > <label
						for="StateName">Package</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" id="arExtKmId" type="number"
					name="arExtKmName" ng-model="ar.arExtKm" ng-keyup="calClaim()" step="0.00001" min="0.00000"
					ng-minlength="1" ng-maxlength="9" ng-init="ar.arExtKm = 0"> <label for="arExtKmName">
					<i class="fa fa-inr"></i> Extra Km
				</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" id="arOvrHgtId" type="number"
					name="arOvtHgtName" ng-model="ar.arOvrHgt" ng-keyup="calClaim()" step="0.00001" min="0.00000"
					ng-minlength="1" ng-maxlength="9" ng-init="ar.arOvrHgt = 0"> <label for="arOvtHgtName">
					<i class="fa fa-inr"></i> Over Height
				</label>
			</div>
			<div class="input-field col s3">
				<input class="validate" id="arPenaltyId" type="number"
					name="arPenaltyName" ng-model="ar.arPenalty" ng-keyup="calClaim()" step="0.00001" min="0.00000"
					ng-minlength="1" ng-maxlength="9" ng-init="ar.arPenalty = 0"> <label for="arPenaltyName">
					<i class="fa fa-inr"></i> Penalty
				</label>
			</div>
			
		</div>

		<div class="row">
			<div class="input-field col s3">
					<input id="arWtShrtgId" type="number"
						name="arWtShrtgName" ng-model="ar.arWtShrtg" step="0.00001" min="0.00000"
						ng-minlength="1" ng-maxlength="9" ng-init="ar.arWtShrtg = 0" readonly/> <label for="arWtShrtgName">
						<i class="fa fa-inr"></i> Weight Shortage
					</label>
			</div>
			
			<div class="input-field col s3">
					<input id="arDrRcvrWtId" type="number"
						name="arDrRcvrWtName" ng-model="ar.arDrRcvrWt" step="0.00001" min="0.00000"
						ng-minlength="1" ng-maxlength="9" ng-init="ar.arDrRcvrWt = 0" readonly/> <label for="arDrRcvrWtName">
						<i class="fa fa-inr"></i> Driver(Claim)
					</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" id="arOther" type="number" name="arOther" ng-keyup="calClaim()"
					ng-model="ar.arOther" step="0.00001" min="0.00000" ng-minlength="1"
					ng-maxlength="9" ng-init="ar.arOther = 0"> <label for="arOther"> <i
					class="fa fa-inr"></i> Others(Claim)
				</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" id="arClaim" type="number" name="arClaim"
					ng-model="ar.arClaim" ng-init="ar.arClaim=0" step="0.00001" min="0.00000" ng-minlength="1"
					ng-maxlength="9" ng-click="calClaim()" readonly> <label for="StateName"> <i
					class="fa fa-inr"></i> Claim
				</label>
			</div>
			
		</div>

		<div class="row">
		
			<div class="input-field col s3">
				<input class="validate" id="arUnloading" type="number"
					name="arUnloading" ng-model="ar.arUnloading" step="0.00001" min="0.00000"
					ng-minlength="1" ng-maxlength="9"> <label for="StateName">
					<i class="fa fa-inr"></i> Unloading
				</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" id="arDetention" type="number"
					name="arDetention" ng-model="ar.arDetention" step="0.00001" min="0.00000"
					ng-minlength="1" ng-maxlength="9"> <label for="StateName">
					<i class="fa fa-inr"></i> Detention
				</label>
			</div>
					
			<div class="input-field col s3">
				<input id="StateName" type="number" name="arLateDelivery"
					ng-model="ar.arLateDelivery" ng-init="ar.arLateDelivery=0" step="0.00001" min="0.00000"
					ng-minlength="1" ng-maxlength="9" readonly> <label> <i
					class="fa fa-inr"></i> Late Delivery Charge
				</label>
			</div>
			<div class="input-field col s3">
				<input id="StateName" type="number" name="arLateAck"
					ng-model="ar.arLateAck" ng-init="ar.arLateAck=0" step="0.00001" min="0.00000" ng-minlength="1"
					ng-maxlength="9" readonly> <label> <i class="fa fa-inr"></i>
					Late Ack. Charge
				</label>
			</div>
			
		</div>

		<div class="row">
			<div class="input-field col s3">
				<input class="validate" id="arDt" type="date" name="arDt"
					ng-model="ar.arDt" ng-blur="unldDtInFY()" required> <label for="StateName">Unloading
					Date</label>
			</div>
			<div class="input-field col s3">
				<input id="StateName" type="time" name="arRepTime"
					ng-model="ar.arRepTime" ng-required="true"> <label>Reporting
					Time</label>
			</div>
			
			<div class="input-field col s3">
				<select name="arSrtgDmgType" ng-model="ar.arSrtgDmg" required
					ng-blur="fillArSrtgDmgType()">
					<option value="OK">OK</option>
					<option value="SHORTAGE">SHORTAGE</option>
					<option value="DAMAGE">DAMAGE</option>
				</select> <label for="arSrtgDmgType">Shortage/Damage Report</label>
			</div>
			
		</div>
		
		<div ng-show="resList.length > 0">
			<div ng-repeat="res in resList">
				<div class="row">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="arImage[$index]">
						<label>Choose File for {{ res.cnmt.cnmtCode }}</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadArImage(res,arImage[$index])">Upload</a>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="ar.arDesc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    	</div>	

		<div class="row">
			<div class="col s12 center">
				<input class="btn" type="submit" value="Submit">
			</div>
		</div>
	</form>

	<div class="col s2 hide-on-med-and-down">&nbsp;</div>
</div>

<div id="arCodesDB" ng-hide="ARCodesDBFlag">

	<input type="text" name="filterARCodes"
		ng-model="filterARCodes" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>SEDR Code</th>
		</tr>
		<tr ng-repeat="ar in arCodeList | filter:filterARCodes">
			<td><input type="radio" name="arCode" id="arId"
				value="{{ ar.brsLeafDetSNo }}" ng-model="arCode"
				ng-click="saveARCodes(ar)"></td>
			<td>{{ ar.brsLeafDetSNo }}</td>
		</tr>
	</table>
</div>

<div id="wtSrtCnmtDB" ng-hide="wtSrtCnmtDBFlag">
	<table>
		<tr ng-repeat="res in resList">
			<td>{{res.cnmt.cnmtCode}}</td>
			<td><input type="number" id="wtSrtgId" name="wtSrtgName" ng-model="wtSrtg[$index]" ng-blur="calWtSrtgByCN(res.cnmt.cnmtCode,res.cnmt.cnmtVOG,res.cnmt.cnmtActualWt,wtSrtg[$index])"/></td>
			<!-- <td><input type="number" id="wtSrtgRsId" name="wtSrtgRsName" ng-model="wtSrtgRs[$index]" value="{{(res.cnmt.cnmtVOG / res.cnmt.cnmtActualWt) * wtSrtg[$index]}}"/></td> -->
		</tr>
		<tr>
			<td></td>
			<td><input type="button" value="Submit" ng-click="saveWtSrtg()"/></td>
			<td></td>
		</tr>
	</table>
</div>


<div id="qSrtCnmtDB" ng-hide="qSrtCnmtDBFlag">
	<table>
		<tr ng-repeat="res in resList">
			<td>{{res.cnmt.cnmtCode}}</td>
			<td><input type="number" id="qSrtgId" name="qSrtgName" ng-model="qSrtg[$index]" ng-blur="calQSrtgByCN(res.cnmt.cnmtCode,res.cnmt.cnmtVOG,res.cnmt.cnmtNoOfPkg,qSrtg[$index])"/></td>
			<!-- <td><input type="number" id="qSrtgRsId" name="qSrtgRsName" ng-model="qSrtgRs[$index]" value="{{(res.cnmt.cnmtVOG / res.cnmt.cnmtActualWt) * wtSrtg[$index]}}"/></td> -->
		</tr>
		<tr>
			<td></td>
			<td><input type="button" value="Submit" ng-click="saveQSrtg()"/></td>
			<td></td>
		</tr>
	</table>
</div>


<div id="chlnCodeDB" ng-hide="chlnCodeFlag">
	<input type="text" name="filterChlnCode"
		ng-model="filterChlnCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>Challan Code</th>
			<th>Total Challan wt</th>
			<th>Charge wt</th>
			<th>Freight</th>
			<th>Total Package</th>
			<th>Time Allow</th>
			<th>Lorry No</th>
			<th>Challan Date</th>
		</tr>
		<tr ng-repeat="challan in chlnList | filter:filterChlnCode">
			<td><input type="radio" name="archlnCode" id="archlnCode"
				value="{{ challan.chlnCode }}" ng-model="archlnCode"
				ng-click="selectedChlnCode(challan)"></td>
			<td>{{challan.chlnCode}}</td>
			<td>{{challan.chlnTotalWt}}</td>
			<td>{{challan.chlnChgWt}}</td>
			<td>{{challan.chlnFreight}}</td>
			<td>{{challan.chlnNoOfPkg}}</td>
			<td>{{challan.chlnTimeAllow}}</td>
			<td>{{challan.chlnLryNo}}</td>
			<td>{{challan.chlnDt | date : format : timezone}}</td>
		</tr>
	</table>
</div>


<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">
	<input type="text" name="filterTextbox"
		ng-model="filterTextbox" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<!-- <th>Branch Code</th> -->

			<th>Branch Name</th>

			<th>Branch Pin</th>
			
			<th>Branch FACode</th>
		</tr>
		<tr ng-repeat="branch in branchList | filter:filterTextbox">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ branch.branchCode }}" ng-model="branchCode"
				ng-click="saveBranchCode(branch)"></td>
			<!-- <td>{{ branch.branchCode }}</td> -->
			<td>{{ branch.branchName }}</td>
			<td>{{ branch.branchPin }}</td>
			<td>{{ branch.branchFaCode }}</td>
		</tr>
	</table>
</div>


<div id="viewARDetailsDB" ng-hide="viewARDetailsFlag">
	<div class="row">
		<div class="col s12"
			style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<h4>
				Here's the detail of <span class="teal-text text-lighten-2">{{ar.arCode}}</span>:
			</h4>
			<table class="table-bordered table-hover table-condensed">
				<tr>
					<td>Arrival Report Type:</td>
					<td>{{ar.arRepType}}</td>
				</tr>
				<tr>
					<td>Branch Code:</td>
					<td>{{ar.branchCode}}</td>
				</tr>
				<tr>
					<td>Arrival Report Code:</td>
					<td>{{ar.arCode}}</td>
				</tr>

				<tr>
					<td>Arrival Report Date:</td>
					<td>{{ar.arDt}}</td>
				</tr>

				<tr>
					<td>Received Weight:</td>
					<td>{{ar.arRcvWt}}</td>
				</tr>

				<tr>
					<td>Package:</td>
					<td>{{ar.arPkg}}</td>
				</tr>

				<tr>
					<td>Unloading:</td>
					<td>{{ar.arUnloading}}</td>
				</tr>

				<tr>
					<td>Claim:</td>
					<td>{{ar.arClaim}}</td>
				</tr>

				<tr>
					<td>Detention:</td>
					<td>{{ar.arDetention}}</td>
				</tr>

				<tr>
					<td>Other:</td>
					<td>{{ar.arOther}}</td>
				</tr>

				<tr>
					<td>Late Delivery:</td>
					<td>{{ar.arLateDelivery}}</td>
				</tr>

				<tr>
					<td>Late Acknowledgement:</td>
					<td>{{ar.arLateAck}}</td>
				</tr>

				<tr>
					<td>Reporting Date :</td>
					<td>{{ar.arRepDt}}</td>
				</tr>

				<tr>
					<td>Reporting Time:</td>
					<td>{{ar.arRepTime}}</td>
				</tr>

				<tr>
					<td>Challan Code:</td>
					<td>{{ar.archlnCode}}</td>
				</tr>
				<tr>
					<td>Arrival Issue Date:</td>
					<td>{{ar.arIssueDt}}</td>
				</tr>
				
				<tr>
					<td>Arrival Shrtg/Damge:</td>
					<td>{{ar.arSrtgDmg}}</td>
				</tr>
				
			</table>

			<input type="button" value="Save" id="saveBtnId" ng-click="saveAR(ar)"> <input
				type="button" value="Cancel" ng-click="closeViewARDetailsDB()">

		</div>

	</div>

</div>


<div id="displayArCodeDB" ng-hide="displayArCodeFlag">
	<table>
		<tr>
			<td>Computerized Arrival Report code  : </td>
			<td>{{ compArCode }} </td>
		</tr>
		
		<tr>
			<td>
				<input type="button" value="OK" ng-click="closeCode()"/>
			</td>
		</tr>
	</table>
</div>


</div>

<!-- <form name="ArrivalReportForm" ng-submit="submitAR(ArrivalReportForm,ar)">
			<table>
				<tr>
					<td>Arrival Report Code: *</td>
					<td><input type ="number" name ="arCode" id="arCode" ng-model="ar.arCode" ng-required="true" ng-minlength="7" ng-maxlength="7"></td>
				</tr>
				
				<tr>
					<td>Arrival Report Date: *</td>
					<td><input type ="date" name ="arDt" ng-model="ar.arDt" required></td>
				</tr>
				
				<tr>
					<td>Received Weight: *</td>
					<td><input type ="number" name ="arRcvWt" id="arRcvWt" ng-model="ar.arRcvWt" ng-minlength="1" ng-maxlength="9" step="0.01" min="0.01" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Package: *</td>
					<td><input type ="number" name ="arPkg" id="arPkg" ng-model="ar.arPkg" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Unloading: *</td>
					<td>Rs.<input type ="number" name ="arUnloading" id="arUnloading" ng-model="ar.arUnloading" step="0.01" min="0.01" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Claim: *</td>
					<td>Rs.<input type ="number" name ="arClaim" id="arClaim" ng-model="ar.arClaim" step="0.01" min="0.01" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Detention: *</td>
					<td>Rs.<input type ="number" name ="arDetention" id="arDetention" ng-model="ar.arDetention" step="0.01" min="0.01" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Other: *</td>
					<td>Rs.<input type ="number" name ="arOther" id="arOther" ng-model="ar.arOther" step="0.01" min="0.01" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Late Delivery: *</td>
					<td>Rs.<input type ="number" name ="arLateDelivery" id="arLateDelivery" step="0.01" min="0.01" ng-model="ar.arLateDelivery" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Late Acknowledgement: *</td>
					<td>Rs.<input type ="number" name ="arLateAck"  id="arLateAck" step="0.01" min="0.01" ng-model="ar.arLateAck" ng-minlength="1" ng-maxlength="9" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Reporting Date: *</td>
					<td><input type ="date" name ="arRepDt" ng-model="ar.arRepDt" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Reporting Time: *</td>
					<td><input type ="time" name ="arRepTime" ng-model="ar.arRepTime" ng-required="true"></td>
				</tr>
				
				<tr>
				<td><input type="file" file-model="arImage"></td>
		    	<td><input class="btn waves-effect waves-light" type="button" ng-click="uploadArImage()" value="upload Image"></td>
				</tr>
							
				<tr>
					<td><input type="submit" value="Submit"></td>
				</tr>
			</table>
			
		</form> -->

		