package com.mylogistics.constants.webservice;

import com.mylogistics.model.webservice.AddressApp;

public class OwnBrkAppCNTS {
	
	public static final String OWN_BRK_ID = "ownBrkId";	
	public static final String BRAN_CHCODE = "branchCode";	
	public static final String USER_CODE = "userCode";	
	public static final String OWN_BRK_NAME = "ownBrkName" ;
	public static final String OWN_BRK_CODE = "ownBrkCode";
	public static final String OWN_BRK_UNION = "ownBrkUnion";	
	public static final String OWN_BRK_EMAIL_ID = "ownBrkEmailId";	
	public static final String OWN_BRK_PHONE_NO = "ownBrkPhoneNo";
	public static final String OWN_BRK_VEHICLE_TYPE = "ownBrkVehicleType";
	public static final String OWN_BRK_SER_TAX_NO = "ownBrkSerTaxNo";
	public static final String OWN_BRK_PP_NO = "ownBrkPPNo";
	public static final String OWN_BRK_VOTER_ID = "ownBrkVoterId";
	public static final String OWN_BRK_FIRM_REG_NO = "ownBrkFirmRegNo";
	public static final String OWN_BRK_REG_PLACE = "ownBrkRegPlace";
	public static final String OWN_BRK_COM_ID_NO = "ownBrkComIdNo";
	public static final String OWN_BRK_BSN_CARD = "ownBrkBsnCard";
	public static final String OWN_BRK_FIRM_TYPE = "ownBrkFirmType";
	public static final String OWN_BRK_COM_EST_DT = "ownBrkComEstDt";
	public static final String OWN_BRK_ACTIVE_DT = "ownBrkActiveDt";
	public static final String OWN_BRK_PAN_NAME = "ownBrkPanName";
	public static final String OWN_BRK_PAN_NO = "ownBrkPanNo";
	public static final String OWN_BRK_PAN_DT = "ownBrkPanDt";
	public static final String OWN_BRK_PAN_DOB = "ownBrkPanDOB";
	public static final String ADDRESS_LIST = "addressList";
	public static final String IS_PENDING = "isPending";
	public static final String IS_CANCEL = "isCancel";	
	public static final String TYPE = "type";
	public static final String OWN_BRK_IMG_ID = "ownBrkImgId";
	public static final String IS_PAN_IMG = "isPanImg";
	public static final String IS_DEC_IMG = "isDecImg";
	public static final String OWN_BRK_CREATION_TIME = "creationTS";
	
}