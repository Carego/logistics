'use strict';

var app = angular.module('application');

app.controller('DisplayChallanCntlr',['$scope','$location','$http','$window','$modal','$log','$filter',
                                      function($scope,$location,$http,$window,$modal,$log,$filter){

	$scope.isViewNo=false;
	$scope.selection=[];
	$scope.availableTags = [];
	$scope.showChallan = true;
	$scope.ChlnCodeFlag=true;
	$scope.chlnCodeByModal=false;
	$scope.chlnCodeByAuto=false;
	/*$scope.ChallanList = [];*/


	$scope.OpenChlnCodeDB = function(){
		$scope.ChlnCodeFlag=false;
		$('div#ChlnCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#ChlnCodeDB').dialog('open');
	}

	$scope.enableModalTextBox = function(){
		$('#chlnCodeByMId').removeAttr("disabled");
		$('#ok').attr("disabled","disabled");
		$('#chlnCodeByAId').attr("disabled","disabled");
		$scope.chlnCodeByA="";
	}

	$scope.enableAutoTextBox = function(){
		$('#chlnCodeByAId').removeAttr("disabled");
		$('#chlnCodeByMId').attr("disabled","disabled");
		$('#ok').removeAttr("disabled");
		$scope.chlnCodeByM="";
	}

	$scope.getChlnIsViewNo = function(){
		var response = $http.post($scope.projectName+'/getChlnIsViewNo');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.successToast(data.result);
				$scope.ChallanList = data.list;
				$scope.isViewNo=false;
				for(var i=0;i<$scope.ChallanList.length;i++){
					$scope.ChallanList[i].creationTS =  $filter('date')($scope.ChallanList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
			}else{
				$scope.isViewNo=true;
				$('#bkToList').attr("disabled","disabled");
				$scope.alertToast(data.result);
			}

			$scope.getAllChallanCode();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getChlnCodeList=function(){
		$( "#chlnCodeByAId" ).autocomplete({
			source: $scope.availableTags
		});
	}

	$scope.getAllChallanCode = function(){
		console.log("entered into getAllChallanCode function------>");
		var response = $http.post($scope.projectName+'/getAllChallanCode');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.challanCodeList = data.challanCodeList;
				$scope.availableTags = data.challanCodeList;
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.verifyChallan = function(){
		if(!$scope.selection.length){
			$scope.alertToast("You have not selected anything");
		}else{
			var response = $http.post($scope.projectName+'/updateIsViewChallan',$scope.selection.toString());
			response.success(function(data, status, headers, config){
				$scope.getChlnIsViewNo();
				console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
			});	 
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	$scope.EditChallanSubmit = function(challan){
		console.log("enter into EditChallanSubmit( function--->"+challan.branchCode);
		var response = $http.post($scope.projectName+'/EditChallanSubmit', challan);
		response.success(function(data, status, headers, config) {
			console.log(data.result);
			$scope.chln="";
			$scope.showChallan = true;
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.backToList = function(){
		// $scope.divForModalCode = true;
		$scope.saveChlnCode = true;
		$scope.isViewNo=false;
		$scope.chlnCodeByM="";
		$scope.chlnCodeByA="";
		$('#chlnCodeByAId').attr("disabled","disabled");
		$('#chlnCodeByMId').attr("disabled","disabled");
		$scope.showChallan=true;
	}

	$scope.toggleSelection = function toggleSelection(chlnCode) {
		var idx = $scope.selection.indexOf(chlnCode);

		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		// is newly selected
		else {
			$scope.selection.push(chlnCode);
		}
	}	 

	$scope.saveChlnCode = function(challanCodes){
		$scope.chlnCodeByM=challanCodes;
		$('div#ChlnCodeDB').dialog('close');
		$scope.showChallan = false;
		//$scope.divForAutoCode = true;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/challanDetails',$scope.chlnCodeByM);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.chln = data.challan;	
				$scope.alertToast(data.result);
				if(data.image === "yes"){
					$('#submitImage').removeAttr("disabled");
				}else{
					$('#submitImage').attr("disabled","disabled");
				}
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getChlnList = function(){

		$scope.code = $( "#chlnCodeByAId" ).val();
		if($scope.code===""){
			$scope.alertToast("Please enter code");	
		}else{
			$scope.saveChlnCode = false;
			$scope.isViewNo=true;
			var response = $http.post($scope.projectName+'/challanDetails',$scope.code);
			response.success(function(data, status, headers, config) {
				$scope.chln = data.challan;	
				$scope.showChallan = false;

			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	$scope.items = ['item1', 'item2', 'item3'];

	$scope.open = function (size) {
		console.log("enter into open fucntion");
		var modalInstance = $modal.open({
			templateUrl: 'myModalContent.html',
			controller: 'ModalInstanceCtrl',
			size: size,
			/*resolve: {
		        items: function () {
		          return $scope.items;
		        }
		      }*/
		});

		/*modalInstance.result.then(function (selectedItem) {
		      $scope.selected = selectedItem;
		    }, function () {
		      $log.info('Modal dismissed at: ' + new Date());
		    });*/
	};

	$scope.ChallanList1 = ['rohit','kaushik'];

	$scope.open = function (size) {
		console.log("enter into open fucntion");
		var modalInstance = $modal.open({
			templateUrl: 'ChallanModal.html',
			controller: 'ModalInstanceCtrl',
			size: size,
			/*resolve: {
		    	  ChallanList1 : function () {
		    		  console.log("enter into resolve = "+$scope.ChallanList1);
		          return $scope.ChallanList1;
		        }
		      }*/
		});

		modalInstance.result.then(function (selectedItem) {
			$scope.selected = selectedItem;
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.saveChallanCode = function(challanCodes){
		console.log("enter into saveChallanCode function");
		$scope.test = challanCodes.chlnCode; 
	}	

	$scope.getImage = function(){
		console.log("enter into getImage function");
		var response = $http.post($scope.projectName+'/getImage');
		response.success(function(data, status, headers, config) {
			$scope.image = data.chImage
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getChlnIsViewNo();
	}else if($scope.logoutStatus === true){
			 $location.path("/");
	}else{
			 console.log("****************");
	}
		
}]);
