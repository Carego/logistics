package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BrhWiseAssgnCntlr {
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@RequestMapping(value = "/getBrFrBWAssgn", method = RequestMethod.POST)
	public @ResponseBody Object getBrFrBWAssgn() {
		System.out.println("enter into getBrFrBWAssgn function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Branch> brList = branchDAO.getAllActiveBranches();
		if(!brList.isEmpty()){
			map.put("list",brList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/submitBrWise", method = RequestMethod.POST)
	public @ResponseBody Object submitBrWise(@RequestBody Branch branch) {
		System.out.println("enter into submitBrWise function");
		Map<String, Object> map = new HashMap<String, Object>();
		int temp = branchDAO.updateBrWise(branch);
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
}
