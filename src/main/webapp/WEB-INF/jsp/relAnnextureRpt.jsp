<style type="text/css">
.printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		}
                		
                		body {
						    width: 10in;
						    height: 210in;
						     
						  }
            }
            @PAGE {
				  size:A4 ;
				  margin:0.5cm 0.5cm 0.5cm 0.5cm;
			      }  
			      
			     			      
}

  .latterh{font-weight:bold;} 
</style>
<div ng-show="operatorLogin || superAdminLogin">

<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>

	<div class="noprint">
		<form name="relBlPrintForm" ng-submit=relBlPrintSubmit(relBlPrintForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="fromDateId" name ="fromDateName" ng-model="fromDate" ng-required="true" ng-blur="fromValidDate(fromDate)">
		       			<label for="code">from Date</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="toDateId" name ="toDateName" ng-model="toDate" ng-required="true" ng-blur="toValidDate(toDate)" >
		       			<label for="code">to Date</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input type ="button" id="relCustNameId" value="CustomerGroup" ng-required="true" ng-click="relCustDB()" >
		       		</div>
		       		
		       </div>
		       
		       <div id="custDB" ng-hide="relCustDBFlag">
		       <input type="text" name="filterTextName" ng-model="filterTextbox" placeholder="search">
		       <table>
		       <tr>
		       <th></th>
		       <th>Group Name</th>
		       <th>GroupId</th>
		       </tr>
		       <tr ng-repeat="custGroup in custGroupList | filter:filterTextbox" >
		        <td> <input type="radio" name="gruoupName"  value="{{custGroup}}"   ng-click="saveCustGroup(custGroup)"></td>
		        <td>{{custGroup.groupName}}</td>
             	 <td>{{custGroup.groupId}}</td>
		       </tr>
		       </table>
		       </div>
		    
		     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	         </div>
	      
		      <div class="row">
		     		<div class="input-field col s12 center">
		       				<input class="validate" type ="button" value="Download Annexture" ng-show="relBlShow" ng-click="downloadAnx()">
		       		</div>
		      </div>
		      	 
		</form>    
	</div>
	
		
	<div id="exportable"  class="noprint">
	<h6 align="center"  ng-if="blList.length>0"><b>Care Go Logistics PVT Ltd</b><br>
		  <b style="border:1px solid black;">RELIANCE CORPORATE PARK BUILDING NO.5B IST FLOOR 5TTC INDUSTRAIL AREA GHANSOLI</b>
		  </h6>
	<table  ng-repeat="bill in blList">

	<tr ng-if="$index==0" style="text-align:center;"> 
			<th style="border:1px solid #000000;">FILE NO</th>
		    <th style="border:1px solid #000000;">FILE DATE</th>
		    <th style="border:1px solid #000000;">BILL NO</th>
		    <th style="border:1px solid #000000;">BILL DATE</th>
		    <th style="border:1px solid #000000;">BILL AMT</th>
		    <th style="border:1px solid #000000;">DCPI NO</th>
		    <th style="border:1px solid #000000;">DCPI DATE</th>
		    <th style="border:1px solid #000000;">TRUCK NO</th>
		    <th style="border:1px solid #000000;">LR NO</th>
		    <th style="border:1px solid #000000;">LR DATE</th>  
		    <th style="border:1px solid #000000;">DELIVERED</th>
		    <th style="border:1px solid #000000;">DELIVERY</th>
		    <th style="border:1px solid #000000;">RATE</th>
		    <th style="border:1px solid #000000;">AMOUNT</th>
		    <th style="border:1px solid #000000;">VENDOR</th>
		    <th style="border:1px solid #000000;">COMPANY</th>
		    <th style="border:1px solid #000000;">OVER HEIGHT</th>
		    <th style="border:1px solid #000000;">TOTAL AMOUNT</th>
		    <th style="border:1px solid #000000;">FROM</th>
		    <th style="border:1px solid #000000;">TO</th>
		    <th style="border:1px solid #000000;">WEIGHT</th>
		    <th style="border:1px solid #000000;">MATERIAL</th>
		    <th style="border:1px solid #000000;">TRUCK</th>
		    <th style="border:1px solid #000000;">REMARKS</th>
		</tr></caption>
		
		<tr ng-repeat="bilDet in bill.blDetList" style="text-align:center;">
			<!--<td style="border:1px solid #000000;">{{ bill.bfNo }}</td>-->
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{bill.bill.blSubTot}}</td> <!--  bilDet.billDet.bdTotAmt | number : 2 -->
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtInvoiceNo[0].date | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bilDet.truckNo }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtCode }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delQty }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> <!--{{bilDet.billDet.bdRate * 1000 | number : 2}}  -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> 
			<td style="border:1px solid #000000;">3234040</td>
			<td style="border:1px solid #000000;">5075</td>
			<td style="border:1px solid #000000;">{{ changeCommaNumber(((bilDet.billDet.bdTotAmt-bilDet.billDet.bdFreight)| number : 0)) }}</td> <!-- bilDet.ovrHgt | number : 2 --> <!-- bilDet.ovrHgt | number : 0 -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdTotAmt | number : 0))}}</td> <!--{{ bilDet.billDet.bdTotAmt | number : 2 }}  -->
			<td style="border:1px solid #000000;">{{ bilDet.frmStn }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.toStn }}</td>
			<td style="border:1px solid #000000;">
				<div ng-if="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
			</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtProductType }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.vehType }}</td>
			<td style="border:1px solid #000000;">{{ bill.bill.blDesc }}</td>
		</tr>
	</table>
	</div>
	
</div>	
       
           