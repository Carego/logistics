package com.mylogistics.constants;

public class TermNConditionCNTS {

	public static final String TNC_ID = "tncId";
	
	public static final String TNC_CONT_CODE = "tncContCode";
	
	public static final String TNC_PENALTY = "tncPenalty";
	
	public static final String TNC_LOADING = "tncLoading";
	
	public static final String TNC_UNLOADING = "tncUnLoading";
	
	public static final String TNC_DETENTION = "tncDetention";
	
	public static final String TNC_BONUS = "tncBonus";
	
	public static final String TNC_TRANSIT_DAY = "tncTransitDay";
	
	public static final String TNC_STATISTICAL_CHANGE = "tncStatisticalChange";
}
