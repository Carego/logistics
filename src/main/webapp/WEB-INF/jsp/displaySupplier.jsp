<div ng-show="adminLogin || superAdminLogin">

<title>Display Supplier</title>
		<div class="container">
			<div class="row">
				<div class="col s2"><input type="radio" ng-model="supplierModalRadio" name="supCode" ng-click="enableSupCodeModal()"></div>
				<div class="col s10"><input type = "text" id="supplierCodeId" ng-model="supplierCode" disabled="disabled" ng-click="OpenSupCodeDB()" readonly></div>
			</div>
			<div class="row">
				<div class="col s2"><input type="radio" ng-model="supplierAutoRadio"  name="supCode" ng-click="enableAutoSupCodeModal()"></div>
				<div class="col s8"><input type = "text" id="supCodeId" ng-model="supCode" disabled="disabled" ng-keyup="getSupCodeList()" ></div>
				<div class="col s2"><input class="col s12" type ="button" id="ok" value = "OK" ng-click="getSupCodeDetails(supCode)" disabled="disabled"></div>
			</div>
			<div class="row">
				<div class="col s12"><input type ="button" value = "Back To list" ng-click="backToList()"></div>
			</div>
		</div>

<div id="supplierListDB" ng-hide="supplierListFlag">
         
   <input type="text" name="filterSupCode" ng-model="filterSupCode.stSupCode" placeholder="Search by Supplier Code">

		<table>
 	 		<tr>
 	 			<th></th>
 	 		 	 		    	 		    		
				<th>Supplier Code</th>
				
				<th>Supplier Name</th>
			
			    <th>Supplier Group</th>
			    
			    <th>Supplier Sub-Group</th>
			    
			    <th>Operator Id</th>
				
				<th>Creation TS</th>	
							
			</tr>
			
			 <tr ng-repeat="sup in supList | filter:filterSupCode ">
               	<td><input type="checkbox" value="{{  sup.stSupCode }}" ng-checked="selection.indexOf(sup.stSupCode) > -1" ng-click="toggleSelection( sup.stSupCode)" /></td>
               	<td>{{ sup.stSupCode }}</td>
                <td>{{ sup.stSupName }}</td>
                <td>{{ sup.stSupGroup }}</td>
                <td>{{ sup.stSupSubGroup }}</td>
                <td>{{ sup.userCode }}</td>
              	<td>{{ sup.creationTS }}</td>
            </tr>
            
            <tr>
              <td><input type="button" value="Submit" ng-click="verifyStSupplier()"></td>
            </tr> 
		</table>
</div>



<div id="supplierCodeDB" ng-hide="supplierCodeFlag"> 
	<input type="text" name="filterSupCode" ng-model="filterSupCode" placeholder="Search">
 	   <table>
 	   		<tr ng-repeat="sup in supplierCodeList | filter:filterSupCode">
		 	  	<td><input type="radio"  name="supName"  class="supCls"  value="{{ sup }}" ng-model="supCode" ng-click="saveSupCode(sup)"></td>
              	<td>{{ sup.stSupCode }}</td>
              	<td>{{ sup.stSupName }}</td>
          	</tr>
       </table>

</div>	

<div ng-hide = "showSupplierDetails" >		
	<div class="container">
	<div class="row">
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupAddName" ng-model="currentSup.stSupAdd" value="{{ currentSup.stSupAdd }}">
			    <label>Supplier Address</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupCity" ng-model="currentSup.stSupCity" value="{{ urrentSup.stSupCity }}">
				<label>Supplier City</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupCode" ng-model="currentSup.stSupCode" value="{{ currentSup.stSupCode }}" readonly>
				<label>Supplier Code</label> 
		</div>
				
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupGroup" ng-model="currentSup.stSupGroup" value="{{ currentSup.stSupGroup }}">
				<label>Supplier Group</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupName" ng-model="currentSup.stSupName" value="{{ currentSup.stSupName }}">
				<label>Supplier Name</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupPanNo" ng-model="currentSup.stSupPanNo" value="{{ currentSup.stSupPanNo }}">
				<label>Supplier Pan No</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupPin" ng-model="currentSup.stSupPin" value="{{ currentSup.stSupPin }}">
				<label>Supplier Pin No</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupState" ng-model="currentSup.stSupState" value="{{ currentSup.stSupState }}">
				<label>Supplier State</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupSubGroup" ng-model="currentSup.stSupSubGroup" value="{{ currentSup.stSupSubGroup }}">
				<label>Supplier Sub Group</label> 
		</div>
		
		<div class="input-field col s12 m4 l3">
				<input type ="text" name ="stSupTdsCode" ng-model="currentSup.stSupTdsCode" value="{{ currentSup.stSupTdsCode }}">
				<label>Supplier TDS Code</label> 
		</div>
		
		</div>
		<div class="row">
		<div class="input-field col s12 center">
				<input type="button" value="Submit" ng-click="editSupplier(currentSup)">
		</div>
	       </div>
	</div>
	         
	</div>
	</div>