<div ng-if="operatorLogin || superAdminLogin">
<title>Daily Contract Details</title>
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="viewContract(ContCode)">
	
			<div class="input-field col s6">
   				<input type="text" id="displayDetails" name="ContCode" ng-model="ContFaCode" ng-click="openCodesDB()" readonly/>
   				<label>Enter Contract Code </label>
 			</div>
			
		 	<div class="input-field col s6 center">
		 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
		 	</div>
			
		</form>
	<div class="col s3"> &nbsp; </div>
</div>
<!-- 	<form ng-submit="viewContract(ContCode)">
				<table>
					<tr>
						<td>Enter Contract Code </td>
						<td><input type="text" name="ContCode" ng-model="ContCode"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
			</form> -->
			
			<div class="row" ng-hide = "dlyCont">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of Daily Contract <span class="teal-text text-lighten-2">{{dailyContract.dlyContFaCode}}</span>:</h4>
		<table class="table-hover table-bordered table-bordered">
				<tr>
	                <td>Contract Code:</td>
	                <td>{{dailyContract.dlyContCode}}</td>
	            </tr>
	            
	             <tr>
	                <td>Branch Code:</td>
	                <td>{{dailyContract.branchCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract BLPM Code:</td>
	                <td>{{dailyContract.dlyContBLPMCode}}</td>
	            </tr>
	           
	            <tr>
	                <td>Consignor Code:</td>
	                <td>{{dailyContract.dlyContCngrCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>CR Name:</td>
	                <td>{{dailyContract.dlyContCrName}}</td>
	            </tr>
	            
	             <tr>
	                <td>Start Date:</td>
	                <td>{{dailyContract.dlyContStartDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>End Date:</td>
	                <td>{{dailyContract.dlyContEndDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>From Station:</td>
	                <td>{{dailyContract.dlyContFromStation}}</td>
	            </tr>
	            
	            <tr ng-show="dailyContract.dlyContToStation">
	                <td>To Station:</td>
	                <td>{{dailyContract.dlyContToStation}}</td>
	            </tr>
	           
	            <tr>
	                <td>Contract Type:</td>
	                <td>{{dailyContract.dlyContType}}</td>
	            </tr>
	            
	             <tr>
	                <td>Contract DC:</td>
	                <td>{{dailyContract.dlyContDc}}</td>
	            </tr>
	            
	             <tr>
	                <td>Cost Grade:</td>
	                <td>{{dailyContract.dlyContCostGrade}}</td>
	            </tr>
	            
	            <tr>
	                <td>Transit Day:</td>
	                <td>{{dailyContract.dlyContTransitDay}}</td>
	            </tr>
	            
	            <tr>
	                <td>Statistical Charge:</td>
	                <td>{{dailyContract.dlyContStatisticalCharge}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Load:</td>
	                <td>{{dailyContract.dlyContLoad}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract UnLoad:</td>
	                <td>{{dailyContract.dlyContUnLoad}}</td>
	            </tr>
	            
	            <tr>
	                <td>Hour Or Day:</td>
	                <td>{{dailyContract.dlyContHourOrDay}}</td>
	            </tr>
	           
	            <tr>
	                <td> Proportionate:</td>
	                <td>{{dailyContract.dlyContProportionate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Additional Rate:</td>
	                <td>{{dailyContract.dlyContAdditionalRate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DDL:</td>
	                <td>{{dailyContract.dlyContDdl}}</td>
	            </tr>
	         
	            <tr>
	                <td>Remarks:</td>
	                <td>{{dailyContract.dlyContRemark}}</td>
	            </tr>
	            
	            <tr>
	                <td>Creation Time</td>
	                <td>{{dailyContract.creationTS}}</td>
	            </tr>
	          </table>
		</div>
	</div>
	

	
		<div class="row" ng-hide = "regCont" >
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >			
		<h4>Here's the details of Regular Contract <span class="teal-text text-lighten-2">{{regularContract.regContFaCode}}</span>:</h4>
		<table class="table-hover table-bordered table-condensed">
				<tr>
	                <td>Contract Code:</td>
	                <td>{{regularContract.regContCode}}</td>
	            </tr>
	            <tr>
	                <td>Contract BLPM Code:</td>
	                <td>{{regularContract.regContBLPMCode}}</td>
	            </tr>
	            <tr>
	                <td>Branch Code:</td>
	                <td>{{regularContract.branchCode}}</td>
	            </tr>
	            <tr>
	                <td>Consignor Code:</td>
	                <td>{{regularContract.regContCngrCode}}</td>
	            </tr>
	            <tr >
	                <td>CR Name:</td>
	                <td>{{regularContract.regContCrName}}</td>
	            </tr>
	            <tr>
	                <td>From Station:</td>
	                <td>{{regularContract.regContFromStation}}</td>
	            </tr>
	            <tr ng-show="regularContract.regContToStation">
	                <td>To Station:</td>
	                <td>{{regularContract.regContToStation}}</td>
	            </tr>
	            
	            <tr>
	                <td>From Date:</td>
	                <td>{{regularContract.regContFromDt}}</td>
	            </tr>
	            <tr>
	                <td>To Date:</td>
	                <td>{{regularContract.regContToDt}}</td>
	            </tr>
	            <tr>
	                <td>Contract Type:</td>
	                <td>{{regularContract.regContType}}</td>
	            </tr>
	            
	            <tr>
	                <td>Transit Day:</td>
	                <td>{{regularContract.regContTransitDay}}</td>
	            </tr>
	            <tr>
	                <td>Statistical Charge:</td>
	                <td>{{regularContract.regContStatisticalCharge}}</td>
	            </tr>
	            <tr>
	                <td>Contract Load:</td>
	                <td>{{regularContract.regContLoad}}</td>
	            </tr>
	            <tr>
	                <td>Contract UnLoad:</td>
	                <td>{{regularContract.regContUnLoad}}</td>
	            </tr>
	           
	            <tr>
	                <td> Proportionate:</td>
	                <td>{{regularContract.regContProportionate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Additional Rate:</td>
	                <td>{{regularContract.regContAdditionalRate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Value:</td>
	                <td>{{regularContract.regContValue}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DC:</td>
	                <td>{{regularContract.regContDc}}</td>
	            </tr>
	            
	            <tr>
	                <td>Cost Grade:</td>
	                <td>{{regularContract.regContCostGrade}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Weight:</td>
	                <td>{{regularContract.regContWt}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DDL:</td>
	                <td>{{regularContract.regContDdl}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Renew:</td>
	                <td>{{regularContract.regContRenew}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Renew Date:</td>
	                <td>{{regularContract.regContRenewDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insured By:</td>
	                <td>{{regularContract.regContInsuredBy}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insure Company:</td>
	                <td>{{regularContract.regContInsureComp}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insure Unit Number:</td>
	                <td>{{regularContract.regContInsureUnitNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insure Policy Number:</td>
	                <td>{{regularContract.regContInsurePolicyNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>Creation Time</td>
	                <td>{{regularContract.creationTS}}</td>
	            </tr>
	            
	            <tr>
	                <td>Remarks:</td>
	                <td>{{regularContract.regContRemark}}</td>
	            </tr>
	           </table>
		</div>
	</div>	
	
		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>CTS</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{fromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		    <tr><td><h5>CTS</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{fromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
			<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	<tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
			<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
		
	<div id ="CodesDB" ng-hide = "CodesDBFlag">
	
	<input type="text" name="filterContractCode" ng-model="filterContractCode" placeholder="Search by Contract Code">
 	   	  <table>
	 	   	  <tr>
	 	   	  	<th></th>
	 	   	  	<th>Contract Code</th>
	 	   	    <th>Customer Code</th>
	 	   	    <th>Branch Code</th>
	 	   	    <th>From Station</th>
	 	   	  </tr>
			 <tr ng-repeat="codes in codeList | filter:filterContractCode">
			 	  <td><input type="radio" name="contCode"  value="{{ codes }}" ng-model="Code" ng-click="saveCode(codes)"></td>
	              <td>{{ codes.contFaCode }}</td>
	              <td>{{ codes.custName }}</td>
	              <td>{{ codes.brhName }}</td>
	              <td>{{ codes.frmStnName }}</td>
	          </tr>
         </table>       
	</div>
	</div>