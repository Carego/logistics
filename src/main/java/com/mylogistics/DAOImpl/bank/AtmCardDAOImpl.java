package com.mylogistics.DAOImpl.bank;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.bank.AtmCardDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.bank.AtmCardMstrCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.Branch_Atm;
import com.mylogistics.services.bank.AtmCardService;

public class AtmCardDAOImpl implements AtmCardDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public AtmCardDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public int saveAtmCard(AtmCardService atmCardService, User currentUser) {
		
		int temp = 0;
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			//check if atmCardNo exist 
			Criteria cr = session.createCriteria(AtmCardMstr.class);
			cr.add(Restrictions.eq(AtmCardMstrCNTS.ATM_CARD_NO, atmCardService.getAtmCardMstr().getAtmCardNo()));
			if (!cr.list().isEmpty()) {
				session.flush();
				session.clear();
				session.close();
				return -2;
			}
			
			Criteria criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, atmCardService.getBranchId()));
			Branch branch  = (Branch) criteria.list().get(0);
			
			criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, atmCardService.getBankId()));
			BankMstr bankMstr = (BankMstr) criteria.list().get(0);
			
			criteria=session.createCriteria(Employee.class);
			criteria.add(Restrictions.eq(EmployeeCNTS.EMP_ID, atmCardService.getEmpId()));
			Employee employee = (Employee) criteria.list().get(0);
			
			AtmCardMstr atmCardMstr = atmCardService.getAtmCardMstr();
			atmCardMstr.setbCode(currentUser.getUserBranchCode());
			atmCardMstr.setUserCode(currentUser.getUserCode());
			atmCardMstr.setBankMstr(bankMstr);
			
			atmCardMstr.setBranchId(branch.getBranchId());
			atmCardMstr.setEmpId(employee.getEmpId());
			
			bankMstr.getAtmCardMstrList().add(atmCardMstr);
			
			/*branch.getAtmCardMstrList().add(atmCardMstr);*/
			
			session.update(bankMstr);
			
			//insert data in 3rd table
			Branch_Atm branch_atm = new Branch_Atm();
			branch_atm.setAtmCardId(atmCardMstr.getAtmCardId());
			branch_atm.setBranchId(branch.getBranchId());
			session.save(branch_atm);
			
			transaction.commit();
			session.flush();

			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;

		}
		session.clear();
		session.close();
		
		return temp;
		
	}

	@Override
	@Transactional
	public AtmCardMstr getAtmCardByNo(String atmCardNo) {
		System.out.println("AtmCardDAOImpl.getAtmCardByNo()");
		AtmCardMstr atmCardMstr = null;
		
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(AtmCardMstr.class);
			cr.add(Restrictions.eq(AtmCardMstrCNTS.ATM_CARD_NO, atmCardNo));
			
			if (!cr.list().isEmpty()) {
				atmCardMstr = (AtmCardMstr) cr.list().get(0);
				Branch branch = (Branch) session.get(Branch.class, atmCardMstr.getBranchId());
				Employee employee = (Employee) session.get(Employee.class, atmCardMstr.getEmpId());
				atmCardMstr.setBranchName(branch.getBranchName());
				atmCardMstr.setEmpName(employee.getEmpName());
			}
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return atmCardMstr;
	}

	@Transactional
	@Override
	public int transAtmToBr(Map<String, Object> transAtmToBrService) {
		System.out.println("AtmCardDAOImpl.transAtmToBr()");
		int temp = 0;
		
		int atmCardId = (int) transAtmToBrService.get("atmCardId");
		int branchId = (int) transAtmToBrService.get("branchId");
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			AtmCardMstr atmCardMstr = (AtmCardMstr) session.get(AtmCardMstr.class, atmCardId);
			
			// update 3rd table
			Criteria cr = session.createCriteria(Branch_Atm.class);
			cr.add(Restrictions.eq("atmCardId", atmCardMstr.getAtmCardId()));
			if (!cr.list().isEmpty()) {
				Branch_Atm branch_atm = (Branch_Atm) cr.list().get(0);
				branch_atm.setBranchId(branchId);
				session.merge(branch_atm);
			}
			
			atmCardMstr.setBranchId(branchId);
			
			session.merge(atmCardMstr);
			transaction.commit();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
	}
	
	
	@Transactional
	@Override
	public List<AtmCardMstr>  getAtmByBrh(int brhId){
		System.out.println("AtmCardDAOImpl.getAtmByBrh()");
		List<AtmCardMstr> atmList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(AtmCardMstr.class);
			cr.add(Restrictions.eq(AtmCardMstrCNTS.ATM_CARD_BRH_ID,brhId));
			atmList = cr.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return atmList;
	}

	@Transactional
	@Override
	public int deactiveAtmCard(int atmCardId) {
		System.out.println("AtmCardDAOImpl.deactiveAtmCard()");
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			AtmCardMstr atmCardMstr = (AtmCardMstr) session.get(AtmCardMstr.class, atmCardId);
			atmCardMstr.setAtmActive(false);
			session.update(atmCardMstr);
			transaction.commit();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	@Override
	public int activeAtmCard(int atmCardId) {
		System.out.println("AtmCardDAOImpl.activeAtmCard()");
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			AtmCardMstr atmCardMstr = (AtmCardMstr) session.get(AtmCardMstr.class, atmCardId);
			atmCardMstr.setAtmActive(true);
			session.update(atmCardMstr);
			transaction.commit();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
	}

	
}
