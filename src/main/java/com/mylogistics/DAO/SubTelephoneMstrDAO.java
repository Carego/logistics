package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmt;
import com.mylogistics.model.SubTelephoneMstr;

@Repository
public interface SubTelephoneMstrDAO {

	public List<SubTelephoneMstr> getSubFileDetails(List<CashStmt> reqCsList);
}
