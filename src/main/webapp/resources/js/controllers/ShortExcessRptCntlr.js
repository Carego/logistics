'use strict';

var app = angular.module('application');

app.controller('ShortExcessRptCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("shortExcessRptCntlr Started");
	
	$scope.isConsolidate=false;
	$scope.isBranch=false;
	$scope.branchDBFlag=true;
	$scope.custDBFlag=true;
	$scope.branch={};
	$scope.branchList=[];
	$scope.showPrintFlag=false;
	$scope.cust = {};
	$scope.custList=[];
	$scope.isCustStar=false;
	$scope.lodingFlag = false;
	
	/************************* start call getShortExcessRpt function**************/
      $scope.getShortExcessRpt=function(){
    	console.log("start shortExcess rpt..");  
    	
    	var shortExcessRpt={
    			"fromDt"		: $scope.fromDt,
    			"toDt"  		: $scope.toDt,
    			"isBranch"  	: $scope.isBranch,
    			"isConsolidate" : $scope.isConsolidate,
    			"branchName"	: $scope.branch.branchName,
    			"branchId"		: $scope.branch.branchId,
    			"custName"		: $scope.cust.custName,
    			"custNameId"	: $scope.cust.custId,
    			"custList"		: $scope.custList
    	};
    			console.log("define..");
    			console.info(shortExcessRpt);
    			$scope.showPrintFlag=false;
    			$scope.lodingFlag = true;
    			$('#submitId').attr("disabled", "disabled");
    	var response=$http.post($scope.projectName+'/getShortExcessReportHtml',shortExcessRpt);		
  
    			response.success(function(data, status, headers, config){
    				if(data.result==="success"){
    					$scope.showPrintFlag=true;
    					$('#printShortExcesId').removeAttr("disabled");
    					$('#printXlsId').removeAttr("disabled");
    					$scope.lodingFlag =false;
    					$scope.alertToast(data.result);
    				}else{
    					$('#printShortExcesId').attr("disabled","disabled");
    					$('#printXlsId').attr("disabled","disabled");
    					$scope.showPrintFlag=false;
    					$scope.lodingFlag =false;
    					$scope.alertToast(data.result);
    				}
    			});
    			
    			response.error(function(data, status, headers, config){
    				console.log("getShortExcessReport Error: "+data);
    				$('#shortExcessSubmmitId').removeAttr("disabled");
    				$scope.showPrintFlag=false;
					$scope.lodingFlag =false;
    				$scope.alertToast("database exception");
    			});
    	
      }
	
	
	/*************************  end call getShortExcessRpt function**************/
	
	/********************* call submit function*******************************/
	$scope.shortExcessSubmit=function(shortExcessForm){
		console.log("enter shortExcessSubmitfunction..");
		
	/*	if(shortExcessForm.$invalid){
			$scope.alertToast("your form is invalid..");
		}else{*/
			if($scope.isConsolidate||$scope.isBranch){
					if($scope.isConsolidate){
						$scope.getShortExcessRpt();
								}
			else if(angular.isUndefined($scope.branch.branchName)||$scope.branch.branchName==null||$scope.branch.branchName==""){
				$scope.alertToast("your branch is invalid..");
			}else if($scope.isBranch){
				$scope.getShortExcessRpt();
			}
		}
			else{
				$scope.alertToast("Please select either Branch or Consolidate");
				}
	//		}
	}
	
  /*******************************end submit function***********************/	
	
	
	$scope.consolidateRB=function(){
		console.log("click consolidaate...");
		$scope.isConsolidate = true;
		$scope.isBranch=false;
		$scope.isCustStar=false;
		$scope.cust={};
		$scope.branch={};
		$('#branchNameId').attr("disabled","disabled");
		$('#custNameId').removeAttr("disabled");
	}
	
	$scope.branchRB=function(){
        console.log("click Branch...");
        $scope.isConsolidate =false;
		$scope.isBranch=true;
		$scope.isCustStar=false;
		$scope.cust={};
		$('#custNameId').removeAttr("disabled");
		$('#branchNameId').removeAttr("disabled");
	//	$('#custNameId').attr("disabled","disabled");
	}
	
	
	$scope.openCustDB=function(){
		console.log("customer name..");
		$scope.custDBFlag=false;
		$('div#custDB').dialog({
			autoOpen:false,
			modal:true,
			resizable:false,
			position:UDPos,
			show:UDShow,
			hide:UDHide,
			title:"Customer",
			draggable:true,
			close:function(event,ui){
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custDBFlag=true;
			}
		}); 
		$('div#custDB').dialog('open');
	}
	
	
	$scope.openBranchDB=function(){
		
		console.log("open Branch Database...");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		
		$('div#branchDB').dialog('open');
	
	}
		
	
	$scope.saveCust=function(cust){
		console.log("save customer");
		$scope.cust=cust;
	//	$scope.branch = {};
		$('div#custDB').dialog('close');
	}
		
		
	$scope.saveBranch=function(branch){
	 console.log("save branch..");
		$scope.branch=branch;
	//	$scope.cust={};
		$('div#branchDB').dialog('close');
	}
	
	
	
	$scope.getActiveBrList=function(){
		console.log("getActiveBrList Entered..");
	var response=$http.post($scope.projectName+'/getUserBrCustNBrList');
		response.success(function(data,status,headers,config) {
			console.log(data.result);
			if(data.result==="success"){
				$scope.branchList=data.branchList;
				$scope.custList=data.custList;
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrList result Error");
			}
		});
		
		response.error(function(data,status,headers,config) {
			console.log("Error in response brList:"+data);			
		});
	}
	

	console.log("call Active branch functon");
 if($scope.operatorLogin===true||$scope.superAdminLogin===true){
	 $scope.getActiveBrList();
 }else if($scope.logoutStatus === true){
	 $location.path("/");
 }else{
	 console.log("not user........");
 }
  
 // remove from shortExcss
 
 $scope.saveCustForEdit=function(cust){
		console.log("save customer");
		$scope.customer=cust;
		$('div#custDB').dialog('close');
	}
 
 
 $scope.removeBillFromShrtExs=function(formName){
	 console.log("rbfse()"+$scope.customer.custId);
	 
	 var map={
			 "custCode":$scope.customer.custId,
			 "billNo":$scope.billNo
	 }
	 var response=$http.post($scope.projectName+'/removeBillFromSrtExs',map);		
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.alertToast("Removed succesfully");
			}else{
				$scope.alertToast(data.msg);
			}
		});
		
		response.error(function(data, status, headers, config){
			$scope.alertToast("fail to connect server");
		});
		

	 
 }
 
 
	console.log("ShortedExcess report Ended");
}]);