'use strict';

var app = angular.module('application');

app.controller('CancelCNMTCntlr',['$scope','$location','$http',
                               function($scope,$location,$http){
	
	$scope.cnmtCode="";
	$scope.cca="";
	$scope.chlnDt="";
	$scope.cancelCNMT=function(){
		console.log("cancelCNMT()");
		
		var cnmtDtl={
				"cnmtCode" : $scope.cnmtCode,
				"cca" : $scope.cca,
				"date" : $scope.chlnDt
		}
		
		  var response = $http.post($scope.projectName+'/cnmtDtlFrCncl',cnmtDtl);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.alertToast("CNMT successfully canceled");
			  }else{
				  $scope.alertToast("Detail not Match");
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	}
		
		
	
	
}]);