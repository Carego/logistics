'use strict';

var app = angular.module('application');

app.controller('TravVoucherCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	
	$scope.vs = {};
	$scope.tdServ = {};
	$scope.tdServ.employee = {};
	$scope.tdServ.ftdList = [];
	
	$scope.travVList = [];
	$scope.chqList = [];
	
	var temp;
	$scope.totAmtSum=0;
	$scope.bankCodeDBFlag = true;
	$scope.tdsCodeFlag = true;
	$scope.empCodeDBFlag = true;
	$scope.travVoucherDBFlag = true;
	$scope.travDetDBFlag = true;
	$scope.brdDetDBFlag = true;
	$scope.lodgDetDBFlag = true;
	$scope.convDetDBFlag = true;
	$scope.othDetDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.selChqDBFlag = true;
	
	$scope.faTrv1 = {};
	$scope.faTrv2 = {};
	$scope.faTrv3 = {};
	$scope.faTrv4 = {};
	$scope.faTrv5 = {};
	
	var max = 15;
	
	$('#hRateId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#hRateId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
    });
	
	$('#travAmtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#travAmtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
    });
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		var response = $http.post($scope.projectName+'/getVDetFrTravV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.TRV_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getAllEmp();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
			
	
	$scope.getAllEmp = function(){
		console.log("enter into getAllEmp function");
		var response = $http.post($scope.projectName+'/getAllEmpFrTravV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.empList = data.list;
				$scope.getTdsCode();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.payTo = "";
			$('#payToId').attr("disabled","disabled");
			$scope.empCode = "";
			$('#empCodeId').attr("disabled","disabled");	
		}else if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			
			$('#payToId').removeAttr("disabled");
			$('#empCodeId').removeAttr("disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}	
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	 $scope.saveBankCode =  function(bankCode){
			console.log("enter into saveBankCode----->"+bankCode);
			$scope.vs.bankCode = bankCode;
			$('div#bankCodeDB').dialog('close');
			$scope.bankCodeDBFlag = true;
			
			if($scope.vs.payBy === 'Q'){
				
				var chqDet = {
						"bankCode" : bankCode,
						"CType"    : $scope.vs.chequeType
				};
				
				var response = $http.post($scope.projectName+'/getChequeNoFrTravV',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$('#payToId').removeAttr("disabled");
						$('#empCodeId').removeAttr("disabled");
						$scope.chqList = data.list;
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						console.log("Error in bringing data from getChequeNo");
						$scope.alertToast("Branch does not have any "+$scope.vs.chequeType+" cheque of "+bankCode+" bank");
					}

				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}else{
				$('#payToId').removeAttr("disabled");
				$('#empCodeId').removeAttr("disabled");
			}		
	 }
	 
	 
	 $scope.openChqDB = function(){
		 console.log("enter into openChqDB function");
		    $scope.selChqDBFlag = false;
	    	$('div#selChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select Cheque No.",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selChqDBFlag = true;
			    }
				});
			$('div#selChqDB').dialog('open');
	 }
	
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function");
		 $scope.vs.chequeLeaves = chq;
		 $('div#selChqDB').dialog('close');
	 }
	 
	 
	 $scope.getTdsCode = function(){
			console.log("enter into getTdsCode function");
			var response = $http.post($scope.projectName+'/getTdsCode');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.fsMTdsList = data.list;
				}else{
					$scope.fetchVMEDetails();
					console.log(data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	}
	 
	
	 $scope.selectTds = function(){
			console.log("enter into selectTds function");
			$scope.tdsCodeFlag = false;
			$('div#tdsCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#tdsCodeDB').dialog('open');
	}
	 
	 
	 $scope.saveTdsCode = function(fsMTds){
			console.log("enter into saveTdsCode function");
			$scope.vs.tdsCode = fsMTds.faMfaCode;
			$scope.tdsCodeFlag = true;
			$('div#tdsCodeDB').dialog('close');
	} 
	 
	 
	 
	$scope.openEmpDB = function(){
		console.log("enter into openEmpDB function");
		
		$scope.travelling = false;
		$scope.boarding = false;
		$scope.lodging = false;
		$scope.conveyance = false;
		$scope.other = false;
		
		$scope.empCodeDBFlag = false;
		$('div#empCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#empCodeDB').dialog('open');
	} 
	
	
	$scope.saveEmpCode = function(emp){
		console.log("enter into saveEmpCode function");
		$scope.empCode = emp.empFaCode;
		$scope.empCodeDBFlag = true;
		$('div#empCodeDB').dialog('close');
		
		var countEmpNo = 0;
		 for(var i=0;i<$scope.travVList.length;i++){
			 if($scope.travVList[i].employee.empFaCode === emp.empFaCode){
				 countEmpNo = countEmpNo +1;
			 }
		 }
		 
		 if(countEmpNo > 0){
				$scope.alertToast("you already create the voucher of "+emp.empFaCode);
		 }else{
			 	 $scope.tdServ.employee = emp
				 $scope.travVoucherDBFlag = false;
				 $('div#travVoucherDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Voucher Details for "+emp.empName+" ("+emp.empFaCode+") Travel",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					    }
						});
					
					$('div#travVoucherDB').dialog('open');
		   }
	}
	
	
	
	$scope.saveTravelling = function(){
		 console.log("enter into saveTravelling function = "+$scope.travelling);
		 if($scope.travelling){
			 $('#travDetId').removeAttr("disabled");
		 }else{
			 $('#travDetId').attr("disabled","disabled");
		 }
	 } 
	
	
	$scope.openTravDet = function(){
		 console.log("enter into openTravDet function");
		 $scope.travDetDBFlag = false;
		 $('div#travDetDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Travelling Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#travDetDB').dialog('open');	
	 }
	
	
	$scope.submitTrvlDet = function(trvlDetForm){
		 console.log("enter into submitTrvlDet function");
		 if(trvlDetForm.$invalid){
			 $scope.alertToast("please fill correct Details of Travelling Exp");
		 }else{
			 $scope.travDetDBFlag = true;
			 $('div#travDetDB').dialog('close');
			 $scope.faTrv1.ftdTravType = "TRAVELLING";
			 
			 for(var i=0;i<$scope.tdServ.ftdList.length;i++){
				 if($scope.tdServ.ftdList[i].ftdTravType === "TRAVELLING"){
					 $scope.tdServ.ftdList.splice(i,1);
					 $scope.alertToast("UPDATE  TRAVELLING  EXP")	
				 }
			 }
			 
			 $scope.tdServ.ftdList.push($scope.faTrv1);
		 }
	 }
	
	
	$scope.saveBoarding = function(){
		 console.log("enter into saveBoarding function = "+$scope.boarding);
		 if($scope.boarding){
			 $('#travDetId').removeAttr("disabled");
		 }else{
			 $('#travDetId').attr("disabled","disabled");
		 }
	}
	
	
	$scope.openBrdDet = function(){
		console.log("enter into openBrdDet function");
		 $scope.brdDetDBFlag = false;
		 $('div#brdDetDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Boarding Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#brdDetDB').dialog('open');	
	}
	
	
	$scope.submitBrdDet = function(brdDetForm){
		console.log("enter into submitBrdDet function = "+brdDetForm.$invalid);
		if(brdDetForm.$invalid){
			 $scope.alertToast("please fill correct Details of Boarding Exp");
		 }else{
			 $scope.brdDetDBFlag = false;
			 $('div#brdDetDB').dialog('close');	
			 $scope.faTrv2.ftdTravType = "BOARDING";
			 
			 for(var i=0;i<$scope.tdServ.ftdList.length;i++){
				 if($scope.tdServ.ftdList[i].ftdTravType === "BOARDING"){
					 $scope.tdServ.ftdList.splice(i,1);
					 $scope.alertToast("UPDATE  BOARDING  EXP")	
				 }
			 }
			 
			 $scope.tdServ.ftdList.push($scope.faTrv2);
		 }
	}
	
	
	$scope.saveLodging = function(){
		console.log("enter into saveLodging function = "+$scope.lodging);
		 if($scope.lodging){
			 $('#lodgDetId').removeAttr("disabled");
		 }else{
			 $('#lodgDetId').attr("disabled","disabled");
		 }
	}
	
	
	$scope.openLodgDet = function(){
		console.log("enter into openLodgDet function");
		$scope.lodgDetDBFlag = false;
		 $('div#lodgDetDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Lodging Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#lodgDetDB').dialog('open');	
	}
	
	
	
	$scope.submitLodgDet = function(lodgDetForm){
		console.log("enter into submitLodgDet function = "+lodgDetForm.$invalid);
		if(lodgDetForm.$invalid){
			 $scope.alertToast("please fill correct Details of Lodging Exp");
		 }else{
			 $scope.lodgDetDBFlag = false;
			 $('div#lodgDetDB').dialog('close');	
			 $scope.faTrv3.ftdTravType = "LODGING";
			 
			 for(var i=0;i<$scope.tdServ.ftdList.length;i++){
				 if($scope.tdServ.ftdList[i].ftdTravType === "LODGING"){
					 $scope.tdServ.ftdList.splice(i,1);
					 $scope.alertToast("UPDATE  LODGING  EXP")	
				 }
			 }
			 
			 $scope.tdServ.ftdList.push($scope.faTrv3);
		 }
	}
	
	
	$scope.saveConv = function(){
		console.log("enter into saveConv funciton = "+$scope.conveyance);
		if($scope.conveyance){
			 $('#convDetId').removeAttr("disabled");
		 }else{
			 $('#convDetId').attr("disabled","disabled");
		 }
	}
	
	
	$scope.openConvDet = function(){
		console.log("enter into openConvDet function");
		$scope.convDetDBFlag = false;
		 $('div#convDetDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Conveyance Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#convDetDB').dialog('open');	
	}
	
	$scope.submitConvDet = function(convDetForm){
		console.log("enter into submitConvDet function = "+convDetForm.$invalid);
		if(convDetForm.$invalid){
			 $scope.alertToast("please fill correct Details of Conveyance Exp");
		 }else{
			 $scope.convDetDBFlag = true;
			 $('div#convDetDB').dialog('close');	
			 $scope.faTrv4.ftdTravType = "CONVEYANCE";
			 
			 for(var i=0;i<$scope.tdServ.ftdList.length;i++){
				 if($scope.tdServ.ftdList[i].ftdTravType === "CONVEYANCE"){
					 $scope.tdServ.ftdList.splice(i,1);
					 $scope.alertToast("UPDATE  CONVEYANCE  EXP")	
				 }
			 }
			 
			 $scope.tdServ.ftdList.push($scope.faTrv4);
		 }
	}
	
	
	$scope.saveOther = function(){
		console.log("enter into saveOther funciton = "+$scope.other);
		if($scope.other){
			 $('#othDetId').removeAttr("disabled");
		 }else{
			 $('#othDetId').attr("disabled","disabled");
		 }
	}
	
	
	$scope.openOthDet = function(){
		console.log("enter into openOthDet function");
		$scope.othDetDBFlag = false;
		 $('div#othDetDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Other Expense Details",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
			$('div#othDetDB').dialog('open');	
	}
	
	
	$scope.submitOthDet = function(othDetForm){
		console.log("enter into submitOthDet funciton = "+othDetForm.$invalid);
		if(othDetForm.$invalid){
			 $scope.alertToast("please fill correct Details of Other Exp");
		 }else{
			 $scope.othDetDBFlag = true;
			 $('div#othDetDB').dialog('close');	
			 $scope.faTrv5.ftdTravType = "OTHER";
			 
			 for(var i=0;i<$scope.tdServ.ftdList.length;i++){
				 if($scope.tdServ.ftdList[i].ftdTravType === "OTHER"){
					 $scope.tdServ.ftdList.splice(i,1);
					 $scope.alertToast("UPDATE  OTHER  EXP")	
				 }
			 }
			 
			 $scope.tdServ.ftdList.push($scope.faTrv5);
		 }
	}
	

	
	$scope.submitTRVoucher = function(travVoucherForm){
		console.log("enter into submitTRVoucher function = "+travVoucherForm.$invalid);
		if(travVoucherForm.$invalid){
			 $scope.alertToast("please enter correct details of Travel Expense");
		 }else{
			 $scope.travVoucherDBFlag = true;
			 $('div#travVoucherDB').dialog('close');
			 $scope.tdServ.totAmt = 0;
			 for(var i=0 ; i< $scope.tdServ.ftdList.length ; i++){
				 $scope.tdServ.totAmt =  $scope.tdServ.totAmt + $scope.tdServ.ftdList[i].ftdTravAmt;
			 }
			 $scope.totAmtSum=$scope.totAmtSum+$scope.tdServ.totAmt;
			 $scope.addTravEDetails();
		 }
	}
	
	
	$scope.addTravEDetails = function(){
		 console.log("enter into addVMDetails function");
		 var response = $http.post($scope.projectName+'/addTravEDetails',$scope.tdServ);
			response.success(function(data, status, headers, config){
				$scope.tdServ = {};
				$scope.tdServ.Employee = {};
				$scope.tdServ.ftdList = [];
				
				$scope.faTrv1 = {};
				$scope.faTrv2 = {};
				$scope.faTrv3 = {};
				$scope.faTrv4 = {};
				$scope.faTrv5 = {};
				
				$scope.fetchTravEDetails();
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	
	
	
	$scope.fetchTravEDetails = function(){
		console.log("enter into fetchTravEDetails function");
		var response = $http.post($scope.projectName+'/fetchTravEDetails');
		response.success(function(data, status, headers, config){
			$scope.travVList = data.list;
			temp=0;
			for(var i=0; i<data.list.length; i++){
				temp=temp+data.list[i].totAmt;
			}
			$scope.totAmtSum=temp;
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	} 
	
	
	
	$scope.removeTravE = function(index,travV){
		console.log("enter into removeTravE function")
		$scope.totAmtSum=$scope.totAmtSum-travV.totAmt;
		var remIndex = {
			"index" : index
		};
		var response = $http.post($scope.projectName+'/removeTravE',remIndex);
		response.success(function(data, status, headers, config){
			$scope.fetchTravEDetails();
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("entre into voucherSubmit function = "+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			$scope.alertToast("please enter correct vouher details") 
		}else{
			console.log("final submittion");
		    $scope.saveVsFlag = false;
	    	$('div#saveVsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveVsDB').dialog('open');
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	
	$scope.saveVS = function(){
		 console.log("enter into saveVS function");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
		 $('#saveId').attr("disabled","disabled");
		 var response = $http.post($scope.projectName+'/submitTravVoucher',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("success");
					$('#saveId').removeAttr("disabled");
					$scope.vs = {};
					$scope.tdServ = {};
					$scope.tdServ.employee = {};
					$scope.tdServ.ftdList = [];
					
					$scope.travVList = [];
					
					$scope.faTrv1 = {};
					$scope.faTrv2 = {};
					$scope.faTrv3 = {};
					$scope.faTrv4 = {};
					$scope.faTrv5 = {};
					$scope.getVoucherDetails();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);