package com.mylogistics.constants;

public class OwnerCNTS {


	public static final String OWN_CODE = "ownCode";
	
	public static final String OWN_FA_CODE = "ownFaCode";

	public static final String OWN_ID = "ownId";

	public static final String BRANCH_CODE = "branchCode";

	public static final String OWN_NAME = "ownName";

	public static final String OWN_COM_STB_DATE = "ownComStbDt";

	public static final String OWN_PAN_DOB = "ownPanDOB";

	public static final String OWN_PAN_DATE = "ownPanDt";

	public static final String OWN_PAN_NAME = "ownPanName";
	
	public static final String OWN_PAN_NO = "ownPanNo";

	public static final String OWN_VEHICLE_TYPE = "ownVehicleType";

	public static final String OWN_PP_NO = "ownPPNo";

	public static final String OWN_VOTER_ID = "ownVoterId";

	public static final String OWN_SRV_TAX_NO = "ownSrvTaxNo";

	public static final String OWN_FIRM_REG_NO = "ownFirmRegNo";

	public static final String OWN_REG_PLACE = "ownRegPlace";

	public static final String OWN_FIRM_TYPE = "ownFirmType";

	public static final String OWN_CIN = "ownCIN";

	public static final String OWN_BSN_CARD = "ownBsnCard";

	public static final String OWN_UNION = "ownUnion";

	public static final String OWN_EMAIL_ID = "ownEmailId";

	public static final String OWN_ACTIVE_DT = "ownActiveDt";

	public static final String OWN_DEACTIVE_DT = "ownDeactiveDt";

	public static final String OWN_USER_CODE = "userCode";

	public static final String USER_BRANCH_CODE = "bCode";

	public static final String CREATION_TS = "creationTS";

	public static final String IS_VIEW = "isView";

	public static final String IS_OWN_BNK_DET = "isOwnBnkDet";

	public static final String OWN_ACCNT_NO = "ownAccntNo";

	public static final String OWN_IFSC = "ownIfsc";

	public static final String OWN_MICR = "ownMicr";

	public static final String OWN_BNK_BRANCH = "ownBnkBranch";
	
	public static final String OWN_VALID_PAN = "ownValidPan";
	
	public static final String OWN_INVALID_PAN = "ownInvalidPan";
	
	public static final String OWN_VALID_BNK_DET = "ownBnkDetValid";
	
	public static final String OWN_INVALID_BNK_DET = "ownBnkDetInValid";
}
