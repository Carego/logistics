package com.mylogistics.constants;

public class VehicleMstrCNTS {
	
	public static final String VEH_ID = "vehId";
	
	public static final String VEH_NO = "vehNo";
	
	public static final String VEH_MODEL = "vehModel";
	
	public static final String VEH_TYPE = "vehType";
	
	public static final String VEH_OWN_NAME = "vehOwnName";
	
	public static final String VEH_OWN_ADD = "vehOwnAdd";
	
	public static final String VEH_POLICY_NO = "vehPolicyNo";
	
	public static final String VEH_INS_UPTO = "vehInsUpto";
	
	public static final String VEH_INS_PREM = "vehInsPrem";
	
	public static final String VEH_TAX_UPTO = "vehTaxUpto";
	
	public static final String VEH_TAX_PLACE = "vehTaxPlace";
	
	public static final String VEH_TAX_AMT = "vehTaxAmt";
	
	public static final String VEH_SALE_VAL = "vehSaleVal";
	
	public static final String VEH_SOLD_TO = "vehSoldTo";
	
	public static final String VEH_SOLD_DT = "vehSoldDt";
	
	public static final String VEH_FIT_UPTO = "vehFitUpto";
	
	public static final String VEH_FIT_PLACE = "vehFitPlace";
	
	public static final String VEH_STATUS = "vehStatus";
	
	public static final String VEH_COMMENT = "vehComment";
	
	public static final String VEH_ENGINE_NO = "vehEngineNo";
	
	public static final String VEH_CHESIS_NO = "vehChesisNo";
	
	public static final String VEH_WEF_DT = "vehWEFDt";
	
	public static final String VEH_EMP_LNO = "vehEmpLNo";
	
	public static final String VEH_EMP_LUPTO = "vehEmpLUpto";
	
	public static final String BRANCH = "branch";
	
	public static final String VEH_RTO_ADD = "vehRtoAdd";
	
	public static final String EMPLOYEE = "employee";
	
	

}
