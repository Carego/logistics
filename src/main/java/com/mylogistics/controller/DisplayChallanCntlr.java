package com.mylogistics.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.model.Challan;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayChallanCntlr {

	@Autowired
	ChallanDAO challanDAO;

	@Autowired
	private HttpSession httpSession;

	@RequestMapping(value="/getChlnIsViewNo",method = RequestMethod.POST)
	public @ResponseBody Object getChlnIsViewNo()throws Exception{

		System.out.println("Controller called of getChlnIsViewNo");
		Map<String,Object> map = new HashMap<String,Object>();

		User currentUser = (User)httpSession.getAttribute("currentUser");
		//System.out.println("Cureent USer Name = "+currentUser.getUserName());
		String branchCode = currentUser.getUserBranchCode();

		List<Challan> challans = challanDAO.getChallanisViewFalse(branchCode);
		List<Map<String,Object>> finalChlnList = new ArrayList<Map<String,Object>>();
		System.out.println("Controller called after retrieving list---------"+challans);

		if(!challans.isEmpty()){
			for(int i=0;i<challans.size();i++){
				/*System.out.println("The values in the list is"+challans.get(i).getBranchCode());
				System.out.println(" -------------->"+challans.get(0).getChallanImage().length());*/
				Map<String,Object> chlnMap = new HashMap<String, Object>();
				chlnMap.put(ChallanCNTS.CHLN_ID,challans.get(i).getChlnId());
				chlnMap.put(ChallanCNTS.BRANCH_CODE,challans.get(i).getBranchCode());
				chlnMap.put(ChallanCNTS.CHLN_ADVANCE,challans.get(i).getChlnAdvance());
				chlnMap.put(ChallanCNTS.CHLN_BALANCE,challans.get(i).getChlnBalance());
				chlnMap.put(ChallanCNTS.CHLN_BR_RATE,challans.get(i).getChlnBrRate());
				chlnMap.put(ChallanCNTS.CHLN_CHG_WT,challans.get(i).getChlnChgWt());
				chlnMap.put(ChallanCNTS.CHALLAN_CODE,challans.get(i).getChlnCode());
				chlnMap.put(ChallanCNTS.CHLN_CHG_WT,challans.get(i).getChlnChgWt());
				chlnMap.put(ChallanCNTS.CHLN_DT,challans.get(i).getChlnDt());
				chlnMap.put(ChallanCNTS.CHLN_EMP_CODE,challans.get(i).getChlnEmpCode());
				chlnMap.put(ChallanCNTS.CHLN_EXTRA,challans.get(i).getChlnExtra());
				chlnMap.put(ChallanCNTS.CHLN_FREIGHT,challans.get(i).getChlnFreight());
				chlnMap.put(ChallanCNTS.CHLN_FROM_STN,challans.get(i).getChlnFromStn());
				chlnMap.put(ChallanCNTS.CHLN_LOADING_AMT,challans.get(i).getChlnLoadingAmt());
				chlnMap.put(ChallanCNTS.CHLN_LRY_LOAD_TIME,challans.get(i).getChlnLryLoadTime());
				chlnMap.put(ChallanCNTS.CHLN_LRY_NO,challans.get(i).getChlnLryNo());
				chlnMap.put(ChallanCNTS.CHLN_LRY_RATE,challans.get(i).getChlnLryRate());
				chlnMap.put(ChallanCNTS.CHLN_LRY_REP_DT,challans.get(i).getChlnLryRepDT());
				chlnMap.put(ChallanCNTS.CHLN_LRY_RPT_TIME,challans.get(i).getChlnLryRptTime());
				chlnMap.put(ChallanCNTS.CHLN_NO_OF_PKG,challans.get(i).getChlnNoOfPkg());
				chlnMap.put(ChallanCNTS.CHLN_PAY_AT,challans.get(i).getChlnPayAt());
				chlnMap.put(ChallanCNTS.CHLN_RR_NO,challans.get(i).getChlnRrNo());
				chlnMap.put(ChallanCNTS.CHLN_STATISTICAL_CHG,challans.get(i).getChlnStatisticalChg());
				chlnMap.put(ChallanCNTS.CHLN_TDS_AMOUNT,challans.get(i).getChlnTdsAmt());
				chlnMap.put(ChallanCNTS.CHLN_TIME_ALLOW,challans.get(i).getChlnTimeAllow());
				chlnMap.put(ChallanCNTS.CHLN_TO_STN,challans.get(i).getChlnToStn());
				chlnMap.put(ChallanCNTS.CHLN_TOTAL_FREIGHT,challans.get(i).getChlnTotalFreight());
				chlnMap.put(ChallanCNTS.CHLN_TOTAL_WT,challans.get(i).getChlnTotalWt());
				chlnMap.put(ChallanCNTS.CHLN_TRAIN_NO,challans.get(i).getChlnTrainNo());
				chlnMap.put(ChallanCNTS.CHLN_VEHICLE_TYPE,challans.get(i).getChlnVehicleType());
				chlnMap.put(ChallanCNTS.CHLN_WT_SLIP,challans.get(i).getChlnWtSlip());
				chlnMap.put(ChallanCNTS.CREATION_TS,challans.get(i).getCreationTS());
				chlnMap.put(ChallanCNTS.USER_BRANCH_CODE,challans.get(i).getUserCode());
				chlnMap.put(ChallanCNTS.USER_BRANCH_CODE,challans.get(i).getbCode());
				chlnMap.put(ChallanCNTS.IS_VIEW,challans.get(i).isView());
				chlnMap.put(ChallanCNTS.USER_CODE,challans.get(i).getUserCode());

				finalChlnList.add(chlnMap);
			}
		}

		if(!finalChlnList.isEmpty()){
			System.out.println("enter into if -------------->"+challans.get(0).getBranchCode());
			map.put("list",finalChlnList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
			System.out.println("map is ready for client");
		}else {
			System.out.println("enter into else -------------->");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		System.out.println("finally send the data");
		return map;
	}

	/*@RequestMapping(value = "/getAllChallanCode" , method = RequestMethod.GET)  
	  public @ResponseBody Object getAllChallanCode(){

		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();

		List<Challan> challans = challanDAO.getAllChallanCodes(branchCode);
		String chlnCodes [] = new String[challans.size()];

		for(int i=0;i<challans.size();i++){
			chlnCodes[i]=challans.get(i).getChlnCode();	
		}

		return chlnCodes;

		}*/ 

	@RequestMapping(value="/getAllChallanCode",method = RequestMethod.POST)
	public @ResponseBody Object getAllChallanCode(){

		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode=currentUser.getUserBranchCode();
		System.out.println("branch code of current user------>"+branchCode);

		List<Challan> challanCodeList = challanDAO.getAllChallanCodes(branchCode);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(challanCodeList.isEmpty())) {			
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("challanCodeList",challanCodeList);
		}else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	@RequestMapping(value="/downloadChallan" , method = RequestMethod.POST)
	public void downloadChallan(HttpServletRequest request , HttpServletResponse response )throws Exception {
		System.out.println("enter into downloadChallan funntion");
		String chlnCode = request.getParameter("submitImageName");
		System.out.println("*************chlnCode = "+chlnCode);
		//String chlnCode = challan.getChlnCode();
		String fileName = chlnCode+".jpg";

		Blob chlnImage = challanDAO.getChallanImageByCode(chlnCode);

		//Blob chlnImage = challan.getChallanImage();

		InputStream in = chlnImage.getBinaryStream();
		int fileLength = in.available();
		System.out.println("####################size of downloading image = " + fileLength);
		String mimeType = "application/octet-stream";
		response.setContentType(mimeType);
		response.setContentLength(fileLength);
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		OutputStream out = response.getOutputStream();
		byte[] buffer = new byte[2048];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
		}
		in.close();
		out.flush();
		out.close(); 
		//return "success";
	}


	/*@RequestMapping(value="/challanDetails" , method = RequestMethod.POST)
	public @ResponseBody Object challanDetails(@RequestBody String chlnCode) {
		System.out.println("enter into challanDetails fucntion");
		Map<String,Object> map = new HashMap<String, Object>();

		return map;
	}*/

	@RequestMapping(value = "/updateIsViewChallan", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewChallan(@RequestBody String selection) {

		String contids[] = selection.split(",");

		int temp = challanDAO.updateChallanisViewTrue(contids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getImage", method = RequestMethod.POST)
	public @ResponseBody Object getImage()throws Exception {
		System.out.println("enter into getImage function");
		Map<String, Object> map = new HashMap<String, Object>();
		Blob blob = challanDAO.getChallanImageByCode("1234201");
		InputStream in = blob.getBinaryStream();
		
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		
		System.out.println("&&&&&&&&&&&&&&&&&&&"+blobAsBytes.length);
		map.put("chImage",blobAsBytes);
		return map;
	}
}
