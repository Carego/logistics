'use strict';

var app = angular.module('application');

app.controller('LedgerCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("LedgerCntlr Started");
	
	$scope.branchList = [];
	$scope.cashStmtList = [];
	$scope.faCodeNameList = [];
	$scope.frmFaCodeList = [];
	$scope.toFaCodeList = [];
	
	$scope.branch = {};
	
	$scope.fromFaCode="";
	$scope.toFaCode="";
	
	$scope.isConsolidate = false;
	$scope.isBranch = false;
	$scope.showTableFlag = false;
	
	$scope.branchDBFlag = true;
	$scope.frmFACodeDBFlag = true;
	$scope.toFACodeDBFlag = true;
	$scope.isFormSubmitted = false;
	
	$scope.openFromFaCodeDB=function(){
		console.log("openfromFaCodeDB()")
		if($scope.fromFaCode.length==4){
			
			var frmFaCode=$scope.fromFaCode;
			console.log("frmFaCode "+frmFaCode);	
			var res = $http.post($scope.projectName+'/getFaCodeFrLedger', frmFaCode);
			res.success(function(data, status, headers, config) {
				
				if(data.result === "success"){
					$scope.frmFACodeDBFlag = false;
					 $scope.frmFaCodeList = data.codeList;
					  $('div#frmFACodeDB').dialog({
						  autoOpen: false,
						  modal:true,
						  resizable: false,
						  position: UDPos,
						  show: UDShow,
						  hide: UDHide,
						  title: "FACode",
						  draggable: true,
						  close: function(event, ui) { 
							  $(this).dialog('destroy');
							  $(this).hide();
							  $scope.frmFACodeDBFlag = true;
							  
						  }
					  });
					  $('div#frmFACodeDB').dialog('open');
						} else{
							$scope.alertToast("No Record found");
						}
					});
					
				res.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	}
	
	$scope.saveFrmFACode = function(fromFaCode) {
		console.log("saveFrmFACode()");
		$scope.fromFaCode = fromFaCode;
		$('div#frmFACodeDB').dialog('close');
	}
	
	$scope.openToFaCodeDB=function(){
		console.log("openToFaCodeDB()")
		
		if($scope.toFaCode.length==4){
			var toFaCode=$scope.toFaCode;
			console.log("toFaCode "+toFaCode);	
			var res = $http.post($scope.projectName+'/getFaCodeFrLedger', toFaCode);
			res.success(function(data, status, headers, config) {
				
				if(data.result === "success"){
					$scope.toFACodeDBFlag = false;
					 $scope.toFaCodeList = data.codeList;
					  $('div#toFACodeDB').dialog({
						  autoOpen: false,
						  modal:true,
						  resizable: false,
						  position: UDPos,
						  show: UDShow,
						  hide: UDHide,
						  title: "FACode",
						  draggable: true,
						  close: function(event, ui) { 
							  $(this).dialog('destroy');
							  $(this).hide();
							  $scope.toFACodeDBFlag = true;
							  
						  }
					  });
					  $('div#toFACodeDB').dialog('open');
						} else{
							$scope.alertToast("No Record found");
						}
					});
					
				res.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}

		}
	
	$scope.saveToFACode = function(toFaCode) {
		console.log("saveToFACode()");
		$scope.toFaCode = toFaCode;
		$('div#toFACodeDB').dialog('close');
	}
	
	
	
	$scope.consolidateRB = function() {
		console.log("consolidateRB()");
		$scope.branch = {};
		$scope.isConsolidate = true;
		$scope.isBranch = false;
		$('#branchNameId').attr("disabled","disabled");
		
	}
	
	$scope.branchRB = function() {
		console.log("branchRB()");
		$scope.isConsolidate = false;
		$scope.isBranch = true;
		$('#branchNameId').removeAttr("disabled");
	}
	
	$scope.getBranchNameIdFa = function() {
		console.log("getBranchNameIdFa");
		
		var response = $http.post($scope.projectName+'/getBranchNameIdFa');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.branchList = data.branchList;
			}else {
				console.log("getBankMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
		
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.ledgerSubmit = function(ledgerForm) {
		if (ledgerForm.$valid) {
			$scope.isFormSubmitted = false;
			if ($scope.isBranch) {
				if (angular.isUndefined($scope.branch.branchName) || $scope.branch.branchName === null || $scope.branch.branchName == "") {
					$scope.alertToast("Please enter Branch");
				}else {
					$scope.getLedgerReport();
				}
			}else if ($scope.isConsolidate){
				$scope.getLedgerReport();
			}else {
				$scope.alertToast("Please select either Branch or Consolidate");
			}
		} else {
			$scope.isFormSubmitted = true;
		}
	}
	
	$scope.getLedgerReport = function() {
		console.log("getLedgerReport()");
		$('#printLedgerId').attr("disabled","disabled");
		$('#printXlsId').attr("disabled","disabled");
		$scope.showTableFlag = false;
		var ledgerReport = {
				"fromDt"		: $scope.fromDt,
				"toDt"			: $scope.toDt,
				"fromFaCode"	: $scope.fromFaCode,
				"toFaCode"		: $scope.toFaCode,
				"branchId"		: $scope.branch.branchId,
				"branchName"	: $scope.branch.branchName,
				"isBranch"		: $scope.isBranch,
				"isConsolidate"	:$scope.isConsolidate
		}
		
		$('#ledgerSubmitId').attr("disabled","disabled");
		
		var response = $http.post($scope.projectName+'/getLedgerReportHtml', ledgerReport);
		response.success(function(data, status, headers, config){
			$('#ledgerSubmitId').removeAttr("disabled");
			if (data.result === "success") {
				
				$scope.alertToast(data.result);
				$scope.cashStmtList = data.ledgerMapList;
				$scope.faCodeNameList = data.faCodeNameList;
				$scope.showTableFlag = true;
				$('#printLedgerId').removeAttr("disabled");
				$('#printXlsId').removeAttr("disabled");
			}else {
				$scope.alertToast(data.result);
				$scope.showTableFlag = false;
				$('#printLedgerId').attr("disabled","disabled");
				$('#printXlsId').attr("disabled","disabled");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getLedgerReport Error: "+data);
			$('#ledgerSubmitId').removeAttr("disabled");
			$scope.alertToast("database exception");
		});
	}
	
	//print pdf
	$scope.printPdf = function() {
		console.log("printPdf()");
		//$window.location.href = $scope.projectName+'/getLedgerReportPdf';
		window.open($scope.projectName+'/getLedgerReportPdf','_blank');
		
	}
	
	//print xls
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "ledger.xls");
	}
	
	
	$('#fromFaCodeId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#fromFaCodeId').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});
	
	$('#toFaCodeId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#toFaCodeId').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchNameIdFa();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("LedgerCntlr Ended");
}]);