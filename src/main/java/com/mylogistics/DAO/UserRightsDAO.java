package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.UserRights;

@Repository
public interface UserRightsDAO {

	public UserRights getUserRights(int userId);
	
	public int saveUserRights(UserRights userRights);
	
}
