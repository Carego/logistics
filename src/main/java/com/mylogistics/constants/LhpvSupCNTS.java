package com.mylogistics.constants;

public class LhpvSupCNTS {

	public static final String LHPV_SUP_DT = "lspDt";
	
	public static final String LHPV_SUP_BCODE = "bCode";
	
	public static final String LHPV_SUP_ID = "lspId";
	
	public static final String LHPV_SUP_BRKOWN = "lspBrkOwn";
	
	public static final String LHPV_SUP_FINAL_TOT = "lspFinalTot";
	
	public static final String LHPV_SUP_WT_SHRTG = "lspWtShrtgCR";
	
	public static final String LHPV_SUP_DR_RCVR_WT = "lspDrRcvrWtCR";
	
	public static final String LHPV_SUP_LATE_DEL = "lspLateDelCR";
	
	public static final String LHPV_SUP_LATE_ACK = "lspLateAckCR";
	
	public static final String LHPV_SUP_OTH_EXT_KM = "lspOthExtKmP";
	
	public static final String LHPV_SUP_OTH_OVR_HGT = "lspOthOvrHgtP";
	
	public static final String LHPV_SUP_OTH_PNLTY = "lspOthPnltyP";
	
	public static final String LHPV_SUP_OTH_MISC = "lspOthMiscP";
	
	public static final String LHPV_SUP_UNLOADING = "lspUnLoadingP";
	
	public static final String LHPV_SUP_UNP_DET = "lspUnpDetP";
	
	public static final String LHPV_SUP_TOT_RCVR_AMT = "lspTotRcvrAmt";
	
	public static final String LHPV_SUP_PAY_METHOD = "lspPayMethod";
	
	public static final String LHPV_SUP_IS_CLOSE = "lspIsClose";
	
	public static final String LHPV_SUP_CHQ_NO = "lspChqNo";
	
	public static final String LHPV_SUP_BANK_CODE = "lspBankCode";

}
