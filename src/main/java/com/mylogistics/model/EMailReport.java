package com.mylogistics.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "emailrepot")
public class EMailReport {

	@Id@GeneratedValue
	@Column(name = "emrId")	
	private Integer emrId;
	@NotNull@NotBlank
	@Column(name = "emrReportName")	
	private String emrRoportName;
	@NotNull@NotBlank
	@Column(name = "emrSubject")
	private String emrSubject;
	@NotNull@NotBlank
	@Column(name = "emrBody")
	private String emrBody;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(
			name = "email_list_mapping",
			joinColumns = @JoinColumn(name = "emrId"),
			inverseJoinColumns = @JoinColumn(name = "emrlId")
		)
	List<EMailReportList> emrEMailReportList;

	public EMailReport() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getEmrId() {
		return emrId;
	}

	public void setEmrId(Integer emrId) {
		this.emrId = emrId;
	}

	public String getEmrRoportName() {
		return emrRoportName;
	}

	public void setEmrRoportName(String emrRoportName) {
		this.emrRoportName = emrRoportName;
	}

	public String getEmrSubject() {
		return emrSubject;
	}

	public void setEmrSubject(String emrSubject) {
		this.emrSubject = emrSubject;
	}

	public String getEmrBody() {
		return emrBody;
	}

	public void setEmrBody(String emrBody) {
		this.emrBody = emrBody;
	}

	public List<EMailReportList> getEmrEMailReportList() {
		return emrEMailReportList;
	}

	public void setEmrEMailReportList(List<EMailReportList> emrEMailReportList) {
		this.emrEMailReportList = emrEMailReportList;
	}	
	
}