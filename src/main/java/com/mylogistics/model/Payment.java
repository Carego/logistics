package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "payment")
public class Payment {
	
	@Id
	@Column(name="ptId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ptId;
	
	@Column(name="ptCode")
	private String ptCode;
	
	@Column(name="billedOn")
	private String billedOn;
	
	@Column(name="billSubTerm")
	private String 	billSubTerm;
	
	@Column(name="billPaymentTerm")
	private String billPaymentTerm;
	
	@Column(name="contId")
	private int contId;
	
	@Column(name="custId")
	private int custId;
	
	
	public int getPtId() {
		return ptId;
	}
	public void setPtId(int ptId) {
		this.ptId = ptId;
	}
	public String getPtCode() {
		return ptCode;
	}
	public void setPtCode(String ptCode) {
		this.ptCode = ptCode;
	}
	public String getBilledOn() {
		return billedOn;
	}
	public void setBilledOn(String billedOn) {
		this.billedOn = billedOn;
	}
	public String getBillSubTerm() {
		return billSubTerm;
	}
	public void setBillSubTerm(String billSubTerm) {
		this.billSubTerm = billSubTerm;
	}
	public String getBillPaymentTerm() {
		return billPaymentTerm;
	}
	public void setBillPaymentTerm(String billPaymentTerm) {
		this.billPaymentTerm = billPaymentTerm;
	}
	public int getContId() {
		return contId;
	}
	public void setContId(int contId) {
		this.contId = contId;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	
	
}
