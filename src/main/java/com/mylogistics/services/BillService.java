package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.Bill;

public class BillService {

	private List<BillDetService> bdSerList = new ArrayList<>();
	
	private Bill bill;

	public List<BillDetService> getBdSerList() {
		return bdSerList;
	}

	public void setBdSerList(List<BillDetService> bdSerList) {
		this.bdSerList = bdSerList;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}
	
	
}
