package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.IBTVoucherDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.TempIntBrTv;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.IBTService;
import com.mylogistics.services.VoucherService;

public class IBTVoucherDAOImpl implements IBTVoucherDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public IBTVoucherDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> saveIBTVoucher(IBTService ibtService){
		System.out.println("enter into saveIBTVoucher function");
		Map<String,Object> map = new HashMap<String, Object>();
		VoucherService voucherService = ibtService.getVoucherService();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<TempIntBrTv> tibList = ibtService.getTibList();
		
		int voucherNo = 0;
		if(!tibList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				 
				 Criteria cr = session.createCriteria(CashStmt.class);
				 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				 ProjectionList proList = Projections.projectionList();
				 proList.add(Projections.property("csVouchNo"));
				 cr.setProjection(proList);
					
				 List<Integer>  voucherNoList = cr.list();
				 if(!voucherNoList.isEmpty()){
						int vNo = voucherNoList.get(voucherNoList.size() - 1);
						voucherNo = vNo + 1;
				 }else{
						voucherNo = 1;
				 }
				 
				 for(int i=0;i<tibList.size();i++){
					 
					 Date date = new Date(new java.util.Date().getTime());
					 Calendar calendar = Calendar.getInstance();
					 calendar.setTime(date);
					 System.out.println("unique id =====>"+calendar.getTimeInMillis());
					 long milliSec = calendar.getTimeInMillis();
					 String tvNo = String.valueOf(milliSec);
						
					 TempIntBrTv tempIntBrTv = tibList.get(i);
					  
					 CashStmt cashStmt1 = new CashStmt();
					 cashStmt1.setbCode(currentUser.getUserBranchCode());
					 cashStmt1.setUserCode(currentUser.getUserCode());
					 cashStmt1.setCsDescription(tempIntBrTv.getTibDesc());
					 cashStmt1.setCsDrCr(tempIntBrTv.getTibDOrC());
					 cashStmt1.setCsAmt(tempIntBrTv.getTibAmt());
					 cashStmt1.setCsType(voucherService.getVoucherType());
					 cashStmt1.setCsVouchType("cash");
					 cashStmt1.setCsTvNo(tvNo);
					 cashStmt1.setCsFaCode(tempIntBrTv.getTibFaCode());
					 cashStmt1.setCsVouchNo(voucherNo);
					 cashStmt1.setCsDt(sqlDate);
					 cashStmt1.setPayMode("C");
					 
					 List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
					 Criteria cr1 = session.createCriteria(CashStmtStatus.class);
					 cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					 cssList = cr1.list();
					 
					 int csId = 0;
					 if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
					 }
					 
					 tempIntBrTv.setbCode(currentUser.getUserBranchCode());
					 tempIntBrTv.setUserCode(currentUser.getUserCode());
					 tempIntBrTv.setTibBranchCode(voucherService.getBranch().getBranchFaCode());
					 if(csId > 0){
						 tempIntBrTv.setTibCsId(csId);
					 }
					 tempIntBrTv.setTibIsClear(false);
					 
					 int tbId = (Integer) session.save(tempIntBrTv);
					 cashStmt1.setCsSFId(tbId);
					 session.update(cashStmt1);
				 }
				 
				transaction.commit();
				session.flush();	
				map.put("vhNo",voucherNo); 
			}catch(Exception e){
				e.printStackTrace();
				map.put("vhNo",0);
			}
			session.clear();
			session.close();
		}else{
			System.out.println("tibList is empty");
		}
		
		return map;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int clrSIBTV(TempIntBrTv tempIntBrTv){
		System.out.println("enter into clrSIBTV function");
		int res = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int voucherNo = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Date csDate = tempIntBrTv.getTibDate();
			
			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,csDate));
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
			ProjectionList proList = Projections.projectionList();
			proList.add(Projections.property("csVouchNo"));
			cr.setProjection(proList);
				
			List<Integer>  voucherNoList = cr.list();
			 if(!voucherNoList.isEmpty()){
					int vNo = voucherNoList.get(voucherNoList.size() - 1);
					voucherNo = vNo + 1;
			 }else{
					voucherNo = 1;
			 }
			
			CashStmt cashStmt = (CashStmt) session.get(CashStmt.class,tempIntBrTv.getTibCsId());
			
			List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
			cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,csDate));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
			cssList = cr.list();
			
			if(!cssList.isEmpty()){
				
				CashStmtStatus css = cssList.get(0);
				
				CashStmt cashStmt1 = new CashStmt();
				cashStmt1.setbCode(currentUser.getUserBranchCode());
				cashStmt1.setUserCode(currentUser.getUserCode());
				cashStmt1.setCsDescription("CLEAR "+cashStmt.getCsDescription());
				if(cashStmt.getCsDrCr() == 'C'){
					 cashStmt1.setCsDrCr('D');
				}else{
					cashStmt1.setCsDrCr('C');
				}
				cashStmt1.setCsAmt(cashStmt.getCsAmt());
				cashStmt1.setCsAmt(cashStmt.getCsAmt());
				cashStmt1.setCsType(cashStmt.getCsType());
				cashStmt1.setCsTvNo(cashStmt.getCsTvNo());
				cashStmt1.setCsFaCode(tempIntBrTv.getTibBranchCode());
				cashStmt1.setCsVouchNo(voucherNo);
				cashStmt1.setCsDt(csDate);
				cashStmt1.setPayMode(cashStmt.getPayMode());
				
				if(tempIntBrTv.getTibBnkFaCode() != null){
					 cashStmt1.setCsVouchType("BANK");
					 
					 CashStmt cashStmt2 = new CashStmt();
					 cashStmt2.setbCode(currentUser.getUserBranchCode());
					 cashStmt2.setUserCode(currentUser.getUserCode());
					 cashStmt2.setCsDescription("CLEAR "+cashStmt.getCsDescription());
					 if(cashStmt1.getCsDrCr() == 'C'){
						 cashStmt2.setCsDrCr('D');
					 }else{
						 cashStmt2.setCsDrCr('C');
					 }
					 cashStmt2.setCsAmt(cashStmt.getCsAmt());
					 cashStmt2.setCsType(cashStmt.getCsType());
					 cashStmt2.setCsTvNo(cashStmt.getCsTvNo());
					 cashStmt2.setCsFaCode(tempIntBrTv.getTibBnkFaCode());
					 cashStmt2.setCsVouchNo(voucherNo);
					 cashStmt2.setCsDt(csDate);
					 cashStmt2.setCsVouchType("BANK");
					 cashStmt2.setCsSFId(tempIntBrTv.getTibId());
					 cashStmt2.setCsNo(css);
					 cashStmt2.setPayMode(cashStmt.getPayMode());
					 
					 session.save(cashStmt2);
					 css.getCashStmtList().add(cashStmt2);
					 session.update(css);
					 
					 List<BankMstr> bankMstrList = new ArrayList<>();
					 String bankCode = tempIntBrTv.getTibBnkFaCode();
					 cr=session.createCriteria(BankMstr.class);
					 cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					 bankMstrList = cr.list();
					 
					 if(!bankMstrList.isEmpty()){
							
							BankMstr bank = bankMstrList.get(0);
							double balAmt = bank.getBnkBalanceAmt();
							double dedAmt = cashStmt.getCsAmt();
							double newBalAmt = balAmt - dedAmt;
							System.out.println("balAmt = "+balAmt);
							System.out.println("dedAmt = "+dedAmt);
							System.out.println("newBalAmt = "+newBalAmt);
							bank.setBnkBalanceAmt(newBalAmt);
							session.merge(bank);
					}	                                                                                                    
					 
				}else{
					cashStmt1.setCsVouchType("CASH");
				}
				
				
				CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
				cashStmt1.setCsSFId(tempIntBrTv.getTibId());
				cashStmt1.setCsNo(css);
				session.save(cashStmt1);
				actCss.getCashStmtList().add(cashStmt1);
				session.merge(actCss);
				
				
				TempIntBrTv actTIB = (TempIntBrTv) session.get(TempIntBrTv.class,tempIntBrTv.getTibId());
				actTIB.setTibBnkFaCode(tempIntBrTv.getTibBnkFaCode());
				actTIB.setTibIsClear(true);
				
				session.merge(actTIB);
				res = 1;
			}
			
			transaction.commit();
			session.flush();	
			
		}catch(Exception e){
			e.printStackTrace();
			res = 0;
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> clrAllIBTV(IBTService ibtService){
		System.out.println("enter into cleAllIBTV function");
		Map<String,Object> map = new HashMap<String,Object>();
		VoucherService voucherService = ibtService.getVoucherService();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<TempIntBrTv> tibList = ibtService.getTibList();
		
		int voucherNo = 0;
		if(!tibList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				 
				 Criteria cr = session.createCriteria(CashStmt.class);
				 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				 ProjectionList proList = Projections.projectionList();
				 proList.add(Projections.property("csVouchNo"));
				 cr.setProjection(proList);
					
				 List<Integer>  voucherNoList = cr.list();
				 if(!voucherNoList.isEmpty()){
						int vNo = voucherNoList.get(voucherNoList.size() - 1);
						voucherNo = vNo + 1;
				 }else{
						voucherNo = 1;
				 }
				 
				 

				 for(int i=0;i<tibList.size();i++){
					 	
					 TempIntBrTv tempIntBrTv = tibList.get(i);
					 System.out.println("tempIntBrTv.getTibCsId() = "+tempIntBrTv.getTibCsId());
					 tempIntBrTv.setTibIsClear(true);
					 session.update(tempIntBrTv) ;
					 
					 List<CashStmt> csList = new ArrayList<CashStmt>();
					 Criteria cr1 = session.createCriteria(CashStmt.class);
					 cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,tempIntBrTv.getTibCsId()));
					 csList = cr1.list();
					 
					 CashStmt cashStmt = csList.get(0);
					 
					 CashStmt cashStmt1 = new CashStmt();
					 cashStmt1.setbCode(currentUser.getUserBranchCode());
					 cashStmt1.setUserCode(currentUser.getUserCode());
					 cashStmt1.setCsDescription("CLEAR "+cashStmt.getCsDescription());
					 if(cashStmt.getCsDrCr() == 'C'){
						 cashStmt1.setCsDrCr('D');
					 }else{
						 cashStmt1.setCsDrCr('C');
					 }
					 cashStmt1.setCsAmt(cashStmt.getCsAmt());
					 cashStmt1.setCsType(voucherService.getVoucherType());
					 cashStmt1.setCsTvNo(cashStmt.getCsTvNo());
					 cashStmt1.setCsFaCode(tempIntBrTv.getTibBranchCode());
					 cashStmt1.setCsVouchNo(voucherNo);
					 cashStmt1.setCsDt(sqlDate);
					 
					 if(tempIntBrTv.getTibBnkFaCode() != null){
						 cashStmt1.setCsVouchType("BANK");
						 
						 CashStmt cashStmt2 = new CashStmt();
						 cashStmt2.setbCode(currentUser.getUserBranchCode());
						 cashStmt2.setUserCode(currentUser.getUserCode());
						 cashStmt2.setCsDescription("CLEAR "+cashStmt.getCsDescription());
						 if(cashStmt1.getCsDrCr() == 'C'){
							 cashStmt2.setCsDrCr('D');
						 }else{
							 cashStmt2.setCsDrCr('C');
						 }
						 cashStmt2.setCsAmt(cashStmt.getCsAmt());
						 cashStmt2.setCsType(voucherService.getVoucherType());
						 cashStmt2.setCsTvNo(cashStmt.getCsTvNo());
						 cashStmt2.setCsDt(sqlDate);
						 cashStmt2.setCsFaCode(tempIntBrTv.getTibBnkFaCode());
						 cashStmt2.setCsVouchNo(voucherNo);
						 cashStmt2.setCsDt(sqlDate);
						 cashStmt2.setCsVouchType("BANK");
						 
						 List<CashStmtStatus> cssList1 = new ArrayList<CashStmtStatus>();
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList1 = cr.list();
						 
						 int csId2 = 0;
						 if(!cssList1.isEmpty()){
								CashStmtStatus csStatus = cssList1.get(0);
								cashStmt2.setCsSFId(tempIntBrTv.getTibId());
								cashStmt2.setCsNo(csStatus);
								csId2 = (Integer) session.save(cashStmt2);
								csStatus.getCashStmtList().add(cashStmt2);
								session.update(csStatus);
						 }
						 
						 
						 List<BankMstr> bankMstrList = new ArrayList<>();
						 String bankCode = tempIntBrTv.getTibBnkFaCode();
						 cr=session.createCriteria(BankMstr.class);
						 cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
						 bankMstrList = cr.list();
						 
						 if(!bankMstrList.isEmpty()){
								
								BankMstr bank = bankMstrList.get(0);
								double balAmt = bank.getBnkBalanceAmt();
								double dedAmt = cashStmt.getCsAmt();
								double newBalAmt = balAmt - dedAmt;
								System.out.println("balAmt = "+balAmt);
								System.out.println("dedAmt = "+dedAmt);
								System.out.println("newBalAmt = "+newBalAmt);
								bank.setBnkBalanceAmt(newBalAmt);
								session.merge(bank);
						}	
						 
					 }else{
						 cashStmt1.setCsVouchType("CASH");
					 }
					 
					 List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
					 Criteria cr2 = session.createCriteria(CashStmtStatus.class);
					 cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					 cssList = cr2.list();
					 
					 int csId = 0;
					 if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsSFId(tempIntBrTv.getTibId());
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
					 }
				
				 }
				 
				transaction.commit();
				session.flush();	
				map.put("vhNo",voucherNo);  
			}catch(Exception e){
				e.printStackTrace();
				map.put("vhNo",0);  
			}
			session.clear();
			session.close();
		}else{
			System.out.println("tibtList is empty");
		}
		return map;
	}
}
