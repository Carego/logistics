package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.JVoucherDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.JourVouchDRCR;
import com.mylogistics.services.VoucherService;

public class JVoucherDAOImpl implements JVoucherDAO{
	
	public static final Logger logger = Logger.getLogger(JVoucherDAOImpl.class);
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public JVoucherDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> saveJVoucher(VoucherService voucherService , List<JourVouchDRCR> jvList){
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatusTemp = voucherService.getCashStmtStatus();
		//java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>"+calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		map.put("tvNo",tvNo);
		
		int voucherNo = 0;
		if(!jvList.isEmpty()){
			try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 
				 Criteria cr = session.createCriteria(CashStmtStatus.class);
				 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,cashStmtStatusTemp.getCssDt()));
				 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
				 cssList = cr.list();
				 
				 if(!cssList.isEmpty()){
					 CashStmtStatus cashStmtStatus = cssList.get(0);
					 
					 java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					 
					 cr = session.createCriteria(CashStmt.class);
					 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
					 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
					 ProjectionList proList = Projections.projectionList();
					 proList.add(Projections.property("csVouchNo"));
					 cr.setProjection(proList);
						
					 List<Integer>  voucherNoList = cr.list();
					 if(!voucherNoList.isEmpty()){
							int vNo = voucherNoList.get(voucherNoList.size() - 1);
							voucherNo = vNo + 1;
					 }else{
							voucherNo = 1;
					 }
					 
					 for(int i=0; i<jvList.size() ; i++){
						 
						 JourVouchDRCR jvDrCr = jvList.get(i);
						 if(jvDrCr.getCrAmt() > 0){
							 
							 if(jvDrCr.getFaCode().substring(0,2).equalsIgnoreCase("07")){
								 
								 List<BankMstr> bnkList = new ArrayList<>();
								 cr = session.createCriteria(BankMstr.class);
								 cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,jvDrCr.getFaCode()));
								 bnkList = cr.list();
								 
								 if(!bnkList.isEmpty()){
									 BankMstr bankMstr = bnkList.get(0);
									 double newBalAmt = bankMstr.getBnkBalanceAmt() - jvDrCr.getCrAmt();
									 bankMstr.setBnkBalanceAmt(newBalAmt);
									 
									 session.merge(bankMstr);
									
									 ChequeLeaves chqLTemp = null;
									 
									 if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= ""){
										List<ChequeLeaves> chqList = bankMstr.getChequeLeavesList(); 
										
										if(!chqList.isEmpty()){
											
											for (ChequeLeaves chequeLeaves : chqList) {
												if (chequeLeaves.getChqLChqNo().equalsIgnoreCase(jvDrCr.getChqNo())) {
													
													chequeLeaves.setChqLChqAmt(jvDrCr.getCrAmt());		
													chequeLeaves.setChqLUsed(true);
													chequeLeaves.setChqLUsedDt(cashStmtStatus.getCssDt());
													chqLTemp = chequeLeaves;
													session.merge(chequeLeaves);
												}
											}
											
										}
									 }
									 
									 CashStmt cashStmt1 = new CashStmt();
									 cashStmt1.setbCode(currentUser.getUserBranchCode());
									 cashStmt1.setUserCode(currentUser.getUserCode());
									 cashStmt1.setCsDescription(jvDrCr.getDesc());
									 cashStmt1.setCsDrCr('C');
									 cashStmt1.setCsAmt(jvDrCr.getCrAmt());
									 cashStmt1.setCsType(voucherService.getVoucherType());
									 cashStmt1.setCsVouchType("contra");
									 //cashStmt1.setCsTvNo(jvDrCr.getChqNo()+"000000");
									 cashStmt1.setCsTvNo(jvDrCr.getChqNo());
									 cashStmt1.setCsFaCode(jvDrCr.getFaCode());
									 cashStmt1.setCsVouchNo(voucherNo);
									 cashStmt1.setCsDt(sqlDate);
									 if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= "")
										 cashStmt1.setPayMode("Q");
									 else
										 cashStmt1.setPayMode("C");
									 if (chqLTemp != null) {
										 cashStmt1.setCsChequeType(chqLTemp.getChqLCType());
									}
									 
									 
									 
									 Criteria cr1 = session.createCriteria(CashStmtStatus.class);
									 cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									 cssList = cr1.list();
									 
									 if(!cssList.isEmpty()){
											CashStmtStatus csStatus = cssList.get(0);
											cashStmt1.setCsNo(csStatus);
										    session.save(cashStmt1);
											csStatus.getCashStmtList().add(cashStmt1);
											session.update(csStatus);
									 }
									 
								 }
								 
							 }else{
								 CashStmt cashStmt1 = new CashStmt();
								 cashStmt1.setbCode(currentUser.getUserBranchCode());
								 cashStmt1.setUserCode(currentUser.getUserCode());
								 cashStmt1.setCsDescription(jvDrCr.getDesc());
								 cashStmt1.setCsDrCr('C');
								 cashStmt1.setCsAmt(jvDrCr.getCrAmt());
								 cashStmt1.setCsType(voucherService.getVoucherType());
								 cashStmt1.setCsVouchType("contra");
								 cashStmt1.setCsTvNo(tvNo);
								 cashStmt1.setCsFaCode(jvDrCr.getFaCode());
								 cashStmt1.setCsVouchNo(voucherNo);
								 cashStmt1.setCsDt(sqlDate);
								 cashStmt1.setPayMode("C");
								 
								 Criteria cr1 = session.createCriteria(CashStmtStatus.class);
								 cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								 cssList = cr1.list();
								 
								 if(!cssList.isEmpty()){
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt1.setCsNo(csStatus);
									    session.save(cashStmt1);
										csStatus.getCashStmtList().add(cashStmt1);
										session.update(csStatus);
								 }
							 }
							 							 
						 }else{
							 
							 if(jvDrCr.getFaCode().substring(0,2).equalsIgnoreCase("07")){
								 
								 List<BankMstr> bnkList = new ArrayList<>();
								 cr = session.createCriteria(BankMstr.class);
								 cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,jvDrCr.getFaCode()));
								 bnkList = cr.list();
								 
								 if(!bnkList.isEmpty()){
									 BankMstr bankMstr = bnkList.get(0);
									 double newBalAmt = bankMstr.getBnkBalanceAmt() + jvDrCr.getDbAmt();
									 bankMstr.setBnkBalanceAmt(newBalAmt);
									 
									 session.merge(bankMstr);
										 
									 ChequeLeaves chqLTemp = null;
									 
									 if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= ""){
										List<ChequeLeaves> chqList = bankMstr.getChequeLeavesList(); 
										
										if(!chqList.isEmpty()){
											
											for (ChequeLeaves chequeLeaves : chqList) {
												if (chequeLeaves.getChqLChqNo().equalsIgnoreCase(jvDrCr.getChqNo())) {
													
													chequeLeaves.setChqLChqAmt(jvDrCr.getDbAmt());		
													chequeLeaves.setChqLUsed(true);
													chequeLeaves.setChqLUsedDt(cashStmtStatus.getCssDt());
													chqLTemp = chequeLeaves;
													session.merge(chequeLeaves);
												}
											}
											
										}
									 }
									 
									 
									 
									 CashStmt cashStmt2 = new CashStmt();
									 cashStmt2.setbCode(currentUser.getUserBranchCode());
									 cashStmt2.setUserCode(currentUser.getUserCode());
									 cashStmt2.setCsDescription(jvDrCr.getDesc());
									 cashStmt2.setCsDrCr('D');
									 cashStmt2.setCsAmt(jvDrCr.getDbAmt());
									 cashStmt2.setCsType(voucherService.getVoucherType());
									 cashStmt2.setCsVouchType("contra");
									 cashStmt2.setCsTvNo(jvDrCr.getChqNo());
									 cashStmt2.setCsFaCode(jvDrCr.getFaCode());
									 cashStmt2.setCsVouchNo(voucherNo);
									 cashStmt2.setCsDt(sqlDate);
									 if (chqLTemp != null) {
										 cashStmt2.setCsChequeType(chqLTemp.getChqLCType());
									}
									if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= "")
										 cashStmt2.setPayMode("Q");
									else
										cashStmt2.setPayMode("C");
									 
								 	
								     Criteria cr2 = session.createCriteria(CashStmtStatus.class);
									 cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									 cssList = cr2.list();
									 if(!cssList.isEmpty()){
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt2.setCsNo(csStatus);
										session.save(cashStmt2);
										csStatus.getCashStmtList().add(cashStmt2);
										session.update(csStatus);
									 } 
									 
								 }
								 
							 }else{
								 CashStmt cashStmt2 = new CashStmt();
								 cashStmt2.setbCode(currentUser.getUserBranchCode());
								 cashStmt2.setUserCode(currentUser.getUserCode());
								 cashStmt2.setCsDescription(jvDrCr.getDesc());
								 cashStmt2.setCsDrCr('D');
								 cashStmt2.setCsAmt(jvDrCr.getDbAmt());
								 cashStmt2.setCsType(voucherService.getVoucherType());
								 cashStmt2.setCsVouchType("contra");
								 cashStmt2.setCsTvNo(tvNo);
								 cashStmt2.setCsFaCode(jvDrCr.getFaCode());
								 cashStmt2.setCsVouchNo(voucherNo);
								 cashStmt2.setCsDt(sqlDate);
								 cashStmt2.setPayMode("C");
							 	
							     Criteria cr2 = session.createCriteria(CashStmtStatus.class);
								 cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								 cssList = cr2.list();
								 if(!cssList.isEmpty()){
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt2.setCsNo(csStatus);
									session.save(cashStmt2);
									csStatus.getCashStmtList().add(cashStmt2);
									session.update(csStatus);
								 } 
							 }
						 }
					 }
					 transaction.commit();
					 session.flush();	
					 map.put("vhNo",voucherNo);
					 
				 }else{
					 map.put("vhNo",-1);
					 map.put("msg","invalid CS date");
				 }
				 
			}catch(Exception e){
				e.printStackTrace();
				map.put("vhNo",0);
			}
			session.clear();
			session.close();
		}else{
			System.out.println("jvList is empty");
			map.put("vhNo",0);
		}
		
		return map;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> saveJVoucherN(VoucherService voucherService , List<JourVouchDRCR> jvList){
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatusTemp = voucherService.getCashStmtStatus();
		
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		logger.info("unique id =====>"+calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		map.put("tvNo",tvNo);
		
		Session session = this.sessionFactory.openSession();
		 
		int voucherNo = 0;
		if(!jvList.isEmpty()){
			Transaction transaction = session.beginTransaction();
			try{
				 Criteria cr = session.createCriteria(CashStmtStatus.class);
				 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,cashStmtStatusTemp.getCssDt()));
				 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
				 List<CashStmtStatus> cssList = cr.list();
				 
				 if(!cssList.isEmpty()){
					 CashStmtStatus cashStmtStatus = cssList.get(0);
					 
					 java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					 
					 cr = session.createCriteria(CashStmt.class);
					 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
					 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
					 ProjectionList proList = Projections.projectionList();
					 proList.add(Projections.property("csVouchNo"));
					 cr.setProjection(proList);
						
					 List<Integer>  voucherNoList = cr.list();
					 if(!voucherNoList.isEmpty()){
							int vNo = voucherNoList.get(voucherNoList.size() - 1);
							voucherNo = vNo + 1;
					 }else{
							voucherNo = 1;
					 }
					 
					 for(int i=0; i<jvList.size() ; i++){
						 
						 JourVouchDRCR jvDrCr = jvList.get(i);
						 if(jvDrCr.getCrAmt() > 0){
							 
							 if(jvDrCr.getFaCode().substring(0,2).equalsIgnoreCase("07")){
								 
								 cr = session.createCriteria(BankMstr.class);
								 cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,jvDrCr.getFaCode()));
								 List<BankMstr> bnkList = cr.list();
								 
								 if(!bnkList.isEmpty()){
									 BankMstr bankMstr = bnkList.get(0);
									 double newBalAmt = bankMstr.getBnkBalanceAmt() - jvDrCr.getCrAmt();
									 bankMstr.setBnkBalanceAmt(newBalAmt);
									 
									 session.merge(bankMstr);
									
									 ChequeLeaves chqLTemp = null;
									 
									 if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= ""){
										List<ChequeLeaves> chqList = bankMstr.getChequeLeavesList(); 
										
										if(!chqList.isEmpty()){
											
											for (ChequeLeaves chequeLeaves : chqList) {
												if (chequeLeaves.getChqLChqNo().equalsIgnoreCase(jvDrCr.getChqNo())) {
													
													chequeLeaves.setChqLChqAmt(jvDrCr.getCrAmt());		
													chequeLeaves.setChqLUsed(true);
													chequeLeaves.setChqLUsedDt(cashStmtStatus.getCssDt());
													chqLTemp = chequeLeaves;
													session.merge(chequeLeaves);
												}
											}
											
										}
									 }
									 CashStmt cashStmt1 = new CashStmt();
									 cashStmt1.setbCode(currentUser.getUserBranchCode());
									 cashStmt1.setUserCode(currentUser.getUserCode());
									 cashStmt1.setCsDescription(jvDrCr.getDesc());
									 cashStmt1.setCsDrCr('C');
									 cashStmt1.setCsAmt(jvDrCr.getCrAmt());
									 cashStmt1.setCsType(voucherService.getVoucherType());
									 cashStmt1.setCsVouchType("contra");
									 cashStmt1.setCsTvNo(jvDrCr.getChqNo());
									 cashStmt1.setCsFaCode(jvDrCr.getFaCode());
									 cashStmt1.setCsVouchNo(voucherNo);
									 cashStmt1.setCsDt(sqlDate);
									 if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= "") {
										 cashStmt1.setPayMode("Q");
										 cashStmt1.setCsTvNo(jvDrCr.getChqNo());
									} else {
										cashStmt1.setPayMode("C");
										cashStmt1.setCsTvNo(tvNo);
									}
									 if (chqLTemp != null) {
										 cashStmt1.setCsChequeType(chqLTemp.getChqLCType());
									}
									 Criteria cr1 = session.createCriteria(CashStmtStatus.class);
									 cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									 cssList = cr1.list();
									 
									 if(!cssList.isEmpty()){
											CashStmtStatus csStatus = cssList.get(0);
											cashStmt1.setCsNo(csStatus);
										    session.save(cashStmt1);
											csStatus.getCashStmtList().add(cashStmt1);
											session.update(csStatus);
									 }
								 }
							 }else{
								 CashStmt cashStmt1 = new CashStmt();
								 cashStmt1.setbCode(currentUser.getUserBranchCode());
								 cashStmt1.setUserCode(currentUser.getUserCode());
								 cashStmt1.setCsDescription(jvDrCr.getDesc());
								 cashStmt1.setCsDrCr('C');
								 cashStmt1.setCsAmt(jvDrCr.getCrAmt());
								 cashStmt1.setCsType(voucherService.getVoucherType());
								 cashStmt1.setCsVouchType("contra");
								 cashStmt1.setCsTvNo(tvNo);
								 cashStmt1.setCsFaCode(jvDrCr.getFaCode());
								 cashStmt1.setCsVouchNo(voucherNo);
								 cashStmt1.setCsDt(sqlDate);
								 cashStmt1.setPayMode("C");
								 
								 Criteria cr1 = session.createCriteria(CashStmtStatus.class);
								 cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								 cssList = cr1.list();
								 
								 if(!cssList.isEmpty()){
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt1.setCsNo(csStatus);
									    session.save(cashStmt1);
										csStatus.getCashStmtList().add(cashStmt1);
										session.update(csStatus);
								 }
							 }
						 }else{
							 if(jvDrCr.getFaCode().substring(0,2).equalsIgnoreCase("07")){								 
								 cr = session.createCriteria(BankMstr.class);
								 cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,jvDrCr.getFaCode()));
								 List<BankMstr> bnkList = cr.list();
								 
								 if(!bnkList.isEmpty()){
									 BankMstr bankMstr = bnkList.get(0);
									 double newBalAmt = bankMstr.getBnkBalanceAmt() + jvDrCr.getDbAmt();
									 bankMstr.setBnkBalanceAmt(newBalAmt);
									 
									 session.merge(bankMstr);
										 
									 ChequeLeaves chqLTemp = null;
									 
									 if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= ""){
										List<ChequeLeaves> chqList = bankMstr.getChequeLeavesList(); 
										
										if(!chqList.isEmpty()){
											
											for (ChequeLeaves chequeLeaves : chqList) {
												if (chequeLeaves.getChqLChqNo().equalsIgnoreCase(jvDrCr.getChqNo())) {
													
													chequeLeaves.setChqLChqAmt(jvDrCr.getDbAmt());		
													chequeLeaves.setChqLUsed(true);
													chequeLeaves.setChqLUsedDt(cashStmtStatus.getCssDt());
													chqLTemp = chequeLeaves;
													session.merge(chequeLeaves);
												}
											}
											
										}
									 }
									 
									 CashStmt cashStmt2 = new CashStmt();
									 cashStmt2.setbCode(currentUser.getUserBranchCode());
									 cashStmt2.setUserCode(currentUser.getUserCode());
									 cashStmt2.setCsDescription(jvDrCr.getDesc());
									 cashStmt2.setCsDrCr('D');
									 cashStmt2.setCsAmt(jvDrCr.getDbAmt());
									 cashStmt2.setCsType(voucherService.getVoucherType());
									 cashStmt2.setCsVouchType("contra");
									 cashStmt2.setCsFaCode(jvDrCr.getFaCode());
									 cashStmt2.setCsVouchNo(voucherNo);
									 cashStmt2.setCsDt(sqlDate);
									 if (chqLTemp != null) {
										 cashStmt2.setCsChequeType(chqLTemp.getChqLCType());
									}
									if(jvDrCr.getChqNo() != null && jvDrCr.getChqNo()!= "") {
										 cashStmt2.setPayMode("Q");
										 cashStmt2.setCsTvNo(jvDrCr.getChqNo());
									} else {
										cashStmt2.setPayMode("C");
										cashStmt2.setCsTvNo(tvNo);
									}
									 
								     Criteria cr2 = session.createCriteria(CashStmtStatus.class);
									 cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									 cssList = cr2.list();
									 if(!cssList.isEmpty()){
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt2.setCsNo(csStatus);
										session.save(cashStmt2);
										csStatus.getCashStmtList().add(cashStmt2);
										session.update(csStatus);
									 } 
									 
								 }
								 
							 }else{
								 CashStmt cashStmt2 = new CashStmt();
								 cashStmt2.setbCode(currentUser.getUserBranchCode());
								 cashStmt2.setUserCode(currentUser.getUserCode());
								 cashStmt2.setCsDescription(jvDrCr.getDesc());
								 cashStmt2.setCsDrCr('D');
								 cashStmt2.setCsAmt(jvDrCr.getDbAmt());
								 cashStmt2.setCsType(voucherService.getVoucherType());
								 cashStmt2.setCsVouchType("contra");
								 cashStmt2.setCsTvNo(tvNo);
								 cashStmt2.setCsFaCode(jvDrCr.getFaCode());
								 cashStmt2.setCsVouchNo(voucherNo);
								 cashStmt2.setCsDt(sqlDate);
								 cashStmt2.setPayMode("C");
							 	
							     Criteria cr2 = session.createCriteria(CashStmtStatus.class);
								 cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								 cssList = cr2.list();
								 if(!cssList.isEmpty()){
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt2.setCsNo(csStatus);
									session.save(cashStmt2);
									csStatus.getCashStmtList().add(cashStmt2);
									session.update(csStatus);
								 } 
							 }
						 }
					 }
					 map.put("vhNo",voucherNo);
					 
				 }else{
					 map.put("vhNo",-1);
					 map.put("msg","invalid CS date");
				 }
				 transaction.commit();
				 session.flush();
			}catch(Exception e){
				transaction.rollback();
				e.printStackTrace();
				map.put("vhNo",0);
			} 
		}else{
			logger.info("jvList is empty");
			map.put("vhNo",0);
		}
		session.clear();
		session.close();
		return map;
	}
	
}
