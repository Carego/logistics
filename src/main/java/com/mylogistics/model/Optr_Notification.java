package com.mylogistics.model;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "optr_notification")
public class Optr_Notification {

	@Id
	@Column(name="onId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int onId;
	
	@Column(name="dispatchCode")
	private String dispatchCode;
	
	@Column(name="dispatchDate")
	private Date dispatchDate;
	
	@Column(name="operatorCode")
	private String operatorCode;
	
	@Column(name="isRecieved")
	private String isRecieved;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public int getOnId() {
		return onId;
	}

	public void setOnId(int onId) {
		this.onId = onId;
	}

	public String getDispatchCode() {
		return dispatchCode;
	}

	public void setDispatchCode(String dispatchCode) {
		this.dispatchCode = dispatchCode;
	}

	public Date getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getIsRecieved() {
		return isRecieved;
	}

	public void setIsRecieved(String isRecieved) {
		this.isRecieved = isRecieved;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
}
