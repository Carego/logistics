package com.mylogistics.model.webservice;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
//import java.util.Date;


@Entity
@Table(name = "cnmtapp")

public class CnmtApp {
	
	@Id
	@Column(name="cnmtId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer cnmtId;
	
	@Column(name="cnmtCode")
	private String cnmtCode;		
	
	@Column(name="cnmtCustCode")
	private String cnmtCustCode;
	
	@Column(name="cnmtCustFaCode")
	private String cnmtCustFaCode;
	
	@Column(name="cnmtVOG")
	private Double cnmtVOG;
	
	@Column(name="cnmtDt")
	private Date cnmtDt;
	
	@Column(name="cnmtFromStCode")
	private String cnmtFromStCode;
	
	@Column(name="cnmtFromStName")
	private String cnmtFromStName;
	
	@Column(name="cnmtContCode")
	private String cnmtContCode;
	
	@Column(name="cnmtContFaCode")
	private String cnmtContFaCode;
	
	@Column(name="cnmtToStCode")
	private String cnmtToStCode;
	
	@Column(name="cnmtToStName")
	private String cnmtToStName;
	
	@Column(name="cnmtConsignorCode")
	private String cnmtConsignorCode;
	
	@Column(name="cnmtConsignorFaCode")
	private String cnmtConsignorFaCode;
	
	@Column(name="cnmtConsigneeCode")
	private String cnmtConsigneeCode;
	
	@Column(name="cnmtConsigneeFaCode")
	private String cnmtConsigneeFaCode;
	
	@Column(name="cnmtVehicleTypeCode")
	private String cnmtVehicleTypeCode;
	
	@Column(name="cnmtVehicleType")
	private String cnmtVehicleType;	
	
	@Column(name="cnmtProductType")
	private String cnmtProductType;
	
	@Column(name="cnmtActualWt")
	private Double cnmtActualWt;
	
	@Column(name="cnmtGuaranteeWt")
	private Double cnmtGuaranteeWt;
	
	@Column(name="cnmtKm")
	private Double cnmtKm;
	
	@Column(name="cnmtStateCode")
	private String cnmtStateCode;
	
	@Column(name="cnmtState")
	private String cnmtState;
	
	@Column(name="cnmtPayAtCode")
	private String cnmtPayAtCode;
	
	@Column(name="cnmtPayAtFaCode")
	private String cnmtPayAtFaCode;
	
	@Column(name="cnmtBillAtCode")
	private String cnmtBillAtCode;
	
	@Column(name="cnmtBillAtFaCode")
	private String cnmtBillAtFaCode;
	
	@Column(name="cnmtRate")
	private Double cnmtRate;
	
	@Column(name="cnmtNoOfPkg")
	private Integer cnmtNoOfPkg;
	
	@Column(name="cnmtFreight")
	private Double cnmtFreight;
	
	@Column(name="cnmtTOT")
	private Double cnmtTOT;
	
	@Column(name="cnmtEmpCode")
	private String cnmtEmpCode;
	
	@Column(name="cnmtEmpFaCode")
	private String cnmtEmpFaCode;
	
	@Column(name="cnmtExtraExp")
	private Double cnmtExtraExp;	
	
	@Column(name="cnmtDtdDly")
	private String cnmtDtdDly;
	
	@Column(name="cnmtDc")
	private String cnmtDc;
	
	@Column(name="cnmtCostGrade")
	private String cnmtCostGrade;
	
	@Column(name="cnmtInvoiceNo")
	private String cnmtInvoiceNo;
	
	@Column(name="cnmtInvoiceDate")
	private String cnmtInvoiceDate;
	
	@Column(name="cnmtInvoiceDt")
	private Date cnmtInvoiceDt;

	@Column(name="cnmtUserCode")
	private String cnmtUserCode;
	
	@Column(name="cnmtBCode")
	private String cnmtBCode;
	
	@Column(name="cnmtBranchCode")
	private String cnmtBranchCode;
	
	@Column(name="cnmtIsPending")
	private Boolean cnmtIsPending;
	
	@Column(name="cnmtIsCancel")
	private Boolean cnmtIsCancel;
	
	@Column(name="creationTS", insertable = false, updatable = true)
	private Date creationTS;
	
	@Transient
	private Boolean dirty;	

	public Integer getCnmtId() {
		return cnmtId;
	}

	public void setCnmtId(Integer cnmtId) {
		this.cnmtId = cnmtId;
	}

	public String getCnmtCode() {
		return cnmtCode;
	}

	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}

	public String getCnmtCustCode() {
		return cnmtCustCode;
	}

	public void setCnmtCustCode(String cnmtCustCode) {
		this.cnmtCustCode = cnmtCustCode;
	}

	public String getCnmtCustFaCode() {
		return cnmtCustFaCode;
	}

	public void setCnmtCustFaCode(String cnmtCustFaCode) {
		this.cnmtCustFaCode = cnmtCustFaCode;
	}

	public Double getCnmtVOG() {
		return cnmtVOG;
	}

	public void setCnmtVOG(Double cnmtVOG) {
		this.cnmtVOG = cnmtVOG;
	}

	public Date getCnmtDt() {
		return cnmtDt;
	}

	public void setCnmtDt(Date cnmtDt) {
		this.cnmtDt = cnmtDt;
	}

	public String getCnmtFromStCode() {
		return cnmtFromStCode;
	}

	public void setCnmtFromStCode(String cnmtFromStCode) {
		this.cnmtFromStCode = cnmtFromStCode;
	}

	public String getCnmtFromStName() {
		return cnmtFromStName;
	}

	public void setCnmtFromStName(String cnmtFromStName) {
		this.cnmtFromStName = cnmtFromStName;
	}

	public String getCnmtContCode() {
		return cnmtContCode;
	}

	public void setCnmtContCode(String cnmtContCode) {
		this.cnmtContCode = cnmtContCode;
	}

	public String getCnmtContFaCode() {
		return cnmtContFaCode;
	}

	public void setCnmtContFaCode(String cnmtContFaCode) {
		this.cnmtContFaCode = cnmtContFaCode;
	}

	public String getCnmtToStCode() {
		return cnmtToStCode;
	}

	public void setCnmtToStCode(String cnmtToStCode) {
		this.cnmtToStCode = cnmtToStCode;
	}

	public String getCnmtToStName() {
		return cnmtToStName;
	}

	public void setCnmtToStName(String cnmtToStName) {
		this.cnmtToStName = cnmtToStName;
	}

	public String getCnmtConsignorCode() {
		return cnmtConsignorCode;
	}

	public void setCnmtConsignorCode(String cnmtConsignorCode) {
		this.cnmtConsignorCode = cnmtConsignorCode;
	}

	public String getCnmtConsignorFaCode() {
		return cnmtConsignorFaCode;
	}

	public void setCnmtConsignorFaCode(String cnmtConsignorFaCode) {
		this.cnmtConsignorFaCode = cnmtConsignorFaCode;
	}

	public String getCnmtConsigneeCode() {
		return cnmtConsigneeCode;
	}

	public void setCnmtConsigneeCode(String cnmtConsigneeCode) {
		this.cnmtConsigneeCode = cnmtConsigneeCode;
	}

	public String getCnmtConsigneeFaCode() {
		return cnmtConsigneeFaCode;
	}

	public void setCnmtConsigneeFaCode(String cnmtConsigneeFaCode) {
		this.cnmtConsigneeFaCode = cnmtConsigneeFaCode;
	}

	public String getCnmtVehicleTypeCode() {
		return cnmtVehicleTypeCode;
	}

	public void setCnmtVehicleTypeCode(String cnmtVehicleTypeCode) {
		this.cnmtVehicleTypeCode = cnmtVehicleTypeCode;
	}

	public String getCnmtVehicleType() {
		return cnmtVehicleType;
	}

	public void setCnmtVehicleType(String cnmtVehicleType) {
		this.cnmtVehicleType = cnmtVehicleType;
	}

	public String getCnmtProductType() {
		return cnmtProductType;
	}

	public void setCnmtProductType(String cnmtProductType) {
		this.cnmtProductType = cnmtProductType;
	}

	public Double getCnmtActualWt() {
		return cnmtActualWt;
	}

	public void setCnmtActualWt(Double cnmtActualWt) {
		this.cnmtActualWt = cnmtActualWt;
	}

	public Double getCnmtGuaranteeWt() {
		return cnmtGuaranteeWt;
	}

	public void setCnmtGuaranteeWt(Double cnmtGuaranteeWt) {
		this.cnmtGuaranteeWt = cnmtGuaranteeWt;
	}

	public Double getCnmtKm() {
		return cnmtKm;
	}

	public void setCnmtKm(Double cnmtKm) {
		this.cnmtKm = cnmtKm;
	}

	public String getCnmtState() {
		return cnmtState;
	}

	public void setCnmtState(String cnmtState) {
		this.cnmtState = cnmtState;
	}

	public String getCnmtPayAtCode() {
		return cnmtPayAtCode;
	}

	public void setCnmtPayAtCode(String cnmtPayAtCode) {
		this.cnmtPayAtCode = cnmtPayAtCode;
	}

	public String getCnmtPayAtFaCode() {
		return cnmtPayAtFaCode;
	}

	public void setCnmtPayAtFaCode(String cnmtPayAtFaCode) {
		this.cnmtPayAtFaCode = cnmtPayAtFaCode;
	}

	public String getCnmtBillAtCode() {
		return cnmtBillAtCode;
	}

	public void setCnmtBillAtCode(String cnmtBillAtCode) {
		this.cnmtBillAtCode = cnmtBillAtCode;
	}

	public String getCnmtBillAtFaCode() {
		return cnmtBillAtFaCode;
	}

	public void setCnmtBillAtFaCode(String cnmtBillAtFaCode) {
		this.cnmtBillAtFaCode = cnmtBillAtFaCode;
	}

	public Double getCnmtRate() {
		return cnmtRate;
	}

	public void setCnmtRate(Double cnmtRate) {
		this.cnmtRate = cnmtRate;
	}

	public Integer getCnmtNoOfPkg() {
		return cnmtNoOfPkg;
	}

	public void setCnmtNoOfPkg(Integer cnmtNoOfPkg) {
		this.cnmtNoOfPkg = cnmtNoOfPkg;
	}

	public Double getCnmtFreight() {
		return cnmtFreight;
	}

	public void setCnmtFreight(Double cnmtFreight) {
		this.cnmtFreight = cnmtFreight;
	}

	public Double getCnmtTOT() {
		return cnmtTOT;
	}

	public void setCnmtTOT(Double cnmtTOT) {
		this.cnmtTOT = cnmtTOT;
	}

	public String getCnmtEmpCode() {
		return cnmtEmpCode;
	}

	public void setCnmtEmpCode(String cnmtEmpCode) {
		this.cnmtEmpCode = cnmtEmpCode;
	}

	public String getCnmtEmpFaCode() {
		return cnmtEmpFaCode;
	}

	public void setCnmtEmpFaCode(String cnmtEmpFaCode) {
		this.cnmtEmpFaCode = cnmtEmpFaCode;
	}

	public Double getCnmtExtraExp() {
		return cnmtExtraExp;
	}

	public void setCnmtExtraExp(Double cnmtExtraExp) {
		this.cnmtExtraExp = cnmtExtraExp;
	}

	public String getCnmtDtdDly() {
		return cnmtDtdDly;
	}

	public void setCnmtDtdDly(String cnmtDtdDly) {
		this.cnmtDtdDly = cnmtDtdDly;
	}

	public String getCnmtDc() {
		return cnmtDc;
	}

	public void setCnmtDc(String cnmtDc) {
		this.cnmtDc = cnmtDc;
	}

	public String getCnmtCostGrade() {
		return cnmtCostGrade;
	}

	public void setCnmtCostGrade(String cnmtCostGrade) {
		this.cnmtCostGrade = cnmtCostGrade;
	}

	public String getCnmtInvoiceNo() {
		return cnmtInvoiceNo;
	}

	public void setCnmtInvoiceNo(String cnmtInvoiceNo) {
		this.cnmtInvoiceNo = cnmtInvoiceNo;
	}

	public Date getCnmtInvoiceDt() {
		return cnmtInvoiceDt;
	}

	public void setCnmtInvoiceDt(Date cnmtInvoiceDt) {
		this.cnmtInvoiceDt = cnmtInvoiceDt;
	}

	public String getCnmtUserCode() {
		return cnmtUserCode;
	}

	public void setCnmtUserCode(String cnmtUserCode) {
		this.cnmtUserCode = cnmtUserCode;
	}

	public String getCnmtBCode() {
		return cnmtBCode;
	}

	public void setCnmtBCode(String cnmtBCode) {
		this.cnmtBCode = cnmtBCode;
	}

	public String getCnmtBranchCode() {
		return cnmtBranchCode;
	}

	public void setCnmtBranchCode(String cnmtBranchCode) {
		this.cnmtBranchCode = cnmtBranchCode;
	}

	public Boolean getCnmtIsPending() {
		return cnmtIsPending;
	}

	public void setCnmtIsPending(Boolean cnmtIsPending) {
		this.cnmtIsPending = cnmtIsPending;
	}

	public Boolean getCnmtIsCancel() {
		return cnmtIsCancel;
	}

	public void setCnmtIsCancel(Boolean cnmtIsCancel) {
		this.cnmtIsCancel = cnmtIsCancel;
	}

	public String getCnmtStateCode() {
		return cnmtStateCode;
	}

	public void setCnmtStateCode(String cnmtStateCode) {
		this.cnmtStateCode = cnmtStateCode;
	}

	public Boolean getDirty() {
		return dirty;
	}

	public void setDirty(Boolean dirty) {
		this.dirty = dirty;
	}	

	public Date getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Date creationTS) {
		this.creationTS = creationTS;
	}

	public String getCnmtInvoiceDate() {
		return cnmtInvoiceDate;
	}

	public void setCnmtInvoiceDate(String cnmtInvoiceDate) {
		this.cnmtInvoiceDate = cnmtInvoiceDate;
	}
	
	
}