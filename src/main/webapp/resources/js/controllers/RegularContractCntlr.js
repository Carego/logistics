'use strict';

var app = angular.module('application');

app.controller('RegularContractCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.regCnt = {};
	$scope.show = "true";
	$scope.selection=[];
	$scope.rbkm = {};
	$scope.pbdP = {};
	$scope.pbdB = {};
	$scope.pbdD = {};
	$scope.pt = {};
	$scope.vt = {};
	$scope.tnc={};
	$scope.type =[];
	$scope.cts={};
	$scope.station={};
	
	
	$scope.metricType = "Ton";
	$scope.afterSaveContCode = "";
	$scope.afterSaveDBFlag = true;
	$scope.branchCodeFlag = true;
	$scope.blpmCodeFlag = true;
	$scope.CngrCodeFlag = true;
	$scope.CrNameFlag = true;
	$scope.FromStationFlag = true;
	$scope.ToStationFlag = true;
	$scope.VehicleTypeFlag = true;
	$scope.ProductTypeFlag = true;
	$scope.RbkmIdFlag = true;
	$scope.ProductTypeFlag1 = true;
	$scope.VehicleTypeFlag1 = true;
	$scope.rbkmFromStationFlag = true;
	$scope.checkTransitFlagRbkm=true;
	$scope.checkTransitFlagRbkm=true;
	$scope.checkStatisticalChargeFlagRbkm=true;
	$scope.rbkmToStationFlag = true;
	$scope.rbkmVehicleTypeFlag = true;
	$scope.PenaltyFlag = true;
	$scope.BonusFlag = true;
	$scope.DetentionFlag = true;
	$scope.PenaltyVehicleTypeFlag = true;
	$scope.PenaltyToStationFlag = true;
	$scope.PenaltyFromStationFlag = true;
	$scope.BonusVehicleTypeFlag = true;
	$scope.BonusToStationFlag = true;
	$scope.BonusFromStationFlag = true;
	$scope.DetentionVehicleTypeFlag = true;
	$scope.DetentionToStationFlag = true;
	$scope.DetentionFromStationFlag = true;
	$scope.rbkmStateCodeFlag = true;
	$scope.Qflag=true;
	$scope.Wflag=true;
	$scope.Kflag=true;
	$scope.checkUnLoadFlag=true;
	$scope.checkUnLoadFlagRbkm=true;
	$scope.checkPenaltyFlag=true;
	$scope.checkLoadFlag=true;
	$scope.checkLoadFlagRbkm=true;
	$scope.checkBonusFlag=true;
	$scope.checkDetentionFlag=true;
	$scope.checkTransitFlag=true;
	$scope.checkStatisticalChargeFlag=true;
	$scope.Fflag=true;
	$scope.regContFlag = true;
	$scope.regCnt.regContType=false;
	$scope.contToStationFlag = true;
	$scope.ctsflagW=false;
	$scope.ctsflagQ=false;
	$scope.onWQClick=false;
	$scope.onKClick=false;
	$scope.ToStationsDBFlag = true;
	$scope.addStationFlag = true;
	$scope.StateCodeFlag = true;
	$scope.RbkmAddVTFlag = true;
	$scope.contToStationFlagQ = true;
	$scope.contToStationFlagW = true;
	$scope.AddProductTypeFlag=true;
	$scope.contToStnTemp=[];
	$scope.rbkmStnList=[];
	$scope.pbdStnList=[];
	
	var max = 15;
	 
	 
	    $('#regContUnLoad').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContLoad').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContTransitDay').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContStatisticalCharge').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContAdditionalRate').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContFromWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContToWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContInsureUnitNo').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContInsurePolicyNo').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContValue').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmFromKm').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmToKm').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayP').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayP').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtP').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayB').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayB').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtB').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayD').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayD').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtD').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtLoadLimit').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtGuaranteeWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    $('#ctsRate').keypress(function(key) {
    		if(key.charCode < 46 || key.charCode > 57)
    			return false;
    	});
    	
    	$('#ctsFromWt').keypress(function(key) {
    		if(key.charCode < 46 || key.charCode > 57)
    			return false;
    	});
    	
    	$('#ctsToWt').keypress(function(key) {
    		if(key.charCode < 46 || key.charCode > 57)
    			return false;
    	});
    	
    	$('#ctsRateQ').keypress(function(key) {
    		if(key.charCode < 46 || key.charCode > 57)
    			return false;
    	});
    	
    	$('#ctsFromWtQ').keypress(function(key) {
    		if(key.charCode < 46 || key.charCode > 57)
    			return false;
    	});
    	
    	$('#ctsToWtQ').keypress(function(key) {
    		if(key.charCode < 46 || key.charCode > 57)
    			return false;
    	});
	    
	   
	    
        $('#pbdAmtD').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            }
        });
        
        $('#ctsFromWt').keypress(function(e) {
    		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
    			e.preventDefault();
    		}
    	});
    	
    	$('#ctsToWt').keypress(function(e) {
    		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
    			e.preventDefault();
    		}
    	});
    
    	$('#ctsRate').keypress(function(e) {
    		if (this.value.length == max) {
    			e.preventDefault();
    		} 
    	});
    	
    	$('#ctsFromWtQ').keypress(function(e) {
    		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
    			e.preventDefault();
    		}
    	});
    	
    	$('#ctsToWtQ').keypress(function(e) {
    		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
    			e.preventDefault();
    		}
    	});
    
    	$('#ctsRateQ').keypress(function(e) {
    		if (this.value.length == max) {
    			e.preventDefault();
    		} 
    	});
        
        $('#pbdToDayD').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#ctsRate').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#pbdFromDayD').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#pbdAmtB').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            }
        });
        
        $('#pbdToDayB').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        
        $('#pbdFromDayB').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#pbdAmtP').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#pbdFromDayP').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            }
        });
        
        $('#pbdToDayP').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#rbkmFromKm').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#rbkmToKm').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#rbkmRate').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });

        $('#regContRate').keypress(function(e) { 
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContFromWt').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContToWt').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            }
        });
        
        $('#regContTransitDay').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            }
        });
        
        $('#regContStatisticalCharge').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContLoad').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContUnLoad').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
	    
        $('#regContAdditionalRate').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContValue').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContInsureUnitNo').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#regContInsuredBy').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#regContInsureComp').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#regContInsurePolicyNo').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#regContWt').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#vtLoadLimit').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#vtGuaranteeWt').keypress(function(e) {
        	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });

        $('#vtVehicleType').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#vtServiceType').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
	    
        $('#vtCode').keypress(function(e) {
            if (this.value.length == 1) {
                e.preventDefault();
            } 
            
        });
        
        $('#stnPin').keypress(function(key) {
	 	       if(key.charCode < 48 || key.charCode > 57)
	 	           return false;
	 	   });

	        $('#stnPin').keypress(function(e) {
	 	       if (this.value.length == 6) {
	 	           e.preventDefault();
	 	       }
	 	   });
	 	 
	        $('#stnName').keypress(function(e) {
	 	       if (this.value.length == 40) {
	 	           e.preventDefault();
	 	       }
	        });
	 	 
	        $('#district').keypress(function(e) {
	 	       if (this.value.length == 40) {
	 	           e.preventDefault();
	 	       }
	        });
	
	$scope.saveTncUnload = function(){
		if($scope.checkUnLoad){
			$scope.checkUnLoadFlag=false;
			$scope.tnc.tncUnLoading="yes";
		}
		if(!($scope.checkUnLoad)){
			$scope.cts.ctsToUnLoad="";
			$scope.checkUnLoadFlag=true;
			$scope.tnc.tncUnLoading="no";
		}	
	}
	
	
	$scope.saveTncUnloadRbkm = function(){
		if($scope.checkUnLoadRbkm){
			$scope.checkUnLoadFlagRbkm=false;
			$scope.tnc.tncUnLoading="yes";
		}
		if(!($scope.checkUnLoadRbkm)){
			$scope.rbkm.rbkmUnLoad = "";
			$scope.checkUnLoadFlagRbkm=true;
			$scope.tnc.tncUnLoading="no";
		}	
	}

	$scope.saveTncPenalty = function(){
		if($scope.checkPenalty){
			$scope.checkPenaltyFlag=false;
			$scope.tnc.tncPenalty="yes";
		}
		if(!($scope.checkPenalty)){
			$scope.checkPenaltyFlag=true;
			$scope.tnc.tncPenalty="no";
		}	
	}

	$scope.saveTncLoad= function(){
		console.log("enter into saveTncLoad --- >"+$scope.checkLoad);
		if($scope.checkLoad){
			$scope.checkLoadFlag=false;
			$scope.tnc.tncLoading="yes";
		}
		if(!($scope.checkLoad)){
			$scope.cts.ctsToLoad="";
			
			$scope.checkLoadFlag=true;
			$scope.tnc.tncLoading="no";
		}	
	}
	
	
	$scope.saveTncLoadRbkm = function(){
		if($scope.checkLoadRbkm){
			$scope.checkLoadFlagRbkm=false;
			$scope.tnc.tncLoading="yes";
		}
		if(!($scope.checkLoadRbkm)){
			$scope.rbkm.rbkmLoad = "";
			
			$scope.checkLoadFlagRbkm=true;
			$scope.tnc.tncLoading="no";
		}	
	}


	$scope.saveTncBonus = function(){
		if($scope.checkBonus){
			$scope.checkBonusFlag=false;
			$scope.tnc.tncBonus="yes";
		}
		if(!($scope.checkBonus)){
			$scope.checkBonusFlag=true;
			$scope.tnc.tncBonus="no";
		}	
	}

	$scope.saveTncDetention = function(){
		if($scope.checkDetention){
			$scope.checkDetentionFlag=false;
			$scope.tnc.tncDetention="yes";
		}
		if(!($scope.checkDetention)){
			$scope.checkDetentionFlag=true;
			$scope.tnc.tncDetention="no";
		}	
	}

	$scope.saveTncTransitDay = function(){
		if($scope.checkTransitDay){
			$scope.checkTransitFlag=false;
			$scope.tnc.tncTransitDay="yes";
		}
		if(!($scope.checkTransitDay)){
			$scope.cts.ctsToTransitDay="";
			$scope.checkTransitFlag=true;
			$scope.tnc.tncTransitDay="no";
		}	
	}	
	
	$scope.saveTncTransitDayRbkm = function(){
		if($scope.checkTransitDayRbkm){
			$scope.checkTransitFlagRbkm=false;
			$scope.tnc.tncTransitDay="yes";
		}
		if(!($scope.checkTransitDayRbkm)){
			$scope.rbkm.rbkmTransitDay = "";
			$scope.checkTransitFlagRbkm=true;
			$scope.tnc.tncTransitDay="no";
		}	
	}

	$scope.saveTncStatisticalCharge = function(){
		if($scope.checkStatisticalCharge){
			$scope.checkStatisticalChargeFlag=false;
			$scope.tnc.tncStatisticalCharge="yes";
		}
		if(!($scope.checkStatisticalCharge)){
			$scope.cts.ctsToStatChg="";
			$scope.checkStatisticalChargeFlag=true;
			$scope.tnc.tncStatisticalCharge="no";
		}	
	}
	
	
	$scope.saveTncStatisticalChargeRbkm = function(){
		if($scope.checkStatisticalChargeRbkm){
			$scope.checkStatisticalChargeFlagRbkm=false;
			$scope.tnc.tncStatisticalCharge="yes";
		}
		if(!($scope.checkStatisticalChargeRbkm)){
			$scope.rbkm.rbkmStatChg = "";
			$scope.checkStatisticalChargeFlagRbkm=true;
			$scope.tnc.tncStatisticalCharge="no";
		}	
	}
	
	$scope.setTransitFlagTrue = function(cts){
		if(cts.ctsToTransitDay ===null || cts.ctsToTransitDay==="" || angular.isUndefined(cts.ctsToTransitDay)){
			$scope.checkTransitFlag=false;
		}else{
			$scope.checkTransitFlag=true;
		}	
	}
	
	$scope.setTransitFlagTrueRbkm = function(rbkm){
		if(rbkm.rbkmTransitDay ===null || rbkm.rbkmTransitDay==="" || angular.isUndefined(rbkm.rbkmTransitDay)){
			$scope.checkTransitFlagRbkm=false;
		}else{
			$scope.checkTransitFlagRbkm=true;
		}	
	}
	
	$scope.setSCFlagTrue = function(cts){
		if(cts.ctsToStatChg ===null || cts.ctsToStatChg==="" || angular.isUndefined(cts.ctsToStatChg)){
			$scope.checkStatisticalChargeFlag=false;
		}else{
			$scope.checkStatisticalChargeFlag=true;
		}
	}
	
	$scope.setSCFlagTrueRbkm = function(rbkm){
		if(rbkm.rbkmStatChg ===null || rbkm.rbkmStatChg==="" || angular.isUndefined(rbkm.rbkmStatChg)){
			$scope.checkStatisticalChargeFlagRbkm=false;
		}else{
			$scope.checkStatisticalChargeFlagRbkm=true;
		}
	}
	
	$scope.setLoadFlagTrue = function(cts){
		if(cts.ctsToLoad ===null || cts.ctsToLoad==="" || angular.isUndefined(cts.ctsToLoad)){
			$scope.checkLoadFlag=false;	
		}else{
			$scope.checkLoadFlag=true;
		}
	}
	
	$scope.setLoadFlagTrueRbkm = function(rbkm){
		if(rbkm.rbkmLoad ===null || rbkm.rbkmLoad==="" || angular.isUndefined(rbkm.rbkmLoad)){
			$scope.checkLoadFlagRbkm=false;	
		}else{
			$scope.checkLoadFlagRbkm=true;
		}
	}
	
	$scope.setUnLoadFlagTrue = function(cts){
		if(cts.ctsToUnLoad ===null || cts.ctsToUnLoad==="" || angular.isUndefined(cts.ctsToUnLoad)){
			$scope.checkUnLoadFlag=false;	
		}else{
			$scope.checkUnLoadFlag=true;
		}
	}
	
	$scope.setUnLoadFlagTrueRbkm = function(rbkm){
		if(rbkm.rbkmUnLoad ===null || rbkm.rbkmUnLoad==="" || angular.isUndefined(rbkm.rbkmUnLoad)){
			$scope.checkUnLoadFlagRbkm=false;	
		}else{
			$scope.checkUnLoadFlagRbkm=true;
		}
	}
	
	$scope.setFflagTrue = function(regCnt){
		console.log(regCnt.regContAdditionalRate);
		if(regCnt.regContAdditionalRate===null || regCnt.regContAdditionalRate==="" || angular.isUndefined(regCnt.regContAdditionalRate)){
			$scope.Fflag=false;	
		}else{
			$scope.Fflag=true;
		}
	}
	
	$scope.OpenBranchCodeDB = function(){
		$scope.branchCodeFlag = false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui){ 
			     $(this).dialog('destroy');
			     $(this).hide();
			    }
			});
	
		$('div#branchCodeDB').dialog('open');
		}
	
	$scope.OpenregContBLPMCodeDB = function(){
		$scope.blpmCodeFlag = false;
		$('div#regContBLPMCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "BLPM Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});

		$('div#regContBLPMCodeDB').dialog('open');
		}

	$scope.OpenregContCngrCodeDB=function(){
		$scope.CngrCodeFlag = false;
		$('div#regContCngrCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Consignor Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContCngrCodeDB').dialog('open');
		}

	$scope.OpenregContCrNameDB = function(){
		$scope.CrNameFlag = false;
		$('div#regContCrNameDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Representative",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
	$('div#regContCrNameDB').dialog('open');
	}

	$scope.OpenregContFromStationDB = function(){
		$scope.FromStationFlag = false;
		$('div#regContFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContFromStationDB').dialog('open');
		}

	$scope.OpenregContToStationDB = function(){
		$scope.ToStationFlag = false;
		$('div#regContToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContToStationDB').dialog('open');
	}

	$scope.openCtsVehicleType = function(){
		$scope.VehicleTypeFlag = false;
		$('div#ctsVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#ctsVehicleTypeDB').dialog('open');		
	}

	$scope.openProductTypeDB = function(){
		console.log("open product type DB");
		$scope.ProductTypeFlag = false;
		$('div#productTypeDB').dialog({
		autoOpen: false,
		modal:true,
		resizable: false,
		position: UDPos,
		show: UDShow,
		hide: UDHide,
		title: "Product Type",
		draggable: true,
		close: function(event, ui) { 
	        $(this).dialog('destroy');
	        $(this).hide();
	    }
		});
		$('div#productTypeDB').dialog('open');
	}


	$scope.OpenregContRbkmIdDB = function(regCnt){
		if(regCnt.regContFromStationTemp==="" || regCnt.regContFromStationTemp===null || angular.isUndefined(regCnt.regContFromStationTemp) || 
			regCnt.regContToStationTemp==="" || regCnt.regContToStationTemp===null || angular.isUndefined(regCnt.regContToStationTemp)){
			$scope.alertToast("Please enter From Station & To Station");
		}else{
			$scope.RbkmIdFlag = false;
			
			$('div#regContRbkmIdDB').dialog({
			autoOpen: false,
			modal:true,
			title: "RBKM ID",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContRbkmIdDB').dialog('open');	
		$scope.rbkm.rbkmToStationTemp=$scope.regCnt.regContToStationTemp;
		$scope.rbkm.rbkmToStation=$scope.regCnt.regContToStation;
		}
}

	$scope.OpenProductTypeDB1 = function(){
		$scope.AddProductTypeFlag = false;
		$('div#AddProductTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Product Type",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#AddProductTypeDB').dialog('open');
		}
	
	$scope.OpenAddVehicleTypeDB = function(){
		$scope.VehicleTypeFlag1 = false;
		$('div#addVehicleTypeDB1').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#addVehicleTypeDB1').dialog('open');
		}
	
	$scope.OpenrbkmFromStationDB = function(){
		$scope.rbkmFromStationFlag = false;
		$('div#rbkmFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});

			$('div#rbkmFromStationDB').dialog('open');
		}
	
	$scope.OpenrbkmToStationDB = function(){
		$scope.rbkmToStationFlag = false;
		$('div#rbkmToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			$('div#rbkmToStationDB').dialog('open');
		}
	
	$scope.OpenRbkmVehicleTypeDB = function(){
		$scope.rbkmVehicleTypeFlag = false;
		$('div#rbkmVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenPenaltyDB = function(){
		$scope.PenaltyFlag = false;
		$scope.pbdP.pbdPenBonDet = "P";
		$('div#PenaltyDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Penalty",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#PenaltyDB').dialog('open');
	}
	
	$scope.OpenBonusDB = function(){
		$scope.BonusFlag = false;
		$scope.pbdB.pbdPenBonDet = "B";
		
		$('div#BonusDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Bonus",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
			
		$('div#BonusDB').dialog('open');
		}
	
	$scope.OpenDetentionDB = function(){
		$scope.DetentionFlag = false;
		$scope.pbdD.pbdPenBonDet = "D";
		
		$('div#DetentionDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Detention",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#DetentionDB').dialog('open');
	}

	$scope.OpenPenaltyFromStationDB = function(){
	$scope.PenaltyFromStationFlag = false;
		$('div#PenaltyFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyFromStationDB').dialog('open');
		}
		
	
	$scope.OpenPenaltyToStationDB = function(){
		
		$scope.PenaltyToStationFlag = false;
		$('div#PenaltyToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyToStationDB').dialog('open');
		}
	
	$scope.OpenPenaltyVehicleTypeDB = function(){
		$scope.PenaltyVehicleTypeFlag = false;
		$('div#PenaltyVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenBonusFromStationDB = function(){
		$scope.BonusFromStationFlag = false;
		$('div#BonusFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusFromStationDB').dialog('open');
		}
		
	
	$scope.OpenBonusToStationDB = function(){
		$scope.BonusToStationFlag = false;
		$('div#BonusToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusToStationDB').dialog('open');
		}
	
	$scope.OpenBonusVehicleTypeDB = function(){
		$scope.BonusVehicleTypeFlag = false;
		$('div#BonusVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenDetentionFromStationDB = function(){
		$scope.DetentionFromStationFlag = false;
		$('div#DetentionFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionFromStationDB').dialog('open');
		}
		
	
	$scope.OpenDetentionToStationDB = function(){
		$scope.DetentionToStationFlag = false;
		$('div#DetentionToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionToStationDB').dialog('open');
		}
	
	$scope.OpenDetentionVehicleTypeDB = function(){
		$scope.DetentionVehicleTypeFlag = false;
		$('div#DetentionVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenrbkmStateCodeDB = function(){
		$scope.rbkmStateCodeFlag = false;
		$('div#rbkmStateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "State Code",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmStateCodeDB').dialog('open');
		}
	
	$scope.contToStnDB = function(regCnt){
		console.log("enter into contToStnDB function");
		if(regCnt.regContFromStationTemp===null || regCnt.regContFromStationTemp==="" || angular.isUndefined(regCnt.regContFromStationTemp))
		{
			$scope.alertToast("Please Enter From Station");
		}else if(regCnt.regContType==='W'){
			$scope.contToStationFlagW = false;
			$scope.cts.ctsProductType="-100";
			if($scope.regCnt.regContProportionate === "F"){
				$('#ctsAdditionalRate').removeAttr("disabled");
			}
			$('div#contToStationW').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
				
				$('div#contToStationW').dialog('open');
		}else if(regCnt.regContType==='Q'){
			$scope.contToStationFlagQ = false;
			$scope.cts.ctsProductType="";
			$('div#contToStationQ').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
				
				$('div#contToStationQ').dialog('open');
		}
	}
	
	$scope.openToStationsDB = function(){	
		$scope.ToStationsDBFlag = false;
		$('div#ToStationsDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Stations",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#ToStationsDB').dialog('open');
		}
	
	$scope.openAddNewStnDB = function(){	
		$scope.addStationFlag = false;
		$('div#addStation').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#addStation').dialog('open');
		}
	
	$scope.openStateCodeDB = function(){	
		$scope.StateCodeFlag = false;
		$('div#StateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State Code",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			$('div#StateCodeDB').dialog('open');
		}
	
	/*$scope.openAddVTForRbkm = function(){	
		$scope.RbkmAddVTFlag = false;
		$('div#addVTForRbkmDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			$('div#addVTForRbkmDB').dialog('open');
		}*/
	
	$scope.setStartDate=function(startDate){
		console.log("Inside setStartDate");
		$scope.pbdP.pbdStartDt=startDate;
		$scope.pbdB.pbdStartDt=startDate;
		$scope.pbdD.pbdStartDt=startDate;
	}
	
	$scope.setEndDate=function(endDate){
		console.log("Inside setEndDate");
		$scope.pbdP.pbdEndDt=endDate;
		$scope.pbdB.pbdEndDt=endDate;
		$scope.pbdD.pbdEndDt=endDate;	
	}
	
	$scope.savBLPMCode = function(blpmCode){
		$scope.regCnt.regContBLPMCode = blpmCode.custCode;
		$scope.regContBLPMCodeTemp = blpmCode.custFaCode;
		$scope.regCnt.regContCrNameTemp="";
		$('div#regContBLPMCodeDB').dialog("destroy");
		$scope.blpmCodeFlag = true;	
		
		 var response = $http.post($scope.projectName+'/getCRDataForRegCont', blpmCode.custCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.crList = data.list;
			   if(!angular.isUndefined($scope.metricType)){
				   $scope.sendMetricType($scope.metricType);
			   }
			   $('#regContCrNameTemp').removeAttr("disabled");
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	}
	
	$scope.savCngrCode = function(cngrCode){
		$scope.regCnt.regContCngrCode = cngrCode.custCode;
		$scope.regContCngrCodeTemp = cngrCode.custFaCode;
		$('div#regContCngrCodeDB').dialog("destroy");
		$scope.CngrCodeFlag = true;
	}
	
	$scope.savBranchCode = function(branch){
		$scope.regCnt.branchCode = branch.branchCode;
		console.log("---<<>>"+$scope.regCnt.branchCode);
		$('div#branchCodeDB').dialog('destroy');
		$scope.branchCodeFlag = true;
	}

	$scope.savFrmStnCode = function(Fromstation){
		$scope.regCnt.regContFromStationTemp = Fromstation.stnName;
		$scope.regCnt.regContFromStation = Fromstation.stnCode;
		
		$scope.rbkm.rbkmFromStationTemp=Fromstation.stnName;
		$scope.rbkm.rbkmFromStation=Fromstation.stnCode;
		
		$scope.pbdP.pbdFromStnTemp=Fromstation.stnName;
		$scope.pbdP.pbdFromStn=Fromstation.stnCode;
		
		$scope.pbdB.pbdFromStnTemp=Fromstation.stnName;
		$scope.pbdB.pbdFromStn=Fromstation.stnCode;
		
		$scope.pbdD.pbdFromStnTemp=Fromstation.stnName;
		$scope.pbdD.pbdFromStn=Fromstation.stnCode;
		
		$('div#regContFromStationDB').dialog("destroy");
		$scope.FromStationFlag = true;
		
	}
	
	$scope.savToStnCode = function(Tostation){
		$scope.regCnt.regContToStationTemp = Tostation.stnName;
		$scope.regCnt.regContToStation =Tostation.stnCode;
		
		/*$scope.rbkm.rbkmToStationTemp=Tostation.stnName;
		$scope.rbkm.rbkmToStation=Tostation.stnCode;*/
		$('div#regContToStationDB').dialog("destroy");
		$scope.ToStationFlag = true;
		$scope.toStnFlag=true;
	}
	
	$scope.savCrName = function(cr){
		$scope.regCnt.regContCrNameTemp =cr.custRepName;
		$scope.regCnt.regContCrName = cr.custRepCode;
		$('div#regContCrNameDB').dialog("destroy");
		$scope.CrNameFlag = true;
	}
	
	$scope.savVehicleType = function(VT){
		$scope.cts.ctsVehicleType = VT.vtCode;
		$scope.pbdP.pbdVehicleType = VT.vtCode;
		$scope.pbdB.pbdVehicleType = VT.vtCode;
		$scope.pbdD.pbdVehicleType = VT.vtCode;
		$('div#ctsVehicleTypeDB').dialog("destroy");
		$scope.VehicleTypeFlag = true;
	}
	
	$scope.savProductName = function(value){
		//$scope.Qflag =true;
		$scope.cts.ctsProductType = value;
		$('div#productTypeDB').dialog("destroy");
		$scope.ProductTypeFlag = true;
	}
	
	$scope.savRbkmFromStnCode = function(rbkmFromstation){
		$scope.rbkm.rbkmFromStationTemp = rbkmFromstation.stnName;
		$scope.rbkm.rbkmFromStation =rbkmFromstation.stnCode;;
		$('div#rbkmFromStationDB').dialog("destroy");
		$scope.rbkmFromStationFlag = true;
	}
	
	$scope.saveRbkmToStnCode = function(rbkmTostation){
		$scope.rbkm.rbkmToStationTemp = rbkmTostation.stnName;
		$scope.rbkm.rbkmToStation =rbkmTostation.stnCode;;
		$('div#rbkmToStationDB').dialog("destroy");
		$scope.rbkmToStationFlag = true;
	}
	
	$scope.savRbkmVehicleType = function(vType){
		$scope.rbkm.rbkmVehicleType = vType.vtCode;
		$scope.pbdP.pbdVehicleType = vType.vtCode;
		$scope.pbdB.pbdVehicleType = vType.vtCode;
		$scope.pbdD.pbdVehicleType = vType.vtCode;
		$('div#rbkmVehicleTypeDB').dialog("destroy");
		$scope.rbkmVehicleTypeFlag = true;
	}
	
	$scope.saveStateCode = function(state){
		console.log("enter into saveStateCode function");
		
		$scope.rbkm.rbkmStateCodeTemp=state.stateName;
		$scope.rbkm.rbkmStateCode=state.stateCode;
	
		/*$scope.pbdP.pbdToStnTemp = state.stnName;
		$scope.pbdP.pbdToStn = state.stnCode;
		$scope.pbdB.pbdToStnTemp = state.stnName;
		$scope.pbdB.pbdToStn = state.stnCode;
		$scope.pbdD.pbdToStnTemp = state.stnName;
		$scope.pbdD.pbdToStn = state.stnCode;*/
		
		$('div#rbkmStateCodeDB').dialog("destroy");
		$scope.rbkmStateCodeFlag = true;
	}
	
	$scope.savPenaltyFromStn = function(PenaltyFromstation){
		$scope.pbdP.pbdFromStnTemp = PenaltyFromstation.stnName;
		$scope.pbdP.pbdFromStn =PenaltyFromstation.stnCode;;
		$('div#PenaltyFromStationDB').dialog("destroy");
		$scope.PenaltyFromStationFlag = true;
	}
	
	$scope.savPenaltyToStn = function(PenaltyTostation){
		$scope.pbdP.pbdToStnTemp = PenaltyTostation.stnName;
		$scope.pbdP.pbdToStn =PenaltyTostation.stnCode;;
		$('div#PenaltyToStationDB').dialog("destroy");
		$scope.PenaltyToStationFlag = true;
	}
	
	$scope.savPenaltyVehicleType = function(VtPenalty){
		$scope.pbdP.pbdVehicleType=VtPenalty.vtCode;
		$('div#PenaltyVehicleTypeDB').dialog("destroy");
		$scope.PenaltyVehicleTypeFlag = true;
	}
	
	
	$scope.savBonusFromStn = function(BonusFromstation){
		$scope.pbdB.pbdFromStnTemp = BonusFromstation.stnName;
		$scope.pbdB.pbdFromStn =BonusFromstation.stnCode;;
		$('div#BonusFromStationDB').dialog("destroy");
		$scope.BonusFromStationFlag = true;
	}
	
	$scope.savBonusToStn = function(BonusTostation){
		$scope.pbdB.pbdToStnTemp = BonusTostation.stnName;
		$scope.pbdB.pbdToStn =BonusTostation.stnCode;;
		$('div#BonusToStationDB').dialog("destroy");
		$scope.BonusToStationFlag = true;
	}
	
	$scope.savBonusVehicleType = function(vtBonus){
		$scope.pbdB.pbdVehicleType=vtBonus.vtCode;
		$('div#BonusVehicleTypeDB').dialog("destroy");
		$scope.BonusVehicleTypeFlag = true;
	}
	
	$scope.savDetentionFromStn = function(DetentionFromstation){
		$scope.pbdD.pbdFromStnTemp = DetentionFromstation.stnName;
		$scope.pbdD.pbdFromStn =DetentionFromstation.stnCode;;
		$('div#DetentionFromStationDB').dialog("destroy");
		$scope.DetentionFromStationFlag = true;
	}
	
	$scope.savDetentionToStn = function(DetentionTostation){
		$scope.pbdD.pbdToStnTemp = DetentionTostation.stnName;
		$scope.pbdD.pbdToStn =DetentionTostation.stnCode;;
		$('div#DetentionToStationDB').dialog("destroy");
		$scope.DetentionToStationFlag = true;
	}
	
	$scope.savDetentionVehicleType = function(vtDetention){
		$scope.pbdD.pbdVehicleType=vtDetention.vtCode;
		$('div#DetentionVehicleTypeDB').dialog("destroy");
		$scope.DetentionVehicleTypeFlag = true;
	}
	
	$scope.saveToStations = function(toStations){
		console.log(toStations.stnName);
		$scope.cts.ctsToStnTemp=toStations.stnName;
		$scope.cts.ctsToStn=toStations.stnCode;
		$scope.pbdP.pbdToStnTemp = toStations.stnName;
		$scope.pbdP.pbdToStn = toStations.stnCode;
		$scope.pbdB.pbdToStnTemp = toStations.stnName;
		$scope.pbdB.pbdToStn = toStations.stnCode;
		$scope.pbdD.pbdToStnTemp = toStations.stnName;
		$scope.pbdD.pbdToStn = toStations.stnCode;
		$('div#ToStationsDB').dialog("destroy");
		$scope.ToStationsDBFlag = true;
	}	
	
	$scope.savStateCode = function(statecode){
		$scope.station.stateCode=statecode.stateCode;
		$('div#StateCodeDB').dialog("destroy");
		$scope.StateCodeFlag = true;
	}
	
	$scope.clickW = function(metricType,regContProportionate){
		console.log(regContProportionate);
		if(metricType==="" || angular.isUndefined(metricType)){
			$scope.alertToast("please Enter metric type ");
			$('input[name=regContType]').attr('checked',false);
		}else if(regContProportionate==="" || angular.isUndefined(regContProportionate)) {
			$scope.alertToast("please Select Proportionate");
			$('input[name=regContType]').attr('checked',false);
		}else{
			//$scope.Qflag=false;
			$scope.Wflag=false;
			$scope.Kflag=true;
			$scope.ContToStnFlag=false;
			$scope.toStnFlag=true;
			$scope.regCnt.regContFromStationTemp="";
			$scope.regCnt.regContFromStation="";
			$scope.regCnt.regContToStationTemp="";
			$scope.regCnt.regContToStation="";
			
			$scope.rbkm.rbkmFromStationTemp="";
			$scope.rbkm.rbkmToStationTemp="";
			$scope.rbkm.rbkmStartDate="";
			$scope.rbkm.rbkmStateCodeTemp="";
			$scope.rbkm.rbkmEndDate="";
			$scope.rbkm.rbkmFromKm="";
			$scope.rbkm.rbkmToKm="";
			$scope.rbkm.rbkmVehicleType="";
			$scope.rbkm.rbkmRate="";
			
			$scope.removeAllCTS();
			 $('#regContProductType').removeAttr("disabled");
			 $('#openProductTypeDB').removeAttr("disabled");
			 $('#regContRbkmId').attr("disabled","disabled");
			 //$('#openProductTypeDB').attr("disabled","disabled");
			
			 $scope.regCnt.regContProductType="";
			 $scope.removeAllRbkm();
			 $scope.onWQClick=true;
			 $scope.onKClick=false;	
		}
	}
	
	$scope.clickQ = function(metricType,regContProportionate){
		console.log(metricType);
		if(metricType==="" || angular.isUndefined(metricType)){
			$scope.alertToast("please Enter metric type ");
			$('input[name=regContType]').attr('checked',false);
		}else if(regContProportionate==="" || angular.isUndefined(regContProportionate)){
			console.log(regContProportionate);
			$scope.alertToast("please Select Proportionate--");	
			$('input[name=regContType]').attr('checked',false);
		}else if(regContProportionate==='P'){
			//$scope.Qflag=false;
			$scope.Wflag=true;
			$scope.Kflag=true;
			$scope.ContToStnFlag=false;
			$scope.toStnFlag=true;
			
			$scope.regCnt.regContFromStationTemp="";
			$scope.regCnt.regContFromStation="";
			$scope.regCnt.regContToStationTemp="";
			$scope.regCnt.regContToStation="";
			
			$scope.removeAllCTS();
			
			$scope.rbkm.rbkmFromStationTemp="";
			$scope.rbkm.rbkmToStationTemp="";
			$scope.rbkm.rbkmStartDate="";
			$scope.rbkm.rbkmStateCodeTemp="";
			$scope.rbkm.rbkmEndDate="";
			$scope.rbkm.rbkmFromKm="";
			$scope.rbkm.rbkmToKm="";
			$scope.rbkm.rbkmVehicleType="";
			$scope.rbkm.rbkmRate="";
			$('#regContProductType').removeAttr("disabled");
			 $('#regContRbkmId').attr("disabled","disabled");
			 $('#openProductTypeDB').removeAttr("disabled");
			 $scope.removeAllRbkm();
			 $scope.onWQClick=true;
			 $scope.onKClick=false;
		}else{
			$scope.alertToast("Select Proportionate");
			$('input[name=regContType]').attr('checked',false);
		}
	}

	$scope.clickK = function(metricType,regContProportionate){
		console.log(metricType);
		if(metricType==="" || angular.isUndefined(metricType)){
			$scope.alertToast("please Enter metric type ");
			$('input[name=regContType]').attr('checked',false);
		}else if(regContProportionate==="" || angular.isUndefined(regContProportionate)){
			console.log(regContProportionate);
			$scope.alertToast("please Select Proportionate--");	
			$('input[name=regContType]').attr('checked',false);
		}else if(regContProportionate==='P'){
			//$scope.Qflag=true;
			$scope.Wflag=true;
			$scope.Kflag=false;
			$scope.toStnFlag=false;
			$scope.ContToStnFlag=true;
			$scope.regCnt.regContFromStationTemp="";
			$scope.regCnt.regContFromStation="";
			//$scope.ctsLengthFlag=true;
			
			$('#regContRbkmId').removeAttr("disabled");
			 $('#regContProductType').attr("disabled","disabled");
			 $('#openProductTypeDB').attr("disabled","disabled");
			 $scope.regCnt.regContProductType="";
			 $scope.onWQClick=false;
			 $scope.onKClick=true;	
			 $scope.removeAllCTS();
		}else{
			$scope.alertToast("Select Proportionate");
			$('input[name=regContType]').attr('checked',false);
		}
	}
	
	$scope.clickF = function(){
		$scope.Fflag=false;
		$('#regContAdditionalRate').removeAttr("disabled");
	}
	
	$scope.clickP = function(){
		$scope.Fflag=true;
		$scope.regCnt.regContAdditionalRate="";
		$('#regContAdditionalRate').attr("disabled","disabled");
	}
	
	$scope.setToStnFlag=function(regCnt){
		console.log("setToStnFlag");
		if(regCnt.regContToStationTemp==="" || regCnt.regContToStationTemp===null || angular.isUndefined(regCnt.regContToStationTemp)){
			$scope.toStnFlag=false;
			console.log($scope.toStnFlag);
		}else{
			$scope.toStnFlag=true;
			console.log($scope.toStnFlag);
		}
	}
	
	
	$scope.submit = function(RegularContractForm,regCnt,regContRatePer,tnc,rbkm,pbdP,pbdD,pbdB,cts){
		console.log("Enter into Regular contract controller js --->");
		
		if(RegularContractForm.$invalid){
			console.log("Enter into RegularContractForm.$invalid--->"+RegularContractForm.$invalid);
			if(RegularContractForm.regContBLPMCode.$invalid){
				$scope.alertToast("please Enter BLPM Code");
			}else if(RegularContractForm.regContCrNameTemp.$invalid){
				$scope.alertToast("please Enter CR Name");
			}/*else if(RegularContractForm.regContVehicleType.$invalid){
				$scope.alertToast("please Enter Vehicle type");
			}*/else if(RegularContractForm.regContFromStationTemp.$invalid){
				$scope.alertToast("please Enter From Station Name");
			}/*else if(RegularContractForm.regContFromWt.$invalid){
				$scope.alertToast("please Enter correct From Weight ");
			}else if(RegularContractForm.regContToWt.$invalid){
				$scope.alertToast("please Enter correct To Weight");
			}*/else if(RegularContractForm.regContWt.$invalid){
				$scope.alertToast("please Enter correct Weight");
			}else if(RegularContractForm.regContValue.$invalid){
				$scope.alertToast("please Enter value");
			}else if(RegularContractForm.regContDc.$invalid){
				$scope.alertToast("please Enter DC");
			}else if(RegularContractForm.regContCostGrade.$invalid){
				$scope.alertToast("please Enter Cost Grade");
			}else if(RegularContractForm.regContDdl.$invalid){
				$scope.alertToast("please Enter DDL");
			}else if(RegularContractForm.regContRenew.$invalid){
				$scope.alertToast("please Enter Renew");
			}else if(RegularContractForm.metricType.$invalid){
				$scope.alertToast("please Enter Metric Type");
			}/*else if(RegularContractForm.fromWt.$invalid){
				$scope.alertToast("please Enter Ton/Kg in From Weight");
			}else if(RegularContractForm.toWt.$invalid){
				$scope.alertToast("please Enter Ton/Kg in To Weight");
			}else if(RegularContractForm.weight.$invalid){
				$scope.alertToast("please Enter Ton/Kg inWeight");
			}*/
		}else{ 
			/*if($scope.Qflag===false){
				console.log($scope.Qflag);
				if(regCnt.regContProductType===null || angular.isUndefined(regCnt.regContProductType) || regCnt.regContProductType==="")
					{
					$scope.alertToast("please fill Product Type");
					}
			}else */if ($('input[name=regContType]:checked').length === 0 ){
				$scope.alertToast("Please select contract Type");	
			}
			else if($scope.Kflag===false){
				if(rbkm.rbkmFromStationTemp ===null || angular.isUndefined(rbkm.rbkmFromStationTemp) || rbkm.rbkmFromStationTemp==="" || rbkm.rbkmToStationTemp===null || angular.isUndefined(rbkm.rbkmToStationTemp) || rbkm.rbkmToStationTemp==="" || rbkm.rbkmStartDate===null || angular.isUndefined(rbkm.rbkmStartDate) || rbkm.rbkmStartDate===""
			|| rbkm.rbkmEndDate===null || angular.isUndefined(rbkm.rbkmEndDate) || rbkm.rbkmEndDate==="" || rbkm.rbkmStateCodeTemp===null || angular.isUndefined(rbkm.rbkmStateCodeTemp) || rbkm.rbkmStateCodeTemp==="" || rbkm.rbkmFromKm===null || angular.isUndefined(rbkm.rbkmFromKm) || rbkm.rbkmFromKm===""
			|| rbkm.rbkmToKm ===null || angular.isUndefined(rbkm.rbkmToKm) || rbkm.rbkmToKm==="" || rbkm.rbkmVehicleType ===null || angular.isUndefined(rbkm.rbkmVehicleType) || rbkm.rbkmVehicleType==="" || rbkm.rbkmRate===null || angular.isUndefined(rbkm.rbkmRate) || rbkm.rbkmRate==="")	
			{
				$scope.alertToast("Please fill RBKM");
			}
			}/*else if($scope.Fflag===false){
				if(regCnt.regContAdditionalRate===null || regCnt.regContAdditionalRate==="" || angular.isUndefined(regCnt.regContAdditionalRate)){
					$scope.alertToast("please fill Product Additional Rate");	
				}
			}*/
			else if($scope.checkTransitFlag === false){
				if(regCnt.regContTransitDay ===null || regCnt.regContTransitDay==="" || angular.isUndefined(regCnt.regContTransitDay))
				{
					$scope.alertToast("Please fill Transit Day");
				}
			}else if($scope.checkStatisticalChargeFlag ===false){
				if(regCnt.regContStatisticalCharge ===null || regCnt.regContStatisticalCharge==="" || angular.isUndefined(regCnt.regContStatisticalCharge))
				{
					$scope.alertToast("Please fill StatisticalCharge");
				}
			}else if($scope.checkLoadFlag === false){
				if(regCnt.regContLoad ===null || regCnt.regContLoad==="" || angular.isUndefined(regCnt.regContLoad))
				{
					$scope.alertToast("Please fill Load");
				}
			}else if($scope.checkUnLoadFlag ===false){
				if(regCnt.regContUnLoad ===null || regCnt.regContUnLoad==="" || angular.isUndefined(regCnt.regContUnLoad))
				{
					$scope.alertToast("Please fill UnLoad");
				}
			}else if($scope.checkPenaltyFlag === false){
				if(pbdP.pbdFromStnTemp === null || pbdP.pbdFromStnTemp === "" || angular.isUndefined(pbdP.pbdFromStnTemp) || pbdP.pbdToStnTemp === null || pbdP.pbdToStnTemp === "" || angular.isUndefined(pbdP.pbdToStnTemp) || pbdP.pbdStartDt === null || pbdP.pbdStartDt === "" || angular.isUndefined(pbdP.pbdStartDt) ||
						pbdP.pbdEndDt===null || pbdP.pbdEndDt==="" ||angular.isUndefined(pbdP.pbdEndDt) || pbdP.pbdFromDay===null || pbdP.pbdFromDay==="" || angular.isUndefined(pbdP.pbdFromDay) || pbdP.pbdToDay===null || pbdP.pbdToDay==="" || angular.isUndefined(pbdP.pbdToDay) || pbdP.pbdVehicleType===null || pbdP.pbdVehicleType==="" || angular.isUndefined(pbdP.pbdVehicleType) ||
						pbdP.pbdHourNDay===null || pbdP.pbdHourNDay==="" || angular.isUndefined(pbdP.pbdHourNDay) || pbdP.pbdAmt===null || pbdP.pbdAmt==="" || angular.isUndefined(pbdP.pbdAmt))
						{
							$scope.alertToast("Please fill Penalty");
						}
					}
				else if($scope.checkBonusFlag === false){
						if(pbdB.pbdFromStnTemp===null || pbdB.pbdFromStnTemp==="" || angular.isUndefined(pbdB.pbdFromStnTemp) || pbdB.pbdToStnTemp===null || pbdB.pbdToStnTemp==="" || angular.isUndefined(pbdB.pbdToStnTemp) || pbdB.pbdStartDt===null || pbdB.pbdStartDt==="" || angular.isUndefined(pbdB.pbdStartDt)
						|| pbdB.pbdEndDt===null || pbdB.pbdEndDt==="" ||angular.isUndefined(pbdB.pbdEndDt) || pbdB.pbdFromDay===null || pbdB.pbdFromDay==="" || angular.isUndefined(pbdB.pbdFromDay) || pbdB.pbdToDay===null || pbdB.pbdToDay==="" || angular.isUndefined(pbdB.pbdToDay) || pbdB.pbdVehicleType===null || pbdB.pbdVehicleType==="" || angular.isUndefined(pbdB.pbdVehicleType)
						|| pbdB.pbdHourNDay===null || pbdB.pbdHourNDay==="" || angular.isUndefined(pbdB.pbdHourNDay) || pbdB.pbdAmt===null || pbdB.pbdAmt==="" || angular.isUndefined(pbdB.pbdAmt))
						{
							$scope.alertToast("Please fill Bonus");
						}
					}
					else if($scope.checkDetentionFlag === false){
						if(pbdD.pbdFromStnTemp===null || pbdD.pbdFromStnTemp==="" || angular.isUndefined(pbdD.pbdFromStnTemp) || pbdD.pbdToStnTemp===null || pbdD.pbdToStnTemp==="" || angular.isUndefined(pbdD.pbdToStnTemp) || pbdD.pbdStartDt===null || pbdD.pbdStartDt==="" || angular.isUndefined(pbdD.pbdStartDt)
						|| pbdD.pbdEndDt===null || pbdD.pbdEndDt==="" ||angular.isUndefined(pbdD.pbdEndDt) || pbdD.pbdFromDay===null || pbdD.pbdFromDay==="" || angular.isUndefined(pbdD.pbdFromDay) || pbdD.pbdToDay===null || pbdD.pbdToDay==="" || angular.isUndefined(pbdD.pbdToDay) || pbdD.pbdVehicleType===null || pbdD.pbdVehicleType==="" || angular.isUndefined(pbdD.pbdVehicleType)
						|| pbdD.pbdHourNDay===null || pbdD.pbdHourNDay==="" || angular.isUndefined(pbdD) || pbdD.pbdAmt===null || pbdD.pbdAmt==="" || angular.isUndefined(pbdD.pbdAmt))
						{
							$scope.alertToast("Please fill Detention");
						}
					}else if($scope.toStnFlag===false){
						console.log($scope.toStnFlag);
						if(regCnt.regContToStationTemp==="" || regCnt.regContToStationTemp===null || angular.isUndefined(regCnt.regContToStationTemp)){
							$scope.alertToast("Please fill To Station");
						}
					}else if($scope.ContToStnFlag===false){
						console.log($scope.ContToStnFlag);
						if(cts.ctsToStn==="" || cts.ctsToStn===null || angular.isUndefined(cts.ctsToStn) || cts.ctsRate==="" || cts.ctsRate===null || angular.isUndefined(cts.ctsRate) || cts.ctsVehicleType=="" || cts.ctsVehicleType==null || angular.isUndefined(cts.ctsVehicleType) || cts.ctsProductType===null || cts.ctsProductType==="" || angular.isUndefined(cts.ctsProductType) ){
							$scope.alertToast("Please fill To Stations");
						}
					}else if($scope.regCnt.regContFromDt>$scope.regCnt.regContToDt){
				$scope.alertToast("From date cannot be greater than to date");
			}else if($scope.regCnt.regContFromWt>$scope.regCnt.regContToWt){
				$scope.alertToast("From weight cannot be greater than to weight");
			}else if ( $('input[name=regContProportionate]:checked').length === 0 ){
				$scope.alertToast("Please select contract Proportionate/Fixed");	
			}/*else if($scope.ctsLengthFlag===false){
				console.log($scope.ctsList.length);
				if(!$scope.ctsList.length){
					$scope.alertToast("Please enter to stations");	
				}
			}*/else if($scope.ctsList.length <= 0){
				$scope.alertToast("Please fill atleast one station rate");
			}
			else{
				$scope.regContFlag = false;
		    	$('div#regContDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#regContDB').dialog('open');
			}	
			}	
	}
	
	$scope.back= function(){
		$scope.regContFlag = true;
		$('div#regContDB').dialog('close');
	}
	
	$scope.saveContract = function(regCnt,tnc,regContRatePer,metricType){
		console.log("Enter into save contract function--"+metricType);
		$('div#regContDB').dialog('close');
		
		if(metricType==="Ton"){
			regCnt.regContWt=(regCnt.regContWt*$scope.kgInTon);
			regCnt.regContWt=regCnt.regContWt.toFixed(5);
			console.log(regCnt.regContWt);
			regCnt.regContAdditionalRate=(regCnt.regContAdditionalRate/$scope.kgInTon);
			regCnt.regContAdditionalRate=regCnt.regContAdditionalRate.toFixed(5);
			console.log(regCnt.regContAdditionalRate);
		}
		/*if(regContRatePer === "Per Ton"){
			regCnt.regContRate =(regCnt.regContRate/907.185);
			console.log(regCnt.regContRate);
		}
		
		if(fromWtPer === "Ton"){
			regCnt.regContFromWt=(regCnt.regContFromWt*907.185);
			console.log(regCnt.regContFromWt);
		}
		
		if(toWtPer === "Ton"){
			regCnt.regContToWt=(regCnt.regContToWt*907.185);
			console.log(regCnt.regContToWt);
		}
		
		if(weight === "Ton"){
			regCnt.regContWt=(regCnt.regContWt*907.185);
			console.log(regCnt.regContWt);
		}
		
		console.log(regCnt.regContFromWt);
		console.log(regCnt.regContToWt);
		console.log(regCnt.regContWt);*/
		
		
		$('#saveBtnId').attr("disabled","disabled");
		
		
			var response = $http.post($scope.projectName+'/saveRegularContract',regCnt);
			response.success(function(data, status, headers, config) {
				
				$('#saveBtnId').removeAttr("disabled");
				
				console.log("Message from controller is "+data.result);
				$scope.afterSaveContCode = data.regContFaCode;
				
				var response = $http.post($scope.projectName+'/saveTncForRegCont', tnc);
				response.success(function(data, status, headers, config) {
					$scope.successToast(data.resultTnc);
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
				
				$scope.afterSaveDBFlag=false;
				$('div#afterSaveDB').dialog({
					autoOpen: false,
					modal:true,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});

				$('div#afterSaveDB').dialog('open');
				
				$scope.successToast(data.result);
				$scope.successToast(data.resultRbkm);
				$scope.successToast(data.resultPbd);
				$scope.successToast(data.resultCts);
				/*$("form").trigger("reset");*/
				$scope.metricType="";
				$scope.regCnt.regContBLPMCode="";
				$scope.regCnt.branchCode="";
				$scope.regCnt.regContCngrCode="";
				$scope.regCnt.regContCrNameTemp="";
				$scope.regCnt.regContFromStation="";
				$scope.regCnt.regContToStation="";
				$scope.regCnt.regContFromStationTemp="";
				$scope.regCnt.regContToStationTemp="";
				$scope.regCnt.regContRate="";
				$scope.regCnt.regContVehicleType="";
				$scope.regCnt.regContFromDt="";
				$scope.regCnt.regContToDt="";
				$scope.regCnt.regContType="";
				$scope.regCnt.regContFromWt="";
				$scope.regCnt.regContToWt="";
				$scope.regCnt.regContProductType="";
				$scope.regCnt.regContTransitDay="";
				$scope.regCnt.regContStatisticalCharge="";
				$scope.regCnt.regContLoad="";
				$scope.regCnt.regContUnLoad="";
				$scope.regCnt.regContProportionate="";
				$scope.regCnt.regContAdditionalRate="";
				$scope.regCnt.regContValue="";
				$scope.regCnt.regContDc="";
				$scope.regCnt.regContCostGrade="";
				$scope.regCnt.regContWt="";
				$scope.regCnt.regContDdl="";
				$scope.regCnt.regContRenew="";
				$scope.regCnt.regContRenewDt="";
				$scope.regCnt.regContInsuredBy="";
				$scope.regCnt.regContInsureComp="";
				$scope.regCnt.regContInsureUnitNo="";
				$scope.regCnt.regContInsurePolicyNo="";
				$scope.regCnt.regContRemark="";
				
				$scope.fromWtPer="";
				$scope.toWtPer="";
				$scope.weight="";
				$scope.regContRatePer="";
				$scope.checkLoad=false;
				$scope.checkUnLoad=false;
				$scope.checkTransitDay=false;
				$scope.checkStatisticalCharge=false;
				$scope.regCnt.regContType=false;
				$scope.checkDetention=false;
				$scope.checkPenalty=false;
				$scope.checkBonus=false;
				$('input[name=regContProportionate]').attr('checked',false);
				$('#regContAdditionalRate').attr("disabled","disabled");
				$scope.onWQClick=false;
				$scope.onKClick=false;
				$scope.toStnFlag=true;
				$('#regContCrNameTemp').attr("disabled","disabled");
				$('#openProductTypeDB').attr("disabled","disabled");
				$scope.fetch();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	}
	
	
	
	$scope.afterSaveClose = function(){
		$('div#afterSaveDB').dialog('close');
		$scope.afterSaveContCode = "";
	}
	
	
	 	$scope.saveproducttype = function(ProductTypeDB1Form,pt){
	 		$scope.Qflag=true;
	 		$('div#AddProductTypeDB').dialog("destroy");
		 	//	$scope.ProductTypeFlag1=true;
	 		$scope.AddProductTypeFlag=true;
	 		$scope.ProductTypeFlag=true;
	 		pt.ptName=pt.ptName.toUpperCase();
	 		if($.inArray(pt.ptName,$scope.ptList)!==-1){
	 			$scope.alertToast("Name already exists");
	 			$scope.pt.ptName="";
	 		}else{
				var response = $http.post($scope.projectName+'/saveproducttypeforRegCont',pt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
					$scope.successToast(data.result);
					$scope.getProductName();
					$scope.pt.ptName="";
					}else{
						console.log(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}	
		}
	
	 $scope.savevehicletype = function(VehicleForm,vt){
		 $scope.VehicleTypeFlag1=true;
			console.log("enter into saveVehicleType function--->");
			$('div#addVehicleTypeDB1').dialog("destroy");
			vt.vtServiceType=vt.vtServiceType.toUpperCase();
			vt.vtVehicleType=vt.vtVehicleType.toUpperCase();
			$scope.code = vt.vtServiceType+vt.vtVehicleType;
			if($scope.vtList.length>0){
				for(var i=0;i<$scope.vtList.length;i++){
					$scope.type[i] = $scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType;
					$scope.type.push($scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType);
				}
				
				if(VehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(VehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(VehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(VehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleTypeForRegCont',vt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getVehicleTypeCode();
						$scope.vt="";	
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}else{
				if(VehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(VehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(VehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(VehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}/*else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}*/else {
				var response = $http.post($scope.projectName+'/saveVehicleTypeForRegCont',vt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getVehicleTypeCode();
						$scope.vt="";	
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}			
}
	 
		$scope.getCustomerData = function(){
		   console.log("getCustomerData------>");
		   var response = $http.post($scope.projectName+'/getCustomerDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.customerList = data.list;
			   $scope.getStationData();
			  // $scope.getCustomerRepresentativeData();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
		
	$scope.getBranchData = function(){
	   console.log("getBranchData------>");
	   var response = $http.post($scope.projectName+'/getBranchDataForRegCont');
	   response.success(function(data, status, headers, config){
		   if(data.result==="success"){
		   $scope.branchList = data.list;
		   $scope.getCustomerData();
		   }else{
			   console.log(data.result);
		   }
	   });
	   response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getStationData = function(){
		   console.log("getStationData------>");
		   var response = $http.post($scope.projectName+'/getStationDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.stationList = data.list;
			   $scope.getVehicleTypeCode();
			   for(var i=0;i<$scope.stationList.length;i++){
					$scope.type[i] = $scope.stationList[i].stnName+$scope.stationList[i].stnPin;
					$scope.type.push($scope.stationList[i].stnName+$scope.stationList[i].stnPin);
			   }
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	}
		 
		   
		
	   $scope.getVehicleTypeCode = function(){
		   console.log("getVehicleTypeCode------>");
		   var response = $http.post($scope.projectName+'/getVehicleTypeCodeForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.vtList = data.list;
			   $scope.getProductName();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	   $scope.getProductName = function(){
		   console.log("getProductName------>");
		   var response = $http.post($scope.projectName+'/getProductNameForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.ptList = data.list;
			   $scope.getStateData();
			   }else{
				   console.log(data);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	   
	   $scope.Submit = function(regContCode){
			console.log("enter into regular contract submit function--->");
			$scope.show = "false";
			var response = $http.post($scope.projectName+'/regularcontractdetails',regContCode);
			response.success(function(data, status, headers, config) {
				$scope.regularContract = data.regularContract;	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	   
	   $scope.EditRegularContract = function(regContCode){
			console.log("enter into EditRegularContract function--->");
			var response = $http.post($scope.projectName+'/EditRegularContract',regContCode);
			response.success(function(data, status, headers, config) {
				$scope.successToast(data.result);
				$scope.regularContract = data.regularContract;
				$scope.show = "false";
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	   
	   $scope.EditRegularContractSubmit = function(regularContract){
		   $scope.regContCode="";
			console.log("enter into EditRegularContractSubmit function--->"+regularContract.branchCode);
			var response = $http.post($scope.projectName+'/EditRegularContractSubmit',regularContract);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	   
	   $scope.getRegContIsVerify = function(){
		   var response = $http.post($scope.projectName+'/verifyRegularContract');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				   console.log("---------->list==>"+data.list);
				   $scope.regularContracts = data.list;
			   }else{
				   console.log(data);
			   }   
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
		   
	   $scope.getStateData = function(){
		   var response = $http.post($scope.projectName+'/getStateDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.listState = data.list;
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	   
		$scope.saveRbkmId = function(RbkmForm,rbkm){
			$('div#regContRbkmIdDB').dialog("destroy");
			$scope.RbkmIdFlag = true;
			//$scope.Kflag=true;
			
			if($scope.rbkmList.length>0){
				if(RbkmForm.rbkmFromStationTemp.$invalid){
					$scope.alertToast("please Enter From station");
				}else if(RbkmForm.rbkmToStationTemp.$invalid){
					$scope.alertToast("please Enter To station");
				}else if(RbkmForm.rbkmStateCodeTemp.$invalid){
					$scope.alertToast("please Enter State code");
				}else if(RbkmForm.rbkmToKm.$invalid){
					$scope.alertToast("to km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmFromKm.$invalid){
					$scope.alertToast("from km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmVehicleType.$invalid){
					$scope.alertToast("please Enter Vehicle Type");
				}else if(RbkmForm.rbkmRate.$invalid){
					$scope.alertToast("please Enter correct rbkm rate Type");
				}else if($scope.rbkm.rbkmStartDate>$scope.rbkm.rbkmEndDate){
					$scope.alertToast("Start date cannot be greater than end date");
				}else if($scope.rbkm.rbkmFromKm>$scope.rbkm.rbkmToKm){
					$scope.alertToast("from km cannot be greater than to km");
				}else{
					rbkm.rbkmStateCode=rbkm.rbkmStateCode.toUpperCase();
					rbkm.rbkmVehicleType=rbkm.rbkmVehicleType.toUpperCase();
					$scope.rbkmTemp=rbkm.rbkmStateCode+rbkm.rbkmVehicleType;
					
					for(var i=0;i<$scope.rbkmList.length;i++){
						$scope.type[i] = $scope.rbkmList[i].rbkmStateCode+$scope.rbkmList[i].rbkmVehicleType;
						$scope.type.push($scope.rbkmList[i].rbkmStateCode+$scope.rbkmList[i].rbkmVehicleType);
					}
					if($.inArray($scope.rbkmTemp,$scope.type)!==-1){
						$scope.alertToast("Name already exists");
						/*$scope.rbkm.rbkmFromStationTemp="";
						$scope.rbkm.rbkmToStationTemp="";
						$scope.rbkm.rbkmStateCodeTemp="";
						$scope.rbkm.rbkmStartDate="";
						$scope.rbkm.rbkmEndDate="";
						$scope.rbkm.rbkmFromKm="";
						$scope.rbkm.rbkmToKm="";
						$scope.rbkm.rbkmVehicleType="";
						$scope.rbkm.rbkmRate="";*/
					} else{
						var temp = {
								"ToStn" : 	rbkm.rbkmToStationTemp,
								//"ToStnTemp" :rbkm.rbkmToStation,
								"FromStn" : rbkm.rbkmFromStationTemp,
								//"FromStnTemp" :rbkm.rbkmFromStation,
								"vehicleType" :rbkm.rbkmVehicleType,
								"stateCode" :rbkm.rbkmStateCode
							};
						$scope.rbkmStnList.push(temp);
						console.log("length of $scope.contToStnTemp = "+$scope.rbkmStnList.length);
						var response = $http.post($scope.projectName+'/addRbkmForRegCont', rbkm);
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								
								console.log("from server ---->"+data.result);
								$scope.fetchRbkmList();
								//$scope.rbkm.rbkmFromStationTemp="";
						/*		$scope.rbkm.rbkmToStationTemp="";
								$scope.rbkm.rbkmStateCodeTemp="";
								$scope.rbkm.rbkmStartDate="";
								$scope.rbkm.rbkmEndDate="";
								$scope.rbkm.rbkmFromKm="";
								$scope.rbkm.rbkmToKm="";
								$scope.rbkm.rbkmVehicleType="";
								$scope.rbkm.rbkmRate="";*/
								$scope.checkLoadRbkm = false;
								$scope.checkUnLoadRbkm = false;
								$scope.checkTransitDayRbkm = false;
								$scope.checkStatisticalChargeRbkm = false;
					
								$('input[name=rbkmFStationName]').attr('checked',false);
								$('input[name=rbkmTStationName]').attr('checked',false);
								$('input[name="state"]').attr('checked',false);
								$('input[name=rbkmVTypeName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});
					}	
				}
			}else{
				console.log("M here");
				if(RbkmForm.rbkmFromStationTemp.$invalid){
					$scope.alertToast("please Enter From station");
				}else if(RbkmForm.rbkmToStationTemp.$invalid){
					$scope.alertToast("please Enter To station");
				}else if(RbkmForm.rbkmStateCodeTemp.$invalid){
					$scope.alertToast("please Enter State code");
				}else if(RbkmForm.rbkmToKm.$invalid){
					$scope.alertToast("to km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmFromKm.$invalid){
					$scope.alertToast("from km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmVehicleType.$invalid){
					$scope.alertToast("please Enter Vehicle Type");
				}else if($scope.rbkm.rbkmStartDate>$scope.rbkm.rbkmEndDate){
   					$scope.alertToast("Start date cannot be greater than end date");
   				}else if($scope.rbkm.rbkmFromKm>$scope.rbkm.rbkmToKm){
   					$scope.alertToast("from km cannot be greater than to km");
   				}else{
   					rbkm.rbkmStateCode=rbkm.rbkmStateCode.toUpperCase();
					rbkm.rbkmVehicleType=rbkm.rbkmVehicleType.toUpperCase();
					
					var temp = {
							"ToStn" : 	rbkm.rbkmToStationTemp,
							//"ToStnTemp" :rbkm.rbkmToStation,
							"FromStn" : rbkm.rbkmFromStationTemp,
							//"FromStnTemp" :rbkm.rbkmFromStation,
							"vehicleType" :rbkm.rbkmVehicleType,
							"stateCode" :rbkm.rbkmStateCode
						};
					$scope.rbkmStnList.push(temp);
					console.log("length of $scope.contToStnTemp = "+$scope.rbkmStnList.length);
   					var response = $http.post($scope.projectName+'/addRbkmForRegCont', rbkm);
   					response.success(function(data, status, headers, config) {
   							if(data.result==="success"){
   								$scope.fetchRbkmList();
   								//$scope.rbkm.rbkmFromStationTemp="";
   								/*$scope.rbkm.rbkmToStationTemp="";
   								$scope.rbkm.rbkmStartDate="";
   								$scope.rbkm.rbkmStateCodeTemp="";
   								$scope.rbkm.rbkmEndDate="";
   								$scope.rbkm.rbkmFromKm="";
   								$scope.rbkm.rbkmToKm="";
   								$scope.rbkm.rbkmVehicleType="";
   								$scope.rbkm.rbkmRate="";*/
   								
   								$('input[name=rbkmFromstationName]').attr('checked',false);
   								$('input[name=rbkmTostationName]').attr('checked',false);
   								$('input[name="stateName"]').attr('checked',false);
   								$('input[name=rbkmVTypeName]').attr('checked',false);			
   							}else{
   								console.log(data);
   							}
   						});
   						response.error(function(data, status, headers, config) {
   							$scope.errorToast(data.result);
   						});
   					}
   				}	
			}	
	   
		$scope.fetchRbkmList = function(){
			console.log("enter into fetchRbkmList function");
			 var response = $http.get($scope.projectName+'/fetchRbkmListForRegCont');
			 response.success(function(data, status, headers, config){
				 if(data.result === "success"){
					// $scope.removeAllPbd();
					 $scope.rbkmList = data.list; 
					 $scope.rbkmFlag=true;
					 $scope.Kflag=true;
				 }else{
					 console.log(data);
					// $scope.removeAllPbd();
					 $scope.rbkmList = data.list; 
					 $scope.rbkmFlag=false;
					 $('#regContRbkmId').attr("disabled","disabled");
					 console.log($scope.regCnt.regContType);
					 if($scope.regCnt.regContType==="K"){
						 $scope.regCnt.regContType=false;
						 $scope.Kflag=false;
					 }
				 }
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
		
		$scope.removeRbkm = function(rbkm) {
			console.log("enter into removeRbkm function");
			var response = $http.post($scope.projectName+'/removeRbkmForRegCont',rbkm);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					console.log(rbkm.rbkmStateCode);
					console.log(rbkm.rbkmVehicleType);
					console.log($scope.rbkmStnList[0].stateCode);
					console.log($scope.rbkmStnList[0].vehicleType);
					for(var i=0;i<$scope.rbkmStnList.length;i++){
						if(($scope.rbkmStnList[i].stateCode === rbkm.rbkmStateCode) && ($scope.rbkmStnList[i].vehicleType === rbkm.rbkmVehicleType)){
							$scope.rbkmStnList.splice(i,1);
							i--;
						}
					}
					console.log($scope.rbkmStnList.length);
					$scope.fetchRbkmList();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
		
		$scope.removeAllRbkm = function() {
			var response = $http.post($scope.projectName+'/removeAllRbkmForRegCont');
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					if($scope.rbkmStnList.length > 0){
						for(var i=0;i<$scope.rbkmStnList.length;i++){
							$scope.rbkmStnList.splice(i,1);
							i--;
						}
					}
					console.log($scope.rbkmStnList.length);
					$scope.fetchRbkmList();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }

	    
	    $scope.savePbdForPenalty = function(PenaltyForm,pbdP){
			$('div#PenaltyDB').dialog("destroy");
			$scope.PenaltyFlag=true;
			//$scope.checkPenaltyFlag=true;
			if(PenaltyForm.$invalid){
				
				if(PenaltyForm.pbdFromStnTempP.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(PenaltyForm.pbdToStnTempP.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(PenaltyForm.pbdVehicleTypeP.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(PenaltyForm.pbdToDayP.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(PenaltyForm.pbdFromDayP.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(PenaltyForm.pbdHourNDayP.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(PenaltyForm.pbdAmtP.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
			}else{
				if($scope.pbdP.pbdFromDay>$scope.pbdP.pbdToDay){
					$scope.alertToast("From day cannot be greater than to day");
			     }else if($scope.pbdP.pbdStartDt>$scope.pbdP.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
				     }else {
				    	 
				    	 var temp = {
				    			 "ToStn" : 	pbdP.pbdToStnTemp,
									"ToStnTemp" : 	pbdP.pbdToStn,
									"FromStn" : pbdP.pbdFromStnTemp,
									"FromStnTemp" : pbdP.pbdFromStn,
										"pbd"	  :pbdP.pbdPenBonDet
								};
							$scope.pbdStnList.push(temp);
							console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
				    	 
					var response = $http.post($scope.projectName+'/addPbdForRegCont', pbdP);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.checkPenaltyFlag=true;
							console.log("from server--<<<"+data.result);
							$scope.fetchPbdList();
							//$scope.pbdP.pbdFromStnTemp = "";
							//$scope.pbdP.pbdToStnTemp = "";
							//$scope.pbdP.pbdStartDt = "";
							//$scope.pbdP.pbdEndDt = "";
							$scope.pbdP.pbdFromDay = "";
							$scope.pbdP.pbdToDay = "";
							$scope.pbdP.pbdVehicleType= "";
							$scope.pbdP.pbdHourNDay = "";
							$scope.pbdP.pbdAmt = "";
							
							$('input[name=penaltyFromstationName]').attr('checked',false);
							$('input[name=penaltyTostationName]').attr('checked',false);
							$('input[name="VtPenaltyName"]').attr('checked',false);
							
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
			
		$scope.savePbdForBonus = function(BonusForm,pbdB){
			$('div#BonusDB').dialog("destroy");
			console.log(pbdB.pbdFromDay);
			console.log(pbdB.pbdToDay);
			$scope.BonusFlag=true;
			//$scope.checkBonusFlag=true;
			if(BonusForm.$invalid){
				if(BonusForm.pbdFromStnTempB.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(BonusForm.pbdToStnTempB.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(BonusForm.pbdVehicleTypeB.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(BonusForm.pbdToDayB.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(BonusForm.pbdFromDayB.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(BonusForm.pbdHourNDayB.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(BonusForm.pbdAmtB.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
			}else{
				console.log(pbdB.pbdFromDay+" "+pbdB.pbdToDay);
				if($scope.pbdB.pbdFromDay > $scope.pbdB.pbdToDay){
					$scope.alertToast("From day cant be greater than to day");
			     }else if(pbdB.pbdStartDt>pbdB.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
			     }else{
			    	 
			    	 var temp = {
			    			 "ToStn" : 	pbdB.pbdToStnTemp,
								"ToStnTemp" : 	pbdB.pbdToStn,
								"FromStn" : pbdB.pbdFromStnTemp,
								"FromStnTemp" : pbdB.pbdFromStn,
									"pbd"	  :pbdB.pbdPenBonDet
							};
						$scope.pbdStnList.push(temp);
						console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
					var response = $http.post($scope.projectName+'/addPbdForRegCont', pbdB);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
						$scope.checkBonusFlag=true;
						$scope.fetchPbdList();
						//$scope.pbdB.pbdFromStnTemp = "";
						//$scope.pbdB.pbdToStnTemp = "";
						//$scope.pbdB.pbdStartDt = "";
						//$scope.pbdB.pbdEndDt = "";
						$scope.pbdB.pbdFromDay = "";
						$scope.pbdB.pbdToDay = "";
						//$scope.pbdB.pbdVehicleType = "";
						$scope.pbdB.pbdHourNDay = "";
						$scope.pbdB.pbdAmt = "";
						
						$('input[name=bonusFromstationName]').attr('checked',false);
						$('input[name=bonusTostationName]').attr('checked',false);
						$('input[name="vtBonusName"]').attr('checked',false);
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
		
		$scope.savePbdForDetention = function(DetentionForm,pbdD){
			$('div#DetentionDB').dialog("destroy");
			$scope.DetentionFlag=true;
			//$scope.checkDetentionFlag=true;
			console.log("enter into savePbd function--->");
			if(DetentionForm.$invalid){
				
				if(DetentionForm.pbdFromStnTempD.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(DetentionForm.pbdToStnTempD.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(DetentionForm.pbdVehicleTypeD.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(DetentionForm.pbdToDayD.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(DetentionForm.pbdFromDayD.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(DetentionForm.pbdHourNDayD.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(DetentionForm.pbdAmtD.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
				}else{
				 if($scope.pbdD.pbdFromDay>$scope.pbdD.pbdToDay){
					$scope.alertToast("From day cannot be greater than to day");
			     }else if($scope.pbdD.pbdStartDt>$scope.pbdD.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
				     }else{
				    	 
				    	 var temp = {
				    			    "ToStn" : 	pbdD.pbdToStnTemp,
									"ToStnTemp" : 	pbdD.pbdToStn,
									"FromStn" : pbdD.pbdFromStnTemp,
									"FromStnTemp" : pbdD.pbdFromStn,
									"pbd"	  :pbdD.pbdPenBonDet
								};
							$scope.pbdStnList.push(temp);
							console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
					var response = $http.post($scope.projectName+'/addPbdForRegCont', pbdD);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
						$scope.checkDetentionFlag=true;
						$scope.fetchPbdList();
						//$scope.pbdD.pbdFromStnTemp= "";
						//$scope.pbdD.pbdToStnTemp = "";
						//$scope.pbdD.pbdStartDt = "";
						//$scope.pbdD.pbdEndDt = "";
						$scope.pbdD.pbdFromDlay = "";
						$scope.pbdD.pbdToDay = "";
						$scope.pbdD.pbdVehicleType = "";
						$scope.pbdD.pbdHourNDay = "";
						$scope.pbdD.pbdAmt = "";
						
						$('input[name=detentionFromstationName]').attr('checked',false);
						$('input[name=detentionTostationName]').attr('checked',false);
						$('input[name="vtDetentionName"]').attr('checked',false);
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}
		}
		
		$scope.fetchPbdList = function(){
			console.log("enter into fetchPbdList function");
			 var response = $http.get($scope.projectName+'/fetchPbdListForRegCont');
			 response.success(function(data, status, headers, config){
				if(data.result==="success"){
					
				 	$scope.pbdList = data.list;
				 	//$scope.getBranchData();
				 	$scope.pbdFlag=true;
				 	$scope.checkPenaltyFlag=true;
				}else{
					console.log(data);
					//$scope.getBranchData();
					$scope.checkPenalty=false;
			 		$scope.checkBonus=false;
			 		$scope.checkDetention=false;
			 		$scope.pbdFlag=false;
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
		$scope.removePbd = function(pbd) {
			console.log("enter into removePbd function");
			var response = $http.post($scope.projectName+'/removePbdForRegCont',pbd);
			response.success(function(data, status, headers, config){
				for(var i=0;i<$scope.pbdStnList.length;i++){
					if(($scope.pbdStnList[i].ToStnTemp === pbd.pbdToStn) && ($scope.pbdStnList[i].FromStnTemp === pbd.pbdFromStn) && ($scope.pbdStnList[i].pbd === pbd.pbdPenBonDet)){
						$scope.pbdStnList.splice(i,1);
						i--;
					}
				}
				console.log($scope.pbdStnList.length);
				$scope.fetchPbdList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
		
		$scope.removeAllPbd = function() {
			var response = $http.post($scope.projectName+'/removeAllPbdForRegCont');
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					if($scope.pbdStnList.length > 0){
						for(var i=0;i<$scope.pbdStnList.length;i++){
							$scope.pbdStnList.splice(i,1);
							i--;
						}
					}
					console.log($scope.pbdStnList.length);
					$scope.fetchPbdList();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    };
	   
	   $scope.toggleSelection = function toggleSelection(regContCode) {
		   var idx = $scope.selection.indexOf(regContCode);
		   
		   // is currently selected
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
		   }
		   // is newly selected
		   else {
			   $scope.selection.push(regContCode);
		   }
	   }
	   
	   $scope.update = function(){
			 console.log("enter into update with $scope.selection = "+$scope.selection)
			 var response = $http.post($scope.projectName+'/updateIsVerifyRegularContract',$scope.selection.toString());
			 response.success(function(data, status, headers, config){
				   console.log("---------->message==>"+data.result);
		    });
			 response.error(function(data, status, headers, config) {
	 				$scope.errorToast(data.result);
	 			});
	   }
	   
	   
	   $scope.saveContToStnW = function(ContToStationFormW,cts,regCnt,metricType){
			console.log("Enter into saveContToStnW function"+ContToStationFormW.$invalid);
			$('div#contToStationW').dialog("destroy");
			$scope.contToStationFlagW = true;
			$scope.ctsflagW=true;
			$scope.regContFrmStn=regCnt.regContFromStationTemp;
			
			/*if(metricType==="Ton"){
				cts.ctsAdditionalRate=(cts.ctsAdditionalRate/907.185);
				console.log(cts.ctsAdditionalRate);
			}*/
			
			if($scope.ctsList.length>0){
				console.log("if list exists"+$scope.ctsList.length);
				if(ContToStationFormW.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormW.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				} else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else {
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					$scope.ctsTemp= cts.ctsToStn+cts.ctsVehicleType;
					
					for(var i=0;i<$scope.ctsList.length;i++){
						console.log($scope.ctsList[i].ctsToStn);
						$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType;
						$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType);
					}
					if($.inArray($scope.ctsTemp,$scope.type)!==-1){
						$scope.alertToast("Already exists");
						/*cts.ctsToStn="";
						cts.ctsToStnTemp="";
						cts.ctsRate="";
						cts.ctsFromWt="";
						cts.ctsToWt="";
						cts.ctsVehicleType="";
						cts.ctsAdditionalRate="";*/
					}else{
						var temp = {
								"ToStn" : 	cts.ctsToStn,
								"ToStnTemp" : cts.ctsToStnTemp,
								"vehicleType" :cts.ctsVehicleType
							};
						$scope.contToStnTemp.push(temp);
						console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
						
						var response = $http.post($scope.projectName+'/addCTSForRC', cts);	   
						response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									console.log($scope.ContToStnFlag);
									/*cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									cts.ctsAdditionalRate="";*/
								/*	$scope.checkLoad = false;
									
									$scope.cts.ctsToLoad="";
									$scope.checkLoadFlag=true;
									$scope.tnc.tncLoading="no";
									
									$scope.checkUnLoad = false;
									
									$scope.cts.ctsToUnLoad="";
									$scope.checkUnLoadFlag=true;
									$scope.tnc.tncUnLoading="no";
									
									$scope.checkTransitDay = false;
									
									$scope.cts.ctsToTransitDay="";
									$scope.checkTransitFlag=true;
									$scope.tnc.tncTransitDay="no";
									
									$scope.checkStatisticalCharge = false;
									
									$scope.cts.ctsToStatChg="";
									$scope.checkStatisticalChargeFlag=true;
									$scope.tnc.tncStatisticalCharge="no";
									
									$scope.checkPenalty = false;
									
									$scope.checkPenaltyFlag=true;
									$scope.tnc.tncPenalty="no";
									
									$scope.checkBonus = false;
									
									$scope.checkBonusFlag=true;
									$scope.tnc.tncBonus="no";
									
									$scope.checkDetention = false;
									
									$scope.checkDetentionFlag=true;
									$scope.tnc.tncDetention="no";*/
									
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}			
					}
			}else{
				console.log("if list does not exists"+cts.ctsFromWt+""+cts.ctsToWt);
				if(ContToStationFormW.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormW.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				} else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					
					var temp = {
							"ToStn" : 	cts.ctsToStn,
							"ToStnTemp" : cts.ctsToStnTemp,
							"vehicleType" :cts.ctsVehicleType
						};
					$scope.contToStnTemp.push(temp);
					console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
					var response = $http.post($scope.projectName+'/addCTSForRC',cts);	   
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();	
								$scope.ContToStnFlag=true;
								console.log("saved In W"+$scope.ContToStnFlag);
								/*cts.ctsToStn="";
								cts.ctsToStnTemp="";
								cts.ctsRate="";
								cts.ctsFromWt="";
								cts.ctsToWt="";
								cts.ctsVehicleType="";
								cts.ctsAdditionalRate="";*/
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}	
				}
			}
		
		$scope.saveContToStnQ = function(ContToStationFormQ,cts,regCnt){
			console.log("Enter into saveContToStnQ function"+ContToStationFormQ.$invalid);
			$('div#contToStationQ').dialog("destroy");
			$scope.contToStationFlagQ = true;
			$scope.ctsflagQ=true;
			$scope.regContFrmStn=regCnt.regContFromStationTemp;
		
			if($scope.ctsList.length>0){
				console.log("if list exists"+$scope.ctsList.length);
				if(ContToStationFormQ.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormQ.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				}else if(ContToStationFormQ.ctsProductType.$invalid){
					$scope.alertToast("Please enter Product Type");
				}else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsProductType=cts.ctsProductType.toUpperCase();
					$scope.ctsTemp= cts.ctsToStn+cts.ctsProductType;
					
					for(var i=0;i<$scope.ctsList.length;i++){
						console.log($scope.ctsList[i].ctsToStn);
						$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType;
						$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType);
					}
					if($.inArray($scope.ctsTemp,$scope.type)!==-1){
						$scope.alertToast("Already exists");
						/*cts.ctsToStn="";
						cts.ctsToStnTemp="";
						cts.ctsRate="";
						cts.ctsFromWt="";
						cts.ctsToWt="";
						cts.ctsVehicleType="";*/
					}else{
						
						var temp = {
								"ToStn" : 	cts.ctsToStn,
								"ToStnTemp" : cts.ctsToStnTemp,
								"vehicleType" :cts.ctsVehicleType
							};
						$scope.contToStnTemp.push(temp);
						console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
						var response = $http.post($scope.projectName+'/addCTSForRC', cts);	   
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									/*cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									cts.ctsProductType="";*/
									$scope.checkLoad = false;
									$scope.checkUnLoad = false;
									$scope.checkTransitDay = false;
									$scioe.checkStatisticalCharge = false;
									$scope.checkPenalty = false;
									$scope.checkBonus = false;
									$scope.checkDetention = false;
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}
					} 
			}else{
				console.log("if list does not exists");
				if(ContToStationFormQ.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormQ.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				}else if(ContToStationFormQ.ctsProductType.$invalid){
					$scope.alertToast("Please enter Product Type");
				}else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsProductType=cts.ctsProductType.toUpperCase();
					
					var temp = {
							"ToStn" : 	cts.ctsToStn,
							"ToStnTemp" : cts.ctsToStnTemp,
							"vehicleType" :cts.ctsVehicleType
						};
					$scope.contToStnTemp.push(temp);
					console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
					var response = $http.post($scope.projectName+'/addCTSForRC', cts);	   
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();
								$scope.ContToStnFlag=true;
								console.log("saved In Q"+$scope.ContToStnFlag);
								/*cts.ctsToStn="";
								cts.ctsToStnTemp="";
								cts.ctsRate="";
								cts.ctsFromWt="";
								cts.ctsToWt="";
								cts.ctsVehicleType="";
								cts.ctsProductType="";*/
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}	
				}
			}
	   
	   $scope.fetchCTSList = function(){
		   console.log("Entr into fetchCTSList function");
			 var response = $http.get($scope.projectName+'/fetchCTSListForRC');
			 response.success(function(data, status, headers, config){
				 if(data.result==="success"){
					 //$scope.getBranchData();
					 $scope.ctsList = data.list;
					// $scope.ctsLengthFlag=true;
					 if($scope.regCnt.regContType==='W'){
						 $scope.ctsFlagW=true;	 
					 }else if($scope.regCnt.regContType==='Q'){
						 $scope.ctsFlagQ=true;	 
					 }
				 }else{
					 console.log(data.result);
					 //$scope.getBranchData();
					 $scope.ctsList = data.list;
					 $scope.ctsFlagW=false;
					 $scope.ctsFlagQ=false;
					 if($scope.regCnt.regContType==='W' || $scope.regCnt.regContType==='Q'){
						 console.log($scope.regCnt.regContType);
						 $scope.ContToStnFlag=false; 
					 }
					 }	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
	   $scope.removeCTS = function(cts) {
			console.log("Entr into removeCTS function");
			var response = $http.post($scope.projectName+'/removeCTSForRC',cts);
			response.success(function(data, status, headers, config) {
				console.log(cts.ctsToStn);
				for(var i=0;i<$scope.contToStnTemp.length;i++){
					if(($scope.contToStnTemp[i].ToStn === cts.ctsToStn) && ($scope.contToStnTemp[i].vehicleType === cts.ctsVehicleType)){
						$scope.contToStnTemp.splice(i,1);
						i--;
					}
				}
				console.log($scope.contToStnTemp.length);
				$scope.fetchCTSList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
		$scope.removeAllCTS = function() {
			console.log("Entr into removeAllCts function");
			var response = $http.post($scope.projectName+'/removeAllCTSForRC');
			response.success(function(data, status, headers, config) {
				if($scope.contToStnTemp.length > 0){
					for(var i=0;i<$scope.contToStnTemp.length;i++){
						$scope.contToStnTemp.splice(i,1);
						i--;
					}
				}
				console.log($scope.contToStnTemp.length);
				$scope.fetchCTSList();
				$scope.ctsflagW=false;
				$scope.ctsflagQ=false;
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
		
		$scope.sendMetricType=function(metricType){
			console.log(metricType);
			if(metricType==="" || metricType===null || angular.isUndefined(metricType)){
				$scope.alertToast("Please enter Metric type");
			}else{
				var response = $http.post($scope.projectName+'/sendMetricTypeToRC',metricType);
				response.success(function(data, status, headers, config) {
					$scope.alertToast(data.result);
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});	
			}
		}
	   
		$scope.saveNewStation=function(StationForm,station){
			console.log("Entered into Submit function");
			$scope.station.stnName = station.stnName.toUpperCase();
			$scope.codeState = station.stnName+station.stnPin;
			console.log($scope.codeState);
			if(StationForm.$invalid){
				if(StationForm.stnName.$invalid){
					$scope.alertToast("Please enter station name between 3-40 characters...");
					$scope.station.stnName="";
				}else if(StationForm.stnDistrict.$invalid){
					$scope.alertToast("Please enter station district between 3-40 characters...");
					$scope.station.stnDistrict="";
				}else if(StationForm.stateCode.$invalid){
					$scope.alertToast("Please enter state code...");
				}else if(StationForm.stnPin.$invalid){
					$scope.alertToast("Please enter station pin of 6 digits...");
					$scope.station.stnPin="";
				}
			}else{
				if($.inArray($scope.codeState,$scope.type)!==-1){
					$scope.alertToast("Station already exists...");
					$scope.station.stnName="";
					$scope.station.stnDistrict="";
					$scope.station.stateCode="";
					$scope.station.stnPin="";
				}else{
					var response = $http.post($scope.projectName+'/submitStation',station);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
							$scope.station.stnName="";
							$scope.station.stnDistrict="";
							$scope.station.stateCode="";
							$scope.station.stnPin="";
							$('input[name=stateName]').attr('checked',false);
							$scope.getStationData();
							$('div#addStation').dialog('close');
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});
				}
			}	
		}
		 
	   $scope.fetch = function(){
			$scope.removeAllRbkm();
			$scope.removeAllCTS();
			$scope.removeAllPbd();
			//$scope.removeAllCTS();
			$scope.getBranchData();
		}
	   
	   if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		   $scope.fetch();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		
	 
			
}]);