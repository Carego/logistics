<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="cancelMrForm" ng-submit=cancelMrSubmit(cancelMrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch.branchName" ng-required="true" ng-click="openBrhDB()" readonly>
		       			<label for="code">Branch</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="mrTypeName" id="mrTypeId" ng-model="mrType" ng-init="mrType = 'O'" ng-required="true">
								<option value='O'>On Account</option>
								<!-- <option value='P'>Payment Detail</option>
								<option value='D'>Direct MR</option> -->
							</select>
							<label>MR Type</label>
					</div>
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="mrId" name ="mrName" ng-model="mrNo" ng-required="true" >
		       			<label for="code">Mr No</label>	
		       		</div>
		    </div>
		    <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>	 
		</form>    
	</div>
	
	<div id ="openBrhId" ng-hide="openBrhFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
		  <tr ng-repeat="brh in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
</div>	