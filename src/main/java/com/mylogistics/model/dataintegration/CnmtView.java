package com.mylogistics.model.dataintegration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cnmt_view")
public class CnmtView {

	@Id
	@Column(name="cnmtId")
	private int cnmtId;
	
	@Column(name="cnmtCode")
	private String cnmtCode;

	public int getCnmtId() {
		return cnmtId;
	}

	public void setCnmtId(int cnmtId) {
		this.cnmtId = cnmtId;
	}

	public String getCnmtCode() {
		return cnmtCode;
	}

	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}
	
}
