package com.mylogistics.DAO;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.TempIntBrTv;
import com.mylogistics.services.IBTService;

@Repository
public interface IBTVoucherDAO {

	public Map<String,Object> saveIBTVoucher(IBTService ibtService);
	
	public Map<String,Object> clrAllIBTV(IBTService ibtService);
	
	public int clrSIBTV(TempIntBrTv tempIntBrTv);
}
