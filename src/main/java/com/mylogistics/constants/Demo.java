package com.mylogistics.constants;

public class Demo {
	int demoInt;
	String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDemoInt() {
		return demoInt;
	}

	public void setDemoInt(int demoInt) {
		this.demoInt = demoInt;
	}
	
	
}
