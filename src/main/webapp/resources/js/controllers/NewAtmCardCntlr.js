'use strict';

var app = angular.module('application');

app.controller('NewAtmCardCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	console.log("NewAtmCardCntlr Started");
	
	$scope.bankMstrList	= [];
	$scope.branchList = [];
	$scope.employeeList = [];
	
	$scope.bankMstr	= {};
	$scope.branch = {};
	$scope.employee = {};
	$scope.atmCard = {};
	
	$scope.bankDBFlag = true;
	$scope.branchDBFlag = true;
	$scope.employeeDBFlag = true;
	$scope.saveNewAtmCardDBFlag = true;

	//bank
	$scope.getBankMstr = function(){
		console.log("getBankMstr Entered");
		var response = $http.post($scope.projectName+'/viewBankMstr');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.getAllBranchesList();
				console.log("getBankMstr Success");
				$scope.bankMstrList = data.bankMstrList;
			}else {
				console.log("getBankMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankDBFlag = false;
		$('div#bankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankDBFlag = true;
			}
		});

		$('div#bankDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr, bankMstrIndex){
		console.log("bankMstr.bnkName----->"+bankMstr.bnkName);
		console.log("bankMstr.bnkFaCode----->"+bankMstr.bnkFaCode);
		console.log("bankMstrIndex----->"+bankMstrIndex);
		$scope.bankMstr = bankMstr;
		$scope.bankMstrIndex = bankMstrIndex;
		$('div#bankDB').dialog('close');
		
	}
	
	
	//Branch
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.getAllEmployeeList();
				$scope.branchList = data.branchCodeList;
			}else {
				console.log("getAllBranchesList Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getAllBranchesList Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch = branch;
		$scope.showTableFlag = false;
		$('div#branchDB').dialog('close');
	}
	
	
	//Employee
	$scope.getAllEmployeeList = function(){
		console.log("getAllEmployeeList Entered");
		var response = $http.post($scope.projectName+'/getAllEmployees');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllEmployeeList Success");
				$scope.employeeList = data.empCodeList;
			}else {
				console.log("getAllEmployeeList error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getAllEmployeeList Error: "+data);
		});
	}
	
	$scope.openEmployeeDB = function(){
		console.log("Entered into openEmployeeDB");
		$scope.employeeDBFlag = false;
		$('div#employeeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.employeeDBFlag = true;
			}
		});

		$('div#employeeDB').dialog('open');
	}
	
	$scope.saveEmployee =  function(employee){
		console.log("saveEmployee");
		$scope.employee = employee;
		$('div#employeeDB').dialog('close');
	}
	
	//submit
	$scope.saveNewAtmCard = function(newAtmCardForm){
		console.log("enter into submit function = "+newAtmCardForm.$invalid);
		if(newAtmCardForm.$invalid){
			if (newAtmCardForm.bnkName.$invalid) {
				$scope.alertToast("enter Bank");
			} else if (newAtmCardForm.branchName.$invalid) {
				$scope.alertToast("enter branch");
			} else if (newAtmCardForm.employeeName.$invalid) {
				$scope.alertToast("enter employee");
			} else if (newAtmCardForm.atmCardNoName.$invalid) {
				$scope.alertToast("enter valid Atm Card No.");
			} else if (newAtmCardForm.atmCardExpireDtName.$invalid) {
				$scope.alertToast("enter Expire Date");
			} 
		}else{
			console.log("final submission");
			$scope.saveNewAtmCardDBFlag = false;
	    	$('div#saveNewAtmCardDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Atm Card Detail",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveNewAtmCardDB').dialog('open');
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveNewAtmCardDBFlag = true;
		$('div#saveNewAtmCardDB').dialog('close');
	}
	
	
	$scope.saveAtmCard = function(){
		console.log("enter into saveAtmCard function");
		$scope.saveNewAtmCardDBFlag = true;
		$('div#saveNewAtmCardDB').dialog('close');
		
		var atmCardService = {
				"atmCardMstr"	: $scope.atmCard,
				"bankId"		: $scope.bankMstr.bnkId,
				"empId"			: $scope.employee.empId,
				"branchId"		: $scope.branch.branchId
		}
			
		var response = $http.post($scope.projectName+'/saveAtmCard', atmCardService);
		response.success(function(data, status, headers, config){
					
			if(data.result === "success"){
				$scope.alertToast(data.msg);
				//empty the resources
				$scope.bankMstr	= {};
				$scope.branch = {};
				$scope.employee = {};
				$scope.atmCard = {};
				
				$scope.bankDBFlag = true;
				$scope.branchDBFlag = true;
				$scope.employeeDBFlag = true;
				$scope.saveNewAtmCardDBFlag = true;
			}else{
				$scope.alertToast(data.msg);
			}
					
		});
		response.error(function(data, status, headers, config){
			alert(data);
		});
			
	}
	
	$('#atmCardNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#atmCardNoId').keypress(function(e) {
		if (this.value.length == 16) {
			e.preventDefault();
		}
	});
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBankMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("NewAtmCardCntlr Ended");
	
}]);