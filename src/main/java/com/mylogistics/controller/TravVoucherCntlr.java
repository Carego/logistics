package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.FaTravDivisionDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.TravDivService;
import com.mylogistics.services.TravVoucherService;
import com.mylogistics.services.TravVoucherServiceImpl;
import com.mylogistics.services.VoucherService;

@Controller
public class TravVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private FaTravDivisionDAO faTravDivisionDAO;
	
	private TravVoucherService travVoucherService = new TravVoucherServiceImpl();
	
	@RequestMapping(value = "/getVDetFrTravV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrTravV() {  
		System.out.println("Enter into getVDetFrTravV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getAllEmpFrTravV" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllEmpFrTravV() {  
		System.out.println("Enter into getAllEmpFrTravV---->");
		Map<String, Object> map = new HashMap<>();	
		List<Employee> empList = employeeDAO.getAllActiveEmployees();
		if(!empList.isEmpty()){
			map.put("list",empList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getChequeNoFrTravV" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrTravV(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNoFrTravV---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		ChequeLeaves chequeLeaves = bankMstrDAO.getAccPayChqByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getAPChqLByBnkC(bankCode,CType);
		if(chequeLeaves != null){
			System.out.println("***************** 1");
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put("list",chqList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/addTravEDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object addTravEDetails(@RequestBody TravDivService travDivService) {  
		System.out.println("Enter into getAllEmpFrTravV---->");
		Map<String, Object> map = new HashMap<>();	
		travVoucherService.addTDService(travDivService);
		return map;
	}	
	
	
	@RequestMapping(value = "/fetchTravEDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object fetchTravEDetails() {  
		System.out.println("Enter into fetchTravEDetails---->");
		Map<String, Object> map = new HashMap<>();	
		List<TravDivService> tdSerList = travVoucherService.getAllTDService();
		map.put("list",tdSerList);
		return map;
	}	
	
	
	@RequestMapping(value = "/removeTravE" , method = RequestMethod.POST)  
	public @ResponseBody Object removeTravE(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into removeTravE---->");
		Map<String, Object> map = new HashMap<>();	
		int index = (int) clientMap.get("index");
		travVoucherService.remove(index);
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitTravVoucher" , method = RequestMethod.POST)
	public @ResponseBody Object submitTravVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("enter into submitTravVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(voucherService.getVoucherType().equalsIgnoreCase("Travel")){
			 map = saveTravVoucher(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveTravVoucher(VoucherService voucherService){
		System.out.println("Enter into saveTravVoucher----->>>");
		Map<String,Object> map = new HashMap<String,Object>();
		char payBy = voucherService.getPayBy();
		System.out.println("value of payBy = "+payBy);
		if(payBy == 'C'){
			map = faTravDivisionDAO.saveTrvlVoucByCash(voucherService,travVoucherService);
			travVoucherService.deleteAllTDService();
		}else if(payBy == 'Q'){
			map = faTravDivisionDAO.saveTrvlVouchByChq(voucherService, travVoucherService);
			travVoucherService.deleteAllTDService();
		}else if(payBy == 'O'){
			map = faTravDivisionDAO.saveTrvlVouchByOnline(voucherService, travVoucherService);
			travVoucherService.deleteAllTDService();
		}else{
			map.put("vhNo",0);
			map.put("tvNo","0");
			System.out.println("invalid payment method");
		}
		
		return map;
	}

	
	//TODO Back Data
	@RequestMapping(value = "/getVDetFrTravVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrTravVBack() {  
		System.out.println("Enter into getVDetFrTravV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	
	
	
	/*public Map<String,Object> saveTravByCash(VoucherService voucherService , String carFaCode){
		System.out.println("Enter into saveTravByCash----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		
		saveTravelVoucher()
		
		
		return map;
	}	*/
}	
