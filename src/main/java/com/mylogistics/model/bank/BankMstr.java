package com.mylogistics.model.bank;

import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;

@Entity
@Table(name = "bankmstr")
public class BankMstr {

	@Id
	@Column(name="bnkId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bnkId;
	
	@Column(name="bnkFaCode")
	private String bnkFaCode;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;
	
	@Column(name="bnkAcNo")
	private String bnkAcNo;
		
	@Column(name="bnkOpenDt")
	private Date bnkOpenDt;
	
	@Column(name="bnkCloseDt")
	private Date bnkCloseDt;
	
	@Column(name="bnkLocal")
	private String bnkLocal;
	
	@Column(name="bnkBrMinChq")
	private int bnkBrMinChq;
	
	@Column(name="bnkBrMaxChq")
	private int bnkBrMaxChq;
	
	@Column(name="bnkHoMinChq")
	private int bnkHoMinChq;
	
	@Column(name="bnkHoMaxChq")
	private int bnkHoMaxChq;

	@Column(name="bnkName")
	private String bnkName;
	
	@Column(name="bnkIfscCode")
	private String bnkIfscCode;
	
	@Column(name="bnkMicrCode")
	private String bnkMicrCode;
	
	@Column(name="bnkBalanceAmt" , columnDefinition = "double default 0.0")
	private double bnkBalanceAmt;
	
	@ManyToOne
	//@JsonBackReference(value = "bnkBrh")
	@JoinColumn(name = "branchId") 
	private Branch branch;

	//One To Many with ChequeLeaves
	@JsonManagedReference(value = "bnkChqL")
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="bank_chequeleaves",
	joinColumns=@JoinColumn(name="bnkId"),
	inverseJoinColumns=@JoinColumn(name="chqLId"))
	private List<ChequeLeaves> chequeLeavesList = new ArrayList<ChequeLeaves>();
	
	//@JsonManagedReference(value = "bnkChqL")
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="bank_chequebook",
	joinColumns=@JoinColumn(name="bnkId"),
	inverseJoinColumns=@JoinColumn(name="chqBkId"))
	private List<ChequeBook> chequeBookList = new ArrayList<>();
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="bank_chqreq",
	joinColumns=@JoinColumn(name="bnkId"),
	inverseJoinColumns=@JoinColumn(name="cReqId"))
	private List<ChequeRequest> chequeRequestList = new ArrayList<>();
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="bank_atmcard",
	joinColumns=@JoinColumn(name="bnkId"),
	inverseJoinColumns=@JoinColumn(name="atmCardId"))
	private List<AtmCardMstr> atmCardMstrList = new ArrayList<>(); 
	
	
	@Column(name="bnkSingleSignatory", columnDefinition="longblob")
	private ArrayList<Map<String, Object>> bnkSingleSignatoryList;
	
	@Column(name="bnkJointSignatory", columnDefinition="longblob")
	private ArrayList<Map<String, Object>> bnkJointSignatoryList;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	 
	@Column(name="bnkAlloted", columnDefinition="boolean default false", nullable=false)
	private boolean bnkAlloted;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	
	public double getBnkBalanceAmt() {
		return bnkBalanceAmt;
	}

	public void setBnkBalanceAmt(double bnkBalanceAmt) {
		this.bnkBalanceAmt = bnkBalanceAmt;
	}

	public String getBnkName() {
		return bnkName;
	}

	public void setBnkName(String bnkName) {
		this.bnkName = bnkName;
	}

	public int getBnkId() {
		return bnkId;
	}

	public void setBnkId(int bnkId) {
		this.bnkId = bnkId;
	}

	public String getBnkFaCode() {
		return bnkFaCode;
	}

	public void setBnkFaCode(String bnkFaCode) {
		this.bnkFaCode = bnkFaCode;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getBnkAcNo() {
		return bnkAcNo;
	}

	public void setBnkAcNo(String bnkAcNo) {
		this.bnkAcNo = bnkAcNo;
	}

	public Date getBnkOpenDt() {
		return bnkOpenDt;
	}

	public void setBnkOpenDt(Date bnkOpenDt) {
		this.bnkOpenDt = bnkOpenDt;
	}

	public Date getBnkCloseDt() {
		return bnkCloseDt;
	}

	public void setBnkCloseDt(Date bnkCloseDt) {
		this.bnkCloseDt = bnkCloseDt;
	}

	public ArrayList<Map<String, Object>> getBnkSingleSignatoryList() {
		return bnkSingleSignatoryList;
	}

	public void setBnkSingleSignatoryList(
			ArrayList<Map<String, Object>> bnkSingleSignatoryList) {
		this.bnkSingleSignatoryList = bnkSingleSignatoryList;
	}

	public ArrayList<Map<String, Object>> getBnkJointSignatoryList() {
		return bnkJointSignatoryList;
	}

	public void setBnkJointSignatoryList(
			ArrayList<Map<String, Object>> bnkJointSignatoryList) {
		this.bnkJointSignatoryList = bnkJointSignatoryList;
	}

	public List<ChequeBook> getChequeBookList() {
		return chequeBookList;
	}

	public void setChequeBookList(List<ChequeBook> chequeBookList) {
		this.chequeBookList = chequeBookList;
	}

	public List<ChequeRequest> getChequeRequestList() {
		return chequeRequestList;
	}

	public void setChequeRequestList(List<ChequeRequest> chequeRequestList) {
		this.chequeRequestList = chequeRequestList;
	}

	public String getBnkLocal() {
		return bnkLocal;
	}

	public void setBnkLocal(String bnkLocal) {
		this.bnkLocal = bnkLocal;
	}

	public int getBnkBrMinChq() {
		return bnkBrMinChq;
	}

	public void setBnkBrMinChq(int bnkBrMinChq) {
		this.bnkBrMinChq = bnkBrMinChq;
	}

	public int getBnkBrMaxChq() {
		return bnkBrMaxChq;
	}

	public void setBnkBrMaxChq(int bnkBrMaxChq) {
		this.bnkBrMaxChq = bnkBrMaxChq;
	}

	public int getBnkHoMinChq() {
		return bnkHoMinChq;
	}

	public void setBnkHoMinChq(int bnkHoMinChq) {
		this.bnkHoMinChq = bnkHoMinChq;
	}

	public int getBnkHoMaxChq() {
		return bnkHoMaxChq;
	}

	public void setBnkHoMaxChq(int bnkHoMaxChq) {
		this.bnkHoMaxChq = bnkHoMaxChq;
	}

	public String getBnkIfscCode() {
		return bnkIfscCode;
	}

	public void setBnkIfscCode(String bnkIfscCode) {
		this.bnkIfscCode = bnkIfscCode;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public List<ChequeLeaves> getChequeLeavesList() {
		return chequeLeavesList;
	}

	public void setChequeLeavesList(List<ChequeLeaves> chequeLeavesList) {
		this.chequeLeavesList = chequeLeavesList;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getBnkMicrCode() {
		return bnkMicrCode;
	}

	public void setBnkMicrCode(String bnkMicrCode) {
		this.bnkMicrCode = bnkMicrCode;
	}

	public boolean isBnkAlloted() {
		return bnkAlloted;
	}

	public void setBnkAlloted(boolean bnkAlloted) {
		this.bnkAlloted = bnkAlloted;
	}

	public List<AtmCardMstr> getAtmCardMstrList() {
		return atmCardMstrList;
	}

	public void setAtmCardMstrList(List<AtmCardMstr> atmCardMstrList) {
		this.atmCardMstrList = atmCardMstrList;
	}
	
	 
}
