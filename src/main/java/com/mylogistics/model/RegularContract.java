package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "regularcontract")
public class RegularContract {
	
	@Id
	@Column(name="regContId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int regContId;
	
	@Column(name="regContCode")
	private String regContCode;
	
	@Column(name="regContBLPMCode")
	private String regContBLPMCode;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="regContCngrCode")
	private String regContCngrCode;
	
	@Column(name="regContCrName")
	private String regContCrName;
	
	@Column(name="regContFromStation")
	private String regContFromStation;
	
	@Column(name="regContToStation")
	private String regContToStation;
	
	@Column(name="regContRate")
	private double regContRate;
	
	@Column(name="regContFromDt")
	private Date regContFromDt;
	
	@Column(name="regContToDt")
	private Date regContToDt;
	
	@Column(name="regContType")
	private String regContType;
	
	/*@Column(name="regContFromWt")
	private double regContFromWt;
	
	@Column(name="regContToWt")
	private double regContToWt;*/
	
	/*@Column(name="regContProductType")
	private String regContProductType;*/
	
	/*@Column(name="regContTransitDay")
	private int regContTransitDay;
	
	@Column(name="regContStatisticalCharge")
	private double regContStatisticalCharge;*/
	
	/*@Column(name="regContLoad")
	private double regContLoad;
	
	@Column(name="regContUnLoad")
	private double regContUnLoad;*/
	
	@Column(name="regContProportionate")
	private String regContProportionate;
	
	/*@Column(name="regContAdditionalRate")
	private double regContAdditionalRate;*/

	@Column(name="regContValue")
	private double regContValue;
	
	@Column(name="regContDc")
	private String regContDc;
	
	@Column(name="regContCostGrade")
	private String regContCostGrade;
	
	@Column(name="regContWt")
	private double regContWt;
	
	@Column(name="regContDdl")
	private String regContDdl;
	
	@Column(name="regContRenew")
	private String regContRenew;
	
	@Column(name="regContRenewDt")
	private Date regContRenewDt;
	
	@Column(name="regContInsuredBy")
	private String regContInsuredBy;
	
	@Column(name="regContInsureComp")
	private String regContInsureComp;
	
	@Column(name="regContInsureUnitNo")
	private String regContInsureUnitNo;
	
	@Column(name="regContInsurePolicyNo")
	private String regContInsurePolicyNo;
	
	@Column(name="regContRemark")
	private String regContRemark;
	
	@Column(name="regContBillBasis")
	private String regContBillBasis;
	
	@Column(name="regContIsVerify")
	private String regContIsVerify;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="regContFaCode")
	private String regContFaCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getRegContId() {
		return regContId;
	}

	public void setRegContId(int regContId) {
		this.regContId = regContId;
	}

	public String getRegContCode() {
		return regContCode;
	}

	public void setRegContCode(String regContCode) {
		this.regContCode = regContCode;
	}

	public String getRegContBLPMCode() {
		return regContBLPMCode;
	}

	public void setRegContBLPMCode(String regContBLPMCode) {
		this.regContBLPMCode = regContBLPMCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getRegContCngrCode() {
		return regContCngrCode;
	}

	public void setRegContCngrCode(String regContCngrCode) {
		this.regContCngrCode = regContCngrCode;
	}

	
	public String getRegContCrName() {
		return regContCrName;
	}

	public void setRegContCrName(String regContCrName) {
		this.regContCrName = regContCrName;
	}

	public String getRegContFromStation() {
		return regContFromStation;
	}

	public void setRegContFromStation(String regContFromStation) {
		this.regContFromStation = regContFromStation;
	}

	public String getRegContToStation() {
		return regContToStation;
	}

	public void setRegContToStation(String regContToStation) {
		this.regContToStation = regContToStation;
	}

	public double getRegContRate() {
		return regContRate;
	}

	public void setRegContRate(double regContRate) {
		this.regContRate = regContRate;
	}

	public String getRegContType() {
		return regContType;
	}

	public void setRegContType(String regContType) {
		this.regContType = regContType;
	}

	/*public int getRegContTransitDay() {
		return regContTransitDay;
	}

	public void setRegContTransitDay(int regContTransitDay) {
		this.regContTransitDay = regContTransitDay;
	}

	public double getRegContStatisticalCharge() {
		return regContStatisticalCharge;
	}

	public void setRegContStatisticalCharge(double regContStatisticalCharge) {
		this.regContStatisticalCharge = regContStatisticalCharge;
	}

	public double getRegContLoad() {
		return regContLoad;
	}

	public void setRegContLoad(double regContLoad) {
		this.regContLoad = regContLoad;
	}

	public double getRegContUnLoad() {
		return regContUnLoad;
	}

	public void setRegContUnLoad(double regContUnLoad) {
		this.regContUnLoad = regContUnLoad;
	}
*/
	public String getRegContProportionate() {
		return regContProportionate;
	}

	public void setRegContProportionate(String regContProportionate) {
		this.regContProportionate = regContProportionate;
	}

	public double getRegContValue() {
		return regContValue;
	}

	public void setRegContValue(double regContValue) {
		this.regContValue = regContValue;
	}

	public String getRegContDc() {
		return regContDc;
	}

	public void setRegContDc(String regContDc) {
		this.regContDc = regContDc;
	}

	public String getRegContCostGrade() {
		return regContCostGrade;
	}

	public void setRegContCostGrade(String regContCostGrade) {
		this.regContCostGrade = regContCostGrade;
	}

	public double getRegContWt() {
		return regContWt;
	}

	public void setRegContWt(double regContWt) {
		this.regContWt = regContWt;
	}

	public String getRegContDdl() {
		return regContDdl;
	}

	public void setRegContDdl(String regContDdl) {
		this.regContDdl = regContDdl;
	}

	public String getRegContRenew() {
		return regContRenew;
	}

	public void setRegContRenew(String regContRenew) {
		this.regContRenew = regContRenew;
	}

	public String getRegContInsuredBy() {
		return regContInsuredBy;
	}

	public void setRegContInsuredBy(String regContInsuredBy) {
		this.regContInsuredBy = regContInsuredBy;
	}

	public String getRegContInsureComp() {
		return regContInsureComp;
	}

	public void setRegContInsureComp(String regContInsureComp) {
		this.regContInsureComp = regContInsureComp;
	}

	public String getRegContInsureUnitNo() {
		return regContInsureUnitNo;
	}

	public void setRegContInsureUnitNo(String regContInsureUnitNo) {
		this.regContInsureUnitNo = regContInsureUnitNo;
	}

	public String getRegContInsurePolicyNo() {
		return regContInsurePolicyNo;
	}

	public void setRegContInsurePolicyNo(String regContInsurePolicyNo) {
		this.regContInsurePolicyNo = regContInsurePolicyNo;
	}

	public String getRegContRemark() {
		return regContRemark;
	}

	public void setRegContRemark(String regContRemark) {
		this.regContRemark = regContRemark;
	}

	

	public Date getRegContFromDt() {
		return regContFromDt;
	}

	public void setRegContFromDt(Date regContFromDt) {
		this.regContFromDt = regContFromDt;
	}

	public Date getRegContToDt() {
		return regContToDt;
	}

	public void setRegContToDt(Date regContToDt) {
		this.regContToDt = regContToDt;
	}

	public Date getRegContRenewDt() {
		return regContRenewDt;
	}

	public void setRegContRenewDt(Date regContRenewDt) {
		this.regContRenewDt = regContRenewDt;
	}

	public String getRegContIsVerify() {
		return regContIsVerify;
	}

	public void setRegContIsVerify(String regContIsVerify) {
		this.regContIsVerify = regContIsVerify;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getRegContFaCode() {
		return regContFaCode;
	}

	public void setRegContFaCode(String regContFaCode) {
		this.regContFaCode = regContFaCode;
	}

	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}

	public String getRegContBillBasis() {
		return regContBillBasis;
	}

	public void setRegContBillBasis(String regContBillBasis) {
		this.regContBillBasis = regContBillBasis;
	}
	
	
}
