package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.AllStation;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;

@Repository
public interface StationDAO {
	
	//Satarupa: saves station to database
	public int saveStationToDB(Station station);
	
	//Satarupa: retrieves the complete database of station table
	public List<Station> getStationData();
	
	public List<Station> getStationDataByNameCode(String stnNameCode);
	
	public List<Station> getStationDataByStationName(String stnName);
	
	/*//Satarupa: retrieves the list of station code from database
	public List<String> getStationCode();*/
	
	/*//Satarupa:
	 public List<String> getAllStationCodes();*/
	
	//Satarupa: retrieves the details of station on the basis of stationCode
	public List<Station> retrieveStation(String stationCode);
	 
	//Satarupa: updates station 
	public int updateStation(Station station);
	
	//Satarupa: 
	public List<Station> getLastStationRow();
	
	//Satarupa:
	public long totalCount();
	
	public List<Map<String, Object>> getStnNameCodePin();
	
	public List<Station> getFrmStnList(String custCode);
	
	public List<Station> getToStnList(String contCode);

	public List<Station> getStationList();
	
	public List<Station> getStationDataByNameCode(Session session, User user, String stnNameCode);
	
	public Station getStationByStnCode(Session session, User user, String stnCode);

	public List getDistByStateCode(String stateCode);

	public List getCityByDistName(String distName, String stateCode);

	public List<Station> getStnByCityDistState(String cityName,String distName, String stateCode);

	public List<Station> getStnByPin(String pin);

	public List getADistByStateCode(String stateCode);

	public List getACityByDistName(String distName, String stateCode);

	public List<AllStation> getAStnByCityDistState(String cityName, String distName, String stateCode);

	public List<AllStation> getAStnByPin(String pin);
	
	public boolean getStationExist(String stnName,String stnCity,String pin);
	
}
