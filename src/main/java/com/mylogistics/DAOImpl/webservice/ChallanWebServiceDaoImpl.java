

package com.mylogistics.DAOImpl.webservice;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.inst2xsd.VenetianBlindStrategy;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.log.SysoCounter;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.itextpdf.text.pdf.PdfWriter;
import com.mylogistics.DAO.werservice.ChallanWebServiceDao;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.BranchStockLeafDetCNTS;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.ContPersonCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.VehicleVendorMstrCNTS;
import com.mylogistics.constants.webservice.ChallanAppCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.Customer;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.model.webservice.ChallanApp;
import com.mylogistics.model.webservice.ChallanDTApp;
import com.mylogistics.model.webservice.CnmtApp;
import com.mylogistics.model.webservice.CnmtChlnApp;
import com.mylogistics.model.webservice.ImageApp;
import com.mylogistics.model.webservice.OwnBrkApp;
import com.mylogistics.model.webservice.VehicleApp;
import com.mylogistics.services.ConstantsValues;



public class ChallanWebServiceDaoImpl implements ChallanWebServiceDao{

	private SessionFactory sessionFactory;
	private Session session;	
	private Transaction transaction;
	
	private String ownerPanImgPath = "/var/www/html/Erp_Image/Owner/Pan";
	private String ownerDecImgPath = "/var/www/html/Erp_Image/Owner/Dec";
	
	private String brokerPanImgPath = "/var/www/html/Erp_Image/Broker/Pan";
	private String brokerDecImgPath = "/var/www/html/Erp_Image/Broker/Dec";
	
	private String newImgPath = "/var/www/html/Erp_Image/New";
	
	@Autowired
	private HttpServletRequest httpServletRequest;
	
	
	public static Logger logger = Logger.getLogger(ChallanWebServiceDaoImpl.class);
	
	@Autowired
	public ChallanWebServiceDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getCnmtCodeForChallan(String branchCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
		try{			
			session = this.sessionFactory.openSession();
			List<Map<String, Object>> cnmtCodeList = session.createQuery("SELECT cnmtCode AS cnmtCode, cnmtNoOfPkg AS cnmtNoOfPkg, cnmtActualWt AS cnmtActualWt FROM Cnmt WHERE branchCode = :branchCode")
					.setParameter("branchCode", branchCode)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			System.out.println("CnmtCode List : "+cnmtCodeList.size());
			if(! cnmtCodeList.isEmpty()){
				Iterator it = cnmtCodeList.iterator();
				List<Map<String, Object>> cnmtCodeList1 = new ArrayList<Map<String, Object>>();
				while(it.hasNext()){
					Map map = (Map)it.next();
					Double cnmtActualWt = Double.parseDouble(String.valueOf(map.get("cnmtActualWt")));					
					cnmtActualWt = cnmtActualWt / 1000;			
					map.put("cnmtActualWt", cnmtActualWt);					
					cnmtCodeList1.add(map);
				}
				finalList.addAll(cnmtCodeList1);
			}		
			List<Map<String, Object>> cnmtTransList = session.createQuery("SELECT cnmtCode AS cnmtCode, cnmtNoOfPkg AS cnmtNoOfPkg, cnmtActualWt AS cnmtActualWt FROM Cnmt WHERE branchCode != :branchCode AND isTrans = :isTrans")
					.setParameter("branchCode", branchCode)
					.setParameter("isTrans", true)					
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();	
			System.out.println("CnmtTrans List : "+cnmtTransList.size());
			if(! cnmtTransList.isEmpty()){
				Iterator it = cnmtTransList.iterator();
				List<Map<String, Object>>cnmtTransList1 = new ArrayList<Map<String, Object>>();
				while(it.hasNext()){
					Map map = (Map)it.next();					
					Double cnmtActualWt = Double.parseDouble(String.valueOf(map.get("cnmtActualWt")));					
					cnmtActualWt = cnmtActualWt / 1000;					
					map.put("cnmtActualWt", cnmtActualWt);
					cnmtTransList1.add(map);
				}
				finalList.addAll(cnmtTransList1);
			}			
			List<Map<String, Object>> cnmtPendingCodeList = session.createQuery("SELECT cnmtCode AS cnmtCode, cnmtNoOfPkg AS cnmtNoOfPkg, cnmtActualWt AS cnmtActualWt FROM CnmtApp WHERE cnmtBranchCode = :cnmtBranchCode AND cnmtIsPending = :cnmtIsPending AND cnmtIsCancel = :cnmtIsCancel")					
					.setParameter("cnmtBranchCode", branchCode)
					.setParameter("cnmtIsPending", true)
					.setParameter("cnmtIsCancel", false)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			System.out.println("Pending Cnmt : "+cnmtPendingCodeList.size());
			if(! cnmtPendingCodeList.isEmpty()){
				Iterator it = cnmtPendingCodeList.iterator();
				List<Map<String, Object>> finalPendingCodeList = new ArrayList<Map<String, Object>>();
				while(it.hasNext()){
					Map map = (Map)it.next();
					Double cnmtActualWt = Double.parseDouble(String.valueOf(map.get("cnmtActualWt")));
					cnmtActualWt = cnmtActualWt;
					map.put("cnmtActualWt", cnmtActualWt);
					finalPendingCodeList.add(map);
				}
				finalList.addAll(finalPendingCodeList);
			}						
			if(! finalList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("cnmtCodeList", finalList);
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No Such Record !");
			}			
		}catch(Exception e){
			System.out.println("Error in getCnmtCodeForChallan - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getChallanCode(String branchCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		try{			
			List<Map<String, Object>> chlnCodeList = session.createQuery("SELECT brsLeafDetSNo AS chlnCode FROM BranchStockLeafDet WHERE brsLeafDetStatus = :brsLeafDetStatus AND brsLeafDetBrCode = :brsLeafDetBrCode")
					.setParameter("brsLeafDetStatus", "chln")
					.setParameter("brsLeafDetBrCode", branchCode)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			resultMap.put("chlnCodeList", chlnCodeList);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
		}catch(Exception e){
			System.out.println("Error in getChallanCode - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);			
		}		
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public String getBranchNameByCode(String branchCode){
		String branchName = "";
		try{
			session = this.sessionFactory.openSession();
			Criteria criteria = session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_CODE, branchCode));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(BranchCNTS.BRANCH_NAME));
			criteria.setProjection(projectionList);
			List<String> branchNameList = criteria.list();
			if(! branchNameList.isEmpty())
				branchName = branchNameList.get(0);
		}catch(Exception e){
			System.out.println("Error in getBranchNameByCode - ChallanWebServiceDaoImpl : "+e);			
		}
		session.flush();
		session.clear();
		session.close();
		return branchName;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map getCnmtDetailForChallan(String cnmtCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		try{
			System.out.println("CNMT Code : "+cnmtCode);
			session = this.sessionFactory.openSession();
			List<Map<String, Object>> cnmtList = session.createQuery("SELECT cnmtCode AS cnmtCode, cnmtFromSt AS cnmtFromStCode, cnmtToSt AS cnmtToStCode, cnmtEmpCode AS cnmtEmpCode, cnmtNoOfPkg AS cnmtNoOfPkg, cnmtActualWt AS cnmtActualWt FROM Cnmt WHERE cnmtCode = :cnmtCode")
					.setParameter("cnmtCode", cnmtCode)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();			
			if(! cnmtList.isEmpty()){
				Map map = cnmtList.get(0);				
				String cnmtFromStCode = String.valueOf(map.get("cnmtFromStCode"));
				String cnmtToStCode = String.valueOf(map.get("cnmtToStCode"));						
				String empCode = String.valueOf(map.get("cnmtEmpCode"));
				
				List cnmtFromStList = session.createQuery("SELECT stnName FROM Station WHERE stnCode = :stnCode")
					.setParameter("stnCode", cnmtFromStCode)					
					.list();
				map.put("cnmtFromStName", cnmtFromStList.get(0));
				List cnmtToStList = session.createQuery("SELECT stnName FROM Station WHERE stnCode = :stnCode")
						.setParameter("stnCode", cnmtToStCode)
						.list();
				map.put("cnmtToStName", cnmtToStList.get(0));
				List empNameList = session.createQuery("SELECT empName FROM Employee WHERE empCode = :empCode")
						.setParameter("empCode", empCode)
						.list();
				map.put("cnmtEmpName", empNameList.get(0));
				Double cnmtActualWt = Double.parseDouble(String.valueOf(map.get("cnmtActualWt")));
				cnmtActualWt = cnmtActualWt / 1000;
				map.put("cnmtActualWt", cnmtActualWt);
				
				map.put("cnmtVehicleTypeCode", "");
				map.put("cnmtVehicleType", "");
				
				List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
				finalList.add(map);
				if(! finalList.isEmpty()){
					resultMap.put("cnmtList", finalList);				
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				}else{									
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "No Such Record!");
				}
			}else{
				List<Map<String, Object>> cnmtPendingList = session.createQuery("SELECT cnmtCode AS cnmtCode, cnmtFromStCode AS cnmtFromStCode, cnmtFromStName AS cnmtFromStName, cnmtToStCode AS cnmtToStCode, cnmtToStName AS cnmtToStName, cnmtEmpCode AS cnmtEmpCode, cnmtNoOfPkg AS cnmtNoOfPkg, cnmtActualWt AS cnmtActualWt, cnmtVehicleTypeCode AS cnmtVehicleTypeCode, cnmtVehicleType AS cnmtVehicleType FROM CnmtApp WHERE cnmtCode = :cnmtCode")
						.setParameter("cnmtCode", cnmtCode)
						.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
						.list();				
				if(! cnmtPendingList.isEmpty()){
					
					Map map = cnmtPendingList.get(0);
					String empCode = String.valueOf(map.get("cnmtEmpCode"));
					List empNameList = session.createQuery("SELECT empName FROM Employee WHERE empCode = :empCode")
							.setParameter("empCode", empCode)
							.list();
					map.put("cnmtEmpName", empNameList.get(0));					
					List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
					finalList.add(map);
					resultMap.put("cnmtList", finalList);
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				}else{
					resultMap.put("msg", "No Such Record!");
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				}
			}
		}catch(Exception e){
			System.out.println("Error in getChallanCode - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.flush();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getVehicleNoList(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List finalList = new ArrayList();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);			
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(VehicleVendorMstrCNTS.VV_RCNO));
			cr.setProjection(projList);
			List list = cr.list();
			if(! list.isEmpty())
				finalList.addAll(list);
			
			cr = session.createCriteria(VehicleApp.class);
			cr.add(Restrictions.eq("isPending", true));
			projList = Projections.projectionList();
			projList.add(Projections.property("vRcNo"));
			cr.setProjection(projList);
			List list1 = cr.list();			
			if(! list1.isEmpty())
				finalList.addAll(list1);
			
			if(! finalList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("vehicleNoList", finalList);
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No Such Record !");
			}
		}catch(Exception e){
			System.out.println("Error in getVehicleNoList - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getChlnCodeForChallan(String branchCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			List chlnCodeList = session.createQuery("SELECT brsLeafDetSNo FROM BranchStockLeafDet WHERE brsLeafDetStatus = :brsLeafDetStatus AND brsLeafDetBrCode = :brsLeafDetBrCode")
					.setParameter("brsLeafDetStatus", "chln")
					.setParameter("brsLeafDetBrCode", branchCode)					
					.list();
			if(! chlnCodeList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("chlnCodeList", chlnCodeList);
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No Such Record !");
			}
		}catch(Exception e){
			System.out.println("Error in getChlnCodeForChallan - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Cnmt> getCnmtByCnmtCode(String cnmtCode, String type){
		List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		try{
			session = this.sessionFactory.openSession();
			if(type.equalsIgnoreCase("cnmt")){
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
				cnmtList = cr.list();
			}else{
				
			}
		}catch(Exception e){
			System.out.println("Error in getCnmtByCnmtCode - ChallanWebServiceDaoImpl : "+e);			
		}
		session.flush();
		session.clear();
		session.close();
		return cnmtList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Integer> getCnmt_ChallanByCnmtId(int cnmtId){
		List<Integer> cnmt_challanList = new ArrayList<Integer>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt_Challan.class);
			 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtId));
			 ProjectionList projectionList = Projections.projectionList();
			 projectionList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_Id));
			 cr.setProjection(projectionList);
			 cnmt_challanList =cr.list();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		session.clear();
		session.close();
		return cnmt_challanList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getChallanToStationById(int chlnId){
		List<String> challanList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHLN_ID, chlnId));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(ChallanCNTS.CHLN_TO_STN));
			cr.setProjection(projectionList);
			challanList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return challanList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public String saveChallanAndChallanDTAndCnmtChlnApp(ChallanApp challanApp, ChallanDTApp challanDTApp, List<CnmtChlnApp> cnmtChlnAppList){
		String status = "";
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			List<String> vtCode = session.createQuery("SELECT vtCode FROM VehicleType WHERE vtVehicleType = :vtVehicleType")
					.setParameter("vtVehicleType", challanApp.getChlnVehicleType())
					.list();
			if(! vtCode.isEmpty())
				challanApp.setChlnVehicleCode(vtCode.get(0));
			
			Integer challanDTAppId = (Integer) session.save(challanDTApp);
			challanApp.setChlnChdCode(String.valueOf(challanDTAppId));
			String payAt = challanApp.getChlnPayAt();
			
			String payAtCode = (String)session.createQuery("SELECT branchCode FROM Branch WHERE branchName = :branchName")
					.setParameter(BranchCNTS.BRANCH_NAME, payAt)
					.list().get(0);
			challanApp.setChlnPayAtCode(payAtCode);
			session.save(challanApp);			
			for(CnmtChlnApp cnmtChlnApp : cnmtChlnAppList){
				cnmtChlnApp.setChlnCode(challanApp.getChlnCode());
				session.save(cnmtChlnApp);
			}		
			transaction.commit();
			status = "success";
		}catch(Exception e){
			status = "error";
			System.out.println("Error in saveChallan - ChallanWebService : "+e);
		}		
		session.clear();
		session.close();
		return status;
	}	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getVehicleForChln(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			//cr.add(Restrictions.eq(VehicleVendorMstrCNTS.B_CODE, currentUser.getUserBranchCode()));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(VehicleVendorMstrCNTS.VV_RCNO));
			cr.setProjection(projList);
			List vehicleList = cr.list();
			if(vehicleList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No Such Record !");
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("vNoList", vehicleList);			
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");			
		}
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getChlnDtByVehicleNo(String vehicleNo){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, vehicleNo));
			List vvmList = cr.list();
			if(vvmList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No Such Record !");
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("vehicleMst", vvmList.get(0));
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
			System.out.println("Error in getChlnDtByVehicleNo - ChallanWebServiceDaoImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getVehicleDtWe(String vehicleNo){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> errorMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, vehicleNo));
			List<VehicleVendorMstr> vVMList = cr.list();			
			if(vVMList.isEmpty()){				
				List<VehicleApp> vehicleAppList = session.createCriteria(VehicleApp.class)
						.add(Restrictions.eq("vRcNo", vehicleNo))
						.list();
				if(vehicleAppList.isEmpty()){					
					errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					errorMap.put("msg", "No such Record !");
					return errorMap;
				}else{
					VehicleApp vehicleApp = (VehicleApp) vehicleAppList.get(0);					
					resultMap.put("type", "new");
					resultMap.put("vehicle", vehicleApp);
					String ownCode = vehicleApp.getvOwnCode();
					String ownName = vehicleApp.getvOwnName();
					String brkCode = vehicleApp.getvBrkCode();
					String brkName = vehicleApp.getvBrkName();
					List<String> brkMobList = new ArrayList<String>();
					List<String> ownMobList = new ArrayList<String>();
					if(ownCode.equalsIgnoreCase("")){
						logger.error("New Owner !");
						List<OwnBrkApp> ownBrkList = session.createCriteria(OwnBrkApp.class)
								.add(Restrictions.eq("ownBrkName", ownName))
								.add(Restrictions.eq("type", "owner"))
								.list();
						if(ownBrkList.isEmpty()){
							logger.error("New Owner Not Found !");
							errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							errorMap.put("msg", "Pending Owner not found !");
							return errorMap;
						}else{
							logger.error("New Owner Found !");
							OwnBrkApp ownBrkApp = ownBrkList.get(0);
							List<String> ownMob = new ArrayList<String>();
							ownMob.add(ownBrkApp.getOwnBrkPhoneNo());
							resultMap.put("ownMobList", ownMob);
							resultMap.put("isOwnPan", ownBrkApp.getIsPanImg());							
						}
					}else{
						List<Owner> ownerList = session.createCriteria(Owner.class)
								.add(Restrictions.eq(OwnerCNTS.OWN_CODE, ownCode))
								.list();
						if(ownerList.isEmpty()){
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Owner not found !");
							return resultMap;
						}
						Owner owner = (Owner)ownerList.get(0);
						resultMap.put("isOwnPan", owner.isOwnIsPanImg());					
						 Criteria ownCriteria = session.createCriteria(ContPerson.class);
					        ownCriteria.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE, ownCode));
							List<ContPerson>contPersonList = ownCriteria.list();
					        if(! contPersonList.isEmpty()){
					        	ContPerson contPerson = contPersonList.get(0);
					        	ownMobList = contPerson.getCpMobile();
					        }
					        if(ownMobList == null)
					        	ownMobList = new ArrayList<String>();
					        Set finalOwnMobList = new HashSet(ownMobList);	
					        resultMap.put("ownMobList", finalOwnMobList);
					}
					if(brkCode.equalsIgnoreCase("")){
						List<OwnBrkApp> ownBrkList = session.createCriteria(OwnBrkApp.class)
								.add(Restrictions.eq("ownBrkName", brkName))
								.add(Restrictions.eq("type", "broker"))
								.list();
						if(ownBrkList.isEmpty()){
							errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							errorMap.put("msg", "Pending Broker not found !");
							return errorMap;
						}else{
							OwnBrkApp ownBrkApp = ownBrkList.get(0);
							List<String> ownMob = new ArrayList<String>();
							ownMob.add(ownBrkApp.getOwnBrkPhoneNo());
							resultMap.put("brkMobList", ownMob);
							resultMap.put("isBrkPan", ownBrkApp.getIsPanImg());							
						}
					}else{
						List<Broker> brokerList = session.createCriteria(Broker.class)
								.add(Restrictions.eq(BrokerCNTS.BRK_CODE, brkCode))
								.list();
						if(brokerList.isEmpty()){
							errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							errorMap.put("msg", "Broker not found !");
							return errorMap;
						}
						Broker broker = (Broker)brokerList.get(0);
						resultMap.put("isBrkPan", broker.isBrkIsPanImg());					
						 Criteria ownCriteria = session.createCriteria(ContPerson.class);
					        ownCriteria.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE, brkCode));
							List<ContPerson>contPersonList = ownCriteria.list();
					        if(! contPersonList.isEmpty()){
					        	ContPerson contPerson = contPersonList.get(0);
					        	brkMobList = contPerson.getCpMobile();
					        }
					        if(brkMobList == null)
					        	brkMobList = new ArrayList<String>();
					        Set finalBrkMobList = new HashSet(brkMobList);	
					        resultMap.put("brkMobList", finalBrkMobList);
					}
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				}				
			}else{				
				VehicleVendorMstr mstr = vVMList.get(0);				
				// Broker and Owner Mobile List
				Broker broker = mstr.getBroker();
				Owner owner = mstr.getOwner();				
				String brkCode = broker.getBrkCode();
				String ownCode = owner.getOwnCode();
				List<ContPerson> contPersonList = new ArrayList<ContPerson>();
				List<String> brkMobList = new ArrayList<String>();
				List<String> ownMobList = new ArrayList<String>();
				// Broker Mobile List			
				Criteria brkCriteria = session.createCriteria(ContPerson.class);
				brkCriteria.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE, brkCode));
				contPersonList = brkCriteria.list();
		        if(! contPersonList.isEmpty()){
		        	ContPerson contPerson = contPersonList.get(0);
		        	brkMobList = contPerson.getCpMobile();
		        }
		     
		        Criteria ownCriteria = session.createCriteria(ContPerson.class);
		        ownCriteria.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE, ownCode));
				contPersonList = ownCriteria.list();
		        if(! contPersonList.isEmpty()){
		        	ContPerson contPerson = contPersonList.get(0);
		        	ownMobList = contPerson.getCpMobile();
		        }     
		       
		        if(brkMobList == null)
		        	brkMobList = new ArrayList<String>();
		        if(ownMobList == null)
		        	ownMobList = new ArrayList<String>();
		        
		        Set finalBrkMobList = new HashSet(brkMobList);
		        Set finalOwnMobList = new HashSet(ownMobList);		        
		        resultMap.put("vehicleMstr", mstr);
		        resultMap.put("brkMobList", finalBrkMobList);
		        resultMap.put("ownMobList", finalOwnMobList);
		        resultMap.put("type", "old");
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);				
			}
		}catch(Exception e){
			System.out.println("Error in getVehicleDtWe - ChallanWebServiceDaoImpl : "+e);
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Database Exception !");
			return errorMap;
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> uploadOwnPanDecImage(OwnerImg ownerImg, Owner owner){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Owner ow = (Owner) session.get(Owner.class, owner.getOwnId());
			Integer imgId = -1;
			Integer ownImgId = owner.getOwnImgId();
			if(ownImgId == null)
				ownImgId = 0;
			if(ownImgId > 0){
				OwnerImg img = (OwnerImg)session.get(OwnerImg.class, owner.getOwnImgId());
				ownerImg.setOwnImgId(owner.getOwnImgId());
				session.merge(ownerImg);
				imgId = owner.getOwnImgId();
			}else{
				imgId = (Integer) session.save(ownerImg);
			}
			owner.setOwnImgId(imgId);
			session.merge(owner);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);			
		}catch(Exception e){
			System.out.println("Error in uploadOwnPanDecImage - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		transaction.commit();
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> uploadBrkPanDecImage(BrokerImg brokerImg, Broker broker){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Integer imgId = -1;
			Broker brk = (Broker) session.get(Broker.class, broker.getBrkId());
			Integer brkImgId = broker.getBrkImgId();
			if(brkImgId == null)
				brkImgId = 0;
			if(broker.getBrkImgId() > 0){
				BrokerImg img = (BrokerImg)session.get(BrokerImg.class, broker.getBrkImgId());
				brokerImg.setBrkImgId(broker.getBrkImgId());
				session.merge(brokerImg);
				imgId = broker.getBrkImgId();
			}else{
				imgId = (Integer) session.save(brokerImg);
			}
			broker.setBrkImgId(imgId);
			session.merge(broker);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
		}catch(Exception e){
			System.out.println("Error in uploadBrkPanDecImage - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		transaction.commit();
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Owner getOwnerById(Integer ownId){
		Owner owner = null;
		try{
			session = this.sessionFactory.openSession();			
			owner = (Owner) session.get(Owner.class, ownId);
		}catch(Exception e){
			System.out.println("Error in getOwnerById - ChallanWebServiceDaoImpl : "+e);			
		}		
		session.flush();
		session.clear();
		session.close();
		return owner;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Broker getBrokerById(Integer brkId){
		Broker broker = null;
		try{
			session = this.sessionFactory.openSession();			
			broker = (Broker) session.get(Broker.class, brkId);
		}catch(Exception e){
			System.out.println("Error in getBrokerById - ChallanWebServiceDaoImpl : "+e);			
		}		
		session.flush();
		session.clear();
		session.close();
		return broker;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getPendingChallanCode(){
		List<String> chlnCodeList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			Criteria chlnCodeCriteria = session.createCriteria(ChallanApp.class);
			chlnCodeCriteria.add(Restrictions.eq("isPending", true));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(ChallanCNTS.CHALLAN_CODE));
			chlnCodeCriteria.setProjection(projectionList);
			chlnCodeList = chlnCodeCriteria.list();
			Set set = new HashSet(chlnCodeList);
			chlnCodeList.clear();
			chlnCodeList = new ArrayList(set);
		}catch(Exception e){
			System.out.println("Error in getPendingChallanCode - ChallanWebServiceDaoImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return chlnCodeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getPendingChallanDetail(String chlnCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			// Find ChallanApp
			List<ChallanApp> challanAppList = session.createCriteria(ChallanApp.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
					.list();
			if(challanAppList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "Challan Detail Not Found !");
			}else{
				resultMap.put("challanApp", challanAppList.get(0));
				// Find ChallanDetailApp
				List<ChallanDTApp> challanDTAppList = session.createCriteria(ChallanDTApp.class)
						.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
						.list();
				resultMap.put("challnDTApp", challanDTAppList.get(0));			
				// Find CnmtChallanApp
				List<CnmtChlnApp> cnmtChlnAppList = session.createCriteria(CnmtChlnApp.class)
						.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
						.list();
				Boolean isPending = false;
				List<String> pendingCnmtList = new ArrayList<String>();
				for(int i=0; i<cnmtChlnAppList.size(); i++){
					CnmtChlnApp cnmtChlnApp = (CnmtChlnApp)cnmtChlnAppList.get(i);
					List cnmtAppList = session.createCriteria(CnmtApp.class)
							.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtChlnApp.getCnmtCode()))
							.add(Restrictions.eq("cnmtIsPending", true))
							.list();
					if(! cnmtAppList.isEmpty()){
						isPending = true;
						pendingCnmtList.add(cnmtChlnApp.getCnmtCode());
					}
				}
				resultMap.put("isPending", isPending);
				resultMap.put("pendingCnmt", pendingCnmtList);
				resultMap.put("cnmtChlnAppList", cnmtChlnAppList);
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> updateChlnApp(Challan challan, List<Map<String, Object>> cnmtList){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			// Find Challan And Challan Detail
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			System.out.println("updateChallan(chlnCode : "+challan.getChlnCode()+")");
			List<ChallanApp> chlnAppList = session.createCriteria(ChallanApp.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, challan.getChlnCode()))
					.list();
			if(! chlnAppList.isEmpty()){
				ChallanApp challanApp = chlnAppList.get(0);				
				String chlnCode = challanApp.getChlnCode();
				Criteria chlnDtApp = session.createCriteria(ChallanDTApp.class);
				chlnDtApp.add(Restrictions.eq("chlnCode", chlnCode));
				List<ChallanDTApp> chlnDtAppList = chlnDtApp.list();
				if(chlnDtAppList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Challan detail not found !");
					return resultMap;
				}
				ChallanDTApp challanDTApp = (ChallanDTApp)chlnDtAppList.get(0);
				String ownCode = challanDTApp.getChdOwnCode();
				String brkCode = challanDTApp.getChdBrkCode();
				String rcNo = challanDTApp.getChdLorryNo();
				
				System.out.println("Owner Code : "+ownCode);
				System.out.println("BrkCode : "+brkCode);
				System.out.println("Rc No : "+rcNo);
				
				if(ownCode == null && brkCode == null){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify owner/broker  OR add owner/broker");
					return resultMap;
				}else if(ownCode == null){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify owner  OR add new owner");					
					return resultMap;
				}else if(brkCode == null){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify broker  OR add new broker");
					return resultMap;
				}else if(ownCode.equalsIgnoreCase("") && brkCode.equalsIgnoreCase("")){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify owner/broker  OR add owner/broker");
					return resultMap;
				}else if(ownCode.equalsIgnoreCase("")){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify owner  OR add new owner");
					return resultMap;
				}else if(brkCode.equalsIgnoreCase("")){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify broker  OR add new broker");
					return resultMap;
				}
				
				Owner owner = (Owner) session.createCriteria(Owner.class)
						.add(Restrictions.eq(OwnerCNTS.OWN_CODE, ownCode))
						.list()
						.get(0);
				Broker broker = (Broker) session.createCriteria(Broker.class)
						.add(Restrictions.eq(BrokerCNTS.BRK_CODE, brkCode))
						.list()
						.get(0);
				
				if(! owner.isOwnIsPanImg() && ! broker.isBrkIsPanImg()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Owner/Broker does not have pan image");
					return resultMap;
				}
				List<VehicleVendorMstr> vvMstrList = session.createCriteria(VehicleVendorMstr.class)
						.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo))
						.list();
				if(vvMstrList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verfiy vehicle OR add new vehicle");
					return resultMap;
				}			
				challanApp.setIsPending(false);
				session.update(challanApp);
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Challan not found !");
				return resultMap;
			}
			transaction.commit();				
		}catch(Exception e){
			System.out.println("Error in updateChallan - ChallanWebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exceptipn !");
		}		
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerListByCnmtList(List<Map<String, Object>> cnmtList){
		List<Customer> customerList = new ArrayList<Customer>();
		List<String> custCodeList = new ArrayList<String>();
		System.out.println("CnmtList Size : "+cnmtList.size());
		try{
			Iterator it = cnmtList.iterator();
			session = this.sessionFactory.openSession();
			while(it.hasNext()){
				Map<String, Object> map = (Map)it.next();
				String cnmt = String.valueOf(map.get("cnmt"));
				System.out.println("Cnmt : "+cnmt);
				Criteria criteria = session.createCriteria(Cnmt.class);
				criteria.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmt));
				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property(CnmtCNTS.CUST_CODE));
				criteria.setProjection(projectionList);
				List list = criteria.list();
				if(! list.isEmpty()){
					custCodeList.add(String.valueOf(list.get(0)));
				}else{
					criteria = session.createCriteria(CnmtApp.class);
					criteria.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmt));
					projectionList = Projections.projectionList();
					projectionList.add(Projections.property("cnmtCustCode"));
					criteria.setProjection(projectionList);
					list = criteria.list();
					if(! list.isEmpty())
						custCodeList.add(String.valueOf(list.get(0)));
				}
			}
			if(custCodeList != null){
				Set<String> remCustCodeList = new HashSet<String>(custCodeList);
				Criteria custCriteria = session.createCriteria(Customer.class);
				it = remCustCodeList.iterator();
				while(it.hasNext()){
					String custCode = String.valueOf(it.next());
					Customer customer = (Customer) custCriteria.add(Restrictions.eq(CustomerCNTS.CUST_CODE, custCode)).list().get(0);
					customerList.add(customer);
				}
			}
		}catch(Exception e){
			System.out.println("Error in getCustomerListByCnmtList - ChallanWebServiceDaoImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return customerList;
	}
	
	private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
	   }
	
	public Map<String, Object> uploadImage(String code,String type, String ownBrkId, String panName,String panNo,String panDOB,String panDate,MultipartFile panImg, MultipartFile decImg, User user){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			java.util.Date panDOBUtil = null;
			java.util.Date panDtUtil = null;
			Date panDOBSQL = null;
			Date panDtSQL = null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try{
				if(panDOB != null){
					if(! panDOB.equalsIgnoreCase("")){
						panDOBUtil = dateFormat.parse(panDOB);
						panDOBSQL = new Date(panDOBUtil.getTime());
					}
				}
				if(panDate != null){
					if(! panDate.equalsIgnoreCase("")){
						panDtUtil = dateFormat.parse(panDate);
						panDtSQL = new Date(panDtUtil.getTime());
					}
				}
			}catch(Exception e){
				logger.error("Error in converting date !");
			}		
			if(code.equalsIgnoreCase("new")){
				Integer id = Integer.parseInt(ownBrkId);
				OwnBrkApp ownBrkApp = (OwnBrkApp) session.get(OwnBrkApp.class, id);				
				List<ImageApp> imageAppList = session.createCriteria(ImageApp.class)
						.add(Restrictions.eq("ownBrkId", id))
						.list();
				// Creating directory if not exists
				try{
					Path path = Paths.get(newImgPath);
					if(! Files.exists(path))
						Files.createDirectories(path);
				}catch(Exception e){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Error in creating directory !");
					logger.error("Exception : "+e);
				}	
				if(panImg != null){
					ownBrkApp.setIsPanImg(true);
					if(! panName.equalsIgnoreCase(""))
						ownBrkApp.setOwnBrkPanName(panName);
					if(! panNo.equalsIgnoreCase(""))
						ownBrkApp.setOwnBrkPanNo(panNo);
					if(panDOBSQL != null)
						ownBrkApp.setOwnBrkPanDOB(panDOBSQL);
					if(panDtSQL != null)
						ownBrkApp.setOwnBrkPanDt(panDtSQL);
					try{						
						String panJPGPath = httpServletRequest.getRealPath("/")+"pan.jpg";						
						String panPDFPath = newImgPath+"/pan"+ownBrkApp.getOwnBrkId()+".pdf";
						File panJPG = new File(panJPGPath);
						File panPDF = new File(panPDFPath);
						File file = new File(httpServletRequest.getRealPath("/")+"pan1.jpg");
						if(panJPG.exists()){}
							panJPG.delete();
						// Converting image to PDF And Write Image
						if(panJPG.createNewFile()){
							FileOutputStream panJPGOut = new FileOutputStream(panJPG);
							panJPGOut.write(panImg.getBytes());
							panJPGOut.close();	
							// Resize Dimension Orignial Image								
							BufferedImage originalImage = ImageIO.read(panJPG);
							int imageType = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
							BufferedImage resizeImageJpg = resizeImage(originalImage, imageType, 600, 850);								
							ImageIO.write(resizeImageJpg, "jpg", file);	
							// Convering Image To PDF
							FileOutputStream panPDFOut = new FileOutputStream(panPDFPath);
							Document document = new Document(PageSize.A4);	
							document.setMargins(0,0,0,0);				
							PdfWriter writer = PdfWriter.getInstance(document, panPDFOut);
						    writer.open();
						    document.open();							    
						    Image img = Image.getInstance(httpServletRequest.getRealPath("/")+"pan1.jpg");							    
						    document.add(img);							    
						    document.close();
						    writer.close();		
						}else{
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Error in uploading pan image !");							
							logger.info("Error in uploading pan image !");
							return resultMap;
						}
						if(panJPG.exists())
							panJPG.delete();
						if(file.exists())
							file.delete();						
					}catch(Exception e){
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
						resultMap.put("msg", "Error in uploading pan image !");						
						logger.error("Exception : "+e);
						return resultMap;
					}
				}
				if(decImg != null){
					ownBrkApp.setIsDecImg(true);
					try{
						String decJPGPath = httpServletRequest.getRealPath("/")+"dec.jpg";
						String decPDFPath = newImgPath+"/dec"+ownBrkApp.getOwnBrkId()+".pdf";							
						File decJPG = new File(decJPGPath);
						File decPDF = new File(decPDFPath);	
						File file = new File(httpServletRequest.getRealPath("/")+"dec1.jpg");
						if(decJPG.exists())
							decJPG.delete();													
						if(decJPG.createNewFile()){
							// Writing Original Image
							FileOutputStream decJPGOut = new FileOutputStream(decJPG);
							decJPGOut.write(decImg.getBytes());
							decJPGOut.close();
							// Resize Dimension Orignial Image								
							BufferedImage originalImage = ImageIO.read(decJPG);
							int imageType = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
							BufferedImage resizeImageJpg = resizeImage(originalImage, imageType, 600, 850);								
							ImageIO.write(resizeImageJpg, "jpg", file);	
							// Convering Image To PDF
							FileOutputStream decPDFOut = new FileOutputStream(decPDFPath);
							Document document = new Document(PageSize.A4);
							document.setMargins(0,0,0,0);								
							PdfWriter writer = PdfWriter.getInstance(document, decPDFOut);
						    writer.open();
						    document.open();							    
						    Image img = Image.getInstance(httpServletRequest.getRealPath("/")+"dec1.jpg");							    
						    document.add(img);							    
						    document.close();
						    writer.close();	
						}else{
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Error in uploading declaration image !");
							logger.info("Error in uploading pan image !");
							return resultMap;
						}
						if(decJPG.exists())
							decJPG.delete();
						if(file.exists())
							file.delete();
					}catch(Exception e){
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
						resultMap.put("msg", "Error in uploading pan image !");
						logger.error("Exception : "+e);
						return resultMap;
					}
				}
				
				if(imageAppList.isEmpty()){
					ImageApp imageApp = new ImageApp();
					imageApp.setOwnBrkId(ownBrkApp.getOwnBrkId());					
					imageApp.setType(ownBrkApp.getType());				
					if(panImg != null)
						imageApp.setPanPath(newImgPath+"/"+"pan"+ownBrkApp.getOwnBrkId()+".pdf");					
					if(decImg != null)
						imageApp.setDecPath(newImgPath+"/"+"dec"+ownBrkApp.getOwnBrkId()+".pdf");					
					Integer imgId = (Integer) session.save(imageApp);
					ownBrkApp.setOwnBrkImgId(imgId);
					session.update(ownBrkApp);
				}else{					
					ImageApp imageApp = (ImageApp)imageAppList.get(0);
					imageApp.setOwnBrkId(ownBrkApp.getOwnBrkId());					
					imageApp.setType(ownBrkApp.getType());
					if(panImg != null)
						imageApp.setPanPath(newImgPath+"/"+"pan"+ownBrkApp.getOwnBrkId()+".pdf");					
					if(decImg != null)
						imageApp.setDecPath(newImgPath+"/"+"dec"+ownBrkApp.getOwnBrkId()+".pdf");				
					session.update(imageApp);
					ownBrkApp.setOwnBrkImgId(imageApp.getId());
					session.update(ownBrkApp);
				}				
			}else if(code.equalsIgnoreCase("old")){			
				if(type.equalsIgnoreCase("owner")){
					Owner owner = (Owner) session.get(Owner.class, Integer.parseInt(ownBrkId));				
					if(panImg != null){
						if(! panName.equalsIgnoreCase(""))
							owner.setOwnPanName(panName);
						if(! panNo.equalsIgnoreCase(""))
							owner.setOwnPanNo(panNo);
						if(panDOBSQL != null)
							owner.setOwnPanDOB(panDOBSQL);
						if(panDtSQL != null)
							owner.setOwnPanDt(panDtSQL);
						owner.setOwnIsPanImg(true);						
						try{
							String panJPGPath = httpServletRequest.getRealPath("/")+"pan.jpg";							
							String panPDFPath = ownerPanImgPath+"/"+owner.getOwnCode()+".pdf";							
							//String panPDFPath = httpServletRequest.getRealPath("/")+owner.getOwnCode()+"Pan.pdf";
							File panJPG = new File(panJPGPath);
							File panPDF = new File(panPDFPath);
							File file = new File(httpServletRequest.getRealPath("/")+"pan1.jpg");
							if(panJPG.exists())
								panJPG.delete();
							// Converting image to PDF And Write Image
							if(panJPG.createNewFile()){
								FileOutputStream panJPGOut = new FileOutputStream(panJPG);
								panJPGOut.write(panImg.getBytes());
								panJPGOut.close();
								// Resize Dimension Orignial Image								
								BufferedImage originalImage = ImageIO.read(panJPG);
								int imageType = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
								BufferedImage resizeImageJpg = resizeImage(originalImage, imageType, 600, 850);								
								ImageIO.write(resizeImageJpg, "jpg", file);
								// Convering Image To PDF
								FileOutputStream panPDFOut = new FileOutputStream(panPDFPath);
								Document document = new Document(PageSize.A4);			
								document.setMargins(0,0,0,0);				
								PdfWriter writer = PdfWriter.getInstance(document, panPDFOut);
							    writer.open();
							    document.open();							    
							    Image img = Image.getInstance(httpServletRequest.getRealPath("/")+"pan1.jpg");							    
							    document.add(img);							    
							    document.close();
							    writer.close();		
							}else{
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
								resultMap.put("msg", "Error in uploading pan image !");
								logger.info("Error in uploading pan image !");
								return resultMap;
							}
							if(panJPG.exists())
								panJPG.delete();
							if(file.exists())
								file.delete();
						}catch(Exception e){
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Error in uploading pan image !");
							logger.error("Exception : "+e);
							return resultMap;
						}
					}
					if(decImg != null){					
						owner.setOwnIsDecImg(true);	
						try{							
							String decJPGPath = httpServletRequest.getRealPath("/")+"dec.jpg";							
							String decPDFPath = ownerDecImgPath+"/"+owner.getOwnCode()+".pdf";
							//String decPDFPath = httpServletRequest.getRealPath("/")+owner.getOwnCode()+"Dec.pdf";
							File decJPG = new File(decJPGPath);
							File decPDF = new File(decPDFPath);							
							File file = new File(httpServletRequest.getRealPath("/")+"dec1.jpg");
							if(decJPG.exists())
								decJPG.delete();													
							if(decJPG.createNewFile()){
								// Writing Original Image
								FileOutputStream decJPGOut = new FileOutputStream(decJPG);
								decJPGOut.write(decImg.getBytes());
								decJPGOut.close();
								// Resize Dimension Orignial Image								
								BufferedImage originalImage = ImageIO.read(decJPG);
								int imageType = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
								BufferedImage resizeImageJpg = resizeImage(originalImage, imageType, 600,850);								
								ImageIO.write(resizeImageJpg, "jpg", file);								
								// Convering Image To PDF
								FileOutputStream decPDFOut = new FileOutputStream(decPDFPath);
								Document document = new Document(PageSize.A4);
								document.setMargins(0,0,0,0);								
								PdfWriter writer = PdfWriter.getInstance(document, decPDFOut);
							    writer.open();
							    document.open();							    
							    Image img = Image.getInstance(httpServletRequest.getRealPath("/")+"dec1.jpg");							    
							    document.add(img);							    
							    document.close();
							    writer.close();							    
							}else{
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
								resultMap.put("msg", "Error in uploading declaration image !");
								logger.info("Error in uploading declaration image !");
								return resultMap;
							}
							if(decJPG.exists())
								decJPG.delete();
							if(file.exists())
								file.delete();
						}catch(Exception e){
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Error in uploading declaration image !");
							logger.error("Exception : "+e);
							return resultMap;
						}
					}				
					Integer ownImgId = owner.getOwnImgId();					
					if(ownImgId != null){
						if(ownImgId > 0){
							OwnerImg ownerImg = (OwnerImg) session.get(OwnerImg.class, ownImgId);
							if(ownerImg != null){
								ownerImg.setUserCode(user.getUserCode());						
								if(panImg != null){
									ownerImg.setOwnPanImgPath(ownerPanImgPath+"/"+owner.getOwnCode()+".pdf");								
								}
								if(decImg != null){							
									ownerImg.setOwnDecImgPath(ownerDecImgPath+"/"+owner.getOwnCode()+".pdf");								
								}
								ownerImg.setOwnId(owner.getOwnId());
								session.update(ownerImg);
								owner.setOwnImgId(ownerImg.getOwnImgId());
								session.update(owner);							
							}
						}else{
							OwnerImg ownerImg = new OwnerImg();
							ownerImg.setbCode(user.getUserBranchCode());
							ownerImg.setUserCode(user.getUserCode());
							ownerImg.setOwnId(owner.getOwnId());
							if(panImg != null)
								ownerImg.setOwnPanImgPath(ownerPanImgPath+"/"+owner.getOwnCode()+".pdf");			
							if(decImg != null)							
								ownerImg.setOwnDecImgPath(ownerDecImgPath+"/"+owner.getOwnCode()+".pdf");
							Integer imgId = (Integer)session.save(ownerImg);
							owner.setOwnImgId(imgId);
							session.update(owner);
						}					
					}else{
						OwnerImg ownerImg = new OwnerImg();
						ownerImg.setbCode(user.getUserBranchCode());
						ownerImg.setUserCode(user.getUserCode());
						ownerImg.setOwnId(owner.getOwnId());
						if(panImg != null)
							ownerImg.setOwnPanImgPath(ownerPanImgPath+"/"+owner.getOwnCode()+".pdf");			
						if(decImg != null)							
							ownerImg.setOwnDecImgPath(ownerDecImgPath+"/"+owner.getOwnCode()+".pdf");
						Integer imgId = (Integer)session.save(ownerImg);
						owner.setOwnImgId(imgId);
						session.update(owner);
					}
				}else if(type.equalsIgnoreCase("broker")){
					Broker broker = (Broker) session.get(Broker.class, Integer.parseInt(ownBrkId));
					System.out.println("Broker ID : "+broker.getBrkId());					
					if(panImg != null){						
						if(! panName.equalsIgnoreCase(""))
							broker.setBrkPanName(panName);
						if(! panNo.equalsIgnoreCase(""))
							broker.setBrkPanNo(panNo);
						if(panDOBSQL != null)
							broker.setBrkPanDOB(panDOBSQL);
						if(panDtSQL != null)
							broker.setBrkPanDt(panDtSQL);						
						broker.setBrkIsPanImg(true);						
						try{
							String panJPGPath = httpServletRequest.getRealPath("/")+"pan.jpg";
							String panPDFPath = brokerPanImgPath+"/"+broker.getBrkCode()+".pdf";	
							File panJPG = new File(panJPGPath);
							File panPDF = new File(panPDFPath);
							File file = new File(httpServletRequest.getRealPath("/")+"pan1.jpg");
							if(panJPG.exists())
								panJPG.delete();
							// Converting image to PDF And Write Image
							if(panJPG.createNewFile()){
								FileOutputStream panJPGOut = new FileOutputStream(panJPG);
								panJPGOut.write(panImg.getBytes());
								panJPGOut.close();	
								// Resize Dimension Orignial Image								
								BufferedImage originalImage = ImageIO.read(panJPG);
								int imageType = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
								BufferedImage resizeImageJpg = resizeImage(originalImage, imageType, 600, 850);								
								ImageIO.write(resizeImageJpg, "jpg", file);	
								// Convering Image To PDF
								FileOutputStream panPDFOut = new FileOutputStream(panPDFPath);
								Document document = new Document(PageSize.A4);	
								document.setMargins(0,0,0,0);				
								PdfWriter writer = PdfWriter.getInstance(document, panPDFOut);
							    writer.open();
							    document.open();							    
							    Image img = Image.getInstance(httpServletRequest.getRealPath("/")+"pan1.jpg");							    
							    document.add(img);							    
							    document.close();
							    writer.close();		
							}else{
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
								resultMap.put("msg", "Error in uploading pan image !");
								logger.info("Error in uploading pan image !");
								return resultMap;
							}
							if(panJPG.exists())
								panJPG.delete();
							if(file.exists())
								file.delete();
						}catch(Exception e){
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Error in uploading pan image !");
							logger.error("Exception : "+e);
							return resultMap;
						}
					}
					if(decImg != null){
						broker.setBrkIsDecImg(true);
						try{							
							String decJPGPath = httpServletRequest.getRealPath("/")+"dec.jpg";
							String decPDFPath = brokerDecImgPath+"/"+broker.getBrkCode()+".pdf";														
							File decJPG = new File(decJPGPath);
							File decPDF = new File(decPDFPath);	
							File file = new File(httpServletRequest.getRealPath("/")+"dec1.jpg");
							if(decJPG.exists())
								decJPG.delete();													
							if(decJPG.createNewFile()){
								// Writing Original Image
								FileOutputStream decJPGOut = new FileOutputStream(decJPG);
								decJPGOut.write(decImg.getBytes());
								decJPGOut.close();	
								// Resize Dimension Orignial Image								
								BufferedImage originalImage = ImageIO.read(decJPG);
								int imageType = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
								BufferedImage resizeImageJpg = resizeImage(originalImage, imageType, 600, 850);								
								ImageIO.write(resizeImageJpg, "jpg", file);	
								// Convering Image To PDF
								FileOutputStream decPDFOut = new FileOutputStream(decPDFPath);
								Document document = new Document(PageSize.A4);
								document.setMargins(0,0,0,0);								
								PdfWriter writer = PdfWriter.getInstance(document, decPDFOut);
							    writer.open();
							    document.open();							    
							    Image img = Image.getInstance(httpServletRequest.getRealPath("/")+"dec1.jpg");							    
							    document.add(img);							    
							    document.close();
							    writer.close();							    
							}else{
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
								resultMap.put("msg", "Error in uploading declaration image !");
								logger.info("Error in uploading declaration image !");
								return resultMap;
							}	
							if(decJPG.exists())
								decJPG.delete();
							if(file.exists())
								file.delete();
						}catch(Exception e){
							resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
							resultMap.put("msg", "Error in uploading declaration image !");
							logger.error("Exception : "+e);
							return resultMap;
						}
					}					
					Integer brkImgId = broker.getBrkImgId();
					if(brkImgId != null){
						if(brkImgId > 0){
							BrokerImg brkImg = (BrokerImg) session.get(BrokerImg.class, brkImgId);
							if(brkImg != null){
								brkImg.setUserCode(user.getUserCode());					
								if(panImg != null){
									brkImg.setBrkPanImgPath(brokerPanImgPath+"/"+broker.getBrkCode()+".pdf");			
								}
								if(decImg != null){							
									brkImg.setBrkDecImgPath(brokerDecImgPath+"/"+broker.getBrkCode()+".pdf");
								}
								brkImg.setBrkId(broker.getBrkId());								
								session.update(brkImg);
								broker.setBrkImgId(brkImg.getBrkImgId());
								session.update(broker);
							}
						}else{
							BrokerImg brokerImg = new BrokerImg();
							brokerImg.setbCode(user.getUserBranchCode());
							brokerImg.setUserCode(user.getUserCode());							
							brokerImg.setBrkId(broker.getBrkId());
							if(panImg != null)
								brokerImg.setBrkPanImgPath(brokerPanImgPath+"/"+broker.getBrkCode()+".pdf");			
							if(decImg != null)							
								brokerImg.setBrkDecImgPath(brokerDecImgPath+"/"+broker.getBrkCode()+".pdf");
							Integer imgId = (Integer)session.save(brokerImg);
							broker.setBrkImgId(imgId);
							session.update(broker);
						}			
					}else{
						BrokerImg brokerImg = new BrokerImg();
						brokerImg.setbCode(user.getUserBranchCode());
						brokerImg.setUserCode(user.getUserCode());							
						brokerImg.setBrkId(broker.getBrkId());
						if(panImg != null)
							brokerImg.setBrkPanImgPath(brokerPanImgPath+"/"+broker.getBrkCode()+".pdf");			
						if(decImg != null)							
							brokerImg.setBrkDecImgPath(brokerDecImgPath+"/"+broker.getBrkCode()+".pdf");
						Integer imgId = (Integer)session.save(brokerImg);
						broker.setBrkImgId(imgId);
						session.update(broker);
					}
				}
			}
			
			transaction.commit();
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			resultMap.put("msg", "Pan/Declaration image are successfully uploaded !");
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	public Boolean isChlnAvailable(String chlnCode){
		logger.info("Enter into isChlnAvailable()...");
		Boolean available = false;
		try{
			session = this.sessionFactory.openSession();
			List<Challan> chlnList = session.createCriteria(Challan.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
					.list();
			if(chlnList.isEmpty()){
				List<ChallanApp> chlnAppList = session.createCriteria(ChallanApp.class)
						.add(Restrictions.eq(ChallanAppCNTS.CHLN_CODE, chlnCode))
						.list();
				if(chlnAppList.isEmpty()){
					available = true;
					logger.info("Challan does not exist !");
				}else{
					available = false;
					logger.info("Challan already exist !");
				}
			}else{
				available = false;
				logger.info("Challan already exist !");
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from isChlnAvailable()...");
		return available;
	}
	
}