'use strict';

var app = angular.module('application');

app.controller('CancelChallanCntlr',['$scope','$location','$http',
                               function($scope,$location,$http){
	
	$scope.chlnCode="";
	$scope.lorryNo="";
	$scope.chlnDt="";
	$scope.cancelChallan=function(){
		console.log("cancelChallan()");
		console.log("chlnDt"+$scope.chlnDt);
		var chlnDtl={
				"chlnCode" : $scope.chlnCode,
				"lorryNo" : $scope.lorryNo,
				"date" : $scope.chlnDt
		}
		
		  var response = $http.post($scope.projectName+'/chlnDtlFrCncl',chlnDtl);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.alertToast("challan successfully canceled");
			  }else{
				  $scope.alertToast("Detail not Match");
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	}
		
		
	
	
}]);