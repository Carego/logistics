package com.mylogistics.controller;

import java.text.SimpleDateFormat;
import java.net.Inet4Address;
//import java.util.Date;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;
import javax.validation.Constraint;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.log.SysoCounter;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.ChCnSeDetailDAO;
import com.mylogistics.DAO.HBO_StnNotificationDAO;
import com.mylogistics.DAO.HOSLastDAO;
import com.mylogistics.DAO.MasterStnStkDao;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.StationaryDAO;
import com.mylogistics.DAO.StationarySupplierDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.HBO_StnNotification;
import com.mylogistics.model.HOSLast;
import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.Stationary;
import com.mylogistics.model.StationarySupplier;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.StationaryService;
import com.mylogistics.services.StnTransferService;
import com.mylogistics.services.StnTransferService.DoTransfer;
import com.mylogistics.services.StnTransferService.IsAvailable;
import com.mylogistics.services.ValidatorFactoryUtil;

@Controller
public class StationaryCntlr{
	
	@Autowired
	private ChCnSeDetailDAO chCnSeDAO;
	
	@Autowired
	private StationaryDAO stationaryDAO;
	
	@Autowired
	private StationarySupplierDAO stationarySupplierDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
    private HOSLastDAO hOSLastDAO;
	
	@Autowired
    private BranchMUStatsDAO branchMUStatsDAO;

	@Autowired
    private HBO_StnNotificationDAO 	hBO_StnNotificationDAO;
	
	@Autowired
	private BranchSStatsDAO branchSStatsDAO;
	
	@Autowired
	private MasterStnStkDao masterStnStkDao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private BranchDAO branchDAO;
	
	public static Logger logger = Logger.getLogger(StationaryCntlr.class);	

	@RequestMapping(value="/saveStationaryOrder",method=RequestMethod.POST)
	public @ResponseBody Object saveStationaryOrder(@RequestBody StationaryService stationaryService) {
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Stationary stationary = stationaryService.getStationary();
		HBO_StnNotification hBO_StnNotification = stationaryService.gethBO_StnNotification();
		try {
			Date date = stationary.getStOdrDt();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			String formattedDate = formatter.format(date);
			System.out.println("date is"+formattedDate);
			System.out.println(formattedDate);
			
			String dateParts[] = formattedDate.split("/");
			String month=dateParts[1];
			String year = dateParts[0];
			System.out.println("year"+year);
			System.out.println("month"+month);
			
			String stOdrCode = null;
			String last = null;
			long totalRows = stationaryDAO.totalCount();
			
			if (!(totalRows == -1)) {
				
				if(totalRows==0){
					stOdrCode = year+month+"0000001";
					System.out.println("The generated code for rows 0 is"+stOdrCode);
				}else{
					long id = stationaryDAO.getLastStationaryId();
					System.out.println("st id for stCode = "+id);
					long id1 =id+1;
					long end = 10000000+id1;
					last = String.valueOf(end).substring(1,8);
					stOdrCode = year+month+last;
					System.out.println("***************new stOdrCode = "+stOdrCode);
					System.out.println("The generated code for  is"+stOdrCode);
				}
				
				stationary.setUserCode(currentUser.getUserCode());
				stationary.setbCode(currentUser.getUserBranchCode());
				stationary.setStCode(stOdrCode);
				stationary.setStIsRec("no");
				int saveStatusOfSO=stationaryDAO.saveStationary(stationary);
				System.out.println("saveStatusOfSO = "+saveStatusOfSO);
				if(saveStatusOfSO > 0){
					hBO_StnNotification.sethBOSN_stnId(saveStatusOfSO);
					int res = hBO_StnNotificationDAO.updateIsCreate(hBO_StnNotification);
					if(res > 0){
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						map.put("stationary",stationary);
						//stationaryRecService.setStationary(stationary);
					}	
				}else {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}

			int nobCnmt = stationary.getStOdrCnmt();
			int nobChln = stationary.getStOdrChln();
			int nobSedr = stationary.getStOdrSedr();
			
/*			if (nobCnmt>0) {
				System.out.println("+++++++++++++++++nobCnmt if+++++++++++++++++");
				ChCnSeDetail chCnSeDetail  = new ChCnSeDetail();
				String chCnSeCode = null;
				
				chCnSeDetail.setChCnSeNob(nobCnmt);
				chCnSeDetail.setChCnSeStOdrCode(stOdrCode);
				chCnSeDetail.setChCnSeStatus("cnmt");
				chCnSeDetail.setUserCode(currentUser.getUserCode());
				chCnSeDetail.setbCode(currentUser.getUserBranchCode());
				
				List<ChCnSeDetail> cnmtList= chCnSeDAO.getLastStationary("cnmt");
				
				int cnmtSize = cnmtList.size();
				
				if(cnmtSize==0){
					int endNo=50*nobCnmt;
					int endNo1=endNo+10000000;
					String strEndNo  = Integer.toString(endNo1);
					String substrEndNo = strEndNo.substring(1, 8);
					System.out.println("The End Number for CNMT in if case is------<<>>>---"+substrEndNo);
					chCnSeDetail.setChCnSeStartNo("0000001");
					chCnSeDetail.setChCnSeEndNo(substrEndNo);
					chCnSeCode = "cn"+"0000000";
					chCnSeDetail.setChCnSeCode(chCnSeCode);
				}else{
					String startNo =String.valueOf((Integer.parseInt(cnmtList.get(0).getChCnSeEndNo())+1));
					startNo = String.valueOf((Integer.parseInt(startNo)+10000000)).substring(1, 8);
					int startNoInt = Integer.parseInt(cnmtList.get(0).getChCnSeEndNo());
					System.out.println("Start number int is-----"+startNoInt);
					int endNo=50*nobCnmt;
					int endNo1=startNoInt+endNo+10000000;
					String strEndNo = String.valueOf(endNo1).substring(1, 8);
					System.out.println("The End Number for CNMT in else case is------<<>>>---"+strEndNo);
					chCnSeDetail.setChCnSeStartNo(startNo);
					chCnSeDetail.setChCnSeEndNo(strEndNo);
					chCnSeCode="cn"+startNo;
					chCnSeDetail.setChCnSeCode(chCnSeCode);
				}
				
				int temp=chCnSeDAO.saveChCnSE(chCnSeDetail);
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Cnmt",ConstantsValues.SUCCESS+"Cnmt");
					map.put("cnmtSeq",chCnSeDetail.getChCnSeStartNo());
					stationaryRecService.setCnmtSeq(chCnSeDetail.getChCnSeStartNo());
				}else{
					map.put(ConstantsValues.RESULT+"Cnmt",ConstantsValues.ERROR+"Cnmt");
				}
			}else {
				map.put(ConstantsValues.RESULT+"Cnmt",ConstantsValues.ERROR+"Cnmt");
				System.out.println("+++++++++++++++++nobCnmt else+++++++++++++++++");
			}
			
			if (nobChln>0) {
				System.out.println("+++++++++++++++++nobChln if+++++++++++++++++");
				ChCnSeDetail chCnSeDetail  = new ChCnSeDetail();
				
				String chCnSeCode = null;
				
				chCnSeDetail.setChCnSeNob(nobChln);
				chCnSeDetail.setChCnSeStOdrCode(stOdrCode);
				chCnSeDetail.setChCnSeStatus("chln");
				chCnSeDetail.setUserCode(currentUser.getUserCode());
				chCnSeDetail.setbCode(currentUser.getUserBranchCode());
				
				List<ChCnSeDetail> chlnList= chCnSeDAO.getLastStationary("chln");
				int chlnSize = chlnList.size();
				
				if(chlnSize==0){
					int endNo=50*nobChln;
					int endNo1=endNo+10000000;
					String strEndNo  = Integer.toString(endNo1);
					String substrEndNo = strEndNo.substring(1, 8);
					System.out.println("The End Number for CHLn in if case is------<<>>>---"+substrEndNo);
					chCnSeDetail.setChCnSeStartNo("0000001");
					chCnSeDetail.setChCnSeEndNo(substrEndNo);
					chCnSeCode = "ch"+"0000000";
					chCnSeDetail.setChCnSeCode(chCnSeCode);		
				}
				else{
					String startNo = String.valueOf((Integer.parseInt(chlnList.get(0).getChCnSeEndNo())+1));
					startNo = String.valueOf((Integer.parseInt(startNo)+10000000)).substring(1, 8);
					int startNoInt = Integer.parseInt(chlnList.get(0).getChCnSeEndNo());
					int endNo = 50*nobChln;
					int endNo1=startNoInt+endNo+10000000;
					String strEndNo = String.valueOf(endNo1).substring(1, 8);
					System.out.println("The End Number for CHLN in else case is------<<>>>---"+strEndNo);
					chCnSeDetail.setChCnSeStartNo(startNo);
					chCnSeDetail.setChCnSeEndNo(strEndNo);
					chCnSeCode ="ch"+startNo; 
					chCnSeDetail.setChCnSeCode(chCnSeCode);	
				}
				int temp=chCnSeDAO.saveChCnSE(chCnSeDetail);
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Chln",ConstantsValues.SUCCESS+"Chln");
					map.put("chlnSeq",chCnSeDetail.getChCnSeStartNo());
					stationaryRecService.setChlnSeq(chCnSeDetail.getChCnSeStartNo());
				}else{
					map.put(ConstantsValues.RESULT+"Chln",ConstantsValues.ERROR+"Chln");
				}
			}else {
				map.put(ConstantsValues.RESULT+"Chln",ConstantsValues.ERROR+"Chln");
				System.out.println("+++++++++++++++++nobChln else+++++++++++++++++");
			}
			
			if (nobSedr>0) {
				System.out.println("+++++++++++++++++nobSedr if+++++++++++++++++");
				ChCnSeDetail chCnSeDetail  = new ChCnSeDetail();
				String chCnSeCode = null;
				
				chCnSeDetail.setChCnSeNob(nobSedr);
				chCnSeDetail.setChCnSeStOdrCode(stOdrCode);
				chCnSeDetail.setChCnSeStatus("sedr");
				chCnSeDetail.setUserCode(currentUser.getUserCode());
				chCnSeDetail.setbCode(currentUser.getUserBranchCode());
				
				List<ChCnSeDetail> sedrList= chCnSeDAO.getLastStationary("sedr");
				
				int sedrSize=sedrList.size();
				
				if(sedrSize==0){
					int endNo=50*nobSedr;
					int endNo1=endNo+10000000;
					String strEndNo  = Integer.toString(endNo1);
					String substrEndNo = strEndNo.substring(1, 8);
					System.out.println("The End Number for SEDR in if case is------<<>>>---"+substrEndNo);
					chCnSeDetail.setChCnSeStartNo("0000001");
					chCnSeDetail.setChCnSeEndNo(substrEndNo);
					chCnSeCode="se"+"0000000";
					chCnSeDetail.setChCnSeCode(chCnSeCode);
				}
				else {
					String startNo = String.valueOf((Integer.parseInt(sedrList.get(0).getChCnSeEndNo())+1));
					startNo = String.valueOf((Integer.parseInt(startNo)+10000000)).substring(1, 8);
					int startNoInt = Integer.parseInt(sedrList.get(0).getChCnSeEndNo());
					int endNo = 50*nobSedr;
					int endNo1=startNoInt+endNo+10000000;
					String strEndNo = String.valueOf(endNo1).substring(1, 8);
					System.out.println("The End Number for SEDR in else case is------<<>>>---"+strEndNo);
					chCnSeDetail.setChCnSeStartNo(startNo);
					chCnSeDetail.setChCnSeEndNo(strEndNo);
					chCnSeCode="se"+startNo;
					chCnSeDetail.setChCnSeCode(chCnSeCode);
				}
				int temp=chCnSeDAO.saveChCnSE(chCnSeDetail);
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Sedr",ConstantsValues.SUCCESS+"Sedr");
					map.put("sedrSeq",chCnSeDetail.getChCnSeStartNo());
					stationaryRecService.setSedrSeq(chCnSeDetail.getChCnSeStartNo());
				}else{
					map.put(ConstantsValues.RESULT+"Sedr",ConstantsValues.ERROR+"Sedr");
				}
			}else {
				map.put(ConstantsValues.RESULT+"Sedr",ConstantsValues.ERROR+"Sedr");
				System.out.println("+++++++++++++++++nobSedr else+++++++++++++++++");
			}*/
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT,"Enter Date");
			e.printStackTrace();
		}
		
		
		
		
		return map;
	}	
	
	@RequestMapping(value = "/getStationarySupplierCode", method = RequestMethod.POST)
	public @ResponseBody Object getStationarySupplierCode() {
		System.out.println("enter into getStationarySupplierCode function");
		List<StationarySupplier> list = stationarySupplierDAO.getStationarySupplierCode();
		Map<String, Object> map = new HashMap<String, Object>();
		if(!list.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", list);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	
	@RequestMapping(value = "/recStationary", method = RequestMethod.POST)
	public @ResponseBody Object recStationary(@RequestBody Stationary stationary){
		logger.info("Enter into /recStationary() ");
		Map<String, Object> map = new HashMap<String, Object>();		
		try{
			User currentUser = (User)httpSession.getAttribute("currentUser");
			int temp = stationaryDAO.updateStationary(stationary);
			if(temp > 0){
				List<HOSLast> list = hOSLastDAO.getLastArrival();
				HOSLast hOSLast = list.get(0);
				int lastCnmt = hOSLast.gethOSCnmt();
				int lastChln = hOSLast.gethOSChln();
				int lastSedr = hOSLast.gethOSSedr();
				
				double cnmtDaysLast = 0;
				double chlnDaysLast = 0;
				double sedrDaysLast = 0;
				
				lastCnmt = lastCnmt + stationary.getStOdrCnmt();
				lastChln = lastChln + stationary.getStOdrChln();
				lastSedr = lastSedr + stationary.getStOdrSedr();
				
				if(lastCnmt >= 0){
					double monUsgCNMT = getAvgMonUsgCNMT();
					System.out.println("monUsgCNMT---->"+monUsgCNMT);
					cnmtDaysLast = (30/monUsgCNMT)*lastCnmt;
				}
				
				if(lastChln >= 0){
					double monUsgCHLN = getAvgMonUsgCHLN();
					System.out.println("monUsgCHLN---->"+monUsgCHLN);
					chlnDaysLast = (30/monUsgCHLN)*lastChln;
				}
				
				if(lastChln >= 0){
					double monUsgSEDR = getAvgMonUsgSEDR();
					System.out.println("monUsgSEDR---->"+monUsgSEDR);
					sedrDaysLast = (30/monUsgSEDR)*lastSedr;
				}
				
				HOSLast ho = new HOSLast();
				
				
				ho.setbCode(currentUser.getUserBranchCode());
				ho.sethOSChln(lastChln);
				ho.sethOSChlnDaysLast((int)chlnDaysLast);
				ho.sethOSChlnDisOrRec(stationary.getStOdrChln());
				ho.sethOSCnmt(lastCnmt);
				ho.sethOSCnmtDaysLast((int)cnmtDaysLast);
				ho.sethOSCnmtDisOrRec(stationary.getStOdrCnmt());
				ho.sethOSDate(new Date(new java.util.Date().getTime()));
				ho.sethOSSedr(lastSedr);
				ho.sethOSSedrDaysLast((int)sedrDaysLast);
				ho.sethOSSedrDisOrRec(stationary.getStOdrSedr());
				ho.sethOSType("receive");
				ho.setUserCode(currentUser.getUserCode());
				
				int result = hOSLastDAO.saveHosLast(ho);
				if(result > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("message","Stationary doesn't save in HOSLast");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getStnRecService", method = RequestMethod.POST)
	public @ResponseBody Object getStnRecService(@RequestBody Map<String,Object> clientMap) {
		logger.info("Enter into /getStnRecService() : ID = "+clientMap.get("stnId"));
		Map<String, Object> map = new HashMap<String, Object>();
		int stnId = (int) clientMap.get("stnId");
		if(stnId > 0){
			Stationary stationary = stationaryDAO.getStnById(stnId);
			map.put("stn", stationary);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			logger.info("Result : "+ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			logger.info("Result : "+ConstantsValues.ERROR);
		}
		logger.info("Exit from /getStnRecService()");
		return map;
	}
	
	
	public double getAvgMonUsgCNMT(){
		System.out.println("enter into getAvgMonUsgCNMT function");
		double avgMonUsgCNMT = 0;
		avgMonUsgCNMT = branchMUStatsDAO.getAvgMonUsg("cnmt");
		return avgMonUsgCNMT;
	}
	
	public double getAvgMonUsgCHLN(){
		System.out.println("enter into getAvgMonUsgCNMT function");
		double avgMonUsgCHLN = 0;
		avgMonUsgCHLN = branchMUStatsDAO.getAvgMonUsg("chln");
		return avgMonUsgCHLN;
	}
	
	public double getAvgMonUsgSEDR(){
		System.out.println("enter into getAvgMonUsgSEDR function");
		double avgMonUsgSEDR = 0;
		avgMonUsgSEDR = branchMUStatsDAO.getAvgMonUsg("sedr");
		return avgMonUsgSEDR;
	}
	
	@RequestMapping(value="/saveCustStnry", method=RequestMethod.POST)
	public @ResponseBody Object saveCustStnry(@RequestBody Map<String, Object> custStnryService){
		System.out.println("StationaryCntlr.saveCustStnry()");
		Map<String, Object> map = new HashMap<>();
		
		try {
			if (Integer.parseInt(String.valueOf(custStnryService.get("frmStnryNo")))>Integer.parseInt(String.valueOf(custStnryService.get("toStnryNo")))) {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				map.put("msg", "please enter valid Stationary no");
				return map;
			} else {
				int temp = stationaryDAO.saveCustStnry(custStnryService);
				if (temp>0) {
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				} else if (temp == -2) {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					map.put("msg", "Either some Or all stationary already entered");
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "please enter valid Stationary no");
			return map;
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getBrhStkDetail", method = RequestMethod.POST)
	public @ResponseBody Object getBrhStkDetail(@RequestBody String brhCode){
		logger.info("Enter into /getBrhStkDetail() : BranchCode = "+brhCode);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> stkEnqList = stationaryDAO.getBrhStkDetail(brhCode);
		if(stkEnqList.isEmpty()){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such record found !");
		}else{
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("stkEnqList", stkEnqList);
		}
		logger.info("Exit from /getBrhStkDetail()");
		return resultMap;
	}
	
	@RequestMapping(value = "/fetchStnaryDetail", method = RequestMethod.POST)
	public @ResponseBody Object fetchStnaryDetail(@RequestBody Map<String, String> initParam){
		logger.info("Enter into /fetchStnaryDetail()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> usedUnusedList = stationaryDAO.fetchStnaryDetail(initParam);
		if(usedUnusedList.isEmpty()){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such record found !");
		}else{
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("usedUnusedList", usedUnusedList);
		}
		logger.info("Exit from /fetchStnaryDetail()");
		return resultMap;
	}
	
	@RequestMapping(value = "/getMstrStock", method = RequestMethod.POST)
	public @ResponseBody Object getMstrStock(){
		logger.info("Enter into /getMstrStock()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{			
			if(session == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				logger.info("Session is not initialized !");
			}else{
				List<Map<String, Object>> masterStk =	stationaryDAO.getMstrStock(session);		
				if(masterStk.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("masterStk", masterStk);
				}
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
			logger.error("Error : "+e);
		}finally{
			if(session != null){
				session.flush();
				session.clear();
				session.close();
			}
		}		
		logger.info("Exit from /getMstrStock()");
		return resultMap;
	}
	
	@RequestMapping(value = "/findByStk", method = RequestMethod.POST)
	public @ResponseBody Object findByStk(@RequestBody Map<String, String> initParam){
		logger.info("Enter into /findByStk()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{			
			if(session == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				logger.info("Session is not initialized !");
			}else{
				List<Map<String, Object>> byStkList =	stationaryDAO.findByStk(session, initParam);		
				if(byStkList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("byStkList", byStkList);
				}
			}
		}catch(Exception e){
			logger.error("Error : "+e);
		}finally{
			if(session != null){
				session.flush();
				session.clear();
				session.close();
			}
		}		
		logger.info("Exit from /findByStk()");
		return resultMap;
	}
	
	@RequestMapping(value = "/isStnAvailable", method = RequestMethod.POST)
	public @ResponseBody Object isStnAvailable(@RequestBody StnTransferService stnService){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		User user = (User) httpSession.getAttribute("currentUser");
		logger.error("Enter into /isStnAvailable : UserID = "+user.getUserId()+" : start "+stnService.getStartList()+" : type "+stnService.getTransferType());
		Session session = this.sessionFactory.openSession();
		try{				
			ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<StnTransferService>> validationMsg = validator.validate(stnService, IsAvailable.class);
			if(! validationMsg.isEmpty()){
				List<String> valMsg = new ArrayList<String>();
				Iterator<ConstraintViolation<StnTransferService>> it = validationMsg.iterator();				
				while(it.hasNext()){
					ConstraintViolation<StnTransferService> msg = it.next();
					valMsg.add(msg.getMessage());
				}
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", valMsg);
			}else{			
				resultMap = stationaryDAO.isStnAvailable(session, user, stnService);
				System.err.println("Is Available : "+resultMap);
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}finally{
			session.flush();
			session.clear();
			session.close();
		}
		logger.info("Exit from /isStnAvailable : UserID = "+user.getUserId()+" : Result ="+resultMap);
		return resultMap;
	}	
	
	@RequestMapping(value = "/stnTransferToBrh", method = RequestMethod.POST)
	public @ResponseBody Object stnTransferToBrh(@RequestBody StnTransferService stnTransferService){		
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("Enter into stnTransferToBrh() : UserID = "+user.getUserId());
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> errMsg = new ArrayList<String>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			// Do Validation
			Validator validator = ValidatorFactoryUtil.getValidator();
			Set<ConstraintViolation<StnTransferService>> valdErr = validator.validate(stnTransferService, DoTransfer.class);
			//	IF object has some validation error
			if(! valdErr.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				Iterator<ConstraintViolation<StnTransferService>> it = valdErr.iterator();
				while(it.hasNext()){
					ConstraintViolation<StnTransferService> err = it.next();
					errMsg.add(err.getMessage());
				}
				resultMap.put("msg", errMsg);
			}else{
				//	Check Availability
				resultMap = stationaryDAO.isStnAvailable(session, user, stnTransferService);
				if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){
					resultMap = stationaryDAO.doStnTransfer(session, user, stnTransferService);
				}
			}	
			if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){
				System.err.println("Sucessss........");
				transaction.commit();
			}else{
				System.err.println("Error........");
				transaction.rollback();
			}
		}catch(Exception e){
			transaction.rollback();
			logger.error("Exception : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from stnTransferToBrh()");
		return resultMap;
	}
	
	@RequestMapping(value = "/getMissingReport", method = RequestMethod.POST)
	public @ResponseBody Object getMissingReport(@RequestBody Map<String, String> initParam){		
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("Enter into getMissingReport() : UserID = "+user.getUserId()+" : InitParam = "+initParam);		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> missStn = new ArrayList();
		
		Session session = this.sessionFactory.openSession();
		try{
			String branchCode = initParam.get("branchCode");
			String fromDate = initParam.get("dateFrom");
			String toDate = initParam.get("dateTo");
			String stnType = initParam.get("stnType");
			
			if(branchCode == null)
				branchCode = "";
			if(fromDate == null)
				fromDate = "";
			if(toDate == null)
				toDate = "";
			if(stnType == null)
				stnType = "";
			
			if(!stnType.equalsIgnoreCase("") && !branchCode.equalsIgnoreCase("")){
				List<String> tempMissList = findMissing(session, user, branchCode, fromDate, toDate, stnType);
				if(! tempMissList.isEmpty()){
					resultMap.put(stnType, tempMissList);
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}
			}else if(!stnType.equalsIgnoreCase("")){
				List<Branch> branchList = branchDAO.getAllBranch();
				List<String> temp = new ArrayList<String>();
				for(int i=0; i<branchList.size(); i++){					
					temp.addAll(findMissing(session, user, branchList.get(i).getBranchCode(), fromDate, toDate, stnType));
				}
				if(! temp.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put(stnType, temp);					
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}
			}else if(!branchCode.equalsIgnoreCase("")){
				List<String> tempMissList = findMissing(session, user, branchCode, fromDate, toDate, "cnmt");
				if(! tempMissList.isEmpty())
					resultMap.put("cnmt", tempMissList);
				tempMissList = findMissing(session, user, branchCode, fromDate, toDate, "chln");
				if(! tempMissList.isEmpty())
					resultMap.put("chln", tempMissList);
				tempMissList = findMissing(session, user, branchCode, fromDate, toDate, "sedr");
				if(! tempMissList.isEmpty())
					resultMap.put("sedr", tempMissList);
				
				if(resultMap.get("cnmt") == null && resultMap.get("chln") == null && resultMap.get("sedr") == null){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}
			}else{
				List<Branch> branchList = branchDAO.getAllBranch();
				
				List<String> cnmt = new ArrayList<String>();
				List<String> chln = new ArrayList<String>();
				List<String> sedr = new ArrayList<String>();
				
				for(int i=0; i<branchList.size(); i++){					
					cnmt.addAll(findMissing(session, user, branchList.get(i).getBranchCode(), fromDate, toDate, "cnmt"));
					chln.addAll(findMissing(session, user, branchList.get(i).getBranchCode(), fromDate, toDate, "chln"));
					sedr.addAll(findMissing(session, user, branchList.get(i).getBranchCode(), fromDate, toDate, "sedr"));
				}
				
				if(! cnmt.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("cnmt", cnmt);
				}
				
				if(! chln.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("chln", chln);
				}
				
				if(! sedr.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("sedr", sedr);
				}
				
			}
		}catch(Exception e){	
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
			logger.error("Exception : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from getMissingReport()");
		return resultMap;
	}
	
	public List<String> findMissing(Session session, User user, String branchCode, String fromDate, String toDate, String stnType){
		List<String> missStn = new ArrayList<String>();
		
		// Branch Barbil - 23,
		if(branchCode.equalsIgnoreCase("23")){
			// Find Min & Max stationary no. used 
			List<Map<String, String>> minMaxList = branchSStatsDAO.getMinMax(session, user, branchCode, fromDate, toDate, stnType, "yes");
			
			String minNo = String.valueOf(minMaxList.get(0).get("minNo"));
			String maxNo = String.valueOf(minMaxList.get(0).get("maxNo"));
			
			System.out.println("Min NO = "+minNo);
			System.out.println("Max NO = "+maxNo);
			
			// Creating Start Book and End Book Form min and max
			if(minNo != null && maxNo != null &&  !minNo.equalsIgnoreCase("null") && !maxNo.equalsIgnoreCase("null")){								
				if(minNo.length() > 1){
					if(minNo.substring(minNo.length()-2, minNo.length()).equalsIgnoreCase("00"))
						minNo = String.valueOf(Integer.parseInt(minNo) - 49);
					else{
						if(Integer.parseInt(minNo.substring(minNo.length()-2, minNo.length())) <= 50)
							minNo = minNo.substring(0, minNo.length()-2)+"01";
						else
							minNo = minNo.substring(0, minNo.length()-2)+"51";
					}
				}else
					minNo = "1";
				
				if(maxNo.length() > 1){
					if(maxNo.substring(maxNo.length()-2, maxNo.length()).equalsIgnoreCase("00")){
						maxNo = String.valueOf(Integer.parseInt(maxNo) - 49);
					}else{
						if(Integer.parseInt(maxNo.substring(maxNo.length()-2, maxNo.length())) <= 50)
							maxNo = maxNo.substring(0, maxNo.length()-2)+"01";
						else
							maxNo = maxNo.substring(0, maxNo.length()-2)+"51";
					}
				}else
					maxNo = "50";
			}
			// Print Start Book and End Book
			System.out.println("Start Book No = "+minNo);
			System.out.println("End Book No = "+maxNo);
			
			// Loop will work arrording to Book No.
			for(int i=Integer.parseInt(minNo); i<Integer.parseInt(maxNo); i= i+50){
				Long st = new Long(i);
				
				// Find used stationary using book no.
				List<String> used = branchSStatsDAO.getBranchSStats(session, user, branchCode, st, st+49, stnType, "yes");
				System.out.println("Start No = "+i+ " : Used = "+used.size());

				// If empty Total book is missing
				if(used.isEmpty()){
					System.err.println("&&&& : Total Book = "+i);					
					missStn.add(" >>> bbl/"+i+":"+branchCode);						
				}else{
				
					//Set book start no if it is not found in list
					// It will not work for first book no.
					if(st > Integer.parseInt(minNo) && Integer.parseInt(used.get(0)) != st)
						used.add(String.valueOf(st-1));
					
					// set book (start+49) no if it is not found
					// It will not work for last book no
					if(st!= Integer.parseInt(maxNo) && Integer.parseInt(used.get(used.size()-1)) != st+49)
						used.add(used.size(), String.valueOf(st + 50));
					
					// Find Missing stationary
					for(int j=0; j<used.size()-1; j++){
						int diff = (Integer.parseInt(used.get(j+1)) - Integer.parseInt(used.get(j))) -1;
						// If diff > 10
						if(diff >= 10){
							missStn.add("bbl/"+String.valueOf(Integer.parseInt(used.get(j))+1) +" ---- "+(diff-1)+":"+branchCode);
							System.err.println("Missing = "+Integer.parseInt(used.get(j)+1) +" ---- "+(diff-1));
						}else{
							// find missing stataionary 
							for(int k=1; k<=diff; k++){
								missStn.add("bbl/"+String.valueOf(Integer.parseInt(used.get(j)) + k) +":"+branchCode);
								System.err.println("Missing = "+(Integer.parseInt(used.get(j)) + k));
							}
						}
					}
				}
			}
			// return found missing stationary if branch is 23 because barbil has only Customer Stationary
			if(branchCode.equalsIgnoreCase("23"))
				return missStn;
		}
		// Find Min & Max stationary no. used
		List<Map<String, String>> minMaxList = branchSStatsDAO.getMinMax(session, user, branchCode, fromDate, toDate, stnType, "no");
		
		if(! minMaxList.isEmpty()){			
			String minNo = String.valueOf(minMaxList.get(0).get("minNo"));
			String maxNo = String.valueOf(minMaxList.get(0).get("maxNo"));
			
			System.out.println("Min NO = "+minNo);
			System.out.println("Max NO = "+maxNo);
			
			// Creating Start Book and End Book Form min and max
			if(minNo != null && maxNo != null &&  !minNo.equalsIgnoreCase("null") && !maxNo.equalsIgnoreCase("null")){			
				// Creating start book
				if(minNo.substring(minNo.length()-2, minNo.length()).equalsIgnoreCase("00")){
					minNo = String.valueOf(Integer.parseInt(minNo) - 49);
				}else{ 
					if(Integer.parseInt(minNo.substring(minNo.length()-2, minNo.length())) <= 50)
						minNo = minNo.substring(0, minNo.length()-2)+"01";
					else
						minNo = minNo.substring(0, minNo.length()-2)+"51";
				}

				// Creating end book
				if(maxNo.substring(maxNo.length()-2, maxNo.length()).equalsIgnoreCase("00")){
					maxNo = String.valueOf(Integer.parseInt(maxNo) - 49);
				}else{
					if(Integer.parseInt(maxNo.substring(maxNo.length()-2, maxNo.length())) <= 50)
						maxNo = maxNo.substring(0, maxNo.length()-2)+"01";
					else
						maxNo = maxNo.substring(0, maxNo.length()-2)+"51";
				}
				
				System.out.println("Start Book = "+minNo+" : End Book = "+maxNo);
				
				List<MasterStationaryStk> mstrList = null;
				
				// Find Issued stationary to branch
				if(fromDate.equalsIgnoreCase("") || toDate.equalsIgnoreCase(""))
					mstrList = masterStnStkDao.getMasterStnStk(session, user, branchCode, "1", maxNo, stnType);
				else
					mstrList = masterStnStkDao.getMasterStnStk(session, user, branchCode, minNo, maxNo, stnType);
				
				// Find missing accorrding to book start no
				for(int i=0; i<mstrList.size(); i++){								
					Long startNo = mstrList.get(i).getMstrStnStkStartNo();					
					List<String> used = branchSStatsDAO.getBranchSStats(session, user, branchCode, startNo, startNo+49, stnType, "no");					
					System.out.println("Start No = "+startNo+ " : Used = "+used.size());

					// Total book missing
					if(used.isEmpty()){
						System.err.println("&&&& : Total Book = "+startNo);
						missStn.add(" >>> "+startNo+":"+branchCode);						
					}else{
						//Set book start no if it is not found in list
						// It will not work for first book no. 
						if( i!= 0 && Long.parseLong(used.get(0)) != startNo)
							used.add(0, String.valueOf(startNo-1));
						
						// set book (startNo+49) no if it is not found
						// It will not work for last book no
						if(mstrList.size() > 1 && (i+1) != mstrList.size() && Long.parseLong(used.get(used.size()-1)) != startNo+49 )
							used.add(used.size(), String.valueOf(startNo + 50));
						
						// Find missing
						for(int j=0; j<used.size()-1; j++){
							int diff = (Integer.parseInt(used.get(j+1)) - Integer.parseInt(used.get(j))) -1;
							// if diff > 10
							if(diff >= 10){
								missStn.add(String.valueOf(Integer.parseInt(used.get(j)) +1 ) +" ---- "+(diff-1)+":"+branchCode);
								System.err.println("Missing = "+Integer.parseInt(used.get(j)+1) +" ---- "+(diff-1));
							}else{
								// find missing
								for(int k=1; k<=diff; k++){
									missStn.add(String.valueOf(Integer.parseInt(used.get(j)) + k) +":"+branchCode);
									System.err.println("Missing = "+(Integer.parseInt(used.get(j)) + k));
								}
							}
						}
					}
				
				}
			
			}			
		}
		
		return missStn;
	}
	
	
	
	
	@RequestMapping(value = "/reIssueStnry", method = RequestMethod.POST)
	public @ResponseBody Object reIssueStnry(@RequestBody Map<String, String> stnryData){
		logger.info("Enter into /reIssueStnry()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int i = stationaryDAO.reIssueStsnry(stnryData);
		if(i==1){
			resultMap.put("result", "success");
		}else{
			resultMap.put("result", "error");
			resultMap.put("msg", "Recheck or Retry");
		}
		logger.info("Exit from /reIssueStnry()");
		return resultMap;
	}

	
	
	
}