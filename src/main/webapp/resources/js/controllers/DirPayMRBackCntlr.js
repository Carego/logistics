'use strict';

var app = angular.module('application');

app.controller('DirPayMRBackCntlr',['$scope','$location','$http','$filter','$sce',
                              function($scope,$location,$http,$filter,$sce){
	
	$scope.onAcc = 0;
	$scope.dpmr = {};
	$scope.subPdmr = {};
	$scope.custList = [];
	$scope.bankList = [];
	$scope.blList = [];
	$scope.selBill = {};
	$scope.cnmtList = [];
	$scope.subPdmrList = [];
	$scope.dedDetailList = [];
	
	$scope.selCustFlag = true;
	$scope.selBankFlag = true;
	$scope.selBillFlag = true;
	$scope.billInfoFlag = true;
	$scope.selCnmtFlag = true;
	$scope.dedDetFlag = true;
	$scope.duplicateFlag = true;
	
	$('#netAmtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#netAmtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 15)){
			e.preventDefault();
		}
	});
	
	
	$('#chqId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#chqId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 10)){
			e.preventDefault();
		}
	});
	
	$scope.getDPMRDetail = function(){
		console.log("enter into getDPMRDetail function");
		var response = $http.post($scope.projectName+'/getDPMRDetailBack');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.csDt = data.cssDt;
				  $scope.cssDt = $scope.csDt; 
				  if($scope.currentBranch === "CRPO"){
					  $("#csDtId").attr("readonly", false);
				  }else{
					  $("#csDtId").attr("readonly", true);
				  }
				  $scope.dpmr.mrDate = $scope.csDt;
				  $scope.custList = data.list;
				  $scope.bankList = data.bnkList;
			  }else{
				  $scope.errorToast("SERVER ERROR");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
	
	
	$scope.chngDt = function(){
		console.log("enter into chngDt function = "+$scope.csDt);
		if($scope.csDt > $scope.cssDt){
			$scope.alertToast("You can't enter greater than "+$scope.dpmr.mrDate+" date");
			$scope.csDt = $scope.dpmr.mrDate;
		}else{
			$scope.dpmr.mrDate = $scope.csDt;
		}	
	}
	
	
	$scope.selectCust = function(){
		console.log("enter into selectCust function");
		$scope.selCustFlag = false;
		$('div#selCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCustFlag = true;
		    }
			});
		
		$('div#selCustId').dialog('open');	
	}
	
	
	
	$scope.saveCustomer = function(cust){
		console.log("enter into saveCustomer function");
		$('div#selCustId').dialog('close');	
		$scope.customer = cust;
		$scope.dpmr.mrCustId = cust.custId;
		
		$scope.blList = [];
		$scope.selBill = {};
		$scope.cnmtList = [];
		$scope.subPdmrList = [];
		$scope.dedDetailList = [];
		$scope.totRecAmt = 0;
		
		var response = $http.post($scope.projectName+'/getBillFrDPMR',$scope.dpmr.mrCustId);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				 $scope.blList = data.blList;
				 if($scope.blList.length > 0){
					 console.log("size of blList = "+$scope.blList.length);
					// console.log("bill list="+JSON.stringify($scope.blList));
					// console.log($scope.customer.custEditBillAmt);
				 }else{
					 $scope.alertToast("There is no pending bill for "+$scope.customer.custName);
				 }
			  }else{
				  $scope.blList = [];
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
		
	}
	
	
	$scope.selectBank = function(){
		console.log("enter into selectBank function");
		$scope.selBankFlag = false;
		$('div#selBankId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Bank",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selBankFlag = true;
		    }
			});
		
		$('div#selBankId').dialog('open');	
	}
	
	
	$scope.saveBank = function(bnk){
		console.log("enter into saveBank funciton");
		$('div#selBankId').dialog('close');
		$scope.bank = bnk;
		$scope.dpmr.mrBnkId = bnk.bnkId;
	}
	
	
	$scope.chngMRPay = function(){
		console.log("enter into chngMRPay function");
		if($scope.dpmr.mrPayBy === 'C'){
			
			$('#bnkId').attr("disabled","disabled");
			//$scope.bank.bnkName = "";
			
			$('#chqId').attr("disabled","disabled");
			$scope.dpmr.mrChqNo = "";
			
			$('#custBnkId').attr("disabled","disabled");
			$scope.dpmr.mrCustBnkName = "";
			
			$('#refId').attr("disabled","disabled");
			$scope.dpmr.mrRtgsRefNo = "";
			
		}else if($scope.dpmr.mrPayBy === 'Q'){
			$('#bnkId').removeAttr("disabled");
			$('#chqId').removeAttr("disabled");
			$('#custBnkId').removeAttr("disabled");
			
			$('#refId').attr("disabled","disabled");
			$scope.dpmr.mrRtgsRefNo = "";
			
		}else if($scope.dpmr.mrPayBy === 'R'){
			
			$('#chqId').attr("disabled","disabled");
			$scope.dpmr.mrChqNo = "";
			$('#bnkId').removeAttr("disabled");
			$('#refId').removeAttr("disabled");
			$('#custBnkId').removeAttr("disabled");
			
		}else{
			console.log("invalid Receipt method");
		}
	}
	
	
	$scope.selectBill = function(){
		console.log("enter into selectBill funciton");
		$scope.selBillFlag = false;
		$('div#selBillId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Bill No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selBillFlag = true;
		    }
			});
		
		$('div#selBillId').dialog('open');
	}
	
	
	
	$scope.saveBill = function(bill){
		console.log("enter into saveBill function");
		var duplicate = false;
		$scope.selBill = bill;
		$scope.cnmtList = [];
		$scope.subPdmr.mrFreight = parseFloat(bill.blRemAmt.toFixed(2));

		var response = $http.post($scope.projectName+'/getCnmtFrDPMR',$scope.selBill.blBillNo);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				$scope.cnmtList = data.list
				console.log("cnmtList"+$scope.cnmtList); //mk
				if($scope.subPdmrList.length > 0){
					for(var i=0;i<$scope.subPdmrList.length;i++){
						if($scope.subPdmrList[i].bill.blBillNo === bill.blBillNo){
							duplicate = true;
							break;
						}
					}
					
					if(duplicate === true){
						$scope.alertToast("You already enter the detail of "+bill.blBillNo);
					}else{
						
						$('div#selBillId').dialog('close');
						$scope.billInfoFlag = false;
						$('div#billInfoId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Detail For Bill "+bill.blBillNo+" ("+bill.blRemAmt.toFixed(2)+")",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.billInfoFlag = true;
						    }
							});
						
						$('div#billInfoId').dialog('open');
					}
					
				}else{
					$('div#selBillId').dialog('close');
					$scope.billInfoFlag = false;
					$('div#billInfoId').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Detail For Bill "+bill.blBillNo+" ("+bill.blRemAmt.toFixed(2)+")",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.billInfoFlag = true;
					    }
						});
					
					$('div#billInfoId').dialog('open');
				}
				
			  }else{
				  $scope.alertToast($scope.selBill.blBillNo+" doesn't have any cnmt")
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
		
	}
	
	
	
	$scope.removePDMR = function(index){
		console.log("enter into removePDMR function");
		$scope.subPdmrList.splice(index,1);
		
		$scope.totRecAmt = 0;
		if($scope.subPdmrList.length > 0){
			for(var i=0;i<$scope.subPdmrList.length;i++){
				$scope.totRecAmt = $scope.totRecAmt + $scope.subPdmrList[i].mrNetAmt;
			}
		} 
	}
	
	

	
	$scope.submitBillInfo = function(billInfoForm){
		console.log("enter into submitBillInfo function");
		if(billInfoForm.$invalid){
			$scope.alertToast("Please fill the correct form");
		}else{
			if(angular.isUndefined($scope.subPdmr.mrNetAmt)){
				$scope.subPdmr.mrNetAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrFreight)){
				$scope.subPdmr.mrFreight = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrTdsAmt)){
				$scope.subPdmr.mrTdsAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrDedAmt)){
				$scope.subPdmr.mrDedAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrAccessAmt)){
				$scope.subPdmr.mrAccessAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrSrvTaxAmt)){
				$scope.subPdmr.mrSrvTaxAmt = 0;
			}
				
			
			var ded = 0;
			for(var i=0;i<$scope.dedDetailList.length;i++){
				ded = ded + $scope.dedDetailList[i].dedAmt;
			}
			
			if(ded === $scope.subPdmr.mrDedAmt){
				/*var finalAmt = 0.0;
				finalAmt = parseFloat(($scope.subPdmr.mrFreight + $scope.subPdmr.mrSrvTaxAmt + $scope.subPdmr.mrAccessAmt + $scope.subPdmr.mrDedAmt) - $scope.subPdmr.mrTdsAmt);
				
				console.log("finalAmt = "+finalAmt);*/
				
				if(parseFloat($scope.subPdmr.mrFreight + $scope.subPdmr.mrSrvTaxAmt + $scope.subPdmr.mrAccessAmt) === parseFloat($scope.subPdmr.mrTdsAmt + $scope.subPdmr.mrNetAmt + ded)){
		
					 /*if($scope.selBill.blRemAmt < finalAmt){
					$scope.alertToast("Payment detail must be <= "+$scope.selBill.blRemAmt);
				}else{*/
					//$scope.selBill.blRemAmt = finalAmt - $scope.selBill.blRemAmt;
					console.log("Remaining amount of "+$scope.selBill.blBillNo+" = "+$scope.selBill.blRemAmt);
					$scope.subPdmr.bill = $scope.selBill;
					$scope.subPdmr.mrDate = $scope.dpmr.mrDate;
					$scope.subPdmr.mrCustId = $scope.dpmr.mrCustId;
					$scope.subPdmr.mrBnkId = $scope.dpmr.mrBnkId;
					$scope.subPdmr.mrPayBy = $scope.dpmr.mrPayBy;
					$scope.subPdmr.mrChqNo = $scope.dpmr.mrChqNo;
					$scope.subPdmr.mrCustBankName = $scope.dpmr.mrCustBankName;
					$scope.subPdmr.mrRtgsRefNo = $scope.dpmr.mrRtgsRefNo;
					$scope.subPdmr.mrDedDetList = $scope.dedDetailList;
					
					/*console.log("$scope.selOAMR.mrNetAmt = "+$scope.selOAMR.mrNetAmt);
					console.log("$scope.totRecAmt = "+$scope.totRecAmt);
					console.log("$scope.subPdmr.mrNetAmt = "+$scope.subPdmr.mrNetAmt);*/
					
					if(parseFloat($scope.dpmr.mrNetAmt) < parseFloat($scope.totRecAmt + $scope.subPdmr.mrNetAmt)){
						$scope.alertToast("please enter bill detail less than equal to MR amount "+$scope.dpmr.mrNetAmt);
					}else{
						$('div#billInfoId').dialog('close');
						$scope.subPdmrList.push($scope.subPdmr);
						console.log("size of subPdmrList = "+$scope.subPdmrList.length);
						
						$scope.selBill = {};
						$scope.subPdmr = {};
						
						$scope.totRecAmt = 0;
						if($scope.subPdmrList.length > 0){
							for(var i=0;i<$scope.subPdmrList.length;i++){
								$scope.totRecAmt = $scope.totRecAmt + $scope.subPdmrList[i].mrNetAmt;
							}
						} 
					//}
						
					$scope.dedDetailList = [];	
					}
					
					
				}else{
					$scope.alertToast("Please enter complete detail of "+parseFloat($scope.subPdmr.mrFreight + $scope.subPdmr.mrSrvTaxAmt).toFixed(2));
				} 
		
			}else{
				$scope.alertToast("Please enter complete deduction detail of "+$scope.subPdmr.mrDedAmt);
			}
		}
	}
	
	
	
	$scope.selectCnmt = function(){
		console.log("enter into selectCnmt function");
		$scope.selCnmtFlag = false;
		$('div#selCnmtId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Cnmt",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCnmtFlag = true;
		    }
			});
		
		$('div#selCnmtId').dialog('open');
	}
	
	
	$scope.saveCnmt = function(cnmt){
		console.log("enter into saveCnmt function");
		$scope.cnmtCode = cnmt;
		$('div#selCnmtId').dialog('close');
	}
	
	
	$scope.submitDedDet = function(dedDetForm){
		console.log("enter into submitDedDet funciton = "+dedDetForm.$invalid);
		if(dedDetForm.$invalid){
			$scope.alertToast("Please fill the correct deduction detail");
		}else{
			if($scope.dedDetailList.length == 0){
				
				if($scope.dedAmt > $scope.subPdmr.mrDedAmt){
					$scope.alertToast("Deduction detail amount can't be greater than "+$scope.subPdmr.mrDedAmt);
				}else{
					var map = {
							"cnmtNo"  : $scope.cnmtCode,
							"dedType" : $scope.dedType,
							"dedAmt"  : $scope.dedAmt
					};
					
					$scope.dedDetailList.push(map);
					$('div#dedDetId').dialog('close');
				}
				
			}else{
				var ded = 0;
				var duplicate = false;
				for(var i=0;i<$scope.dedDetailList.length;i++){
					ded = ded + $scope.dedDetailList[i].dedAmt;
					if($scope.dedDetailList[i].cnmtNo === $scope.cnmtCode && $scope.dedDetailList[i].dedType === $scope.dedType){
						duplicate = true;
						break;
					}
				}
				
				if(duplicate === false){
					ded = ded + $scope.dedAmt;
					
					if(ded > $scope.subPdmr.mrDedAmt){
						$scope.alertToast("Deduction detail amount can't be greater than "+$scope.subPdmr.mrDedAmt);
					}else{
						var map = {
								"cnmtNo"  : $scope.cnmtCode,
								"dedType" : $scope.dedType,
								"dedAmt"  : $scope.dedAmt
						};
						
						$scope.dedDetailList.push(map);
						$('div#dedDetId').dialog('close');
					}
				}else{
					$scope.alertToast("You can't repeat the same deduction detail");
				}
			}
			
			$scope.cnmtCode = "";
			$scope.dedType = "";
			$scope.dedAmt = 0;
		}
	}
	
	
	$scope.addDedDetail = function(){
		console.log("enter into addDedDetail function");
		if($scope.subPdmr.mrDedAmt > 0){
			$scope.dedDetFlag = false;
			$('div#dedDetId').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Deduction Detail",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.dedDetFlag = true;
			    }
				});
			
			$('div#dedDetId').dialog('open');
		}else{
			$scope.alertToast("Please enter deduction amount");
		}
	}
	
	
	$scope.removeDed = function(index){
		console.log("enter into removeDed function = "+index);
		$scope.dedDetailList.splice(index,1);
	}
	
	
	$scope.dpmrSubmit = function(dpmrForm){
		console.log("enter into dpmrForm function = "+dpmrForm.$invalid);
		if(dpmrForm.$invalid){
			$scope.alertToast("Please fill the correct form");
		}else{
			if($scope.dpmr.mrNetAmt !== $scope.totRecAmt + $scope.onAcc){
				$scope.alertToast("Please enter the complete payment detail of "+$scope.dpmr.mrNetAmt);
			}else{
				if($scope.subPdmrList.length > 0){
					for(var i=0;i<$scope.subPdmrList.length;i++){
						$scope.subPdmrList[i].mrDesc = $scope.dpmr.mrDesc;
						$scope.subPdmrList[i].mrOthMrAmt = $scope.onAcc;
					}
					
					$('#saveId').attr("disabled","disabled");
					
					var response = $http.post($scope.projectName+'/submitDPMR',$scope.subPdmrList);
					  response.success(function(data, status, headers, config){
						  if(data.result === "success"){
							  $scope.duplicateFlag = false;
						    	$('div#duplicateDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
								        $(this).dialog('destroy');
								        $(this).hide();
								        $scope.duplicateFlag = true;
								    }
									});
								$('div#duplicateDB').dialog('open');
								
						  }else{
							  $scope.saveMR();
							  //$scope.alertToast("SERVER ERROR");
						  }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
					   });	
				}else{
					$scope.alertToast("Please enter the payment detail of "+$scope.dpmr.mrNetAmt);
				}
			}
		}
		
	}
	
	
	
	$scope.cancelMR = function(){
		console.log("enter into cancelMR function");
		$('div#duplicateDB').dialog('close');
		$('#saveId').removeAttr("disabled");
	}
	
	
	
	$scope.saveMR = function(){
		console.log("enter into saveMR function");
		var response = $http.post($scope.projectName+'/submitFinalDPMR',$scope.subPdmrList);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  if($scope.duplicateFlag === false){
					  $scope.duplicateFlag = true;
					  $('div#duplicateDB').dialog('close');
				  }
				  $('#saveId').removeAttr("disabled");
				  $scope.alertToast("Successfully generat MR NO. = "+data.mrNo);
					$scope.dpmr = {};
					$scope.custList = [];
					$scope.bankList = [];
					$scope.blList = [];
					$scope.selBill = {};
					$scope.cnmtList = [];
					$scope.subPdmrList = [];
					$scope.dedDetailList = [];
					$scope.totRecAmt = 0;
					
					$scope.onAcc = 0;
					$scope.csDt = $scope.cssDt;
					$scope.getDPMRDetail();
					
					//$scope.totOAAmt = 0;
			  }else{
				  $scope.errorToast("SERVER ERROR");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getDPMRDetail();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);