package com.mylogistics.DAO.report;

import java.sql.Blob;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.RelianceCNStatus;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.services.CnDetailService;
import com.mylogistics.services.ReconRptService;

@Repository
public interface ReportDAO {
	
	public List<Map<String, Object>> getBrLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp);
	
	public List<Map<String, Object>> getConsolidateLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp);

	public List<String> getMisCnmt(String branchId, String fromCnmtNo, String toCnmtNo, String status);
	
	public List<Map<String, Object>> getBrLedger(Map<String, Object> ledgerReportMap);
	
	public List<Map<String, Object>> getConsolidateLedger(Map<String, Object> ledgerReportMap);
	
	public List<String> getFinYears();
	
	public Map<String, Object> getBrTrial(Map<String, Object> trialReportMap);
	
	public Map<String, Object> getConsolidateTrial(Map<String, Object> trialReportMap);
	
	public List<CnDetailService> getCnDetByDt(Date frDt, Date toDt);
	
	public List<CnDetailService> getCnDetByBrh(Date frDt, Date toDt, String brhCode);
	
	public List<CnDetailService> getCnDetByCust(Date frDt, Date toDt, String custCode);
	
	public void test();
	
	public int saveFirstBCS(Map<String,Object> clientMap);
	
	public Map<String, Object> getReconReport(ReconRptService reconRptService);
	
	public List<CashStmt> getCsListFrmCsTemp(CashStmtTemp cashStmtTemp);
	
	public List<Map<String, Object>> getStockAndOsRpt(Map<String, Object> stockAndOsRptService);
	
	public List<Map<String, Object>> getHoStockAndOsRpt(User user, Map<String, Object> initParam);
	
	public Map<String, Object> getStockRpt(Map<String, Object> stockRptService);
	
	public Map<String, Object> getOsRpt(Map<String, Object> osRptService);
	
	public Map<String, Object> getOsRptN(Map<String, Object> osRptService);
	
	public List<Map<String, Object>> getShortExcesRpt(Map<String,Object> shortExcessRptMap);
	
	public Map<String, Object> sendStockAndOsRpt(MultipartFile attachmentFile);

	public List<String> getMisChln(String branchId, String status);

	public List<String> getMisSedr(String branchId, String status);

	public Map<String, Object> getRelOsRpt(Map<String, Object> osRptService);

	public Map<String, Object> getRelStockRpt(Map<String, Object> osRptService);

	public List<RelianceCNStatus> getDataFromRelianceCNStatus(String cnmtCode);

	public List<RelianceCNStatus> fetchCNStatus(Date fromdate,
			Date todate);

	public void saveRelianceCNStatus(RelianceCNStatus cnStatus);

	public List<Integer> getChallanId(List<Integer> integers);

	public List<Challan> getChallanData(List<Integer> cnmt_challans_Id);

	public List<ArrivalReport> getArrivalReoprtData(String challanno);

	public List<Integer> getChallanData(String cnmtCode);

	public List<Cnmt> getCnmtCodes(String cnmtCode);

	public List<Station> getStation();

	public Map<String, Object> getStockRptN(Map<String, Object> stockRptService);

	public Map<String, Object> getRelOs(Map<String, Object> osRptService);
	
	public Map<String, Object> getRelStockReport(Map<String, Object> osRptService);

	public List<Challan> getPayableReport(Date fromDt, Date toDt);
}
