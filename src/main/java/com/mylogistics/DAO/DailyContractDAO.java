package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.DailyContract;


@Repository
public interface DailyContractDAO {

	//Sanjana: saves daily contract to database
	public int saveDailyContract(DailyContract dailyContract);
	
	//Sanjana: retrieves daily contract from database on the basis of entered contract code
	public List<DailyContract> getDailyContract(String dlyContCode);
	
	//Sanjana: updates the contract
	public int updateDailyContract(DailyContract dailyContract);
	
	//Sanjana: retrieves the list of daily contracts whose isVerify is 'no'
	public List<DailyContract> getDlyContractisVerifyNo();
	
	//Sanjana: updates the isVerify to 'yes'
	public int updateContractisVerify(String code[]);
	
	//Sanjana: retrieves the last row of daily contract table
	public int getLastDlycontractId();
	
	//Sanjana: returns the total number of rows in daily contract table
	public long totalCount();
	
	//Sanjana: retrieves the list of daily contracts whose isView is false
	public List<DailyContract> getDlyContractisViewFalse(String branchCode);
	
	//Sanjana: updates the isView to true
	public int updateContractisViewTrue(String code[]);
	
	//Sanjana: get the list of dailyContract codes
	public List<String> getDailyContractCode();
	
	//Sanjana: get the list of dailyContractCodes based on entered branchCode
	public List<String> getDailyContractCodes(String branchCode);

	public List<DailyContract> getDailyContractCode(String custCode);
	
	public List<DailyContract> getDailyContForCnmt(String custCode,String cnmtFromSt);
	
	//Sanjana: get dailyContract data for saving in oldDailyContract
	 public List<DailyContract> getDailyContractData(int id);
	 
	 public List<DailyContract> getDailyContractData(String contractCode);
	 
	 public List<String> getDlyFaCode();

	 public List<DailyContract> getDlyContByFaCode(String faCode);
	 
	 public List<Map<String,String>> getDlyCont();
	 
	 public int extDtOfCont(String contFaCode , Date date);
	 
	 public int updateBillBase(String contCode , String billBase);
	 
}
