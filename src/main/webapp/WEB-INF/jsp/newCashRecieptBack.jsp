<style type="text/css">
            .printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		border: 1px solid black;
                		}
            }
</style>



<div ng-show="operatorLogin || superAdminLogin">
<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>

	<div class="row noprint">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="faCodeId" name ="faCode" ng-model="vs.faCode"  ng-keyup="openFaCodeDB($event.keyCode)" ng-required="true" >
		       			<label for="code">FA Code</label>	
		       		</div>
			 <!--  		 <div class="col s4 input-field">
				    		<select name="voucherType" id="voucherTypeId" ng-model="vs.voucherType" ng-required="true">
								<option value="CashWithdraw">Cash Withdrawal</option>
								<option value="CashReceipt">Cash Reciept</option>
							</select>
							<label>Voucher Type </label>
					</div> 
			      	<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" required  ng-change="selectChqType()">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div> -->
		    </div>
		  
	       <div class="row">
	      	 	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="amountId" name ="amount" ng-model="vs.amount" step = "0.01" ng-required="true" >
		       			<label for="code">Amount</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="mrNoId" name ="mrNo" ng-model="vs.mrNo" step = "0.01" >
		       			<label for="code">MR NO.</label>	
		       	</div>
	     		
	   	   </div>
	   	
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="submit" value="submit" >
		       		</div>
		       		<!-- <div class="col s4 input-field">
		       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
		       		</div> -->
	   		 </div>
		  </form>
    </div>
    
    <div id="faCodeDB" ng-hide="faCodeFlag" class="noprint">
    	<input type="text" name="filterTextbox" ng-model="filterTextbox.bankCode" placeholder="Search by Bank Code ">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> Fa Code </th>
	 	  	  	  <th> Name </th>
	 	  	  	  <th> Type </th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="faM in faMList | filter:filterTextbox.bankCode ">
			 	  <td><input type="radio"  name="faM"   value="{{ faM }}" ng-model="faMaster" ng-click="saveFaCode(faM)"></td>
	              <td>{{faM.faMfaCode}}</td>
	              <td>{{faM.faMfaName}}</td>
	              <td>{{faM.faMfaType}}</td>
	          </tr>
	      </table> 
    </div>
    
    
    <div id="saveVsDB" ng-hide="saveVsFlag" class="noprint"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
						
				<tr>
					<td>Fa Code</td>
					<td>{{vs.faCode}}</td>
				</tr>
											
				<tr>
					<td>Amount</td>
					<td>{{vs.amount}}</td>
				</tr>
						
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>


  <div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
   </div>
	
	
	<div class="printable"> 
		<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<div class="container" style="width: 100% !important;">
				<div class="row">
					<div class="col s8">
						<div class="row">
							<div class="col s12">{{cmpnyName}}<br/>
												Voucher Type: {{vs.voucherType}}<br/>
												<!-- Cheque Type: {{vs.chequeType}}<br/> -->
												<!-- <br/> -->
												Pay to: {{vs.payTo}}</div>
						</div>
					</div>
					<div class="col s4">
						<div class="row">
							<div class="col s12">Branch Code:{{vs.branch.branchFaCode}}<br/>
												Sheet No: {{vs.cashStmtStatus.cssCsNo}}<br/>
												Date: {{dateTemp}}<br/>
												Voucher No: {{vs.vhNo}}<br/>
												<!-- Cheque No: {{vs.chequeLeaves.chqLChqNo}} --></div>
						</div>						
					</div>
				</div>
				<table style="width: 100% !important;">
					<tr style="border-top: 1px solid black; border-bottom: 1px solid black;">
						<!-- <td style="width: 1in;">BCD</td> -->
						<td style="width: 1in;">BRH NAME</td>
						<td style="width: 1in;">FA CODE</td>
						<td style="width: 1in;">FA NAME</td>
						<td style="width: 1in;">MR NO</td>
						<td style="width: 1in;">TV NO</td>
						<td style="width: 1.5in;">DEBIT AMT</td>
						<td style="width: 1.5in;">CREDIT AMT</td>
						<td style="width: 3in;">DESCRIPTION</td>
					</tr>
					<tr style="border-bottom: 1px solid black;">
						<!-- <td style="width: 1in;">{{vs.branch.branchFaCode}}</td> -->
						<td style="width: 1in;">{{vs.branch.branchName}}</td>
						<td style="width: 1in;">{{vs.faCode}}</td>
						<td style="width: 1in;">{{vs.faName}}</td>
						<td style="width: 1in;">{{vs.mrNo}}</td>
						<td style="width: 1in;">{{vs.tvNo}}</td>
						<td style="width: 1.5in;"> </td>
						<td style="width: 1.5in; text-align: center;">{{vs.amount}}</td>
						<td style="width: 3in;">{{vs.desc}}</td>
					</tr>
				</table>
				<div class="row">
					<div class="col s12">Net Cash Withdrawal Amount {{vs.amount}}</div>
				</div>
				<div class="row">
					<div class="col s12">Total Amount </div>
				</div>
				<div class="row">
					<div class="col s12">Note: Cashier has confirmed that he has obtained signature of passing authority & Payee on this payment by putting his signature on the same. </div>
				</div>
				<div class="row">
					<div class="col s3">Passed by<br/></>Code No</div><div class="col s3">Cash by<br/></>Code No</div><div class="col s3">Vouchered by<br/></>Code No</div><div class="col s3">Payees Signature</div>
				</div>
			</div>
		</div>
	</div>


</div>
