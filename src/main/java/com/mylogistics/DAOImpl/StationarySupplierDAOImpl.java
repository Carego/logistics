package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.StationarySupplierDAO;
import com.mylogistics.constants.StationarySupplierCNTS;
import com.mylogistics.model.StationarySupplier;


public class StationarySupplierDAOImpl implements StationarySupplierDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public StationarySupplierDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
     public int saveStationarySupplier(StationarySupplier stationarySupplier){
		 int temp;
		  Date date = new Date();
	 	  Calendar calendar = Calendar.getInstance();
	 	  calendar.setTime(date);	 	
		 try{
		      session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  stationarySupplier.setCreationTS(calendar);
			  session.saveOrUpdate(stationarySupplier);
			  transaction.commit();
			  session.flush();
			  temp= 1;
		 }catch(Exception e){
			  e.printStackTrace();
			  temp= -1;
		 }
		 session.clear();
		 session.close();	
		 return temp;
	 }
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<StationarySupplier> getOldStSupplier(int stSupID){
			List<StationarySupplier> list = new ArrayList<StationarySupplier>();
			try{
				 session = this.sessionFactory.openSession();
				 transaction =  session.beginTransaction();
				 Criteria cr=session.createCriteria(StationarySupplier.class);
				 cr.add(Restrictions.eq(StationarySupplierCNTS.ST_SUP_ID,stSupID));
				 list =cr.setMaxResults(1).list();
				 session.flush();
			}catch(Exception e){
				 e.printStackTrace();
			}
			session.clear();
			session.close();
			return list; 
	}
	 
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<StationarySupplier> getStationarySupplierCode(){
		List<StationarySupplier> list = new ArrayList<StationarySupplier>();
		try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         list = session.createCriteria(StationarySupplier.class).list();
	         transaction.commit();
	         session.flush();
		}catch (Exception e) {
			 e.printStackTrace();
		}
		session.clear();
		session.close();
		return list;
	}
		
	@Transactional
	public long totalCount(){
		long num=-1;
		try{
		     session = this.sessionFactory.openSession();
			 num = (Long) session.createCriteria(StationarySupplier.class).setProjection(Projections.rowCount()).uniqueResult();
			 session.flush();
		}catch(Exception e){
			 e.printStackTrace();
		}
		session.clear();
		session.close();
		return  num;
	}
		
	@Transactional
    @SuppressWarnings("unchecked")
    public List<StationarySupplier> getLastStationarySupplier(){
		List<StationarySupplier> last = new ArrayList<StationarySupplier>();
	    try{
		     session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 last = session.createQuery("from StationarySupplier order by stSupID DESC ").setMaxResults(1).list();
			 session.flush();
		}catch(Exception e){
			 e.printStackTrace();
		}
	    session.clear();
		session.close();
		return last;
	}
		 
	/*@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getSupplierCodes(){
		List<String> supplierCodeList = new ArrayList<String>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(StationarySupplier.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(StationarySupplierCNTS.ST_SUP_CODE));
		     cr.setProjection(projList);
			 supplierCodeList = cr.list();
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
				e.printStackTrace();
		}
		session.close();
		return supplierCodeList;
	}*/
		 
	@SuppressWarnings("unchecked")
	@Transactional
	public List<StationarySupplier> retrieveSupplier(String stSupplierCode){
		List<StationarySupplier> list = new ArrayList<StationarySupplier>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr=session.createCriteria(StationarySupplier.class);
			 cr.add(Restrictions.eq(StationarySupplierCNTS.ST_SUP_CODE,stSupplierCode));
			 list =cr.setMaxResults(1).list();
			 session.flush();
		}catch(Exception e){
			 e.printStackTrace();
		}
		session.clear();
		session.close();
		return list; 
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<StationarySupplier> getSupplierListForAdmin(){
		 List<StationarySupplier> list = new ArrayList<StationarySupplier>();
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(StationarySupplier.class);
				cr.add(Restrictions.eq(StationarySupplierCNTS.IS_VIEW,false));
				list =cr.list();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return list;  
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateStSupplierIsViewTrue(String code[]){
		
		 System.out.println("Entered into updateStSupplierIsViewTrue function in StSupplierDAOImpl--->");
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
		 
         for(int i=0;i<code.length;i++){
        	 System.out.println("The values inside array is ----------"+code[i]);
         }
         try{
             session = this.sessionFactory.openSession();
             for(int i=0;i<code.length;i++){
                transaction = session.beginTransaction();
                StationarySupplier stSupplier  = new StationarySupplier();
                List<StationarySupplier> list = new ArrayList<StationarySupplier>();
                Criteria cr=session.createCriteria(StationarySupplier.class);
                cr.add(Restrictions.eq(StationarySupplierCNTS.ST_SUP_CODE,code[i]));
                list =cr.list();
                stSupplier = list.get(0);
                stSupplier.setView(true);
                stSupplier.setCreationTS(calendar);
                session.update(stSupplier);
                transaction.commit();
             }
             session.flush();
             temp = 1;
        }catch(Exception e){
             e.printStackTrace();
             temp = -1;
        }
         session.clear();
        session.close();
        return temp;
		
	}
	
}
