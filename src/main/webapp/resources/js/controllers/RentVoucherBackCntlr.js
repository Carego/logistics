'use strict';

var app = angular.module('application');

app.controller('RentVoucherBackCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.vs = {};
	$scope.rs = {};
	$scope.rentList = [];
	$scope.rentInfo = [];
	$scope.faMList = [];
	$scope.chqList = [];
	$scope.amtSum=0;
	$scope.tdsAmtSum=0;
	$scope.rentDBFlag = true;
	$scope.bankCodeDBFlag = true;
	$scope.rentPayDBFlag = true;
	$scope.tdsCheck = false;
	$scope.tdsCodeDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.selChqDBFlag = true;
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVDetFrRentBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.RENT_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getRentMstr();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getRentMstr = function(){
		console.log("enter into getRentMstr funciton");
		var response = $http.post($scope.projectName+'/getRentMstrData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.rentInfo = data.list;
				$scope.faMList = data.faMList;
			}else{
				$scope.alertTost("Rent Master is not avaliable");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	}
	
	$scope.selectRentId = function(){
		console.log("enter into selectRentId funciton");
		
		$scope.rentDBFlag = false;
    	$('div#rentDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "RENT FOR",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.rentDBFlag = true;
		    }
			});
		
		$('div#rentDB').dialog('open');	
	}
	
	
	$scope.selectRent = function(rent){
		console.log("enter into selectRent funciton");
		$('div#rentDB').dialog('close');
		
		$scope.tdsCheck = false;
		$scope.rs.tdsCode = "";
		$scope.rs.tdsAmt = 0;
		$('#tdsId').attr("disabled","disabled");
		$('#tdsAmtId').attr("disabled","disabled");
		
		var dublicate = false;
		if($scope.rentList.length > 0){
			for(var i=0;i<$scope.rentList.length;i++){
				if($scope.rentList[i].rmId === rent.rmId){
					$scope.alertToast("you already select this id");
					dublicate = true;
				}	
			}
			if(dublicate === false){
				if(rent.rentFr === "staff"){
					$('#empId').removeAttr("disabled");
				}else{
					$('#empId').attr("disabled","disabled");
				}
				
				$scope.rs.brhName   = rent.brhName;
				$scope.rs.brhId     = rent.brhId;
				$scope.rs.brhFaCode = rent.brhFaCode;
				$scope.rs.empName   = rent.empName;
				$scope.rs.empId     = rent.empId;
				$scope.rs.empFaCode = rent.empFaCode;
				$scope.rs.rmId      = rent.rmId;
				$scope.rs.rentFr    = rent.rentFr;
				$scope.rs.landLord  = rent.landLord;
				
				
				$scope.rentPayDBFlag = false;
		    	$('div#rentPayDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					title: "Phone Number",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.rentPayDBFlag = true;
				    }
					});
				
				$('div#rentPayDB').dialog('open');	
			}
		}else{
			if(rent.rentFr === "staff"){
				$('#empId').removeAttr("disabled");
			}else{
				$('#empId').attr("disabled","disabled");
			}
			
			$scope.rs.brhName   = rent.brhName;
			$scope.rs.brhId     = rent.brhId;
			$scope.rs.brhFaCode = rent.brhFaCode;
			$scope.rs.empName   = rent.empName;
			$scope.rs.empId     = rent.empId;
			$scope.rs.empFaCode = rent.empFaCode;
			$scope.rs.rmId      = rent.rmId;
			$scope.rs.rentFr    = rent.rentFr;
			$scope.rs.landLord  = rent.landLord;
			
			
			$scope.rentPayDBFlag = false;
	    	$('div#rentPayDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Phone Number",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.rentPayDBFlag = true;
			    }
				});
			
			$('div#rentPayDB').dialog('open');	
		}	
	}
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$('#phNoId').removeAttr("disabled");
			
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			/*$scope.vs.payTo = "";
			$('#payToId').attr("disabled","disabled");*/
			
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankCodeDBFlag = true;
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	 $scope.saveBankCode =  function(bankCode){
			console.log("enter into saveBankCode----->"+bankCode);
			$scope.vs.bankCode = bankCode;
			$('div#bankCodeDB').dialog('close');
			$scope.bankCodeDBFlag = true;
			

			//$('#payToId').removeAttr("disabled");
			//$('#phNoId').removeAttr("disabled");
					
			if($scope.vs.payBy === 'Q'){
				if(angular.isUndefined($scope.vs.chequeType) ||  $scope.vs.chequeType === null || $scope.vs.chequeType === ""){
					$scope.alertToast("please select cheque type");
				}else{
					var chqDet = {
							"bankCode" : bankCode,
							"CType"    : $scope.vs.chequeType
					};
					
					var response = $http.post($scope.projectName+'/getChequeNoFrTV',chqDet);
					response.success(function(data, status, headers, config){
						if(data.result === "success"){
							console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
							$('#chequeNoId').removeAttr("disabled");
							$scope.chqList = data.list;
							$scope.vs.chequeLeaves = data.chequeLeaves;
							$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
						}else{
							$scope.alertToast("Branch does not have '"+$scope.vs.chequeType+"' type cheeque of Bank "+bankCode);
							console.log("Error in bringing data from getChequeNo");
						}
					});
					response.error(function(data, status, headers, config){
						console.log(data);
					});
				}
				
			}
			
	 }
	
	 
	 $scope.openChqDB = function(){
		 console.log("enter into openChqDB function");
		    $scope.selChqDBFlag = false;
	    	$('div#selChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select Cheque No.",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selChqDBFlag = true;
			    }
				});
			$('div#selChqDB').dialog('open');
	 }
	
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function");
		 $scope.vs.chequeLeaves = chq;
		 $('div#selChqDB').dialog('close');
	 }
	 
	 
	 $scope.openTdsCode = function(){
		 console.log("enter into openTdsCode funciton");
		 $scope.tdsCodeDBFlag = false;
			$('div#tdsCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "TDS CODE",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.tdsCodeDBFlag = true;
			    }
				});
			
			$('div#tdsCodeDB').dialog('open');	
	 }
	 
	 
	 $scope.saveTdsCode = function(faM){
		 console.log("enter into saveTdsCode funciton");
		 $('div#tdsCodeDB').dialog('close');
		 $scope.rs.tdsCode = faM.faMfaCode;
	 }
	 
	 
	 $scope.allowTds = function(){
		 console.log("enter into allowTds funciton");
		 if($scope.tdsCheck === true){
			 $('#tdsId').removeAttr("disabled");
			 $('#tdsAmtId').removeAttr("disabled");
			 $scope.tdsCodeReq=true;
		 }else{
			 $scope.rs.tdsCode = "";
			 $scope.rs.tdsAmt = 0;
			 $scope.tdsCodeReq=false;
			 $('#tdsId').attr("disabled","disabled");
			 $('#tdsAmtId').attr("disabled","disabled");
		 }
	 }
	 
	 
	 $scope.submitRentPay = function(rentPayForm){
		 console.log("enter into submitRentPay function = "+rentPayForm.$invalid);
		 if(rentPayForm.$invalid){
			 $scope.alertToast("please fill correct form");
		 }else{
			 $scope.amtSum=$scope.amtSum+$scope.rs.amt;
			 $scope.tdsAmtSum=$scope.tdsAmtSum+$scope.rs.tdsAmt;
			 $('div#rentPayDB').dialog('close');
			 $scope.rentList.push($scope.rs);
			 $scope.rs = {};
		 }
	 }
	 
	 
	 $scope.removeRent = function(index,rent){
		 console.log("enter into removeRent funciton");
		 $scope.amtSum=$scope.amtSum-rent.amt;
		 $scope.tdsAmtSum=$scope.tdsAmtSum-rent.tdsAmt;
		 $scope.rentList.splice(index,1);
	 }
	 
	 $scope.voucherSubmit = function(newVoucherForm){
		 console.log("enter into voucherSubmit function ==>"+newVoucherForm.$invalid);
		 if(newVoucherForm.$invalid){
			 $scope.alertToast("please enter correct value");
		 }else{
			    console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	
	 
	 $scope.back = function(){
			console.log("enter into back function");
			$('div#saveVsDB').dialog('close');
	}
	 
	 
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$scope.vs.subFList = [];
		$scope.vs.subFList = $scope.rentList;
		
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitRentVouch',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('#saveId').removeAttr("disabled");
				$('div#saveVsDB').dialog('close');
				
				
				$scope.vs = {};
				$scope.rs = {};
				$scope.rentList = [];
				$scope.rentInfo = [];
				$scope.faMList = [];
				
				//$scope.vs.chequeType = '';
				$('#chequeTypeId').attr("disabled","disabled");
				//$scope.vs.bankCode = "";
				$('#bankCodeId').attr("disabled","disabled");
				//$scope.vs.chequeLeaves = {};
				$('#chequeNoId').attr("disabled","disabled");
				
				$scope.getVoucherDetails();
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);