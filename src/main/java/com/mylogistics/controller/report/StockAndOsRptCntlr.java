package com.mylogistics.controller.report;

import java.sql.Blob;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class StockAndOsRptCntlr {

	@Autowired
	ReportDAO reportDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(StockAndOsRptCntlr.class);
	
	
	@RequestMapping(value = "/sendStockAndOsRpt", method = RequestMethod.POST)
	public @ResponseBody Object sendStockAndOsRpt(@RequestParam("report") MultipartFile report){			
		logger.info("Enter into /sendStockAndOsRpt() ");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap = reportDAO.sendStockAndOsRpt(report);
		logger.info("Exit from /sendStockAndOsRpt()");
		return resultMap;
	}
	
	@RequestMapping(value="/getStockAndOsRpt", method=RequestMethod.POST)
	public @ResponseBody Object getStockAndOsRpt(@RequestBody Map<String, Object> stockAndOsRptService){
		System.out.println("StockAndOsRptCntlr.getStockAndOsRpt()");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> stockAndOsRptList = reportDAO.getStockAndOsRpt(stockAndOsRptService); 
		System.out.println("stockAndOsRptList size: "+stockAndOsRptList.size());
		//calculate totals
		double os0_30Total = 0.0;
		double os31_60Total = 0.0;
		double os61_90Total = 0.0;
		double os91_120Total = 0.0;
		double os121_moreTotal = 0.0;
		double onACTotal = 0.0;
		double osTotalFinal = 0.0;
		double stkUpTo30Total = 0.0;
		double stkAbove30Total = 0.0;
		double stkTotalFinal = 0.0;
		
		for (Map<String, Object> stockAndOsRpt : stockAndOsRptList) {
			if (stockAndOsRpt.get("OS0_30") != null) {
				os0_30Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS0_30")));
			}
			if (stockAndOsRpt.get("OS31_60") != null) {
				os31_60Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS31_60")));
			}
			if (stockAndOsRpt.get("OS61_90") != null) {
				os61_90Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS61_90")));
			}
			if (stockAndOsRpt.get("OS91_120") != null) {
				os91_120Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS91_120")));
			}
			if (stockAndOsRpt.get("OS121_more") != null) {
				os121_moreTotal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS121_more")));
			}
			if (stockAndOsRpt.get("onAC") != null) {
				onACTotal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("onAC")));
			}
			if (stockAndOsRpt.get("OSTotal") != null) {
				osTotalFinal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OSTotal")));
			}
			if (stockAndOsRpt.get("stkUpTo30") != null) {
				stkUpTo30Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("stkUpTo30")));
			}
			if (stockAndOsRpt.get("stkAbove30") != null) {
				stkAbove30Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("stkAbove30")));
			}
			if (stockAndOsRpt.get("stkTotal") != null) {
				stkTotalFinal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("stkTotal")));
			}
		}
		
		
		Map<String, Object> stockAndOsRptTotals = new HashMap<>();
		stockAndOsRptTotals.put("OS0_30Total", os0_30Total);
		stockAndOsRptTotals.put("OS31_60Total", os31_60Total);
		stockAndOsRptTotals.put("OS61_90Total", os61_90Total);
		stockAndOsRptTotals.put("OS91_120Total", os91_120Total);
		stockAndOsRptTotals.put("OS121_moreTotal", os121_moreTotal);
		stockAndOsRptTotals.put("onACTotal", onACTotal);
		stockAndOsRptTotals.put("OSTotalFinal", osTotalFinal);
		stockAndOsRptTotals.put("stkUpTo30Total", stkUpTo30Total);
		stockAndOsRptTotals.put("stkAbove30Total", stkAbove30Total);
		stockAndOsRptTotals.put("stkTotalFinal", stkTotalFinal);
		
		if (!stockAndOsRptList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("stockAndOsRptList", stockAndOsRptList);
			map.put("stockAndOsRptTotals", stockAndOsRptTotals);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getHoStockAndOsRpt", method=RequestMethod.POST)
	public @ResponseBody Object getHoStockAndOsRpt(@RequestBody Map<String, Object> initParam){
		User user = (User) httpSession.getAttribute("currentUser");	
		logger.info("UserID = "+user.getUserId()+" : Enter into getHoStockAndOsRpt() : BranchID = "+initParam.get("branchId") + " : CustID = "+initParam.get("custId") + " : UpTo Date = "+initParam.get("upToDt"));
		
		Map<String, Object> resultMap = new HashMap<>();
		List<Map<String, Object>> stockAndOsRptList = reportDAO.getHoStockAndOsRpt(user, initParam); 
		logger.info("UserID = "+user.getUserId()+" : StockAndOs Report List Size = "+stockAndOsRptList.size());
		
		//calculate totals
		double os0_30Total = 0.0;
		double os31_60Total = 0.0;
		double os61_90Total = 0.0;
		double os91_120Total = 0.0;
		double os121_moreTotal = 0.0;
		double onACTotal = 0.0;
		double osTotalFinal = 0.0;
		double stkUpTo30Total = 0.0;
		double stkAbove30Total = 0.0;
		double stkTotalFinal = 0.0;
		
		for (Map<String, Object> stockAndOsRpt : stockAndOsRptList) {
			if (stockAndOsRpt.get("OS0_30") != null) {
				os0_30Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS0_30")));
			}
			if (stockAndOsRpt.get("OS31_60") != null) {
				os31_60Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS31_60")));
			}
			if (stockAndOsRpt.get("OS61_90") != null) {
				os61_90Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS61_90")));
			}
			if (stockAndOsRpt.get("OS91_120") != null) {
				os91_120Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS91_120")));
			}
			if (stockAndOsRpt.get("OS121_more") != null) {
				os121_moreTotal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OS121_more")));
			}
			if (stockAndOsRpt.get("onAC") != null) {
				onACTotal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("onAC")));
			}
			if (stockAndOsRpt.get("OSTotal") != null) {
				osTotalFinal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("OSTotal")));
			}
			if (stockAndOsRpt.get("stkUpTo30") != null) {
				stkUpTo30Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("stkUpTo30")));
			}
			if (stockAndOsRpt.get("stkAbove30") != null) {
				stkAbove30Total += Double.parseDouble(String.valueOf(stockAndOsRpt.get("stkAbove30")));
			}
			if (stockAndOsRpt.get("stkTotal") != null) {
				stkTotalFinal += Double.parseDouble(String.valueOf(stockAndOsRpt.get("stkTotal")));
			}
		}
		
		
		Map<String, Object> stockAndOsRptTotals = new HashMap<>();
		
		stockAndOsRptTotals.put("OS0_30Total", os0_30Total);
		stockAndOsRptTotals.put("OS31_60Total", os31_60Total);
		stockAndOsRptTotals.put("OS61_90Total", os61_90Total);
		stockAndOsRptTotals.put("OS91_120Total", os91_120Total);
		stockAndOsRptTotals.put("OS121_moreTotal", os121_moreTotal);
		stockAndOsRptTotals.put("onACTotal", onACTotal);
		stockAndOsRptTotals.put("OSTotalFinal", osTotalFinal);
		stockAndOsRptTotals.put("stkUpTo30Total", stkUpTo30Total);
		stockAndOsRptTotals.put("stkAbove30Total", stkAbove30Total);
		stockAndOsRptTotals.put("stkTotalFinal", stkTotalFinal);
		
		if (!stockAndOsRptList.isEmpty()) {
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("stockAndOsRptList", stockAndOsRptList);
			resultMap.put("stockAndOsRptTotals", stockAndOsRptTotals);
		} else {
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("UserID = "+user.getUserId() + " : Exit from /getHoStockAndOsRpt : Result = "+resultMap);
		return resultMap;
	}
	
}
