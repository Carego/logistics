'use strict';

var app = angular.module('application');

app.controller('StnOdrInvCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.Stnry={};
	
	$scope.getStnOdrInvData = function(){
		console.log("enter into getStnOdrInvData function");	
		var response = $http.post($scope.projectName+'/getStnOdrInvData');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.Stnry=data.stnOdrInvService.stationary;
				$scope.Stnry.stOdrDt=$filter('date')(data.stnOdrInvService.stationary.stOdrDt,'MM/dd/yyyy');
				console.log("$scope.Stnry"+$scope.Stnry.stCode);
				$scope.cnmtSeq = data.stnOdrInvService.cnmtSeq;
				$scope.chlnSeq = data.stnOdrInvService.chlnSeq;
				$scope.sedrSeq = data.stnOdrInvService.sedrSeq;
			}
		});
	}
	
	$scope.getStnOdrInvData();
	
}]);