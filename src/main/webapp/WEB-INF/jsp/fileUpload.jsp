<div class="row" id="docUploadDBId" >
      			
      			<div class="row" >
					<div class="input-field col s6">
						<input type="file" class="teal white-text btn" name="bankExcelName" file-model="bankExcel" required>
						<label>Choose File</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadFile(bankExcel)">Upload</a>
					</div>
				</div>
				
				<div class="row" >
				<form method="post" action="dwnldExcelFile" enctype="multipart/form-data">
				
					<div class="input-field col s3">
						<input type="date"  name="date" ng-model="date" required>
						<label>Date</label>
					</div>
					
					<div class="input-field col s3">
						<input type="text"  name="fileName" ng-model="fileName" ng-click="getFiles()" readonly required>
						<label>File Name</label>
					</div>
					<div class="input-field col s3">
						<input type="number"  name="bsuIdName" ng-model="bsuId" readonly required>
						<label>File Number</label>
					</div>
					<div class="col s12 center">
					<input type="submit" id="printXlsId" value="Download" >
			  	
			</div>
			</form>
				</div>
      			
				
				
		<div id ="filesId" ng-hide="filesDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr> 	  	  	  
 	  	  		<th></th>
 	  	  	  <th>File Name</th>
 	  	  	  <th>file No.</th>	  	    
 	  	  </tr>
		  <tr ng-repeat="obj in fileList | filter:filterTextbox1">
		  	  <td><input type="radio" name="code" value="{{ obj }}" ng-click="saveFile(obj)"></td>		 	 
              <td>{{obj.fileName}}</td>
              <td>{{obj.bsuId}}</td>
          </tr>
      </table> 
	</div>
				
				
				
</div>