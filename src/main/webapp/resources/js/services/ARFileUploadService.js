'use strict';

angular.module('application').service('ARFileUploadService', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, res){
    	console.log("enter into FileUploadService fucntion");
        var fd = new FormData();
        fd.append('file', file);
        fd.append('id',res.cnmt.cnmtId);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	console.log("message from server after image = "+data);
        	if (data.result === "success") {
        		toast(res.cnmt.cnmtCode+" image successfully inserted", 4000);
			}else{
				toast(res.cnmt.cnmtCode+" image not inserted at the time of entry",4000);
			}
        	/*alert(data);*/
        	
        })
        .error(function(data){
        	/*alert(data);*/
        	toast(data, 4000);
        });
    }
}]);