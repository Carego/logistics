package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.DAO.VehicleVendorDAO;
import com.mylogistics.model.UserRights;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VehVendorService;

@Controller
public class VehicleVendorCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private VehicleVendorDAO vehicleVendorDAO;
	
	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@RequestMapping(value = "/getOwnNameCodeId", method = RequestMethod.POST)
	public @ResponseBody Object getOwnNameCodeId() {
		System.out.println("Enter into getOwnNameCodeId---->");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> ownerList = ownerDAO.getOwnNameCodeId();
		
		if (!ownerList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("ownerList", ownerList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getBrkNameCodeId", method = RequestMethod.POST)
	public @ResponseBody Object getBrkNameCodeId() {
		System.out.println("Enter into getBrkNameCodeId---->");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> brokerList = brokerDAO.getBrkNameCodeId();
		
		if (!brokerList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("brokerList", brokerList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/vehVenValid", method = RequestMethod.POST)
	public @ResponseBody Object vehVenValid(@RequestBody VehVendorService vehVendorService) {
		System.out.println("Enter into saveAtmCard---->");
		Map<String, Object> map = new HashMap<>();

		//-2. if both has no pan card
		//-1. if vehicle no is not valid
		//0. exception
		//1. if every thing ok
		
		int temp = vehicleVendorDAO.validateVehicle(vehVendorService);
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else if (temp == 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorResult", "Exception at Database");
		} else if (temp == -1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorResult", "Vehicle no already exist");
		} else if (temp == -2) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorResult", "Both Owner & Broker has no PAN Card");
		}
		
		return map;
	}
	
	@RequestMapping(value = "/saveVehVenMstr", method = RequestMethod.POST)
	public @ResponseBody Object saveVehVenMstr(@RequestBody VehVendorService vehVendorService) {
		System.out.println("Enter into saveVehVenMstr---->");
		Map<String, Object> map = new HashMap<>();

		/*System.out.println("ownerId: "+vehVendorService.getOwnId());
		System.out.println("brokerId: "+vehVendorService.getBrkId());
		System.out.println("Mob No: "+vehVendorService.getVehVenMstr().getVvDriverMobNo());
		System.out.println("DL No: "+vehVendorService.getVehVenMstr().getVvDriverDLNo());*/
		
		int temp = vehicleVendorDAO.saveVehVenMstr(vehVendorService);
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getOwnBrkDriRC", method = RequestMethod.POST)
	public @ResponseBody Object getOwnBrkDriRC() {
		System.out.println("Enter into getOwnBrkDriRC---->");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> vehVendorList = new ArrayList<>();

		List<VehicleVendorMstr> vehicleVendorMstrList = vehicleVendorDAO.getVehicleVendorList();
		
		for (VehicleVendorMstr vehicleVendorMstr : vehicleVendorMstrList) {
			Map<String, Object> vehVenMap = new HashMap<>();
			vehVenMap.put("vvId", vehicleVendorMstr.getVvId());
			vehVenMap.put("rcNo", vehicleVendorMstr.getVvRcNo());
			vehVenMap.put("ownName", vehicleVendorMstr.getOwner().getOwnName());
			if(vehicleVendorMstr.getBroker() != null)
			vehVenMap.put("brkName", vehicleVendorMstr.getBroker().getBrkName());
			vehVenMap.put("driName", vehicleVendorMstr.getVvDriverName());
			
			vehVendorList.add(vehVenMap);
		}
		
		if (!vehVendorList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("vehVendorList", vehVendorList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		vehicleVendorMstrList.clear();
		
		return map;
	}
	
	@RequestMapping(value = "/getVehicleVendor", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleVendor(@RequestBody String vvId) {
		System.out.println("Enter into getVehicleVendor vvId---->"+vvId);
		Map<String, Object> map = new HashMap<>();

		VehicleVendorMstr vehicleVendorMstr = vehicleVendorDAO.getVehVendor(vvId);
		
		if (vehicleVendorMstr != null) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("vehVendor", vehicleVendorMstr);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getVehicleVendorByRC", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleVendorByRC(@RequestBody String rcNo) {
		System.out.println("Enter into getVehicleVendor vvId---->"+rcNo);
		Map<String, Object> map = new HashMap<>();

		VehicleVendorMstr vehicleVendorMstr = vehicleVendorDAO.getVehVendorByRc(rcNo);
		
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		
		if (vehicleVendorMstr != null) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			vehicleVendorMstr.getVvType();
			String vehicleType=vehicleTypeDAO.getVehType(vehicleVendorMstr.getVvType());
			map.put("vt", vehicleType);
			map.put("vehVendor", vehicleVendorMstr);
			map.put("isUrOwner", userRights.isUrOwner());
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/updateVehVenMstr", method = RequestMethod.POST)
	public @ResponseBody Object updateVehVenMstr(@RequestBody VehVendorService vehVendorService) {
		System.out.println("Enter into updateVehVenMstr---->");
		Map<String, Object> map = new HashMap<>();

		//-2. if both has no pan card
		//-1. if vehicle no is not valid
		//0. exception
		//1. if every thing ok
		vehVendorService.setVvRcNo("-1111111111");		
		int temp = vehicleVendorDAO.validateVehicle(vehVendorService);
		if (temp == 1) {
			temp = vehicleVendorDAO.updateVehVenMstr(vehVendorService);
			if (temp > 0) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				map.put("errorResult", "Exception at Database");
			}
		} else if (temp == 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorResult", "Exception at Database");
		} else if (temp == -1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorResult", "Vehicle no already exist");
		} else if (temp == -2) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorResult", "Both Owner & Broker has no PAN Card");
		}
		
		return map;
	}
	
	@RequestMapping(value = "/vvRcOwnBrk", method = RequestMethod.POST)
	public @ResponseBody Object vvRcOwnBrk(){
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, String>> rcOwnBrkList = vehicleVendorDAO.getRcOwnBrkList();
		
		if (!rcOwnBrkList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("rcOwnBrkList", rcOwnBrkList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}
		return map;
	}
	
	@RequestMapping(value="/getOwnBrkFrVehicle", method=RequestMethod.POST)
	public @ResponseBody Object getOwnBrkFrVehicle(@RequestBody String rcNo){
		System.out.println("VehicleVendorCntlr.getOwnBrkFrVehicle()");
		Map<String, Object> map = new HashMap<>();
		
		map = vehicleVendorDAO.getOwnBrkFrVehicle(rcNo);
		
		if ((int) map.get("temp") > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	
	@RequestMapping(value = "/getOwnNameCodeIdByName", method = RequestMethod.POST)
	public @ResponseBody Object getOwnNameCodeIdByName(@RequestBody String ownName) {
		System.out.println("Enter into getOwnNameCodeId---->");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> ownerList = ownerDAO.getOwnNameCodeIdByName(ownName);
		
		if (!ownerList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("ownerList", ownerList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getBrkNameCodeIdByName", method = RequestMethod.POST)
	public @ResponseBody Object getBrkNameCodeIdByName(@RequestBody String brkName) {
		System.out.println("Enter into getBrkNameCodeIdByName---->");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> brokerList = brokerDAO.getBrkNameCodeIdByName(brkName);
		
		if (!brokerList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("brokerList", brokerList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
}
