'use strict';

var app = angular.module('application');

app.controller('AssignBankCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("AssignBankCntlr Started");
	
	$scope.bankMstrList	= [];
	$scope.assignedBankList = [];
	$scope.branchList	= [];
	$scope.branchBMList	= [];
	
	$scope.bankMstr	= {};
	$scope.branch = {};
	$scope.branchBankMstr = {};
	$scope.dissAssBankMstr = {};
	
	$scope.bankMstrIndex = -1;
	$scope.branchBMIndex = -1;
	$scope.dissAssBankIndex = -1;
	
	$scope.bankDBFlag = true;
	$scope.branchDBFlag = true;
	$scope.assignBankFlag = true;
	$scope.dissBankFlag = true;
	$scope.dissAssBankFlag = true;
	
	$scope.assignBankRBFlag = false;
	$scope.dissociateBankRBFlag = false;
	$scope.bankDissRBFlag = false;
	
	$scope.showTableFlag = false;
	
	
	$scope.getBankMstr = function(){
		console.log("getBankMstr Entered");
		var response = $http.post($scope.projectName+'/getBankMstr');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getBankMstr Success");
				$scope.bankMstrList = data.bankMstrList;
				for ( var i = 0; i < $scope.bankMstrList.length; i++) {
					console.log("Bank Name: "+$scope.bankMstrList[i].bnkName);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankDBFlag = false;
		$('div#bankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankDBFlag = true;
			}
		});

		$('div#bankDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr, bankMstrIndex){
		console.log("bankMstr.bnkName----->"+bankMstr.bnkName);
		console.log("bankMstr.bnkFaCode----->"+bankMstr.bnkFaCode);
		console.log("bankMstrIndex----->"+bankMstrIndex);
		$scope.bankMstr = bankMstr;
		$scope.bankMstrIndex = bankMstrIndex;
		$('div#bankDB').dialog('close');
		$scope.getAllBranchesList();
		
	}
	
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				if ($scope.assignBankRBFlag) {
					$('#selectBranchId').removeAttr("disabled");
				}
				
				for ( var i = 0; i < $scope.branchList.length; i++) {
					console.log("Branch Name: "+$scope.branchList[i].branchName);
					console.log("Branch Code: "+$scope.branchList[i].branchCode);
					console.log("Branch Address: "+$scope.branchList[i].branchAdd);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch = branch;
		$scope.showTableFlag = false;
		$('div#branchDB').dialog('close');
	}
	
	/*Final Submit*/
	$scope.assignBank = function(assignBankForm){
		if(assignBankForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			console.log("final submittion");
			$scope.assignBankFlag = false;
			$('div#assignBankDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.assignBankFlag = true;
				}
			});
			$('div#assignBankDB').dialog('open');
		}
	};
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#assignBankDB').dialog('close');
	}
	
	$scope.assignBankToBranch = function(){
		console.log("enter into assignBankToBranch");
		$('div#assignBankDB').dialog('close');
		
		var bankBranchService = {
				"bankMstr"		: $scope.bankMstr,
				"branch"		: $scope.branch
		};
		
		var response = $http.post($scope.projectName+'/assignBankToBranch',bankBranchService);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast("Success");
				console.log("assignBankToBranch: Success");
				
				//clear data
				$scope.bankMstr		= {};
				$scope.branch		= {};
				
				//remove object from list
				$scope.bankMstrList.splice($scope.bankMstrIndex, 1);
				
			}else{
				console.log("submitBankMster: Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	/*final submit Dissociate*/
	
	$scope.dissBankSubmit = function(dissBankForm){
		console.log("entered into dissBankSubmit function------>");
		if(dissBankForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			var response = $http.post($scope.projectName+'/getChildBankMstrList', $scope.branch.branchId);
			response.success(function(data, status, headers, config){
				if (data.result === "success") {
					$scope.branchBMList = data.bankMstrList;
					$scope.showTableFlag = true;
				}else{
					$scope.showTableFlag = false;
					console.log("Eror");
					$scope.alertToast("No Bank assigned to this branch");
				}
			});
			response.error(function(data, status, headers, config){
				console.log("dissBank Error: "+data);
			});
		}
		
	}

	$scope.dissBank = function(bankMstr, branchBMIndex){
		
		$scope.branchBankMstr = bankMstr;
		$scope.branchBMIndex = branchBMIndex;
		
		console.log("final submittion");
		$scope.dissBankFlag = false;
		$('div#dissBankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.dissBankFlag = true;
			}
		});
		$('div#dissBankDB').dialog('open');
		
	};
	
	$scope.dissBack = function(){
		console.log("enter into back function");
		$('div#dissBankDB').dialog('close');
	}
	
	$scope.dissBankFromBranch = function(){
		console.log("enter into dissBankFromBranch");
		$('div#dissBankDB').dialog('close');
		
		var bankBranchService = {
				"bankMstr"		: $scope.branchBankMstr,
				"branch"		: $scope.branch
		 };
		
		var response = $http.post($scope.projectName+'/dissBankFromBranch',bankBranchService);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast("Success");
				console.log("assignBankToBranch: Success");
				
				//clear data
				$scope.branchBankMstr = {};
				$scope.branch = {};
				$scope.bankMstrList = [];
				
				//remove object from list
				$scope.branchBMList.splice($scope.branchBMIndex, 1);
				$scope.getBankMstr();
				
			}else{
				console.log("submitBankMster: Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	/*assigned bank list*/
	$scope.getAssignedBank = function(){
		console.log("getAssignedBank Entered");
		var response = $http.post($scope.projectName+'/getAssignedBank');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("assignedBankList Success");
				$scope.assignedBankList = data.assignedBankList;
				console.log("size of list: "+data.assignedBankList.length);
				for ( var i = 0; i < $scope.assignedBankList.length; i++) {
					console.log("Bank Name: "+$scope.assignedBankList[i].bnkName);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getAssignedBank Error: "+data);
		});
	}
	
	$scope.assignBankRB = function(){
		console.log("entered into: assignBankRB()");
		if ($scope.bankMstrList.length < 1) {
			$scope.getBankMstr();
			console.log("getBankMstr() called: ")
		}else{
			console.log("getBankMstr() not called: ")
		}
		
		$scope.assignBankRBFlag = true;
		$scope.dissociateBankRBFlag = false;
		$scope.bankDissRBFlag = false;
		$scope.showTableFlag = false;
		$scope.branchBMList = [];
		$scope.branch = {};
		
		console.log("assignBankRBFlag: "+$scope.assignBankRBFlag);
		console.log("dissociateBankRBFlag: "+$scope.dissociateBankRBFlag);
		console.log("bankDissRBFlag: "+$scope.bankDissRBFlag);
	}
	
	$scope.dissociateBankRB = function(){
		console.log("entered into: dissociateBankRB");
		if ($scope.branchList.length < 1) {
			$scope.getAllBranchesList();
			console.log("getAssignedBank() called: ")
		}else{
			console.log("getAssignedBank() not called: ")
		}
		
		$scope.assignBankRBFlag = false;
		$scope.dissociateBankRBFlag = true;
		$scope.bankDissRBFlag = false;
		$scope.branch = {};
		
		console.log("assignBankRBFlag: "+$scope.assignBankRBFlag);
		console.log("dissociateBankRBFlag: "+$scope.dissociateBankRBFlag);
		console.log("bankDissRBFlag: "+$scope.bankDissRBFlag);
	}
	
	$scope.bankDissRB = function(){
		console.log("entered into: bankDissRB");
		if ($scope.assignedBankList.length < 1) {
			$scope.getAssignedBank();
			console.log("assignedBankList called: ")
		}else{
			console.log("assignedBankList not called: ")
		}
		$scope.assignBankRBFlag = false;
		$scope.dissociateBankRBFlag = false;
		$scope.bankDissRBFlag = true;
		$scope.showTableFlag = false;
		$scope.branch = {};
		
		console.log("assignBankRBFlag: "+$scope.assignBankRBFlag);
		console.log("dissociateBankRBFlag: "+$scope.dissociateBankRBFlag);
		console.log("bankDissRBFlag: "+$scope.bankDissRBFlag);
	}
	
	$scope.dissAssignedBank = function(bankMstr, dissAssBankIndex){
		
		$scope.dissAssBankMstr = bankMstr;
		$scope.dissAssBankIndex = dissAssBankIndex;
		
		console.log("final submittion");
		$scope.dissAssBankFlag = false;
		$('div#dissAssBankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.dissAssBankFlag = true;
			}
		});
		$('div#dissAssBankDB').dialog('open');
		
	};
	
	$scope.dissAssBack = function(){
		console.log("enter into back function");
		$('div#dissAssBankDB').dialog('close');
	}
	
	$scope.dissAssBank = function(){
		console.log("enter into dissAssBank");
		$('div#dissAssBankDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/dissAssBank', $scope.dissAssBankMstr);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast("Success");
				console.log("dissAssBank: Success");
				
				//clear data
				$scope.dissAssBankMstr = {};
				
				//remove object from list
				$scope.assignedBankList.splice($scope.dissAssBankIndex, 1);
				$scope.getBankMstr();
				
			}else{
				console.log("dissAssBank: Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	console.log("AssignBankCntlr Ended");
}]);