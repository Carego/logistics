package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmt;
import com.mylogistics.model.SubElectricityMstr;

@Repository
public interface SubElectricityMstrDAO {

	public List<SubElectricityMstr> getSubFileDetails(List<CashStmt> reqCsList);
}
