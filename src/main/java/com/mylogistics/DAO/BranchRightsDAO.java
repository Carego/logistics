package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.BranchRights;

public interface BranchRightsDAO {
	
	public int saveBrRights(BranchRights branchRights);
	
	//Satarupa: get the list of all branchrights where isAllow=yes.
	public List<BranchRights> getBranchRightsIsAllow();
	
	//satarupa: updating the isAllow  of branchrights table field to "no".
	public int updateBrRightsIsAllowNo(int contIdsInt[]);
}
