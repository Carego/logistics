<div ng-show="operatorLogin || superAdminLogin" title="Branch Stock Information">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		
		<!-- Find By Branch Form -->
		<form name="TrafficOffForm" ng-submit="submitTrafficOffForm(TrafficOffForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="branchFaCode" name="branchFaCode" ng-model="branchFaCode" ng-click="OpenBranchDB()" readonly="readonly">
		       		<input type ="hidden" id="branchCode" name="branchCode" ng-model="trafficOff.branchCode"/>
					<label>Branch</label>
				</div>       	
				
				<div class="col s3 input-field">
		    		<select name="typeName" id="officerName" ng-model="trafficOff.officerName">
						<option ng-repeat="name in trafficOfficerNames">{{name}}</option>
					</select>
					<label>Officer Name</label>
				</div>
				
				<div class="input-field col s3">
					<input type="date" name="fromDate" id="fromDate" ng-model="trafficOff.fromDate"> <label>From Date</label>
				</div>
				
				<div class="input-field col s3">
					<input type="date" name="toDate" id="toDate" ng-model="trafficOff.toDate"> <label>To Date</label>
				</div>
				
				
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" id="submit" name ="submit" value="Enquiry" >
	       		</div>
			</div>		
			
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<!-- Enquiry Table -->
	<div ng-hide="enquiryTable">
		<table class="table">
 	    	<caption class="coltag tblrow">Traffic Officer Detail</caption> 	    
 	  	  <tr>
 	  	  		<th class="rowcelheader">S.No.</th>
 	  	  		<th class="rowcelheader">Branch Name</th>
 	  	  		<th class="rowcelheader">Officer Name</th>
 	  	  		<th class="rowcelheader">Cust. Name</th>
 	  	  		<th class="rowcelheader">Source</th>
 	  	  		<th class="rowcelheader">Destination</th>
 	  	  		<th class="rowcelheader">Quantity</th>
 	  	  		<th class="rowcelheader">Type</th>
 	  	  		<th class="rowcelheader">Vehicle Type</th>
 	  	  		<th class="rowcelheader">VendorDetail</th>
 	  	  </tr>
 	  	  <tr ng-repeat="obj in trafficOffList">
				<td class="rowcelwhite">{{$index +1}}</td>
				<td class="rowcelwhite">{{obj.branchName}}</td>
				<td class="rowcelwhite">{{obj.officerName}}</td>
				<td class="rowcelwhite">{{obj.customerName}}</td>
				<td class="rowcelwhite">{{obj.sourceName}}</td>
				<td class="rowcelwhite">{{obj.destinationName}}</td>
				<td class="rowcelwhite">{{obj.quantity}}</td>
				<td class="rowcelwhite">{{obj.metricOrTon}}</td>
				<td class="rowcelwhite">{{obj.vehicleType}}</td>
				<td class="rowcelwhite">
					<table>
						<tr>
							<th>S.No.</th>
							<th>Vend. Name</th>
							<th>Vend. Type</th>
							<th>Mobile</th>
							<th>Rate</th>
							<th>IsSelected</th>
							<th>Reason</th>
						</tr>
						<tr ng-repeat="venDt in obj.vendorDetails">
							<td>{{$index+1}}</td>
							<td>{{venDt.vendorName}}</td>
							<td>{{venDt.vendorType}}</td>
							<td>{{venDt.mobile}}</td>
							<td>{{venDt.rate}}</td>
							<td>{{venDt.isSelected}}</td>
							<td>{{venDt.reason}}</td>
						</tr>
					</table>
				</td>
          </tr>
      </table> 
	</div>	
		
	
	

</div>