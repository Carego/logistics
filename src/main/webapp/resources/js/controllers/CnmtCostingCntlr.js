'use strict';

var app = angular.module('application');

app.controller('CnmtCostingCntlr', ['$scope','$location','$http','$filter','$q',
		function($scope, $location, $http, $filter, $q) {

			console.log("CnmtCostingCntlr started");

			$scope.brhOpenFlag = true;
			$scope.isConsolidate = false;
			$scope.isBranch = false;
			$scope.custOpenFlag = true;
			$scope.isFormSubmitted = false;

			$scope.getBankCsInfo = function() {
				console.log("enter into getBankCsInfo function");
				var response = $http.post($scope.projectName + '/getAllBranchesList');
				response.success(function(data, status, headers, config) {
					if (data.result === "success") {
						$scope.brhList = data.branchCodeList;
					} else {
						$scope.brhList = [];
						$scope.alertToast("Server error");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}

			$scope.consolidateRB = function() {
				console.log("consolidateRB()");
				$scope.branch = {};
				$scope.isConsolidate = true;
				$scope.isBranch = false;
				$scope.isConBrh = "con"
				$('#branchNameId').attr("disabled", "disabled");

			}

			$scope.branchRB = function() {
				console.log("branchRB()");
				$scope.isConsolidate = false;
				$scope.isBranch = true;
				$scope.isConBrh = "brh"
				$('#branchNameId').removeAttr("disabled");
			}

			$scope.openBranchDB = function() {
				$scope.getBankCsInfo();
				console.log("enter into openBranchDB function");
				$scope.brhOpenFlag = false;
				$('div#brhOpenDB').dialog({
					autoOpen : false,
					modal : true,
					resizable : false,
					title : "Bank Code",
					show : UDShow,
					hide : UDHide,
					position : UDPos,
					draggable : true,
					close : function(event, ui) {
						$(this).dialog('destroy');
						$(this).hide();
						$scope.brhOpenFlag = true;
					}
				});

				$('div#brhOpenDB').dialog('open');
			}

			$scope.saveBrhCode = function(brh) {
				console.log("enter into saveBrhCode function");
				$scope.branch = brh;
				$('div#brhOpenDB').dialog('close');
			}

			$scope.openCustDB = function() {
				$scope.getCustomer();
				console.log("enter into openCustDB  function");
				$scope.custOpenFlag = false;
				$('div#custOpenDB').dialog({
					autoOpen : false,
					modal : true,
					resizable : false,
					title : "Customer Code",
					show : UDShow,
					hide : UDHide,
					position : UDPos,
					draggable : true,
					close : function(event, ui) {
						$(this).dialog('destroy');
						$(this).hide();
						$scope.brhOpenFlag = true;
					}
				});

				$('div#custOpenDB').dialog('open');
			}

			$scope.saveCustCode = function(cust) {
				console.log("enter into saveBrhCode function");
				$scope.custCode = cust.custCode;
				$scope.customerName = cust.custName;
				$('div#custOpenDB').dialog('close');
			}

			$scope.getCustomer = function() {
				console.log("getCustomer------>Kamal");
				var response = $http.get($scope.projectName	+ '/getCustomerList');
				response.success(function(data, status, headers, config) {
					if (data.result === "success") {
						$scope.customerList = data.list;
					} else {
						$scope.alertToast("you don't have any customer");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}

			$scope.sendABC = function(ABCForm) {
				if (ABCForm.$invalid) {
					$scope.isFormSubmitted = true;
				} else {
					$scope.isFormSubmitted = false;
					$("#ABC").attr("action", "getCostingReportXLSX");
					$("#ABC").trigger('submit');
				}
			}

			$scope.costing = function() {

				var detail = {
					"branch" : $scope.branch.branchId,
					"frmDt" : $scope.frmDt,
					"toDt" : $scope.toDt,
					"isBranch" : $scope.isBranch,
					"isConsolidate" : $scope.isConsolidate,
					"custCode" : $scope.custCode
				}
				$('#submitId').attr("disabled", "disabled");

				var response = $http.post($scope.projectName + '/cnmtCosting',
						detail);
				response.success(function(data, status, headers, config) {
					$('#submitId').removeAttr("disabled");
					if (data.result === "success") {
						$scope.alertToast(" success");
						console.log(data);
					} else {
						$scope.alertToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
					$('#submitId').removeAttr("disabled");
				});
			}
			
			$scope.getStationFrom = function(keyCode){
				console.log("Enter into getStationData()");
				if(keyCode === 8 || parseInt($scope.frmStationCode.length) < 2)
					return;	
				
				var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.frmStationCode);
				response.success(function(data, status, headers, config){
					console.log(data);
					if(data.result==="success"){
						$scope.frmStationCode = "";
						$scope.stationList = data.stationList;
						$scope.OpenCnmtFromStationDB();
					}else{
						$scope.alertToast("you don't have any station");
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					console.log("Error in hitting /getStationDataByNameCode");
				});
				console.log("Exit from getStationData()");
			}
			
			$scope.CnmtFromStationDBFlag=true;
			
			$scope.OpenCnmtFromStationDB = function(){
				$scope.CnmtFromStationDBFlag=false;
				$('div#cnmtFromStationDB').dialog({
					autoOpen: false,
					modal:true,
					title: "Cnmt From Station",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});

				$('div#cnmtFromStationDB').dialog('open');
			}
			
			$scope.CnmtToStationDBFlag=true;
			
			$scope.OpenCnmtToStationDB = function(){
				$scope.CnmtToStationDBFlag=false;
				$('div#cnmtToStationDB').dialog({
					autoOpen: false,
					modal:true,
					title: "Cnmt To Station",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
						$scope.CnmtToStationDBFlag=true;
					}
				});

				$('div#cnmtToStationDB').dialog('open');
			}
			
			$scope.getStationTo = function(keyCode){
				console.log("Enter into getStationData()");
				if(keyCode === 8 || parseInt($scope.toStationCode.length) < 2)
					return;	
				var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.toStationCode);
				response.success(function(data, status, headers, config){
					console.log(data);
					if(data.result==="success"){
						$scope.toStationCode = "";
						$scope.stationList = data.stationList;
						$scope.OpenCnmtToStationDB();
					}else{
						$scope.alertToast("you don't have any station");
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					console.log("Error in hitting /getStationDataByNameCode");
				});
				console.log("Exit from getStationData()");
			}
			
			$scope.frmStationCode = "";
			$scope.toStationCode = "";
			
			$scope.saveToStnCode = function(station){
				$scope.toStationCode = station.stnCode;
				$scope.CnmtToStationDBFlag=true;
				$('div#cnmtToStationDB').dialog('close');
			}
			
			$scope.saveFrmStnCode = function(station){
				$scope.frmStationCode = station.stnCode;
				$scope.CnmtFromStationDBFlag=true;
				$("#cnmtFromStationDB").dialog("close");
			}

		} ]);