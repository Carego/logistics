package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name = "station")
public class Station {

	
	@Id
	@Column(name="stnId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stnId;
	
	@Column(name="stnCode")
	private String stnCode;
	
	@Column(name="stnName")
	private String stnName;
	
	@Column(name="stateCode")
	private String stateCode;
	
	@Column(name="stnDistrict")
	private String stnDistrict;
	
	@Column(name="stnPin")
	private String stnPin;
	
	@Column(name="stnCity", length=75)
	private String stnCity;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;
	
	//Many To One
    @ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "stateId") 
	private State state;
	
	public String getStnPin() {
		return stnPin;
	}

	public void setStnPin(String stnPin) {
		this.stnPin = stnPin;
	}

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;

   	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(Calendar creationTS) {
        this.creationTS = creationTS;
    }

	public int getStnId() {
		return stnId;
	}

	public void setStnId(int stnId) {
		this.stnId = stnId;
	}

	public String getStnCode() {
		return stnCode;
	}

	public void setStnCode(String stnCode) {
		this.stnCode = stnCode;
	}

	public String getStnName() {
		return stnName;
	}

	public void setStnName(String stnName) {
		this.stnName = stnName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStnDistrict() {
		return stnDistrict;
	}

	public void setStnDistrict(String stnDistrict) {
		this.stnDistrict = stnDistrict;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getStnCity() {
		return stnCity;
	}

	public void setStnCity(String stnCity) {
		this.stnCity = stnCity;
	}
	
	
	
}
