<div ng-show="adminLogin || superAdminLogin">

<title>Display Challan</title>
<div>
<input type="button" value="getImage" ng-click="getImage()">
	<img ng-src="data:image/JPEG;base64,{{image}}">
</div>
<div class="container">
	<div class="row">
		<div class="col s2">
			<input type="radio" name="chlnCodeList" ng-model="chlnCodeByModal"
				ng-click="enableModalTextBox()">
		</div>
		<div class="col s10">
			<input type="text" id="chlnCodeByMId" name="chlnCodeByM"
				ng-model="chlnCodeByM" disabled="disabled"
				ng-click="OpenChlnCodeDB()">
		</div>
	</div>
	<div class="row">
		<div class="col s2">
			<input type="radio" name="chlnCodeList" ng-model="chlnCodeByAuto"
				ng-click="enableAutoTextBox()">
		</div>
		<div class="col s8">
			<input type="text" id="chlnCodeByAId" name="chlnCodeByA"
				ng-model="chlnCodeByA" disabled="disabled"
				ng-keyup="getChlnCodeList()">
		</div>
		<div class="col s2">
			<input class="col s12" type="button" id="ok" value="OK"
				ng-click="getChlnList()" disabled="disabled">
		</div>
	</div>

	<div class="row">

		<div class="col s12 center">
			<input id="bkToList" type="button" value="Back To list"
				ng-click="backToList()">
		</div>
	</div>
</div>

<div class="container" id="ChlnCodeDB" ng-hide="ChlnCodeFlag">
	<input type="text" name="filterChlnCode" ng-model="filterChlnCode"
		placeholder="Search by Challan Code">
	<table>
		<tr
			ng-repeat="challanCodes in challanCodeList | filter:filterChlnCode">
			<td><input type="radio" name="ChallanName"
				value="{{ challanCodes }}" ng-model="chlnCodes"
				ng-click="saveChlnCode(challanCodes)"></td>
			<td>{{ challanCodes}}</td>
		</tr>
	</table>
</div>

<div class="container" id="ChallanTableDB" ng-hide="isViewNo">
	<input type="text" name="filterChallan"
		ng-model="filterChallan.chlnCode" placeholder="Search by Challan Code">
	<table class="table-hover table-bordered table-condensed">
		<tr>
			<th></th>

			<th>Challan Code</th>

			<th>Lorry Number</th>

			<th>User Code</th>

			<th>Time Stamp</th>
		</tr>


		<tr ng-repeat="challan in ChallanList | filter:filterChallan">
			<td><input id="{{ challan }}" type="checkbox"
				value="{{ challan.chlnCode}}"
				ng-checked="selection.indexOf(challan.chlnCode) > -1"
				ng-click="toggleSelection(challan.chlnCode)" /></td>
			<td>{{ challan.chlnCode}}</td>
			<td>{{ challan.chlnLryNo }}</td>
			<td>{{ challan.userCode }}</td>
			<td>{{ challan.creationTS }}</td>
		</tr>
	</table>
	<table>
		<tr>
			<td class="center"><input type="submit" value="Submit"
				ng-click="verifyChallan()"></td>
		</tr>
	</table>
</div>

<div class="container" ng-hide="showChallan">
	<div class="row">
		<div class="input-field col s12 m4 l3">
			<input type="text" id="chlnCode" name="chlnCode"
				ng-model="chln.chlnCode" value="{{chln.chlnCode }}" readonly>
			<label>Challan Code</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="branchCode" ng-model="chln.branchCode"
				value="{{chln.branchCode }}"> <label>Challan Branch
				Code</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnLryNo" id="chlnLryNo"
				ng-model="chln.chlnLryNo" value="{{ chln.chlnLryNo}}"> <label>Lorry
				No</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnFromStn" id="chlnFromStn"
				ng-model="chln.chlnFromStn" value="{{ chln.chlnFromStn}}"> <label>Challan
				From Station</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnToStn" id="chlnToStn"
				ng-model="chln.chlnToStn" value="{{chln.chlnToStn }}"> <label>Challan
				To Station</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnEmpCode" id="chlnEmpCode"
				ng-model="chln.chlnEmpCode" value="{{chln.chlnEmpCode }}"> <label>Employee
				Code</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnLryRate" ng-model="chln.chlnLryRate"
				value="{{ chln.chlnLryRate}}"> <label>Lorry Rate</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnChgWt" id="chlnChgWt"
				ng-model="chln.chlnChgWt" value="{{chln.chlnChgWt }}"> <label>Charge
				Weight</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnNoOfPkg" ng-model="chln.chlnNoOfPkg"
				value="{{chln.chlnNoOfPkg }}"> <label>No. Of Package</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnTotalWt" id="chlnTotalWt"
				ng-model="chln.chlnTotalWt" value="{{chln.chlnTotalWt }}"> <label>Challan
				Total Weight</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnFreight" id="chlnFreight"
				ng-model="chln.chlnFreight" value="{{chln.chlnFreight }}"> <label>Challan
				Freight(Rs.)</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnLoadingAmt" id="chlnLoadingAmt"
				ng-model="chln.chlnLoadingAmt" value="{{chln.chlnLoadingAmt }}">
			<label>Loading Amount(Rs.)</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnExtra" id="chlnExtra"
				ng-model="chln.chlnExtra" value="{{ chln.chlnExtra}}"> <label>Challan
				Extra(Rs.)</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnTotalFreight" id="chlnTotalFreight"
				ng-model="chln.chlnTotalFreight" value="{{chln.chlnTotalFreight }}">
			<label>Total Freight(Rs.)</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnAdvance" id="chlnAdvance"
				ng-model="chln.chlnAdvance" value="{{ chln.chlnAdvance}}"> <label>Challan
				Advance</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnBalance" id="chlnBalance"
				ng-model="chln.chlnBalance" value="{{chln.chlnBalance }}"> <label>Challan
				Balance</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnPayAt" id="chlnPayAt"
				ng-model="chln.chlnPayAt" value="{{ chln.chlnPayAt}}"> <label>Challan
				Pay At</label>
		</div>
		<!-- 			<div class="input-field col s12 m4 l3"><input type ="text" name ="chlnSedr" id="chlnSedr" ng-model="chln.chlnSedr" value="{{chln.chlnSedr }}">
				<label>Challan Sedr</label></div> -->
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnTimeAllow" id="chlnTimeAllow"
				ng-model="chln.chlnTimeAllow" value="{{ chln.chlnTimeAllow}}">
			<label>Time Allow</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnDt" ng-model="chln.chlnDt"
				value="{{chln.chlnDt }}"> <label>Challan Date</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnBrRate" ng-model="chln.chlnBrRate"
				value="{{chln.chlnBrRate }}"> <label>BR Rate</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnWtSlip" id="chlnWtSlip"
				ng-model="chln.chlnWtSlip" value="{{chln.chlnWtSlip }}"> <label>Weight
				Slip</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnVehicleType" id="chlnVehicleType"
				ng-model="chln.chlnVehicleType" value="{{chln.chlnVehicleType }}">
			<label>Vehicle type</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnStatisticalChg"
				ng-model="chln.chlnStatisticalChg"
				value="{{chln.chlnStatisticalChg }}"> <label>Statistical
				Charge</label>
		</div>
		<!-- <div class="input-field col s12 m4 l3"><input type ="text" name ="chdChlnCode" id="chdChlnCode" ng-model="chln.chdChlnCode" value="{{chln.chdChlnCode }}">
				<label>Challan Chd Code</label></div> -->
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnTdsAmt" id="chlnTdsAmt"
				ng-model="chln.chlnTdsAmt" value="{{ chln.chlnTdsAmt}}"> <label>Tds
				Amount</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnRrNo" ng-model="chln.chlnRrNo"
				value="{{ chln.chlnRrNo}}"> <label>Challan RR No</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnTrainNo" ng-model="chln.chlnTrainNo"
				value="{{chln.chlnTrainNo }}"> <label>Challan Train
				No</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnLryLoadTime"
				ng-model="chln.chlnLryLoadTime" value="{{ chln.chlnLryLoadTime}}">
			<label>Lorry Load Time</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnLryRepDT" ng-model="chln.chlnLryRepDT"
				value="{{chln.chlnLryRepDT }}"> <label>Lorry
				Reporting Date</label>
		</div>
		<div class="input-field col s12 m4 l3">
			<input type="text" name="chlnLryRptTime"
				ng-model="chln.chlnLryRptTime" value="{{ chln.chlnLryRptTime}}">
			<label>Lorry Reporting Time</label>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<input type="button" value="Submit"
				ng-click="EditChallanSubmit(chln)">
		</div>
	</div>




	<form method="post" action="downloadChallan"
		enctype="multipart/form-data">
		<table>
			<tr>
				<td><input type="hidden" id="submitImageId"
					name="submitImageName" value="{{ chln.chlnCode }}" size="50" /></td>
			</tr>
			<tr>
				<td><input type="submit" id="submitImage" value="download"
					disabled="disabled" /></td>
			</tr>
		</table>
	</form>
	
	
</div>
</div>