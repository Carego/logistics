<div ng-show="operatorLogin || superAdminLogin">
<title>View Vehicle Type</title>
<!-- <div ng-controller = "VehicleTypeCntlr" > -->
<div class="row">
	<div class="col s1 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s10 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
		<h4>View Vehicle Type</h4>	
		<table class="table-bordered table-hover table-condensed">
 	 		<tr>
								
				<th>Vehicle Type</th>
				
				<th>Vehicle Type Code</th>
			
				<th>VTService Type</th>
				
				<th>VTLoad Limit</th>
				
				<th>VTGuarantee Weight</th>
				
				<th>VT CreationTS</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="vehicletype in vehicletypeList">
             <td colspan="6">
             	<form name="VehicleTypeForm" ng-submit="update(VehicleTypeForm,vehicletype)">
             		<table>
             			<tr>
              				<td><input type ="text"  name ="vtVehicleType" ng-model=" vehicletype.vtVehicleType" value={{vehicletype.vtVehicleType}} ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
              				<td><input type ="text"  name ="vtCode" ng-model=" vehicletype.vtCode" value={{vehicletype.vtCode}} ng-required="true" ng-minlength="1" ng-maxlength="1"></td>
             	 			<td><input type ="text"  name ="vtServiceType" ng-model=" vehicletype.vtServiceType" value={{vehicletype.vtServiceType}} ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
              				<td><input type ="number"  name ="vtLoadLimit" ng-model=" vehicletype.vtLoadLimit" value={{vehicletype.vtLoadLimit}} ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/"  ng-maxlength="10" step="0.01" min="0.00" ></td>
              				<td><input type ="number"  name ="vtGuaranteeWt" ng-model=" vehicletype.vtGuaranteeWt" value={{vehicletype.vtGuaranteeWt}}  ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" ng-maxlength="10"  step="0.01" min="0.00"></td>
              				<td>{{vehicletype.creationTS | date : 'short'}}</td>
              				<td><input type="submit" value="Update"></td>
              			</tr>
              		</table>
              	</form>
              </td>
        </tr>
		</table>
	</div>
</div>	
</div>
<!--  <table>
 	 		<tr>
								
				<th>Vehicle Type</th>
			
				<th>VTService Type</th>
				
				<th>VTLoad Limit</th>
				
				<th>VTGuarantee Weight</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="vehicletype in vehicletypeList">
		 	  <td>{{ vehicletype.vtVehicleType }}</td>
              <td>{{ vehicletype.vtServiceType }}</td>
              <td>{{ vehicletype.vtLoadLimit }}</td>
              <td>{{ vehicletype.vtGuaranteeWt }}</td>
          </tr>
         </table> -->

<!-- </div> -->
