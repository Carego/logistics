package com.mylogistics.DAOImpl;

import java.util.ArrayList;
//import java.util.Date;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.constants.BranchSStatsCNTS;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.User;
import com.mylogistics.services.BrStkDisDetService;

public class BranchSStatsDAOImpl implements BranchSStatsDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(BranchSStatsDAOImpl.class);
	
	@Autowired
	public BranchSStatsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}	
	
	@Transactional
	public int saveBranchSStats(BranchSStats branchSStats){
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(branchSStats);
			 session.flush();
			 transaction.commit();
			 System.out.println("branchSStats Data is inserted successfully");
			 temp = 1;
			 
			
		 }
		 catch(Exception e){
				 transaction.rollback();
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	
	
	@Override
	public int saveBranchSStats(BranchSStats branchSStats,Session session){
		 int temp;
			 session.save(branchSStats);
			 temp = 1;
		 return temp;
	 }
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public double getLastMonthPerDayUsed(Date startDate,Date endDate,String type){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		int cnmtUsed=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE,type)); 
			 branchSStats = criteria.list();
			 int listSize=branchSStats.size();
			 
			 HashSet<Date> hashSet = new HashSet<Date>();
			 for(int i=0;i<branchSStats.size();i++){
				 hashSet.add(branchSStats.get(i).getBrsStatsDt());
			 }
			 
			 int hashSize=hashSet.size();
			 
			 if(hashSize > 0){
				 cnmtUsed=listSize/hashSize;
			 }			 
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.clear();
		 session.close();
		 return cnmtUsed;
	 }

	
	
	
	@Override
	public double getLastMonthPerDayUsed(Date startDate,Date endDate,String type,Session session){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		int cnmtUsed=0;
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE,type)); 
			 branchSStats = criteria.list();
			 int listSize=branchSStats.size();
			 
			 HashSet<Date> hashSet = new HashSet<Date>();
			 for(int i=0;i<branchSStats.size();i++){
				 hashSet.add(branchSStats.get(i).getBrsStatsDt());
			 }
			 
			 int hashSize=hashSet.size();
			 
			 if(hashSize > 0){
				 cnmtUsed=listSize/hashSize;
			 }			 
		 return cnmtUsed;
	 }

	
	@SuppressWarnings("unchecked")
	@Transactional
	public int getLastMnthDataOfCnmt(Date startDate,Date endDate){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		int hashSize=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,"cnmt")); 
			 branchSStats = criteria.list();
			 
			 HashSet<Date> hashSet = new HashSet<Date>();
			 for(int i=0;i<branchSStats.size();i++){
				 hashSet.add(branchSStats.get(i).getBrsStatsDt());
			 }
			 hashSize=hashSet.size();
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.clear();
		 session.close();
		 return hashSize;
	 }
	
/*	@SuppressWarnings("unchecked")
	@Transactional
	public Date getFirstEntryDateOfCnmt(){
		System.out.println("--------------------enter into getFirstEntryDate");
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		 Date date = null;
		 try{
			 session = this.sessionFactory.openSession();
			 branchSStats = session.createQuery("from BranchSStats where brsStatsType='cnmt' order by brsStatsId ").setMaxResults(1).list();
			 date = branchSStats.get(0).getBrsStatsDt();
			 System.out.println("The first date is---------------"+date);
			 session.flush();
			 
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.close();
		 return date;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Date getFirstEntryDateOfChln(){
		System.out.println("--------------------enter into getFirstEntryDate");
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		 Date date = null;
		 try{
			 session = this.sessionFactory.openSession();
			 branchSStats = session.createQuery("from BranchSStats where brsStatsType='chln' order by brsStatsId ").setMaxResults(1).list();
			 date = branchSStats.get(0).getBrsStatsDt();
			 System.out.println("The first date is---------------"+date);
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.close();
		 return date;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Date getFirstEntryDateOfSedr(){
		System.out.println("--------------------enter into getFirstEntryDate");
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		 Date date = null;
		 try{
			 session = this.sessionFactory.openSession();
			 branchSStats = session.createQuery("from BranchSStats where brsStatsType='sedr' order by brsStatsId ").setMaxResults(1).list();
			 date = branchSStats.get(0).getBrsStatsDt();
			 System.out.println("The first date is---------------"+date);
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.close();
		 return date;
	}*/
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Date getFirstEntryDate(String type){
		System.out.println("--------------------enter into getFirstEntryDate");
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		 Date date = null;
		 try{
			 session = this.sessionFactory.openSession();
			 org.hibernate.Query query  = session.createQuery("from BranchSStats where brsStatsType=:type order by brsStatsId ").setMaxResults(1);
			 query.setParameter("type",type);
			 branchSStats=query.list();
			 date = branchSStats.get(0).getBrsStatsDt();
			 System.out.println("The first date is---------------"+date);
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return date;
	}
	
	
	@Override
	public Date getFirstEntryDate(String type,Session session){
		System.out.println("--------------------enter into getFirstEntryDate");
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		 Date date = null;
			 org.hibernate.Query query  = session.createQuery("from BranchSStats where brsStatsType=:type order by brsStatsId ").setMaxResults(1);
			 query.setParameter("type",type);
			 branchSStats=query.list();
			 date = branchSStats.get(0).getBrsStatsDt();
			 System.out.println("The first date is---------------"+date);
		 return date;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public double getNoOfStationary(Date startDate,Date endDate,String type){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		double listSize=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,type)); 
			 branchSStats = criteria.list();
			 
			 listSize=branchSStats.size();
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.clear();
		 session.close();
		 return listSize;
	 }
	
	
	
	@Override
	public double getNoOfStationary(Date startDate,Date endDate,String type,Session session){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		double listSize=0;
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,type)); 
			 branchSStats = criteria.list();
			 
			 listSize=branchSStats.size();
		 return listSize;
	 }
	
	
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public double getLastMonthPerDayUsedChln(Date startDate,Date endDate){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		double cnmtUsed=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,"chln")); 
			 branchSStats = criteria.list();
			 double listSize=branchSStats.size();
			 
			 HashSet<Date> hashSet = new HashSet<Date>();
			 for(int i=0;i<branchSStats.size();i++){
				 hashSet.add(branchSStats.get(i).getBrsStatsDt());
			 }
			 double hashSize=hashSet.size();
			 
			 cnmtUsed=listSize/hashSize;
			 
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.close();
		 return cnmtUsed;
	 }*/

	
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public double getNoOfChln(Date startDate,Date endDate){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		double listSize=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,"chln")); 
			 branchSStats = criteria.list();
			 
			 listSize=branchSStats.size();
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.close();
		 return listSize;
	 }
	*/
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public double getLastMonthPerDayUsedSedr(Date startDate,Date endDate){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		double sedrUsed=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,"sedr")); 
			 branchSStats = criteria.list();
			 double listSize=branchSStats.size();
			 
			 System.out.println("list size is -------------"+listSize);
			 HashSet<Date> hashSet = new HashSet<Date>();
			 for(int i=0;i<branchSStats.size();i++){
				 hashSet.add(branchSStats.get(i).getBrsStatsDt());
			 }
			 double hashSize=hashSet.size();
			 System.out.println("hashSize size is -------------"+hashSize);
			 
			 sedrUsed=listSize/hashSize;
			 System.out.println("sedrUsed size is -------------"+sedrUsed);
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.close();
		 return sedrUsed;
	 }*/
	
	
//	@SuppressWarnings("unchecked")
//	@Transactional
//	public double getNoOfSedr(Date startDate,Date endDate){
//		
//		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
//		double listSize=0;
//		 try{
//			 session = this.sessionFactory.openSession();
//			 transaction = session.beginTransaction();
//			 Criteria criteria = session.createCriteria(BranchSStats.class);
//			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
//			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
//			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,"sedr")); 
//			 branchSStats = criteria.list();
//			 listSize=branchSStats.size();
//			 transaction.commit();
//			 session.flush();
//			
//		 }
//		 catch(Exception e){
//			 e.printStackTrace(); 
//		 }
//		 session.close();
//		 return listSize;
//	 }
	
	@Override
	public List<String> getBranchSStats(Session session, User user, String branchCode, Long startNo, Long endNo, String stnType, String custStn){
		logger.info("UserID = "+user.getUserId()+" : Enter into getBranchSStats() : BranchCode = "+branchCode+ " : StartNo = "+startNo+" : End No = "+endNo+" : StnType = "+stnType);
		List<String> list = null;
		try{
			if(branchCode.equalsIgnoreCase("23")){
				// Substring start no.
				int no = 0;
				if(branchCode.equalsIgnoreCase("23"))
					no = 5;
				
				list = session.createSQLQuery("SELECT SUBSTRING(brsStatsSerialNo, "+no+", LENGTH(brsStatsSerialNo)) " +
						"FROM branchsstats " +
						"WHERE brsStatsType = '"+stnType+"' " +
						"AND brsStatsBrCode = '"+branchCode+"' " +
						"AND CAST(SUBSTRING(brsStatsSerialNo, "+no+", LENGTH(brsStatsSerialNo)) AS UNSIGNED) >= "+startNo+" AND CAST(SUBSTRING(brsStatsSerialNo, "+no+", LENGTH(brsStatsSerialNo)) AS UNSIGNED) <="+endNo+" " +
						"ORDER BY brsStatsSerialNo ASC")
						.list();
			}else{
				list = session.createSQLQuery("SELECT brsStatsSerialNo FROM branchsstats " +
						"WHERE brsStatsType = '"+stnType+"' " +
						"AND CAST(brsStatsSerialNo AS UNSIGNED)>= "+startNo+" AND CAST(brsStatsSerialNo AS UNSIGNED) <= "+endNo+" " +
						"ORDER BY CAST(brsStatsSerialNo AS UNSIGNED) ASC")					
						.list();
			}
			
			logger.info("UserID = "+user.getUserId()+" : BranchSStats Size = "+list.size());
			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" Exit from getBranchSStatus()");
		return list;
	}
	
	@Override
	public List<Map<String, String>> getMinMax(Session session, User user, String branchCode, String fromDate, String toDate, String stnType, String custStn){
		logger.info("UserID = "+user.getUserId()+" : Enter into getMinMax()");
		List<Map<String, String>> list = null;
		try{
			Criteria cr = session.createCriteria(BranchSStats.class);	
			if(branchCode.equalsIgnoreCase("23")){
				list = session.createSQLQuery("SELECT MIN(CAST(SUBSTRING(brsStatsSerialNo, 5, LENGTH(brsStatsSerialNo)) AS UNSIGNED)) AS minNo, " +
						"MAX(CAST(SUBSTRING(brsStatsSerialNo, 5, LENGTH(brsStatsSerialNo)) AS UNSIGNED)) AS maxNo FROM branchsstats " +
						"WHERE brsStatsBrCode = '"+branchCode+"' " +
						"AND brsStatsType = '"+stnType+"' " +
						"AND IF('"+fromDate+"' = '', brsStatsDt LIKE '%%', brsStatsDt >= '"+fromDate+"') " +
						"AND IF('"+toDate+"' = '', brsStatsDt LIKE '%%', brsStatsDt <= '"+toDate+"') " +
						"ORDER BY brsStatsSerialNo ASC")
						.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
						.list();
			}else if(custStn.equalsIgnoreCase("no")){
				list = session.createSQLQuery("SELECT MIN(CAST(brsStatsSerialNo AS UNSIGNED)) AS minNo, MAX(CAST(brsStatsSerialNo AS UNSIGNED)) AS maxNo FROM branchsstats " +
						"WHERE brsStatsBrCode = '"+branchCode+"' " +
						"AND brsStatsType = '"+stnType+"' " +
						"AND IF('"+fromDate+"' = '', brsStatsDt LIKE '%%', brsStatsDt >= '"+fromDate+"') " +
						"AND IF('"+toDate+"' = '', brsStatsDt LIKE '%%', brsStatsDt <= '"+toDate+"') " +
						"AND brsStatsSerialNo NOT LIKE 'c%' AND brsStatsSerialNo NOT LIKE 'r%' AND brsStatsSerialNo NOT LIKE 'b%' " +
						"ORDER BY brsStatsSerialNo ASC")
						.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
						.list();
			}
//			}else if(custStn.equalsIgnoreCase("yes") && branchCode.equalsIgnoreCase("7")){
//				list = session.createSQLQuery("SELECT MIN(CAST(SUBSTRING(brsStatsSerialNo, 11, LENGTH(brsStatsSerialNo)) AS UNSIGNED)) AS minNo, " +
//						"MAX(CAST(SUBSTRING(brsStatsSerialNo, 11, LENGTH(brsStatsSerialNo)) AS UNSIGNED)) AS minNo " +
//						"FROM branchsstats " +
//						"WHERE brsStatsBrCode = '"+branchCode+"' " +
//						"AND brsStatsType = '"+stnType+"' " +
//						"AND IF('"+fromDate+"' = '', brsStatsDt LIKE '%%', brsStatsDt >= '"+fromDate+"') " +
//						"AND IF('"+toDate+"' = '', brsStatsDt LIKE '%%', brsStatsDt <= '"+toDate+"') " +
//						"AND brsStatsSerialNo NOT LIKE 'c%' AND brsStatsSerialNo NOT LIKE 'r%' AND brsStatsSerialNo NOT LIKE 'b%' " +
//						"ORDER BY brsStatsSerialNo ASC")
//						.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
//						.list();
//			}
			
			logger.info("UserID = "+user.getUserId()+" : BranchSStats Size = "+list.size());
			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" Exit from getMinMax()");
		return list;
	}
	
}
