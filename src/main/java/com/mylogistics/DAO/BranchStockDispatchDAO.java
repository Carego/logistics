package com.mylogistics.DAO;


import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.BranchStockDispatch;

@Repository
public interface BranchStockDispatchDAO {
	
	 public int saveBranchStockDispatch(BranchStockDispatch branchStockDispatch);
	 
	 public List<BranchStockDispatch> getBranchStockDispatchData(String branchCode);
	 
	 public List<BranchStockDispatch> getBSDataByDisCode(String dispatchCode);
	 
	 public int stockRecieved(String bSDCode);
	 
	 public int updateIsAdminView(String dispatchCode);
	 
	 public int updateDispatchMsg(String code);
	 
	 public String getBrCodeByDisCode(String dispatchCode,Date recDt);
	 
	 public long totalCount();
	 
	 public int getLastBrnchStckDisptchId();
	 
	 public void updateMasterStock(Integer branchCode, List<Long> cnmtList, List<Long> chlnList, List<Long> sedrList);
}
