package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="rentdetail")
public class RentDetail {
	
	@Id
	@GeneratedValue
	@Column(name="rdId")
	private int rdId;
	
	@Column(name="rentAmt")
	private Double rentAmt;
	
	@Column(name="tdsAmt")
	private Double tdsAmt;
	
	@Column(name="netAmt")
	private Double netAmt;
	
	@Column(name="rentMstrId")
	private int rentMstrId;
	
	@Column(name="rdDate")
	private Date rdDate;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="isUploaded")
	private boolean isUploaded;
	
	@Column(name="bankFaCode")
	private String bankFaCode;
	
	@Column(name="fromBranch")
	private String fromBranch;
	
	@Column(name="rentDesc")
	private String rentDesc;

	public int getRdId() {
		return rdId;
	}

	public void setRdId(int rdId) {
		this.rdId = rdId;
	}

	public Double getRentAmt() {
		return rentAmt;
	}

	public void setRentAmt(Double rentAmt) {
		this.rentAmt = rentAmt;
	}

	public Double getTdsAmt() {
		return tdsAmt;
	}

	public void setTdsAmt(Double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}

	public Double getNetAmt() {
		return netAmt;
	}

	public void setNetAmt(Double netAmt) {
		this.netAmt = netAmt;
	}

	public int getRentMstrId() {
		return rentMstrId;
	}

	public void setRentMstrId(int rentMstrId) {
		this.rentMstrId = rentMstrId;
	}

	public Date getRdDate() {
		return rdDate;
	}

	public void setRdDate(Date rdDate) {
		this.rdDate = rdDate;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public boolean isUploaded() {
		return isUploaded;
	}

	public void setUploaded(boolean isUploaded) {
		this.isUploaded = isUploaded;
	}
	
	
	public void setBankFaCode(String bankFaCode) {
		this.bankFaCode = bankFaCode;
	}
	
	public String getBankFaCode() {
		return bankFaCode;
	}
	
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	
	public String getFromBranch() {
		return fromBranch;
	}

	public String getRentDesc() {
		return rentDesc;
	}

	public void setRentDesc(String rentDesc) {
		this.rentDesc = rentDesc;
	}
	
}
