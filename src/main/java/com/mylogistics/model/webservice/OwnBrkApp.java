package com.mylogistics.model.webservice;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mylogistics.model.Address;

@Entity
@Table(name = "ownbrk_app")
public class OwnBrkApp {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ownBrkId")
	private Integer ownBrkId;
	@Column(name = "branchCode")
	private String branchCode;
	@Column(name = "branchName")
	private String branchName;
	@Column(name = "userCode")
	private String userCode;
	@Column(name = "ownBrkName", unique = true)
	private String ownBrkName;	
	@Column(name = "ownBrkCode")
	private String ownBrkCode;	
	@Column(name = "ownBrkUnion")
	private String ownBrkUnion;
	@Column(name = "ownBrkEmailId")
	private String ownBrkEmailId;
	@Column(name = "ownBrkPhoneNo")
	private String ownBrkPhoneNo;
	@Column(name = "ownBrkVehicleType")
	private String ownBrkVehicleType;
	@Column(name = "ownBrkSerTaxNo")
	private String ownBrkSerTaxNo;
	@Column(name = "ownBrkPPNo")
	private String ownBrkPPNo;
	@Column(name = "ownBrkVoterId")
	private String ownBrkVoterId;
	@Column(name = "ownBrkFirmRegNo")
	private String ownBrkFirmRegNo;
	@Column(name = "ownBrkRegPlace")
	private String ownBrkRegPlace;
	@Column(name = "ownBrkComIdNo")
	private String ownBrkComIdNo;
	@Column(name = "ownBrkBsnCard")
	private String ownBrkBsnCard;
	@Column(name = "ownBrkFirmType")
	private String ownBrkFirmType;
	@Column(name = "ownBrkComEstDt")
	private Date ownBrkComEstDt;
	@Column(name = "ownBrkActiveDt")
	private Date ownBrkActiveDt;
	@Column(name = "ownBrkPanName")
	private String ownBrkPanName;
	@Column(name = "ownBrkPanNo")
	private String ownBrkPanNo;
	@Column(name = "ownBrkPanDt")
	private Date ownBrkPanDt;
	@Column(name = "ownBrkPanDOB")
	private Date ownBrkPanDOB;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ownBrkId")
	List<AddressApp> addressList;
	@Column(name = "isPending")
	private Boolean isPending;
	@Column(name = "isCancel")
	private Boolean isCancel;	
	@Column(name = "type")
	private String type;
	@Column(name = "ownBrkImgId")
	private Integer ownBrkImgId = 0;
	@Column(name = "isPanImg")
	private Boolean isPanImg = false;
	@Column(name = "isDecImg")
	private Boolean isDecImg = false;
	@Column(name = "creationTS", insertable = false, updatable = true)
	private Date creationTS;
	
	public Integer getOwnBrkId() {
		return ownBrkId;
	}
	public void setOwnBrkId(Integer ownBrkId) {
		this.ownBrkId = ownBrkId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getOwnBrkName() {
		return ownBrkName;
	}
	public void setOwnBrkName(String ownBrkName) {
		this.ownBrkName = ownBrkName;
	}
	public String getOwnBrkUnion() {
		return ownBrkUnion;
	}
	public void setOwnBrkUnion(String ownBrkUnion) {
		this.ownBrkUnion = ownBrkUnion;
	}
	public String getOwnBrkEmailId() {
		return ownBrkEmailId;
	}
	public void setOwnBrkEmailId(String ownBrkEmailId) {
		this.ownBrkEmailId = ownBrkEmailId;
	}
	public String getOwnBrkPhoneNo() {
		return ownBrkPhoneNo;
	}
	public void setOwnBrkPhoneNo(String ownBrkPhoneNo) {
		this.ownBrkPhoneNo = ownBrkPhoneNo;
	}
	public String getOwnBrkVehicleType() {
		return ownBrkVehicleType;
	}
	public void setOwnBrkVehicleType(String ownBrkVehicleType) {
		this.ownBrkVehicleType = ownBrkVehicleType;
	}
	public String getOwnBrkSerTaxNo() {
		return ownBrkSerTaxNo;
	}
	public void setOwnBrkSerTaxNo(String ownBrkSerTaxNo) {
		this.ownBrkSerTaxNo = ownBrkSerTaxNo;
	}
	public String getOwnBrkPPNo() {
		return ownBrkPPNo;
	}
	public void setOwnBrkPPNo(String ownBrkPPNo) {
		this.ownBrkPPNo = ownBrkPPNo;
	}
	public String getOwnBrkVoterId() {
		return ownBrkVoterId;
	}
	public void setOwnBrkVoterId(String ownBrkVoterId) {
		this.ownBrkVoterId = ownBrkVoterId;
	}
	public String getOwnBrkFirmRegNo() {
		return ownBrkFirmRegNo;
	}
	public void setOwnBrkFirmRegNo(String ownBrkFirmRegNo) {
		this.ownBrkFirmRegNo = ownBrkFirmRegNo;
	}
	public String getOwnBrkRegPlace() {
		return ownBrkRegPlace;
	}
	public void setOwnBrkRegPlace(String ownBrkRegPlace) {
		this.ownBrkRegPlace = ownBrkRegPlace;
	}
	public String getOwnBrkComIdNo() {
		return ownBrkComIdNo;
	}
	public void setOwnBrkComIdNo(String ownBrkComIdNo) {
		this.ownBrkComIdNo = ownBrkComIdNo;
	}
	public String getOwnBrkBsnCard() {
		return ownBrkBsnCard;
	}
	public void setOwnBrkBsnCard(String ownBrkBsnCard) {
		this.ownBrkBsnCard = ownBrkBsnCard;
	}
	public String getOwnBrkFirmType() {
		return ownBrkFirmType;
	}
	public void setOwnBrkFirmType(String ownBrkFirmType) {
		this.ownBrkFirmType = ownBrkFirmType;
	}
	public Date getOwnBrkComEstDt() {
		return ownBrkComEstDt;
	}
	public void setOwnBrkComEstDt(Date ownBrkComEstDt) {
		this.ownBrkComEstDt = ownBrkComEstDt;
	}
	public Date getOwnBrkActiveDt() {
		return ownBrkActiveDt;
	}
	public void setOwnBrkActiveDt(Date ownBrkActiveDt) {
		this.ownBrkActiveDt = ownBrkActiveDt;
	}
	public String getOwnBrkPanName() {
		return ownBrkPanName;
	}
	public void setOwnBrkPanName(String ownBrkPanName) {
		this.ownBrkPanName = ownBrkPanName;
	}
	public String getOwnBrkPanNo() {
		return ownBrkPanNo;
	}
	public void setOwnBrkPanNo(String ownBrkPanNo) {
		this.ownBrkPanNo = ownBrkPanNo;
	}
	public Date getOwnBrkPanDt() {
		return ownBrkPanDt;
	}
	public void setOwnBrkPanDt(Date ownBrkPanDt) {
		this.ownBrkPanDt = ownBrkPanDt;
	}
	public Date getOwnBrkPanDOB() {
		return ownBrkPanDOB;
	}
	public void setOwnBrkPanDOB(Date ownBrkPanDOB) {
		this.ownBrkPanDOB = ownBrkPanDOB;
	}
	public List<AddressApp> getAddressList() {
		return addressList;
	}
	public void setAddressList(List<AddressApp> addressList) {
		this.addressList = addressList;
	}
	public Boolean getIsPending() {
		return isPending;
	}
	public void setIsPending(Boolean isPending) {
		this.isPending = isPending;
	}
	public Boolean getIsCancel() {
		return isCancel;
	}
	public void setIsCancel(Boolean isCancel) {
		this.isCancel = isCancel;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOwnBrkCode() {
		return ownBrkCode;
	}
	public void setOwnBrkCode(String ownBrkCode) {
		this.ownBrkCode = ownBrkCode;
	}
	
	public Integer getOwnBrkImgId() {
		return ownBrkImgId;
	}
	public void setOwnBrkImgId(Integer ownBrkImgId) {
		this.ownBrkImgId = ownBrkImgId;
	}
	public Boolean getIsPanImg() {
		return isPanImg;
	}
	public void setIsPanImg(Boolean isPanImg) {
		this.isPanImg = isPanImg;
	}
	public Boolean getIsDecImg() {
		return isDecImg;
	}
	public void setIsDecImg(Boolean isDecImg) {
		this.isDecImg = isDecImg;
	}
	public Date getCreationTS() {
		return creationTS;
	}
	public void setCreationTs(Date creationTS) {
		this.creationTS = creationTS;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public void setCreationTS(Date creationTS) {
		this.creationTS = creationTS;
	}	
	
}