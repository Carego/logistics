package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.ContToStn;

public class ContToStnServiceImpl implements ContToStnService{
	
	private static List<ContToStn> contToStns = new ArrayList<ContToStn>();
	
	 public List<ContToStn> getAllCTS() {
	        return contToStns;
	    }

	    public void addCTS(ContToStn contToStn) {
	    	contToStns.add(contToStn);
	    }

	    public void deleteAllCTS() {
	    	contToStns.clear();
	    }
	    
	    public void deleteCTS(ContToStn contToStn) {
        	System.out.println("enter into deleteCTS of deleteCTSImpl");
        	String temp = contToStn.getCtsContCode()
        			+contToStn.getCtsRate()
        			+contToStn.getCtsToStn()
        			+contToStn.getCtsVehicleType()
        			+contToStn.getCtsFromWt()
        			+contToStn.getCtsToWt()
        			+contToStn.getCtsProductType();
        			
        	if (!contToStns.isEmpty()) {
        		System.out.println("size of contToStns = "+contToStns.size());
				for (int i = 0; i < contToStns.size(); i++) {
					String listTemp = contToStns.get(i).getCtsContCode()
							+contToStns.get(i).getCtsRate()
							+contToStns.get(i).getCtsToStn()
							+contToStns.get(i).getCtsVehicleType()
							+contToStns.get(i).getCtsFromWt()
							+contToStns.get(i).getCtsToWt()
							+contToStns.get(i).getCtsProductType();
							
					if (temp.equalsIgnoreCase(listTemp)) {
						contToStns.remove(contToStns.get(i));
					}
				}
				System.out.println("after deleting size of contToStns = "+contToStns.size());
			}
	    }
}
