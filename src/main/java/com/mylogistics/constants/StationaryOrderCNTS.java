package com.mylogistics.constants;

public class StationaryOrderCNTS {

	public static final String SO_ID = "stOdrId";
	
	public static final String SO_CODE = "stOdrCode";
	
	public static final String SO_DT = "stOdrDt";
	
	public static final String SR_DT = "stRecDt";
	
	public static final String SO_CNMT = "stOdrCnmt";
	
	public static final String SO_SEDR = "stOdrSedr";
	
	public static final String SO_CHLN = "stOdrChln";
	
	public static final String SO_REC_CNMT = "stOdrRecCnmt";
	
	public static final String SO_REC_SEDR = "stOdrRecSedr";
	
	public static final String SO_REC_CHLN = "stOdrRecChln";
	
	public static final String SO_SUP_CODE = "stOdrSupCode";
	
	public static final String SO_SUP_INO = "stOdrSupInvNo";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
