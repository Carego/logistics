package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrhWiseMunsDAO;
import com.mylogistics.model.lhpv.BrhWiseMuns;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BrhWiseMunsCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private BrhWiseMunsDAO brhWiseMunsDAO;
	
	@RequestMapping(value="/getBrhFrMuns",method = RequestMethod.POST)
	public @ResponseBody Object getBrhFrMuns(){
		System.out.println("Entered into getBrhFrMuns function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> brhList = new ArrayList<>();
		brhList = branchDAO.getNameAndCode();
		if(!brhList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",brhList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/submitMuns",method = RequestMethod.POST)
	public @ResponseBody Object submitMuns(@RequestBody BrhWiseMuns brhWiseMuns){
		System.out.println("Entered into submitMuns function");
		Map<String,Object> map = new HashMap<>();
		int res = brhWiseMunsDAO.saveBrhMuns(brhWiseMuns);
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	@RequestMapping(value="/viewBrhWiseMuns",method = RequestMethod.POST)
	public @ResponseBody Object viewBrhWiseMuns(@RequestBody String brhId){
		System.out.println("Entered into viewBrhWiseMuns function");
		Map<String,Object> map = new HashMap<>();
		int brId = Integer.parseInt(brhId);
		BrhWiseMuns brhWiseMuns = brhWiseMunsDAO.getBrhMuns(brId);	
		if(brhWiseMuns != null){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("brMuns",brhWiseMuns);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
}
