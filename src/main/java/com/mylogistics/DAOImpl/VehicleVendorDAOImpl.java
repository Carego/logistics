package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import com.mylogistics.DAO.VehicleVendorDAO;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.VehicleVendorMstrCNTS;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Owner;
import com.mylogistics.model.TransferVehicle;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.VehVendorService;

public class VehicleVendorDAOImpl implements VehicleVendorDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	private static Logger logger = Logger.getLogger(VehicleVendorDAOImpl.class);
	
	private String vehRcImgPath = "/var/www/html/Erp_Image/Vehicle/RC";
	private String vehPolicyPath = "/var/www/html/Erp_Image/Vehicle/Policy";
	private String vehPSImgPath = "/var/www/html/Erp_Image/Vehicle/PermitSlip";
	private String vehTCImgPath = "/var/www/html/Erp_Image/Vehicle/TC";

	
	@Autowired
	HttpSession httpSession;

	@Autowired
	public VehicleVendorDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	//this function return
	//-2. if both has no pan card
	//-1. if vehicle no is not valid
	//0. exception
	//1. if every thing ok
	
	@Transactional
	@Override
	public int validateVehicle(VehVendorService vehVendorService) {
		
		System.out.println("enter into validateVehicle function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria crOwn=session.createCriteria(Owner.class);
			crOwn.add(Restrictions.eq(OwnerCNTS.OWN_ID, vehVendorService.getOwnId()));
			Owner owner = (Owner)crOwn.list().get(0);
			
			Criteria crBrk=session.createCriteria(Broker.class);
			crBrk.add(Restrictions.eq(BrokerCNTS.BRK_ID, vehVendorService.getBrkId()));
			Broker broker = (Broker)crBrk.list().get(0);
			
			Criteria cr=session.createCriteria(VehicleVendorMstr.class);
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(VehicleVendorMstrCNTS.VV_RCNO));
			cr.setProjection(projectionList);
			List<String> vehVenList = cr.list();
			
			boolean rcNoFlag = false;
			
			if (!vehVenList.isEmpty()) {
				for (String vvRcNo : vehVenList) {
					if (vvRcNo.equalsIgnoreCase(vehVendorService.getVvRcNo())) {
						rcNoFlag = true;
						break;
					}
				}
			}
			
			
			if (owner.isOwnIsPanImg() || broker.isBrkIsPanImg()) {
				if (rcNoFlag) {
					temp = -1;
				}else {
					temp = 1;
				}
			} else {
				temp = -2;
			}
			
			session.flush();
		}catch(Exception e){
			temp = 0;
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return temp;
	}

	@Transactional
	@Override
	public int saveVehVenMstr(VehVendorService vehVendorService) {
		
		System.out.println("Enter into saveVehVenMstr()");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int temp;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			if (vehVendorService.getOwnId() == 0 &&	vehVendorService.getBrkId() == 0 &&
					vehVendorService.getVehVenMstr() == null &&	vehVendorService.getVvRcNo() == null) {
				temp = -2;
			} else if (vehVendorService.getVehVenMstr().getVvRcNo() != null) {
				Owner owner = (Owner) session.get(Owner.class, vehVendorService.getOwnId());
				Broker broker = (Broker) session.get(Broker.class, vehVendorService.getBrkId());
				VehicleVendorMstr vehicleVendorMstr = vehVendorService.getVehVenMstr();
				
				
				
				vehicleVendorMstr.setUserCode(currentUser.getUserCode());
				vehicleVendorMstr.setbCode(currentUser.getUserBranchCode());
				vehicleVendorMstr.setBroker(broker);
				vehicleVendorMstr.setOwner(owner);
				
				int tempId = (Integer) session.save(vehicleVendorMstr);
				if (tempId>0) {
					owner.getVehicleVendorMstrList().add(vehicleVendorMstr);
					broker.getVehicleVendorMstrList().add(vehicleVendorMstr);
					session.update(owner);
					session.update(broker);
				}
				temp = 1;
			} else {
				temp = -2;
			}
			
			transaction.commit();
			session.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
	}

	@Override
	@Transactional
	public List<VehicleVendorMstr> getVehicleVendorList() {
		
		System.out.println("Enter into getVehicleVendorList");
		
		List<VehicleVendorMstr> vehicleVendorMstrList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			vehicleVendorMstrList = cr.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehicleVendorMstrList;
	}

	@Override
	@Transactional
	public VehicleVendorMstr getVehVendor(String vvId) {
		
		VehicleVendorMstr vehicleVendorMstr = new VehicleVendorMstr();
		
		try {
			session = this.sessionFactory.openSession();
			vehicleVendorMstr = (VehicleVendorMstr) session.get(VehicleVendorMstr.class, Integer.parseInt(vvId));
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.clear();
		return vehicleVendorMstr;
	}
	
	@Override
	public VehicleVendorMstr getVehVendorByRc(String rcNo) {
		
		VehicleVendorMstr vehicleVendorMstr = new VehicleVendorMstr();
		
		try {
			session = this.sessionFactory.openSession();
//			vehicleVendorMstr = (VehicleVendorMstr) session.get(VehicleVendorMstr.class, Integer.parseInt(rcNo));
		vehicleVendorMstr=(VehicleVendorMstr) session.createCriteria(VehicleVendorMstr.class)
				.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo)).list().get(0);
		} catch (Exception e) {
			e.printStackTrace();
			vehicleVendorMstr=null;
		}
		session.clear();
		session.clear();
		return vehicleVendorMstr;
	}
	

	@Override
	@Transactional
	public int updateVehVenMstr(VehVendorService vehVendorService) {
		
		System.out.println("Enter into updateVehVenMstr()");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int temp;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			VehicleVendorMstr vehicleVendorMstr = (VehicleVendorMstr) session.get(VehicleVendorMstr.class, vehVendorService.getVehVenMstr().getVvId());
			Owner ownerOld = (Owner) session.get(Owner.class, vehicleVendorMstr.getOwner().getOwnId());
			Broker brokerOld = (Broker) session.get(Broker.class, vehicleVendorMstr.getBroker().getBrkId());
			
			ownerOld.getVehicleVendorMstrList().remove(vehicleVendorMstr);
			brokerOld.getVehicleVendorMstrList().remove(vehicleVendorMstr);
			session.update(ownerOld);
			session.update(brokerOld);
			
			vehicleVendorMstr.setOwner(null);
			vehicleVendorMstr.setBroker(null);
			
			session.update(vehicleVendorMstr);
			
			Owner owner = (Owner) session.get(Owner.class, vehVendorService.getOwnId());
			Broker broker = (Broker) session.get(Broker.class, vehVendorService.getBrkId());
			vehicleVendorMstr = (VehicleVendorMstr) session.get(VehicleVendorMstr.class, vehVendorService.getVehVenMstr().getVvId());
			
			vehicleVendorMstr.setUserCode(currentUser.getUserCode());
			vehicleVendorMstr.setbCode(currentUser.getUserBranchCode());
			vehicleVendorMstr.setBroker(broker);
			vehicleVendorMstr.setOwner(owner);
			vehicleVendorMstr.setVvDriverDLIssueDt(vehVendorService.getVehVenMstr().getVvDriverDLIssueDt());
			vehicleVendorMstr.setVvDriverDLNo(vehVendorService.getVehVenMstr().getVvDriverDLNo());
			vehicleVendorMstr.setVvDriverDLValidDt(vehVendorService.getVehVenMstr().getVvDriverDLValidDt());
			vehicleVendorMstr.setVvDriverMobNo(vehVendorService.getVehVenMstr().getVvDriverMobNo());
			vehicleVendorMstr.setVvDriverName(vehVendorService.getVehVenMstr().getVvDriverName());
			vehicleVendorMstr.setVvFinanceBankName(vehVendorService.getVehVenMstr().getVvFinanceBankName());
			vehicleVendorMstr.setVvFitIssueDt(vehVendorService.getVehVenMstr().getVvFitIssueDt());
			vehicleVendorMstr.setVvFitNo(vehVendorService.getVehVenMstr().getVvFitNo());
			vehicleVendorMstr.setVvFitState(vehVendorService.getVehVenMstr().getVvFitState());
			vehicleVendorMstr.setVvFitValidDt(vehVendorService.getVehVenMstr().getVvFitValidDt());
			vehicleVendorMstr.setVvPerIssueDt(vehVendorService.getVehVenMstr().getVvPerIssueDt());
			vehicleVendorMstr.setVvPerNo(vehVendorService.getVehVenMstr().getVvPerNo());
			vehicleVendorMstr.setVvPerState(vehVendorService.getVehVenMstr().getVvPerState());
			vehicleVendorMstr.setVvPerValidDt(vehVendorService.getVehVenMstr().getVvPerValidDt());
			vehicleVendorMstr.setVvPolicyComp(vehVendorService.getVehVenMstr().getVvPolicyComp());
			vehicleVendorMstr.setVvPolicyNo(vehVendorService.getVehVenMstr().getVvPolicyNo());
			vehicleVendorMstr.setVvRcIssueDt(vehVendorService.getVehVenMstr().getVvRcIssueDt());
			vehicleVendorMstr.setVvRcValidDt(vehVendorService.getVehVenMstr().getVvRcValidDt());
			vehicleVendorMstr.setVvTransitPassNo(vehVendorService.getVehVenMstr().getVvTransitPassNo());
			vehicleVendorMstr.setVvType(vehVendorService.getVehVenMstr().getVvType());
			vehicleVendorMstr.setVvRcNo(vehVendorService.getVehVenMstr().getVvRcNo());
			
			session.merge(vehicleVendorMstr);
			
			owner.getVehicleVendorMstrList().add(vehicleVendorMstr);
			broker.getVehicleVendorMstrList().add(vehicleVendorMstr);
			session.update(owner);
			session.update(broker);
			session.merge(vehicleVendorMstr);
			
			
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
		
	}
	
	@Override
	@Transactional
	public List<String> getAllVehNo(){
		System.out.println("enter into getAllVehNo funciton");
		List<String> vehList = new ArrayList<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			//cr.add(Restrictions.eq(VehicleVendorMstrCNTS.B_CODE, currentUser.getUserBranchCode()));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(VehicleVendorMstrCNTS.VV_RCNO));
			cr.setProjection(projList);
			vehList = cr.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehList;
	}
	
	@Override
	@Transactional
	public List<String> getAllVehNoByLryNo(String lryNo){		
		logger.info("Enter into getAllVehNoByLryNo() : lryNo = "+lryNo);
		List<String> vehList = new ArrayList<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.like(VehicleVendorMstrCNTS.VV_RCNO, "%"+lryNo+"%"));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(VehicleVendorMstrCNTS.VV_RCNO));
			cr.setProjection(projList);
			vehList = cr.list();
			if(vehList.isEmpty())
				logger.info("Vehicle not found !");
			else
				logger.info("Vehicle found : Total = "+vehList.size());
			session.flush();
		}catch(Exception e){
			logger.error("Exception : "+e);
			e.printStackTrace();
		}		
		session.clear();
		session.close();
		logger.info("Exit from getAllVehNoByLryNo()");
		return vehList;
	}
	
	@Override
	@Transactional
	public VehicleVendorMstr getVehByVehNo(String vehNo){
		logger.info("Enter into getVehByVehNo() : VehNo = "+vehNo);
		VehicleVendorMstr vvm = null;
		List<VehicleVendorMstr> vvmList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO,vehNo));
			vvmList = cr.list();
			if(!vvmList.isEmpty()){
				logger.info("Vehicle found : Vehicle ID = "+vvmList.get(0).getVvId());
				vvm = vvmList.get(0);
			}else
				logger.info("Vehicle nof found !");
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from getVehByVehNo()");
		return vvm;
	}

	@Override
	@Transactional
	public List<Map<String, String>> getRcOwnBrkList() {
		
		List<Map<String, String>> rcOwnBrkList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(vvRcNo as rcNo, owner.ownCode as ownCode, broker.brkCode as brkCode) from VehicleVendorMstr");
			rcOwnBrkList = query.list();
			
			for (Map<String, String> map : rcOwnBrkList) {
				System.out.print("vvRcNo: "+map.get("rcNo"));
				System.out.print("ownCode: "+map.get("ownCode"));
				System.out.println("brkCode: "+map.get("brkCode"));
			}
			
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.close();
		return rcOwnBrkList;
	}

	@Override
	@Transactional
	public Map<String, Object> getOwnBrkFrVehicle(String rcNo) {
		System.out.println("VehicleVendorDAOImpl.getOwnBrkFrVehicle()");
		Map<String, Object> map = new HashMap<>();
		int temp = 0;
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo));
			
			if (cr.list() != null && !cr.list().isEmpty()) {
				VehicleVendorMstr vvMstr = (VehicleVendorMstr) cr.list().get(0);
				map.put("own", vvMstr.getOwner());
				map.put("brk", vvMstr.getBroker());
			}
			
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		} finally {
			session.clear();
			session.close();
		}
		
		map.put("temp", temp);
		return map;
	}

	@Override
	@Transactional
	public List<String> getAllVehNoForBbl(){
		System.out.println("enter into getAllVehNo funciton");
		List<String> vehList = new ArrayList<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.eq(VehicleVendorMstrCNTS.B_CODE, currentUser.getUserBranchCode()));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(VehicleVendorMstrCNTS.VV_RCNO));
			cr.setProjection(projList);
			vehList = cr.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehList;
	}
	
	@Override
	public List<String> getAllVehNoLike(String vehNo){
		System.out.println("enter into getAllVehNoLike funciton" +vehNo);
		List<String> vehList = new ArrayList<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try{
			session = this.sessionFactory.openSession();
			
			Query cr = session.createQuery("SELECT vvRcNo FROM VehicleVendorMstr " +
					"WHERE  vvRcNo LIKE :vehNo");
			cr.setString("vehNo", "%"+vehNo+"%");
			
			vehList = cr.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehList;
	}
	
	
	
	@Override
	public int saveVehVenMstr(Session session,VehicleVendorMstr vehicleVendorMstr,int ownId,Blob rcImg,Blob policyImg,Blob psImg){
		
		System.out.println("Enter into saveVehVenMstr()");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int  tempId=0;
		
				Owner owner = (Owner) session.get(Owner.class, ownId);
				
				vehicleVendorMstr.setUserCode(currentUser.getUserCode());
				//vehicleVendorMstr.setbCode(currentUser.getUserBranchCode());
				vehicleVendorMstr.setbCode(owner.getbCode());
				vehicleVendorMstr.setOwner(owner);
				
			  tempId = (Integer) session.save(vehicleVendorMstr);
				if (tempId>0) {
					
					try{
						
						Path rcPath = Paths.get(vehRcImgPath);
						if(! Files.exists(rcPath))
							Files.createDirectories(rcPath);
						
						Path policyPath = Paths.get(vehPolicyPath);
						if(! Files.exists(policyPath))
							Files.createDirectories(policyPath);
						
						Path psPath = Paths.get(vehPSImgPath);
						if(! Files.exists(psPath))
							Files.createDirectories(psPath);
						
					}catch(Exception e){
						System.out.println("Error in creating direcotry : "+e);
						logger.error("Error in creating directory : "+e);
					}
					
					
					
					try {
						
						
						if (rcImg != null ) {
							
							
							vehicleVendorMstr.setVvRcImgPath(vehRcImgPath+"/"+"RC"+tempId+".pdf");
							byte vehRcImgInBytes[] = rcImg.getBytes(1, (int)rcImg.length());
							File rcFile = new File(vehicleVendorMstr.getVvRcImgPath());
							if(rcFile.exists())
								rcFile.delete();
							if(rcFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(rcFile);
					            targetFile.write(vehRcImgInBytes);			            
					            targetFile.close();	
							}
							
							
						}  if (policyImg != null ) {
							vehicleVendorMstr.setVvPolicyImgPath(vehPolicyPath+"/"+"INSUR"+tempId+".pdf");
							byte vehPolImgInBytes[] = policyImg.getBytes(1, (int)policyImg.length());
							File polFile = new File(vehicleVendorMstr.getVvPolicyImgPath());
							if(polFile.exists())
								polFile.delete();
							if(polFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(polFile);
					            targetFile.write(vehPolImgInBytes);			            
					            targetFile.close();	
							}
						}
							  if (psImg != null ) {
									vehicleVendorMstr.setVvPerSlpImgPath(vehPSImgPath+"/"+"PS"+tempId+".pdf");
									byte vehPsImgInBytes[] = psImg.getBytes(1, (int)psImg.length());
									File psFile = new File(vehicleVendorMstr.getVvPerSlpImgPath());
									if(psFile.exists())
										psFile.delete();
									if(psFile.createNewFile()){											    
										OutputStream targetFile=  new FileOutputStream(psFile);
							            targetFile.write(vehPsImgInBytes);			            
							            targetFile.close();	
									}
							
							  }	
						
						
						}catch(Exception e) {
							e.printStackTrace();
							logger.info("exception in VehicleImg save"+e);
							System.out.println("exception in VehicleImg save"+e);
						}
						
					
					
					
					session.update(vehicleVendorMstr);
					
					
					
					owner.getVehicleVendorMstrList().add(vehicleVendorMstr);
					session.update(owner);
				}
		//session.flush();
		//session.clear();
		return tempId;
	}
	
	
	@Override
	public Map<String,String> checkVehicleExist(Map<String,String> map) {

		Boolean b=false;
		Map<String, String> resultMap = new HashMap<>();
		Session session=this.sessionFactory.openSession();
		String rcNo=map.get("rcNo");
		String enginNo=map.get("engineNo");
		String chassisNo=map.get("chassisNo");
		
		try {
			
			Criteria cr=session.createCriteria(VehicleVendorMstr.class)
					.add(Restrictions.disjunction()
					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo))
					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_CHASSIS_NO, chassisNo))
					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_ENGINE_NO, enginNo)));
			List list=cr.list();
			
			if(!list.isEmpty()) {
				resultMap.put("result", "success");
				VehicleVendorMstr mstr=(VehicleVendorMstr) list.get(0);
				if(rcNo.equalsIgnoreCase(mstr.getVvRcNo()))
					resultMap.put("msg", "Vehicle No. already exist");
				else if(mstr.getVvEngineNo().equalsIgnoreCase(enginNo))
					resultMap.put("msg", "Engine No. already exist");
				else if(mstr.getVvChassisNo().equalsIgnoreCase(chassisNo))
					resultMap.put("msg", "Chassis No. already exist");
				
			}else {
				resultMap.put("result", "error");
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		return resultMap;
	}
	
	
	@Override
	public boolean checkVehicleExist(String rcNo,Session session) {

		Boolean b=false;
		
		
			
			Criteria cr=session.createCriteria(VehicleVendorMstr.class)
					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo));
			List list=cr.list();
			if(!list.isEmpty())
				b=true;
			else 
				b=false;
		return b;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public VehicleVendorMstr getVehicleByRc(String rcNo,Session session) {

		VehicleVendorMstr vv;
		
		
			
			Criteria cr=session.createCriteria(VehicleVendorMstr.class)
					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo));
			List<VehicleVendorMstr> list=cr.list();
			if(!list.isEmpty())
				vv= list.get(0);
			else 
				vv=null;
		return vv;
	}
	
	@Override
	public void updateVehVenMstr(Session session,VehicleVendorMstr vehicleVendorMstr,int ownId,Blob rcImg,Blob policyImg,Blob psImg){
		
		System.out.println("Enter into updateVehVenMstr()");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
				Owner owner = (Owner) session.get(Owner.class, ownId);
				 
				Owner oldOwner=vehicleVendorMstr.getOwner();
				
				oldOwner.getVehicleVendorMstrList().remove(vehicleVendorMstr);
				
				vehicleVendorMstr.setUserCode(currentUser.getUserCode());
				//vehicleVendorMstr.setbCode(currentUser.getUserBranchCode());
				vehicleVendorMstr.setbCode(owner.getbCode());
				//vehicleVendorMstr.setOwner(null);
				vehicleVendorMstr.setOwner(owner);
				
			 // tempId = (Integer) session.save(vehicleVendorMstr);
					
					try{
						
						Path rcPath = Paths.get(vehRcImgPath);
						if(! Files.exists(rcPath))
							Files.createDirectories(rcPath);
						
						Path policyPath = Paths.get(vehPolicyPath);
						if(! Files.exists(policyPath))
							Files.createDirectories(policyPath);
						
						Path psPath = Paths.get(vehPSImgPath);
						if(! Files.exists(psPath))
							Files.createDirectories(psPath);
						
						/*Path tcPath=Paths.get(vehTCImgPath);
						if(! Files.exists(tcPath))
							Files.createDirectories(tcPath);*/
					}catch(Exception e){
						System.out.println("Error in creating direcotry : "+e);
						logger.error("Error in creating directory : "+e);
					}
					
					
					
					try {
						
						
						/*if (tcImg != null ) {
							
							
							vehicleVendorMstr.setVvTcImgPath(vehTCImgPath+"/"+"TC"+vehicleVendorMstr.getVvId()+".pdf");
							byte vehTcImgInBytes[] = rcImg.getBytes(1, (int)tcImg.length());
							File tcFile = new File(vehicleVendorMstr.getVvTcImgPath());
							if(tcFile.exists())
								tcFile.delete();
							if(tcFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(tcFile);
					            targetFile.write(vehTcImgInBytes);			            
					            targetFile.close();	
							}
							
							
						}*/
						
						
						if (rcImg != null ) {
							
							
							vehicleVendorMstr.setVvRcImgPath(vehRcImgPath+"/"+"TRC"+vehicleVendorMstr.getVvId()+".pdf");
							byte vehRcImgInBytes[] = rcImg.getBytes(1, (int)rcImg.length());
							File rcFile = new File(vehicleVendorMstr.getVvRcImgPath());
							if(rcFile.exists())
								rcFile.delete();
							if(rcFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(rcFile);
					            targetFile.write(vehRcImgInBytes);			            
					            targetFile.close();	
							}
							
							
						}  if (policyImg != null ) {
							vehicleVendorMstr.setVvPolicyImgPath(vehPolicyPath+"/"+"TINSUR"+vehicleVendorMstr.getVvId()+".pdf");
							byte vehPolImgInBytes[] = policyImg.getBytes(1, (int)policyImg.length());
							File polFile = new File(vehicleVendorMstr.getVvPolicyImgPath());
							if(polFile.exists())
								polFile.delete();
							if(polFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(polFile);
					            targetFile.write(vehPolImgInBytes);			            
					            targetFile.close();	
							}
						}
							  if (psImg != null ) {
									vehicleVendorMstr.setVvPerSlpImgPath(vehPSImgPath+"/"+"TPS"+vehicleVendorMstr.getVvId()+".pdf");
									byte vehPsImgInBytes[] = psImg.getBytes(1, (int)psImg.length());
									File psFile = new File(vehicleVendorMstr.getVvPerSlpImgPath());
									if(psFile.exists())
										psFile.delete();
									if(psFile.createNewFile()){											    
										OutputStream targetFile=  new FileOutputStream(psFile);
							            targetFile.write(vehPsImgInBytes);			            
							            targetFile.close();	
									}
							
							  }	
						
						
						}catch(Exception e) {
							e.printStackTrace();
							logger.info("exception in VehicleImg save"+e);
							System.out.println("exception in VehicleImg save"+e);
						}
						
					
					
					session.update(vehicleVendorMstr);
					
					
					
					owner.getVehicleVendorMstrList().add(vehicleVendorMstr);
					session.update(owner);
	}

	@Override
	public int saveTransferVehicle(Session session, TransferVehicle tv) {
		// TODO Auto-generated method stub
		Integer i=(Integer) session.save(tv);
		return i;
	}
	
	
	
	
	
}
