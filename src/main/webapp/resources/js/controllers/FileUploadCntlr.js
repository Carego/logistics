
'use strict';

var app = angular.module('application');

app.controller('FileUploadCntlr',['$scope','$location','$filter','$http','FileUploadService',
                                 function($scope,$location,$filter,$http,FileUploadService){
	
	
	
	
	$scope.filesDBFlag = true;

		$scope.uploadFile = function(excelFile){
			console.log("enter into excelFile function");
			var file = excelFile;
			console.log("file name="+file.name);
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				
				 var index = file.name.lastIndexOf(".");
                 var strsubstring = file.name.substring(index, file.name.length);
                 if (strsubstring == '.xlsx'  || strsubstring == '.xls' )
                 {
                 
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldExcelFile";
				FileUploadService.uploadExcelFileToUrl(file, uploadUrl);
				console.log("file save on server");
				  console.log('File Uploaded sucessfully');
                 }else{
                	 
                	 $scope.alertToast("File extension should be .xlsx or .xls");
                 }
				
				
			}
			
			
		}
		
		
		$scope.getFiles=function(){
			
			console.log("date="+$scope.date);
			
			var date={
					"date":$scope.date
			};
				var response = $http.post($scope.projectName+'/getFileByDate',date);
				response.success(function(data, status, headers, config){
					  if(data.result === "success"){
						  $scope.fileList=data.list;
						  
						  $scope.filesDBFlag = false;
							$('div#filesId').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								title: "Select File",
								show: UDShow,
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.selCustFlag = true;
							    }
								});
							
							$('div#filesId').dialog('open');	
						  
						  
					  }else{
						  $scope.alertToast("File not found");
					  }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
				   });
		}


		$scope.saveFile = function(obj){
			$scope.fileName = obj.fileName;
			$scope.bsuId = obj.bsuId;
			$('div#filesId').dialog('close');
		}
		


}]);