package com.mylogistics.constants;

public class BillCNTS {
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String BILL_ID = "blId";
	
	public static final String BILL_CUST_ID = "blCustId";
	
	public static final String BILL_REM_AMT = "blRemAmt";
	
	public static final String BILL_BF = "billForwarding";
	
	public static final String BILL_TYPE = "blType";
	
	public static final String Bill_NO = "blBillNo";
	
	public static final String USER_CODE = "userCode";
	
	public static final String BILL_DT = "blBillDt";
	
	public static final String BILL_SUB_DT = "blBillSubDt";
	
	public static final String BILL_FINAL_TOT = "blFinalTot";
	
	public static final String BILL_CANCEL = "blCancel";
	
	public static final String BILL_BRH_ID = "blBrhId";
	
}
