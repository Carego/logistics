'use strict';

var app = angular.module('application');

app.controller('VehicleTypeCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.vehicletype = {};
	$scope.viewVTDetailsFlag = true;
	$scope.vtList =[];
	var code;
	$scope.type=[];
		
	$scope.Submit=function(VehicleTypeForm,vehicletype){
		console.log("Entered into Submit function = "+VehicleTypeForm.$invalid);
		$scope.vehicletype.vtServiceType = vehicletype.vtServiceType.toUpperCase();
		$scope.vehicletype.vtVehicleType = vehicletype.vtVehicleType.toUpperCase();
		$scope.code = vehicletype.vtServiceType+vehicletype.vtVehicleType;
				
		if(VehicleTypeForm.$invalid){
			if(VehicleTypeForm.vtVehicleType.$invalid){
				$scope.alertToast("Please enter vehicle type between 3-40 characters.....");
			}else if(VehicleTypeForm.vtServiceType.$invalid){
				$scope.alertToast("Please enter service type between 3-40 characters.....");
			}else if(VehicleTypeForm.vtLoadLimit.$invalid){
				$scope.alertToast("Please enter valid load limit that is number of digits before decimal can be atmost seven.....");
			}else if(VehicleTypeForm.vtGuaranteeWt.$invalid){
				$scope.alertToast("Please enter guarentee weight that is number of digits before decimal can be atmost seven.....");
			}else if(VehicleTypeForm.grnteeWt.$invalid){
				$scope.alertToast("Please enter guarentee weight Ton/Kg");
			}
		}else{
			console.log("vehicletype.vtGuaranteeWt = "+vehicletype.vtGuaranteeWt);
			 if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Vehicle type already exists...");
			}else if(angular.isUndefined(vehicletype.vtGuaranteeWt) || vehicletype.vtGuaranteeWt === null){
				$scope.viewVTDetailsFlag = false;
				$('div#viewVTDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Vehicle Type Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewVTDB').dialog('open');
			}else if(!angular.isUndefined(vehicletype.vtGuaranteeWt)){
			
				console.log("*****************");
				
				if(angular.isUndefined($scope.grnteeWt) || $scope.grnteeWt === "" || $scope.grnteeWt === null){
					$scope.alertToast("Please fill guarentee weight type Ton/Kg");
				}else{
					$scope.viewVTDetailsFlag = false;
					$('div#viewVTDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "View Vehicle Type Details",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
						}
					});
					$('div#viewVTDB').dialog('open');
				}
			}else{
				$scope.viewVTDetailsFlag = false;
				$('div#viewVTDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Vehicle Type Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewVTDB').dialog('open');
			}
		}
	}
	
	
	$scope.saveVehicleType = function(vehicletype,grnteeWt){
		console.log("Entered into saveVehicleType function---");
		if(grnteeWt==="Ton"){
			 vehicletype.vtGuaranteeWt=(vehicletype.vtGuaranteeWt*907.185);
		}
		var response = $http.post($scope.projectName+'/submitVehicleType', vehicletype);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewVTDB').dialog('close');
				$scope.successToast(data.result);
				$scope.vehicletype.vtVehicleType="";
				$scope.vehicletype.vtServiceType="";
				$scope.vehicletype.vtCode="";
				$scope.vehicletype.vtLoadLimit="";
				$scope.vehicletype.vtGuaranteeWt="";
				$scope.grnteeWt="";
				 $scope.getVehicleTypeList();
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.closeVTDetailsDB = function(){
		$('div#viewVTDB').dialog('close');
	}
	
	$('#LoadLimit').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });

	$('#LoadLimit').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	});
	   
	$('#weight').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	});

	$('#weight').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	});
	
	 $('#VehicleType').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	});
	 
	 $('#VehicleTypeCode').keypress(function(e) {
	       if (this.value.length == 1) {
	           e.preventDefault();
	       }
	});
	 
	 $('#ServiceType').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	});
	 
	 $scope.getVehicleTypeList = function(){
		 console.log("Entered into getVehicleTypeList function------>");
			var response = $http.post($scope.projectName+'/getVehicleTypeDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.vtList = data.list;
					for(var i=0;i<$scope.vtList.length;i++){
							$scope.type[i] = $scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType;
							$scope.type.push($scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType);
							console.log($scope.type[i]);
					}
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	 }
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVehicleTypeList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	 	
	
}]);