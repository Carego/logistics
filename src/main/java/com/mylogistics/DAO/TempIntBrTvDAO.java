package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.TempIntBrTv;

@Repository
public interface TempIntBrTvDAO {

	public List<TempIntBrTv> getOpenIBTV(String brFaCode);
	
}
