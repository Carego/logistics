<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		       	
		    <div class="row">
	     		<div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       			<label for="code">Pay To</label>	
	       		</div>
	       		
	       		<div class="col s4 input-field">
						<input type ="text" name="vehNo" id="vehNoId" ng-model="vehNo" ng-click="openVehNoDB()" disabled="disabled" readonly ng-required="true">
					<label>Select Vehicle No.</label>
				</div>
				
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="tdsCodeId" name ="tdsCode" ng-model="vs.tdsCode" ng-click="selectTds()" readonly>
		       			<label for="code">TDS Code</label>	
		       	</div>
		      
	   		</div>
	   		
	   		
	   		 <div class="row">
	   		 	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="tdsAmtId" name ="tdsAmt" ng-model="vs.tdsAmt"  STEP = "0.01" >
		       			<label for="code">TDS Amount</label>	
		       	</div>
	   		 </div>
	   		 
	   		 
	   		<div class="row">
	   		<div ng-show="vmAndSvmList.length > 0">
	   			<table class="tblrow">
	   				   <caption class="coltag tblrow">Vehicle Expense Details</caption>
	   				 <tr class="rowclr">
                        <th class="colclr">VH NO.</th>
                        <th class="colclr">PETROL</th>
                        <th class="colclr">REPAIR</th>
                        <th class="colclr">ROADTAX</th>    
                        <th class="colclr">INSURANCE</th>    
                        <th class="colclr">SERVICE</th>    
                        <th class="colclr">OTHERS</th>  
                        <th class="colclr">TOTAL EXP</th>   
                        <th class="colclr">ACTION</th>                      
                    </tr>
                     <tr class="tbl" ng-repeat="vmAndSvm in vmAndSvmList">
                        <td class="rowcel">{{vmAndSvm.vehMstr.vehNo}}</td>
                        <td class="rowcel">
                        	<div ng-repeat="faCar in vmAndSvm.faCarList">
                        		<div ng-if="faCar.fcdExpType == 'PETROL'">
                        			<table>
                        				<tr>
                        					<td>F</td>
                        					<td>{{faCar.fcdFuel}}</td>
                        				</tr>
                        				<tr>
                        					<td>R</td>
                        					<td>{{faCar.fcdReading}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faCar.fcdExpAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faCar.fcdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">
                        	<div ng-repeat="faCar in vmAndSvm.faCarList">
                        		<div ng-if="faCar.fcdExpType == 'REPAIR'">
                        			<table>
                        				<tr>
                        					<td>R</td>
                        					<td>{{faCar.fcdReading}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faCar.fcdExpAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faCar.fcdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">
                        	<div ng-repeat="faCar in vmAndSvm.faCarList">
                        		<div ng-if="faCar.fcdExpType == 'ROADTAX'">
                        			<table>
                        				<tr>
                        					<td>R</td>
                        					<td>{{faCar.fcdReading}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faCar.fcdExpAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faCar.fcdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                         <td class="rowcel">
                        	<div ng-repeat="faCar in vmAndSvm.faCarList">
                        		<div ng-if="faCar.fcdExpType == 'INSURANCE'">
                        			<table>
                        				<tr>
                        					<td>R</td>
                        					<td>{{faCar.fcdReading}}</td>
                        				</tr>
                        				<tr>
                        					<td>F DT</td>
                        					<td>{{faCar.fcdInsFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>T DT</td>
                        					<td>{{faCar.fcdInsToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faCar.fcdExpAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faCar.fcdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                         <td class="rowcel">
                        	<div ng-repeat="faCar in vmAndSvm.faCarList">
                        		<div ng-if="faCar.fcdExpType == 'SERVICE'">
                        			<table>
                        				<tr>
                        					<td>R</td>
                        					<td>{{faCar.fcdReading}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faCar.fcdExpAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faCar.fcdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">
                        	<div ng-repeat="faCar in vmAndSvm.faCarList">
                        		<div ng-if="faCar.fcdExpType == 'OTHER'">
                        			<table>
                        				<tr>
                        					<td>F</td>
                        					<td>{{faCar.fcdFuel}}</td>
                        				</tr>
                        				<tr>
                        					<td>R</td>
                        					<td>{{faCar.fcdReading}}</td>
                        				</tr>
                        				<tr>
                        					<td>F DT</td>
                        					<td>{{faCar.fcdInsFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>T DT</td>
                        					<td>{{faCar.fcdInsToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faCar.fcdExpAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faCar.fcdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">
                        	{{vmAndSvm.sVehMstr.svmTotAmt}}
                        </td>
                        <td class="rowcel">
                        	<input type="button" value="REMOVE" ng-click="removeVME($index,vmAndSvm)"/>
                        </td>
                    </tr>
                    <tr class="tbl" >							
							<td class="rowcel" colspan="7">Total Amount</td>
							<td class="rowcel">{{svmTotAmtSum}}</td>
							<td class="rowcel"></td>
						</tr>
	   			</table>
	   		</div>
	   		</div>
	   			   		
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<!-- <div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div> -->
	   		 </div>
		  </form>
    </div>
    
    
    
    <div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	 <div id ="vehNoDB" ng-hide="vehNoDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Vehicle No</th>
			  <th>Vehicle Model</th> 	 		
 	  	  </tr>
 	  
		  <tr ng-repeat="vehMstr in vehMstrList | filter:filterTextbox">
		 	  <td><input type="radio"  name="vehMstr"   value="{{ vehMstr }}" ng-model="vehMstrCode" ng-click="saveVehMstr(vehMstr)"></td>
              <td>{{vehMstr.vehNo}}</td>
              <td>{{vehMstr.vehModel}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	
	<div id="tdsCodeDB" ng-hide=tdsCodeFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Tds Code ">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> TDS Code </th>
	 	  	  	  <th> Name </th>
	 	  	  	  <th> Type </th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="fsMTds in fsMTdsList | filter:filterTextbox">
			 	  <td><input type="radio"  name="fsMTds"   value="{{ fsMTds }}" ng-model="fsMT" ng-click="saveTdsCode(fsMTds)"></td>
	              <td>{{fsMTds.faMfaCode}}</td>
	              <td>{{fsMTds.faMfaName}}</td>
	              <td>{{fsMTds.faMfaType}}</td>
	          </tr>
	      </table> 
    </div>
    
	
	
	<div id="vehVoucherDB" ng-hide="vehVoucherDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="vmVoucherForm" ng-submit="submitVMVoucher(vmVoucherForm)">
		 <table>
		 	<tr>
		 		<td></td>
		 		<td>Expense Type</td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="petrolId" ng-model="petrol" ng-change="savePetrol()"></td>
		 		<td><input type ="button" id="petDetId" value="PETROL EXPENSE" ng-disabled="!petrol" ng-click="openPetDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="repairId" ng-model="repair" ng-change="saveRepair()"></td>
		 		<td><input type ="button" id="repDetId" value="REPAIR EXPENSE" ng-disabled="!repair" ng-click="openRepDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="roadTaxId" ng-model="roadTax" ng-change="saveRoadTax()"></td>
		 		<td><input type ="button" id="roadTaxDetId" value="ROAD TAX" ng-disabled="!roadTax" ng-click="openRTaxDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="insuranceId" ng-model="insurance" ng-change="saveInsu()"></td>
		 		<td><input type ="button" id="insuDetId" value="INSURANCE" ng-disabled="!insurance" ng-click="openInsuDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="serviceId" ng-model="service" ng-change="saveService()"></td>
		 		<td><input type ="button" id="serDetId" value="SERVICE" ng-disabled="!service" ng-click="openSerDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="otherId" ng-model="other" ng-change="saveOther()"></td>
		 		<td><input type ="button" id="othDetId" value="OTHER" ng-disabled="!other" ng-click="openOthDet()"></td>
		 	</tr>
		 	<tr>
		 		<td>
		 			<input type="submit" value="submit"/>
		 		</td>
		 	</tr>
		 </table>
	</form>
	</div>
	
	
	<div id="petExpDB" ng-hide="petExpDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="petDetForm" ng-submit="submitPetDet(petDetForm)">
			<table>
				<tr>
					<td>Fuel</td>
					<td><input type="number" name="fuelName" id="fuelId" ng-model="faCar1.fcdFuel"/></td>
				</tr>
				
				<tr>
					<td>Reading</td>
					<td><input type="number" name="reading" id="readingId" ng-model="faCar1.fcdReading"/></td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td><input type="number" name="amount" id="amountId" ng-model="faCar1.fcdExpAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="fcdDescId" name ="fcdDesc" ng-model="faCar1.fcdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	
	<div id="repExpDB" ng-hide="repExpDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="repDetForm" ng-submit="submitRepDet(repDetForm)">
			<table>
				<tr>
					<td>Reading</td>
					<td><input type="number" name="reading" id="readingId" ng-model="faCar2.fcdReading"/></td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td><input type="number" name="amount" id="amountId" ng-model="faCar2.fcdExpAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="fcdDescId" name ="fcdDesc" ng-model="faCar2.fcdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="rTaxDB" ng-hide="rTaxDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="rTaxDetForm" ng-submit="submitRTaxDet(rTaxDetForm)">
			<table>
				<tr>
					<td>Reading</td>
					<td><input type="number" name="reading" id="readingId" ng-model="faCar3.fcdReading"/></td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td><input type="number" name="amount" id="amountId" ng-model="faCar3.fcdExpAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="fcdDescId" name ="fcdDesc" ng-model="faCar3.fcdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="insuDB" ng-hide="insuDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="insuDetForm" ng-submit="submitInsuDet(insuDetForm)">
			<table>
				<tr>
					<td>Reading</td>
					<td><input type="number" name="reading" id="readingId" ng-model="faCar4.fcdReading"/></td>
				</tr>
				
				<tr>
					<td>From Date</td>
					<td><input type="date" name="insFrDt" id="insFrDtId" ng-model="faCar4.fcdInsFrDt"/></td>
				</tr>
				
				<tr>
					<td>To Date</td>
					<td><input type="date" name="insToDt" id="insToDtId" ng-model="faCar4.fcdInsToDt"/></td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td><input type="number" name="amount" id="amountId" ng-model="faCar4.fcdExpAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="fcdDescId" name ="fcdDesc" ng-model="faCar4.fcdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="serDB" ng-hide="serDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="serDetForm" ng-submit="submitSerDet(serDetForm)">
			<table>
				<tr>
					<td>Reading</td>
					<td><input type="number" name="reading" id="readingId" ng-model="faCar5.fcdReading"/></td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td><input type="number" name="amount" id="amountId" ng-model="faCar5.fcdExpAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="fcdDescId" name ="fcdDesc" ng-model="faCar5.fcdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="othDB" ng-hide="othDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="othDetForm" ng-submit="submitOthDet(othDetForm)">
			<table>
				<tr>
					<td>Reading</td>
					<td><input type="number" name="reading" id="readingId" ng-model="faCar6.fcdReading"/></td>
				</tr>
				
				<tr>
					<td>Fuel</td>
					<td><input type="number" name="fuelName" id="fuelId" ng-model="faCar6.fcdFuel"/></td>
				</tr>
				
				<tr>
					<td>From Date</td>
					<td><input type="date" name="insFrDt" id="insFrDtId" ng-model="faCar6.fcdInsFrDt"/></td>
				</tr>
				
				<tr>
					<td>To Date</td>
					<td><input type="date" name="insToDt" id="insToDtId" ng-model="faCar6.fcdInsToDt"/></td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td><input type="number" name="amount" id="amountId" ng-model="faCar6.fcdExpAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="fcdDescId" name ="fcdDesc" ng-model="faCar6.fcdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
	
</div>    