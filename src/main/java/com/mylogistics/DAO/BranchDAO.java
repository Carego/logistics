package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;

@Repository
public interface BranchDAO {
	
	 //Satarupa: returns the total number of rows in branch table
	 public long totalCount();
	
	 //Satarupa: retrieves the last row of branch table
	 public List<Branch> getLastBranch();
	
	 //Satarupa: saves branch to database
	 public int saveBranchToDB(Branch branch);
	
	 public List<Branch> getAllBranch();
	
	 //Satarupa: retrieve the details of branch table on the basis of branchCode
	 public List<Branch> retrieveBranch(String branchCode);
	 
	 public List<Branch> retrieveBranch(String branchCode,Session session);
	 
	//Satarupa: retrieve the details of branch table on the basis of branchCodeTemp
	 public List<Branch> getBranchByBrCodeTemp(String branchCodeTemp);
	 
	 //Satarupa: retrieve the list of all branches where isView=no.
	 public List<Branch> getAllBranchForSuperAdmin();
	 
	 //Satarupa: updates the branch table for the branches that have been checked. 
	 public int updateBranchisViewTrue(String code[]);
	 
	//Satarupa: updates the branch table on editing by super admin
	 public int updateBranchToDB(Branch branch);
	 
	//Satarupa: get the list of all opened branches.
	 public List<Branch> getAllActiveBranches();
	 
	 public List<Branch> getAllActiveBranchesByBrhName(String brhName);
	 
	/*//Satarupa: get the list of all branch codes.
	 public List<String> getBranchCodes();*/
	 
	//Satarupa: retrieve the details of branch table on the basis of branch Id.
	 public List<Branch> getOldBranch(int oldBranchId);
	 
	 //Satarupa: gets branch name on the basis of branch code
	 public String getBrNameByBrCode(String branchCode);
	 
	 public Branch getBranchBMstrLt(int branchId);
	 
	 public int updateBrWise(Branch branch);
	 
	 public List<Map<String,Object>> getNameAndCode();
	 
	 public Branch getBranchById(int brhId);
	 
	 public List<Map<String, Object>> getActiveBrNCI();
	 
	 public Map<String, Object> getActiveBrNCustNCI();
	 
	 public List<Employee> getEmployeeList(int branchId);
}
