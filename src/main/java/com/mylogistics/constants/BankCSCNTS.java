package com.mylogistics.constants;

public class BankCSCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String USER_CODE = "userCode";
	
	public static final String BCS_ID = "bcsId";
	
	public static final String BCS_DT = "bcsDt";
	
	public static final String BCS_BRH_ID = "bcsBrhId";
	
	public static final String BCS_BNK_ID = "bcsBankId";
	
	public static final String BCS_BNK_CODE = "bcsBankCode";
}