package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="petrocardadv")
public class PetroCardAdv {
	
	@Id
	@GeneratedValue
	@Column(name="petId")
	private int petId;
	
	@Column(name="bCode", length=3,nullable=false)
	private String bCode;
	
	@Column(name="userCode", length=4,nullable=false)
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="isGenerated", length=3,nullable=false)
	private String isGenerated;
	
	@Column(name="email", length=40,nullable=false)
	private String email;
	
	@Column(name="amount", columnDefinition="double default 0")
	private double amount;
	
	@Column(name="amountReq", columnDefinition="double default 0")
	private double amountReq;
	
	@Column(name="cardType",length=3,nullable=false)
	private String cardType;
	
	@Column(name="cardNo",length=30,nullable=false)
	private String cardNo;
	
	@Column(name="cnmtNo",length=30)
	private String cnmtNo;
	
	@Column(name="challanNo",length=20)
	private String challanNo;
	
	@Column(name="loryNo",length=20,nullable=false)
	private String loryNo;
	
	@Column(name="chlnDt",nullable=false)
	private Date chlnDt;
	
	@Column(name="fromStn",length=60,nullable=false)
	private String fromStn;
	
	@Column(name="toStn",length=60,nullable=false)
	private String toStn;
	
	@Column(name="ownCode",length=10)
	private String ownCode;
	
	@Column(name="brkCode",length=10)
	private String brkCode;
	
	@Column(name="freight", columnDefinition="double default 0")
	private double freight;
	
	@Column(name="rate", columnDefinition="double default 0")
	private double rate;
	
	@Column(name="advance", columnDefinition="double default 0")
	private double advance;
	
	@Column(name="tdsAmt", columnDefinition="double default 0")
	private double tdsAmt;
	
	@Column(name="rejected", columnDefinition="boolean default false")
	private boolean rejected;
	
	@Column(name="approved", columnDefinition="boolean default false")
	private boolean approved;
	
	@Column(name="allowed", columnDefinition="boolean default false")
	private boolean allowed;
	
	public int getPetId() {
		return petId;
	}

	public void setPetId(int petId) {
		this.petId = petId;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getIsGenerated() {
		return isGenerated;
	}

	public void setIsGenerated(String isGenerated) {
		this.isGenerated = isGenerated;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCnmtNo() {
		return cnmtNo;
	}

	public void setCnmtNo(String cnmtNo) {
		this.cnmtNo = cnmtNo;
	}

	public String getChallanNo() {
		return challanNo;
	}

	public void setChallanNo(String challanNo) {
		this.challanNo = challanNo;
	}

	public String getLoryNo() {
		return loryNo;
	}

	public void setLoryNo(String loryNo) {
		this.loryNo = loryNo;
	}

	public Date getChlnDt() {
		return chlnDt;
	}

	public void setChlnDt(Date chlnDt) {
		this.chlnDt = chlnDt;
	}

	public String getFromStn() {
		return fromStn;
	}

	public void setFromStn(String fromStn) {
		this.fromStn = fromStn;
	}

	public String getToStn() {
		return toStn;
	}

	public void setToStn(String toStn) {
		this.toStn = toStn;
	}

	public double getAmountReq() {
		return amountReq;
	}

	public void setAmountReq(double amountReq) {
		this.amountReq = amountReq;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	public String getBrkCode() {
		return brkCode;
	}

	public void setBrkCode(String brkCode) {
		this.brkCode = brkCode;
	}

	public double getFreight() {
		return freight;
	}

	public void setFreight(double freight) {
		this.freight = freight;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getAdvance() {
		return advance;
	}

	public void setAdvance(double advance) {
		this.advance = advance;
	}

	public double getTdsAmt() {
		return tdsAmt;
	}

	public void setTdsAmt(double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isAllowed() {
		return allowed;
	}

	public void setAllowed(boolean allowed) {
		this.allowed = allowed;
	}
	
	
	
}
