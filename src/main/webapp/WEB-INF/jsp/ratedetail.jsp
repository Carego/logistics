<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rate Details</title>
 <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
      <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      <script src='jquery-1.9.1.min.js'></script>
   
    <script>
     $(document).ready(function(){
   	 $('#submit1').click(function() {
   		var para ="billedOn="+$('#billedOn').val()+"&"
   		+"billSubTerm="+$('#billSubTerm').val()+"&"
   		+"contId="+$('#contId').val()+"&"
   		+"custId="+$('#custId').val()+"&"
   		+"billPaymentTerm="+$('#billPaymentTerm').val();
   		var xmlhttp;
   		
   		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  		xmlhttp=new XMLHttpRequest();
	  	}else{// code for IE6, IE5
	  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  	}
   		
	  	xmlhttp.open("POST","/MyLogistics/savepayment",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send(para);
   	 });
 });
     </script>  
     
      <script>
     $(document).ready(function(){
   	 $('#submit2').click(function(){
   		var para ="businessHName="+$('#businessHName').val()+"&"
   		+"directorName="+$('#directorName').val()+"&"
   		+"corporatePersonName="+$('#corporatePersonName').val()+"&"
   		+"allotedBy="+$('#allotedBy').val()+"&"
   		+"dtOfAllotment="+$('#dtOfAllotment').val()+"&"
   		+"custId="+$('#custId').val()+"&"
   		+"branchPersonName="+$('#branchPersonName').val();
   		
   		var xmlhttp;
   		
   		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  		xmlhttp=new XMLHttpRequest();
	  	}else{// code for IE6, IE5
	  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  	}
   		 
   		xmlhttp.open("POST","/MyLogistics/savecontractauthority",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send(para);
   	 });
   });
     </script> 
      
      <script>
      var i= 1;
     $(document).ready(function(){
   	 $('#submit3').click(function(){
   		var para ="placeFrom="+$('#placeFrom').val()+"&"
   		+"placeTo="+$('#placeTo').val()+"&"
   		+"rate="+$('#rate').val()+"&"
   		+"vehicleType="+$('#vehicleType').val()+"&"
   		+"guaranteeWt="+$('#guaranteeWt').val()+"&"
   		+"transitTime="+$('#transitTime').val()+"&"
   		+"contId="+$('#contId').val();
   	
   		var xmlhttp;
		var res;
		$.ajax({
	        type: "POST",
	        url: "/MyLogistics/savecontractratedetail",
	        data: para,
	        async: false,
	        success: function(response){
	        // we have the response
	     
	        $('.contRateId').val(response);
	        res = response;
	        },
	        error: function(e){
	        alert('Error: ' + e);
	        }
	        });
		
			var w= $('#placeFrom').val();
			$('#placeFrom1').val(w);
		    
		   
	   		var e = $('#placeTo').val();
	   		$('#placeTo1').val(e);
	   		  	
	     	var t = $('#rate').val();
	   	  	$('#rate1').val(t);
				 
			var y = $('#vehicleType').val()
			$('#vehicleType1').val(y);
			
			var r = $('#guaranteeWt').val();
	   	 	$('#guaranteeWt1').val(r);
			
			var u = $('#transitTime').val();
	   	 	$('#transitTime1').val(u);
			
	   		var p = $('#contId').val();
	   		$('#contId1').val(p);
	   		
	   		$('div#contractratedetail').dialog('close');	
		if(i==1)
			{
		var data="<tr><th></th><th>Serial No.</th><th>Place From</th><th>Place To</th><th>Rate</th><th>Vehicle Type</th><th>Guarantee Weight</th><th>Transit Time</th><th>Contract Id</th><th>Contract Rate Id</th></tr>"+
		"<tr><td><input type='checkbox' name='case' class='case'/></td><td>"+i+"</td>";
		data +="<td>"+$('#placeFrom1').val()+"</td> <td>"+$('#placeTo1').val()+"</td><td>"+$('#rate').val()+"</td><td>"+$('#vehicleType1').val()+"</td><td>"+$('#guaranteeWt1').val()+"</td><td>"+$('#transitTime1').val()+"</td><td>"+$('#contId1').val()+"</td><td>"+res+"</td></tr>";
			}
		else{
			var data="<tr></td><td><input type='checkbox' name='case' class='case'/></td><td>"+i+"</td>"			
			data+="<td>"+$('#placeFrom1').val()+"</td> <td>"+$('#placeTo1').val()+"</td><td>"+$('#rate').val()+"</td><td>"+$('#vehicleType1').val()+"</td><td>"+$('#guaranteeWt1').val()+"</td><td>"+$('#transitTime1').val()+"</td><td>"+$('#contId1').val()+"</td><td>"+res+"</td></tr>";
			
		}
		$('table#addnewrow').append(data);
		i=i+1;
		$('#placeFrom').val('');
		$('#placeTo').val('');
		$('#rate').val('');
		$('#vehicleType').val('');
		$('#guaranteeWt').val('');
		$('#transitTime').val('');
		 });
   });
     </script> 
    
   <script>
     $(document).ready(function() {
    	 $('#deleterows').click(function () {
    		
    		 var contractId = [];
    		 $.each($('.case:checked').parents("tr").remove(), function(){  
    			    var id = $(this).closest("tr").find('td:eq(9)').text();
    			    contractId.push(id);
    			});
    		 $y=contractId.toString();
 			$('input[id="storecontId"]').val($y);
				
 			 var para="storecontId="+$('#storecontId').val();
	 		 var xmlhttp;
 			
 			if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  		xmlhttp=new XMLHttpRequest();
		  	}else{// code for IE6, IE5
		  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  	}
			
			xmlhttp.open("POST","/MyLogistics/deleteratecontract.html",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send(para);
		 }); 
    });
     </script>
     
	<script type="text/javascript">
	
			$(document).ready(
				function() {
				$('div#contractratedetail').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				draggable:false
				});
		
			$('input#addcontractratedetail').click(
			function($e) {
			$('div#contractratedetail').dialog('open');
			});
		});
			
			$(function() {
		    	$( "#dtOfAllotment" ).datepicker();
		 				});
			
	</script>
</head>

<body>


<h3>Payment Details</h3>
	<table>
		<tr>
			<td>Billed On</td>
			<td><input type="text" name="billedOn" id="billedOn"></td>
		</tr>
		
		<tr>
			<td>Bill Submission Term</td>
			<td><input type="text" name="billSubTerm" id="billSubTerm"></td>
		</tr>
		
		<tr>
			<td>Bill Payment Term</td>
			<td><input type="text" name="billPaymentTerm" id="billPaymentTerm"></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="contId" id="contId" value="${contract.contId}"></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="custId" id="custId" value="${contract.custId}"></td>
		</tr>
		
		<tr>
			<td><input type="submit" id="submit1" value="Submit"></td>
		</tr>
		
	</table>

		
		
	
<h3>Contract Authority Details</h3>	
	<table>	
		
		<tr>
			<td>Business Head Name</td>
			<td><input type="text" name="businessHName" id="businessHName"></td>
		</tr>
		
		<tr>
			<td>Director Name</td>
			<td><input type="text" name="directorName" id="directorName"></td>
		</tr>
		
		<tr>
			<td>Branch Person Name</td>
			<td><input type="text" name="branchPersonName" id="branchPersonName"></td>
		</tr>
		
		<tr>
			<td>Corporate Person Name</td>
			<td><input type="text" name="corporatePersonName" id="corporatePersonName"></td>
		</tr>
		
		<tr>
			<td>Alloted By</td>
			<td><input type="text" name="allotedBy" id="allotedBy"></td>
		</tr>
		
		<tr>
			<td>Date Of Allotment</td>
			<td><input type="text" name="dtOfAllotment" id="dtOfAllotment" readonly="true"></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="custId" id="custId" value="${contract.custId}"></td>
		</tr>
		
		<tr>
			<td><input type="submit" id="submit2" value="Submit"></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="placeFrom1" id="placeFrom1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="placeTo1" id="placeTo1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="rate1" id="rate1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="vehicleType1" id="vehicleType1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="guaranteeWt1" id="guaranteeWt1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="transitTime1" id="transitTime1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="contId1" id="contId1" ></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="contRateId1" id="contRateId1" class="contRateId"></td>
		</tr>
		
		<tr>
				<td><input type ="hidden" name="storecontId" id="storecontId">
		</tr>
		
	</table>


<div id="contractratedetail">

<table>
		<tr>
			<td>Place From</td>
			<td><input type="text" name="placeFrom" id="placeFrom"></td>
		</tr>
		
		<tr>
			<td>Place To</td>
			<td><input type="text" name="placeTo" id="placeTo"></td>
		</tr>
		
		<tr>
			<td>Rate</td>
			<td><input type="text" name="rate" id="rate"></td>
		</tr>
		
		<tr>
			<td>Vehicle Type</td>
			<td><input type="text" name="vehicleType" id="vehicleType"></td>
		</tr>
		
		<tr>
			<td>Guarantee Weight</td>
			<td><input type="text" name="guaranteeWt" id="guaranteeWt"></td>
		</tr>
		
		<tr>
			<td>Transit Time</td>
			<td><input type="text" name="transitTime" id="transitTime"></td>
		</tr>
		
		<tr>
			<td><input type="hidden" name="contId" id="contId" value="${contract.contId}"></td>
		</tr>
		
		<tr>
			<td align="center"><input type="submit" id="submit3" value="Submit!"></td>
		</tr> 

		
</table>

</div>

<form>
	<input type="button" id="addcontractratedetail" value="Add Contract Rate Details">	
	<input type="button" id="deleterows" value="Delete Rows" class='delete'>	
	
		<table id="addnewrow"  border="1" cellspacing="0">	
			
		</table>
		
</form>
</body>
</html>