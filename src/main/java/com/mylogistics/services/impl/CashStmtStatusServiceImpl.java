package com.mylogistics.services.impl;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.services.CashStmtStatusService;

@Service
public class CashStmtStatusServiceImpl implements CashStmtStatusService {
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	@Autowired
	private CashStmtDAO cashStmtDAO;
	
	@Transactional
	public CashStmtStatus getCashStmtStatus(String branchCode, Date cssDate) {
		return cashStmtStatusDAO.getCashStmtStatus(branchCode, cssDate);
	}
	
	@Transactional
	public int getLastVoucherNo(int cssId) {
		CashStmt cashStmt = cashStmtDAO.getLastCashStmt(cssId);
		if (cashStmt != null)
			return cashStmt.getCsVouchNo();
		
		return 0;
	}
	
	@Transactional
	public void merge(CashStmtStatus css) {
		cashStmtStatusDAO.merge(css);
	}

}