package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "broker")
public class Broker {
	
	@Id
	@Column(name="brkId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brkId;
	
	@Column(name="brkImgId" , columnDefinition = "int default -1")
	private Integer brkImgId;
	
	@Column(name="brkIsPanImg" , columnDefinition = "boolean default false")
	private boolean brkIsPanImg;
	
	@Column(name="brkIsDecImg" , columnDefinition = "boolean default false")
	private boolean brkIsDecImg;
	
	@Column(name="brkCode")
	private String brkCode;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="brkName")
	private String brkName;
	
	@Column(name="brkComStbDt")
	private Date brkComStbDt;
	
	@Column(name="brkPanDOB")
	private Date brkPanDOB;
	
	@Column(name="brkPanDt")
	private Date brkPanDt;
	
	@Column(name="brkPanName")
	private String brkPanName;
	
	@Column(name="brkPanNo")
	private String brkPanNo;
	
	@Column(name="brkVehicleType")
	private String brkVehicleType;
	
	@Column(name="brkPPNo")
	private String brkPPNo;
	
	@Column(name="brkVoterId")
	private String brkVoterId;
	
	@Column(name="brkSrvTaxNo")
	private String brkSrvTaxNo;
	
	@Column(name="brkFirmRegNo")
	private String brkFirmRegNo;
	
	@Column(name="brkRegPlace")
	private String brkRegPlace;
	
	@Column(name="brkFirmType")
	private String brkFirmType;
	
	@Column(name="brkFirmName")
	private String brkFirmName;
	
	@Column(name="brkAcntHldrName")
	private String brkAcntHldrName;
	
	@Column(name="brkCIN")
	private String brkCIN;
	
	@Column(name="brkBsnCard")
	private String brkBsnCard;
	
	@Column(name="brkUnion")
	private String brkUnion;
	
	@Column(name="brkEmailId")
	private String brkEmailId;
	
	@Column(name="brkActiveDt")
	private Date brkActiveDt;
	
	@Column(name="brkDeactiveDt")
	private Date brkDeactiveDt;
	
	@Column(name="brkPhNoList")
	private ArrayList<String> brkPhNoList = new ArrayList<>();
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="brkFaCode")
	private String brkFaCode;
	
	@Column(name="brkPanIntRt" , columnDefinition = "float default 0.0")
	private float brkPanIntRt;

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="isView")
	private boolean isView;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;
	
	//Bank Details
	@Column(name = "brkAccntNo")
	private String brkAccntNo;
	
	@Column(name = "brkIfsc")
	private String brkIfsc;
	
	@Column(name = "brkBnkBranch")
	private String brkBnkBranch;
	
	@Column(name = "brkMicr")
	private String brkMicr;
	
	@Column(name="isBrkBnkDet",nullable = false,columnDefinition = "bit default false")
	private boolean isBrkBnkDet;
	
	@Column(name="brkValidPan",nullable = false,columnDefinition = "bit default false")
	private boolean brkValidPan;
	
	@Column(name="brkInvalidPan",nullable = false,columnDefinition = "bit default false")
	private boolean brkInvalidPan;
	
	@Column(name="brkBnkDetValid", columnDefinition="boolean default false")
	private boolean brkBnkDetValid;
	
	@Column(name="brkBnkDetInValid", columnDefinition="boolean default false")
	private boolean brkBnkDetInValid;
	
	@Column(name="brkBnkDetVerifyBy", length=6)
	private String brkBnkDetVerifyBy;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="broker_vehven",
	joinColumns=@JoinColumn(name="brkId"),
	inverseJoinColumns=@JoinColumn(name="vvId"))
	private List<VehicleVendorMstr> vehicleVendorMstrList = new ArrayList<>();

	public int getBrkId() {
		return brkId;
	}

	public void setBrkId(int brkId) {
		this.brkId = brkId;
	}
	
	public String getBrkPanNo() {
		return brkPanNo;
	}

	public void setBrkPanNo(String brkPanNo) {
		this.brkPanNo = brkPanNo;
	}

	public String getBrkCode() {
		return brkCode;
	}

	public void setBrkCode(String brkCode) {
		this.brkCode = brkCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBrkName() {
		return brkName;
	}

	public void setBrkName(String brkName) {
		this.brkName = brkName;
	}

	public Date getBrkComStbDt() {
		return brkComStbDt;
	}

	public void setBrkComStbDt(Date brkComStbDt) {
		this.brkComStbDt = brkComStbDt;
	}

	public Date getBrkPanDOB() {
		return brkPanDOB;
	}

	public void setBrkPanDOB(Date brkPanDOB) {
		this.brkPanDOB = brkPanDOB;
	}

	public Date getBrkPanDt() {
		return brkPanDt;
	}

	public void setBrkPanDt(Date brkPanDt) {
		this.brkPanDt = brkPanDt;
	}

	public String getBrkPanName() {
		return brkPanName;
	}

	public void setBrkPanName(String brkPanName) {
		this.brkPanName = brkPanName;
	}

	public String getBrkVehicleType() {
		return brkVehicleType;
	}

	public void setBrkVehicleType(String brkVehicleType) {
		this.brkVehicleType = brkVehicleType;
	}

	public String getBrkPPNo() {
		return brkPPNo;
	}

	public void setBrkPPNo(String brkPPNo) {
		this.brkPPNo = brkPPNo;
	}

	public String getBrkVoterId() {
		return brkVoterId;
	}

	public void setBrkVoterId(String brkVoterId) {
		this.brkVoterId = brkVoterId;
	}

	public String getBrkSrvTaxNo() {
		return brkSrvTaxNo;
	}

	public void setBrkSrvTaxNo(String brkSrvTaxNo) {
		this.brkSrvTaxNo = brkSrvTaxNo;
	}

	public String getBrkFirmRegNo() {
		return brkFirmRegNo;
	}

	public void setBrkFirmRegNo(String brkFirmRegNo) {
		this.brkFirmRegNo = brkFirmRegNo;
	}

	public String getBrkRegPlace() {
		return brkRegPlace;
	}

	public void setBrkRegPlace(String brkRegPlace) {
		this.brkRegPlace = brkRegPlace;
	}

	public String getBrkFirmType() {
		return brkFirmType;
	}

	public void setBrkFirmType(String brkFirmType) {
		this.brkFirmType = brkFirmType;
	}

	public String getBrkCIN() {
		return brkCIN;
	}

	public void setBrkCIN(String brkCIN) {
		this.brkCIN = brkCIN;
	}

	public String getBrkBsnCard() {
		return brkBsnCard;
	}

	public void setBrkBsnCard(String brkBsnCard) {
		this.brkBsnCard = brkBsnCard;
	}

	public String getBrkUnion() {
		return brkUnion;
	}

	public void setBrkUnion(String brkUnion) {
		this.brkUnion = brkUnion;
	}

	public String getBrkEmailId() {
		return brkEmailId;
	}

	public void setBrkEmailId(String brkEmailId) {
		this.brkEmailId = brkEmailId;
	}

	public Date getBrkActiveDt() {
		return brkActiveDt;
	}

	public void setBrkActiveDt(Date brkActiveDt) {
		this.brkActiveDt = brkActiveDt;
	}

	public Date getBrkDeactiveDt() {
		return brkDeactiveDt;
	}

	public void setBrkDeactiveDt(Date brkDeactiveDt) {
		this.brkDeactiveDt = brkDeactiveDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getBrkFaCode() {
		return brkFaCode;
	}

	public void setBrkFaCode(String brkFaCode) {
		this.brkFaCode = brkFaCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}

	public String getBrkAccntNo() {
		return brkAccntNo;
	}

	public void setBrkAccntNo(String brkAccntNo) {
		this.brkAccntNo = brkAccntNo;
	}

	public String getBrkIfsc() {
		return brkIfsc;
	}

	public void setBrkIfsc(String brkIfsc) {
		this.brkIfsc = brkIfsc;
	}

	public String getBrkBnkBranch() {
		return brkBnkBranch;
	}

	public void setBrkBnkBranch(String brkBnkBranch) {
		this.brkBnkBranch = brkBnkBranch;
	}

	public String getBrkMicr() {
		return brkMicr;
	}

	public void setBrkMicr(String brkMicr) {
		this.brkMicr = brkMicr;
	}

	public boolean isBrkBnkDet() {
		return isBrkBnkDet;
	}

	public void setBrkBnkDet(boolean isBrkBnkDet) {
		this.isBrkBnkDet = isBrkBnkDet;
	}

	public Integer getBrkImgId() {
		return brkImgId;
	}

	public void setBrkImgId(Integer brkImgId) {
		this.brkImgId = brkImgId;
	}

	public float getBrkPanIntRt() {
		return brkPanIntRt;
	}

	public void setBrkPanIntRt(float brkPanIntRt) {
		this.brkPanIntRt = brkPanIntRt;
	}

	public boolean isBrkIsPanImg() {
		return brkIsPanImg;
	}

	public void setBrkIsPanImg(boolean brkIsPanImg) {
		this.brkIsPanImg = brkIsPanImg;
	}

	public boolean isBrkIsDecImg() {
		return brkIsDecImg;
	}

	public void setBrkIsDecImg(boolean brkIsDecImg) {
		this.brkIsDecImg = brkIsDecImg;
	}

	public List<VehicleVendorMstr> getVehicleVendorMstrList() {
		return vehicleVendorMstrList;
	}

	public void setVehicleVendorMstrList(
			List<VehicleVendorMstr> vehicleVendorMstrList) {
		this.vehicleVendorMstrList = vehicleVendorMstrList;
	}

	public ArrayList<String> getBrkPhNoList() {
		return brkPhNoList;
	}

	public void setBrkPhNoList(ArrayList<String> brkPhNoList) {
		this.brkPhNoList = brkPhNoList;
	}

	public boolean isBrkValidPan() {
		return brkValidPan;
	}

	public void setBrkValidPan(boolean brkValidPan) {
		this.brkValidPan = brkValidPan;
	}

	public boolean isBrkInvalidPan() {
		return brkInvalidPan;
	}

	public void setBrkInvalidPan(boolean brkInvalidPan) {
		this.brkInvalidPan = brkInvalidPan;
	}

	public String getBrkFirmName() {
		return brkFirmName;
	}

	public void setBrkFirmName(String brkFirmName) {
		this.brkFirmName = brkFirmName;
	}

	public String getBrkAcntHldrName() {
		return brkAcntHldrName;
	}

	public void setBrkAcntHldrName(String brkAcntHldrName) {
		this.brkAcntHldrName = brkAcntHldrName;
	}

	public boolean isBrkBnkDetValid() {
		return brkBnkDetValid;
	}

	public void setBrkBnkDetValid(boolean brkBnkDetValid) {
		this.brkBnkDetValid = brkBnkDetValid;
	}

	public String getBrkBnkDetVerifyBy() {
		return brkBnkDetVerifyBy;
	}

	public void setBrkBnkDetVerifyBy(String brkBnkDetVerifyBy) {
		this.brkBnkDetVerifyBy = brkBnkDetVerifyBy;
	}

	public boolean isBrkBnkDetInValid() {
		return brkBnkDetInValid;
	}

	public void setBrkBnkDetInValid(boolean brkBnkDetInValid) {
		this.brkBnkDetInValid = brkBnkDetInValid;
	}
	
	
}
