package com.mylogistics.model.lhpv;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mylogistics.model.Challan;

@Entity
@Table(name = "lhpvbal")
public class LhpvBal {

	@Id
	@Column(name="lbId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int lbId;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonBackReference(value = "lslb")
	@JoinColumn(name = "lsId") 
	private LhpvStatus lhpvStatus;
	
	//One To One
	@JsonIgnore
	@JsonManagedReference(value = "lbchln")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvbal_challan",
	joinColumns=@JoinColumn(name="lbId"),
	inverseJoinColumns=@JoinColumn(name="chlnId"))
	private Challan challan;
	
	@Column(name="lbNo")
	private int lbNo;
	
	@Column(name="lbDt")
	private Date lbDt;
	
	@Column(name="lbIsClose" , columnDefinition="boolean default false")
	private boolean lbIsClose;
	
	@Column(name="lbBrhCode")
	private String lbBrhCode;
	
	@Column(name="lbBrkOwn")
	private String lbBrkOwn;
	
	@Column(name="lbLryBalP" , columnDefinition="double default 0.0")
	private double lbLryBalP;
	
	@Column(name="lbUnLoadingP" , columnDefinition="double default 0.0")
	private double lbUnLoadingP;
	
	@Column(name="lbUnpDetP" , columnDefinition="double default 0.0")
	private double lbUnpDetP;
	
	@Column(name="lbOthExtKmP" , columnDefinition="double default 0.0")
	private double lbOthExtKmP;
	
	@Column(name="lbOthOvrHgtP" , columnDefinition="double default 0.0")
	private double lbOthOvrHgtP;
	
	@Column(name="lbOthPnltyP" , columnDefinition="double default 0.0") 
	private double lbOthPnltyP;
	
	@Column(name="lbOthMisctP" , columnDefinition="double default 0.0")
	private double lbOthMiscP;
	
	@Column(name="lbCashDiscR" , columnDefinition="double default 0.0")
	private double lbCashDiscR;
	
	@Column(name="lbMunsR" , columnDefinition="double default 0.0")
	private double lbMunsR;
	
	@Column(name="lbTdsR" , columnDefinition="double default 0.0")
	private double lbTdsR;
	
	@Column(name="lbWtShrtgCR" , columnDefinition="double default 0.0")
	private double lbWtShrtgCR;
	
	@Column(name="lbDrRcvrWtCR" , columnDefinition="double default 0.0")
	private double lbDrRcvrWtCR;
	
	@Column(name="lbDmgCR" , columnDefinition="double default 0.0")
	private double lbDmgCR;
	
	@Column(name="lbLateDelCR" , columnDefinition="double default 0.0")
	private double lbLateDelCR;
	
	@Column(name="lbLateAckCR" , columnDefinition="double default 0.0")
	private double lbLateAckCR;
	
	@Column(name="lbPayMethod")
	private char lbPayMethod;
	
	@Column(name="lbBankCode")
	private String lbBankCode;
	
	@Column(name="lbChqType")
	private char lbChqType;
	
	@Column(name="lbChqNo")
	private String lbChqNo;
	
	@Column(name="lbTotPayAmt" , columnDefinition="double default 0.0")
	private double lbTotPayAmt;
	
	@Column(name="lbTotRcvrAmt" , columnDefinition="double default 0.0")
	private double lbTotRcvrAmt;
	
	@Column(name="lbFinalTot" , columnDefinition="double default 0.0")
	private double lbFinalTot;
	
	@Column(name="lbDesc")
	private String lbDesc;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="stafCode")
	private String stafCode;
	
	@Column (name="payToStf")
	private String payToStf;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="lbIsArRept" , columnDefinition="boolean default true")
	private boolean lbIsArRept;

	@Column(name="lbBankName")
	private String lbBankName;
	
	@Column(name="lbIfscCode")
	private String lbIfscCode;

	@Column(name="lbAccountNo")
	private String lbAccountNo;

	@Column(name="lbPayeeName")
	private String lbPayeeName;
	
	@Column(name="lbIsPaid")
	private Boolean lbIsPaid;
	
	@Transient
	private String sheetNo;
	
	public int getLbId() {
		return lbId;
	}

	public void setLbId(int lbId) {
		this.lbId = lbId;
	}

	public LhpvStatus getLhpvStatus() {
		return lhpvStatus;
	}

	public void setLhpvStatus(LhpvStatus lhpvStatus) {
		this.lhpvStatus = lhpvStatus;
	}

	public double getLbLryBalP() {
		return lbLryBalP;
	}

	public void setLbLryBalP(double lbLryBalP) {
		this.lbLryBalP = lbLryBalP;
	}

	public double getLbUnLoadingP() {
		return lbUnLoadingP;
	}

	public void setLbUnLoadingP(double lbUnLoadingP) {
		this.lbUnLoadingP = lbUnLoadingP;
	}

	public double getLbUnpDetP() {
		return lbUnpDetP;
	}

	public void setLbUnpDetP(double lbUnpDetP) {
		this.lbUnpDetP = lbUnpDetP;
	}

	public double getLbOthExtKmP() {
		return lbOthExtKmP;
	}

	public void setLbOthExtKmP(double lbOthExtKmP) {
		this.lbOthExtKmP = lbOthExtKmP;
	}

	public double getLbOthOvrHgtP() {
		return lbOthOvrHgtP;
	}

	public void setLbOthOvrHgtP(double lbOthOvrHgtP) {
		this.lbOthOvrHgtP = lbOthOvrHgtP;
	}

	public double getLbOthPnltyP() {
		return lbOthPnltyP;
	}

	public void setLbOthPnltyP(double lbOthPnltyP) {
		this.lbOthPnltyP = lbOthPnltyP;
	}

	public double getLbOthMiscP() {
		return lbOthMiscP;
	}

	public void setLbOthMiscP(double lbOthMiscP) {
		this.lbOthMiscP = lbOthMiscP;
	}

	public double getLbCashDiscR() {
		return lbCashDiscR;
	}

	public void setLbCashDiscR(double lbCashDiscR) {
		this.lbCashDiscR = lbCashDiscR;
	}

	public double getLbMunsR() {
		return lbMunsR;
	}

	public void setLbMunsR(double lbMunsR) {
		this.lbMunsR = lbMunsR;
	}

	public double getLbTdsR() {
		return lbTdsR;
	}

	public void setLbTdsR(double lbTdsR) {
		this.lbTdsR = lbTdsR;
	}

	public double getLbWtShrtgCR() {
		return lbWtShrtgCR;
	}

	public void setLbWtShrtgCR(double lbWtShrtgCR) {
		this.lbWtShrtgCR = lbWtShrtgCR;
	}

	public double getLbDmgCR() {
		return lbDmgCR;
	}

	public void setLbDmgCR(double lbDmgCR) {
		this.lbDmgCR = lbDmgCR;
	}

	public double getLbLateDelCR() {
		return lbLateDelCR;
	}

	public void setLbLateDelCR(double lbLateDelCR) {
		this.lbLateDelCR = lbLateDelCR;
	}

	public double getLbLateAckCR() {
		return lbLateAckCR;
	}

	public void setLbLateAckCR(double lbLateAckCR) {
		this.lbLateAckCR = lbLateAckCR;
	}

	public char getLbPayMethod() {
		return lbPayMethod;
	}

	public void setLbPayMethod(char lbPayMethod) {
		this.lbPayMethod = lbPayMethod;
	}

	public String getLbBankCode() {
		return lbBankCode;
	}

	public void setLbBankCode(String lbBankCode) {
		this.lbBankCode = lbBankCode;
	}

	public char getLbChqType() {
		return lbChqType;
	}

	public void setLbChqType(char lbChqType) {
		this.lbChqType = lbChqType;
	}

	public String getLbChqNo() {
		return lbChqNo;
	}

	public void setLbChqNo(String lbChqNo) {
		this.lbChqNo = lbChqNo;
	}

	public double getLbTotPayAmt() {
		return lbTotPayAmt;
	}

	public void setLbTotPayAmt(double lbTotPayAmt) {
		this.lbTotPayAmt = lbTotPayAmt;
	}

	public double getLbTotRcvrAmt() {
		return lbTotRcvrAmt;
	}

	public void setLbTotRcvrAmt(double lbTotRcvrAmt) {
		this.lbTotRcvrAmt = lbTotRcvrAmt;
	}

	public double getLbFinalTot() {
		return lbFinalTot;
	}

	public void setLbFinalTot(double lbFinalTot) {
		this.lbFinalTot = lbFinalTot;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public Challan getChallan() {
		return challan;
	}

	public void setChallan(Challan challan) {
		this.challan = challan;
	}

	public String getLbBrkOwn() {
		return lbBrkOwn;
	}

	public void setLbBrkOwn(String lbBrkOwn) {
		this.lbBrkOwn = lbBrkOwn;
	}

	public int getLbNo() {
		return lbNo;
	}

	public void setLbNo(int lbNo) {
		this.lbNo = lbNo;
	}

	public boolean isLbIsClose() {
		return lbIsClose;
	}

	public void setLbIsClose(boolean lbIsClose) {
		this.lbIsClose = lbIsClose;
	}

	public Date getLbDt() {
		return lbDt;
	}

	public void setLbDt(Date lbDt) {
		this.lbDt = lbDt;
	}

	public String getLbDesc() {
		return lbDesc;
	}

	public void setLbDesc(String lbDesc) {
		this.lbDesc = lbDesc;
	}

	public String getLbBrhCode() {
		return lbBrhCode;
	}

	public void setLbBrhCode(String lbBrhCode) {
		this.lbBrhCode = lbBrhCode;
	}
	
	public double getLbDrRcvrWtCR() {
		return lbDrRcvrWtCR;
	}

	public void setLbDrRcvrWtCR(double lbDrRcvrWtCR) {
		this.lbDrRcvrWtCR = lbDrRcvrWtCR;
	}

	public boolean isLbIsArRept() {
		return lbIsArRept;
	}

	public void setLbIsArRept(boolean lbIsArRept) {
		this.lbIsArRept = lbIsArRept;
	}

	public String getSheetNo() {
		return sheetNo;
	}

	public void setSheetNo(String sheetNo) {
		this.sheetNo = sheetNo;
	}

	public String getStafCode() {
		return stafCode;
	}

	public void setStafCode(String stafCode) {
		this.stafCode = stafCode;
	}

	public String getPayToStf() {
		return payToStf;
	}

	public void setPayToStf(String payToStf) {
		this.payToStf = payToStf;
	}

	public String getLbBankName() {
		return lbBankName;
	}

	public void setLbBankName(String lbBankName) {
		this.lbBankName = lbBankName;
	}

	public String getLbIfscCode() {
		return lbIfscCode;
	}

	public void setLbIfscCode(String lbIfscCode) {
		this.lbIfscCode = lbIfscCode;
	}

	public String getLbAccountNo() {
		return lbAccountNo;
	}

	public void setLbAccountNo(String lbAccountNo) {
		this.lbAccountNo = lbAccountNo;
	}

	public String getLbPayeeName() {
		return lbPayeeName;
	}

	public void setLbPayeeName(String lbPayeeName) {
		this.lbPayeeName = lbPayeeName;
	}

	public Boolean getLbIsPaid() {
		return lbIsPaid;
	}

	public void setLbIsPaid(Boolean lbIsPaid) {
		this.lbIsPaid = lbIsPaid;
	}
	
	
	
}
