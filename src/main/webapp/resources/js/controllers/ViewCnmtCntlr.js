'use strict';

var app = angular.module('application');

app.controller('ViewCnmtCntlr',['$scope','$location','$http','FileUploadService','$filter',
                                 function($scope,$location,$http,FileUploadService,$filter){
	
	$scope.CnmtDBFlag=true;
	$scope.show = true;
	$scope.showCnmtDetailsFlag=true;
	$scope.BranchCodeDBFlag=true;
	$scope.CnmtCodeDBFlag=true;
	$scope.CustomerCodeDBFlag=true;
	$scope.CNMTConsignorDBFlag=true;
	$scope.CNMTConsigneeDBFlag=true;
	$scope.ContractCodeDBFlag=true;
	$scope.CNMTPayAtDBFlag=true;
	$scope.CNMTBillAtDBFlag=true;
	$scope.EmployeeCodeDBFlag=true;
	$scope.StateCodeDBFlag=true;
	$scope.saveInvoiceNoFlag=true;
	$scope.CnmtFromStationDBFlag=true;
	$scope.CnmtToStationDBFlag=true;
	$scope.ProductTypeFlag = true;
	$scope.VehicleTypeDBFlag=true;
	$scope.cnmt = {};
	$scope.K_Type = false;
	$scope.cnmtRate = 0;
	$scope.cnmtGuaranteeWt = 0;
	$scope.actualGarWt = 0;
	$scope.actualCnmtRate = 0;
	$scope.autoToStnFlag = false;
	
	$scope.actWt = 0;
	$scope.garunteeWt = 0;
	
	$scope.contract = {};
	$scope.customer = {};
	
	$scope.chlnNSedrList = [];
	    
	  $('#cnmtRate').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  });


	  $('#cnmtRate').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	          e.preventDefault();
	      }
	  });
	  
	  $('#cnmtFreight').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  });


	  $('#cnmtFreight').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	          e.preventDefault();
	      }
	  });
	
	$('#cnmtActualWt').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  });
	  
	  $('#cnmtGuaranteeWt').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  });
	  
	  $('#cnmtNoOfPkg').keypress(function(key) {
	      if(key.charCode < 48 || key.charCode > 57)
	          return false;
	  });


	  $('#cnmtNoOfPkg').keypress(function(e) {
	      if (this.value.length == 9) {
	          e.preventDefault();
	      }
	  });
	  	  
	  $('#cnmtVOG').keypress(function(key) {
	      if(key.charCode < 48 || key.charCode > 57)
	          return false;
	  });


	  $('#cnmtVOG').keypress(function(e) {
		  if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	          e.preventDefault();
	      }
	  });
	  
	  $('#cnmtExtraExp').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  });
	  
	  $('#cnmtKmId').keypress(function(key) {
	      if(key.charCode < 48 || key.charCode > 57)
	          return false;
	  });


	  $('#cnmtKmId').keypress(function(e) {
	      if (this.value.length == 15) {
	          e.preventDefault();
	      }
	  });
	  
	  $('#cnmtInvoiceNo').keypress(function(e) {
	      if (this.value.length == 15) {
	          e.preventDefault();
	      }
	  });
	  
	  $('#invoiceNo').keypress(function(e) {
	      if (this.value.length == 15) {
	          e.preventDefault();
	      }
	  });
	
	
	$scope.OpenCnmtDB = function(){		
		if($scope.cnmtCode.length < 6)
			return;		
		
		$scope.getCnmtList();		
		$scope.CnmtDBFlag=false;
		$('div#cnmtDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#cnmtDB').dialog('open');
	}
	
	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#branchCodeDB').dialog('open');
	}
	
	$scope.OpenCnmtCodeDB = function(){
		$scope.CnmtCodeDBFlag=false;
		$('div#cnmtCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#cnmtCodeDB').dialog('open');
	}
	
	$scope.OpenCustomerCodeDB = function(){
		$scope.CustomerCodeDBFlag=false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#customerCodeDB').dialog('open');
	}
	
	$scope.OpenCnmtConsignorDB = function(){
		$scope.CNMTConsignorDBFlag=false;
		$('div#CNMTConsignorDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Consignor Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#CNMTConsignorDB').dialog('open');
	}
	
	$scope.OpenCnmtConsigneeDB = function(){
		$scope.CNMTConsigneeDBFlag=false;
		$('div#CNMTConsigneeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Consignee Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#CNMTConsigneeDB').dialog('open');
	}
	
	$scope.OpenContractCodeDB = function(){
		$scope.ContractCodeDBFlag=false;
		$('div#contractCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Contract Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#contractCodeDB').dialog('open');
	}
	
	$scope.OpenCnmtPayAtDB = function(){
		$scope.CNMTPayAtDBFlag=false;
		
		$('div#CNMTPayAtDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Pay At Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#CNMTPayAtDB').dialog('open');
	}
	
	$scope.OpenCnmtBillAtDB = function(){
		$scope.CNMTBillAtDBFlag=false;
		$('div#CNMTBillAtDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Bill At Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#CNMTBillAtDB').dialog('open');
	}
	
	$scope.OpenEmployeeCodeDB = function(){
		$scope.EmployeeCodeDBFlag=false;
		$('div#employeeCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Employee Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#employeeCodeDB').dialog('open');
	}
	
	$scope.OpenStateCodeDB = function(){
		$scope.StateCodeDBFlag=false;
		$('div#stateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "State Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#stateCodeDB').dialog('open');
	}
	
	$scope.openInvoiceNo = function(){
		console.log("Enter into openInvoiceNo");
		$scope.saveInvoiceNoFlag=false;
		$scope.invoiceNo = "";
		$('div#saveInvoiceNo').dialog({
			autoOpen: false,
			modal:true,
			title: "Save Invoice No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});
		console.log("Line no 342");
		$('div#saveInvoiceNo').dialog('open');	
	}
	
	$scope.OpenCnmtFromStationDB = function(){
		$scope.CnmtFromStationDBFlag=false;
		$('div#cnmtFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Cnmt From Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#cnmtFromStationDB').dialog('open');
	}
	
	$scope.OpenCnmtToStationDB = function(){
		$scope.CnmtToStationDBFlag=false;
		$('div#cnmtToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Cnmt To Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#cnmtToStationDB').dialog('open');
	}
	
	$scope.OpencnmtProductTypeDB = function(){
		if(angular.isUndefined($scope.cnmt.cnmtToSt)){
			$scope.alertToast("enter To Station Code and No. OF Packages");
		}else{
		$scope.ProductTypeFlag = false;
		$('div#cnmtProductTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Product Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
	$('div#cnmtProductTypeDB').dialog('open');
		}
	}
	
	
	$scope.OpenVehicleTypeDB = function(){
		$scope.VehicleTypeDBFlag=false;
		$('div#vehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vihicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
		});

		$('div#vehicleTypeDB').dialog('open');
	}
	
	$scope.saveStateCode =  function(state){
		console.log("enter into saveStateCode----->");
		$scope.cnmt.cnmtState = state.stateCode;
		$('div#stateCodeDB').dialog('close');
		$scope.StateCodeDBFlag=true;
		if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState === null || $scope.cnmt.cnmtState ==="" || 
			angular.isUndefined($scope.cnmt.cnmtKm)|| $scope.cnmt.cnmtKm === null || $scope.cnmt.cnmtKm==="" ||$scope.cnmt.cnmtKm===0){
			console.log("************************fill state and km");
		}else{
			$scope.addRateForCnmtByK($scope.cnmt.cnmtKm,$scope.cnmt.cnmtState,$scope.cnmt.contractCode);
		}
	}
	
	$scope.saveEmployeeCode =  function(employee){
		$scope.cnmt.cnmtEmpCode = employee.empCode;
		$('div#employeeCodeDB').dialog('close');
		$scope.EmployeeCodeDBFlag=true;
	}
	
	$scope.savePayAtBranchCode = function(payat){
		$scope.cnmt.cnmtPayAt = payat.branchCode;
		$('div#CNMTPayAtDB').dialog('close');
		$scope.CNMTPayAtDBFlag=true;
	}
	
	$scope.saveBillAtBranchCode = function(billat){
		$scope.cnmt.cnmtBillAt = billat.branchCode;
		$('div#CNMTBillAtDB').dialog('close');
		$scope.CNMTBillAtDBFlag=true;
	}
	
	$scope.saveContractCode = function(contract){
		console.log("enter into saveContractCode function = "+contract.contType);
		$scope.contract = contract;
		if($scope.cnmt.contractCode === contract.contCode){
			$scope.alertToast("nothing to be done now");	
			$('div#contractCodeDB').dialog('close');
			$scope.ContractCodeDBFlag=true;
		}else{
			$scope.cnmt.contractCode = contract.contCode;
			$scope.cnmtDDL     = contract.cnmtDDL;
			$scope.cnmtCostGrade = contract.cnmtCostGrade;
			$scope.cnmt.cnmtGuaranteeWt = contract.toWeight;
			$scope.garunteeWt = contract.toWeight;
			$scope.cnmt.cnmtRate="";
			$scope.cnmt.cnmtActualWt="";
			$scope.cnmt.cnmtGuaranteeWt="";
			$scope.cnmt.cnmtFreight="";
			$scope.cnmt.cnmtRate="";
			$scope.cnmt.cnmtTOT="";
			$scope.cnmt.cnmtVehicleType="";
			$scope.cnmt.cnmtNoOfPkg="";
			$scope.cnmt.cnmtToSt="";
			$scope.cnmt.cnmtProductType="";
			$('div#contractCodeDB').dialog('close');
			$scope.ContractCodeDBFlag=true;
			if($scope.cnmt.cnmtToSt === "" || $scope.cnmt.cnmtToSt === null || angular.isUndefined($scope.cnmt.cnmtToSt)){
				$scope.alertToast("Please enter ToStation");	
			}else{
				console.log("no need of ToStation");
			}
			if(contract.cnmtDC === "01"){
				$('#cnmtBillAt').removeAttr("disabled");
				$scope.cnmtDC = "1 bill";
			}else if(contract.cnmtDC === "02"){
				$('#cnmtBillAt').removeAttr("disabled");
				$scope.cnmtDC = "2 bill";
			}else if(contract.cnmtDC === "10"){
				$('#cnmtPayAt').removeAttr("disabled");
				$scope.cnmtDC = "Direct payment through CNMT";
			}else if(contract.cnmtDC === "20"){
				$('#cnmtPayAt').removeAttr("disabled");
				$scope.cnmtDC = "Twice Payment";
			}else if(contract.cnmtDC === "11"){
				$('#cnmtBillAt').removeAttr("disabled");
				$scope.cnmtDC = "Partially through CNMT";
			}
			$scope.cnmt.cnmtDC      = contract.cnmtDC;
			if($scope.cnmt.cnmtPayAt === "" || $scope.cnmt.cnmtPayAt === null || angular.isUndefined($scope.cnmt.cnmtPayAt)){
				$('#cnmtPayAt').attr("disabled","disabled");	
			}else{
				$('#cnmtPayAt').removeAttr("disabled");
			}

			if($scope.cnmt.cnmtBillAt === "" || $scope.cnmt.cnmtBillAt === null || angular.isUndefined($scope.cnmt.cnmtBillAt)){
				$('#cnmtBillAt').attr("disabled","disabled");
			}else{
				$('#cnmtBillAt').removeAttr("disabled");
			}

			if($scope.cnmt.cnmtVehicleType === "rail"){
				$('#cnmtKm').removeAttr("disabled");
				$('#cnmtState').removeAttr("disabled");
			}
			if(contract.contType === "q" || contract.contType === "Q"){
				$('#cnmtKmId').attr("disabled","disabled");
				$('#cnmtStateId').attr("disabled","disabled");
				$scope.K_Type = false;

			}else if(contract.contType === "w" || contract.contType === "W"){
				$('#cnmtKmId').attr("disabled","disabled");
				$('#cnmtStateId').attr("disabled","disabled");
				$scope.K_Type = false;

			}else if(contract.contType === "k" || contract.contType === "K"){
				$('#cnmtKmId').removeAttr("disabled");
				$('#cnmtStateId').removeAttr("disabled");
				//$scope.cnmt.cnmtToSt = contract.toStation;
				$scope.K_Type = true;
				console.log("oyeee pagal--------------------->>>>>"+contract.toStation);
				$scope.autoToStnFlag = true;
				$scope.saveToStnCode(contract.toStation);	
			}

		}
	}
	
	
	$scope.saveConsigneeCustomerCode = function(consignee){
		$scope.cnmt.cnmtConsignee = consignee.custCode;
		$('div#CNMTConsigneeDB').dialog('close');
		$scope.CNMTConsigneeDBFlag=true;
	}
	
	$scope.saveConsignorCustomerCode = function(consignor){
		$scope.cnmt.cnmtConsignor = consignor.custCode;
		$('div#CNMTConsignorDB').dialog('close');
		$scope.CNMTConsignorDBFlag=true;
	}
	
	
	$scope.saveCustomerCode = function(customer){
		console.log("hiiiiiiiiiiiiiiii");
		$scope.Cflag=true;
		$scope.Fflag=true;
		if($scope.cnmt.custCode === customer.custCode){
			$scope.alertToast("nothing to be done now");	
			$('div#CustomerCodeDB').dialog('close');
			$scope.CustomerCodeDBFlag=true;
		}else{
			$scope.alertToast("please check customer code,date and from station are filled");
			$('#cnmtPayAt').attr("disabled","disabled");
			$('#cnmtBillAt').attr("disabled","disabled");
			$scope.cnmt.custCode = customer.custCode;
			$scope.cnmt.cnmtRate="";
			$scope.cnmt.cnmtDt="";
			$scope.cnmt.cnmtConsignor="";
			$scope.cnmt.cnmtConsignee="";
			$scope.cnmt.contractCode="";
			$scope.cnmt.cnmtActualWt="";
			$scope.cnmt.cnmtGuaranteeWt="";
			$scope.cnmtDC="";
			$scope.cnmt.cnmtNoOfPkg="";
			$scope.cnmt.cnmtFromSt="";
			$scope.cnmt.cnmtToSt="";
			$scope.cnmt.cnmtProductType="";
			$scope.cnmt.cnmtTOT="";
			$scope.cnmtDDL="";
			$scope.cnmt.cnmtPayAt="";
			$scope.cnmt.cnmtBillAt="";
			$scope.cnmt.cnmtFreight="";
			$scope.cnmt.cnmtVOG="";
			$scope.cnmt.cnmtExtraExp="";
			$scope.cnmtCostGrade="";
			$scope.cnmt.cnmtDtOfDly="";
			$scope.cnmt.cnmtEmpCode="";
			$scope.cnmt.cnmtKm="";
			$scope.cnmt.cnmtVehicleType="";
			$scope.cnmt.cnmtState="";
			$('div#CustomerCodeDB').dialog('close');
			$scope.CustomerCodeDBFlag=true;
			if(angular.isUndefined($scope.cnmt.custCode) || $scope.cnmt.custCode === null || $scope.cnmt.custCode === ""||
				angular.isUndefined($scope.cnmt.cnmtDt)  || $scope.cnmt.cnmtDt === null || $scope.cnmt.cnmtDt === ""||
				angular.isUndefined($scope.cnmt.cnmtFromSt)  || $scope.cnmt.cnmtFromSt === null || $scope.cnmt.cnmtFromSt === ""){
				$scope.alertToast("Enter customer code,date and from station");
				$('#contractCode1').attr("disabled","disabled");
			}else{
				//fire database call here and enable contract code on success
				var longTime = $scope.cnmt.cnmtDt;
				var date = $filter('date')(longTime, 'yyyy-MM-dd hh:mm:ss');
				var group = {
						"custCode" :  $scope.cnmt.custCode,
						"cnmtDt"	  : date,
						"cnmtFromSt" : $scope.cnmt.cnmtFromSt
				}
				var response = $http.post($scope.projectName+'/getContractDataForView',group);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
						$scope.contractList = data.list;
						$scope.alertToast("relevant contract exists enter correct contract code,date and from station");
						$('#contractCode1').removeAttr("disabled");
					}else{
						//$scope.alertToast("relevant contract does not exists");
						console.log("msg from server----->"+data.result);
						$scope.cnmt.contractCode="";
						$('#contractCode1').attr("disabled","disabled");
						$scope.cnmtDC = "";
						$scope.cnmtDDL = "";
						$scope.cnmtCostGrade = "";
					}

					console.log("----->from server = "+$scope.contractList);
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}
	}
	
	
	$scope.saveCnmtCode = function(cnmt){
		$scope.cnmt.cnmtCode = cnmt.brsLeafDetSNo;
		$('div#cnmtCodeDB').dialog('close');
		$scope.CnmtCodeDBFlag=true;
	}

	$scope.saveBranchCode = function(branch){
		$scope.cnmt.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}

	$scope.saveCnmt = function(value){
		$scope.cnmtCode = value;
		$('div#cnmtDB').dialog('close');
		$scope.CnmtDBFlag=true;
	}
	
	
	$scope.saveToStnCode = function(station){
		console.log("enter into saveToStnCode----->"+station.stnCode);
		console.log("---------->$scope.contract.contCode = "+$scope.contract.contCode);
		if($scope.cnmt.cnmtToSt === station.stnCode){
			$scope.alertToast("nothing to be done now");	
			$('div#cnmtToStationDB').dialog('close');
			$scope.CnmtToStationDBFlag=true;
		}else{
			if($scope.cnmt.custCode === "" || $scope.cnmt.custCode === null || angular.isUndefined($scope.cnmt.custCode)||
					$scope.cnmt.cnmtDt === "" || $scope.cnmt.cnmtDt === null || angular.isUndefined($scope.cnmt.cnmtDt) ||
					$scope.cnmt.cnmtFromSt === "" || $scope.cnmt.cnmtFromSt === null || angular.isUndefined($scope.cnmt.cnmtFromSt) ||
					$scope.cnmt.contractCode === "" || $scope.cnmt.contractCode === null || angular.isUndefined($scope.cnmt.contractCode)){
				$scope.alertToast("Enter custCode,date,from station and contract code");
			}else{
				var group = {
						"cnmtToSt" : station.stnCode,
						"contractCode" : $scope.cnmt.contractCode
				}
				var response = $http.post($scope.projectName+'/getContForView',group);
				response.success(function(data, status, headers, config) {
					console.log("msg from server-->"+data.result);
					if(data.result === "success"){
						$scope.cnmt.cnmtToSt = station.stnCode;
						console.log("value is-->"+$scope.cnmt.cnmtToSt);
						$scope.cnmt.cnmtRate="";
						$scope.cnmt.cnmtActualWt="";
						$scope.cnmt.cnmtGuaranteeWt="";
						$scope.cnmt.cnmtFreight="";
						$scope.cnmt.cnmtRate="";
						$scope.cnmt.cnmtTOT="";
						$scope.cnmt.cnmtVehicleType="";
						$scope.cnmt.cnmtNoOfPkg="";
						$scope.cnmt.cnmtProductType="";
						if($scope.autoToStnFlag == true){
							console.log(" automatically to stn fill");
						}else{
							$('div#cnmtToStationDB').dialog('close');
						}
						$scope.autoToStnFlag == false;
						console.log("^^^^^^^^^^^^^^^^"+$scope.contract.contType);
						if($scope.contract.contType === "w" || $scope.contract.contType === "W"){
							console.log("enter into w view");
							$scope.autoToStnFlag == true;
							if(angular.isUndefined($scope.cnmt.cnmtToSt) || angular.isUndefined($scope.cnmt.cnmtVehicleType)|| $scope.cnmt.cnmtVehicleType===null || $scope.cnmt.cnmtVehicleType===""){
								$scope.alertToast("please vehicle type");
							}else{
								$scope.addRateForCnmtByW($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtVehicleType,$scope.cnmt.contractCode);
							}
						}else if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
							console.log("enter into q view");
							$scope.autoToStnFlag == true;
							if(angular.isUndefined($scope.cnmt.cnmtToSt) || $scope.cnmt.cnmtProductType===null || angular.isUndefined($scope.cnmt.cnmtProductType) || $scope.cnmt.cnmtProductType===""){
								$scope.alertToast("please fill product type");
							}else{
								$scope.addRateForCnmtByQ($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtProductType,$scope.cnmt.contractCode);
							}
						}else if($scope.contract.contType === "k" || $scope.contract.contType === "K"){
							console.log("enter into k view");
							if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState===null ||$scope.cnmt.cnmtState==="" || angular.isUndefined($scope.cnmt.cnmtKm) || $scope.cnmt.cnmtKm===null ||$scope.cnmt.cnmtKm===""){
								$('#cnmtKm').removeAttr("disabled");
								$('#cnmtState').removeAttr("disabled");
								$scope.alertToast("fill state and km");
							}
						}else{
							console.log("try again");
						}
					}else{
						$scope.cnmt.cnmtToSt = "";
						$scope.alertToast("invalid to station");	
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
				$('div#cnmtToStationDB').dialog('close');	
			}
		}
	}
	
	

	$scope.saveVehicleType = function(vt){
		$scope.cnmt.cnmtVehicleType = vt.vtCode;
		$('div#vehicleTypeDB').dialog('close');
		$scope.VehicleTypeDBFlag=true;
		if($scope.contract.contType === "w" || $scope.contract.contType === "W"){
			if(angular.isUndefined($scope.cnmt.cnmtToSt)|| $scope.cnmt.cnmtState === null || $scope.cnmt.cnmtState === ""||
				angular.isUndefined($scope.cnmt.cnmtVehicleType) || $scope.cnmt.cnmtVehicleType === null || $scope.cnmt.cnmtVehicleType === "" ){
				console.log("######################fill station and vehicle type");
			}else{
				$scope.addRateForCnmtByW($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtVehicleType,$scope.cnmt.contractCode);
			}
		}else{
			console.log("no need");
		}
	}


	$scope.savProductName = function(pt){
		$scope.cnmt.cnmtProductType = pt;
		$('div#cnmtProductTypeDB').dialog('close');
		$scope.ProductTypeFlag=true;
		if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState === null || $scope.cnmt.cnmtState === ""|| 
			 angular.isUndefined($scope.cnmt.cnmtProductType) || $scope.cnmt.cnmtProductType === null || $scope.cnmt.cnmtProductType === ""){
				console.log("************************fill station and product type");
			}else{
				$scope.addRateForCnmtByQ($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtProductType,$scope.cnmt.contractCode);
			}
		}else{
			console.log("no need again");
		}	
	}
	
	
	$scope.saveFrmStnCode = function(station){
		if($scope.cnmt.cnmtFromSt === station.stnCode){
			$scope.alertToast("nothing to be done now");	
			$('div#cnmtFromStationDB').dialog('close');
			$scope.CnmtFromStationDBFlag=true;
		}else{
			$scope.alertToast("please check customer code,date and from station are filled");
			$('#cnmtPayAt').attr("disabled","disabled");
			$('#cnmtBillAt').attr("disabled","disabled");
			$scope.cnmt.cnmtFromSt = station.stnCode;
			//$scope.cnmt.custCode = "";
			$scope.cnmt.cnmtRate="";
			//$scope.cnmt.cnmtDt="";
			$scope.cnmt.cnmtConsignor="";
			$scope.cnmt.cnmtConsignee="";
			$scope.cnmt.contractCode="";
			$scope.cnmt.cnmtActualWt="";
			$scope.cnmt.cnmtGuaranteeWt="";
			$scope.cnmtDC="";
			$scope.cnmt.cnmtNoOfPkg="";
			//$scope.cnmt.cnmtFromSt="";
			$scope.cnmt.cnmtToSt="";
			$scope.cnmt.cnmtProductType="";
			$scope.cnmt.cnmtTOT="";
			$scope.cnmtDDL="";
			$scope.cnmt.cnmtPayAt="";
			$scope.cnmt.cnmtBillAt="";
			$scope.cnmt.cnmtFreight="";
			$scope.cnmt.cnmtVOG="";
			$scope.cnmt.cnmtExtraExp="";
			$scope.cnmtCostGrade="";
			$scope.cnmt.cnmtDtOfDly="";
			$scope.cnmt.cnmtEmpCode="";
			$scope.cnmt.cnmtKm="";
			$scope.cnmt.cnmtVehicleType="";
			$scope.cnmt.cnmtState="";
			$('div#cnmtFromStationDB').dialog('close');
			$scope.CnmtFromStationDBFlag=true;
			if(angular.isUndefined($scope.cnmt.custCode) || $scope.cnmt.custCode === null || $scope.cnmt.custCode === ""||
				angular.isUndefined($scope.cnmt.cnmtDt)  || $scope.cnmt.cnmtDt === null || $scope.cnmt.cnmtDt === ""||
				angular.isUndefined($scope.cnmt.cnmtFromSt)  || $scope.cnmt.cnmtFromSt === null || $scope.cnmt.cnmtFromSt === ""){
				$scope.alertToast("Enter customer code,date and from station");
				$('#contractCode1').attr("disabled","disabled");
			}else{
				var longTime = $scope.cnmt.cnmtDt;
				var date = $filter('date')(longTime, 'yyyy-MM-dd hh:mm:ss');
				var group = {
						"custCode" :  $scope.cnmt.custCode,
						"cnmtDt"	  : date,
						"cnmtFromSt" : $scope.cnmt.cnmtFromSt
				}
				var response = $http.post($scope.projectName+'/getContractDataForView',group);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
						$scope.contractList = data.list;
						$scope.alertToast("relevant contract exists enter correct contract code,date and from station");
						$('#contractCode1').removeAttr("disabled");
					}else{
						$scope.alertToast("relevant contract exists enter correct contract code,date and from station");
						console.log("msg from server----->"+data.result);
						$scope.cnmt.contractCode="";
						$('#contractCode1').attr("disabled","disabled");
						$scope.cnmtDC = "";
						$scope.cnmtDDL = "";
						$scope.cnmtCostGrade = "";
					}		
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}
	}
	
	
	
	$scope.dateSelect = function(cnmtDate){
		console.log("enter into dateSelect function");
		if(angular.isUndefined($scope.cnmt.custCode) || $scope.cnmt.custCode === null || $scope.cnmt.custCode === ""||
			angular.isUndefined($scope.cnmt.cnmtDt)  || $scope.cnmt.cnmtDt === null || $scope.cnmt.cnmtDt === ""||
			angular.isUndefined($scope.cnmt.cnmtFromSt)  || $scope.cnmt.cnmtFromSt === null || $scope.cnmt.cnmtFromSt === ""){
			$scope.alertToast("Enter customer code,date and from station");
			$('#contractCode1').attr("disabled","disabled");
		}else{

			$scope.alertToast("please check customer code,date and from station are filled");
			$('#cnmtPayAt').attr("disabled","disabled");
			$('#cnmtBillAt').attr("disabled","disabled");
			//$scope.cnmt.custCode ="";
			$scope.cnmt.cnmtRate="";
			//$scope.cnmt.cnmtDt="";
			$scope.cnmt.cnmtConsignor="";
			$scope.cnmt.cnmtConsignee="";
			//$scope.cnmt.contractCode="";
			$scope.cnmt.cnmtActualWt="";
			$scope.cnmt.cnmtGuaranteeWt="";
			//$scope.cnmtDC="";
			$scope.cnmt.cnmtNoOfPkg="";
			//$scope.cnmt.cnmtFromSt="";
			$scope.cnmt.cnmtToSt="";
			$scope.cnmt.cnmtProductType="";
			$scope.cnmt.cnmtTOT="";
			//$scope.cnmtDDL="";
			$scope.cnmt.cnmtPayAt="";
			$scope.cnmt.cnmtBillAt="";
			$scope.cnmt.cnmtFreight="";
			$scope.cnmt.cnmtVOG="";
			$scope.cnmt.cnmtExtraExp="";
			//$scope.cnmtCostGrade="";
			$scope.cnmt.cnmtDtOfDly="";
			$scope.cnmt.cnmtEmpCode="";
			$scope.cnmt.cnmtKm="";
			$scope.cnmt.cnmtVehicleType="";
			$scope.cnmt.cnmtState="";

			var longTime = $scope.cnmt.cnmtDt;
			var date = $filter('date')(longTime, 'yyyy-MM-dd hh:mm:ss');
			$('#contractCode1').removeAttr("disabled");
			var group = {
					"custCode" : $scope.cnmt.custCode,
					"cnmtDt"	  : date,
					"cnmtFromSt" : $scope.cnmt.cnmtFromSt
			}
			var response = $http.post($scope.projectName+'/getContractDataForView',group);
			response.success(function(data, status, headers, config) {

				if(data.result === "success"){
					$scope.contractList = data.list;
					$scope.alertToast("relevant contract exists enter correct contract code,date and from station");
					$('#contractCode1').removeAttr("disabled");
				}else{
					//$scope.alertToast("relevant contract does not exists");
					console.log("msg from server----->"+data.result);
					$scope.cnmt.contractCode="";
					$('#contractCode1').attr("disabled","disabled");
					$scope.cnmtDC = "";
					$scope.cnmtDDL = "";
					$scope.cnmtCostGrade = "";
				}
				console.log("----->from server = "+$scope.contractList);
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});

		}
	}
	
	$scope.getCnmtList = function(){
		console.log(" entered into getCnmtList------>");		
		var response = $http.post($scope.projectName+'/getCnmtList', $scope.cnmtCode);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if(data.result === "success"){
				$scope.cnmtList = data.list;
				$scope.getBranchData();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForAR');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
				$scope.getCnmtCodeList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}
	 
	$scope.getCnmtCodeList = function(){
		console.log(" entered into getCnmtCodeList------>");
		var response = $http.post($scope.projectName+'/getCnmtCodeListForView');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.cnmtCodeList = data.list;
				$scope.getCustomerList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	 
	 
	$scope.getCustomerList = function(){
		console.log("getCustomerData------>shikha");
		var response = $http.get($scope.projectName+'/getCustomerListForView');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.customerList = data.list;
				$scope.getEmployeeList();
			}else{
				$scope.alertToast("you don't have any cutomer");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	 
	 
	$scope.getEmployeeList = function(){
		console.log(" entered into getListOfEmployee------>");
		var response = $http.post($scope.projectName+'/getListOfEmployeeForView');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.employeeList = data.list;
				$scope.getStateList();
			}else{
				$scope.alertToast("you don't have any employee");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	 
	$scope.getStateList = function(){
		console.log("getStateCodeData------>");
		var response = $http.post($scope.projectName+'/getStateCodeDataForView');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stateList = data.list;
				$scope.getStationData();
				console.log("data from server-->"+$scope.stateList);
			}else{
				$scope.alertToast("you don't have any state");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getStationData = function(){
		console.log("getStationData------>");
		var response = $http.post($scope.projectName+'/getStationDataForChallan');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stationList = data.list;
				$scope.getProductName();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.getProductName = function(){
		console.log("getProductName------>");
		var response = $http.post($scope.projectName+'/getProductNameForRegCont');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.ptList = data.list;
				$scope.getVehicleTypeCode();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getVehicleTypeCode = function(){
		console.log("getVehicleTypeCode------>");
		var response = $http.post($scope.projectName+'/getVehicleTypeCodeForChallan');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vtList = data.list;
			}else{
				$scope.alertToast("you don't have any vehicle type");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	$scope.viewCnmt = function(cnmtCode){
		console.log("enter into viewCnmt------>");
		$scope.code=$('#cnmtCode').val();
		if($scope.code === ""){
			$scope.alertToast("Please enter cnmt code....");
		}else{
			$scope.showCnmtDetailsFlag = false;
			var response = $http.post($scope.projectName+'/viewcnmt',cnmtCode);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					console.log(data);
					$scope.cnmt = data.cnmt;
					
					//put challan here
					$scope.chlnNSedrList = data.chlnNSedrByCnmtList;
					console.log("size: "+$scope.chlnNSedrList.length);
					for ( var i = 0; i < $scope.chlnNSedrList.length; i++) {
						console.log("chlnCode: "+$scope.chlnNSedrList[i].chlnCode);
						console.log("chlnCode: "+$scope.chlnNSedrList[i].arCode);
					}
										
					var num =$scope.cnmt.cnmtActualWt/$scope.kgInTon;
					$scope.cnmt.cnmtActualWt = Math.round(num*100000000)/100000000;
					//$scope.cnmt.cnmtActualWt = $scope.cnmt.cnmtActualWt/$scope.kgInTon;
					$scope.actWt = $scope.cnmt.cnmtActualWt;
					console.log("msg from server---$scope.cnmt.cnmtRate-->"+$scope.cnmt.cnmtRate);
					console.log("####################"+$scope.cnmt.cnmtGuaranteeWt);
					var numb = $scope.cnmt.cnmtGuaranteeWt/$scope.kgInTon;
					$scope.cnmt.cnmtGuaranteeWt = Math.round(numb*100000000)/100000000;
					//$scope.cnmt.cnmtGuaranteeWt = $scope.cnmt.cnmtGuaranteeWt/$scope.kgInTon;
					console.log("form server **************** "+$scope.cnmt.cnmtGuaranteeWt);

					//var sub = $scope.cnmt.getContractCode().substring(0,3);
					var contractCode = $scope.cnmt.contractCode;
					var subcontractCode=contractCode.substring(0,3);
					console.log("string is----->"+subcontractCode);
					if(subcontractCode === "dly"){
						var response = $http.post($scope.projectName+'/getDailyContractDataForView',contractCode);
						response.success(function(data, status, headers, config) {
							if(data.result === "success"){
								$scope.contract.contType  = data.contract.dlyContType;
								$scope.contract.proportionate  = data.contract.dlyContProportionate;
								//$scope.contract.additionalRate  = data.contract.dlyContAdditionalRate;
								$scope.cnmtDC          = data.contract.dlyContDc;
								$scope.cnmtDDL         = data.contract.dlyContDdl;
								//$scope.cnmt.cnmtRate        = data.contract.dlyContRate;
								$scope.actualCnmtRate  = $scope.cnmt.cnmtRate;
								$scope.cnmtCostGrade   = data.contract.dlyContCostGrade;
								//$scope.cnmtVehicleType = data.contract.dlyContVehicleType;
								$scope.actualGarWt     = $scope.cnmtGuaranteeWt;

								console.log("outside getAllContForViewCnmt function--->"+$scope.cnmt.custCode);
								var response = $http.post($scope.projectName+'/getAllContForViewCnmt',$scope.cnmt.custCode);
								response.success(function(data, status, headers, config) {
									$scope.contractList = data.result;
									console.log("here are all contracts--------------->"+$scope.contractList);
								});
								response.error(function(data, status, headers, config) {
									$scope.errorToast(data);
								});

							}else{
								console.log("msg from server ----> "+data.result);
							}					
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data);
						});
					}
					else if(subcontractCode === "reg"){
						var response = $http.post($scope.projectName+'/getRegularContractDataForView',contractCode);
						response.success(function(data, status, headers, config) {
							if(data.result === "success"){
								$scope.contract.contType  = data.contract.regContType;
								$scope.contract.proportionate = data.contract.regContProportionate;
								//$scope.contract.additionalRate  = data.contract.regContAdditionalRate;
								$scope.cnmtDC          = data.contract.regContDc;
								$scope.cnmtDDL         = data.contract.regContDdl;
								//$scope.cnmt.cnmtRate        = data.contract.regContRate;
								$scope.actualCnmtRate  = $scope.cnmt.cnmtRate;
								$scope.cnmtCostGrade   = data.contract.regContCostGrade;
								//$scope.cnmtVehicleType = data.contract.regContVehicleType;
								$scope.actualGarWt     = $scope.cnmtGuaranteeWt;

								console.log("outside getAllContForViewCnmt function--->"+$scope.cnmt.custCode);
								var response = $http.post($scope.projectName+'/getAllContForViewCnmt',$scope.cnmt.custCode);
								response.success(function(data, status, headers, config) {
									$scope.contractList = data.result;
									console.log("here are all contracts--------------->"+$scope.contractList.length);
								});
								response.error(function(data, status, headers, config) {
									$scope.errorToast(data);
								});

							}else{
								console.log("msg from server ----> "+data.result);
							}	
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data);
						});	
					}else{
						console.log("invalid contract code");
					}
					console.log(" ^^^^^^^^^^^^^^ $scope.cnmt.cnmtRate = "+$scope.cnmt.cnmtRate);
					//$scope.cnmt.cnmtRate = $scope.cnmt.cnmtRate;
					if($scope.cnmt.cnmtPayAt === "" || $scope.cnmt.cnmtPayAt === null || angular.isUndefined($scope.cnmt.cnmtPayAt)){
						$('#cnmtPayAt').attr("disabled","disabled");
					}else{
						$('#cnmtPayAt').removeAttr("disabled");
					}


					if($scope.cnmt.cnmtBillAt === "" || $scope.cnmt.cnmtBillAt === null || angular.isUndefined($scope.cnmt.cnmtBillAt)){
						$('#cnmtBillAt').attr("disabled","disabled");
					}else{
						$('#cnmtBillAt').removeAttr("disabled");
					}

					if($scope.cnmt.cnmtKm === "" || $scope.cnmt.cnmtKm === null || angular.isUndefined($scope.cnmt.cnmtKm)|| $scope.cnmt.cnmtKm === 0){
						$scope.cnmt.cnmtKm = "";
						$('#cnmtKm').attr("disabled","disabled");
					}else{
						$('#cnmtKm').removeAttr("disabled");
					}

					if($scope.cnmt.cnmtState === "" || $scope.cnmt.cnmtState === null || angular.isUndefined($scope.cnmt.cnmtState)){
						$('#cnmtState').attr("disabled","disabled");
					}else{
						$('#cnmtState').removeAttr("disabled");
					}
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config){
				$scope.errorToast(data);
			});
		}	
	}

	$scope.chngRate = function(type){
		console.log("enter into chngRate function--->"+type);
		if(type === "Ton"){
			$scope.cnmt.cnmtRate = $scope.actualCnmtRate * 1000;
		}else if(type === "Kg"){
			$scope.cnmt.cnmtRate = $scope.actualCnmtRate;
		}
	}
	
	
	$scope.calculateFreight = function(){
		console.log("enter intp calculateFreight function");
		console.log("---------->$scope.contract.contType = "+$scope.contract.contType);
		if($scope.contract.contType === "k" || $scope.contract.contType === "K"){
			var LryRate = 0;
			var ActWt = 0;
			if($scope.chlnLryRatePer === "Ton"){
				if($scope.cnmtGuaranteeWtPer === "Ton"){
					$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate*$scope.cnmt.cnmtGuaranteeWt;	
				}else if($scope.cnmtGuaranteeWtPer === "Kg"){
					LryRate = $scope.cnmt.cnmtRate/1000;
					$scope.cnmt.cnmtFreight = LryRate * $scope.cnmt.cnmtGuaranteeWt;
				}
			}else if($scope.chlnLryRatePer === "Kg"){
				if($scope.cnmtGuaranteeWtPer === "Ton"){
					LryRate = $scope.cnmt.cnmtRate * 1000;
					$scope.cnmt.cnmtFreight = LryRate*$scope.cnmt.cnmtGuaranteeWt;
				}else if($scope.cnmtGuaranteeWtPer === "Kg"){
					$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate * $scope.cnmt.cnmtGuaranteeWt;
				}
			}
		}else if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			$('#chlnLryRate1').attr("disabled","disabled");
			$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate * $scope.cnmt.cnmtNoOfPkg;

		}else if($scope.contract.contType === "w" || $scope.contract.contType === "W"){
			console.log("entered into w");
			if($scope.contract.proportionate==="p" || $scope.contract.proportionate==="P"){
				if($scope.cnmt.cnmtGuaranteeWt > $scope.garWt){
					$scope.alertToast("garuntee weight cant be greater than ToWeight of contract");
					$scope.cnmt.cnmtGuaranteeWt = $scope.garWt;
				}else{
					var LryRate = 0;
					var ActWt = 0;
					if($scope.chlnLryRatePer === "Ton"){
						if($scope.cnmtGuaranteeWtPer === "Ton"){
							$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate*$scope.cnmt.cnmtGuaranteeWt;	
						}else if($scope.cnmtGuaranteeWtPer === "Kg"){
							LryRate = $scope.cnmt.cnmtRate/1000;
							$scope.cnmt.cnmtFreight = LryRate * $scope.cnmt.cnmtGuaranteeWt;
						}
					}else if($scope.chlnLryRatePer === "Kg"){
						if($scope.cnmtGuaranteeWtPer === "Ton"){
							LryRate = $scope.cnmt.cnmtRate * 1000;
							$scope.cnmt.cnmtFreight = LryRate*$scope.cnmt.cnmtGuaranteeWt;
						}else if($scope.cnmtGuaranteeWtPer === "Kg"){
							$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate * $scope.cnmt.cnmtGuaranteeWt;
						}
					}

					// $scope.cnmt.cnmtFreight = $scope.cnmtRate;
				}  
			}else if($scope.contract.proportionate==="f" || $scope.contract.proportionate==="F"){
				console.log("value $scope.contract.proportionate = "+$scope.contract.proportionate);
				console.log("value of $scope.garWt = "+$scope.garWt);
				console.log("value of $scope.cnmt.cnmtGuaranteeWt = "+$scope.cnmt.cnmtGuaranteeWt);
				if($scope.cnmt.cnmtGuaranteeWt > $scope.garWt){
					console.log("helo m inside f function");
					var LryRate = 0;
					var ActWt = 0;
					var addRateMap = {
							"contCode" : $scope.cnmt.contractCode,
							"toStn"    : $scope.cnmt.cnmtToSt,
							"vType"    : $scope.cnmt.cnmtVehicleType
					};
					var response = $http.post($scope.projectName+'/getAddRateFrCnmt',addRateMap);
					response.success(function(data, status, headers, config){
						if(data.result === "success"){
							$scope.contract.additionalRate = data.aRate;
							$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate + (($scope.cnmt.cnmtGuaranteeWt - $scope.garWt) * $scope.contract.additionalRate );	
						}else{
							console.log(data.result);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});
				}else{
					console.log("helo m outside f function");
					$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate;
				}  
			}else{
				console.log("try again");
			}
		}
	}

	
	$scope.calculateTOT = function(){
		$scope.cnmt.cnmtTOT = $scope.cnmt.cnmtFreight;
	}

	$scope.chkPkg = function(){
		if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			$scope.alertToast("please enter No. Of Packages..");
		}
	}
	

	$scope.addRateForCnmtByK = function(cnmtKm,cnmtState,contractCode){
		console.log("enter into addRateForCnmtByK function");
		var rateKList = {
				"cnmtKm" : cnmtKm,
				"cnmtState"	  : cnmtState,
				"contractCode" : contractCode
		}
		var response = $http.post($scope.projectName+'/addRateForCnmtByKForView',rateKList);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.cnmt.cnmtRate = data.rate;
				$scope.actualCnmtRate = $scope.cnmt.cnmtRate;
				$scope.chlnLryRatePer = "Kg";
				//$scope.actualCnmtRate = data.rate;
				$scope.cnmt.cnmtVehicleType = data.vtCode;
			}else{
				console.log("msg from server-->"+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.addRateForCnmtByQ = function(cnmtToSt,cnmtProductType,contractCode){
		console.log("enter into addRateForCnmtByQ function");
		var rateQList = {
				"cnmtToSt" : cnmtToSt,
				"cnmtProductType"	  : cnmtProductType,
				"contractCode" : contractCode
		}
		var response = $http.post($scope.projectName+'/addRateForCnmtByQForView',rateQList);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.cnmt.cnmtRate = data.rate;
				$scope.actualCnmtRate = $scope.cnmt.cnmtRate;
				$scope.chlnLryRatePer = "";
				$('#chlnLryRate1').attr("disabled","disabled");
				$scope.cnmt.cnmtGuaranteeWt = data.garunteeWt/$scope.kgInTon;
				console.log("value is-->"+$scope.cnmt.cnmtRate);
				console.log("value is-->"+$scope.cnmt.cnmtGuaranteeWt);
			}else{
				console.log("msg from server-->"+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.addRateForCnmtByW = function(cnmtToSt,cnmtVehicleType,contractCode){
		console.log("enter into addRateForCnmtByW function---->"+cnmtVehicleType);
		console.log("enter into addRateForCnmtByW function--cnmtToSt-->"+cnmtToSt);
		console.log("enter into addRateForCnmtByW function--contractCode-->"+contractCode);
		var rateWList = {
				"cnmtToSt" : cnmtToSt,
				"cnmtVehicleType"	  : cnmtVehicleType,
				"contractCode" : contractCode
		}
		var response = $http.post($scope.projectName+'/addRateForCnmtByWForView',rateWList);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.cnmt.cnmtRate = data.rate;
				console.log("++++++++++++++++++++++++++++++++");
				$scope.actualCnmtRate = $scope.cnmt.cnmtRate;
				if($scope.contract.proportionate==="p" || $scope.contract.proportionate==="P"){
					console.log("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP");
					$scope.chlnLryRatePer = "Kg";  
				}else if($scope.contract.proportionate==="f" || $scope.contract.proportionate==="F"){
					console.log("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
					$scope.chlnLryRatePer = "";
					$('#chlnLryRate1').attr("disabled","disabled");
				}
				var num =data.garWt/$scope.kgInTon;
				$scope.cnmt.cnmtGuaranteeWt = Math.round(num*100000000)/100000000;
				//$scope.cnmt.cnmtGuaranteeWt = data.garWt/$scope.kgInTon;
				console.log("$scope.cnmt.cnmtGuaranteeWt"+$scope.cnmt.cnmtGuaranteeWt);
				$scope.garWt = data.garWt/$scope.kgInTon;
				//$scope.cnmt.cnmtGuaranteeWt = data.garunteeWt/$scope.kgInTon;
				console.log("value is-->"+$scope.cnmt.cnmtRate);
				//console.log("value is-->"+$scope.cnmt.cnmtGuaranteeWt);
			}else{
				console.log("msg from server-->"+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.chngActCnmtWt = function(wt){
		console.log("enter into chngActCnmtWt function");
		$scope.actWt = wt;
	}
	
	$scope.uploadCnmtImage = function(){
		console.log("enter into uploadCnmtImage function");
		var file = $scope.cnmtImage;
		console.log("size of file ----->"+file.size);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadCnmtImageV";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}
	
	
	$scope.uploadCnmtConfImage = function(){
		console.log("enter into uploadCnmtConfImage function");
		var file = $scope.cnmtConfirmImage;
		console.log("size of file ----->"+file.size);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadConfCnmtImageV";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}
	
	
	$scope.updateCnmts = function(CnmtForm){
		console.log("Entered into updateCnmt function--- ");
		if(CnmtForm.$invalid){
			if(CnmtForm.cnmtCode.$invalid){
				$scope.alertToast("please enter cnmt code..");
			}else if(CnmtForm.custCode.$invalid){
				$scope.alertToast("please enter customer code..");
			}else if(CnmtForm.cnmtFromSt.$invalid){
				$scope.alertToast("please enter from station..");
			}else if(CnmtForm.cnmtToSt.$invalid){
				$scope.alertToast("please enter to station..");
			}else if(CnmtForm.cnmtConsignor.$invalid){
				$scope.alertToast("please enter cnmt consignor..");
			}else if(CnmtForm.cnmtConsignee.$invalid){
				$scope.alertToast("Please enter cnmt consignee..");
			}else if(CnmtForm.contractCode.$invalid){
				$scope.alertToast("Please enter contract code..");
			}else if(CnmtForm.cnmtProductType.$invalid){
				$scope.alertToast("Please enter product type..");
			}else if(CnmtForm.cnmtNoOfPkg.$invalid){
				$scope.alertToast("please enter no. of package from 1-9..");
			}else if(CnmtForm.cnmtActualWt1.$invalid){
				$scope.alertToast("please Select Per Ton/kg for actual weight..");
			}else if(CnmtForm.cnmtGuaranteeWt1.$invalid){
				$scope.alertToast("please Select Per Ton/kg for guaruntee weight..");
			}else if(CnmtForm.chlnLryRate1.$invalid){
				$scope.alertToast("please Select Per Ton/kg for rate..");
			}else if(CnmtForm.cnmtVOG.$invalid){
				$scope.alertToast("Please enter value of goods..");
			}else if(CnmtForm.cnmtExtraExp.$invalid){
				$scope.alertToast("Please enter extra expenses..");
			}else if(CnmtForm.cnmtDtOfDly.$invalid){
				$scope.alertToast("Please enter correct date..");
			}else if(CnmtForm.cnmtEmpCode.$invalid){
				$scope.alertToast("Please enter employee code..");
			}else if(CnmtForm.cnmtInvoiceNo.$invalid){
				$scope.alertToast("Please enter invoice no..");
			}
			 }else{
				/* if(!(cnmt.custCode === null || angular.isUndefined(cnmt.custCode) || cnmt.custCode==="")){
					 if(cnmt.contractCode === null || angular.isUndefined(cnmt.contractCode) || cnmt.contractCode===""){
							$scope.alertToast("please fill contract code,bill at and Select Per Ton/kg for actual weight again..");
				 	  }else{
				 		 if($scope.K_Type === true){
				 		   if(angular.isUndefined(cnmt.cnmtKm) || cnmt.cnmtKm === "" || cnmt.cnmtKm === null){
					 			$scope.alertToast("please fill kilometer");
					 		 }else if(angular.isUndefined(cnmt.cnmtState) || cnmt.cnmtState === "" || cnmt.cnmtState === null){
					 			$scope.alertToast("please fill state");
					 		 }else{*/
		console.log("cnmt Image ---->"+$scope.cnmt.cnmtImage);
		
		//uncomment this to edit cnmt
		/*var response = $http.post($scope.projectName+'/editCnmt',$scope.cnmt);
		console.log("********************");
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				console.log("success");
				$scope.successToast(data.result);
				$scope.cnmt="";
				$scope.showCnmtDetailsFlag = true;
				$scope.cnmtCode="";
			}else{
				console.log(data);
			}		
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});*/
		
		
		/* }

				 		 }
				 	  }
				 } 
			 }*/
	}
}
	
	
	$scope.updateCnmtInv = function(index,invoiceNo){
		console.log("enter into updateCnmtInv function --->with index ="+index);
		$scope.cnmt.cnmtInvoiceNo[index] = invoiceNo;
	}


	$scope.removeCnmtInv = function(index){
		console.log("enter into removeCnmtInv function");
		$scope.cnmt.cnmtInvoiceNo.splice(index,1);
	}

	$scope.saveInvoiceNum = function(saveInvoiceNoForm,invoiceNo,invoiceDt){
		console.log("enter into saveInvoiceNum function");
		if(saveInvoiceNoForm.$invalid){
			if(saveInvoiceNoForm.invoiceNo.$invalid){
				$scope.alertToast("Invoice number should be between 3-10 digits");
			}else if(saveInvoiceNoForm.invoiceDt.$invalid){
				$scope.alertToast("Please enter invoice date..");
			}
		}else{
			$scope.saveInvoiceNoFlag=true;
			$('div#saveInvoiceNo').dialog('close')
			if($.inArray(invoiceNo , $scope.cnmt.cnmtInvoiceNo) !== -1){
				$scope.alertToast("Invoice Number already present");
				$scope.invoiceNo = "";
			}else {
				var invList = {
						"invoiceNo" : invoiceNo,
						"date"	  : invoiceDt
				}
				$scope.cnmt.cnmtInvoiceNo.push(invList);
				//$scope.cnmt.cnmtInvoiceNo.push(invoiceNo,invoiceDt);
				//$scope.cnmt.cnmtInvoiceNo.push(invoiceDt);
				$scope.invoiceDt = "";
			}
		}						
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getCnmtList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
	
}]);