package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

public class EmployeeDAOImpl implements EmployeeDAO {
	
	private SessionFactory sessionFactory;
	
	private Session session;
	
	private Transaction transaction;
	
	private static Logger logger = Logger.getLogger(EmployeeDAOImpl.class);
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	public EmployeeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
		
	@Transactional
	public int saveEmployeeToDB(Employee employee){
		int temp;
		employee.setIsTerminate("false");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try{
		     session = this.sessionFactory.openSession();
		     transaction =  session.beginTransaction();
		     
		 	List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_EMP);
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			employee.setfAParticular(fAParticular);
		     
			int empId = (Integer) session.save(employee); 
		     
			employee.setEmpCode(String.valueOf(empId));  
	    	int end = 100000+empId;
			String lastFiveChar = String.valueOf(end).substring(1,6);	 
			String empName = employee.getEmpName();
			String subempName=empName.substring(0,3);
			/*Date dateEmpDOAppointment=employee.getEmpDOAppointment();
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
	        String formattedDate = formatter.format(dateEmpDOAppointment);
	        String subempDOAppointment=formattedDate.substring(0,4); */
	        String empCodeTemp = subempName+lastFiveChar; 
		    
	        employee.setEmpCodeTemp(empCodeTemp); 
		    
	        int empTempId = empId+100000;
			String subEmpId = String.valueOf(empTempId).substring(1,6);
			String faCode = fapIdStr+subEmpId;
			System.out.println("faCode code is------------------"+faCode);
			//String pfNo = ConstantsValues.PF_NO+String.valueOf(empId);
			//employee.setEmpPFNo(pfNo);
			employee.setEmpFaCode(faCode);
			
			session.update(employee);
			
			FAMaster fAMaster = new FAMaster();
			fAMaster.setFaMfaCode(faCode);
			fAMaster.setFaMfaType(ConstantsValues.FAP_EMP);
			fAMaster.setFaMfaName(employee.getEmpName());
			fAMaster.setUserCode(currentUser.getUserCode());
			fAMaster.setbCode(currentUser.getUserBranchCode());
			
			session.save(fAMaster);
			
	        transaction.commit();
			temp = empId;
			session.flush();
		}catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}
		
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Employee> retrieveEmployee(String empCode){
		List<Employee> list = new ArrayList<Employee>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr=session.createCriteria(Employee.class);
			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE,empCode));
			 list =cr.setMaxResults(1).list();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
	    session.close();
		return list; 
	 }
		
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int updateEmployee(String employeeCode,String branchCode){
		 System.out.println("enter into updateEmployee of EmployeeDAOImpl--->"+employeeCode);
	     
		 int i,temp = 0;
		 //int temp = 0;
	   //  String newEmpCodeTmp = null;
	     
	     Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
	      
	     //String subBranchName = branchName.substring(0,3);
	 
	      List<Employee> list = new ArrayList<Employee>();
	      try{
			   session = this.sessionFactory.openSession();
			   transaction = session.beginTransaction();
			   Criteria cr=session.createCriteria(Employee.class);
			   cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE,employeeCode));
			   list =cr.list();
			   for(i=0;i<list.size();i++){
				  Employee employee = list.get(i);
				  employee.setBranchCode(branchCode);
				 // employee.setbCode(bcode);
				  /*String empCodeTemp = employee.getEmpCodeTemp();
				  String subEmpCodeTemp = empCodeTemp.substring(7,10);
				  newEmpCodeTmp = empCodeTemp.replace(subEmpCodeTemp, subBranchName);
				  employee.setEmpCodeTemp(newEmpCodeTmp);*/
				  employee.setCreationTS(calendar);
				  temp = 1;
				  session.update(employee); 
			   }
			   transaction.commit();
			   session.flush();
		  }catch (Exception e) {
			  e.printStackTrace();
			  temp = -1;
		  }
	      session.clear();
	      session.close();
		  return temp;
	}
		 
	 @Transactional
	 public long totalCount(){
		 long num = -1;
		 System.out.println("Entered into totalCount function");
		 try{
		     session = this.sessionFactory.openSession();
		     num = (Long) session.createCriteria(Employee.class).setProjection(Projections.rowCount()).uniqueResult();
		     System.out.print("the value of num is "+num);
		     session.flush();
		 } 
		 catch(Exception e){
	 		 e.printStackTrace();
	 	}
		 session.clear();
	 	session.close();
		return num;
		
	}
	 
	  @Transactional
	  @SuppressWarnings("unchecked")
	  public List<Employee> getLastEmployeeRow(){
		  List<Employee> last = new ArrayList<Employee>();
		  try{
			   session = this.sessionFactory.openSession();
			   last = session.createQuery("from Employee order by empId DESC ").setMaxResults(1).list();
			   System.out.println("EmployeeId is"+ last.get(0).getEmpId());
			   session.flush();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  session.clear();
		  session.close(); 
		  return last;
	  }
		 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<Employee> getAllEmployee(){
		 System.out.println("enter into getAllEmployee inside EmployeeDAOImpl");
		 List<Employee> employeeList = new ArrayList<Employee>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 employeeList = session.createCriteria(Employee.class).list();
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
			 e.printStackTrace();
		}
		 session.clear();
		session.close(); 
		return employeeList;
	}
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<Employee> getAllActiveEmployees(){
		 System.out.println("enter into getAllActiveEmployees inside EmployeeDAOImpl");
		 List<Employee> employeeList = new ArrayList<Employee>();
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  Criteria cr=session.createCriteria(Employee.class);
			  cr.add(Restrictions.eq(EmployeeCNTS.IS_TERMINATE, "false"));
			  employeeList =cr.list();
			  transaction.commit();
			  session.flush();
		}catch(Exception e){
			 e.printStackTrace();
		}
		 session.clear();
		session.close(); 
		return employeeList;
	}
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<Employee> getAllActiveEmployeesByName(String empName){
		 logger.info("Enter into getAllActiveEmployeeByName() : EmpName = "+empName);
		 System.out.println("enter into getAllActiveEmployeesByName inside EmployeeDAOImpl : EmpName = "+empName);
		 List<Employee> employeeList = new ArrayList<Employee>();
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  Criteria cr=session.createCriteria(Employee.class);
			  cr.add(Restrictions.eq(EmployeeCNTS.IS_TERMINATE,"false"));
			  cr.add(Restrictions.like(EmployeeCNTS.EMP_NAME, "%"+empName+"%"));
			  employeeList =cr.list();
			  transaction.commit();
			  session.flush();
		}catch(Exception e){
			 e.printStackTrace();
			 logger.error("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from getAllActiveEmployeeByName()");
		return employeeList;
	}
	 
	 	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<Employee> getAllEmployeeForAdmin(String branchCode){
		 List<Employee> list = new ArrayList<Employee>();
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(Employee.class);
				cr.add(Restrictions.eq(EmployeeCNTS.BRANCH_CODE,branchCode));
				//cr.add(Restrictions.eq(EmployeeCNTS.B_CODE,branchCode));
				cr.add(Restrictions.eq(EmployeeCNTS.IS_VIEW,false));
				list =cr.list();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return list;  
	}
	  
	 @SuppressWarnings("unchecked")
     @Transactional
     public int updateEmployeeisViewTrue(String code[]){
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
		 
         for(int i=0;i<code.length;i++){
        	 System.out.println("The values inside array is ----------"+code[i]);
         }
         try{
             session = this.sessionFactory.openSession();
             for(int i=0;i<code.length;i++){
                transaction = session.beginTransaction();
                Employee employee  = new Employee();
                List<Employee> list = new ArrayList<Employee>();
                Criteria cr=session.createCriteria(Employee.class);
                cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE,code[i]));
                list =cr.list();
                employee = list.get(0);
                employee.setView(true);
                employee.setCreationTS(calendar);
                session.update(employee);
                transaction.commit();
             }
             session.flush();
             temp = 1;
        }catch(Exception e){
             e.printStackTrace();
             temp = -1;
        }
         session.clear();
        session.close();
        return temp;
     }
	 
	 
     @Transactional
	 public int updateEmployeeToDB(Employee employee){
    	 System.out.println("enter into updateEmployeeToDB function");
		 int temp = 0;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
			try{
			     session = this.sessionFactory.openSession();
				 transaction =  session.beginTransaction();
				 //employee.setCreationTS(calendar);
				 
				 /*List<Employee> list = new ArrayList<Employee>();
	             Criteria cr=session.createCriteria(Employee.class);
	             cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
	             list =cr.list();
				 
	             Employee actEmp = list.get(0);
				 actEmp = employee;
	             session.merge(actEmp);*/
	             
				 session.saveOrUpdate(employee);
				 transaction.commit();
				 temp= 1;
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
				temp= -1;
			}
			session.clear();
			session.close();
			return temp;
	 }
     
    /* @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getEmployeeCodes(){
		 List<String> empCodeList = new ArrayList<String>();
		 try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(Employee.class);
			  ProjectionList projList = Projections.projectionList();
			  projList.add(Projections.property(EmployeeCNTS.EMP_CODE));
			  cr.setProjection(projList);
			  empCodeList = cr.list();
			  transaction.commit();
			  session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		 	session.close();
		    return empCodeList;
	}*/
     
     @Transactional
	 @SuppressWarnings("unchecked")
     public List<Employee> getAll_HB_Employee(String branchCode){
    	 System.out.println("enter into getAll_HB_Employee");
    	 List<Employee> empCodeList = new ArrayList<Employee>();
    	 try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(Employee.class);
			  cr.add(Restrictions.eq(EmployeeCNTS.BRANCH_CODE,branchCode));
			  empCodeList = cr.list();
			  transaction.commit();
			  session.flush();
		}catch(Exception e){
				e.printStackTrace();
		}
    	 session.clear();
		session.close();
    	return empCodeList;
     }
     
     @SuppressWarnings("unchecked")
	 @Transactional
	 public List<Employee> getAllEmpForAdmin(String branchCode){
    	 List<Employee> empCodeList = new ArrayList<Employee>();
		 try{
			  session = this.sessionFactory.openSession();
			  transaction = session.beginTransaction();
			  Criteria cr=session.createCriteria(Employee.class);
			  cr.add(Restrictions.eq(EmployeeCNTS.BRANCH_CODE,branchCode));
			  /*ProjectionList projList = Projections.projectionList();
			  projList.add(Projections.property(EmployeeCNTS.EMP_CODE));
			  cr.setProjection(projList);*/
			  empCodeList = cr.list();
			  transaction.commit();
			  session.flush();
		}catch(Exception e){
				e.printStackTrace();
		}
		 session.clear();
		session.close();
		return empCodeList;
	}
     
     @Transactional
	 @SuppressWarnings("unchecked")
     public List<Employee> getOldEmployee(int oldEmployeeId){
    	 List<Employee> emplist = new ArrayList<Employee>();
			try{
				 session = this.sessionFactory.openSession();
				 transaction =  session.beginTransaction();
				 Criteria cr=session.createCriteria(Employee.class);
				 cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID,oldEmployeeId));
				 emplist =cr.setMaxResults(1).list();
				 session.flush();
			}catch(Exception e){
				 e.printStackTrace();
			}
			session.clear();
			session.close();
			return emplist; 
    }
     
     @Transactional
	 @SuppressWarnings("unchecked")
     public List<Employee> empDataByEmpCodeTemp(String empCodeTemp){
    	 List<Employee> list = new ArrayList<Employee>();
 		try{
 			 session = this.sessionFactory.openSession();
 			 transaction =  session.beginTransaction();
 			 Criteria cr=session.createCriteria(Employee.class);
 			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE_TEMP,empCodeTemp));
 			 list =cr.setMaxResults(1).list();
 			 session.flush();
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		session.clear();
 	    session.close();
 		return list; 
     }
     
     
     @Transactional
	 @SuppressWarnings("unchecked")
     public List<Employee> getNewEmp(){
    	 System.out.println("enter into getNewEmp function");
    	 List<Employee> list = new ArrayList<Employee>();
    	 List<Employee> empList = new ArrayList<Employee>();
 		try{
 			 session = this.sessionFactory.openSession();
 			 transaction =  session.beginTransaction();
 			 Criteria cr=session.createCriteria(Employee.class);
 			 list =cr.list();
 			 
 			 for(int i=0;i<list.size();i++){
 				 if(list.get(i).getPrBranch() == null){
 					empList.add(list.get(i));
 				 }
 			 }
 			 
 			 session.flush();
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		session.clear();
 	    session.close();
 		return empList; 
     }
      
     
     @Transactional
	 @SuppressWarnings("unchecked")
     public int assignPrBranch(int empId , int branchId){
    	 System.out.println("enter into assignBranch function = "+empId);
    	 int temp = 0;
    	 List<Employee> empList = new ArrayList<Employee>();
    	 List<Branch> brList = new ArrayList<Branch>();
 		try{
 			 session = this.sessionFactory.openSession();
 			 transaction =  session.beginTransaction();
 			 Criteria cr=session.createCriteria(Employee.class);
 			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID,empId));
 			 empList =cr.list();
 			 System.out.println("size of empList = "+empList.size());
 			 Employee employee = empList.get(0);
 			 
 			 Branch empBr = employee.getPrBranch();
 			 if(empBr == null){
 				 cr=session.createCriteria(Branch.class);
 	 			 cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,branchId));
 	 			 brList = cr.list();
 	 			 Branch branch = brList.get(0);
 	 			 
 	 			 employee.setBranchCode(String.valueOf(branch.getBranchId()));
 	 			 employee.setPrBranch(branch);
 	 			 branch.getEmployeeList().add(employee);
 	 			 session.update(branch);
 			 }else{
 				 employee.setPrBranch(null);
 				 session.update(employee);
 				 
 				 for(int i=0;i<empBr.getEmployeeList().size();i++){
 					 if(employee.getEmpId() == empBr.getEmployeeList().get(i).getEmpId()){
 						empBr.getEmployeeList().remove(i);
 					 }
 				 }
 				 
 				 session.update(empBr);
 				 
 				 
 				 cr=session.createCriteria(Branch.class);
 	 			 cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,branchId));
 	 			 brList = cr.list();
 	 			 Branch branch = brList.get(0);
 	 			 
 	 			 employee.setPrBranch(branch);
 	 			 employee.setBranchCode(String.valueOf(branch.getBranchId()));
 	 			 session.update(employee);
 	 			 branch.getEmployeeList().add(employee);
 	 			 session.update(branch);
 			 }
 			
 			 transaction.commit();
 			 temp = 1;
 			 session.flush();
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		session.clear();
 	    session.close();
 		return temp; 
     }
 
     
     @Transactional
  	 @SuppressWarnings("unchecked")
     public String getEmpNameByEmpCode(String empCode){
    	 System.out.println("enter into getEmpNameByEmpCode function");
    	 List<Employee> empList = new ArrayList<>();
    	 String empName = "";
    	 try{
    		 session = this.sessionFactory.openSession();
 			 transaction =  session.beginTransaction();
 			 Criteria cr=session.createCriteria(Employee.class);
 			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE,empCode));
 			 empList = cr.list();
 			 if(!empList.isEmpty()){
 				 empName = empList.get(0).getEmpName();
 			 }
 			 session.flush();
    	 }catch(Exception e){
    		 e.printStackTrace();
    	 }
    	 session.clear();
    	 session.close();
    	 return empName;
     }
     
     @Transactional
  	 @SuppressWarnings("unchecked")
     public List<String> getAllEmpBrh(String empFaCode){
    	 System.out.println("enter into getAllEMpBrh function : empFaCode = "+empFaCode);
    	 logger.info("Enter into getAllEmpBrh() : empFaCode = "+empFaCode);
    	 List<String> brhList = new ArrayList<>();
    	 List<Employee> empList = new ArrayList<>();
    	 try{
    		 session = this.sessionFactory.openSession();
 			 transaction =  session.beginTransaction();
 			 Criteria cr=session.createCriteria(Employee.class);
 			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_FA_CODE, empFaCode));
 			 empList = cr.list();
 			 if(!empList.isEmpty()){
 				 Branch prBranch = empList.get(0).getPrBranch();
 				 
 				 if(prBranch == null)
 					 prBranch = (Branch) session.get(Branch.class, Integer.parseInt(empList.get(0).getbCode()));
 				 
 				 brhList.add(prBranch.getBranchFaCode());
 				 
 				 List<Branch> seBrhList = new ArrayList<>();
 				 seBrhList = empList.get(0).getSeBranch();
 				 if(!seBrhList.isEmpty()){
 					 for(int i=0;i<seBrhList.size();i++){
 						 brhList.add(seBrhList.get(i).getBranchFaCode());
 					 }
 				 }
 			 }else
 				 logger.info("Employee not found !");
 			 session.flush();
    	 }catch(Exception e){
    		 e.printStackTrace();
    	 }
    	 session.clear();
    	 session.close();
    	 return brhList;
     }
     
     
     
     @Transactional
  	 @SuppressWarnings("unchecked") 
     public Employee getCmpltEmp(String empCode){
    	 System.out.println("enter into getCmpltEmp function");
    	 List<Employee> empList = new ArrayList<>();
    	 Employee emp = new Employee();
    	 try{
    		 session = this.sessionFactory.openSession();
 			 transaction =  session.beginTransaction();
 			 Criteria cr=session.createCriteria(Employee.class);
 			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE,empCode));
 			 empList = cr.list();
 			 
 			 if(!empList.isEmpty()){
 				 	emp = empList.get(0);
 				 	Hibernate.initialize(emp.getSeBranch());
 				 	Hibernate.initialize(emp.getElectricityMstrList());
 				 	Hibernate.initialize(emp.getTelephoneMstr());
 				 	Hibernate.initialize(emp.getVehicleMstrList());
 				 	Hibernate.initialize(emp.getPhoneMstrList());
 				 	Hibernate.initialize(emp.getFtdList());
 			 }
 			 
 			 session.flush();
    	 }catch(Exception e){
    		 e.printStackTrace();
    		 emp = null;
    	 }
    	 session.clear();
    	 session.close();
    	 return emp;
     }
     
     

     @Transactional
  	 @SuppressWarnings("unchecked") 
     public int updateEmpPart(Employee employee){
    	System.out.println("enter into updateEmpPart function");
    	int res = 0;
    	List<Employee> empList = new ArrayList<>();
    	try{
    		 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr=session.createCriteria(Employee.class);
			 cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
			 empList = cr.list();
			 
			 if(!empList.isEmpty()){
				 Employee actEmp = empList.get(0);
				 
				 actEmp.setEmpName(employee.getEmpName());
				 actEmp.setEmpFatherName(employee.getEmpFatherName());
				 actEmp.setEmpSex(employee.getEmpSex());
				 actEmp.setEmpEduQuali(employee.getEmpEduQuali());
				 actEmp.setEmpDOB(employee.getEmpDOB());
				 actEmp.setEmpDOAppointment(employee.getEmpDOAppointment());
				 actEmp.setEmpDesignation(employee.getEmpDesignation());
				 actEmp.setEmpNominee(employee.getEmpNominee());
				 actEmp.setEmpAdd(employee.getEmpAdd());
				 actEmp.setEmpCity(employee.getEmpCity());
				 actEmp.setEmpState(employee.getEmpState());
				 actEmp.setEmpPin(employee.getEmpPin());
				 actEmp.setEmpPresentAdd(employee.getEmpPresentAdd());
				 actEmp.setEmpPresentCity(employee.getEmpPresentCity());
				 actEmp.setEmpPresentState(employee.getEmpPresentState());
				 actEmp.setEmpPresentPin(employee.getEmpPresentPin());
				 actEmp.setEmpBasic(employee.getEmpBasic());
				 actEmp.setEmpHRA(employee.getEmpHRA());
				 actEmp.setEmpOtherAllowance(employee.getEmpOtherAllowance());
				 actEmp.setEmpGross(employee.getEmpGross());
				 actEmp.setEmpTds(employee.getEmpTds());
				 actEmp.setEmpESI(employee.getEmpESI());
				 actEmp.setEmpESINo(employee.getEmpESINo());
				 actEmp.setEmpESIDispensary(employee.getEmpESIDispensary());
				 actEmp.setEmpPF(employee.getEmpPF());
				 actEmp.setEmpPFNo(employee.getEmpPFNo());
				 actEmp.setNetSalary(employee.getNetSalary());
				 actEmp.setEmpPFEmplr(employee.getEmpPFEmplr());
				 actEmp.setEmpESIEmplr(employee.getEmpESIEmplr());
				 actEmp.setEmpLoanBal(employee.getEmpLoanBal());
				 actEmp.setEmpLoanPayment(employee.getEmpLoanPayment());
				 actEmp.setEmpLicenseNo(employee.getEmpLicenseNo());
				 actEmp.setEmpLicenseExp(employee.getEmpLicenseExp());
				 actEmp.setEmpMailId(employee.getEmpMailId());
				 actEmp.setEmpPhNo(employee.getEmpPhNo());
				 actEmp.setEmpTelephoneAmt(employee.getEmpTelephoneAmt());
				 actEmp.setEmpMobAmt(employee.getEmpMobAmt());
				 actEmp.setEmpBankAcNo(employee.getEmpBankAcNo());
				 actEmp.setEmpBankName(employee.getEmpBankName());
				 actEmp.setEmpBankBranch(employee.getEmpBankBranch());
				 actEmp.setEmpBankIFS(employee.getEmpBankIFS());
				 actEmp.setEmpPanNo(employee.getEmpPanNo());
				 
				 
				 session.merge(actEmp);
				 res = 1;
			 }
			 transaction.commit();
			 session.flush();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	session.clear();
    	session.close();
    	return res;
     }

	@Override
	@Transactional
	public List<Map<String, Object>> getEmpList() {
	
		List<Map<String, Object>> empList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT new map(empId as empId, empCodeTemp as empCodeTemp, empName as empName, empFaCode as empFaCode) " +
					"FROM Employee WHERE isTerminate = :isTerminate");
			query.setString("isTerminate", "false");
			empList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return empList;
	}
	
	@Transactional
 	 @SuppressWarnings("unchecked")
    public String getEmpNameByCode(String empCode){
   	 System.out.println("enter into getEmpNameByEmpCode function");
   	 List<String> empList = new ArrayList<>();
   	 String empName = "";
   	 try{
   		 session = this.sessionFactory.openSession();
   		 SQLQuery query=session.createSQLQuery("select empName from employee where empFaCode=:code");
   		 query.setParameter("code", empCode);
			// Criteria cr=session.createCriteria(Employee.class);
			 //cr.add(Restrictions.eq(EmployeeCNTS.EMP_CODE,empCode));
			 empList = query.list();
			 if(!empList.isEmpty()){
				 empName = empList.get(0);
			 }
			 session.flush();
   	 }catch(Exception e){
   		 e.printStackTrace();
   	 }
   	 session.clear();
   	 session.close();
   	 return empName;
    }
	
	
 }