<div ng-show="operatorLogin || superAdminLogin">
	<title>Cancel SEDR</title>
	
	<div class="row">
	<form name="sedrDtailForm" ng-submit="cancelSedr()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);" >
	
	<div class="row">
				<div class="input-field col s6">
					<input type="text" name="sedrCode" ng-model="sedrCode" ng-required=true>
					 <label>SEDR No.</label>
				</div>
				<div class="input-field col s6">
					<input type="date" name="sedrDt" ng-model="sedrDt" ng-required=true>
					<label>Date</label>
				</div>
			</div>
	
	<div class="row">
				<div class="col s12 center"> 
					<input class="btn" type="submit" value="Cancel">
				</div>
			 </div>
	</form>
	
	</div>
</div>