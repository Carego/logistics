package com.mylogistics.model;

//import java.util.Date;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "stationary")
public class Stationary {
	
	@Id
	@Column(name="stId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stId;
	
	@Column(name="stCode")
	private String stCode;
	
	@Column(name="stOdrDt")
	private Date stOdrDt;
	
	@Column(name="stRecDt")
	private Date stRecDt;
	
	/*@Column(name="stOdrBranchCode")
	private String stOdrBranchCode;*/
	
	@Column(name="stOdrCnmt")
	private int stOdrCnmt;
	
	@Column(name="stOdrChln")
	private int stOdrChln;
	
	@Column(name="stOdrSedr")
	private int stOdrSedr;
	
	@Column(name="stRecCnmt")
	private int stRecCnmt;
	
	@Column(name="stRecChln")
	private int stRecChln;
	
	@Column(name="stRecSedr")
	private int stRecSedr;
			
	@Column(name="stSupInvcNo")
	private String stSupInvcNo;
	
	@Column(name="stSupCode")
	private String stSupCode;
	
	@Column(name="stIsRec")
	private String stIsRec;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "stnId")
	@JsonIgnore
	private List<MasterStationaryStk> mstrStock;

	
	public String getStIsRec() {
		return stIsRec;
	}

	public void setStIsRec(String stIsRec) {
		this.stIsRec = stIsRec;
	}

	public int getStId() {
		return stId;
	}

	public void setStId(int stId) {
		this.stId = stId;
	}

	public String getStCode() {
		return stCode;
	}

	public void setStCode(String stCode) {
		this.stCode = stCode;
	}

	public Date getStOdrDt() {
		return stOdrDt;
	}

	public void setStOdrDt(Date stOdrDt) {
		this.stOdrDt = stOdrDt;
	}

	public Date getStRecDt() {
		return stRecDt;
	}

	public void setStRecDt(Date stRecDt) {
		this.stRecDt = stRecDt;
	}

	public int getStOdrCnmt() {
		return stOdrCnmt;
	}

	public void setStOdrCnmt(int stOdrCnmt) {
		this.stOdrCnmt = stOdrCnmt;
	}

	public int getStOdrChln() {
		return stOdrChln;
	}

	public void setStOdrChln(int stOdrChln) {
		this.stOdrChln = stOdrChln;
	}

	public int getStOdrSedr() {
		return stOdrSedr;
	}

	public void setStOdrSedr(int stOdrSedr) {
		this.stOdrSedr = stOdrSedr;
	}

	public int getStRecCnmt() {
		return stRecCnmt;
	}

	public void setStRecCnmt(int stRecCnmt) {
		this.stRecCnmt = stRecCnmt;
	}

	public int getStRecChln() {
		return stRecChln;
	}

	public void setStRecChln(int stRecChln) {
		this.stRecChln = stRecChln;
	}

	public int getStRecSedr() {
		return stRecSedr;
	}

	public void setStRecSedr(int stRecSedr) {
		this.stRecSedr = stRecSedr;
	}

	public String getStSupInvcNo() {
		return stSupInvcNo;
	}

	public void setStSupInvcNo(String stSupInvcNo) {
		this.stSupInvcNo = stSupInvcNo;
	}

	public String getStSupCode() {
		return stSupCode;
	}

	public void setStSupCode(String stSupCode) {
		this.stSupCode = stSupCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public List<MasterStationaryStk> getMstrStock() {
		return mstrStock;
	}

	public void setMstrStock(List<MasterStationaryStk> mstrStock) {
		this.mstrStock = mstrStock;
	}
	
	
}
