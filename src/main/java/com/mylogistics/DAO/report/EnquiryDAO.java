package com.mylogistics.DAO.report;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Bill;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.MoneyReceipt;

@Repository
public interface EnquiryDAO {

	public List<String> getCodeListByType(String branchCode, String type);
	
	public List<Map<String, Object>> getEnquiry(String type, String enquiryCode, String faCodeFrom, String faCodeTo, String fromDate, String toDate);
		
	public List getBillDetByMr(String branchCode, String mrNo);
	
	public List<FAMaster> getFaCodeByCode(String branchCode, String faCode);
	
}