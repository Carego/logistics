package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ServiceTaxDAO;
import com.mylogistics.model.ServiceTax;

public class ServiceTaxDAOImpl implements ServiceTaxDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public ServiceTaxDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 public int saveServiceTaxToDB(ServiceTax servicetax){
		 int temp;
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  session.save(servicetax);
			  transaction.commit();
			  session.flush();
			  temp=1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		session.clear();
		session.close();
		return temp;	 
	}
	 
	 @SuppressWarnings("unchecked")
		@Transactional
		 public List<ServiceTax> getLastSrvTaxRow(){
			 List<ServiceTax> list = new ArrayList<ServiceTax>();			
			 try{
				 session = this.sessionFactory.openSession();
				 list = session.createQuery("from ServiceTax order by stId DESC").setMaxResults(1).list();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace();
			 }
			
			 /*System.out.println("servicetax EduCessRate is----"+list.get(0).getStEduCessRt());
			 System.out.println("servicetax hsc rate is----"+list.get(0).getStHscCessRt());*/
			 session.clear();
			 session.close();
			 return list;
		}
	 
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public ServiceTax getLastSerTax(){
		 System.out.println("enter into getLastSerTax function");
		 ServiceTax serviceTax = null;
		 List<ServiceTax> list = new ArrayList<ServiceTax>();		
		 try{
			 session = this.sessionFactory.openSession();
			 list = session.createQuery("from ServiceTax order by stId DESC").setMaxResults(1).list();
			 if(!list.isEmpty()){
				 serviceTax = list.get(0);
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return serviceTax;
	 }
}
