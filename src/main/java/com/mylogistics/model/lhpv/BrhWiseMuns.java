package com.mylogistics.model.lhpv;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "brhwisemuns")
public class BrhWiseMuns {

	@Id
	@Column(name="bwmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bwmId;
	
	@Column(name="bwmBrhId" , columnDefinition="int default -1")
	private int bwmBrhId;

	@Column(name="bwmMuns" , columnDefinition="double default 0")
	private double bwmMuns;
	
	/*@Column(name="bwmTds" , columnDefinition="float default 0")
	private float bwmTds;*/
	
	@Column(name="bwmCsDis" , columnDefinition="float default 0")
	private float bwmCsDis;
	
	@Column(name="bwmActive" , columnDefinition="boolean default false")
	private boolean bwmActive;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getBwmId() {
		return bwmId;
	}

	public void setBwmId(int bwmId) {
		this.bwmId = bwmId;
	}

	public int getBwmBrhId() {
		return bwmBrhId;
	}

	public void setBwmBrhId(int bwmBrhId) {
		this.bwmBrhId = bwmBrhId;
	}

	public double getBwmMuns() {
		return bwmMuns;
	}

	public void setBwmMuns(double bwmMuns) {
		this.bwmMuns = bwmMuns;
	}

	

	public float getBwmCsDis() {
		return bwmCsDis;
	}

	public void setBwmCsDis(float bwmCsDis) {
		this.bwmCsDis = bwmCsDis;
	}

	public boolean isBwmActive() {
		return bwmActive;
	}

	public void setBwmActive(boolean bwmActive) {
		this.bwmActive = bwmActive;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
}
