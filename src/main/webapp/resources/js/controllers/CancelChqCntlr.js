'use strict';

var app = angular.module('application');

app.controller('CancelChqCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	console.log("CancelChqCntlr Started");
	
	$scope.bankMstrList = [];
	$scope.bankMstr = {};
	
	$scope.chqLeaveList = [];
	$scope.chqLeave = {};
	$scope.cancelChqAlertDBFlag = true;
	$scope.bankMstrDBFlag = true;
	$scope.chqLeaveListDBFlag = true;
	
	$scope.getBranchBankMstr = function() {
		var response = $http.post($scope.projectName+'/getBranchBankMstr');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getBranchBankMstr Success");
				$scope.bankMstrList = data.bankMstrList;
			}else {
				console.log("getBranchBankMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getUnUsedChq Error: "+data);
		});
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		
		$scope.bankMstrDBFlag = false;
		$('div#bankMstrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankMstrDBFlag = true;
			}
		});

		$('div#bankMstrDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr){
		console.log("saveBank");
		console.log("enter into branchName----->"+bankMstr.bnkName);
		console.log("enter into branchFaCode----->"+bankMstr.bnkFaCode);
		$scope.bankMstr = bankMstr;
		$('div#bankMstrDB').dialog('close');
		
		$scope.getUnUsedChq();
	}
	
	$scope.getUnUsedChq = function(){
		console.log("getUnUsedChq Entered");
		var response = $http.post($scope.projectName+'/unUsedChq', $scope.bankMstr);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getUnUsedChq Success");
				$scope.chqLeaveList = data.unUsedChqLeaveList;
			}else {
				console.log("getUnUsedChq Error");
				$scope.alertToast("No cheque for cancel");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getUnUsedChq Error: "+data);
		});
	}
	
	$scope.openChqLeaveListDB = function() {
		console.log("Entered into openChqLeaveListDB");
		
		$scope.chqLeaveListDBFlag = false;
		$('div#chqLeaveListDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Unsed Cheque Leaves",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.chqLeaveListDBFlag = true;
			}
		});

		$('div#chqLeaveListDB').dialog('open');
	}
	
	$scope.saveChqLeave =  function(chqLeave){
		console.log("enter into saveChqLeave()");
		$scope.chqLeave = chqLeave;
		$('div#chqLeaveListDB').dialog('close');
		
	}
	
	$scope.cancelChq = function(cancelChqForm) {
		
		if(cancelChqForm.$invalid){
			if (cancelChqForm.cancelChqNoName.$invalid) {
				$scope.alertToast("Enter valid cheque no");
			} 
		} else {
			$scope.cancelChqAlertDBFlag = false;
	    	$('div#cancelChqAlertDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.cancelChqAlertDBFlag = true;
			    }
				});
			$('div#cancelChqAlertDB').dialog('open');
		}
		
	}
	
	$scope.cancel = function() {
		console.log("cancelPrint()");
		$('div#cancelChqAlertDB').dialog('close');
	}
	
	$scope.yes = function() {
		console.log("yesPrint()");
		$('div#cancelChqAlertDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/cancelChqLeave', $scope.chqLeave);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.alertToast("Cheque cancel Success");
				$scope.chqLeave = {};
				$scope.cancelChqAlertDBFlag = true;
				$scope.getUnUsedChq();
			}else {
				$scope.alertToast("Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("cancelChqLeave Error: "+data);
		});
		
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getUnUsedChq();
		$scope.getBranchBankMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
		
	}else{
		console.log("****************");
	}
	
	console.log("CancelChqCntlr Ended");
	
}]);