package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "bill")
public class Bill {

	@Id
	@Column(name="blId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int blId;
	
	@Column(name="blBillNo")
	@NotNull(message = "Bill no. can not be null !", groups = BillInsert.class)
	@NotBlank(message = "Bill no. can not be blank !", groups = BillInsert.class)
	private String blBillNo;
	
	@NotNull(message = "Bill type can not be null !", groups = BillInsert.class)
	@Column(name="blType")
	private char blType;
	
	@Column(name="blCancel", columnDefinition="boolean default false")
	private boolean blCancel;
	
	@Column(name="blCustId")
	@Min(value = 1, message = "Please select customer !", groups = BillInsert.class)
	private int blCustId;
	
	@Column(name="blBrhId")
	private int blBrhId;
	
	@NotNull(message = "Bill not can not be null !", groups = BillInsert.class)	
	@Column(name="blBillDt")
	private Date blBillDt;
	
	@Column(name="blBillSubDt")
	private Date blBillSubDt;
	
	@Column(name="blTaxableSerTax" , columnDefinition="double default 0.0")
	@Min(value = 1, message = "Taxable service tax should be greater than 0", groups = BillInsert.class)
	private double blTaxableSerTax;
	
	@Column(name="blSerTax" , columnDefinition="double default 0.0")
	@Min(value = 1, message = "Service tax should be greater than 0", groups = BillInsert.class)
	private double blSerTax;
	
	@Column(name="blSwachBhCess" , columnDefinition="double default 0.0")
	private double blSwachBhCess;
	
	@Column(name="blKisanKalCess",columnDefinition="double default 0.0")
	private double blKisanKalCess;
		
	@Column(name="blSubTot" , columnDefinition="double default 0.0")
	@Min(value = 1, message = "Bill sub total should be greater than 0", groups = BillInsert.class)
	private double blSubTot;
	
	@Column(name="blFinalTot" , columnDefinition="double default 0.0")
	@Min(value = 1, message = "Bill final total should be greater than 0", groups = BillInsert.class)
	private double blFinalTot;
	
	@Column(name="blRemAmt" , columnDefinition="double default 0.0")
	private double blRemAmt;
	
	//One To Many with BillDetail
	@JsonIgnore
	@JsonManagedReference(value = "billBDet")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="bill_billdetail",
	joinColumns=@JoinColumn(name="blId"),
	inverseJoinColumns=@JoinColumn(name="bdId"))
	private List<BillDetail> billDetList = new ArrayList<BillDetail>();
	
	//Many To One with BillForwarding
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonBackReference(value = "bfBill")
	@JoinColumn(name = "bfId") 
	private BillForwarding billForwarding;
	
	@Column(name="blDesc")
	@NotNull(message = "Bill description can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "Bill description can not be empty !", groups = BillInsert.class)
	private String blDesc;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	@NotNull(message = "BCode can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "BCode can not be empty !", groups = BillInsert.class)
	private String bCode;
	
	@Column(name="userCode")
	@NotNull(message = "User code can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "User code can not be empty !", groups = BillInsert.class)
	private String userCode;
	
	@Column(name="blStateGST")
	@NotNull(message = "State code can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "State code can not be empty !", groups = BillInsert.class)
	private String blStateGST;
	
	@Column(name="blGstNo")
	@NotNull(message = "GST No. can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "GST No. can not be empty !", groups = BillInsert.class)
	private String blGstNo;
	
	@Column(name="blSgst", columnDefinition="float default 0")
	@NotNull(message = "SGST can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "SGST can not be empty !", groups = BillInsert.class)
	private Float blSgst;
	
	@Column(name="blCgst", columnDefinition="float default 0")
	@NotNull(message = "CGST can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "CGST can not be empty !", groups = BillInsert.class)
	private Float blCgst;
	
	@Column(name="blIgst", columnDefinition="float default 0")
	@NotNull(message = "IGST can not be null !", groups = BillInsert.class)
	@NotEmpty(message = "IGST can not be empty !", groups = BillInsert.class)
	private Float blIgst;
	
	@Column(name = "blBillZ")
	private String blBillZ;
	
	@Transient
	private double blFinalTotTemp;

	public int getBlId() {
		return blId;
	}

	public void setBlId(int blId) {
		this.blId = blId;
	}

	public String getBlBillNo() {
		return blBillNo;
	}

	public void setBlBillNo(String blBillNo) {
		this.blBillNo = blBillNo;
	}

	public char getBlType() {
		return blType;
	}

	public void setBlType(char blType) {
		this.blType = blType;
	}

	public int getBlCustId() {
		return blCustId;
	}

	public void setBlCustId(int blCustId) {
		this.blCustId = blCustId;
	}

	public int getBlBrhId() {
		return blBrhId;
	}

	public void setBlBrhId(int blBrhId) {
		this.blBrhId = blBrhId;
	}

	public Date getBlBillDt() {
		return blBillDt;
	}

	public void setBlBillDt(Date blBillDt) {
		this.blBillDt = blBillDt;
	}

	public double getBlTaxableSerTax() {
		return blTaxableSerTax;
	}

	public void setBlTaxableSerTax(double blTaxableSerTax) {
		this.blTaxableSerTax = blTaxableSerTax;
	}

	public double getBlSerTax() {
		return blSerTax;
	}

	public void setBlSerTax(double blSerTax) {
		this.blSerTax = blSerTax;
	}

	public double getBlSwachBhCess() {
		return blSwachBhCess;
	}

	public void setBlSwachBhCess(double blSwachBhCess) {
		this.blSwachBhCess = blSwachBhCess;
	}

		
	public double getBlKisanKalCess() {
		return blKisanKalCess;
	}

	public void setBlKisanKalCess(double blKisanKalCess) {
		this.blKisanKalCess = blKisanKalCess;
	}

	public double getBlSubTot() {
		return blSubTot;
	}

	public void setBlSubTot(double blSubTot) {
		this.blSubTot = blSubTot;
	}

	public double getBlFinalTot() {
		return blFinalTot;
	}

	public void setBlFinalTot(double blFinalTot) {
		this.blFinalTot = blFinalTot;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public List<BillDetail> getBillDetList() {
		return billDetList;
	}

	public void setBillDetList(List<BillDetail> billDetList) {
		this.billDetList = billDetList;
	}

	public String getBlDesc() {
		return blDesc;
	}

	public void setBlDesc(String blDesc) {
		this.blDesc = blDesc;
	}

	public BillForwarding getBillForwarding() {
		return billForwarding;
	}

	public void setBillForwarding(BillForwarding billForwarding) {
		this.billForwarding = billForwarding;
	}

	public Date getBlBillSubDt() {
		return blBillSubDt;
	}

	public void setBlBillSubDt(Date blBillSubDt) {
		this.blBillSubDt = blBillSubDt;
	}

	public double getBlRemAmt() {
		return blRemAmt;
	}

	public void setBlRemAmt(double blRemAmt) {
		this.blRemAmt = blRemAmt;
	}

	public boolean isBlCancel() {
		return blCancel;
	}

	public void setBlCancel(boolean blCancel) {
		this.blCancel = blCancel;
	}

	public double getBlFinalTotTemp() {
		return blFinalTotTemp;
	}

	public void setBlFinalTotTemp(double blFinalTotTemp) {
		this.blFinalTotTemp = blFinalTotTemp;
	}
	
	
	public String getBlStateGST() {
		return blStateGST;
	}

	public void setBlStateGST(String blStateGST) {
		this.blStateGST = blStateGST;
	}

	public String getBlGstNo() {
		return blGstNo;
	}

	public void setBlGstNo(String blGstNo) {
		this.blGstNo = blGstNo;
	}

	public Float getBlSgst() {
		return blSgst;
	}

	public void setBlSgst(Float blSgst) {
		this.blSgst = blSgst;
	}

	public Float getBlCgst() {
		return blCgst;
	}

	public void setBlCgst(Float blCgst) {
		this.blCgst = blCgst;
	}

	public Float getBlIgst() {
		return blIgst;
	}

	public void setBlIgst(Float blIgst) {
		this.blIgst = blIgst;
	}


	public String getBlBillZ() {
		return blBillZ;
	}

	public void setBlBillZ(String blBillZ) {
		this.blBillZ = blBillZ;
	}



	public interface BillInsert{		
	}
	
}
