package com.mylogistics.services;

import java.util.List;

import com.mylogistics.model.PenaltyBonusDetention;

public interface PBDService {
	
	public List<PenaltyBonusDetention> getAllPBD();

    public void addPBD(PenaltyBonusDetention penaltyBonusDetention);
    
    public void deletePBD(PenaltyBonusDetention penaltyBonusDetention);

    public void deleteAllPBD();

}
