package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Challan;
import com.mylogistics.services.ConstantsValues;

@Controller
public class ArvRepAlwCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private ArrivalReportDAO arrivalReportDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	
	@RequestMapping(value="/getBrhSedrAlw",method = RequestMethod.POST)
	public @ResponseBody Object getBrhSedrAlw(){
		System.out.println("Entered into getBrhSedrAlw function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String,Object>> brhList = branchDAO.getNameAndCode();
		if(!brhList.isEmpty()){
			map.put("brhList",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	@RequestMapping(value="/getSedrFrAlw",method = RequestMethod.POST)
	public @ResponseBody Object getSedrFrAlw(@RequestBody String brhCode){
		System.out.println("Entered into getSedrFrAlw function in controller--"+brhCode);
		Map<String,Object> map = new HashMap<String,Object>();
		List<ArrivalReport> arCodeList = arrivalReportDAO.getAllArCodes(brhCode);
		if(!arCodeList.isEmpty()){
			map.put("list",arCodeList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	@RequestMapping(value="/getSedrFrHoAlw",method = RequestMethod.POST)
	public @ResponseBody Object getSedrFrHoAlw(@RequestBody String arCode){
		System.out.println("Entered into getSedrFrAlw function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		if(arCode != null){
			List<ArrivalReport> arList = arrivalReportDAO.getArrivalReport(arCode);
			if(!arList.isEmpty()){
				List<Challan> chlnList=challanDAO.getChallanByCode(arList.get(0).getArchlnCode());
				if(!chlnList.isEmpty()){
					map.put("chlnDt", chlnList.get(0).getChlnDt());
					map.put("lorryNo", chlnList.get(0).getChlnLryNo());
				}
				map.put("arObj",arList.get(0));
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	
	@RequestMapping(value = "/editArByHoAlw", method = RequestMethod.POST)
	public @ResponseBody Object EditChallanSubmit(@RequestBody ArrivalReport arrivalReport) {
		System.out.println("enter into editArByHoAlw function");
		System.out.println("arrival true or falsde===="+arrivalReport.isSrtgDmgAlw());
		int temp=arrivalReportDAO.updateAR(arrivalReport);
		Map<String, Object> map = new HashMap<String, Object>();
			
		if(temp>0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getChlnFrAlwSrtgDmg", method = RequestMethod.POST)
	public @ResponseBody Object getChlnFrAlwSrtgDmg() {
		System.out.println("enter into editArByHoAlw function");
		Map<String,Object> map = new HashMap<String,Object>();
		List arChlnCodeList = arrivalReportDAO.getArChlnCodeFrSrtgDmgAlw();
		if(!arChlnCodeList.isEmpty()){
			map.put("list",arChlnCodeList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	@RequestMapping(value="/getSedrFrSDAlw",method = RequestMethod.POST)
	public @ResponseBody Object getSedrFrSDAlw(@RequestBody String arChlnCode){
		System.out.println("Entered into getSedrFrAlw function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		if(arChlnCode != null){
			List<ArrivalReport> arList = arrivalReportDAO.getARByChlnCode(arChlnCode);
			if(!arList.isEmpty()){
				map.put("arObj",arList.get(0));
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
}
