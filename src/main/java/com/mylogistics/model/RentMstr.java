package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="rentmstr")
public class RentMstr {

	@Id
	@Column(name="rentMId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int rentMId;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "branchId") 
	private Branch branch;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "empId") 
	private Employee employee;
	
	//One To One
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "addId")
	private Address address;
	
	@Column(name="rentMLandLordName")
	private String rentMLandLordName;
	
	@Column(name="rentMRentPM")
	private double rentMRentPM;
	
	@Column(name="rentMSecDep")
	private double rentMSecDep;
	
	@Column(name="rentMAdv")
	private double rentMAdv;
	
	@Column(name="rentMPanNo")
	private String rentMPanNo;
	
	@Column(name="rentMBankName")
	private String rentMBankName;
	
	@Column(name="rentMIFSC")
	private String rentMIFSC;
	
	@Column(name="rentMFor")
	private String rentMFor;
	
	@Column(name="rentMAgreementFrom")
	private Date rentMAgreementFrom;
	
	@Column(name="rentMAgreementTo")
	private Date rentMAgreementTo;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="rentAcNo", length=30)
	private String rentAcNo;
	
	@Column(name="rentBankBranch", length=60)
	private String rentBankBranch;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getRentMId() {
		return rentMId;
	}

	public void setRentMId(int rentMId) {
		this.rentMId = rentMId;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getRentMLandLordName() {
		return rentMLandLordName;
	}

	public void setRentMLandLordName(String rentMLandLordName) {
		this.rentMLandLordName = rentMLandLordName;
	}

	public double getRentMRentPM() {
		return rentMRentPM;
	}

	public void setRentMRentPM(double rentMRentPM) {
		this.rentMRentPM = rentMRentPM;
	}

	public double getRentMSecDep() {
		return rentMSecDep;
	}

	public void setRentMSecDep(double rentMSecDep) {
		this.rentMSecDep = rentMSecDep;
	}

	public double getRentMAdv() {
		return rentMAdv;
	}

	public void setRentMAdv(double rentMAdv) {
		this.rentMAdv = rentMAdv;
	}

	public String getRentMPanNo() {
		return rentMPanNo;
	}

	public void setRentMPanNo(String rentMPanNo) {
		this.rentMPanNo = rentMPanNo;
	}

	public String getRentMBankName() {
		return rentMBankName;
	}

	public void setRentMBankName(String rentMBankName) {
		this.rentMBankName = rentMBankName;
	}

	public String getRentMIFSC() {
		return rentMIFSC;
	}

	public void setRentMIFSC(String rentMIFSC) {
		this.rentMIFSC = rentMIFSC;
	}

	public String getRentMFor() {
		return rentMFor;
	}

	public void setRentMFor(String rentMFor) {
		this.rentMFor = rentMFor;
	}

	public Date getRentMAgreementFrom() {
		return rentMAgreementFrom;
	}

	public void setRentMAgreementFrom(Date rentMAgreementFrom) {
		this.rentMAgreementFrom = rentMAgreementFrom;
	}

	public Date getRentMAgreementTo() {
		return rentMAgreementTo;
	}

	public void setRentMAgreementTo(Date rentMAgreementTo) {
		this.rentMAgreementTo = rentMAgreementTo;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getRentAcNo() {
		return rentAcNo;
	}

	public void setRentAcNo(String rentAcNo) {
		this.rentAcNo = rentAcNo;
	}

	public String getRentBankBranch() {
		return rentBankBranch;
	}

	public void setRentBankBranch(String rentBankBranch) {
		this.rentBankBranch = rentBankBranch;
	}
	
	
}
