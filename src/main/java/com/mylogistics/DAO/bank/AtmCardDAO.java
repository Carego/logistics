package com.mylogistics.DAO.bank;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.User;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.services.bank.AtmCardService;

@Repository
public interface AtmCardDAO {

	public int saveAtmCard(AtmCardService atmCardService, User currentUser);
	
	public AtmCardMstr getAtmCardByNo(String atmCardNo);
	
	public int transAtmToBr(Map<String, Object> transAtmToBrService);
	
	public List<AtmCardMstr> getAtmByBrh(int brhId);
	
	public int deactiveAtmCard(int atmCardId);
	
	public int activeAtmCard(int atmCardId);
}
