package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.services.CsTempNCsListService;

@Repository
public interface CashStmtTempDAO {

	public List<CashStmtTemp> getPendingCS(int cssId);
	
	public int updateCashStmtTemp(CsTempNCsListService csTempNcsListService);
}
