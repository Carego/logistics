package com.mylogistics.constants;

public class MunsianaCNTS {

	public static final String 	MUNS_FR_ADV = "munsFrAdv";
	
	public static final String 	MUNS_TO_ADV = "munsToAdv";
	
	public static final String 	MUNS_AMT = "munsAmt";
}
