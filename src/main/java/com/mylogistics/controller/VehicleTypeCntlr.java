package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.services.ConstantsValues;

@Controller
public class VehicleTypeCntlr {
	
	@Autowired
	private VehicleTypeDAO vehicletypeDAO;
	
	@Autowired
	private HttpSession httpSession;


	@RequestMapping(value = "/submitVehicleType", method = RequestMethod.POST)
    public @ResponseBody Object submitVehicleType(@RequestBody VehicleType vehicletype){
		Map<String,String> map = new HashMap<String,String>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		vehicletype.setUserCode(currentUser.getUserCode());
		vehicletype.setbCode(currentUser.getUserBranchCode());
		
		int temp=vehicletypeDAO.saveVehicleTypeToDB(vehicletype);
		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
	
		return map;
    
	}
	
	@RequestMapping(value="/getVehicleTypeDetails",method = RequestMethod.POST)
	public @ResponseBody Object getVehicleTypeDetails(){
		List<VehicleType> vehicleTypeList = vehicletypeDAO.getVehicleType();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(vehicleTypeList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
  			map.put("list",vehicleTypeList);	
  		}else {
  		  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}		
		return map;	
	}
	
	 @RequestMapping(value = "/updateVehicleType", method = RequestMethod.POST)
	 public @ResponseBody Object updateVehicleType(@RequestBody VehicleType vehicleType){
		 System.out.println("Entered into updateVehicleType of controller----");
		 	
		 Map<String,String> map = new HashMap<String, String>();
		 		
		 int temp=vehicletypeDAO.updateVehicleType(vehicleType);
		 if (temp>=0) {
	        	
			 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		 } else {
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }	    	
		 return map;
	 }
	 
	 @RequestMapping(value = "/getVehTypeList", method = RequestMethod.POST)
	 public @ResponseBody Object getVehTypeList(){
		 System.out.println("getVehTypeList()");
		 Map<String, Object> map = new HashMap<>();
		 
		 List<Map<String, String>> vehTypeList = vehicletypeDAO.getVehTypeList();
		 
		 if (!vehTypeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("vehTypeList", vehTypeList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		 
		 return map;
	 }
	
	 
	 @RequestMapping(value="/getHypo",method = RequestMethod.POST)
		public @ResponseBody Object getHypo(){
			List<String> hypoList = vehicletypeDAO.getHypo();
			Map<String,Object> map = new HashMap<String,Object>();
			if (!(hypoList.isEmpty())) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	  			map.put("list",hypoList);	
	  		}else {
	  		  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}		
			return map;	
		}
	 
}
