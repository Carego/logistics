package com.mylogistics.controller.report;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.LedgerDAO;
import com.mylogistics.model.User;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class LedgerCntlr {

	public static final Logger logger = Logger.getLogger(LedgerCntlr.class);
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	@Autowired
	private LedgerDAO ledgeDAO;
	
	private static final String AUTH = "authorized";
	private static final String UN_AUTH = "unAuthorized";
	private Map<Integer, Object> resultMap = new HashMap<Integer, Object>();
	
	@Autowired
	HttpSession httpSession;
	
	public String authentication(){
		logger.info("---------enter into authentication function");
		User currentUser = (User)httpSession.getAttribute("currentUser");

		if(currentUser == null){
			logger.info("current user is null");
			return UN_AUTH;
		}else{
			logger.info("current user authentication token ="+currentUser.getUserAuthToken());
			logger.info("current session id ="+httpSession.getId());
			if(currentUser.getUserAuthToken().equalsIgnoreCase(httpSession.getId())){
				logger.info("authorized user set in session");
				return AUTH;
			}else{
				logger.info("unAuthorized user set in session");
				return UN_AUTH;
			}
		}
	}
	
	@RequestMapping(value = "/getLedgerReportHtml", method = RequestMethod.POST)
	public @ResponseBody Object getLedgerReportHtml(@RequestBody Map<String, Object> ledgerReportMap) {
		logger.info("LedgerCntlr.getLedgerReportHtml()");
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Map<String, Object> ledRepMap = ledgerReportMap;
		
		Map<String, Object> restMap = new HashMap<String, Object>();
		restMap.put("ledRepMap", ledRepMap);
		
		Map<String, Map<String, Object>> openingBalMap = calBalanceForLedger(ledgerReportMap);
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<Map<String, Object>> ledgerMapList = new ArrayList<>();
		
		if ((boolean) ledgerReportMap.get("isBranch")) {
			List<Map<String, Object>> ledgerMapListTemp = ledgeDAO.getBrLedger(ledgerReportMap); 
			ledgerMapList =  ledgeDAO.getBrLedgerFinalList(ledgerMapListTemp);
		}else if ((boolean) ledgerReportMap.get("isConsolidate")) {
			List<Map<String, Object>> ledgerMapListTemp = ledgeDAO.getConsolidateLedger(ledgerReportMap);
			ledgerMapList = ledgeDAO.getConsolidateLedgerFinalList(ledgerMapListTemp);			
		}
		
		double sumDr = 0;
		double sumCr = 0;
		
		if(!ledgerMapList.isEmpty()){
			//create list of hashMap which having unique fa codes
			Map<Object, Object> faCodeNameMap = new HashMap<>();
			
			List<String> faCode = new ArrayList<>();
			for (Map<String, Object> ledgerMap : ledgerMapList) {
				faCode.add(String.valueOf(ledgerMap.get("csFaCode")));
				if (!ledgerMap.containsKey(ledgerMap.get("csFaCode"))) {
					faCodeNameMap.put(ledgerMap.get("csFaCode"), ledgerMap.get("csFaName"));
				}
			}
					
			Set<String> hs = new HashSet<>();
			hs.addAll(faCode);
			faCode.clear();
			faCode.addAll(hs);
			
			Collections.sort(faCode);
			
			List<Map<String, Object>> faCodeNameList = new ArrayList<>();
			for (int i = 0; i < faCode.size(); i++) {
				Map<String, Object> tempMap = new HashMap<>();
				
				double totalDr = 0.00;
				double totalCr = 0.00;
				double balance = 0.00;
				String balanceType = null;
				
				for (Map<String, Object> ledgerMap : ledgerMapList) {
					 if (faCode.get(i).equalsIgnoreCase(String.valueOf(ledgerMap.get("csFaCode")))) {
						if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("D")) {
							totalDr = (totalDr+Double.parseDouble(String.valueOf(ledgerMap.get("csAmt"))));
						} else if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("C")) {
							totalCr = (totalCr+Double.parseDouble(String.valueOf(ledgerMap.get("csAmt"))));
						}
					} 
				}
				
				if (totalDr > totalCr) {
					balance = totalDr - totalCr;
					balanceType = "C";
				} else if (totalDr < totalCr) {
					balance = totalCr - totalDr;
					balanceType = "D";
				} else if (totalDr == totalCr) {
					balance = totalCr - totalDr;
					balanceType = "equal";
				}
				
				sumDr = sumDr+totalDr;
				sumCr = sumCr+totalCr;
				
				tempMap.put("csFaCode", faCode.get(i));
				tempMap.put("csFaName", String.valueOf(faCodeNameMap.get(faCode.get(i))));
				tempMap.put("csAmtBalance", balance);
				tempMap.put("csDrCrBalance", balanceType);
				if (openingBalMap.get(faCode.get(i)) != null) {
					tempMap.put("csAmtOpeningBalance", openingBalMap.get(faCode.get(i)).get("csAmtOpeningBalance"));
					tempMap.put("csDrCrOpeningBalance", openingBalMap.get(faCode.get(i)).get("csDrCrOpeningBalance"));
					logger.info("==========>>if");
					if (String.valueOf(openingBalMap.get(faCode.get(i)).get("csDrCrOpeningBalance")).equalsIgnoreCase("D")) {
						logger.info("ifif");
						tempMap.put("csAmtTotal", totalDr + Double.parseDouble(String.valueOf(openingBalMap.get(faCode.get(i)).get("csAmtOpeningBalance"))));
						logger.info("sumDr: "+totalDr);
						logger.info("OpeningBalance: "+Double.parseDouble(String.valueOf(openingBalMap.get(faCode.get(i)).get("csAmtOpeningBalance"))));
					} else {
						if (totalDr>0) {
							tempMap.put("csAmtTotal", totalDr);
							logger.info("ifElseif");
						} else {
							tempMap.put("csAmtTotal", "0");
							logger.info("ifElseElse");
						}
					}
				} else {
					logger.info("==========>>Else");
					tempMap.put("csAmtTotal", totalDr);
				}
				faCodeNameList.add(tempMap);
			}
			logger.info("FF - "+ledgerMapList.size());
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("ledgerMapList",ledgerMapList);
			map.put("faCodeNameList", faCodeNameList);
			
			restMap.put("ledgerMapList", ledgerMapList);
			restMap.put("faCodeNameList", faCodeNameList);
			resultMap.put(currentUser.getUserId(), restMap);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		logger.info("Dr sum: "+sumDr);
		logger.info("Cr sum: "+sumCr);
		
		return map;
	}
	
	@RequestMapping(value = "/getLedgerReportPdf")
	public ModelAndView getLedgerReportPdf() {
		logger.info("enter into getLedgerReportPdf funciton");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Map<String, Object> restMap = (Map) resultMap.get(currentUser.getUserId());
		List<Map<String, Object>> ledgerMapList = (List)restMap.get("ledgerMapList");
		List<Map<String, Object>> faCodeNameList = (List)restMap.get("faCodeNameList");
		Map<String, Object> ledRepMap = (Map) restMap.get("ledRepMap");
		
		map.put("ledgerMapList",ledgerMapList);
		map.put("faCodeNameList", faCodeNameList);
		map.put("ledgerReportMap", ledRepMap);
		
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return new ModelAndView("sessionExpired", null);
		else	
			return new ModelAndView("ledgerReportPdfView", map);
	}
	
	@RequestMapping(value = "/getLedgerReportXLSX", method=RequestMethod.POST)
	public void getLedgerReportXLSX(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("getLedgerReportXLSX");
		
		XSSFWorkbook workbook = new XSSFWorkbook(); 
		//Create a blank sheet
		XSSFSheet spreadsheet = workbook.createSheet();
		spreadsheet.setColumnWidth(0, 3*256); //approx room for 15 char
		spreadsheet.setColumnWidth(1, 7*256); //approx room for 15 char
		spreadsheet.setColumnWidth(2, 3*256); //approx room for 15 char
		spreadsheet.setColumnWidth(3, 12*256); //approx room for 20 char
		spreadsheet.setColumnWidth(4, 7*256); //approx room for 20 char
		spreadsheet.setColumnWidth(5, 13*256); //approx room for 20 char
		spreadsheet.setColumnWidth(6, 13*256); //approx room for 100 char
		spreadsheet.setColumnWidth(7, 13*256); //approx room for 20 char
		spreadsheet.setColumnWidth(8, 13*256); //approx room for 20 char
		spreadsheet.setColumnWidth(9, 13*256); //approx room for 20 char
		spreadsheet.setColumnWidth(10, 13*256); //approx room for 20 char
		//Create row object
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Map<String, Object> restMap = (Map) resultMap.get(currentUser.getUserId());
		List<Map<String, Object>> ledgerMapList = (List)restMap.get("ledgerMapList");
		List<Map<String, Object>> faCodeNameList = (List)restMap.get("faCodeNameList");
		Map<String, Object> ledRepMap = (Map) restMap.get("ledRepMap");
		
		XSSFRow row;
				String fromFa=String.valueOf(ledRepMap.get("fromFaCode")).substring(0,2);
					String toFa=String.valueOf(ledRepMap.get("toFaCode")).substring(0,2);
			
				if((fromFa!=null||toFa!=null)&&(!fromFa.equals("03")||!toFa.equals("03"))){
						spreadsheet.setColumnHidden(5,true);
							spreadsheet.setColumnHidden(6,true);
						}
				else{
					spreadsheet.setColumnHidden(5,false);
					spreadsheet.setColumnHidden(6,false);
				}
	    
		//Heading font
		XSSFFont fontHeading = workbook.createFont();
		fontHeading.setFontHeightInPoints((short)18);
		fontHeading.setFontName(FontFactory.HELVETICA_BOLD);
		fontHeading.setBold(true);
		fontHeading.setUnderline(XSSFFont.U_SINGLE);
		
		//Heading Style
		XSSFCellStyle styleHeading = workbook.createCellStyle();
	    styleHeading.setFont(fontHeading);
	    styleHeading.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    
	    //Heading row
	    int rowId = 0;
	    row = spreadsheet.createRow(rowId++);
	    XSSFCell cellHeading = row.createCell(0);
    	cellHeading.setCellStyle(styleHeading);
    	cellHeading.setCellValue("LEDGER REPORT");
	    spreadsheet.addMergedRegion(new CellRangeAddress(0,0,0,10));
		
	    //blank row
	    rowId++;
	    
	    //*****************************************Main Header Start************************************//
	    //MainHeader font
  		XSSFFont fontMainHeader = workbook.createFont();
  		fontMainHeader.setFontHeightInPoints((short)11);
  		fontMainHeader.setColor(IndexedColors.WHITE.getIndex());
  		fontMainHeader.setFontName(FontFactory.HELVETICA);
  		fontMainHeader.setBold(true);
  		
  		//MainHeader Style
  		XSSFCellStyle styleMainHeader = workbook.createCellStyle();
  		styleMainHeader.setFont(fontMainHeader);
  		styleMainHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleMainHeader.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
  		styleMainHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleMainHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleMainHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleMainHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleMainHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
	    
		row = spreadsheet.createRow(rowId++);
		
		for (int i = 0; i <= 7; i++) {
			XSSFCell cellFrmDt = row.createCell(i);
			cellFrmDt.setCellStyle(styleMainHeader);
	        if (i == 0) {
	        	cellFrmDt.setCellValue("Date From: "+CodePatternService.getFormatedDateString(String.valueOf(ledRepMap.get("fromDt")))+
	        			" To: "+CodePatternService.getFormatedDateString((String.valueOf(ledRepMap.get("toDt")))));
	        } 
	    }
		spreadsheet.addMergedRegion(new CellRangeAddress(2,2,0,7));// 3 cell (0,1,2)
		
		for (int i = 8; i <= 10; i++) {
			XSSFCell cellToFa = row.createCell(i);
			cellToFa.setCellStyle(styleMainHeader);
			if (i==8) {
				cellToFa.setCellValue("A/C Code From: "+ledRepMap.get("fromFaCode")+
						" To: "+ledRepMap.get("toFaCode"));
			}
		}
		spreadsheet.addMergedRegion(new CellRangeAddress(2,2,8,10));// 2 cell(6,7)
	
		//*****************************************Main Header End************************************//

		//Header font
  		XSSFFont fontHeader = workbook.createFont();
  		fontHeader.setFontHeightInPoints((short)11);
  		fontHeader.setColor(IndexedColors.WHITE.getIndex());
  		fontHeader.setFontName(FontFactory.COURIER_BOLD);
  		fontHeader.setBold(true);
		
  		//Header style
  		XSSFCellStyle styleHeader = workbook.createCellStyle();
  		styleHeader.setFont(fontHeader);
  		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
  		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		
  		//subHeader font
  		XSSFFont fontSubHeader = workbook.createFont();
  		fontSubHeader.setFontHeightInPoints((short)10);
  		fontSubHeader.setColor(IndexedColors.BLUE.getIndex());
  		fontSubHeader.setFontName(FontFactory.TIMES);
  		fontSubHeader.setBold(true);
		
  		//subHeader style
  		XSSFCellStyle styleSubHeader = workbook.createCellStyle();
  		styleSubHeader.setFont(fontSubHeader);
  		styleSubHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleSubHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleSubHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleSubHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		//subHeader style
  		XSSFCellStyle styleSubHeaderNumber = workbook.createCellStyle();
  		styleSubHeaderNumber.setFont(fontSubHeader);
  		styleSubHeaderNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleSubHeaderNumber.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleSubHeaderNumber.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleSubHeaderNumber.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		
  		//table font
  		XSSFFont font = workbook.createFont();
  		font.setFontHeightInPoints((short)10);
  		font.setColor(IndexedColors.BLACK.getIndex());
  		font.setFontName(FontFactory.TIMES);
  		
  		//table style
  		XSSFCellStyle style = workbook.createCellStyle();
  		style.setFont(font);
  		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
  		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
  		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
  		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
  		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
  		style.setWrapText(true);
  		
  		//table style number
  		XSSFCellStyle styleNumber = workbook.createCellStyle();
  		styleNumber.setFont(font);
  		styleNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleNumber.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
  		styleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		
  		//subHeader font
  		XSSFFont fontBalance = workbook.createFont();
  		fontBalance.setFontHeightInPoints((short)10);
  		fontBalance.setColor(IndexedColors.BLACK.getIndex());
  		fontBalance.setFontName(FontFactory.TIMES);
  		fontBalance.setBold(true);
		
  		//subHeader styleBalance
  		XSSFCellStyle styleBalance = workbook.createCellStyle();
  		styleBalance.setFont(fontBalance);
  		styleBalance.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleBalance.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleBalance.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleBalance.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalance.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalance.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalance.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		//subHeader styleBalanceNumber
  		XSSFCellStyle styleBalanceNumber = workbook.createCellStyle();
  		styleBalanceNumber.setFont(fontBalance);
  		styleBalanceNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleBalanceNumber.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleBalanceNumber.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleBalanceNumber.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalanceNumber.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalanceNumber.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalanceNumber.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		styleBalanceNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		
  		for (Map<String, Object> faCodeNameMap : faCodeNameList) {
  			//header row
  			row = spreadsheet.createRow(rowId++);
  			for (int i = 0; i <= 7; i++) {
  				XSSFCell cellFaCode = row.createCell(i);
  				cellFaCode.setCellStyle(styleHeader);
  		        if (i == 0) {
  		        	cellFaCode.setCellValue("A/C Code: "+faCodeNameMap.get("csFaCode"));
  		        } 
  		    }
  			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,7));
  	  		
  			for (int i = 8; i <= 10; i++) {
  				XSSFCell cellFaName = row.createCell(i);
  				cellFaName.setCellStyle(styleHeader);
  		        if (i == 8) {
  		        	cellFaName.setCellValue("A/C HEAD: "+faCodeNameMap.get("csFaName"));
  		        } 
  		    }
  			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,8,10));
  			
  			//subheader row
  			row = spreadsheet.createRow(rowId++);
  			
			XSSFCell cellBcd = row.createCell(0);
			cellBcd.setCellStyle(styleSubHeader);
			cellBcd.setCellValue("Bcd");
			
	       	XSSFCell cellDate = row.createCell(1);
	       	cellDate.setCellStyle(styleSubHeader);
	       	cellDate.setCellValue("Date");
	       	
	       	XSSFCell cellVNo = row.createCell(2);
	       	cellVNo.setCellStyle(styleSubHeader);
	       	cellVNo.setCellValue("VNo");	       	
	       	
	       	XSSFCell cellTvNo = row.createCell(3);
	       	cellTvNo.setCellStyle(styleSubHeader);
	       	cellTvNo.setCellValue("TvNo");
	       	
	       	XSSFCell cellType = row.createCell(4);
	       	cellType.setCellStyle(styleSubHeader);
	       	cellType.setCellValue("Type");
	       	
	       	XSSFCell cellVType = row.createCell(5);
	       	cellVType.setCellStyle(styleSubHeader);
	       	cellVType.setCellValue("Payment Mode");
	       	
	       	XSSFCell cellChqNo = row.createCell(6);
	       	cellChqNo.setCellStyle(styleSubHeader);
	       	cellChqNo.setCellValue("ChqNo/RTGS Ref.No");
	       	
	       	
	       	
	       	XSSFCell cellPerticular = row.createCell(7);
	       	cellPerticular.setCellStyle(styleSubHeader);
	       	cellPerticular.setCellValue("Particular");
	       	
	       	XSSFCell cellDr = row.createCell(8);
	       	cellDr.setCellStyle(styleSubHeader);
	       	cellDr.setCellValue("Debit");
	       	
	       	XSSFCell cellCr = row.createCell(9);
	       	cellCr.setCellStyle(styleSubHeader);
	       	cellCr.setCellValue("Credit");
	       	
	       	XSSFCell cellBalance = row.createCell(10);
	       	cellBalance.setCellStyle(styleSubHeader);
	       	cellBalance.setCellValue("Balance");
	       	
	       	double openingBal = 0.0;
	       	
	       	//opening balance
	       	row = spreadsheet.createRow(rowId++);
	       	row.setHeight((short)-1);  
  			for (int i = 0; i <= 7; i++) {
  				XSSFCell cellOpeningBalance = row.createCell(i);
  				cellOpeningBalance.setCellStyle(styleBalance);
  		        if (i == 0) {
  		        	cellOpeningBalance.setCellValue("Opening Balance");
  		        } 
  		    }  			
  			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,7));			
  			if (String.valueOf(faCodeNameMap.get("csDrCrOpeningBalance")).equalsIgnoreCase("D")) {
  				logger.info(" DR : "+Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtOpeningBalance"))));
  			}else if(String.valueOf(faCodeNameMap.get("csDrCrOpeningBalance")).equalsIgnoreCase("C")){
  				logger.info(" CR : "+Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtOpeningBalance"))));
  			}
  			if (String.valueOf(faCodeNameMap.get("csDrCrOpeningBalance")).equalsIgnoreCase("D")) {
  				XSSFCell cellBalanceDr = row.createCell(8);
  	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
  	  			cellBalanceDr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtOpeningBalance"))));//opening Dr balance
  	  			
	  	  		XSSFCell cellBalanceCr = row.createCell(9);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
	  			
	  		} else if (String.valueOf(faCodeNameMap.get("csDrCrOpeningBalance")).equalsIgnoreCase("C")) {
	  	  		XSSFCell cellBalanceDr = row.createCell(8);
	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
	  			
	  	  		XSSFCell cellBalanceCr = row.createCell(9);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
	  			cellBalanceCr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtOpeningBalance"))));//opening Cr balance
	  		} else {
		  	  	XSSFCell cellBalanceDr = row.createCell(8);
	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
	  			
	  	  		XSSFCell cellBalanceCr = row.createCell(9);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
	  		}
  			
  			if (String.valueOf(faCodeNameMap.get("csDrCrOpeningBalance")).equalsIgnoreCase("D")) {
  				if (faCodeNameMap.get("csAmtOpeningBalance") != null) {
	  				openingBal = Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtOpeningBalance")));
				}
  				XSSFCell cellBalanceDr = row.createCell(10);
  	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
  	  			cellBalanceDr.setCellValue(openingBal);//opening Dr balance
  	  			
	  	  	} else if (String.valueOf(faCodeNameMap.get("csDrCrOpeningBalance")).equalsIgnoreCase("C")) {
	  	  		if (faCodeNameMap.get("csAmtOpeningBalance") != null) {
	  	  			openingBal = -(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtOpeningBalance"))));
	  	  		}
	  	  		XSSFCell cellBalanceCr = row.createCell(10);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
	  			cellBalanceCr.setCellValue(openingBal);//opening Cr balance
	  			
	  	  	} else {
	  	  		XSSFCell cellBal = row.createCell(10);
	  	  		cellBal.setCellStyle(styleBalanceNumber);
			}
	       	
	       	for (Map<String, Object> ledgerMap : ledgerMapList) {
	       		if (String.valueOf(faCodeNameMap.get("csFaCode")).equalsIgnoreCase(String.valueOf(ledgerMap.get("csFaCode")))) {
	       			 
	       			row = spreadsheet.createRow(rowId++);
	       			row.setHeight((short)-1);  
	       			
	       			//Bcd
	       			XSSFCell cellBcdVal = row.createCell(0);
	       			cellBcdVal.setCellStyle(style);
	       			cellBcdVal.setCellValue(String.valueOf(ledgerMap.get("branchId")));
	    	       	
	    	       	//Date
	    	       	XSSFCell cellDateVal = row.createCell(1);
	    	       	cellDateVal.setCellStyle(style);
	    	       	cellDateVal.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(ledgerMap.get("csDt"))));
	    	       	
	    	       	//Vno
	    	       	XSSFCell cellVnoVal = row.createCell(2);
	    	       	cellVnoVal.setCellStyle(style);
	    	       	cellVnoVal.setCellValue(String.valueOf(ledgerMap.get("csVouchNo")));
	    	       	
	    	       	//TvNo
	    	       	XSSFCell cellTvNoVal = row.createCell(3);
	    	       	cellTvNoVal.setCellStyle(style);
	    	       	cellTvNoVal.setCellValue(String.valueOf(ledgerMap.get("csTvNo")));	    	      
	    	       	
	    	       	//VType
	    	       	XSSFCell cellVTypeVal = row.createCell(4);
	    	       	cellVTypeVal.setCellStyle(style);
	    	       	cellVTypeVal.setCellValue(String.valueOf(ledgerMap.get("csVouchType")).toLowerCase());
	    	       	
	    	       	//VType
	    	       	XSSFCell paymentModeVal = row.createCell(5);
	    	       	paymentModeVal.setCellStyle(style);
	    	       	paymentModeVal.setCellValue(String.valueOf(ledgerMap.get("csPayMode")).toLowerCase());
	    	       	
	    	     	//TvNo
	    	       	XSSFCell cellChqNoVal = row.createCell(6);
	    	       	cellChqNoVal.setCellStyle(style);
	    	       	cellChqNoVal.setCellValue(String.valueOf(ledgerMap.get("chq")));
	    	       	
	    	       	//Perticular
	    	       	XSSFCell cellPerticularVal = row.createCell(7);
	    	       	cellPerticularVal.setCellStyle(style);
	    	       	cellPerticularVal.setCellValue(String.valueOf(ledgerMap.get("csDescription")));
	    	       	
	    	       	//Debit
	    	       	if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("D")) {
	    	       		XSSFCell cellDrVal = row.createCell(8);
	    	       		cellDrVal.setCellStyle(styleNumber);
	    	       		cellDrVal.setCellValue(Double.parseDouble(String.valueOf((ledgerMap.get("csAmt")))));
		    	       	
		    	       	XSSFCell cellCrVal = row.createCell(9);
		    	       	cellCrVal.setCellStyle(styleNumber);
		    	       	
		    	       	openingBal = openingBal + Double.parseDouble(String.valueOf((ledgerMap.get("csAmt"))));
		    	       	
		    	       	XSSFCell cellRunningBalanceDr = row.createCell(10);
		    	       	cellRunningBalanceDr.setCellStyle(styleNumber);
		    	       	cellRunningBalanceDr.setCellValue(openingBal);
		    	       	faCodeNameMap.put("csAmtBalance", String.valueOf(openingBal));
		    	       	
	    	       	} else if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("C")){ //Credit
	    	       		XSSFCell cellDrVal = row.createCell(8);
	    	       		cellDrVal.setCellStyle(styleNumber);
	    	       		
	    	       		XSSFCell cellCrVal = row.createCell(9);
	    	       		cellCrVal.setCellStyle(styleNumber);
	    	       		cellCrVal.setCellValue(Double.parseDouble(String.valueOf(ledgerMap.get("csAmt"))));
	    	       		
	    	       		openingBal = openingBal - Double.parseDouble(String.valueOf((ledgerMap.get("csAmt"))));
	    	       		
	    	       		XSSFCell cellRunningBalanceCr = row.createCell(10);
	    	       		cellRunningBalanceCr.setCellStyle(styleNumber);
	    	       		cellRunningBalanceCr.setCellValue(openingBal);
	    	       		faCodeNameMap.put("csAmtBalance", String.valueOf(openingBal));
	    	       	} else {
	    	       		XSSFCell cellDrVal = row.createCell(8);
	    	       		cellDrVal.setCellStyle(styleNumber);
		    	       	XSSFCell cellCrVal = row.createCell(9);
		    	       	cellCrVal.setCellStyle(styleNumber);
					}
	    	    }
	       	}
	       	
	       	//closing balance
	       	row = spreadsheet.createRow(rowId++);
	       	row.setHeight((short)-1);  
  			for (int i = 0; i <= 7; i++) {
  				XSSFCell cellClosingBalance = row.createCell(i);
  				cellClosingBalance.setCellStyle(styleBalance);
  		        if (i == 0) {
  		        	cellClosingBalance.setCellValue("Closing Balance");
  		        } 
  		    }
  			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,7));  			
	       	
  			if (Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance")))<0) {
  				XSSFCell cellBalanceDr = row.createCell(8);
  	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
  	  			cellBalanceDr.setCellValue(-Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
  	  			
  	  			XSSFCell cellBalanceCr = row.createCell(9);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
	  	  	} else if (Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance")))>0) {
	  	  		XSSFCell cellBalanceDr = row.createCell(8);
	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
	  	  		XSSFCell cellBalanceCr = row.createCell(9);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
	  			cellBalanceCr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
	  	  	} else {
		  	  	XSSFCell cellBalanceDr = row.createCell(8);
	  			cellBalanceDr.setCellStyle(styleBalanceNumber);
	  	  		XSSFCell cellBalanceCr = row.createCell(9);
	  			cellBalanceCr.setCellStyle(styleBalanceNumber);
			}
  			
  			XSSFCell cellCloseRunningBalance = row.createCell(10);
  			cellCloseRunningBalance.setCellStyle(styleBalanceNumber);
  			
  			row = spreadsheet.createRow(rowId++);
  			row.setHeight((short)-1);  
  			for (int i = 0; i <= 7; i++) {
  				XSSFCell cellTotal = row.createCell(i);
  				cellTotal.setCellStyle(styleSubHeader);
  		        if (i == 0) {
  		        	cellTotal.setCellValue("Total");
  		        } 
  		    }
  			
  			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,7));
  			
  			logger.info("Total Balance: "+faCodeNameMap.get("csAmtBalance"));
  			
  			if (Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance")))<0) {
  				
  				//change -ve sign
  	  			faCodeNameMap.put("csAmtBalance", String.valueOf(-Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance")))));
  				
  	  			logger.info("if csAmtTotal: "+ String.valueOf(faCodeNameMap.get("csAmtTotal"))+"\tcsAmtBalance: "+ Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
  	  			
  				//add closing balance
  				XSSFCell cellTotalDr = row.createCell(8);
  	  			cellTotalDr.setCellStyle(styleSubHeaderNumber);
  	  			if (faCodeNameMap.get("csAmtTotal") == null) {
  	  				cellTotalDr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
				}else {
					cellTotalDr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtTotal"))) + Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
				}
  	  			
  	  			
  	  	  		XSSFCell cellTotalCr = row.createCell(9);
  	  	  		cellTotalCr.setCellStyle(styleSubHeaderNumber);
  	  	  		if (faCodeNameMap.get("csAmtTotal") == null) {
  	  	  			cellTotalCr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
				} else {
					cellTotalCr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtTotal"))) + Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
				}
  	  	  		
  	  	  		
  	  	  		XSSFCell cellRunningBalanceTotalCr = row.createCell(10);
  	  	  		cellRunningBalanceTotalCr.setCellStyle(styleSubHeaderNumber);
			} else {
				
				logger.info("if csAmtTotal: "+ String.valueOf(faCodeNameMap.get("csAmtTotal"))+"\tcsAmtBalance: "+ Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtBalance"))));
				
				//add closing balance
  				XSSFCell cellTotalDr = row.createCell(8);
  	  			cellTotalDr.setCellStyle(styleSubHeaderNumber);
  	  			if (faCodeNameMap.get("csAmtTotal") != null) {
  	  				cellTotalDr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtTotal"))));
				}
  	  			
  	  	  		XSSFCell cellTotalCr = row.createCell(9);
  	  	  		cellTotalCr.setCellStyle(styleSubHeaderNumber);
  	  	  		if (faCodeNameMap.get("csAmtTotal") != null) {
  	  	  			cellTotalCr.setCellValue(Double.parseDouble(String.valueOf(faCodeNameMap.get("csAmtTotal"))));
				}
  	  	  		XSSFCell cellRunningBalanceTotalCr = row.createCell(10);
  	  	  		cellRunningBalanceTotalCr.setCellStyle(styleSubHeaderNumber);
			}
  	  		
  	  		row = spreadsheet.createRow(rowId++);
			for (int i = 0; i <= 10; i++) {
				XSSFCell cellTemp = row.createCell(i);
				cellTemp.setCellStyle(style);
		        if (i == 0) {
		        	cellTemp.setCellValue("*****************************************");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,10));
		}
  		
		response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=ledger.xlsx");
		ServletOutputStream out = response.getOutputStream();
		workbook.write(out);
        out.flush();
        out.close();

        
		logger.info("Writesheet.xlsx written successfully" );
	}

	private Map<String, Map<String, Object>> calBalanceForLedger (Map<String, Object> ledgerReportMap){
		logger.info("LedgerCntlr.calBalanceForLedger()");
		Map<String, Map<String, Object>> openingBalMap = new HashMap<>();
		
		String fromDtTemp = String.valueOf(ledgerReportMap.get("fromDt"));
		String toDtTemp = String.valueOf(ledgerReportMap.get("toDt"));
		
		List<Map<String, Object>> ldgrMapList = new ArrayList<>();
		
		logger.info("from date: "+ConstantsValues.CS_BEGIN_DT);
		logger.info("to date: "+ledgerReportMap.get("fromDt"));
		//change from date and to date
		Date toDate = CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")));
		Calendar c = Calendar.getInstance();
		c.setTime(toDate);
		c.add(Calendar.DATE, -1);
		toDate = new Date(c.getTime().getTime());
		logger.info("to date after change: "+toDate);
		
		ledgerReportMap.put("fromDt", ConstantsValues.CS_BEGIN_DT);
		ledgerReportMap.put("toDt", toDate);
		
		if ((boolean) ledgerReportMap.get("isBranch")) {
			ldgrMapList = ledgeDAO.getBrLedger(ledgerReportMap);
		}else if ((boolean) ledgerReportMap.get("isConsolidate")) {
			ldgrMapList = ledgeDAO.getConsolidateLedger(ledgerReportMap);
		}
		
		logger.info("ledgrMapList.size(): "+ldgrMapList.size());
		
		if(!ldgrMapList.isEmpty()){
			//create list of hashMap which having unique fa codes
			Map<Object, Object> faCodeNameMap = new HashMap<>();
			
			List<String> faCode = new ArrayList<>();
			for (Map<String, Object> ledgerMap : ldgrMapList) {
				faCode.add(String.valueOf(ledgerMap.get("csFaCode")));
				if (!ledgerMap.containsKey(ledgerMap.get("csFaCode"))) {
					faCodeNameMap.put(ledgerMap.get("csFaCode"), ledgerMap.get("csFaName"));
				}
			}
					
			Set<String> hs = new HashSet<>();
			hs.addAll(faCode);
			faCode.clear();
			faCode.addAll(hs);
			
			Collections.sort(faCode);
			
			for (int i = 0; i < faCode.size(); i++) {
				Map<String, Object> tempMap = new HashMap<>();
				
				double totalDr = 0.00;
				double totalCr = 0.00;
				double balance = 0.00;
				String balanceType = null;
				
				for (Map<String, Object> ledgerMap : ldgrMapList) {
					 if (faCode.get(i).equalsIgnoreCase(String.valueOf(ledgerMap.get("csFaCode")))) {
						if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("D")) {
							totalDr = totalDr+Double.parseDouble(String.valueOf(ledgerMap.get("csAmt")));
						} else if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("C")) {
							totalCr = totalCr+Double.parseDouble(String.valueOf(ledgerMap.get("csAmt")));
						}
					} 
				}
				
				logger.info("totalDr: ===========>>>"+totalDr);
				logger.info("totalCr: ===========>>>"+totalCr);
				
				if (totalDr > totalCr) {
					balance = totalDr - totalCr;
					balanceType = "D";
				} else if (totalDr < totalCr) {
					balance = totalCr - totalDr;
					balanceType = "C";
				} else if (totalDr == totalCr) {
					balance = totalCr - totalDr;
					balanceType = "equal";
				}
				
				tempMap.put("csAmtOpeningBalance", balance);
				tempMap.put("csDrCrOpeningBalance", balanceType);
				openingBalMap.put(faCode.get(i), tempMap);
			}
		}
		//put orignal date to map
		ledgerReportMap.put("fromDt", fromDtTemp);
		ledgerReportMap.put("toDt", toDtTemp);
		return openingBalMap;
	}
	
	@RequestMapping(value = "/getFaCodeFrLedger", method = RequestMethod.POST)
	public @ResponseBody Object getFaCodeFrLedger(@RequestBody String faCode) {
		logger.info("LedgerCntlr.getFaCodeFrLedger()");
		Map<String, Object> map = new HashMap<>();
		List<Object> list=(List<Object>) faMasterDAO.getFaCodeFrLdr(faCode);
		logger.info(list.size());
		if(!list.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		map.put("codeList", list);
		return map;
	}
}