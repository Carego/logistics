package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Branch;
import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.Employee;
import com.mylogistics.model.SubElectricityMstr;

@Repository
public interface ElectricityMstrDAO {

	public int saveElecMstr(Branch branch , Employee employee , ElectricityMstr electricityMstr);
	
	public List<ElectricityMstr> getAllElectMstr();
	
	public int updateEMBySEM(int emId , SubElectricityMstr subElectricityMstr);
}
