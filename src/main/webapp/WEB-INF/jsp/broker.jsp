<div ng-show="operatorLogin || superAdminLogin">
<title> Broker </title>


<div class="row">
	<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="brokerPanForm" ng-submit="verifyPan(brokerPanForm,panNo)">
		<div class="row">
      			<div class="input-field col s3">
        			<input id="panNoId" type ="text" name ="panNoName" ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/" ng-model="panNo" ng-change="panNoChange(panNo)" required >
        			<label>Pan No.</label>	
      			</div>
      			
      				<div class="col s3 center">
      			 		<input type="submit" value="VerifyPanNo">
      			 	</div>
      		</div>
	</form>
</div>

<div id ="existBrokerId" ng-hide = "existBrokerDB">
	
	<input type="text" name="filterExistBroker" ng-model="filterExistBroker" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Broker Code</th>
				<th>Broker Name</th>
				<th>Broker Pan Name</th>
			</tr>
 	  
		  <tr ng-repeat="brk in brkList  | filter:filterExistBroker">
		 	  <td><input type="radio"  name="brokerCode" id="brokerCode" class="brokerCode"  value="{{ brk }}" ng-model="brkCode" ng-click="editExistBroker(brk)"></td>
              <td>{{ brk.brkCode }}</td>
              <td>{{ brk.brkName }}</td>
              <td>{{ brk.brkPanName}}</td>
          </tr>
         </table>       
	</div>
 <div id="brokerDetailId" ng-show="newBrokerDetailDB">
<div class="row">			
		
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="BrokerForm" ng-submit="saveBroker(BrokerForm,add,broker)">	
			
			<div class="row">
			
			<div class="input-field col s3">
        			<input id="brkBcodeNameId" type ="text" name ="brkBcodeName"  ng-model="broker.bCode" ng-click="getBranch()" readonly required>
        			<label>Branch</label>	
      			</div>
      			<div class="input-field col s3">
        			<input class="validate" id="brkPanNameId" type ="text" name ="brkPanName" ng-model="broker.brkPanName"  required >
        			<label >PAN Name</label>	
      			</div>
      			<div class="input-field col s3">
        			<input class="validate" id="brkFirmName" type ="text" name ="brkFirmName" ng-model="broker.brkFirmName" ng-minlength="3" ng-maxlength="255" ng-required="true">
        			<label for="BrokerName">Firm Name</label>	
      			</div>
      			
      			<div class="input-field col s3">
	        		<select name="brkFirmType" id="brkFirmType" ng-model="broker.brkFirmType" required>
	                    <option value="Proprietor">Proprietor</option>
	                    <option value="Partner">Partner</option>
	                    <option value="Pvt Ltd">Pvt Ltd</option>
	                    <option value="Ltd">Limited</option>
	                </select>
	                <label>Firm Type</label>	
      			</div>
      			
      			
      		</div>
      		
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="completeAddId" type ="text" name ="completeAddName"  ng-model="add.completeAdd" required >
        			<label>Building No./Plot No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addPinId" type ="text" name ="addPinName"  ng-model="add.addPin" ng-keyup="getStnByPin()" minlength="6" maxlength="6"  required>
        			<label>Pin Code:</label>
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addStateId" type ="text" name ="addStateName"  ng-model="add.addState" ng-click="getState()" readonly required>
        			<label>State</label>	
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="districtId" type ="text" name ="districtName"  ng-model="add.addDist" ng-click="getState()" readonly required >
        			<label>District:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addCityId" type ="text" name ="addCityName"  ng-model="add.addCity" ng-click="getState()" readonly required >
        			<label>City/Town:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="locationId" type ="text" name ="locationName"  ng-model="add.addPost" ng-click="getState()" readonly required >
        			<label>Post Office:</label>
      			</div>
      			</div>
      		
      		<div class="row">
      			
      			<div class="input-field col s3">
        			<input class="validate" id="brkNameId" type ="text" name ="brkName" ng-model="broker.brkName" required>
        			<label for="union">Broker Name</label>	
      			</div>
      			<div class="input-field col s3">
        			<input class="validate" id="phNoId" type ="number" name ="phNoName" ng-model="phNo" ng-minlength="10" ng-maxlength="10" required>
        			<label for="phNoId">Broker Mobile No</label>	
      			</div>
      			<div class="input-field col s3">
        			<input class="validate" id="emailid" type ="email" name ="brkEmailId" ng-model="broker.brkEmailId"  ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" >
        			<label for="emailid">Email ID</label>
        				
      			</div>
      			
        		
      		</div>
      		
      		
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="brkAccntNoId" type ="text" name ="brkAccntNoName"  ng-model="broker.brkAccntNo" required>
        			<label>Account No:</label>	
      			</div>
      			
      			
      			<div class="input-field col s3">
        			<input id="accountHldrNameId" type ="text" name ="accountHldrName"  ng-model="broker.brkAcntHldrName" required >
        			<label>Account Holder Name</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="brkIfscId" type ="text" name ="brkIfscName"  ng-model="broker.brkIfsc" pattern="^[A-Z]{4}0[A-Z0-9]{6}$" required >
        			<label>IFSC Code</label>
      			</div>
      			<div class="input-field col s3">
        			<input id="brkBnkBranchId" type ="text" name ="brkBnkBranchName"  ng-model="broker.brkBnkBranch" required >
        			<label>Bank Name</label>
      			</div>
      			</div>
      		
      		
      		<div class="row">
      		
      		<div class="col s3 center">
      			 		<input type="button" value="Upload Documents" id="upldDocId" ng-click="openUploadDB()">
      			 	</div>
      			
      			<input type="submit" value="Save" id="saveBtnId" ng-click=""> 
      			
      			
      			</div>
      			
      		</form>
      		</div>
		
		
		   	<div class="row" id="docUploadDBId" ng-show="docUpldFlag">
<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="docUploadForm" ng-submit="uploadImage(docUploadForm)">
      			
      			
      			
      			<div class="row" >
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="panImg">
						<label>Choose File for PAN card:</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadPanImg()">Upload</a>
					</div>
				</div>
				
				<div class="row" >
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="decImg">
						<label>Choose File for Declaration</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadDecImg()">Upload</a>
					</div>
				</div>
				
				
				<div class="row" >
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="ccImage">
						<label>Choose File for Cancelled cheque</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCCImage(ccImage)">Upload</a>
					</div>
				</div>
				
				
				<div class="row">
      				<div class="col s3 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
</form>
</div>
		
		
		
		</div>
		
		
		
		
		
	<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnName">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.pinCode }}</td>
				<td>{{ stn.stationName }}</td>
				<td>{{ stn.city }}</td>
				<td>{{ stn.district }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
		
		<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

		<input type="text" name="filterBranchCode" ng-model="filterBranchCode"
			placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Branch FaCode</th>

				<th>Branch Name</th>

				<th>Branch Pin</th>
			</tr>
			<tr ng-repeat="branch in branchList | filter:filterBranchCode">
				<td><input type="radio" name="branchCode" id="branchId"
					value="{{ branch.branchCode }}" ng-model="branchCodeTemp"
					ng-click="saveBranchCode(branch)"></td>
				<td>{{ branch.branchFaCode }}</td>
				<td>{{ branch.branchName }}</td>
				<td>{{ branch.branchPin }}</td>
			</tr>
		</table>
	</div>
		
		
</div>