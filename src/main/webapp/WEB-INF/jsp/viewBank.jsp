<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="viewBankForm" ng-submit=viewBankSubmit(viewBankForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="col s4 input-field">
				<input class="validate" type="text" id="selectBankId" name="selectBank" ng-model="bankMstr.bnkName" ng-click="openBankDB()" readonly ng-required="true"> 
				<label for="code">Select Bank</label>
			</div>
			
			<div class="row">
				<div class="col s4 input-field">
					<input class="validate" type="submit" value="submit" >
				</div>
			</div>
		</form>
	</div>
	
	<div id ="bankDB" ng-hide="bankDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Bank Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Name </th>
 	  	  	  <th> Bank FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div class="row">
	<div class="col s3 hidden-xs hidden-sm">&nbsp;</div>
	<div ng-hide="viewBankFlag" class="col s6 card"	style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<h4>
			Here's the detail of bank
		</h4>
		<table class="table-hover table-bordered table-condensed">
			<tr>
				<td>Name:</td>
				<td>{{bankMstr.bnkName}}</td>
			</tr>
			<tr>
				<td>FaCode:</td>
				<td>{{bankMstr.bnkFaCode}}</td>
			</tr>
			<tr>
				<td>Address:</td>
				<td>{{bankMstr.address.completeAdd}}</td>
			</tr>

			<tr>
				<td>City:</td>
				<td>{{bankMstr.address.addCity}}</td>
			</tr>

			<tr>
				<td>State:</td>
				<td>{{bankMstr.address.addState}}</td>
			</tr>

			<tr>
				<td>Pin:</td>
				<td>{{bankMstr.address.addPin}}</td>
			</tr>

			<tr>
				<td>Account No:</td>
				<td>{{bankMstr.bnkAcNo}}</td>
			</tr>

			<tr>
				<td>Opening Date:</td>
				<td>{{bankMstr.bnkOpenDt}}</td>
			</tr>

			<tr>
				<td>Local:</td>
				<td>{{bankMstr.bnkLocal}}</td>
			</tr>
			
			<tr>
				<td>Branch Min Cheque:</td>
				<td>{{bankMstr.bnkBrMinChq}}</td>
			</tr>

			<tr>
				<td>Branch Max Cheque:</td>
				<td>{{bankMstr.bnkBrMaxChq}}</td>
			</tr>

			<tr>
				<td>HO Min Cheque:</td>
				<td>{{bankMstr.bnkHoMinChq}}</td>
			</tr>

			<tr>
				<td>HO Max Cheque:</td>
				<td>{{bankMstr.bnkHoMaxChq}}</td>
			</tr>

			<tr>
				<td>IFSC Code:</td>
				<td>{{bankMstr.bnkIfscCode}}</td>
			</tr>

			<tr>
				<td>MICR Code:</td>
				<td>{{bankMstr.bnkMicrCode}}</td>
			</tr>

			<tr>
				<td>Branch FaCode</td>
				<td>{{bankMstr.branch.branchFaCode}}</td>
			</tr>
			
			<tr>
				<td>Branch Name</td>
				<td>{{bankMstr.branch.branchName}}</td>
			</tr>
			
			<tr>
				<td><h5>Single Signatory</h5></td>
				<td></td>
			</tr>
			
			<tr ng-repeat="singleSList in bankMstr.bnkSingleSignatoryList">
				<td>Single Signatore {{$index+1}}:</td>
				<td>{{singleSList.empName}}</td>
			</tr>
			
			<tr>
				<td><h5>Joint Signatory</h5></td>
				<td></td>
			</tr>
			
			<tr ng-repeat="jointSList in bankMstr.bnkJointSignatoryList">
				<td>Single Signatore {{$index+1}}:</td>
				<td>{{jointSList.empName}}</td>
			</tr>

		</table>

	</div>

</div>

</div>