'use strict';

var app = angular.module('application');

app.controller('ChangePasswordCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.user = {};
	$scope.userList = {};
	$scope.usersDBFlag = true;
	 
	$scope.fetchUserList = function(){
		console.log("enter into fetchUserList function");
		var response = $http.get($scope.projectName+'/fetchUserList');
		response.success(function(data, status, headers, config) {
			$scope.userList = data.result;
			
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.changePassword = function(UserForm,user){
		console.log("enter into changePassword function");
		var response = $http.post($scope.projectName+'/changePassword',user);
		response.success(function(data, status, headers, config) {
			console.log(data.result);
			if(data.result==="success"){
				$scope.successToast("Password Changed Successfully");
				user.userId="";
				user.password="";
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.openUsersDB = function(){
		 console.log("enter into openUsersDB funciton");
		 $scope.usersDBFlag = false;
	    	$('div#usersDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.usersDBFlag = true;
			    }
				});
			$('div#usersDB').dialog('open'); 
	}
	
	$scope.selectedUser=function(user){
		console.log("enter into selectedUser with--->userId=="+user.userId);
		$scope.user.userId=user.userId;
			$scope.usersDBFlag = true;
		$('div#usersDB').dialog('close'); 
	}
	
}]);