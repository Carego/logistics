package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;
//import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "arrivalreport")
public class ArrivalReport {
	
	@Id
	@Column(name="arId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int arId;
	
	@Column(name="arCode")
	private String arCode;
	
	@Column(name="arDt")
	private Date arDt;
	
	@Column(name="arRcvWt")
	private int arRcvWt;
	
	@Column(name="arPkg")
	private int arPkg;
	
	@Column(name="arUnloading" , columnDefinition="double default 0.0")
	private double arUnloading;
	
	@Column(name="arRemUnloading" , columnDefinition="double default 0.0")
	private double arRemUnloading;
	
	@Column(name="arClaim" , columnDefinition="double default 0.0")
	private double arClaim;
	
	/*@Column(name="arRemClaim" , columnDefinition="double default 0.0")
	private double arRemClaim;*/
	
	@Column(name="arDetention" , columnDefinition="double default 0.0")
	private double arDetention;
	
	@Column(name="arRemDetention" , columnDefinition="double default 0.0")
	private double arRemDetention;
	
	@Column(name="arOther" , columnDefinition="double default 0.0")
	private  double arOther;

	@Column(name="arLateDelivery" , columnDefinition="double default 0.0")
	private double arLateDelivery;
	
	@Column(name="arRemLateDelivery" , columnDefinition="double default 0.0")
	private double arRemLateDelivery;
	
	@Column(name="arLateAck" , columnDefinition="double default 0.0")
	private double arLateAck;
	
	@Column(name="arRemLateAck" , columnDefinition="double default 0.0")
	private double arRemLateAck;
	
	@Column(name="arRepDt")
	private Date arRepDt;
	
	@Column(name="arIssueDt")
	private Date arIssueDt;
	
	@Column(name="arRepTime")
	private String arRepTime;
	
	/*@Column(name="arWtSrtCnmt")
	private ArrayList<Map<String,Object>> arWtSrtCnmt = new ArrayList<>();*/
	
	@Column(name="arIsHOAlw" , columnDefinition="boolean default true")
	private boolean arIsHOAlw;
	
	@Column(name="userCode")
	private String userCode;
	    
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="archlnCode")
	private String archlnCode;
	
	/*@Column(name="arImage")
	private Blob arImage;*/
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="arRepType")
	private String arRepType;
	
	@Column(name="arExtKm" , columnDefinition="double default 0.0")
	private double arExtKm;
	
	@Column(name="arRemExtKm" , columnDefinition="double default 0.0")
	private double arRemExtKm;
	
	@Column(name="arOvrHgt" , columnDefinition="double default 0.0")
	private double arOvrHgt;
	
	@Column(name="arRemOvrHgt" , columnDefinition="double default 0.0")
	private double arRemOvrHgt;
	
	@Column(name="arPenalty" , columnDefinition="double default 0.0")
	private double arPenalty;
	
	@Column(name="arWtShrtg" , columnDefinition="double default 0.0")
	private double arWtShrtg;
	
	@Column(name="arRemWtShrtg" , columnDefinition="double default 0.0")
	private double arRemWtShrtg;
	
	@Column(name="arDrRcvrWt" , columnDefinition="double default 0.0")
	private double arDrRcvrWt;
	
	@Column(name="arRemDrRcvrWt" , columnDefinition="double default 0.0")
	private double arRemDrRcvrWt;

	@Column(name="arDesc")
	private String arDesc;
	
	@Column(name="isCancel" , columnDefinition="boolean default false")
	private boolean cancel;
	
	@Column(name="arSrtgDmg" , length = 10)
	private String arSrtgDmg;
	
	@Column(name="isSrtgDmgAlw" , columnDefinition="boolean default false")
	private boolean isSrtgDmgAlw;
	
	@Column(name="isValidate")
	private String isValidate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Memo memo;
	
	public String getArRepType() {
		return arRepType;
	}

	public void setArRepType(String arRepType) {
		this.arRepType = arRepType;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	/*public Blob getArImage() {
		return arImage;
	}

	public void setArImage(Blob arImage) {
		this.arImage = arImage;
	}*/

	public String getArchlnCode() {
		return archlnCode;
	}

	public void setArchlnCode(String archlnCode) {
		this.archlnCode = archlnCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public int getArId() {
		return arId;
	}

	public void setArId(int arId) {
		this.arId = arId;
	}

	public String getArCode() {
		return arCode;
	}

	public void setArCode(String arCode) {
		this.arCode = arCode;
	}

	public Date getArDt() {
		return arDt;
	}

	public void setArDt(Date arDt) {
		this.arDt = arDt;
	}

	public int getArRcvWt() {
		return arRcvWt;
	}

	public void setArRcvWt(int arRcvWt) {
		this.arRcvWt = arRcvWt;
	}

	public int getArPkg() {
		return arPkg;
	}

	public void setArPkg(int arPkg) {
		this.arPkg = arPkg;
	}

	public double getArUnloading() {
		return arUnloading;
	}

	public void setArUnloading(double arUnloading) {
		this.arUnloading = arUnloading;
	}

	public double getArClaim() {
		return arClaim;
	}

	public void setArClaim(double arClaim) {
		this.arClaim = arClaim;
	}

	public double getArDetention() {
		return arDetention;
	}

	public void setArDetention(double arDetention) {
		this.arDetention = arDetention;
	}

	public double getArOther() {
		return arOther;
	}

	public void setArOther(double arOther) {
		this.arOther = arOther;
	}

	public double getArLateDelivery() {
		return arLateDelivery;
	}

	public void setArLateDelivery(double arLateDelivery) {
		this.arLateDelivery = arLateDelivery;
	}

	public double getArLateAck() {
		return arLateAck;
	}

	public void setArLateAck(double arLateAck) {
		this.arLateAck = arLateAck;
	}

	public Date getArRepDt() {
		return arRepDt;
	}

	public void setArRepDt(Date arRepDt) {
		this.arRepDt = arRepDt;
	}

	public String getArRepTime() {
		return arRepTime;
	}

	public void setArRepTime(String arRepTime) {
		this.arRepTime = arRepTime;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public double getArExtKm() {
		return arExtKm;
	}

	public void setArExtKm(double arExtKm) {
		this.arExtKm = arExtKm;
	}

	public double getArOvrHgt() {
		return arOvrHgt;
	}

	public void setArOvrHgt(double arOvrHgt) {
		this.arOvrHgt = arOvrHgt;
	}

	public double getArPenalty() {
		return arPenalty;
	}

	public void setArPenalty(double arPenalty) {
		this.arPenalty = arPenalty;
	}

	public double getArWtShrtg() {
		return arWtShrtg;
	}

	public void setArWtShrtg(double arWtShrtg) {
		this.arWtShrtg = arWtShrtg;
	}

	public String getArDesc() {
		return arDesc;
	}

	public void setArDesc(String arDesc) {
		this.arDesc = arDesc;
	}

	public boolean isArIsHOAlw() {
		return arIsHOAlw;
	}

	public void setArIsHOAlw(boolean arIsHOAlw) {
		this.arIsHOAlw = arIsHOAlw;
	}

	public double getArDrRcvrWt() {
		return arDrRcvrWt;
	}

	public void setArDrRcvrWt(double arDrRcvrWt) {
		this.arDrRcvrWt = arDrRcvrWt;
	}

	/*public ArrayList<Map<String, Object>> getArWtSrtCnmt() {
		return arWtSrtCnmt;
	}

	public void setArWtSrtCnmt(ArrayList<Map<String, Object>> arWtSrtCnmt) {
		this.arWtSrtCnmt = arWtSrtCnmt;
	}*/
	public double getArRemExtKm() {
		return arRemExtKm;
	}

	public void setArRemExtKm(double arRemExtKm) {
		this.arRemExtKm = arRemExtKm;
	}
	

	public double getArRemOvrHgt() {
		return arRemOvrHgt;
	}

	public void setArRemOvrHgt(double arRemOvrHgt) {
		this.arRemOvrHgt = arRemOvrHgt;
	}

	public double getArRemDetention() {
		return arRemDetention;
	}

	public void setArRemDetention(double arRemDetention) {
		this.arRemDetention = arRemDetention;
	}
	
	public double getArRemUnloading() {
		return arRemUnloading;
	}

	public void setArRemUnloading(double arRemUnloading) {
		this.arRemUnloading = arRemUnloading;
	}

	public double getArRemWtShrtg() {
		return arRemWtShrtg;
	}

	public void setArRemWtShrtg(double arRemWtShrtg) {
		this.arRemWtShrtg = arRemWtShrtg;
	}
	/*public double getArRemClaim() {
		return arRemClaim;
	}

	public void setArRemClaim(double arRemClaim) {
		this.arRemClaim = arRemClaim;
	}*/

	public double getArRemLateDelivery() {
		return arRemLateDelivery;
	}

	public void setArRemLateDelivery(double arRemLateDelivery) {
		this.arRemLateDelivery = arRemLateDelivery;
	}

	public double getArRemLateAck() {
		return arRemLateAck;
	}

	public void setArRemLateAck(double arRemLateAck) {
		this.arRemLateAck = arRemLateAck;
	}

	public double getArRemDrRcvrWt() {
		return arRemDrRcvrWt;
	}

	public void setArRemDrRcvrWt(double arRemDrRcvrWt) {
		this.arRemDrRcvrWt = arRemDrRcvrWt;
	}

	public boolean isCancel() {
		return cancel;
	}

	public void setCancel(boolean cancel) {
		this.cancel = cancel;
	}

	public Date getArIssueDt() {
		return arIssueDt;
	}

	public void setArIssueDt(Date arIssueDt) {
		this.arIssueDt = arIssueDt;
	}

	public String getArSrtgDmg() {
		return arSrtgDmg;
	}

	public void setArSrtgDmg(String arSrtgDmg) {
		this.arSrtgDmg = arSrtgDmg;
	}

	public boolean isSrtgDmgAlw() {
		return isSrtgDmgAlw;
	}

	public void setSrtgDmgAlw(boolean isSrtgDmgAlw) {
		this.isSrtgDmgAlw = isSrtgDmgAlw;
	}

	public Memo getMemo() {
		return memo;
	}

	public void setMemo(Memo memo) {
		this.memo = memo;
	}

	public String getIsValidate() {
		return isValidate;
	}

	public void setIsValidate(String isValidate) {
		this.isValidate = isValidate;
	}
	
	

}
