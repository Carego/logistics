'use strict';

var app = angular.module('application');

app.controller('CancelSedrCntlr',['$scope','$location','$http',
                               function($scope,$location,$http){
	
	$scope.sedrCode="";
	$scope.sedrDt="";
	$scope.cancelSedr=function(){
		console.log("cancelSedr()");
		
		var sedrDtl={
				"sedrCode" : $scope.sedrCode,
				"date" : $scope.sedrDt
		}
		
		  var response = $http.post($scope.projectName+'/sedrDtlFrCncl',sedrDtl);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.alertToast("SEDR successfully canceled");
			  }else{
				  $scope.alertToast("Detail not Match");
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	}
		
	
}]);