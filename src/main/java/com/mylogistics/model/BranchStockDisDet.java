package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "branchstockdisdet")
public class BranchStockDisDet {
	
	@Id
	@Column(name="brsDisDetId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brsDisDetId;
	
	@Column(name="brsDisDetBrCode")
	private String brsDisDetBrCode;
	
	@Column(name="brsDisDetStatus")
	private String brsDisDetStatus;
	
	@Column(name="brsDisDetStartNo" , unique=true)
	private String brsDisDetStartNo;
	
	@Column(name="brsDisDetDisCode")
	private String brsDisDetDisCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isVerify")
	private String isVerify;
	
	@Transient
	private Boolean used;

	public String getIsVerify() {
		return isVerify;
	}

	public void setIsVerify(String isVerify) {
		this.isVerify = isVerify;
	}

	public int getBrsDisDetId() {
		return brsDisDetId;
	}

	public void setBrsDisDetId(int brsDisDetId) {
		this.brsDisDetId = brsDisDetId;
	}

	public String getBrsDisDetBrCode() {
		return brsDisDetBrCode;
	}

	public void setBrsDisDetBrCode(String brsDisDetBrCode) {
		this.brsDisDetBrCode = brsDisDetBrCode;
	}

	public String getBrsDisDetStatus() {
		return brsDisDetStatus;
	}

	public void setBrsDisDetStatus(String brsDisDetStatus) {
		this.brsDisDetStatus = brsDisDetStatus;
	}

	public String getBrsDisDetStartNo() {
		return brsDisDetStartNo;
	}

	public void setBrsDisDetStartNo(String brsDisDetStartNo) {
		this.brsDisDetStartNo = brsDisDetStartNo;
	}

	public String getBrsDisDetDisCode() {
		return brsDisDetDisCode;
	}

	public void setBrsDisDetDisCode(String brsDisDetDisCode) {
		this.brsDisDetDisCode = brsDisDetDisCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}
	
	
}
