package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

	public class BranchDAOImpl implements BranchDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	private static Logger logger = Logger.getLogger(BranchDAOImpl.class);
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public BranchDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
		
		
	@Transactional
	public long totalCount(){
		long num=-1;
		try{
		    session = this.sessionFactory.openSession();
	        num = (Long) session.createCriteria(Branch.class).setProjection(Projections.rowCount()).uniqueResult();
	        session.flush();
		 	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  num;
    }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Branch> getLastBranch(){
		 List<Branch> last = new ArrayList<Branch>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 last = session.createQuery("from Branch order by branchId DESC ").setMaxResults(1).list();
			 session.flush();
			 
		 }
		  catch(Exception e){
			 e.printStackTrace();
		  }
		 session.clear();
		 session.close();
		 return last;
	}
	 
	 
	 @Transactional
	 public int saveBranchToDB(Branch branch){
		 int temp;
		 System.out.println("Entered into saveBranchToDB function");
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		 branch.setIsOpen("yes");
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   
			   List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_BRANCH);
			   FAParticular fAParticular = new FAParticular();
			   fAParticular=faParticulars.get(0);
			   int fapId=fAParticular.getFaPerId();
			   String fapIdStr = String.valueOf(fapId); 
			   System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			   if(fapId<10){
				   fapIdStr="0"+fapIdStr;
				   System.out.println("After adding zeroes--"+fapIdStr);
			   }	   
			   branch.setfAParticular(fAParticular);
			   
			   int branchId = (Integer) session.save(branch);
			   
			   System.out.println("branch saved with id------------------"+branch.getBranchId());
			   branch.setBranchCode(String.valueOf(branchId));
			   branchId = branchId+100000;
			   String subBrId = String.valueOf(branchId).substring(1,6);
			   String faCode=fapIdStr+subBrId;
			   System.out.println("faCode code is------------------"+faCode);
			   branch.setBranchFaCode(faCode);
			   
			   String subBranchName = branch.getBranchName().substring(0,3);
			   branchId = branchId+10000000;
			   subBrId = String.valueOf(branchId).substring(1,8);
			   String branchCodeTemp = subBranchName + subBrId;
			   branch.setBranchCodeTemp(branchCodeTemp);
			   
			   session.update(branch);
			   
			   FAMaster fAMaster = new FAMaster();
			   fAMaster.setFaMfaCode(faCode);
			   fAMaster.setFaMfaType(ConstantsValues.FAP_BRANCH);
			   fAMaster.setFaMfaName(branch.getBranchName());
			   fAMaster.setUserCode(currentUser.getUserCode());
			   fAMaster.setbCode(currentUser.getUserBranchCode());
			   
			   session.save(fAMaster);
			   

			   session.flush();
			   session.clear();
			   transaction.commit();
			   temp = branchId;
		}catch(Exception e){
				e.printStackTrace();
				transaction.rollback();
				temp=-1;
		}finally{
		 
		session.close();
		}
		return temp;
	 }
	 	 
	 @Transactional
	 public int updateBranchToDB(Branch branch){
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   branch.setCreationTS(calendar);
			   session.saveOrUpdate(branch);
			   transaction.commit();
			   temp=1;
			   session.flush();
		 }catch(Exception e){
				e.printStackTrace();
				temp=-1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Branch> getAllBranch(){
			
		 List<Branch> branchList = new ArrayList<Branch>();
		 try{
			  session = this.sessionFactory.openSession();
			  branchList = session.createCriteria(Branch.class).list();
			
		 }catch (Exception e) {
			  e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return branchList;
	 }
		 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Branch> retrieveBranch(String branchCode){
		 List<Branch> list = new ArrayList<Branch>();
		 try{
			   session = this.sessionFactory.openSession();
			   Criteria cr=session.createCriteria(Branch.class);
			   cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,branchCode));
			   list =cr.setMaxResults(1).list();
		}
		catch(Exception e){
				e.printStackTrace();
		}
		session.clear();
		session.close();
		return list; 
	 }
	 
	 
	 @Override
	 public List<Branch> retrieveBranch(String branchCode,Session session){
		 List<Branch> list = new ArrayList<Branch>();
			   
		 Criteria cr=session.createCriteria(Branch.class);
		   cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,branchCode));
		   list =cr.setMaxResults(1).list();

			   return list; 
	 }
	 
	 
	 
	 
	 
	 @SuppressWarnings("unchecked")
	 public List<Branch> getAllBranchForSuperAdmin(){
		 List<Branch> list = new ArrayList<Branch>();
		 try{
			   session = this.sessionFactory.openSession();
			   Criteria cr=session.createCriteria(Branch.class);
			   cr.add(Restrictions.eq(BranchCNTS.IS_VIEW,false));
			   list =cr.list();
		 }catch(Exception e){
				e.printStackTrace();
		 }
		 session.clear();
         session.close();
		 return list;  
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int updateBranchisViewTrue(String code[]){
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
	 	 
		 for(int i=0;i<code.length;i++){
	         System.out.println("The values inside array is ----------"+code[i]);
	     }
		 
		 int[] numbers = new int[code.length];
		 for(int i = 0;i < code.length;i++){
		    numbers[i] = Integer.parseInt(code[i]);
		 }
	     try{
	          session = this.sessionFactory.openSession();
	          for(int i=0;i<code.length;i++){
	        	  transaction = session.beginTransaction();
	        	  Branch branch  = new Branch();
	        	  List<Branch> list = new ArrayList<Branch>();
	        	  Criteria cr=session.createCriteria(Branch.class);
	        	  cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,numbers[i]));
	        	  list =cr.list();
	        	  branch = list.get(0);
	        	  branch.setView(true);
	        	  branch.setCreationTS(calendar);
	        	  session.update(branch);
	        	  transaction.commit();
	          }
	          session.flush();
	          temp = 1;
	     }catch(Exception e){
	          e.printStackTrace();
	          temp = -1;
	     }
	     session.clear();
	     session.close();
	     return temp;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public List<Branch> getAllActiveBranches(){
		 System.out.println("enter into getAllActiveBranches inside BranchDAOImpl");
		 List<Branch> branchList = new ArrayList<Branch>();
		 try{
			  session = this.sessionFactory.openSession();
			  Criteria cr=session.createCriteria(Branch.class);
			  cr.add(Restrictions.eq(BranchCNTS.IS_OPEN,"yes"));
			  branchList =cr.list();
		}catch(Exception e){
			 e.printStackTrace();
		}
		 session.clear();
	    session.close(); 
		return branchList;
	}
	 
	 @SuppressWarnings("unchecked")
	 public List<Branch> getAllActiveBranchesByBrhName(String brhName){
		 logger.info("Enter into getAllActiveBranchesByBrhName() : Branch Name = "+brhName);
		 System.out.println("enter into getAllActiveBranches inside BranchDAOImpl");
		 List<Branch> branchList = new ArrayList<Branch>();
		 try{
			  session = this.sessionFactory.openSession();
			  Criteria cr=session.createCriteria(Branch.class);
			  cr.add(Restrictions.eq(BranchCNTS.IS_OPEN,"yes"));
			  cr.add(Restrictions.like(BranchCNTS.BRANCH_NAME, "%"+brhName+"%"));
			  branchList =cr.list();
		}catch(Exception e){
			 e.printStackTrace();
		}
		 session.clear();
	    session.close(); 
	    logger.info("Exit from getAllActiveBranchesByBrhName()");
		return branchList;
	}
	 
	 @SuppressWarnings("unchecked")
	 public List<Map<String,Object>> getNameAndCode(){
		 System.out.println("enter inot getNameAndCode function");
		 List<Map<String,Object>> brhList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 
			 List<Branch> branchList = new ArrayList<>();
			 Criteria cr=session.createCriteria(Branch.class);
			 cr.add(Restrictions.eq(BranchCNTS.IS_OPEN,"yes"));
			 branchList = cr.list();
			 if(!branchList.isEmpty()){
				 for(int i=0;i<branchList.size();i++){
					 Map<String,Object> map = new HashMap<String, Object>();
					 map.put("brhId",branchList.get(i).getBranchId());
					 map.put("branchName",branchList.get(i).getBranchName());
					 map.put("branchFaCode",branchList.get(i).getBranchFaCode());
					 map.put("branchPhone",branchList.get(i).getBranchPhone());
					 map.put("branchEmailId",branchList.get(i).getBranchEmailId());
					 map.put("branchAdd",branchList.get(i).getBranchAdd());
					 brhList.add(map);
				 }
			 }
			 branchList.clear();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return brhList;
	 }
	 
	/* @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getBranchCodes(){
		 List<String> branchCodeList = new ArrayList<String>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Branch.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(BranchCNTS.BRANCH_CODE));
			 cr.setProjection(projList);
			 branchCodeList = cr.list();
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
				e.printStackTrace();
		}
		session.close();
		return branchCodeList;
	}*/
	 
	 
	 @SuppressWarnings("unchecked")
	 public List<Branch> getOldBranch(int oldBranchId){
		 System.out.println("Entered into getOldBranch function in DAOImpl--- ");
		 
		 List<Branch> branchList = new ArrayList<Branch>();
			try{
				 session = this.sessionFactory.openSession();
				 Criteria cr=session.createCriteria(Branch.class);
				 cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,oldBranchId));
				 branchList =cr.setMaxResults(1).list();
			}catch(Exception e){
				 e.printStackTrace();
			}
			session.clear();
			session.close();
			return branchList; 
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 public Branch getBranchById(int brhId){
		 System.out.println("Entered into getBranchById function in DAOImpl--- "+brhId);
		 Branch branch = null;
			try{
				 session = this.sessionFactory.openSession();
				 branch = (Branch) session.get(Branch.class,brhId);
			}catch(Exception e){
				 e.printStackTrace();
			}
			session.clear();
			session.close();
			return branch; 
	 }
	 
	 
	 
	@SuppressWarnings("unchecked")
	 public String getBrNameByBrCode(String branchCode){
	    	System.out.println("Entered into getBrNameByBrCode---"+branchCode);
	    	String branchName = "";
	    	List<Branch> brlist = new ArrayList<Branch>();
	    	
	    	try{
	   	 		session = this.sessionFactory.openSession();
	   	 		Criteria cr=session.createCriteria(Branch.class);
	   	 		cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE, branchCode));
	   	 		brlist = cr.list();
	   	 		if(!brlist.isEmpty()){
	   	 			branchName = brlist.get(0).getBranchName();
	   	 		}
//	   	 		for(int i=0;i<brlist.size();i++){
//	   	 			branchName = brlist.get(i).getBranchName();
//	   	 			System.out.println("branchName---"+branchName);
//	   	 		}
	   	 		System.out.println("branchName---"+branchName);
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	session.clear();
	    	session.close();
	    	return branchName; 
	    }
	
	@SuppressWarnings("unchecked")
	 public List<Branch> getBranchByBrCodeTemp(String branchCodeTemp){
		 List<Branch> list = new ArrayList<Branch>();
		 try{
			   session = this.sessionFactory.openSession();
			   Criteria cr=session.createCriteria(Branch.class);
			   cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE_TEMP,branchCodeTemp));
			   list =cr.setMaxResults(1).list();
		}
		catch(Exception e){
				e.printStackTrace();
		}
		 session.clear();
		session.close();
		return list; 
	 }


	@Override
	public Branch getBranchBMstrLt(int branchId) {
		System.out.println("enter into getBranchBMstrLt function = "+branchId);
		Branch branch = new Branch();
		/*List<BankMstr> bankMstrList = new ArrayList<BankMstr>();*/
		 try{
			   session = this.sessionFactory.openSession();
			  // transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(Branch.class);
			   cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID, branchId));
			   branch = (Branch) cr.list().get(0);
			   Hibernate.initialize(branch.getBankMstrList());
			   //Hibernate.initialize(branch.getAtmCardMstrList());
			  // session.flush();
		}
		catch(Exception e){
				e.printStackTrace();
		}
		 session.clear();
		session.close();
		return branch;
	}
	
	@Override
	 public List<Employee> getEmployeeList(int branchId){
		System.out.println("get employee list...");
		List<Employee> empList=new ArrayList<Employee>();
		try{
		   session=sessionFactory.openSession();
		     Criteria cr=session.createCriteria(Employee.class);
		     cr.add(Restrictions.eq(EmployeeCNTS.BRANCH_CODE,String.valueOf(branchId)));
		      empList=cr.list();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		 session.clear();
		 session.close();
		return empList;
	}
	
	
	@Override
	@Transactional
	public int updateBrWise(Branch branch) {
		System.out.println("enter into updateBrWisefunction = "+branch.getBranchId());
		List<Branch> branchList = new ArrayList<Branch>();
		List<Employee> empList = new ArrayList<Employee>();
		int temp = 0;
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   
			   Criteria cr=session.createCriteria(Branch.class);
			   cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID, branch.getBranchId()));
			   branchList = cr.list();
			   System.out.println("size of branchList = "+branchList.size());
			   if(!branchList.isEmpty()){
				   Branch actBranch = branchList.get(0);
				   if(!branch.getBranchAreaMngr().equalsIgnoreCase(actBranch.getBranchAreaMngr())){
					   actBranch.setBranchAreaMngr(branch.getBranchAreaMngr());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchAreaMngr())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
					   
				   }
				   if(!branch.getBranchCashier().equalsIgnoreCase(actBranch.getBranchCashier())){
					   actBranch.setBranchCashier(branch.getBranchCashier());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchCashier())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchDirector().equalsIgnoreCase(actBranch.getBranchDirector())){
					   actBranch.setBranchDirector(branch.getBranchDirector());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchDirector())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchMarketing().equalsIgnoreCase(actBranch.getBranchMarketing())){
					   actBranch.setBranchMarketing(branch.getBranchMarketing());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchMarketing())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchMarketingHD().equalsIgnoreCase(actBranch.getBranchMarketingHD())){
					   actBranch.setBranchMarketingHD(branch.getBranchMarketingHD());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchMarketingHD())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchMngr().equalsIgnoreCase(actBranch.getBranchMngr())){
					   actBranch.setBranchMngr(branch.getBranchMngr());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchMngr())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchOutStandingHD().equalsIgnoreCase(actBranch.getBranchOutStandingHD())){
					   actBranch.setBranchOutStandingHD(branch.getBranchOutStandingHD());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchOutStandingHD())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchRegionalMngr().equalsIgnoreCase(actBranch.getBranchRegionalMngr())){
					   actBranch.setBranchRegionalMngr(branch.getBranchRegionalMngr());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchRegionalMngr())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   if(!branch.getBranchTraffic().equalsIgnoreCase(actBranch.getBranchTraffic())){
					   actBranch.setBranchTraffic(branch.getBranchTraffic());
					   
					   cr=session.createCriteria(Employee.class);
					   cr.add(Restrictions.eq(EmployeeCNTS.EMP_ID, Integer.parseInt(branch.getBranchTraffic())));
					   empList = cr.list();
					   
					   Employee employee = empList.get(0);
					   employee.getSeBranch().add(actBranch);
					   session.update(employee);
				   }
				   
				   temp = 1;
				   session.merge(actBranch);
			   }
			   transaction.commit();
			   session.flush();
		}catch(Exception e){
				e.printStackTrace();
				temp = -1;
		}
		 session.clear();
		session.close();
		return temp;
	}


	@Override
	public List<Map<String, Object>> getActiveBrNCI() {
		System.out.println("enter inot getActiveBrNCI function");
		List<Map<String,Object>> branchList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(branchId as branchId, branchName as branchName, branchFaCode as branchFaCode) from Branch where isOpen = :isOpen"); 
			query.setString("isOpen", "yes");
			branchList = query.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return branchList;
	}


	@Override
	public Map<String, Object> getActiveBrNCustNCI() {
		System.out.println("enter inot getActiveBrNCustNCI function");
		Map<String, Object> map = new HashMap();
		List<Map<String, Object>> branchList = new ArrayList<>();
		List<Map<String, Object>> custList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Query brQuery = session.createQuery("select new map(branchId as branchId, branchName as branchName, branchFaCode as branchFaCode) from Branch where isOpen = :isOpen"); 
			brQuery.setString("isOpen", "yes");
			branchList = brQuery.list();
			
			Query custQuery = session.createQuery("select new map(custId as custId, custName as custName, custFaCode as custFaCode) FROM Customer ");
			custList = custQuery.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		map.put("branchList", branchList);
		map.put("custList", custList);
		return map;
	}
	
}