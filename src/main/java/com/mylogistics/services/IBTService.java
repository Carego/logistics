package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mylogistics.model.TempIntBrTv;

@Service
public class IBTService {

	private VoucherService voucherService;
	
	private List<TempIntBrTv> tibList = new ArrayList<TempIntBrTv>();

	public VoucherService getVoucherService() {
		return voucherService;
	}

	public void setVoucherService(VoucherService voucherService) {
		this.voucherService = voucherService;
	}

	public List<TempIntBrTv> getTibList() {
		return tibList;
	}

	public void setTibList(List<TempIntBrTv> tibList) {
		this.tibList = tibList;
	}


	
}
