<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="custStnryForm" ng-submit="submitCustStnry(custStnryForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s3 input-field">
		       			<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       		<label for="code">Branch</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       			<input class="validate" type ="number" id="frmStnryNoId" name ="frmStnryNoName" ng-model="frmStnryNo" ng-required="true">
		       		<label for="code">From No.</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       			<input class="validate" type ="number" id="toStnryNoId" name ="toStnryNoName" ng-model="toStnryNo" ng-required="true">
		       		<label for="code">To No.</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		    		<select name="stnryTypeName" id="stnryTypeId" ng-model="stnryType" ng-init="stnryType='cnmt'" required>
						<option value='cnmt'>CNMT</option>
						<option value='chln'>CHALLAN</option>
						<option value='sedr'>SEDR</option>
					</select>
					<label>Type</label>
				</div>
		       	
		       	
			
			</div>
			
			<div class="row">
				
				<div class="col s3 input-field">
		       			<input class="validate" type ="text" id="stnryPrefixId" name ="stnryPrefixName" ng-model="stnryPrefix">
		       		<label for="code">Prefix</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       			<input class="validate" type ="text" id="stnrySuffixId" name ="stnrySuffixName" ng-model="stnrySuffix">
		       		<label for="code">Suffix</label>	
		       	</div>
				
		    	<div class="col s3 input-field">
		       	 		<input class="validate" type ="number" id="noOfDgtId" name ="noOfDgtName" ng-model="noOfDgt"  ng-required="true" >
		       		<label for="code">No. of digits</label>	
		       	</div>
				
				
				<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" id="submitId" value="Submit" >
	       		</div>
			</div>
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
</div>