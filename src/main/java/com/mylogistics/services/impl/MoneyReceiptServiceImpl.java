package com.mylogistics.services.impl;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.exception.custom.BankMstrException;
import com.mylogistics.exception.custom.CashStmtStatusException;
import com.mylogistics.exception.custom.CustomerException;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Customer;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.BankMasterService;
import com.mylogistics.services.CashStmtStatusService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.CustomerService;
import com.mylogistics.services.MoneyReceiptService;

@Service
public class MoneyReceiptServiceImpl implements MoneyReceiptService {

	public static final Logger LOGGER = Logger.getLogger(MoneyReceiptServiceImpl.class);
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private CashStmtStatusService cashStmtStatusService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private BankMasterService bankMasterService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String saveOAMR(MoneyReceipt moneyReceipt) throws CashStmtStatusException, CustomerException, BankMstrException {
		
		LOGGER.info("Enter into saveMR()");
		User currentUser 	=	(User) httpSession.getAttribute("currentUser");
		String mrNo 		= 	generateNewMrNo( Integer.parseInt(currentUser.getUserBranchCode()));
		moneyReceipt.setUserCode(currentUser.getUserCode());
		moneyReceipt.setbCode(currentUser.getUserBranchCode());
		moneyReceipt.setMrType(ConstantsValues.OA_MR);
		moneyReceipt.setMrBrhId(Integer.parseInt(currentUser.getUserBranchCode()));
		moneyReceipt.setMrIsPending(true);
		moneyReceipt.setMrNo(mrNo);
		moneyReceipt.setMrRemAmt(moneyReceipt.getMrNetAmt());
		
		moneyReceiptDAO.saveOAMRN(moneyReceipt);
		
		CashStmtStatus css =  cashStmtStatusService.getCashStmtStatus(currentUser.getUserBranchCode(), moneyReceipt.getMrDate());
		if (css == null)
			throw new CashStmtStatusException(CashStmtStatusCNTS.NOT_FOUND);
		
		int voucherNo = cashStmtStatusService.getLastVoucherNo(css.getCssId()) + 1;
		CashStmt cs1 = new CashStmt();
		cs1.setUserCode(currentUser.getUserCode());
		cs1.setbCode(currentUser.getUserBranchCode());
		cs1.setCsDescription(moneyReceipt.getMrDesc());
		cs1.setCsDrCr('C');
		cs1.setCsAmt(moneyReceipt.getMrNetAmt());
		cs1.setCsType(ConstantsValues.MNY_RCT);
		
		if(moneyReceipt.getMrPayBy() == 'C') {
			cs1.setCsTvNo(mrNo);
			cs1.setPayMode("C");
			cs1.setCsVouchType("CASH");
		} 
		else if(moneyReceipt.getMrPayBy() == 'Q'){
			String mrChqNo = moneyReceipt.getMrChqNo();
			cs1.setCsTvNo(mrNo+" ("+mrChqNo+")");
			cs1.setPayMode("Q");
			cs1.setCsVouchType("BANK");
		} 
		else if(moneyReceipt.getMrPayBy() == 'R'){				
			cs1.setCsTvNo(mrNo +" ("+moneyReceipt.getMrRtgsRefNo()+")");
			cs1.setPayMode("R");
			cs1.setCsVouchType("BANK");
		}
		
		Customer customer = customerService.getCustomer(moneyReceipt.getMrCustId());
		if (customer == null)
			throw new CustomerException(CustomerCNTS.NOT_FOUND);
		
		cs1.setCsFaCode(customer.getCustFaCode());
		cs1.setCsVouchNo(voucherNo);
		cs1.setCsDt(css.getCssDt());		 
		cs1.setCsNo(css);
		
		css.getCashStmtList().add(cs1);
		
		if(moneyReceipt.getMrPayBy()!='C'){
			CashStmt cs2 = new CashStmt();
			cs2.setUserCode(currentUser.getUserCode());
			cs2.setbCode(currentUser.getUserBranchCode());
			cs2.setCsDescription(moneyReceipt.getMrDesc());
			cs2.setCsDrCr('D');
			cs2.setCsAmt(moneyReceipt.getMrNetAmt());
			cs2.setCsType(ConstantsValues.MNY_RCT);
			
			if(moneyReceipt.getMrPayBy() == 'Q'){
				String mrChqNo = moneyReceipt.getMrChqNo();
				cs2.setCsTvNo(mrNo+" ("+mrChqNo+")");
				cs2.setPayMode("Q");
			}
			 else if(moneyReceipt.getMrPayBy() == 'R'){
				cs2.setCsTvNo(mrNo +" ("+moneyReceipt.getMrRtgsRefNo()+")");
				cs2.setPayMode("R");
			}
			BankMstr bankMstr = bankMasterService.getBankMstr(moneyReceipt.getMrBnkId());
			if (bankMstr == null)
				throw new BankMstrException(BankMstrCNTS.NOT_FOUND);
				
			cs2.setCsVouchType("BANK");
			cs2.setCsFaCode(bankMstr.getBnkFaCode());
			
			cs2.setCsVouchNo(voucherNo);
			cs2.setCsDt(css.getCssDt());
			 
			cs2.setCsNo(css);
			css.getCashStmtList().add(cs2);
			
			double newBalAmt = bankMstr.getBnkBalanceAmt() + moneyReceipt.getMrNetAmt();
			bankMstr.setBnkBalanceAmt(newBalAmt);
			
			bankMasterService.update(bankMstr);
			
		}
		cashStmtStatusService.merge(css);
		return mrNo;
	}
	
	@Override
	@Transactional
	public String generateNewMrNo(int userBranchCode) {
		String 			mrNo 				= 	null;
		MoneyReceipt 	lastMoneyReceipt 	= 	moneyReceiptDAO.getLastMoenyReceipt(userBranchCode);
		
		if(lastMoneyReceipt != null){
			String prevMrNo = lastMoneyReceipt.getMrNo();
			mrNo  = "MR" + String.valueOf(Integer.parseInt(prevMrNo.substring(2)) + 1 + 1000000).substring(1);
		} else {
			mrNo = "MR000001";
		}
		LOGGER.info("generated mrNo = "+mrNo);
		return mrNo;
	}
	
}