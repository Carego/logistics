package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ContPerson;

@Repository
public interface ContPersonDAO {
	
	//Sanjana: saves contPerson to database
	public int saveContactPerson(ContPerson contPerson);
	
	public List<ContPerson> getContPerson();
	
	public int saveOwnMobNo(ContPerson contPerson);
	
	public List<String> getMobList(String code);
	
	public int addMoreMobNo(String mobno, String code);
	
	public List<ContPerson> getContactPerson(String code);
	
	public int updateContPerson(ContPerson contPerson);
	
}
