'use strict';

var app = angular.module('application');

app.controller('CreatDispatchOrderCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.brsd = {};
	$scope.HBON={};

	//$scope.brsd.brsDisBranchCode = $scope.getBrsDisBrCode();
	$scope.cnmtTextboxList = [];
	$scope.chlnTextboxList =[];
	$scope.sedrTextboxList = [];
	$scope.modelCnmt = [];
	$scope.modelChln = [];
	$scope.modelSedr = [];
	$scope.brSDisDetCnmtList = [];
	$scope.brSDisDetChlnList = [];
	$scope.brSDisDetSedrList = [];
	$scope.id=0;
	var responseCnmt;
	var responseChln;
	var responseSedr;
	$scope.cnmtFlag=false;
	$scope.chlnFlag=false;
	$scope.sedrFlag=false;
	
	
	$scope.saveCnmt = function(cnmt){
		console.log("Enter into saveCNMT---"+cnmt);
		
		for(var i=0;i<$scope.cnmtTextboxList.length;i++){
			$scope.cnmtTextboxList.splice(i,1);
		}
		
		for(var i=0;i<cnmt;i++){
			$scope.cnmtTextboxList.push(i);
		}
	}
	
	$scope.saveChln = function(chln){
		console.log("Enter into saveChln---"+chln);
		
		for(var i=0;i<$scope.chlnTextboxList.length;i++){
			$scope.chlnTextboxList.splice(i,1);
		}
		
		for(var i=0;i<chln;i++){
			$scope.chlnTextboxList.push(i);
		}
	}
	
	$scope.saveSedr = function(sedr){
		console.log("Enter into saveSedr---"+sedr);
		
		for(var i=0;i<$scope.sedrTextboxList.length;i++){
			$scope.sedrTextboxList.splice(i,1);
		}
		
		for(var i=0;i<sedr;i++){
			$scope.sedrTextboxList.push(i);
		}
	}
	
	$scope.saveNoCnmt = function(modelCnmt,brsd){
		console.log("Enter into saveNoCnmt function-- ");
		for(var i =0;i<modelCnmt.length;i++){
			console.log("----"+modelCnmt[i]);	
			var brSDisDet = {
				"brsDisDetBrCode"  : 	brsd.brsDisBranchCode,
				"brsDisDetStatus"  :    "cnmt",
				"brsDisDetStartNo" : 	modelCnmt[i],
				"brsDisDetDisCode" :	brsd.brsDisCode
			} ;
		
			$scope.brSDisDetList.push(brSDisDet);
		}	
		var response = $http.post($scope.projectName+'/saveNoCnmt', $scope.brSDisDetList);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				console.log(data.result);
				responseCnmt="success";
				console.log("--------------"+responseCnmt);
				for(var i=0;i<$scope.brSDisDetList.length;i++){
					$scope.brSDisDetList.splice(i,1);
				}
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.saveNoChln = function(modelChln,brsd){
		console.log("Enter into saveNoChln function-- ");
		for(var i =0;i<modelChln.length;i++){
			console.log("----"+modelChln[i]);	
			var brSDisDet = {
					"brsDisDetBrCode"  : 	brsd.brsDisBranchCode,
					"brsDisDetStatus"  :    "chln",
					"brsDisDetStartNo" : 	modelChln[i],
					"brsDisDetDisCode" :	brsd.brsDisCode
			};
			$scope.brSDisDetList.push(brSDisDet);
		}
		var response = $http.post($scope.projectName+'/saveNoChln', $scope.brSDisDetList);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				responseChln="success";
				console.log("--------------"+responseChln);
				console.log(data);
				for(var i=0;i<$scope.brSDisDetList.length;i++){
					$scope.brSDisDetList.splice(i,1);
				}
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.saveNoSedr = function(modelSedr,brsd){
		console.log("Enter into saveNoSedr function-- ");
		for(var i =0;i<modelSedr.length;i++){
			console.log("----"+modelSedr[i]);	
			var brSDisDet = {
					"brsDisDetBrCode"  : 	brsd.brsDisBranchCode,
					"brsDisDetStatus"  :    "sedr",
					"brsDisDetStartNo" : 	modelSedr[i],
					"brsDisDetDisCode" :	brsd.brsDisCode
			};
			$scope.brSDisDetList.push(brSDisDet);
		}
		var response = $http.post($scope.projectName+'/saveNoSedr', $scope.brSDisDetList);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				responseSedr="success";
				console.log("--------------"+responseSedr);
				for(var i=0;i<$scope.brSDisDetList.length;i++){
					$scope.brSDisDetList.splice(i,1);
				}
				if(responseChln==="success" || responseCnmt ==="success" || responseSedr==="success"){
					console.log("Entr to enalble submit button");
					$('#submitBSD').removeAttr("disabled");
				}
				
				
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.saveBranchStockDispatch = function(createDispatchOrderForm,brsd,modelCnmt,modelChln,modelSedr){
		console.log("Enter into saveBranchStockDispatch");
		if($scope.cnmtFlag===true && $scope.chlnFlag===true && $scope.sedrFlag===true){
			 var brSDis = {
					"cnmtList"         :    modelCnmt,
					"chlnList"         :    modelChln,
					"sedrList"         :    modelSedr,
					"brStkDis"         :	brsd,
					"id"               :    $scope.id
			 };
			
			   var response = $http.post($scope.projectName+'/saveBranchStockDispatch',brSDis);
			   response.success(function(data, status, headers, config){
				   $scope.alertToast(data.result);
				   $location.path('/operator');
		       });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
			
	    }else{
			$scope.alertToast("Please fill the correct number")
		}
	}
	
	$scope.getDispatchRequirements = function(){
		console.log("enter into getDispatchRequirements function");
		 var response = $http.post($scope.projectName+'/getDispatchRequirements');
		   response.success(function(data, status, headers, config){
			   $scope.alertToast(data.result);
			   $scope.HBON = data.hboN;
			   $scope.id=$scope.HBON.hbonId;
			   console.log("id is -------"+$scope.id);
			   $scope.brsd.brsDisBranchCode = $scope.HBON.disBranchCode;
			  /* $scope.brsd.brsDisCode = $scope.HBON.dispatchCode;*/
			   $scope.brsd.brsDisCnmt = $scope.HBON.noCnmt;
			   $scope.brsd.brsDisChln = $scope.HBON.noChln;
			   $scope.brsd.brsDisSedr = $scope.HBON.noSedr;
			  
			   $scope.saveCnmt($scope.HBON.noCnmt);
			   $scope.saveChln($scope.HBON.noChln);
			   $scope.saveSedr($scope.HBON.noSedr);
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		});	
	}
	
	$scope.checkCnmtNo=function(modelCnmt){
	 console.log("enter into checkCnmtNo function");	
	 for(var i =0;i<modelCnmt.length;i++){
		 	var t = ((modelCnmt[i]-1)%50);
		 	if(t===0 && modelCnmt[i].length===7){
		 		$scope.cnmtFlag=true;
		 		console.log(modelCnmt[i]);
		 	}else{
		 		$scope.cnmtFlag=false;
		 		$scope.alertToast("Enter correct Cnmt Number");
	 			console.log("Failed from checkCnmt");
		 	}
		} 
	}
	
	
	$scope.checkChlnNo=function(modelChln)
	{
	 for(var i =0;i<modelChln.length;i++){
		 	var t = ((modelChln[i]-1)%50);
		 	if(t===0 && modelChln[i].length===7)
		 		{
		 		$scope.chlnFlag=true;
		 		console.log(modelChln[i]);
		    }else
		 		{
		 		$scope.chlnFlag=false;
		 		$scope.alertToast("Enter correct Chln Number");
		 		console.log("Failed from checkChln");
	 			
		 	}
		 }	 
	}
	
	
	$scope.checkSedrNo=function(modelSedr)
	{
	 for(var i =0;i<modelSedr.length;i++){
		 	var t = ((modelSedr[i]-1)%50);
		 	if(t===0 && modelSedr[i].length===7){
		 		$scope.sedrFlag=true;
		 		console.log(modelSedr[i]);
		 	}else{
		 		$scope.sedrFlag=false;
		 		$scope.alertToast("Enter correct Sedr Number");
	 			console.log("Failed from checkSedr");
		 	}
		 }
	}
	
	$scope.getDispatchRequirements();
	
}]);