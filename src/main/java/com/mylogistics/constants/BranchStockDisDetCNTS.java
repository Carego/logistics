package com.mylogistics.constants;

public class BranchStockDisDetCNTS {
	
	public static final String BRSDD_ID = "brsDisDetId";
	
	public static final String BRSDD_BRCODE = "brsDisDetBrCode";
	
	public static final String BRSDD_STATUS = "brsDisDetStatus";
	
	public static final String BRSDD_ST_NO = "brsDisDetStartNo";
	
	public static final String BRSDD_DIS_CODE = "brsDisDetDisCode";
	
	public static final String IS_VERIFY = "isVerify";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";

}
