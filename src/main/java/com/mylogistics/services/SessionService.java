package com.mylogistics.services;

import com.mylogistics.model.User;

public class SessionService {

     private User currentUser;

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
}
