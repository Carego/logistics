package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Bill;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DirPayMRCntlr {

	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private BillDAO billDAO;
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	
	@RequestMapping(value = "/getDPMRDetail", method = RequestMethod.POST)
	public @ResponseBody Object getDPMRDetail() {
		System.out.println("enter into getDPMRDetail function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				map.put("list",custList);
				List<Map<String, Object>> hoBankList = new ArrayList<>();
				if(currentUser.getUserBranchCode().equalsIgnoreCase("1")){
					System.out.println("HO user set in session");
				}else{
					hoBankList =  bankMstrDAO.getBankNCAIList("1");
				}
				List<Map<String, Object>> bankList = bankMstrDAO.getBankNCAIList(currentUser.getUserBranchCode());
				List<Map<String, Object>> actBankList = new ArrayList<>();
				
				actBankList = hoBankList;
				System.out.println("size of actBankList = "+actBankList.size());
				if(!bankList.isEmpty()){
					for(int i=0;i<bankList.size();i++){
						actBankList.add(bankList.get(i));
					}
				}
				System.out.println("size of actBankList = "+actBankList.size());
				map.put("bnkList",actBankList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	
	@RequestMapping(value = "/getBillFrDPMR", method = RequestMethod.POST)
	public @ResponseBody Object getBillFrDPMR(@RequestBody String custId) {
		System.out.println("enter into getPendingMR function = "+custId);
		Map<String,Object> map = new HashMap<>();
		int cId = Integer.parseInt(custId);
		if(cId > 0){
			List<Bill> blList = new ArrayList<>();
			blList = billDAO.getPendingBillFrMr(cId);
			System.out.println("size of blList = "+blList.size());
			if(!blList.isEmpty()){
				map.put("blList",blList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getCnmtFrDPMR", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtFrDPMR(@RequestBody String blNo) {
		System.out.println("enter into getCnmtFrDPMR function = ");
		Map<String,Object> map = new HashMap<>();
		List<String> cnmtList = new ArrayList<String>();
		cnmtList = cnmtDAO.getCnmtNoByBill(blNo);
		if(!cnmtList.isEmpty()){
			map.put("list",cnmtList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitDPMR", method = RequestMethod.POST)
	public @ResponseBody Object submitDPMR(@RequestBody List<MoneyReceipt> mrList) {
		System.out.println("enter into submitDPMR function = ");
		Map<String,Object> map = new HashMap<>();
		/*if(!mrList.isEmpty()){
			String mrNo = moneyReceiptDAO.saveDPMR(mrList);
			if(mrNo != null){
				map.put("mrNo",mrNo);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}*/
		if(!mrList.isEmpty()){
			int res = moneyReceiptDAO.duplicateDPMR(mrList);
			System.out.println("result = "+res);
			if(res > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitFinalDPMR", method = RequestMethod.POST)
	public @ResponseBody Object submitFinalDPMR(@RequestBody List<MoneyReceipt> mrList) {
		System.out.println("enter into submitFinalDPMR function = ");
		Map<String,Object> map = new HashMap<>();
		if(!mrList.isEmpty()){
			if(mrList.get(0).getMrDate() != null){
				String mrNo = moneyReceiptDAO.saveDPMR(mrList);
				if(mrNo != null){
					map.put("mrNo",mrNo);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	//TODO Back Data
	@RequestMapping(value = "/getDPMRDetailBack", method = RequestMethod.POST)
	public @ResponseBody Object getDPMRDetailBack() {
		System.out.println("enter into getDPMRDetailBack function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				map.put("list",custList);
				List<Map<String, Object>> hoBankList = new ArrayList<>();
				if(currentUser.getUserBranchCode().equalsIgnoreCase("1")){
					System.out.println("HO user set in session");
				}else{
					hoBankList =  bankMstrDAO.getBankNCAIList("1");
				}
				List<Map<String, Object>> bankList = bankMstrDAO.getBankNCAIList(currentUser.getUserBranchCode());
				List<Map<String, Object>> actBankList = new ArrayList<>();
				
				actBankList = hoBankList;
				System.out.println("size of actBankList = "+actBankList.size());
				if(!bankList.isEmpty()){
					for(int i=0;i<bankList.size();i++){
						actBankList.add(bankList.get(i));
					}
				}
				System.out.println("size of actBankList = "+actBankList.size());
				map.put("bnkList",actBankList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
}
