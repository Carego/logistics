package com.mylogistics.constants;

public class CashStmtTempCNTS {

	public static final String CS_ID = "csId";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CS_BRH_FA_CODE = "csBrhFaCode";
	
	public static final String CS_FA_CODE = "csFaCode";
	
	public static final String CS_DT = "csDt";
	
	public static final String CS_IS_VIEW = "csIsView";
	
}
