package com.mylogistics.model.lhpv;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "munsiana")
public class Munsiana {

	@Id
	@Column(name="munsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int munsId;
	
	@Column(name="munsFrAdv" , columnDefinition="double default 0.0")
	private double munsFrAdv;
	
	@Column(name="munsToAdv" , columnDefinition="double default 0.0")
	private double munsToAdv;
	
	@Column(name="munsAmt" , columnDefinition="double default 0.0")
	private double munsAmt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getMunsId() {
		return munsId;
	}

	public void setMunsId(int munsId) {
		this.munsId = munsId;
	}

	public double getMunsFrAdv() {
		return munsFrAdv;
	}

	public void setMunsFrAdv(double munsFrAdv) {
		this.munsFrAdv = munsFrAdv;
	}

	public double getMunsToAdv() {
		return munsToAdv;
	}

	public void setMunsToAdv(double munsToAdv) {
		this.munsToAdv = munsToAdv;
	}

	public double getMunsAmt() {
		return munsAmt;
	}

	public void setMunsAmt(double munsAmt) {
		this.munsAmt = munsAmt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	
	
}
