<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dispatch Details</title>
</head>
<body>
	<form name="dispatchDetailForm" ng-submit="submitDisDetForm(dispatchDetailForm,brsDisData.brsDisDt,receiveDate,invoiceNo)">
		<table>
			<tr>
				<td>Dispatch Code</td>
				<td><input type="text" name="dispatchCode" ng-model="dispatchCode"> </td>
			</tr>
			
			<tr>
				<td>CNMT
					<div ng-repeat="cnmtStartNo in cnmtStartNoList">
						<input type="checkbox" name="checkCnmts" class="abc">
						{{cnmtStartNo}}
						<br>
					</div>
				</td>
				<td>CHALLAN
					<div ng-repeat="chlnStartNo in chlnStartNoList">
					<input type="checkbox" name="checkChlns" class="abc">
						{{chlnStartNo}}
						<br>
					</div>
				</td>
				<td>SEDR
					<div ng-repeat="sedrStartNo in sedrStartNoList">
					<input type="checkbox" name="checkSedrs" class="abc">
						{{sedrStartNo}}
						<br>
					</div>
				</td>
				
			</tr>
			
			<tr>
			 	<td>Dispatch Date</td>
			 	<td><input type="date" name="dispatchDate" ng-model="brsDisData.brsDisDt"></td>
			</tr>
			
			<tr>
			 	<td>Receive Date</td>
			 	<td><input type="date" name="receiveDate" ng-model="receiveDate"></td>
			</tr>
			
			<tr>
			 	<td>Dispatch Invoice Number</td>
			 	<td><input type="text" name="invoiceNo" ng-model="invoiceNo"></td>
			</tr>
			
			<tr>
				<td><input type="submit" name="submit" value="Submit"></td>
			</tr>
			
		</table>
	</form>

</body>
</html>