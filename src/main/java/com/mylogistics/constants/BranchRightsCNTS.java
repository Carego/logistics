package com.mylogistics.constants;

public class BranchRightsCNTS {
	
	public static final String BRH_ID = "brhId";
	
	public static final String BRH_BRANCH_CODE = "brhBranchCode";
	
	public static final String BRH_ADV_ALLOW = "brhAdvAllow";
	
	public static final String BRH_ADV_VALID_DT = "brhAdvValidDt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";

}
