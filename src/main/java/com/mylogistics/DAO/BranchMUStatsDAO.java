package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.BranchMUStatsBk;

@Repository
public interface BranchMUStatsDAO {

	public int saveOrUpdateBMUS(BranchMUStats branchMUStats);
	
	public int saveOrUpdateBMUS(BranchMUStats branchMUStats,Session session);
	
	/*public List<BranchMUStats> getLastCnmtData(String branchCode);*/
	
	public List<BranchMUStats> getLastData(String branchCode,String type);
	
	public List<BranchMUStats> getLastData(String branchCode,String type,Session session);
	
	public int totalBMUSRows(String type);
	
	public int totalBMUSRows(String type,Session session);
	
	/*public int totalChlnRows();
	
	public int totalSedrRows();*/
	
	public int updateBMUS(BranchMUStats branchMUStats);
	
	public int updateBMUS(BranchMUStats branchMUStats,Session session);
	
	/*public List<BranchMUStats> getLastChlnData(String branchCode);
	
	public List<BranchMUStats> getLastSedrData(String branchCode);*/
	
	public double getAvgMonUsg(String type);
	
	public List<String> getAllBranch();
	
	/*public Date getFirstEntryDate();*/
	
	/*//Satarupa: shows average month usage of challans
	public double getAvgMonUsgCHLN();
	
	//Satarupa: shows average month usage of sedrs
	public double getAvgMonUsgSEDR();*/
	
	public int saveBMUSBK(BranchMUStatsBk branchMUStatsBk);
	
	public int saveBMUSBK(BranchMUStatsBk branchMUStatsBk,Session session);
	
	public int deleteBMUS(int id);
	
	public int deleteBMUS(int id,Session session);
	
	/*public double getNoOfCnmt(Date startDate,Date endDate);*/
}
