package com.mylogistics.DAO.werservice;

import java.sql.Blob;
import java.util.Map;

import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.ImageApp;
import com.mylogistics.model.webservice.OwnBrkApp;

public interface OwnBrkDao {

	public Map<String, Object> saveOwnBrk(OwnBrkApp ownBrkApp);
	
	public Boolean findOwnBrk(OwnBrkApp ownBrkApp);
	
	public Map<String, Object> getPendingOwnBrk(String type);
	
	public Map<String, Object> getOwnBrkDtById(OwnBrkApp ownBrkApp);
	
	public ImageApp findOwnBrkImg(Integer ownBrkId, String type);
	
	public void copyImagesOrDeleteOwn(ImageApp imgApp, String ownerCode, Blob panImg, Blob decImg, User user);
	
	public void copyImagesOrDeleteBrk(ImageApp imgApp, String brokerCode, Blob panImg, Blob decImg, User user);
	
	public void updateOwnBrkRecords(Integer ownBrkId, String code);
	
}