package com.mylogistics.constants;

public class LhpvTempCNTS {

	public static final String B_CODE = "bCode";
	
	public static final String USER_CODE = "userCode";
	
	public static final String LHPV_START = "ltStart";
	
	public static final String LHPV_END = "ltEnd";
}
