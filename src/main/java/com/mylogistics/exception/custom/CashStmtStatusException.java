package com.mylogistics.exception.custom;

public class CashStmtStatusException extends Exception {

	private static final long serialVersionUID = -3136968594034585877L;

	public CashStmtStatusException(String exceptionMsg) {
		super(exceptionMsg);
	}
	
}