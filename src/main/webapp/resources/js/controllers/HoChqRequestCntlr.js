'use strict';

var app = angular.module('application');

app.controller('HoChqRequestCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("HoChqRequestCntlr Started");
	
	$scope.branchList	= [];
	$scope.bankMstrList = [];
	
	$scope.branch = {};
	$scope.bankMstr = {};
	$scope.chqReq = {};
	$scope.branchDBFlag = true;
	$scope.bankMstrDBFlag = true;
	$scope.saveBankFlag = true;
	$scope.printChqReqDBFlag = true;
	
	$scope.noOfBookFlag = false;
	$scope.noOfChqPerBookFalg = false;
	
	$scope.chqReqPrintTypeFlag = false;
	
	//print variables
	$scope.totalNOChqPrint = "";
	$scope.chqTypeValuePrint = "computerised";
	$scope.chqReqRefNoPrint = "";
	
	$scope.chqReqDtPrint = "";
	$scope.bankNamePrint = "";
	$scope.completeAddPrint = "";
	$scope.addCityPrint = "";
	$scope.addPinPrint = "";
	$scope.addStatePrint = "";
	$scope.bankAcNoPrint = "";
	
	
	//$scope.chqReq.cReqType = "Select cheque type";
	
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				if ($scope.assignBankRBFlag) {
					$('#selectBranchId').removeAttr("disabled");
				}
				
				for ( var i = 0; i < $scope.branchList.length; i++) {
					console.log("Branch Name: "+$scope.branchList[i].branchName);
					console.log("Branch Code: "+$scope.branchList[i].branchCode);
					console.log("Branch Address: "+$scope.branchList[i].branchAdd);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		
		$scope.bankMstr = {};
		
		var response = $http.post($scope.projectName+'/getChildBankMstrList', $scope.branch.branchId);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$('#bankMstrId').removeAttr("disabled");
				$scope.bankMstrList = data.bankMstrList;
				for ( var i = 0; i < $scope.bankMstrList.length; i++) {
					console.log("bankName==>"+$scope.bankMstrList[i].bnkName);
					console.log("bankFaCode==>"+$scope.bankMstrList[i].bnkFaCode);
				}
			}else {
				$('#bankMstrId').attr("disabled","disabled");
				$scope.alertToast("Please assign bank for "+branch.branchName+" branch" );
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
		
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankMstrDBFlag = false;
		$('div#bankMstrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankMstrDBFlag = true;
			}
		});

		$('div#bankMstrDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr){
		console.log("saveBank");
		console.log("enter into branchName----->"+bankMstr.bnkName);
		console.log("enter into branchFaCode----->"+bankMstr.bnkFaCode);
		$scope.bankMstr = bankMstr;
		$('div#bankMstrDB').dialog('close');
	}
	
	$scope.noOfBook =  function(){
		console.log("noOfBook() called");
		if(angular.isUndefined($scope.chqReq.cReqNOBook) || $scope.chqReq.cReqNOBook === "" || $scope.chqReq.cReqNOBook === null){
			$scope.noOfBookFlag = false;
		}else{
			$scope.noOfBookFlag = true;
		}
		$scope.totalNoOfChqs();
	}
	
	$scope.noOfChqPerBook =  function(){
		console.log("noOfChqPerBook() called");
		if(angular.isUndefined($scope.chqReq.cReqNOChqPerBook) || $scope.chqReq.cReqNOChqPerBook === "" || $scope.chqReq.cReqNOChqPerBook === null){
			$scope.noOfChqPerBookFalg = false;
		}else{
			$scope.noOfChqPerBookFalg = true;
		}
		$scope.totalNoOfChqs();
	}
	
	$scope.totalNoOfChqs = function(){
		if ($scope.noOfChqPerBookFalg && $scope.noOfBookFlag) {
			$scope.totalNOChqPrint = parseInt($scope.chqReq.cReqNOChqPerBook)*parseInt($scope.chqReq.cReqNOBook)
			$scope.alertLongToast("Total no of cheques: "+$scope.totalNOChqPrint);
		}
	}
	
	//form submission
	$scope.hoChqRequestSubmit = function(hoChqRequestForm) {
		
		if (hoChqRequestForm.$invalid) {
			if (hoChqRequestForm.branchName.$invalid) {
				$scope.alertToast("please enter branch");
			} else if (hoChqRequestForm.bankMstrName.$invalid) {
				$scope.alertToast("please enter bank");
			} else if (hoChqRequestForm.chqReqDtName.$invalid) {
				$scope.alertToast("please enter Date");
			} else if (hoChqRequestForm.chqReqNOBookName.$invalid) {
				$scope.alertToast("please enter no of books required");
			} else if (hoChqRequestForm.chqReqNOChqPerBookName.$invalid) {
				$scope.alertToast("please enter no of cheque per book required");
			} else if (hoChqRequestForm.chqReqTypeName.$invalid) {
				$scope.alertToast("please enter type of cheque");
			} 
		} else {
			
			if ($scope.chqReq.cReqType == 'C') {
				if (angular.isUndefined($scope.chqReq.cReqPrintType) 
						|| $scope.chqReq.cReqPrintType === "" 
							|| $scope.chqReq.cReqPrintType === null) {
					$scope.chqReqPrintTypeFlag = false;
					console.log("ChequeType Computerised if");
					$scope.alertToast("please enter print Type");
					
				}else {
					$scope.chqReqPrintTypeFlag = true;
					console.log("ChequeType Computerised if");
				}
			}else if ($scope.chqReq.cReqType == 'M') {
				$scope.alertToast("ChequeType Manual");
				if (angular.isUndefined($scope.chqReq.cReqPrintType) 
						|| $scope.chqReq.cReqPrintType === "" 
							|| $scope.chqReq.cReqPrintType === null) {
					console.log("ChequeType Manual if");
					$scope.chqReqPrintTypeFlag = true;
					
				}else {
					$scope.chqReqPrintTypeFlag = false;
					console.log("ChequeType Manual else");
					$scope.alertToast("No printType allow for manual cheques");
				}
			}
			
			if ($scope.chqReqPrintTypeFlag) {
				$scope.saveBankFlag = false;
				$('div#saveBankDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					title: "Cheque Request",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.saveBnkFlag = true;
					}
				});
				$('div#saveBankDB').dialog('open');
			}
			
			
		}
		
		
		
	}
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveBankDB').dialog('close');
	}
	
	$scope.saveBankFinal = function(){
		console.log("enter into saveBankFinal function");
		$('div#saveBankDB').dialog('close');
		
		var chqReqService = {
				"branch"		: $scope.branch,
				"bankMstr"		: $scope.bankMstr,
				"chqReq"		: $scope.chqReq
		 };
		
		var response = $http.post($scope.projectName+'/submitChqReq',chqReqService);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				
				//$scope.branchList	= [];
				//$scope.chqReq = {};
				
				//variable for print
				$scope.chqReqRefNoPrint = data.chqReqRefNo;
				$scope.chqReqDtPrint = $scope.chqReq.cReqDt;
				$scope.bankNamePrint = $scope.bankMstr.bnkName;
				$scope.completeAddPrint = $scope.bankMstr.address.completeAdd;
				$scope.addCityPrint = $scope.bankMstr.address.addCity;
				$scope.addPinPrint = $scope.bankMstr.address.addPin;
				$scope.addStatePrint = $scope.bankMstr.address.addState;
				$scope.bankAcNoPrint = $scope.bankMstr.bnkAcNo;
				
				//clear all variable except print variable
				$scope.bankMstrList = [];
				
				$scope.branch = {};
				$scope.bankMstr = {};
				
				
				$scope.chqReq.cReqDt = "";
				$scope.chqReq.cReqNOBook = "";
				$scope.chqReq.cReqNOChqPerBook = "";
				
				
				$scope.alertLongToast("Your Reference Code: "+$scope.chqReqRefNoPrint);
				$scope.alertToast("success");
				
				$('#bankMstrId').attr("disabled","disabled");
				$scope.noOfBookFlag = false;
				$scope.noOfChqPerBookFalg = false;
				
				//open dialog for print
				$scope.printChqReqDBFlag = false;
		    	$('div#printChqReqDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.printChqReqDBFlag = true;
				    }
					});
				$('div#printChqReqDB').dialog('open');
				
			}else{
				console.log("saveBankFinal: Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.selectChqType = function() {
		console.log("selectChqType()")
		if ($scope.chqReq.cReqType == 'M') {
			console.log("Manual");
			$scope.chqTypeValuePrint = "manual";
			//$scope.chqReqPrintTypeFlag = false;
			$('#chqReqPrintTypeId').attr("disabled","disabled");
			$scope.chqReq.cReqPrintType = "";
		} else if($scope.chqReq.cReqType == 'C') {
			console.log("Computerised");
			$scope.chqTypeValuePrint = "computerised";
			//$scope.chqReqPrintTypeFlag = true;
			$scope.chqReq.cReqPrintType = 'L';
			$('#chqReqPrintTypeId').removeAttr("disabled");
			
		}
	}
	
	$scope.cancelPrint = function() {
		console.log("cancelPrint()");
		$('div#printChqReqDB').dialog('close');
	}
	
	$scope.yesPrint = function() {
		console.log("yesPrint()");
		$('div#printChqReqDB').dialog('close');
		$window.print();
		/*$window.*/
	}
	
	$('#chqReqNOBookId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#chqReqNOBookId').keypress(function(e) {
		if (this.value.length == 5) {
			e.preventDefault();
		}
	});
	
	$('#chqReqNOChqPerBookId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#chqReqNOChqPerBookId').keypress(function(e) {
		if (this.value.length == 5) {
			e.preventDefault();
		}
	});
	
	/*$window.onbeforeunload = closeWindowEvent;
	function closeWindowEvent(){
	   console.log("window close event fired");
	   return null;
	}*/
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getAllBranchesList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("HoChqRequestCntlr Ended");
}]);