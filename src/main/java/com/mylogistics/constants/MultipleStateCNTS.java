package com.mylogistics.constants;

public class MultipleStateCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
	
	public static final String MULTI_STATE_CODE = "mulStateStateCode";
	
	public static final String STATE_REF_CODE = "mulStateRefCode";
	
	
}
