package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BillPrintCntlr {

	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private BillDAO billDAO;
	

	@RequestMapping(value = "/getBlBranch", method = RequestMethod.POST)
	public @ResponseBody Object getBlBranch() {
		System.out.println("enter into getBlBranch function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String,Object>> brhList = branchDAO.getNameAndCode();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/blPrintSubmit", method = RequestMethod.POST)
	public @ResponseBody Object blPrintSubmit(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into blPrintSubmit function");
		Map<String,Object> map = new HashMap<>();
		int brhId = Integer.parseInt(String.valueOf(clientMap.get("brhId")));
		String frBlNo = String.valueOf(clientMap.get("frBlNo"));
		String toBlNo = String.valueOf(clientMap.get("toBlNo"));
		System.out.println("frBlNo="+frBlNo+" "+"toBLBlNo="+toBlNo);
		
		if(brhId > 0){
			if(frBlNo.equalsIgnoreCase(toBlNo)){
				List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
				blList = billDAO.getSingleBlPrint(brhId,frBlNo);
				System.out.println("size="+blList.size());
				if(!blList.isEmpty()){
					System.out.println("size of blList = "+blList.size());
					map.put("list",blList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("msg","Branch Does not have any bill");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			
			}else if(Integer.parseInt(frBlNo.substring(3)) > Integer.parseInt(toBlNo.substring(3))){
				map.put("msg","From Bill No must be less than To Bill No");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}else if(Integer.parseInt(frBlNo.substring(3)) > 0 && Integer.parseInt(toBlNo.substring(3)) > 0){
				List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
				blList = billDAO.getBlFrPrint(brhId,frBlNo,toBlNo);
				System.out.println("size="+blList.size());
				if(!blList.isEmpty()){
					System.out.println("size of blList = "+blList.size());
					map.put("list",blList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("msg","Branch Does not have any bill");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg","Invalid Bill No");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","Invalid Branch");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}
