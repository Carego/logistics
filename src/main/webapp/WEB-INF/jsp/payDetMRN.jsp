<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="pdmrForm" ng-submit=pdmrSubmit(pdmrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
		ng-class = "{'form-error': pdmrForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row"  style = "margin-bottom:-100px;">
			
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" ng-blur="chngDt()" readonly ng-required="true" >
		       			<label for="code" style="color:black;">CS Date</label>
		       			<div class = "text-left errorMargin" ng-messages = "pdmrForm.csDtName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  CS date is required.</span>
							</div>	
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code" style="color:black;">Customer Name</label>
		       			<div class = "text-left errorMargin" ng-messages = "pdmrForm.custName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  CS date is required.</span>
							</div>		
		       		</div>
					
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="pdMRId" name ="pdMRName" ng-model="selOAMR.mrNo" ng-click="selectPDMR()" readonly>
		       			<label for="code" style="color:black;">Pending MR No.</label>
		       			<div class = "text-left errorMargin" ng-if = "isFormSubmitted && pdmr.mrFrPDList.length == 0">
								<span>*  MR is required.</span>
							</div>			
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="billId" name ="billName" ng-model="bill.blBillNo" ng-click="selectBill()"  readonly>
		       			<label for="code" style="color:black;">Select Bill No.</label>
		       			<div class = "text-left errorMargin" ng-if = "isFormSubmitted && subPdmrList.length == 0 ">
								<span>*  Bill is required.</span>
							</div>		
		       		</div>
		     		
		    </div>
		    
		    <div class="row" style = "margin-bottom:-100px;">
		    	<div class="input-field col s12">
				 <div ng-show="pdmr.mrFrPDList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Payment Detail Of ON Accounting MR</caption>
						<tr class="rowclr">
							<th class="colclr">OA MR NO.</th>
							<th class="colclr">Detail Amt</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="item in pdmr.mrFrPDList">
							<td class="rowcel">{{item.onAccMr}}</td>
							<td class="rowcel">{{item.detAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeDetMR($index)" /></td>
						</tr>
						<tr>
							<td></td>
							<td>TOTAL</td>
							<td class="rowcel">{{totOAAmt}}</td>
						</tr>
					</table>
				</div>
				</div>
				
				<div class="input-field col s12">
				 <div ng-show="subPdmrList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Payment Detail Money Receipt</caption>
						<tr class="rowclr">
							<th class="colclr">Bill No.</th>
							<th class="colclr">Bill Date</th>
							<th class="colclr">Freight</th>
							<th class="colclr">Service Tax</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Excess</th>
							<th class="colclr">Deduction</th>
							<th class="colclr">Ded Reason</th>
							<th class="colclr">Net Payment</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="subPmr in subPdmrList">
							<td class="rowcel">{{subPmr.bill.blBillNo}}</td>
							<td class="rowcel">{{subPmr.bill.blBillDt}}</td>
							<td class="rowcel">{{subPmr.mrFreight}}</td>
							<td class="rowcel">{{subPmr.mrSrvTaxAmt}}</td>
							<td class="rowcel">{{subPmr.mrTdsAmt}}</td>
							<td class="rowcel">{{subPmr.mrAccessAmt}}</td>
							<td class="rowcel">{{subPmr.mrDedAmt}}</td>
							<td class="rowcel">{{subPmr.mrDedRsn}}</td>
							<td class="rowcel">{{subPmr.mrNetAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removePDMR($index)" /></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>TOTAL</td>
							<td class="rowcel">{{totRecAmt}}</td>
						</tr>
					</table>
				</div>
				</div>
				
    		 </div>	
		    
		     <div class="row">
				<div class="input-field col s12">
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="pdmr.mrDesc"
 						ng-required = "true" ng-minlength = "2" ng-maxlength = "200"></textarea>
 					<label style="color:black;">Description</label>
 					<div class = "text-left errorMargin" ng-messages = "pdmrForm.desc.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Description is required.</span>
								<span ng-message = "minlength">*  Description should be greater than 1.</span>
								<span ng-message = "maxlength">*  Description should be less than 200.</span>
							</div>
				</div>
         	</div>	
         
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div> 
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="selPDMRId" ng-hide="selPDMRFlag">
		  <input type="text" name="filterMR" ng-model="filterMR" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>MR NO</th>
 	  	  	  <th>Date</th>
 	  	  	  <th>Net Payment</th>
 	  	  	  <th>Rem Amt</th>
 	  	  	  <th>Detail Amt</th>
 	  	  	  <th>Action</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="mr in mrList | filter:filterMR">
		 	  <td><input type="checkbox"  name="mr"   value="{{ mr }}" ></td>
              <td>{{mr.mrNo}}</td>
               <td>{{mr.mrDate | date:'dd/MM/yyyy'}}</td>
              <td>{{mr.mrNetAmt}}</td>
              <td>{{mr.mrRemAmt}}</td>
              <td><input type="number" name="mrAmtName" id="maAmtId[$index]" ng-model="mrAmt[$index]" ng-blur="checkAmt($index,mr.mrRemAmt,mrAmt[$index])"></td>
              <td><input type="button" name="saveMrAmtName" id="saveMrAmtId[$index]" value="Submit" ng-click="saveMR(mrAmt[$index],mr,$index)"></td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBillId" ng-hide="selBillFlag">
		  <input type="text" name="filterBill" ng-model="filterBill" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bill No</th>
 	  	  	  <th>Bill Date</th>
 	  	  	  <th>Final Amount</th>
 	  	  	  <th>Pending Amount</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bill in blList | filter:filterBill">
		 	  <td><input type="radio"  name="bill"   value="{{ bill }}" ng-model="billCode" ng-click="saveBill(bill)"></td>
              <td>{{bill.blBillNo}}</td>
              <td>{{bill.blBillDt}}</td>
              <td>{{bill.blFinalTot | number : 2}}</td>
              <td>{{bill.blRemAmt | number : 2}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="dedDetId" ng-hide="dedDetFlag">
 	  <form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="dedDetForm" ng-submit="submitDedDet(dedDetForm)"
 	  ng-class = "{'form-error': dedDetForm.$invalid && isdedDetFormSubmitted}" novalidate = "novalidate">
		 <div class="row">
		 		
		 		<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="cnmtId" name ="cnmtName" ng-model="cnmtCode" ng-click="selectCnmt()" readonly ng-required="true">
		       			<label for="code" style="color:black;">Select Cnmt No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "dedDetForm.cnmtName.$error" ng-if = "isdedDetFormSubmitted">
								<span ng-message = "required">*  CNMT is required.</span>
							</div>
		       	</div>
		       	
		 		<div class="col s4 input-field">
					<select name="dedTypeName" id="dedTypeId" ng-model="dedType" ng-init="dedType = 'FREIGHT RATE'" ng-required="true">
							<option value='FREIGHT RATE'>FREIGHT RATE</option>
							<option value='FREIGHT WEIGHT'>FREIGHT WEIGHT</option>
							<option value='LATE DELIVERY'>LATE DELIVERY</option>
							<option value='COOLIE LOADING'>COOLIE LOADING</option>
							<option value='COOLIE UNLOADING'>COOLIE UNLOADING</option>			
							<option value='DETENTION'>DETENTION</option>
							<option value='CLAIM SHORTAGE'>CLAIM SHORTAGE</option>
							<option value='CLAIM DAMAGE'>CLAIM DAMAGE</option>
							<option value='NON PLACEMENT'>NON PLACEMENT</option>
							<option value='OTHERS'>OTHERS</option>
					</select>
					<label style="color:black;">Deduction Type</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="dedName" id="dedId" ng-model="dedAmt" step="0.001" min="0.000" ng-required="true"/>
					<label style="color:black;">Deduction Amount</label>
					<div class = "text-left errorMargin" ng-messages = "dedDetForm.dedName.$error" ng-if = "isdedDetFormSubmitted">
								<span ng-message = "required">*  Deduction amount is required.</span>
							</div>
				</div>
				
	     </div>
	  	     
	     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	    </form> 
	</div>
	
	
	<div id ="selCnmtId" ng-hide="selCnmtFlag">
		  <input type="text" name="filterCnmt" ng-model="filterCnmt" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>CNMT NO</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cnmt in cnmtList | filter:filterCnmt">
		 	  <td><input type="radio"  name="cnmt"   value="{{ cnmt }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt)"></td>
              <td>{{cnmt}}</td>
          </tr>
       </table>
	</div>
	
	
	<div id="billInfoId" ng-hide="billInfoFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;;" name="billInfoForm" ng-submit="submitBillInfo(billInfoForm)"
	 ng-class = "{'form-error': dedDetForm.$invalid && isbillInfoFormSubmitted}" novalidate = "novalidate">
		 <div class="row">
		 		<div class="col s4 input-field">
					<input class="validate" type ="number" name="frtAmtName" id="frtAmtId" ng-model="subPdmr.mrFreight" step="0.001" min="0.000" ng-required="true" ng-readonly='customer.custEditBillAmt'>
					<label style="color:black;">Freight Amount</label>
					<div class = "text-left errorMargin" ng-messages = "billInfoForm.frtAmtName.$error" ng-if = "isbillInfoFormSubmitted">
								<span ng-message = "required">*  Freight is required.</span>
							</div>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="tdsName" id="tdsId" ng-model="subPdmr.mrTdsAmt" step="0.001" min="0.000"/>
					<label style="color:black;">TDS</label>
				</div>
				
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="recAmtName" id="recAmtId" ng-model="subPdmr.mrNetAmt" step="0.001" min="0.000" ng-required="true">
					<label style="color:black;">Received Amount</label>
					<div class = "text-left errorMargin" ng-messages = "billInfoForm.recAmtName.$error" ng-if = "isbillInfoFormSubmitted">
								<span ng-message = "required">*  Received amount is required.</span>
							</div>
				</div>
			
	     </div>
	     
	     <div class="row">
	     		<div class="col s4 input-field">
					<input type ="number" name="dedName" id="dedId" ng-model="subPdmr.mrDedAmt" step="0.001" min="0.000">
					<label style="color:black;">Deduction Amount</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="button" name="dedBtn" id="dedBtnId" value="Deduction Detail" ng-click="addDedDetail()">
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="excName" id="excId" ng-model="subPdmr.mrAccessAmt" step="0.001" min="0.000"/>
					<label style="color:black;">Excess</label>
				</div>
	     </div>
	     
	    <div class="row">
	    	<div class="input-field col s12">
			 <div ng-show="dedDetailList.length > 0">
				<table class="tblrow">
				<caption class="coltag tblrow">Deduction Detail</caption>
					<tr class="rowclr">
						<th class="colclr">Cnmt No.</th>
						<th class="colclr">Dedution Type</th>
						<th class="colclr">Deduction Amount</th>
						<th class="colclr">Action</th>
					</tr>
					<tr class="tbl" ng-repeat="dedDetail in dedDetailList">
						<td class="rowcel">{{dedDetail.cnmtNo}}</td>
						<td class="rowcel">{{dedDetail.dedType}}</td>
						<td class="rowcel">{{dedDetail.dedAmt}}</td>
						<td class="rowcel"><input type="button" value="remove" ng-click="removeDed($index)" /></td>
					</tr>
				</table>
			</div>
			</div>
   		</div>	 
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	
</div>	