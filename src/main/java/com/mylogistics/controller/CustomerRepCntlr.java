package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.User;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;


@Controller
public class CustomerRepCntlr {

	@Autowired
	private CustomerRepresentativeDAO customerrepDAO; 

	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;

	@RequestMapping(value="/CustomerRepresentative",method = RequestMethod.POST)
	public @ResponseBody Object saveCustomerRepresentative(@RequestBody CustomerRepresentative custrep){

		custrep.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		custrep.setUserCode(currentUser.getUserCode());
		custrep.setbCode(currentUser.getUserBranchCode());

		String custcode="";
		String custRepCode=null;
		String faCode=null;
		/*String s=custrep.getCrName();
		String custname=s.substring(0,3);*/
		String lastFiveChar;

		/*Random rnd = new Random();
        int n = 1000 + rnd.nextInt(8900);*/
		Map<String,String> map = new HashMap<String,String>();

		long totalRows = customerrepDAO.totalCount();

		if (!(totalRows == -1)) {

			if(totalRows==0)
			{
				lastFiveChar="00001";
				custRepCode="1";

			}else{
				int customerid = customerrepDAO.getLastCustomerRep()+1;
				int end = 100000+customerid;
				lastFiveChar = String.valueOf(end).substring(1,6);
				custRepCode=String.valueOf(customerid);
			}
			System.out.println("The generated custRepCode -------------------"+custRepCode);
			String custRepCodeTemp = CodePatternService.custRepCodeGen(custrep, lastFiveChar);

			/*custcode= custname+n+lastFiveChar;*/
			custrep.setCrCode(custRepCode);
			custrep.setCrCodeTemp(custRepCodeTemp);
			String custCode=custrep.getCustCode();
			String custid=custCode.substring(5,10);
			int custidint=Integer.parseInt(custid);
			custrep.setCustId(custidint);
			
			List<FAParticular> faParticulars = faParticularDAO.getFAParticular("custrep");
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			custrep.setfAParticular(fAParticular);


			int temp=customerrepDAO.saveCustomerRepToDB(custrep);
			if (temp>=0) {
				int custRepId = temp;
				System.out.println("Owner saved with id------------------"+custRepId);
				custRepId=custRepId+100000;
				String custIdStr = String.valueOf(custRepId).substring(1,6);
				faCode=fapIdStr+custIdStr;
				System.out.println("faCode code is------------------"+faCode);
				custrep.setCrFaCode(faCode);
				String crCode = String.valueOf(temp);
				custrep.setCrCode(crCode);
				int updateCustomerRep= customerrepDAO.updateCustomerRepById(custrep);
				System.out.println("After updating owner----------------"+updateCustomerRep);
				
				/*FAMaster famaster = new FAMaster();
				famaster.setFaMfaCode(faCode);
				famaster.setFaMfaType("customerrepresentative");
				famaster.setFaMfaName(custrep.getCrName());
				famaster.setbCode(currentUser.getUserBranchCode());
				famaster.setUserCode(currentUser.getUserCode());
				System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				int saveFAMaster= faMasterDAO.saveFaMaster(famaster);*/
				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("targetPage","customer");
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}	
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}  

	@RequestMapping(value="/getCustomerCodeList",method = RequestMethod.POST)
	public @ResponseBody Object getCustomerCodeData(){

		List<String> custCodeList= customerDAO.getCustomerCode();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!custCodeList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", custCodeList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getCustListFV",method = RequestMethod.POST)
	public @ResponseBody Object getCustListFV(){
		System.out.println("enter into getCustListFV funciton");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Customer> custList= customerDAO.getCustomerFV();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!custList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", custList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	
	@RequestMapping(value="/getCustomerRepCodeList",method = RequestMethod.POST)
	public @ResponseBody Object getCustomerRepCodeList(){

		//List<String> custRepCodeList= customerrepDAO.getCustomerRepCode();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<CustomerRepresentative> custRepList = customerrepDAO.getAllCustRepByBrCode(currentUser.getUserBranchCode());
		Map<String,Object> map = new HashMap<String,Object>();
		if (!custRepList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", custRepList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	@RequestMapping(value="/CustomerRepresentativeNew",method = RequestMethod.POST)
	public @ResponseBody Object saveCustomerRepresentativeNew(@RequestBody CustomerRepresentative custrep){

		custrep.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		custrep.setUserCode(currentUser.getUserCode());
		custrep.setbCode(currentUser.getUserBranchCode());

		String custcode="";
		String s=custrep.getCrName();
		String custname=s.substring(0,3);
		String last;
		String faCode=null;

		Random rnd = new Random();
		int n = 1000 + rnd.nextInt(8900);
		Map<String,String> map = new HashMap<String,String>();

		long totalRows = customerrepDAO.totalCount();

		if (!(totalRows == -1)) {

			if(totalRows==0)
			{
				last="00001";
			}else{
				int customerid = customerrepDAO.getLastCustomerRep()+1;
				int end = 100000+customerid;
				last = String.valueOf(end).substring(1,6);
			}

			custcode= custname+n+last;
			custrep.setCrCodeTemp(custcode);

			String custCode=custrep.getCustCode();
			//String custid=custCode.substring(5,10);
			//int custidint=Integer.parseInt(custid);
			//custrep.setCustId(custidint);
			
			List<FAParticular> faParticulars = faParticularDAO.getFAParticular("custrep");
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			custrep.setfAParticular(fAParticular);


			int temp=customerrepDAO.saveCustomerRepToDB(custrep);
			if (temp>=0) {
				int custRepId = temp;
				System.out.println("Owner saved with id------------------"+custRepId);
				custRepId=custRepId+100000;
				String custIdStr = String.valueOf(custRepId).substring(1,6);
				faCode=fapIdStr+custIdStr;
				System.out.println("faCode code is------------------"+faCode);
				custrep.setCrFaCode(faCode);
				String crCode = String.valueOf(temp);
				custrep.setCrCode(crCode);
				int updateCustomerRep= customerrepDAO.updateCustomerRepById(custrep);
				System.out.println("After updating owner----------------"+updateCustomerRep);
				
				
				/*FAMaster famaster = new FAMaster();
				famaster.setFaMfaCode(faCode);
				famaster.setFaMfaType("customerrepresentative");
				famaster.setFaMfaName(custrep.getCrName());
				famaster.setbCode(currentUser.getUserBranchCode());
				famaster.setUserCode(currentUser.getUserCode());
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				int saveFAMaster= faMasterDAO.saveFaMaster(famaster);*/
				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("targetPage","customerrepresentativenew");
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}	
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}  


	@RequestMapping(value = "/getDesignationForCr" , method = RequestMethod.GET)  
	public @ResponseBody Object getDesignation(){

		String designation[] = {"Android Developer",
				"Clerk",
				"Content Writer",
				"Developer",
				"Director",
				"Human Resources",
				"Intern",
				"IOS Developer",
				"IT Professional",
				"Java Developer",
				"JavaScript Developer",
				"Manager",
				"Network Analyst",
				"Programmer",
				"Software Engineer",
				"Software Tester",
				"SEO Services",
				"UI Designer",
		};
		return designation;
	} 
}
