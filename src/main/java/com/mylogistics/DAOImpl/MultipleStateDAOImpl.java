package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.MultipleStateDAO;
import com.mylogistics.constants.MultipleStateCNTS;
import com.mylogistics.model.MultipleState;

public class MultipleStateDAOImpl implements MultipleStateDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(MultipleStateDAOImpl.class);
	
	@Autowired
	public MultipleStateDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveMultipleState(MultipleState multipleState){
		 logger.info("Enter into saveMultipleState() : State = "+multipleState);
		 int temp = 0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(multipleState);
			 transaction.commit();
			 temp = 1;
			 logger.info("MultipleState is saved !");
		 }catch(Exception e){
			 logger.error("Exception : "+e);
			 temp = -1;
		 }
		 session.flush();
		 session.clear();
		 session.close();
		 logger.info("Exit from saveMultipleState()");
		 return temp;
	 }
	

	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<MultipleState> getMultipleState(String Code){
		 List<MultipleState> multipleStates = new ArrayList<MultipleState>();
		 System.out.println("Enter into getContactPerson function--- "+Code);
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(MultipleState.class);   
	         cr.add(Restrictions.eq(MultipleStateCNTS.STATE_REF_CODE,Code));
	         multipleStates =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return multipleStates;
	}
	 
	 
	 @Transactional
	 public MultipleState deleteState(String code){
			System.out.println("code in dao is-----------------------"+code);
			
			MultipleState multipleState = new MultipleState();
		    try{
		    	session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(MultipleState.class);
				 cr.add(Restrictions.eq(MultipleStateCNTS.STATE_REF_CODE,code));
				 
				 multipleState = (MultipleState) cr.list().get(0);
				 session.delete(multipleState);
				 transaction.commit();
				 session.flush();
				
				 System.out.println("branchStockLeafDet Data is deleted successfully");	 
		    }catch (Exception e){
				e.printStackTrace();
			}
		    session.clear();
		    session.close();
			 return multipleState;
	   }
}
