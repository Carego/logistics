package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "narration")
public class Narration {

	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "narrId")
	private Integer narrId;
	
	@Column(name = "narrType", length = 4, nullable = false)
	private String narrType;
	
	@Column(name = "narrCode", length = 50, nullable = false)
	private String narrCode;
	
	@Column(name = "narrDesc", length = 200, nullable = false)
	private String narrDesc;
	
	@Column(name = "narrUserCode", length = 5, nullable = false)
	private String narrUserCode;
	
	@Column(name = "narrUserBCode", length = 50, nullable = false)
	private String narrUserBCode;
	
	@Column(name = "creationTS", insertable = false, updatable = true)
	private Calendar creationTS;

	public Integer getNarrId() {
		return narrId;
	}
	public void setNarrId(Integer narrId) {
		this.narrId = narrId;
	}
	public String getNarrType() {
		return narrType;
	}
	public void setNarrType(String narrType) {
		this.narrType = narrType;
	}
	public String getNarrCode() {
		return narrCode;
	}
	public void setNarrCode(String narrCode) {
		this.narrCode = narrCode;
	}
	public String getNarrDesc() {
		return narrDesc;
	}
	public void setNarrDesc(String narrDesc) {
		this.narrDesc = narrDesc;
	}
	public String getNarrUserCode() {
		return narrUserCode;
	}
	public void setNarrUserCode(String narrUserCode) {
		this.narrUserCode = narrUserCode;
	}
	public String getNarrUserBCode() {
		return narrUserBCode;
	}
	public void setNarrUserBCode(String narrUserBCode) {
		this.narrUserBCode = narrUserBCode;
	}
	public Calendar getCreationTS() {
		return creationTS;
	}
	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}	
		
	
}