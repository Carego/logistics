<div ng-show="operatorLogin || superAdminLogin">
<title>View Employee</title>
<!-- xvf2015asd00016 -->
<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
		<form name = "EmpForm" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="Submit(EmpForm,employ)" >
	<div id ="empCode">
			<div class="input-field col s6">
				<input type="text" name="empName" ng-model="employ.empName" required readonly ng-click="openEmpCodeDB()"/>
   				<input type="hidden" name="empCode" ng-model="employ.empCode"/>
   				<label>Enter Employee Code</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
	</div>
		</form>
		<div class="col s3"> &nbsp; </div>
</div>	

<div id ="empCodeDB" ng-hide="empCodeDBFlag">
 	  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Employee Code Temp">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  
 	  	  	  <!-- <th>Employee Code</th> -->
 	  	  	  
 	  	  	  <th>Employee Name</th>
 	  	  	  
 	  	  	  <th>Employee FaCode</th>
 	  	  	  
 	  	  	  <th>Employee Code</th>
 	  	  </tr>
		  <tr ng-repeat="emp in empCodeList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="empName"  class="empCls"  value="{{ emp }}" ng-model="empCode" ng-click="saveEmpCode(emp)"></td>
              <!-- <td>{{ emp.empCode }}</td> -->
              <td>{{ emp.empName }}</td>
              <td>{{ emp.empFaCode }}</td>
              <td>{{ emp.empCodeTemp }}</td>
          </tr>
      </table>
         
</div>

<%-- 	 <form ng-submit="Submit(empCode)" >
		<div id ="empCode">
			
				<table>
				
					<tr>
						<td>Enter Employee Code</td>
						<td><input type="text" name="empCode" ng-model="empCode"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
				
			
		</div>
		</form> --%>
	
	<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Employee <span class="teal-text text-lighten-2">{{employee.empName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Branch Code:</td>
	                <td><input type="text" id="empBrhId" name="empBrh" ng-model="employee.prBranch.branchFaCode" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Name:</td>
	                <td><input type="text" id="empNameId" name="empName" ng-model="employee.empName"></td>
	            </tr>
	            <tr>
	                <td>FaCode:</td>
	                <td><input type="text" id="empCodeId" name="empCode" ng-model="employee.empFaCode" readonly></td>
	            </tr>
	            <tr>
	                <td>Employee Code:</td>
	                <td><input type="text" id="empTempCodeId" name="empTempCode" ng-model="employee.empCodeTemp" readonly></td>
	            </tr>
	            <tr>
	                <td>Father Name:</td>
	                <td><input type="text" id="empFaNameId" name="empFaName" ng-model="employee.empFatherName"></td>
	            </tr>
	            <tr>
	            	<td>Sex:</td>
	            	<td><select name="empSex" ng-model="employee.empSex" required>
                    	<option value="Male">Male</option>
                    	<option value="Female">Female</option>   
                     	</select>
                     </td>
	            </tr>
	             <tr>
	                <td>Educational Qualification:</td>
	                <td><input id="empEduQualification" class="validate" type ="text" name ="empEduQuali" ng-model="employee.empEduQuali"  ng-minlength="2" ng-maxlength="255"></td>
	            </tr>
	             <tr>
	                <td>Date Of Birth:</td>
	                <td><input type ="date" name ="empDOB" ng-model="employee.empDOB" ng-required="true"></td>
	            </tr>
	            <tr>
	                <td>Date Of Appointment:</td>
	                <td><input type ="date" name ="empDOAppointment" ng-model="employee.empDOAppointment" ></td>
	            </tr>
	            <tr>
	                <td>Designation:</td>
	                <td><input type="text" id="empDesgId" name="empDesg" ng-model="employee.empDesignation"/></td>
	            </tr>
	            <tr>
	                <td>Nominee:</td>
	                <td><input type="text" id="empNomId" name="empNom" ng-model="employee.empNominee"/></td>
	            </tr>
	            <tr>
	                <td>Address:</td>
	                <td><input type="text" id="empAddId" name="empAdd" ng-model="employee.empAdd"/></td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td><input type="text" id="empCityId" name="empCity" ng-model="employee.empCity"/></td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td><input type="text" id="empStId" name="empSt" ng-model="employee.empState"/></td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td><input type="text" id="empPin" name="empPin" ng-model="employee.empPin"/></td>
	            </tr>
	            <tr>
	                <td>Present Address:</td>
	                <td><input type="text" id="empPresentAdd" name ="empPresentAdd" ng-model="employee.empPresentAdd"  ng-minlength="3" ng-maxlength="255" ></td>
	            </tr>
	            
	            <tr>
	                <td>City:</td>
	                <td><input class="validate" type ="text" id="empPresentCity" name ="empPresentCity" ng-model="employee.empPresentCity"  ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            
	            <tr>
	                <td>State:</td>
	                <td><input class="validate" type ="text" id="empPresentState" name ="empPresentState" ng-model="employee.empPresentState"  ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            
	            <tr>
	                <td>Present Pin:</td>
	                <td><input class="validate" type ="text" id="empPresentPin" name ="empPresentPin" ng-model="employee.empPresentPin"  ng-minlength="6" ng-maxlength="6" ></td>
	            </tr>
	            <tr>
	                <td>Basic:</td>
	                <td><input type="number" name ="empBasic" id="empBasic" ng-model="employee.empBasic" ng-keyup="calculateOnBasic()"  step="0.01" min="0.00" /></td>
	                
	            </tr>
	            <tr>
	                <td>HRA:</td>
	                <td><input type ="text" id="empHRAId" name ="empHRA" ng-model="employee.empHRA" ng-init="employee.empHRA=0" step="0.01" min="0.00" /></td>
	            </tr>
	            <tr>
	                <td>Other Allowance:</td>
	                <td><input type ="number" name ="empOtherAllowance" id="empOtherAllowance" ng-model="employee.empOtherAllowance" ng-init="employee.empOtherAllowance=0.00" step="0.01" min="0.00" /></td>
	            </tr>
	            <tr>
	                <td>Gross Salary:</td>
	                <td><input type ="text"  name ="empGross" ng-model="employee.empGross" ng-click="calGross()" readonly ng-required="true"></td>
	            </tr>
	             <tr>
	                <td>TDS:</td>
	                <td><input type ="text" name ="tdsName" ng-model="employee.empTds" ng-init="employee.empTds=0">	</td>
	            </tr>
	             <tr>
	                <td>ESI:</td>
	                <td><input class="validate " type ="text" id="empESI" name ="empESI" ng-model="employee.empESI" ng-init="employee.empESI=0" readonly></td>
	            </tr>
	            <tr>
	                <td>ESI No:</td>
	                <td><input class="validate" type ="text" id="empESINo" name ="empESINo" ng-model="employee.empESINo" ng-minlength="3" ng-maxlength="40" ></td>
	            </tr>
	            <tr>
	                <td>ESI Dispensary:</td>
	                <td><input class="validate" type ="text" id="empESIDispensary" name ="empESIDispensary" ng-model="employee.empESIDispensary" ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            <tr>
	                <td>PF:</td>
	                <td><input class="validate" type ="text" name ="empPF" ng-model="employee.empPF" readonly ></td>
	            </tr>
	             <tr>
	                <td>PF No:</td>
	                <td><input class="validate" type ="text" name ="empPF" ng-model="employee.empPFNo"></td>
	            </tr>
	            <tr>
	                <td>Net Salary:</td>
	                <td><input type ="text" name ="netSalary" ng-model="employee.netSalary" ng-click="calNetSal()" readonly ng-required="true"></td>
	            </tr>
	            <tr>
	                <td>Employer PF:</td>
	                <td><input class="validate" type ="text" name ="emplrPF" ng-model="employee.empPFEmplr" readonly ></td>
	            </tr>
	            <tr>
	                <td>Employer ESI:</td>
	                <td><input class="validate " type ="text" id="emplrESI" name ="emplrESI" ng-model="employee.empESIEmplr" readonly></td>
	            </tr>
	            <tr>
	                <td>Loan Balance:</td>
	                <td><input class="validate" type ="number" id="empLoanBal" name ="empLoanBal" ng-model="employee.empLoanBal"  step="0.01" min="0.00"></td>
	            </tr>
	            <tr>
	                <td>Loan Payment:</td>
	                <td><input class="validate" type ="number" id="empLoanPayment" name ="empLoanPayment" ng-model="employee.empLoanPayment"  step="0.01" min="0.00"></td>
	            </tr>
	            <tr>
	                <td>License No:</td>
	                <td><input class="validate" type ="text" id="empLicenseNo" name ="empLicenseNo" ng-model="employee.empLicenseNo"  ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            <tr>
	                <td>License Exp:</td>
	                <td><input class="validate" type ="date" name ="empLicenseExp" ng-model="employee.empLicenseExp" ></td>
	            </tr>
	            <tr>
	                <td>Mail Id:</td>
	                <td><input class="validate" type ="email" id="empMailId" name ="empMailId" ng-model="employee.empMailId"  ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40"></td>
	            </tr>
	            <tr>
	                <td>Phone No:</td>
	                <td><input class="validate" type ="text" id="empPhNo" name ="empPhNo" ng-model="employee.empPhNo" ng-minlength="4" ng-maxlength="15" min="0"></td>
	            </tr>
	             <tr>
	                <td>Telephone Amount:</td>
	                <td><input class="validate" type ="number" id="empTelephoneAmt" name ="empTelephoneAmt" ng-model="employee.empTelephoneAmt"  step="0.01" min="0.00"></td>
	            </tr>
	            <tr>
	                <td>Mobile Amount:</td>
	                <td><input class="validate" type ="number" id="empMobAmt" name ="empMobAmt" ng-model="employee.empMobAmt" step="0.01" min="0.00"></td>
	            </tr>
	            <tr>
	                <td>Bank Account No:</td>
	                <td><input class="validate" type ="text" id="empBankAcNo" name ="empBankAcNo" ng-model="employee.empBankAcNo"  ng-minlength="10" ng-maxlength="40" min="0"></td>
	            </tr>
	            <tr>
	                <td>Bank Name:</td>
	                <td><input class="validate" type ="text" id="empBankName" name ="empBankName" ng-model="employee.empBankName" ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            <tr>
	                <td>Bank Branch:</td>
	                <td><input class="validate" type ="text" id="empBankBranch" name ="empBankBranch" ng-model="employee.empBankBranch" ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            <tr>
	                <td>Bank IFSC:</td>
	                <td><input class="validate" type ="text" id="empBankIFS" name ="empBankIFS" ng-model="employee.empBankIFS"  ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            <tr>
	                <td>Pan No:</td>
	                <td><input class="validate" type ="text" id="empPanNo" name ="empPanNo" ng-model="employee.empPanNo"  ng-minlength="10" ng-maxlength="10" ng-pattern="/^[a-zA-Z0-9]*$/"></td>
	            </tr>
	            <tr>
	                <td>IsTerminate:</td>
	                <td><input type="text" id="empAddId" name="empAdd" ng-model="employee.isTerminate" readonly/></td>
	            </tr>
	                 
	            <!-- <tr>
	                <td>CreationTS:</td>
	                <td>{{employee.creationTS | date:'dd/MM/yyyy'}}</td>
	            </tr>  -->   
	            
	            
	           <!--   <tr>
	                <td></td>
	                <td><input type="button" value="UPDATE" ng-click="updateEmp(employee)"/></td>
	            </tr>     -->                                                                                                                                                                                                         
	           
	         	<!-- <tr>
	                <td><input type="hidden" name ="empId" value={{employee.empId}}></td>
	            </tr> -->
	         
</table>
		
		<input type="button" value="UPDATE" ng-click="updateEmp(employee)"/>
</div>
</div>
</div>