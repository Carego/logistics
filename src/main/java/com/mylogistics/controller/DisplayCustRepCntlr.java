package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;


@Controller
public class DisplayCustRepCntlr {

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private CustomerRepresentativeDAO customerRepresentativeDAO;

	@Autowired
	private CustomerDAO customerDAO;


	@RequestMapping(value = "/getCustRepIsViewNo", method = RequestMethod.POST)
	public @ResponseBody Object getCustRepIsViewNo() {
		System.out.println("enter into getCustRepIsViewNo function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> custList = customerDAO.getAllCustCode(currentUser.getUserBranchCode());
		List<CustomerRepresentative> finalCustRepList = new ArrayList<CustomerRepresentative>();
		if(!custList.isEmpty()){
			for(int i=0;i<custList.size();i++){
				List<CustomerRepresentative> custRepList = customerRepresentativeDAO.getCustRepIsViewNo(custList.get(i));
				if(!custRepList.isEmpty()){
					for(int j=0;j<custRepList.size();j++){
						finalCustRepList.add(custRepList.get(j));
					}
				}
			}
		}else{
			System.out.println("no customer avaliable");
		}

		if(!finalCustRepList.isEmpty()){
			map.put("list", finalCustRepList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		return map;
	}


	@RequestMapping(value="/updateIsViewCustRep",method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewCustRep(@RequestBody String select){
		System.out.println("enter into updateIsViewCustomer function");
		String selection[]=select.split(",");
		int temp = customerRepresentativeDAO.updateCustRepIsView(selection);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}


	@RequestMapping(value = "/getAllCustRepCodes", method = RequestMethod.POST)
	public @ResponseBody Object getAllCustRepCodes() {
		System.out.println("enter into getAllCustRepCodes function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> custList = customerDAO.getAllCustCode(currentUser.getUserBranchCode());
		System.out.println("size of custList = "+custList.size());
		List<String> finalCustRepList = new ArrayList<String>();
		if(!custList.isEmpty()){
			for(int i=0;i<custList.size();i++){
				List<String> custRepList = customerRepresentativeDAO.getAllCustRepCode(custList.get(i));
				if(!custRepList.isEmpty()){
					for(int j=0;j<custRepList.size();j++){
						finalCustRepList.add(custRepList.get(j));
					}
				}
			}
		}else{
			System.out.println("no customer avaliable");
		}

		if(!finalCustRepList.isEmpty()){
			map.put("list", finalCustRepList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		return map;
	}


	@RequestMapping(value = "/getCustRepByCode", method = RequestMethod.POST)
	public @ResponseBody Object getCustRepByCode(@RequestBody String custRepCode) {
		System.out.println("enter into getCustRepByCode function");
		Map<String,Object> map = new HashMap<String, Object>();
		CustomerRepresentative customerRepresentative = customerRepresentativeDAO.getCustRepByCode(custRepCode);
		if(customerRepresentative != null){
			map.put("custrep",customerRepresentative);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/getDesignationForECr" , method = RequestMethod.GET)  
	public @ResponseBody Object getDesignation(){

		String designation[] = {"Android Developer",
				"Clerk",
				"Content Writer",
				"Developer",
				"Director",
				"Human Resources",
				"Intern",
				"IOS Developer",
				"IT Professional",
				"Java Developer",
				"JavaScript Developer",
				"Manager",
				"Network Analyst",
				"Programmer",
				"Software Engineer",
				"Software Tester",
				"SEO Services",
				"UI Designer",
		};
		return designation;
	} 

	@RequestMapping(value="/editCustomerRepSubmit", method = RequestMethod.POST)
	public @ResponseBody Object saveeditcontract(@RequestBody CustomerRepresentative custrep){
		System.out.println("enter into editCustomerRepSubmit function");
		int temp=customerRepresentativeDAO.updateCustomerRepByCrCode(custrep);
		Map<String,Object> map = new HashMap<String , Object>();
		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	
		return map;
	}
}
