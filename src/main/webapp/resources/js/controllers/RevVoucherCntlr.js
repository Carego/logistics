'use strict';

var app = angular.module('application');

app.controller('RevVoucherCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.vs = {};
	$scope.csList = [];
	
	$scope.vhNoList = [];
	
	$scope.reqCsList = [];
	
	
	$scope.cStmtDBFalg = true;
	$scope.dateTemp = "";
	
	var vouchType = "";
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		var response = $http.post($scope.projectName+'/getVDetFrRV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.csList = data.csList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.REV_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				if($scope.csList.length > 0){
				
					for(var i=0;i<$scope.csList.length;i++){
						$scope.vhNoList.push($scope.csList[i].csVouchNo);
					}
					
					console.log("$scope.vhNoList = "+$scope.vhNoList);
					
					for(var i=0;i<$scope.vhNoList.length;i++){
						for(var j=i+1;j<$scope.vhNoList.length;j++){
							if($scope.vhNoList[i] == $scope.vhNoList[j]){
								$scope.vhNoList.splice(j,1);
								j=j-1;
							}
						}
					}
					
					console.log("$scope.vhNoList = "+$scope.vhNoList);
				}				
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.openCStmtDB = function(){
		console.log("enter into openCStmtDB function = ");
		if($scope.csList.length < 1){
			$scope.alertToast("Open vouchers are not available for "+$scope.dateTemp);
		}else{
			$scope.cStmtDBFalg = false;
			$('div#cStmtDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Select Voucher No",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        //$scope.cStmtDBFalg = true;
			    }
				});
			
			$('div#cStmtDB').dialog('open');	
		}
	}
	
	
	
	$scope.saveVouchNo = function(vhNo){
		console.log("enter into saveVouchNo function = "+vhNo);
		$scope.vouchNo = parseInt(vhNo);
		$scope.vouchNo = parseInt($scope.vouchNo);
		$('div#cStmtDB').dialog('close');	
		$scope.reqCsList = [];
		for(var i=0;i<$scope.csList.length;i++){
			if($scope.csList[i].csVouchNo == vhNo){
				$scope.reqCsList.push($scope.csList[i]);
			}
		}
		
		$('#getDetId').removeAttr("disabled"); 
	}
	
	
	
	$scope.getDetails = function(){
		console.log("enter into getDetails funciton");
		vouchType = "";
		for(var i=0;i<$scope.reqCsList.length;i++){
			vouchType = $scope.reqCsList[0].csType;
			break;
		}
		
		console.log("vouchType = "+vouchType);
		
		if(vouchType === $scope.CASH_WITHDRAW){
			$scope.alertToast("No Subfile Details for CashWithdraw Voucher");
		}else if(vouchType === $scope.CASH_RECIEPT){
			$scope.alertToast("No Subfile Details for CashReciept Voucher");
		}else if(vouchType === $scope.CASH_DEPOSITE){
			$scope.alertToast("No Subfile Details for CashDeposite Voucher");
		}else if(vouchType === $scope.CASH_PAYMENT){
			$scope.alertToast("No Subfile Details for CashPayment Voucher");
		}else if(vouchType === $scope.JR_VOUCHER){
			$scope.alertToast("No Subfile Details for Journal Voucher");
		}else if(vouchType === $scope.TEL_VOUCHER){
			$scope.getTelVSubFile();
		}else if(vouchType === $scope.ELE_VOUCHER){
			$scope.getEleVSubFile();
		}else if(vouchType === $scope.BP_VOUCHER){
			$scope.getBPVSubFile();
		}else if(vouchType === $scope.VEH_VOUCHER){
			$scope.getVehVSubFile();
		}else if(vouchType === $scope.TRV_VOUCHER){
			$scope.getTrvSubFile();
		}else{
			
		}
		
	}
	
	
	$scope.getTelVSubFile = function(){
		console.log("enter into getTelVSubFile funciton");
		var response = $http.post($scope.projectName+'/getTelVSubFile',$scope.reqCsList);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stmList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}

	
	$scope.getEleVSubFile = function(){
		console.log("enter into getEleVSubFile funciton");
		var response = $http.post($scope.projectName+'/getEleVSubFile',$scope.reqCsList);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.semList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.getBPVSubFile = function(){
		console.log("enter into getBPVSubFile function");
		var response = $http.post($scope.projectName+'/getBPVSubFile',$scope.reqCsList);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.fbpList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getVehVSubFile = function(){
		console.log("enter into getVehVSubFile function");
		var response = $http.post($scope.projectName+'/getVehVSubFile',$scope.reqCsList);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.tDetList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getTrvSubFile = function(){
		console.log("enter into getTrvSubFile funciton");
		var response = $http.post($scope.projectName+'/getTrvSubFile',$scope.reqCsList);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.tDetList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.voucherSubmit = function(revVoucherForm){
		console.log("enter into voucherSubmit function = "+revVoucherForm.$invalid);
		if(revVoucherForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			$('#saveId').attr("disabled","disabled");
			if(!angular.isNumber($scope.vouchNo) || angular.isUndefined($scope.vouchNo)){
				$scope.alertToast("please select a vocher number");
			}else{
				/*
				var revVouch = {
						"vhNo"   : $scope.vouchNo,
						"vhType" : vouchType
				}*/
				var response = $http.post($scope.projectName+'/submitRevVouch',$scope.reqCsList);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						$('#saveId').removeAttr("disabled");
						$scope.alertToast("successfully reverse voucher "+$scope.vouchNo);
						$scope.vs = {};
						$scope.csList = [];
						
						$scope.vhNoList = [];
						$scope.reqCsList = [];
						//$scope.vouchNo 
						$scope.vouchNo = "";
						$scope.getVoucherDetails();
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}
		}
	}
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	}else if($scope.logoutStatus === true){
		 $location.path("/");
	}else{
		 console.log("****************");
	}
}]);