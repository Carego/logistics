package com.mylogistics.controller;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.hyphenation.TernaryTree.Iterator;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Narration;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.InvAndDateService;
import com.mylogistics.services.InvoiceService;
import com.mylogistics.services.InvoiceServiceImpl;
import com.mylogistics.services.ModelService;
//import java.util.Date;

@Controller

public class CnmtCntlr {

	@Autowired
	private CnmtDAO cnmtDAO;

	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;

	@Autowired
	private DailyContractDAO dailyContractDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private EmployeeDAO employeeDAO;

	@Autowired
	private StateDAO stateDAO;

	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private BranchSStatsDAO branchSStatsDAO;

	@Autowired
	private BranchMUStatsDAO branchMUStatsDAO;

	@Autowired
	private ContToStnDAO contToStnDAO;

	@Autowired
	private RateByKmDAO rateByKmDAO;
	
	@Autowired
	private CustomerGroupDAO customerGroupDAO;

	private InvoiceService invoiceService = new InvoiceServiceImpl();

	private ModelService modelService = new ModelService();
	
	private Blob cnmtImgBlob = null;
	
	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private List cnmtFrmToStnList = new ArrayList<>();
	
	private String cnmtImgPath = "/var/www/html/Erp_Image/CNMT";
	
	public static Logger logger = Logger.getLogger(CnmtCntlr.class);

	@RequestMapping(value="/getBranchDataForCnmt",method = RequestMethod.POST)
	public @ResponseBody Object getBranchData(){
		
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!branchList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",branchList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;		
	}

	@RequestMapping(value="/getCustomerList",method = RequestMethod.GET)
	public @ResponseBody Object getCustomerList(){
		System.out.println("enter into getCustomerList function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Customer> customerList = customerDAO.getCustomerForCnmt(currentUser.getUserBranchCode());
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%customerList = "+customerList.size());
		Map<String,Object> map = new HashMap<String,Object>();
		if (!customerList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",customerList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}


	@RequestMapping(value="/getConsigneeList",method = RequestMethod.POST)
	public @ResponseBody Object getConsigneeList(){
		System.out.println("enter into getConsigneeList function");
		List<Customer> customerList = customerDAO.getCustomerCodeNameContract();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!customerList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",customerList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}




	/*@RequestMapping(value="/getContractData",method = RequestMethod.POST)
	public @ResponseBody Object getContractData(@RequestBody Map<String,Object> clientMap){

		System.out.println("--------->>>>>>"+clientMap.get("custCode"));
		System.out.println("--------->>>>>>"+clientMap.get("cnmtDt"));
		System.out.println("--------->>>>>>"+clientMap.get("cnmtFromSt"));
		String code = (String) clientMap.get("custCode");
		String date = (String) clientMap.get("cnmtDt");
		String stn = (String) clientMap.get("cnmtFromSt");
		Map<String,List<Map<String, Object>>> finalMap = new HashMap<String,List<Map<String, Object>>>();
		List<Map<String, Object>> regDailyContList = new ArrayList<Map<String,Object>>();

		List<RegularContract>  regularContractList = regularContractDAO.getRegContractCode(cnmt);
		List<DailyContract> dailyContractList = dailyContractDAO.getDailyContractCode(cnmt);

		if (!regularContractList.isEmpty()) {
			for (int i = 0; i < regularContractList.size(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("custCode",regularContractList.get(i).getRegContBLPMCode());
				map.put("contCode", regularContractList.get(i).getRegContCode());
				map.put("fromStation", regularContractList.get(i).getRegContFromStation());
				map.put("toStation", regularContractList.get(i).getRegContToStation());
				map.put("fromDate", regularContractList.get(i).getRegContFromDt());
				map.put("toDate", regularContractList.get(i).getRegContToDt());
				map.put("rate", regularContractList.get(i).getRegContRate());
				map.put("cnmtDC", regularContractList.get(i).getRegContDc());
				map.put("cnmtDDL", regularContractList.get(i).getRegContDdl());
				map.put("cnmtCostGrade", regularContractList.get(i).getRegContCostGrade());
				map.put("cnmtKilometer",regularContractList.get(i).getRegContType());
				map.put("fromWeight",regularContractList.get(i).getRegContFromWt());
				map.put("toWeight",regularContractList.get(i).getRegContToWt());
				regDailyContList.add(map);
			}
		}

		if (!dailyContractList.isEmpty()) {
			for (int i = 0; i < dailyContractList.size(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("custCode",dailyContractList.get(i).getDlyContBLPMCode());
				map.put("contCode", dailyContractList.get(i).getDlyContCode());
				map.put("fromStation", dailyContractList.get(i).getDlyContFromStation());
				map.put("toStation", dailyContractList.get(i).getDlyContToStation());
				map.put("fromDate", dailyContractList.get(i).getDlyContStartDt());
				map.put("toDate", dailyContractList.get(i).getDlyContEndDt());
				map.put("rate", dailyContractList.get(i).getDlyContRate());
				map.put("cnmtDC", dailyContractList.get(i).getDlyContDc());
				map.put("cnmtDDL", dailyContractList.get(i).getDlyContDdl());
				map.put("cnmtCostGrade", dailyContractList.get(i).getDlyContCostGrade());
				map.put("cnmtKilometer",dailyContractList.get(i).getDlyContType());
				map.put("fromWeight",dailyContractList.get(i).getDlyContFromWt());
				map.put("toWeight",dailyContractList.get(i).getDlyContToWt());
				regDailyContList.add(map);
			}
		}
		finalMap.put("result",regDailyContList);
	  	return finalMap;
	}*/


	@RequestMapping(value="/getContractData",method = RequestMethod.POST)
	public @ResponseBody Object getContractData(@RequestBody Map<String,Object> clientMap){

		System.out.println("--------->>>>>>"+clientMap.get("custCode"));
		System.out.println("--------->>>>>>"+clientMap.get("cnmtDt"));
		System.out.println("--------->>>>>>"+clientMap.get("cnmtFromSt"));
		System.out.println("from client------>"+clientMap.get("cnmtDt").getClass());

		Date cnmtDt = null;
		java.sql.Date sql = null;
		String reqDate = (String) clientMap.get("cnmtDt");
		DateFormat formatter = null;
		try {
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			cnmtDt = new Date(formatter.parse(reqDate).getTime()); // birtDate is a string
			sql = new java.sql.Date(cnmtDt.getTime());
		}
		catch (Exception e) {
			System.out.println("Exception :" + e);
		}

		System.out.println("%%%%%%%%%%%%%%%%%");
		System.out.println("%%%%%%%%%%%%%%%%%"+sql);

		String custCode = (String) clientMap.get("custCode");
		//cnmtDt = (Date) clientMap.get("cnmtDt");
		String cnmtFromSt = (String) clientMap.get("cnmtFromSt");
		Map<String,Object> finalMap = new HashMap<String,Object>();
		List<Map<String, Object>> regDailyContList = new ArrayList<Map<String,Object>>();

		List<RegularContract>  regularContractList = regularContractDAO.getRegContForCnmt(custCode, cnmtFromSt);
		List<DailyContract> dailyContractList = dailyContractDAO.getDailyContForCnmt(custCode, cnmtFromSt);

		List<RegularContract> finalRegContList = new ArrayList<RegularContract>();
		List<DailyContract> finalDlyContList = new ArrayList<DailyContract>();
		
		System.out.println("Regular Contract List : "+regularContractList.size());
		System.out.println("Daily Contract List : "+dailyContractList.size());

		if (!regularContractList.isEmpty()) {
			for (int i = 0; i < regularContractList.size(); i++) {
				System.out.println("yoooooooooooooooooo"+regularContractList.get(i).getRegContFromDt());
				System.out.println("Noooooooooooooooooo"+regularContractList.get(i).getRegContToDt());		
				
				int before = regularContractList.get(i).getRegContFromDt().compareTo(sql);
				int after = regularContractList.get(i).getRegContToDt().compareTo(sql);
				if(before <= 0 && after >= 0){
					System.out.println("-------------&&&&&&&&&&&&----------------"+regularContractList.get(i).getRegContCode());
					finalRegContList.add(regularContractList.get(i));
				}else{
					System.out.println("-------------%%%%%%%%%%%%----------------");
				}
			}	

			for(int i=0;i<finalRegContList.size();i++){
				Map<String, Object> map = new HashMap<String, Object>();

				List<Customer> custList = customerDAO.getCustomer(finalRegContList.get(i).getRegContBLPMCode());
				List<Station> stnList = stationDAO.retrieveStation(finalRegContList.get(i).getRegContFromStation());
				
				map.put("custName",custList.get(0).getCustName());
				map.put("custCode",finalRegContList.get(i).getRegContBLPMCode());
				map.put("contCode", finalRegContList.get(i).getRegContCode());
				map.put("FACode", finalRegContList.get(i).getRegContFaCode());
				map.put("fromStation", finalRegContList.get(i).getRegContFromStation());
				map.put("fromStnName", stnList.get(0).getStnName());
				map.put("toStation", finalRegContList.get(i).getRegContToStation());
				map.put("fromDate", finalRegContList.get(i).getRegContFromDt());
				map.put("toDate", finalRegContList.get(i).getRegContToDt());
				map.put("rate", finalRegContList.get(i).getRegContRate());
				map.put("cnmtDC", finalRegContList.get(i).getRegContDc());
				map.put("cnmtDDL", finalRegContList.get(i).getRegContDdl());
				map.put("cnmtCostGrade", finalRegContList.get(i).getRegContCostGrade());
				map.put("contType",finalRegContList.get(i).getRegContType());
				map.put("proportionate",finalRegContList.get(i).getRegContProportionate());
				
				/*if(regularContractList.get(i).getRegContType().equals("K") || regularContractList.get(i).getRegContType().equals("k")){
					List<ContToStn> ctsList = contToStnDAO.getContToStn(regularContractList.get(i).getRegContCode());
					double addRate = 0.0;
					if(!ctsList.isEmpty()){
						for(int j=0;j<ctsList.size();j++){
							addRate = addRate + ctsList.get(j).getCtsAdditionalRate();
						}
					}
					map.put("additionalRate",addRate);
				}*/
				
				/*map.put("fromWeight",regularContractList.get(i).getRegContFromWt());
				map.put("toWeight",regularContractList.get(i).getRegContToWt());*/
				regDailyContList.add(map);
			}

		}

		if (!dailyContractList.isEmpty()) {
			for (int i = 0; i < dailyContractList.size(); i++) {
				int before = dailyContractList.get(i).getDlyContStartDt().compareTo(sql);
				int after = dailyContractList.get(i).getDlyContEndDt().compareTo(sql);
				if(before <= 0 && after >= 0){
					System.out.println("-------------&&&&&&&&&&&&----------------");
					finalDlyContList.add(dailyContractList.get(i));
				}else{
					System.out.println("-------------%%%%%%%%%%%%----------------");
				}
			}

			for(int i=0;i<finalDlyContList.size();i++){
				Map<String, Object> map = new HashMap<String, Object>();

				List<Customer> custList = customerDAO.getCustomer(finalDlyContList.get(i).getDlyContBLPMCode());
				
				map.put("custName",custList.get(0).getCustName());				
				map.put("custCode",finalDlyContList.get(i).getDlyContBLPMCode());
				map.put("contCode", finalDlyContList.get(i).getDlyContCode());
				map.put("FACode", finalDlyContList.get(i).getDlyContFaCode());
				map.put("fromStation", finalDlyContList.get(i).getDlyContFromStation());
				map.put("toStation", finalDlyContList.get(i).getDlyContToStation());
				map.put("fromDate", finalDlyContList.get(i).getDlyContStartDt());
				map.put("toDate", finalDlyContList.get(i).getDlyContEndDt());
				map.put("rate", finalDlyContList.get(i).getDlyContRate());
				map.put("cnmtDC", finalDlyContList.get(i).getDlyContDc());
				map.put("cnmtDDL", finalDlyContList.get(i).getDlyContDdl());
				map.put("cnmtCostGrade", finalDlyContList.get(i).getDlyContCostGrade());
				map.put("contType",finalDlyContList.get(i).getDlyContType());
				map.put("proportionate",finalDlyContList.get(i).getDlyContProportionate());
				
				/*if(dailyContractList.get(i).getDlyContType().equals("K") || dailyContractList.get(i).getDlyContType().equals("k")){
					List<ContToStn> ctsList = contToStnDAO.getContToStn(dailyContractList.get(i).getDlyContCode());
					double addRate = 0.0;
					if(!ctsList.isEmpty()){
						for(int j=0;j<ctsList.size();j++){
							addRate = addRate + ctsList.get(j).getCtsAdditionalRate();
						}
					}
					map.put("additionalRate",addRate);
				}*/
				/*map.put("fromWeight",dailyContractList.get(i).getDlyContFromWt());
				map.put("toWeight",dailyContractList.get(i).getDlyContToWt());*/
				regDailyContList.add(map);
			}

		}

		if(!regDailyContList.isEmpty()){
			finalMap.put("list",regDailyContList);
			finalMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			finalMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		/*System.out.println("first value"+regDailyContList);*/
		return finalMap;
	}


	@RequestMapping(value="/getListOfEmployee",method = RequestMethod.POST)
	public @ResponseBody Object getListOfEmployee(){

		List<Employee> employeeList = employeeDAO.getAllActiveEmployees();
		System.out.println("list of employee------>>>>"+employeeList);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!employeeList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",employeeList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;		
	}

	@RequestMapping(value="/getStateCodeData",method = RequestMethod.POST)
	public @ResponseBody Object getStateCodeData(){

		List<State> stateList = stateDAO.getStateData();
		System.out.println("list of employee------>>>>"+stateList);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!stateList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",stateList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	@RequestMapping(value="/submitcnmt",method = RequestMethod.POST)
	public @ResponseBody Object submitcnmt(@RequestBody Cnmt cnmt)throws Exception{
		System.out.println("enter into submitcnmt fucntion ---->"+cnmt.getCnmtCode());
		cnmt.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		cnmt.setUserCode(currentUser.getUserCode());
		cnmt.setbCode(currentUser.getUserBranchCode());
		cnmt.setBranchCode(currentUser.getUserBranchCode());

		String cnmtCode =cnmt.getCnmtCode();
		double daysLeft = getCnmtDaysLast();
		double average = getAvgMonthlyUsageForCnmt();
		System.out.println("daysLeft-----------------"+daysLeft);
		String brsLeafDetStatus = "cnmt";
		String actualSNo = cnmtCode;
		
		/*int serialNo = Integer.parseInt(cnmtCode);
		double daysLeft = getCnmtDaysLast();
		double average = getAvgMonthlyUsageForCnmt();
		System.out.println("daysLeft-----------------"+daysLeft);
		System.out.println("SerialNo--------------"+serialNo);
		String brsLeafDetStatus = "cnmt";
		String actualSNo = String.valueOf(serialNo);*/
		
		
	
		/*BranchStockLeafDet branchStockLeafDet = branchStockLeafDetDAO.deleteBSLD(actualSNo,brsLeafDetStatus);
		//System.out.println("deleteBSLD-------------------"+deleteBSLD);

		if(branchStockLeafDet != null){
			BranchSStats branchSStats = new BranchSStats();
			branchSStats.setBrsStatsBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
			branchSStats.setBrsStatsType("cnmt");
			branchSStats.setBrsStatsDt(cnmt.getCnmtDt());
			branchSStats.setBrsStatsSerialNo(branchStockLeafDet.getBrsLeafDetSNo());
			branchSStats.setUserCode(currentUser.getUserCode());
			branchSStats.setbCode(currentUser.getUserBranchCode());
			int result = branchSStatsDAO.saveBranchSStats(branchSStats);

			Calendar cal = Calendar.getInstance();
			Date currentDate = cnmt.getCnmtDt();
			cal.setTime(currentDate);
			System.out.println("Current date is-----"+currentDate);
			int currentMonth = cal.get(Calendar.MONTH)+1;
			System.out.println("Current month is-----"+currentMonth);
			int currentYear = cal.get(Calendar.YEAR);
			System.out.println("Current year is-----"+currentYear);
			if(result==1){
				BranchMUStats branchMUStats = new BranchMUStats();
				branchMUStats.setBmusDaysLast(daysLeft);
				System.out.println("daysLeft = "+daysLeft);
				branchMUStats.setBmusStType("cnmt");
				if(Double.isNaN(average)){
					branchMUStats.setBmusAvgMonthUsage(0.0);
				}else{
					branchMUStats.setBmusAvgMonthUsage(average);
				}
				System.out.println("average = "+average);
				branchMUStats.setbCode(currentUser.getUserBranchCode());
				branchMUStats.setUserCode(currentUser.getUserCode());
				branchMUStats.setBmusBranchCode(branchStockLeafDet.getBrsLeafDetBrCode());
				System.out.println("branchStockLeafDet.getBrsLeafDetBrCode() = "+branchStockLeafDet.getBrsLeafDetBrCode());
				branchMUStats.setBmusDt(currentDate);

				//long rowsCount = branchMUStatsDAO.totalCnmtRows();
				long rowsCount = branchMUStatsDAO.totalBMUSRows("cnmt");
				System.out.println("Rows count for cnmt is-----------"+rowsCount);
				if(rowsCount!=-1){
					if(rowsCount==0){
						branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
					}else{
						List<BranchMUStats> listStats = branchMUStatsDAO.getLastData(branchStockLeafDet.getBrsLeafDetBrCode(),"cnmt");
						BranchMUStats branchMUStats2 = listStats.get(0);
						Date lastDate = listStats.get(0).getBmusDt();
						int lastId = listStats.get(0).getBmusId();

						System.out.println("The last date is ==========="+lastDate);
						Calendar calender = Calendar.getInstance();
						calender.setTime(lastDate);
						int lastMonth = calender.get(Calendar.MONTH)+1;
						System.out.println("lastMonth---------------"+lastMonth);
						int lastyear = calender.get(Calendar.YEAR);
						System.out.println("lastyear---------------"+lastyear);
						if((currentMonth==lastMonth) && (currentYear==lastyear)){
							branchMUStats.setBmusAvgMonthUsage(average);
							branchMUStats.setBmusDt(currentDate);
							branchMUStats.setBmusDaysLast(daysLeft);
							branchMUStats.setBmusId(lastId);
							System.out.println("Before update"+daysLeft+currentDate+average);
							branchMUStatsDAO.updateBMUS(branchMUStats);
							System.out.println("After update");
						}else if((currentMonth!=lastMonth) && (currentYear!=lastyear)){

							System.out.println("Enter into function with id ---------"+lastId);
							int delete = branchMUStatsDAO.deleteBMUS(lastId);
							System.out.println("After deleting old entry---------------------"+delete);
							if(delete==1){
								int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
								System.out.println("--------------------After saving new entry"+save);
								if(save == 1){
									BranchMUStatsBk bk = new BranchMUStatsBk();
									bk.setbCode(branchMUStats2.getbCode());
									bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
									bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
									bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
									bk.setBmusDt(branchMUStats2.getBmusDt());
									bk.setBmusId(branchMUStats2.getBmusId());
									bk.setBmusStType(branchMUStats2.getBmusStType());
									bk.setUserCode(branchMUStats2.getUserCode());
									branchMUStatsDAO.saveBMUSBK(bk);
									System.out.println("---------------After saving into backup");
								}
							}		
						}else if((currentMonth!=lastMonth) && (currentYear==lastyear)){
							System.out.println("Enter into function with id ---------"+lastId);
							int delete = branchMUStatsDAO.deleteBMUS(lastId);
							System.out.println("After deleting old entry---------------------"+delete);
							if(delete==1){
								int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
								System.out.println("--------------------After saving new entry"+save);
								if(save == 1){
									BranchMUStatsBk bk = new BranchMUStatsBk();
									bk.setbCode(branchMUStats2.getbCode());
									bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
									bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
									bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
									bk.setBmusDt(branchMUStats2.getBmusDt());
									bk.setBmusId(branchMUStats2.getBmusId());
									bk.setBmusStType(branchMUStats2.getBmusStType());
									bk.setUserCode(branchMUStats2.getUserCode());
									branchMUStatsDAO.saveBMUSBK(bk);
									System.out.println("---------------After saving into backup");
								}
							}		
						}
					}	
				}	
			} 
			System.out.println("after saving result = "+result);
		}


		Blob blob = modelService.getBlob();
		Blob confirmBlob = modelService.getConfirmBlob();
		System.out.println("cnmt image lenght = "+blob.length());
		cnmt.setCnmtImage(blob);
		cnmt.setCnmtConfirmImage(confirmBlob);
		List<InvAndDateService> invList = invoiceService.getAllInvoice();
		System.out.println("****************"+invList.size());
		ArrayList<Map<String,Object>> cnmtInvList = new ArrayList<Map<String,Object>>();

		if(!invList.isEmpty()){
			for(int i=0;i<invList.size();i++){
				Map<String,Object> cnmtInvMap = new HashMap<String,Object>();
				cnmtInvMap.put("invoiceNo",invList.get(i).getInvoice());
				cnmtInvMap.put("date",invList.get(i).getDate());
				cnmtInvList.add(cnmtInvMap);
			}
		}

		if(!cnmtInvList.isEmpty()){
			cnmt.setCnmtInvoiceNo(cnmtInvList);
		}
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println("*****"+cnmt.getCnmtKm());
		cnmt.setCnmtBillPermission("no");*/
		Blob blob = modelService.getBlob();
		Blob confirmBlob = modelService.getConfirmBlob();
		List<InvAndDateService> invList = invoiceService.getAllInvoice();
		int temp = cnmtDAO.saveCnmtToDB(cnmt,cnmtCode,brsLeafDetStatus,daysLeft,average,blob,confirmBlob,invList);
		invoiceService.deleteAll();
		Map<String,String> map = new HashMap<String,String>();
		if (temp>=0) {
			modelService.setBlob(null);
			modelService.setConfirmBlob(null);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	 	
		return map;    
	}



	@RequestMapping(value = "/biltyDetails", method = RequestMethod.POST)
	public @ResponseBody Object biltyDetails(@RequestBody String cnmtCode) {

		Cnmt cnmt = new Cnmt();

		List<Cnmt> cList = cnmtDAO.getCnmtList(cnmtCode);
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String,Object> cnmtMap = new HashMap<String, Object>();

		if(!cList.isEmpty()){

			cnmtMap.put(CnmtCNTS.CNMT_ID,cList.get(0).getCnmtId());
			cnmtMap.put(CnmtCNTS.CNMT_CODE,cList.get(0).getCnmtCode() );
			cnmtMap.put(CnmtCNTS.BRANCH_CODE,cList.get(0).getBranchCode());
			cnmtMap.put(CnmtCNTS.CNMT_DT,cList.get(0).getCnmtDt());
			cnmtMap.put(CnmtCNTS.CUST_CODE,cList.get(0).getCustCode());
			cnmtMap.put(CnmtCNTS.CNMT_CONSIGNOR,cList.get(0).getCnmtConsignor());
			cnmtMap.put(CnmtCNTS.CNMT_CONSIGNEE,cList.get(0).getCnmtConsignee());
			cnmtMap.put(CnmtCNTS.CNMT_RATE,cList.get(0).getCnmtRate());
			cnmtMap.put(CnmtCNTS.CNMT_NO_OF_PKG,cList.get(0).getCnmtNoOfPkg());
			cnmtMap.put(CnmtCNTS.CNMT_ACTUAL_WT,cList.get(0).getCnmtActualWt());
			cnmtMap.put(CnmtCNTS.CNMT_PAY_AT,cList.get(0).getCnmtPayAt());
			cnmtMap.put(CnmtCNTS.CNMT_BILL_AT,cList.get(0).getCnmtBillAt());
			cnmtMap.put(CnmtCNTS.CNMT_FREIGHT,cList.get(0).getCnmtFreight());
			cnmtMap.put(CnmtCNTS.CNMT_VOG,cList.get(0).getCnmtVOG());
			cnmtMap.put(CnmtCNTS.CNMT_EXTRA_EXP,cList.get(0).getCnmtExtraExp());
			cnmtMap.put(CnmtCNTS.CNMT_DT_OF_DLY,cList.get(0).getCnmtDtOfDly());
			cnmtMap.put(CnmtCNTS.CNMT_EMP_CODE,cList.get(0).getCnmtEmpCode());
			cnmtMap.put(CnmtCNTS.CNMT_KM,cList.get(0).getCnmtKm());
			cnmtMap.put(CnmtCNTS.CNMT_STATE,cList.get(0).getCnmtState());
			cnmtMap.put(CnmtCNTS.CONTRACT_CODE,cList.get(0).getContractCode());
			cnmtMap.put(CnmtCNTS.CNMT_INVOICE_NO,cList.get(0).getCnmtInvoiceNo());
			cnmtMap.put(CnmtCNTS.CREATION_TS,cList.get(0).getCreationTS());
			cnmtMap.put(CnmtCNTS.USER_BRANCH_CODE,cList.get(0).getbCode());
			cnmtMap.put(CnmtCNTS.CNMT_TOT,cList.get(0).getCnmtTOT());
			cnmtMap.put(CnmtCNTS.IS_VIEW,cList.get(0).isView());


			/*Blob blob = cList.get(0).getCnmtImage();
			if(blob == null){
				map.put("image","no");
			}else{
				map.put("image","yes");
			}*/
			
			int cmId = cList.get(0).getCmId();
			if(cmId > 0){
				map.put("image","no");
			}else{
				map.put("image","yes");
			}

			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("cnmt", cnmtMap);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;	
	}	

	@RequestMapping(value = "/updateIsViewBilty", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewBilty(@RequestBody String selection) {

		String contids[] = selection.split(",");

		int temp =  cnmtDAO.updateCnmtisViewTrue(contids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/EditBiltySubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditBiltySubmit(@RequestBody Cnmt cnmt) {

		int temp=cnmtDAO.updateCnmt(cnmt);
		Map<String, Object> map = new HashMap<String, Object>();

		if(temp>0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/uploadCnmtImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadCnmtImage(@RequestParam("file") MultipartFile file)throws Exception {

		System.out.println("enter into uploadCnmtImage function ---");

		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			modelService.setBlob(null); 
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			modelService.setBlob(blob); 
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}


	@RequestMapping(value = "/uploadConfCnmtImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadConfCnmtImage(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into uploadConfCnmtImage function ---");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			modelService.setConfirmBlob(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			modelService.setConfirmBlob(blob); 
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}


	@RequestMapping(value="/getCnmtCodeList",method = RequestMethod.POST)
	public @ResponseBody Object getCnmtCodeList(HttpServletRequest request){
		System.out.println("enter into getCnmtCodeList function----->");	
		
		System.out.println("URL : "+request.getRequestURL());
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<BranchStockLeafDet> cnmtCodeList= branchStockLeafDetDAO.getCodeList(branchCode,"cnmt");
		System.out.println("size of cnmtCodeList = "+cnmtCodeList.size());
		Map<String,Object> map = new HashMap<String,Object>();
		if (!cnmtCodeList.isEmpty()){
			System.out.println("successfully send cnmt list");
			map.put("list", cnmtCodeList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}


	public  double getCnmtDaysLast() {
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar =Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		Date endDate = new Date(calendar.getTime().getTime());
		double cnmtLeft=0;
		User currentUser = (User)httpSession.getAttribute("currentUser");

		double cnmtPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsed(date,endDate,"cnmt");
		double daysLeft = 0;
		if(cnmtPerDayUsed>0){
			cnmtLeft = branchStockLeafDetDAO.getLeftStationary(currentUser.getUserBranchCode(),"cnmt");
			daysLeft=cnmtLeft/cnmtPerDayUsed;
		}

		return daysLeft;
	}

	public double getAvgMonthlyUsageForCnmt() {

		System.out.println("------Enter into getAvgMonthlyUsage function");

		Date todayDate = new Date(new java.util.Date().getTime());
		Date firstEntryDate = branchSStatsDAO.getFirstEntryDate("cnmt");
		System.out.println("**************firstEntryDate = "+firstEntryDate);
		double average=0.00;
		if(firstEntryDate != null){
			Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = new Date(calendar.getTime().getTime());

			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));


			double cnmtForOneMnth = branchSStatsDAO.getNoOfStationary(todayDate, oneMonthPrevDate,"cnmt");
			double cnmtForScndMnth = branchSStatsDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate,"cnmt");
			double cnmtForThirdMnth = branchSStatsDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate,"cnmt");
			double cnmtBwFrstAndScndMnth = branchSStatsDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate,"cnmt");
			double cnmtBwFrstAndOneMnth=branchSStatsDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate,"cnmt");
			double cnmtBwTodayAndFrstDate=branchSStatsDAO.getNoOfStationary(todayDate, firstEntryDate,"cnmt");


			if(days>=90){
				average = ((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + cnmtForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		

				average = (((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + (cnmtBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){

				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				average = (((cnmtForOneMnth*2) +  (cnmtBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				if(daysBwFrstAndToday == 0){
					average = cnmtBwTodayAndFrstDate*30;
				}else{
					average = ((cnmtBwTodayAndFrstDate/daysBwFrstAndToday)*30);
				}
			}
		}	

		return average;
	}


	@RequestMapping(value="/addInvoiceForCnmt",method = RequestMethod.POST)
	public @ResponseBody Object addInvoiceForCnmt(@RequestBody InvAndDateService invAndDateService){
		System.out.println("enter into addInvoiceForCnmt function ==>"+invAndDateService.getDate());
		invoiceService.addInvoice(invAndDateService);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}

	
/*	@RequestMapping(value="/addInvoiceForCnmt",method = RequestMethod.POST)
	public @ResponseBody Object addInvoiceForCnmt(@RequestBody Date date){
		System.out.println("enter into addInvoiceForCnmt function ==>"+date);
		//invoiceService.addInvoice(invAndDateService);
		Map<String,Object> map = new HashMap<String,Object>();
		//map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}*/


	@RequestMapping(value="/getCnmtInvoiceList",method = RequestMethod.POST)
	public @ResponseBody Object getCnmtInvoiceList(){
		System.out.println("enter into getCnmtInvoiceList function");
		List<InvAndDateService> cnmtInvList = invoiceService.getAllInvoice();
		System.out.println("*************"+cnmtInvList);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		map.put("list",cnmtInvList);
		return map;	
	}


	@RequestMapping(value="/deleteCnmtInv",method = RequestMethod.POST)
	public @ResponseBody Object deleteCnmtInv(@RequestBody InvAndDateService invAndDateService){
		System.out.println("enter into deleteCnmtInv function");
		invoiceService.deleteInvoice(invAndDateService);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}


	@RequestMapping(value="/deleteAllInv",method = RequestMethod.POST)
	public @ResponseBody Object deleteAllInv(){
		System.out.println("enter into deleteAllInv function");
		invoiceService.deleteAll();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}

	@RequestMapping(value="/addRateForCnmtByK",method = RequestMethod.POST)
	public @ResponseBody Object addRateForCnmtByK(@RequestBody Map<String,Object> clientMap){
		System.out.println("enter into addRateForCnmtByK function");
		Map<String,Object> map = new HashMap<String,Object>();
		String contCode = (String) clientMap.get("contractCode");
		String checkCode =  contCode.substring(0,3);
		int kmValue = (int) clientMap.get("cnmtKm");
		double cnmtKm = (double) kmValue;
		String stateCode = (String) clientMap.get("cnmtState");
		if(checkCode.equalsIgnoreCase("dly")){
			List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
			DailyContract dailyContract = dailyContList.get(0);
			//double rate = rateByKmDAO.getRbkmRate(stateCode,cnmtKm,contCode);
			List<RateByKm> rbkmList = rateByKmDAO.getRbkmRate(stateCode,cnmtKm,contCode);
			double rate = 0.0;
			String vehTypeCode = "";
			if(!rbkmList.isEmpty()){
				for(int i=0;i<rbkmList.size();i++){
					if(rbkmList.get(i).getRbkmFromKm() <= cnmtKm && rbkmList.get(i).getRbkmToKm() >= cnmtKm){
						rate = rbkmList.get(i).getRbkmRate();
						vehTypeCode = rbkmList.get(i).getRbkmVehicleType();
					}
				}
			}
			double actualRate = cnmtKm * rate;
			System.out.println("**************************------rate = "+actualRate);
			map.put("rate",actualRate);
			map.put("vtCode",vehTypeCode);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);

		}else if(checkCode.equalsIgnoreCase("reg")){
			List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
			RegularContract regularContract = regConList.get(0);
			List<RateByKm> rbkmList = rateByKmDAO.getRbkmRate(stateCode,cnmtKm,contCode);
			double rate = 0.0;
			String vehTypeCode = "";
			if(!rbkmList.isEmpty()){
				for(int i=0;i<rbkmList.size();i++){
					if(rbkmList.get(i).getRbkmFromKm() <= cnmtKm && rbkmList.get(i).getRbkmToKm() >= cnmtKm){
						rate = rbkmList.get(i).getRbkmRate();
						vehTypeCode = rbkmList.get(i).getRbkmVehicleType();
					}
				}
			}
			double actualRate = cnmtKm * rate;
			map.put("rate",actualRate);
			map.put("vtCode",vehTypeCode);
			System.out.println("*************************------rate = "+actualRate);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);

		}else{
			System.out.println("invalid contract code");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		return map;	
	}



	@RequestMapping(value="/addRateForCnmtByQ",method = RequestMethod.POST)
	public @ResponseBody Object addRateForCnmtByQ(@RequestBody Map<String,Object> clientMap){
		System.out.println("enter into addRateForCnmtByQ function");
		Map<String,Object> map = new HashMap<String,Object>();
		String contCode = (String) clientMap.get("contractCode");
		String checkCode =  contCode.substring(0,3);
		String stnCode = (String) clientMap.get("cnmtToSt");
		String productType = (String) clientMap.get("cnmtProductType");
		String vehicleType = (String) clientMap.get("cnmtVehicleType");
		System.out.println("from client vehicleType = "+vehicleType);
		Date cnmtDt = Date.valueOf((String) clientMap.get("cnmtDt"));  
		System.out.println("Cnmt Date="+cnmtDt);
		
		
		if(checkCode.equalsIgnoreCase("dly")){
			List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
			DailyContract dailyContract = dailyContList.get(0);
			List<ContToStn> ContToStnList  = contToStnDAO.getContToStnRateQ(contCode, stnCode, productType,vehicleType,cnmtDt);
			double rate = 0.0;
			double garunteeWt = 0.0;
			if(!ContToStnList.isEmpty()){
				for(int i=0;i<ContToStnList.size();i++){
			  				rate = ContToStnList.get(i).getCtsRate();
			  				garunteeWt = ContToStnList.get(i).getCtsToWt();
			     }
				/*for(int i=0;i<ContToStnList.size();i++){
					int result1 = cnmtDt.compareTo(ContToStnList.get(i).getCtsFrDt());
					int result2 = cnmtDt.compareTo(ContToStnList.get(i).getCtsToDt());
					
					if(result1 >= 0 && result2 <= 0){
						rate = ContToStnList.get(i).getCtsRate();
						garunteeWt = ContToStnList.get(i).getCtsToWt();
						break;
					}
				}*/
				

			}	
			double actualRate = rate;
			map.put("rate",actualRate);
			map.put("garunteeWt",garunteeWt);			
			System.out.println("###########------rate = "+rate);
			System.out.println("Gt : "+garunteeWt);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else if(checkCode.equalsIgnoreCase("reg")){
			List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
			RegularContract regularContract = regConList.get(0);
			List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contCode, stnCode, productType,vehicleType,cnmtDt);
			double rate = 0.0;
			double garunteeWt = 0.0;
			if(!ContToStnList.isEmpty()){
				for(int i=0;i<ContToStnList.size();i++){
					  rate = ContToStnList.get(i).getCtsRate();	
					  garunteeWt = ContToStnList.get(i).getCtsToWt();
				 }
				/*for(int i=0;i<ContToStnList.size();i++){
					int result1 = cnmtDt.compareTo(ContToStnList.get(i).getCtsFrDt());
					int result2 = cnmtDt.compareTo(ContToStnList.get(i).getCtsToDt());
					
					if(result1 >= 0 && result2 <= 0){
						rate = ContToStnList.get(i).getCtsRate();	
						garunteeWt = ContToStnList.get(i).getCtsToWt();
						break;
					}
				}*/
				
			}	 	
			double actualRate = rate;
			map.put("rate",actualRate);
			map.put("garunteeWt",garunteeWt);
			System.out.println("###########------rate = "+rate);
			System.out.println("Gt : "+garunteeWt);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);

		}else{
			System.out.println("invalid contract code");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		return map;	
	}


	@RequestMapping(value="/addRateForCnmtByW",method = RequestMethod.POST)
	public @ResponseBody Object addRateForCnmtByW(@RequestBody Map<String,Object> clientMap){
		System.out.println("enter into addRateForCnmtByW function");
		Map<String,Object> map = new HashMap<String,Object>();
		String contCode = (String) clientMap.get("contractCode");
		System.out.println("from client contCode = "+contCode);
		String checkCode =  contCode.substring(0,3);
		String stnCode = (String) clientMap.get("cnmtToSt");
		System.out.println("from client stnCode = "+stnCode);
		String vehicleType = (String) clientMap.get("cnmtVehicleType");
		System.out.println("from client vehicleType = "+vehicleType);
		Date cnmtDt = Date.valueOf((String) clientMap.get("cnmtDt"));  
		System.out.println("Cnmt Date="+cnmtDt);
		
		if(checkCode.equalsIgnoreCase("dly")){
			List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
			DailyContract dailyContract = dailyContList.get(0);
			List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateCnmtW(contCode, stnCode, vehicleType, cnmtDt);
			System.out.println("yoooooooooooooooooooooo="+ContToStnList.size());
			double rate = 0.0;
			double garWt = 0.0;
			//double addRate = 0.0;
			if(!ContToStnList.isEmpty()){
				for(int i=0;i<ContToStnList.size();i++){
			  				rate = ContToStnList.get(i).getCtsRate();
			  				garWt = ContToStnList.get(i).getCtsToWt();
			  	}
				
				/*for(int i=0;i<ContToStnList.size();i++){
					int result1 = cnmtDt.compareTo(ContToStnList.get(i).getCtsFrDt());
					int result2 = cnmtDt.compareTo(ContToStnList.get(i).getCtsToDt());
					
					if(result1 >= 0 && result2 <= 0){
						rate = ContToStnList.get(i).getCtsRate();
						garWt = ContToStnList.get(i).getCtsToWt();
						break;
					}
				}*/
				//addRate = ContToStnList.get(0).getCtsAdditionalRate();
			}	
			double actualRate = rate;
			map.put("rate",actualRate);
			map.put("garWt",garWt);
			//map.put("addRt", addRate);
			System.out.println("$$$$$$$$$$$$$$------rate = "+rate);
			System.out.println("$$$$$$$$$$$$$$------garWt = "+garWt);
			//System.out.println("$$$$$$$$$$$$$$------addRt = "+addRate);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else if(checkCode.equalsIgnoreCase("reg")){
			List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
			RegularContract regularContract = regConList.get(0);			
			List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateCnmtW(contCode, stnCode, vehicleType , cnmtDt);
			System.out.println("kyaaaaaaaaa hhhhhhhh--->"+ContToStnList.size());
			double rate = 0.0;
			double garWt = 0.0;
			//double addRate = 0.0;
			if(!ContToStnList.isEmpty()){
				for(int i=0;i<ContToStnList.size();i++){
					  rate = ContToStnList.get(i).getCtsRate();
					  garWt = ContToStnList.get(i).getCtsToWt();
				  }
				/*for(int i=0;i<ContToStnList.size();i++){
					int result1 = cnmtDt.compareTo(ContToStnList.get(i).getCtsFrDt());
					int result2 = cnmtDt.compareTo(ContToStnList.get(i).getCtsToDt());
					
					if(result1 >= 0 && result2 <= 0){
						rate = ContToStnList.get(i).getCtsRate();
						garWt = ContToStnList.get(i).getCtsToWt();
						break;
					}
				}*/
				
				/*rate = ContToStnList.get(0).getCtsRate();
				garWt = ContToStnList.get(0).getCtsToWt();*/
				//addRate = ContToStnList.get(0).getCtsAdditionalRate();
				double actualRate = rate;
				map.put("rate",actualRate);
				map.put("garWt",garWt);
				//map.put("addRt", addRate);
				System.out.println("$$$$$$$$$$$$$------rate = "+rate);
				System.out.println("$$$$$$$$$$$$$$------garWt = "+garWt);
				//System.out.println("$$$$$$$$$$$$$$------addRt = "+addRate);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			System.out.println("invalid contract code");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		return map;	
	}


	@RequestMapping(value="/getAddRateFrCnmt",method = RequestMethod.POST)
	public @ResponseBody Object getAddRateFrCnmt(@RequestBody Map<String,Object> clientMap){
		System.out.println("enter into getAddRateFrCnmt function");
		Map<String,Object> map = new HashMap<String,Object>();
		String contCode = (String) clientMap.get("contCode");
		String toStn = (String) clientMap.get("toStn");
		String vType = (String) clientMap.get("vType");
		Date cnmtDt = Date.valueOf((String) clientMap.get("cnmtDt"));        
		if(contCode != null && toStn != null && vType != null){
			String checkCode =  contCode.substring(0,3);
			if(checkCode.equalsIgnoreCase("dly")){
				List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
				DailyContract dailyContract = dailyContList.get(0);
				List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateCnmtW(contCode, toStn, vType, cnmtDt);
				double addRate = 0.0;
				if(!ContToStnList.isEmpty()){
					/*for(int i=0;i<ContToStnList.size();i++){
						
						int result1 = cnmtDt.compareTo(ContToStnList.get(i).getCtsFrDt());
						int result2 = cnmtDt.compareTo(ContToStnList.get(i).getCtsToDt());
						if(result1 >= 0 && result2 <= 0){
							addRate = addRate + ContToStnList.get(i).getCtsAdditionalRate();
							break;
						}
					}*/
					
					for(int i=0;i<ContToStnList.size();i++){
						addRate = addRate + ContToStnList.get(i).getCtsAdditionalRate();
					}
				}	
				map.put("aRate",addRate);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else if(checkCode.equalsIgnoreCase("reg")){
				List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
				RegularContract regularContract = regConList.get(0);
				List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateCnmtW(contCode, toStn, vType ,cnmtDt);
				double addRate = 0.0;
				if(!ContToStnList.isEmpty()){
					for(int i=0;i<ContToStnList.size();i++){
						addRate = addRate + ContToStnList.get(i).getCtsAdditionalRate();
					  }
				}	 	
				map.put("aRate",addRate);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value="/getCnmtForEdit", method=RequestMethod.POST)
	public @ResponseBody Object getCnmtForEdit(@RequestBody String cnmtCode){
		System.out.println("getCnmtForEdit");
		Map<String, Object> map = cnmtDAO.getCnmtNCnmtImg(cnmtCode);
		
		if ((int)map.get("temp")>0) {
			//convert actualWt guranteeWt and rate into ton
			Cnmt cnmt = (Cnmt) map.get("cnmt");
			if (cnmt != null) {
				cnmt.setCnmtActualWt(cnmt.getCnmtActualWt()/1000);
				cnmt.setCnmtGuaranteeWt(cnmt.getCnmtGuaranteeWt()/1000);
				cnmt.setCnmtRate(cnmt.getCnmtRate()*1000);
			}
			
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("cnmt", cnmt);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	//TODO
	@RequestMapping(value="/saveEditCnmt", method=RequestMethod.POST)
	public @ResponseBody Object saveEditCnmt(@RequestBody Cnmt cnmt){
		System.out.println("saveEditCnmt()");
		logger.info("Enter into /saveEditCnmt()...");
		Map<String, Object> map = new HashMap<>();		
		cnmt.setCnmtActualWt(cnmt.getCnmtActualWt()*1000);
		cnmt.setCnmtGuaranteeWt(cnmt.getCnmtGuaranteeWt()*1000);
		cnmt.setCnmtRate(cnmt.getCnmtRate()/1000);
		/*cnmt.setCnmtFreight(Math.round(cnmt.getCnmtFreight()*100.0)/100.0);*/
		
		// Create Direcotry on server if not exists
		try{
			Path path = Paths.get(cnmtImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);
		}catch(Exception e){
			System.out.println("Error : "+e);
			logger.error("Error in creating directory : "+e);
		}		
		// Local Test		
		//cnmtImgPath = httpServletRequest.getRealPath("/CNMT");		
				
		map = cnmtDAO.saveEditCnmt(cnmt, cnmtImgBlob, cnmtImgPath);	
		cnmtImgBlob = null;//clear blob
		logger.info("Exit from /saveEditCnmt()...");
		return map;
	}
	
	//TODO
	@RequestMapping(value="/uploadCnmtImg", method=RequestMethod.POST)
	public @ResponseBody Object uploadCnmtImg(@RequestParam("file") MultipartFile file) throws Exception{
		System.out.println("uploadCnmtImg()");
		Map<String, Object> map = new HashMap<>();
		
		byte[] byteArr = file.getBytes();
		try {
			cnmtImgBlob = new SerialBlob(byteArr);
			System.out.println("cnmtImgBlob ===" + cnmtImgBlob.length());
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getCnmtImg", method = RequestMethod.POST)
    public @ResponseBody Object getCnmtImg(@RequestBody Integer cnmtImgId)throws Exception {
        System.out.println("enter into getCnmtImg function");
        Map<String, Object> map = new HashMap<String, Object>();
        
        Blob blob = cnmtDAO.getCnmtImage(cnmtImgId);
        
        if (blob != null) {        	
            
            int blobLength = (int) blob.length();  
            byte[] blobAsBytes = blob.getBytes(1, blobLength);
            
            System.out.println("&&&&&&&&&&&&&&&&&&&"+blobAsBytes.length);
            map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
            map.put("cnmtImage",blobAsBytes);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
        return map;
    }
	
	@RequestMapping(value="/getUsrBrCnmtCust", method=RequestMethod.POST)
	public @ResponseBody Object getUsrBrCnmtCust(){
		System.out.println("CnmtCntlr.getUsrBrCnmtCust()");
		Map<String, Object> map = new HashMap<>();
		
		map = cnmtDAO.getUsrBrCnmtCust();
		
		if ((int)map.get("temp") > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value="/getRateFrBbl", method=RequestMethod.POST)
	public @ResponseBody Object getRateFrBbl(@RequestBody Map<String, Object> rateService){
		System.out.println("CnmtCntlr.getRateFrBbl()");
		Map<String, Object> map = new HashMap<>();
		System.out.println("rateService: "+rateService);
		
		double rate = contToStnDAO.getRateFrBbl(rateService);//rate in per kg
		rate *= 1000; //rate in per ton
		
		if (rate > -1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("rate", rate);
		} else {
			map.put(ConstantsValues.RESULT , ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value="/saveCnmtBbl", method=RequestMethod.POST)
	public @ResponseBody Object saveCnmtBbl(@RequestBody Map<String, Object> cnmtBblService){
		System.out.println("CnmtCntlr.saveCnmtBbl()");
		Map<String, Object> map = new HashMap<>();
		System.out.println("cnmtBblService: "+cnmtBblService.entrySet());
		
		int temp = cnmtDAO.saveCnmtBbl(cnmtBblService);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else if (temp == -2) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "either owner or broker must have PAN CARD");
		} else if (temp == -3) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "May be this cnmt or challan already entered");
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "Database exception");
		}
		
		
		return map;
	}
	
	@RequestMapping(value="/getBblCnmtCode", method=RequestMethod.POST)
	public @ResponseBody Object getBblCnmtCode(@RequestBody String cnmtBblCode){
		System.out.println("getBblCnmtCode()"+cnmtBblCode);
		Map<String, Object> map = new HashMap<>();
		List<Object> list=(List<Object>) cnmtDAO.getBblCnmtCode(cnmtBblCode);
		if(!list.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		map.put("codeList", list);
		return map;
	}	
	
	
	@RequestMapping(value="/cnmtDtlFrCncl", method=RequestMethod.POST)
	public @ResponseBody Object chlnDtlFrCncl(@RequestBody Map<String,String> cnmtDtl){
		System.out.println("submitChlnNCnmt()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = cnmtDAO.cnmtDtlFrCncl(cnmtDtl);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	//TODO cnmtCosting
		@RequestMapping(value="/cnmtCosting", method=RequestMethod.POST)
		public @ResponseBody Object cnmtCosting(@RequestBody Map<String,Object> cnmtDtl){
			System.out.println("cnmtCosting()");
			Map<String, Object> map = new HashMap<>();
			
			int temp = cnmtDAO.cnmtDtlFrCstng(cnmtDtl);
			
			if (temp==1) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else if(temp==(-1)){
				map.put(ConstantsValues.RESULT, "Database Exception");
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}
		
	// TODO Costing Excel
	@RequestMapping(value = "/getCostingReportXLSX", method = RequestMethod.POST)
	public void getCostingReportXLSX(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		System.out.println("getCostingReportXLSX");

		System.out.println(request.getParameter("customerName"));
		// System.out.println(request.getParameter("isRBName"));
		System.out.println("consolBrh=" + request.getParameter("isRBName"));
		
		Map<String, Object> map = new HashMap<>();
		Set<String> chlnList = new HashSet<>();
		Set<String> cnmtList = new HashSet<>();
		List<Map<String, Object>> list = new ArrayList<>();
		map.put("custCode", request.getParameter("custName"));
		// map.put("isConsolidate", request.getParameter("consolidateRId"));
		map.put("isBrhCon", request.getParameter("isRBName"));
		map.put("branchCode", request.getParameter("branchName"));
		map.put("frmDt", request.getParameter("frmDt"));
		map.put("toDt", request.getParameter("toDt"));
		map.put("fromStation", request.getParameter("fromStationName"));
		map.put("toStation", request.getParameter("toStationName"));
		
		list = cnmtDAO.getCostingReport(map);
		
		Set<String> stationCodesSet = new HashSet<>();
		Set<String> customerCodesSet = new HashSet<>();
		
		for (Map<String, Object> listMap : list) {
			stationCodesSet.add((String) listMap.get("cnmtFrmSt"));
			stationCodesSet.add((String) listMap.get("cnmtToSt"));
			customerCodesSet.add((String) listMap.get("consignee"));
			customerCodesSet.add((String) listMap.get("consignor"));
		}
		
		List<String> stationCodesList = new ArrayList<>(stationCodesSet);
		List<String> customerCodesList = new ArrayList<>(customerCodesSet);
		
		Map<String, String> stationMap = cnmtDAO.getStationMap(stationCodesList);
		Map<String, String> customerMap = cnmtDAO.getCustomerMap(customerCodesList);
 		
		Integer listSize = list.size();

		XSSFWorkbook workbook = new XSSFWorkbook();
		// Create a blank sheet
		XSSFSheet spreadsheet = workbook.createSheet();
		spreadsheet.setColumnWidth(0, 13 * 256); // approx room for 15 char
		spreadsheet.setColumnWidth(1, 25 * 256); // approx room for 15 char
		spreadsheet.setColumnWidth(2, 13 * 256); // approx room for 15 char
		spreadsheet.setColumnWidth(3, 10 * 256); // approx room for 20 char
		spreadsheet.setColumnWidth(4, 35 * 256); // approx room for 20 char
		spreadsheet.setColumnWidth(5, 35 * 256); // approx room for 20 char
		spreadsheet.setColumnWidth(6, 15 * 256); // approx room for 100 char
		spreadsheet.setColumnWidth(7, 15 * 256); // approx room for 20 char
		spreadsheet.setColumnWidth(8, 13 * 256); // approx room for 20 char
		spreadsheet.setColumnWidth(9, 13 * 256); // approx room for 20 char
		spreadsheet.setColumnWidth(10, 13 * 256);
		spreadsheet.setColumnWidth(11, 13 * 256);
		spreadsheet.setColumnWidth(12, 13 * 256);
		spreadsheet.setColumnWidth(13, 13 * 256);
		spreadsheet.setColumnWidth(14, 13 * 256);
		spreadsheet.setColumnWidth(15, 13 * 256);
		spreadsheet.setColumnWidth(16, 13 * 256);
		spreadsheet.setColumnWidth(17, 13 * 256);
		spreadsheet.setColumnWidth(18, 13 * 256);
		spreadsheet.setColumnWidth(19, 13 * 256);
		spreadsheet.setColumnWidth(20, 13 * 256);
		spreadsheet.setColumnWidth(21, 13 * 256);
		spreadsheet.setColumnWidth(22, 13 * 256);// approx room for 20 char
		// Create row object
		XSSFRow row;

		// TODO
		// Heading font
		XSSFFont fontHeading = workbook.createFont();
		fontHeading.setFontHeightInPoints((short) 18);
		fontHeading.setFontName(FontFactory.HELVETICA_BOLD);
		fontHeading.setBold(true);
		fontHeading.setUnderline(XSSFFont.U_SINGLE);

		// Heading Style
		XSSFCellStyle styleHeading = workbook.createCellStyle();
		styleHeading.setFont(fontHeading);
		styleHeading.setAlignment(XSSFCellStyle.ALIGN_CENTER);

		// Heading row
		int rowId = 0;
		row = spreadsheet.createRow(rowId++);
		XSSFCell cellHeading = row.createCell(0);
		cellHeading.setCellStyle(styleHeading);
		if (request.getParameter("custName") != null) {
			cellHeading.setCellValue("COSTING REPORT : " + request.getParameter("customerName") + " " + request.getParameter("custName"));
		} else {
			cellHeading.setCellValue("COSTING REPORT");
		}

		/*
		 * for (int i = 0; i < 8; ++i) { XSSFCell cellHeading1 =
		 * row.createCell(i); cellHeading1.setCellStyle(styleHeading); if (i ==
		 * 0) { cellHeading1.setCellValue("COSTING REPORT"); } }
		 */
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 24));

		// blank row
		rowId++;

		// *****************************************Main Header
		// Start************************************/
		// MainHeader font
		XSSFFont fontMainHeader = workbook.createFont();
		fontMainHeader.setFontHeightInPoints((short) 11);
		fontMainHeader.setColor(IndexedColors.WHITE.getIndex());
		fontMainHeader.setFontName(FontFactory.HELVETICA);
		fontMainHeader.setBold(true);

		// MainHeader Style
		XSSFCellStyle styleMainHeader = workbook.createCellStyle();
		styleMainHeader.setFont(fontMainHeader);
		styleMainHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleMainHeader.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		styleMainHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleMainHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
		styleMainHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
		styleMainHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleMainHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);

		// spreadsheet.addMergedRegion(new CellRangeAddress(2,2,0,7));// 3 cell
		// (0,1,2)

		// spreadsheet.addMergedRegion(new CellRangeAddress(2,2,8,10));// 2
		// cell(6,7)

		// *****************************************Main Header
		// End************************************//

		// subHeader font
		XSSFFont fontSubHeader = workbook.createFont();
		fontSubHeader.setFontHeightInPoints((short) 10);
		fontSubHeader.setColor(IndexedColors.BLUE.getIndex());
		fontSubHeader.setFontName(FontFactory.TIMES);
		fontSubHeader.setBold(true);

		// subHeader style
		XSSFCellStyle styleSubHeader = workbook.createCellStyle();
		styleSubHeader.setFont(fontSubHeader);
		styleSubHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleSubHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleSubHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleSubHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
		styleSubHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
		styleSubHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleSubHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		styleSubHeader.setDataFormat(workbook.createDataFormat().getFormat("0.00"));

		// styleSubSrNo
		XSSFCellStyle styleSubSrNo = workbook.createCellStyle();
		styleSubSrNo.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleSubSrNo.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		styleSubSrNo.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleSubSrNo.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
		styleSubSrNo.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
		styleSubSrNo.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleSubSrNo.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);

		// subColumns style
		XSSFCellStyle styleSubCol = workbook.createCellStyle();
		// styleSubCol.setFont(fontSubHeader);
		styleSubCol.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleSubCol.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		styleSubCol.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleSubCol.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
		styleSubCol.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
		styleSubCol.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleSubCol.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		styleSubCol.setDataFormat(workbook.createDataFormat().getFormat("0.00"));

		row = spreadsheet.createRow(rowId++);

		XSSFCell srNo = row.createCell(0);
		srNo.setCellStyle(styleSubHeader);
		srNo.setCellValue("SrNo.");

		XSSFCell cnmtNo = row.createCell(1);
		cnmtNo.setCellStyle(styleSubHeader);
		cnmtNo.setCellValue("cnmtNo");

		XSSFCell cnmtDt = row.createCell(2);
		cnmtDt.setCellStyle(styleSubHeader);
		cnmtDt.setCellValue("cnmtDt");

		XSSFCell bCode = row.createCell(3);
		bCode.setCellStyle(styleSubHeader);
		bCode.setCellValue("BranchCode");

		XSSFCell consignee = row.createCell(4);
		consignee.setCellStyle(styleSubHeader);
		consignee.setCellValue("Consignee");

		XSSFCell consignor = row.createCell(5);
		consignor.setCellStyle(styleSubHeader);
		consignor.setCellValue("Consignor");

		XSSFCell frmStn = row.createCell(6);
		frmStn.setCellStyle(styleSubHeader);
		frmStn.setCellValue("frm Station");

		XSSFCell toStn = row.createCell(7);
		toStn.setCellStyle(styleSubHeader);
		toStn.setCellValue("To Station");

		XSSFCell guaranteeWt = row.createCell(8);
		guaranteeWt.setCellStyle(styleSubHeader);
		guaranteeWt.setCellValue("GuaranteeWt");

		XSSFCell cnmtFrt = row.createCell(9);
		cnmtFrt.setCellStyle(styleSubHeader);
		cnmtFrt.setCellValue("Cnmt Amount");

		XSSFCell billNo = row.createCell(10);
		billNo.setCellStyle(styleSubHeader);
		billNo.setCellValue("Bill No");

		XSSFCell bilAmt = row.createCell(11);
		bilAmt.setCellStyle(styleSubHeader);
		bilAmt.setCellValue("Bill Amt");

		XSSFCell chlnNo = row.createCell(12);
		chlnNo.setCellStyle(styleSubHeader);
		chlnNo.setCellValue("Challan No.");

		XSSFCell chlnDt = row.createCell(13);
		chlnDt.setCellStyle(styleSubHeader);
		chlnDt.setCellValue("Chln Date");

		XSSFCell chlnFrt = row.createCell(14);
		chlnFrt.setCellStyle(styleSubHeader);
		chlnFrt.setCellValue("Challan Frt");

		XSSFCell lhpv = row.createCell(15);
		lhpv.setCellStyle(styleSubHeader);
		lhpv.setCellValue("LhpvAmt");

		XSSFCell unLdn = row.createCell(16);
		unLdn.setCellStyle(styleSubHeader);
		unLdn.setCellValue("Unloading");

		XSSFCell det = row.createCell(17);
		det.setCellStyle(styleSubHeader);
		det.setCellValue("Detention");

		XSSFCell oHt = row.createCell(18);
		oHt.setCellStyle(styleSubHeader);
		oHt.setCellValue("OverHeight");

		XSSFCell pnlt = row.createCell(19);
		pnlt.setCellStyle(styleSubHeader);
		pnlt.setCellValue("Penelty");

		XSSFCell extKm = row.createCell(20);
		extKm.setCellStyle(styleSubHeader);
		extKm.setCellValue("Extra Km");

		XSSFCell rcvry = row.createCell(21);
		rcvry.setCellStyle(styleSubHeader);
		rcvry.setCellValue("Recovery");

		XSSFCell costField = row.createCell(22);
		costField.setCellStyle(styleSubHeader);
		costField.setCellValue("Cost");

		XSSFCell netMrgn = row.createCell(23);
		netMrgn.setCellStyle(styleSubHeader);
		netMrgn.setCellValue("Net Margin");

		XSSFCell mrgnPr = row.createCell(24);
		mrgnPr.setCellStyle(styleSubHeader);
		mrgnPr.setCellValue("Margin%");

		Double totFrieght = 0.0;// cnmt or bill amount
		Double totCost = 0.0;// challan or lhpv amount
		Double totUnldng = 0.0;// unloading
		Double totDet = 0.0;// detention
		Double totOvrHt = 0.0;// overheight
		Double totPanlty = 0.0;//
		Double totMrgn = 0.0;
		Double totRcvry = 0.0;
		Double finalCost = 0.0;
		int s=1;

		for (int j = rowId, i = 0; i < list.size(); j++, i++) {

			row = spreadsheet.createRow(j);
			String cnmtCode = (String) list.get(i).get("cnmtCode");
			String cnmtBCode = (String) list.get(i).get("bCode");
			String cnmtConsignee = customerMap.get(list.get(i).get("consignee"));
			String cnmtConsignor = customerMap.get(list.get(i).get("consignor"));
			Double cnmtGuaranteeWt = (Double) list.get(i).get("guaranteeWt");
			String cnmtBillNo = (String) list.get(i).get("billNo");
			String cnDate = CodePatternService.getFormatedDateString(String.valueOf(list.get(i).get("cnmtDt")));
			Double cnmtAmt = (Double) list.get(i).get("cnmtFrt");
			String frmStsn = stationMap.get(list.get(i).get("cnmtFrmSt"));
			String toStsn =  stationMap.get(list.get(i).get("cnmtToSt"));
			String chlnCode = (String) list.get(i).get("chlnCode");
			String chDt = "";
			if (list.get(i).get("chlnDt") != null)
				chDt = CodePatternService.getFormatedDateString(String.valueOf(list.get(i).get("chlnDt")));
			Double chlnFreight = 0.0;
			if (list.get(i).get("chlnFrt") != null)
				chlnFreight = ((Double) list.get(i).get("chlnToFrt"));
			Double lhpvAmt = (Double) list.get(i).get("lhpvAmt");
			Double cost = (Double) list.get(i).get("cost");
			Double net_Margin = (Double) list.get(i).get("netMrgn");
			Double lhpvRcvry = (Double) list.get(i).get("rcvry");
			// Float mrgnPer=(float) Math.round((Float)
			// list.get(i).get("mrgnPr")*100)/100;
			Float mrgnPer = (Float) list.get(i).get("mrgnPr");
			// Float mrgnPer=Float.parseFloat(String.format("%.2f", d));
			// System.out.println("float="+ mrgnPer);
			Double billAmt = (Double) list.get(i).get("billAmt");

			double arDet = 0.0;
			double arUnLdng = 0.0;
			double ovrHt = 0.0;
			double penalty = 0.0;
			double arRcvry = 0.0;
			double recovry = 0.0;
			double arExtKm = 0.0;

			if (list.get(i).get("arDet") != null && lhpvAmt > 0) {
				arDet = Double.parseDouble((String) list.get(i).get("arDet"));
				arUnLdng = Double.parseDouble((String) list.get(i).get(
						"arUnLdng"));
				ovrHt = (Double) list.get(i).get("ovrHt");
				penalty = (Double) list.get(i).get("penalty");
				arRcvry = Double.parseDouble((String) list.get(i)
						.get("arRcvry"));
				arExtKm = (Double) list.get(i).get("arExtKm");
				recovry = arRcvry;
			}
			if (billAmt == null) {
				billAmt = 0.0;
			}

			if (lhpvAmt == null) {
				lhpvAmt = 0.0;
				// arDet=0.0;
				// arUnLdng=0.0;
				// ovrHt=0.0;
				// penalty=0.0;
			} else {
				if (lhpvAmt > 0) {
					recovry = lhpvRcvry;
					if (list.get(i).get("chlnToFrt") != null)
						lhpvAmt = (Double) list.get(i).get("chlnToFrt");
					// else
					// lhpvAmt=0.0;
				} else {
					recovry = arRcvry;
				}
			}
			/*
			 * if(lhpvAmt == 0 ){ arDet=0.0; arUnLdng=0.0; ovrHt=0.0;
			 * penalty=0.0; }
			 */
			if (chlnFreight == null) {
				chlnFreight = 0.0;
			}

			// Calacultion for summary--------------------------------->

			if (cnmtList.contains((String) list.get(i).get("cnmtCode"))) {

			} else {
				// totMrgn=totMrgn+net_Margin;
				if (billAmt > 0) {
					totFrieght = totFrieght + billAmt;
				} else {
					totFrieght = totFrieght + cnmtAmt;
				}
			}

			if (chlnList.contains((String) list.get(i).get("chlnCode"))) {

			} else {

				totUnldng = totUnldng + arUnLdng;
				totDet = totDet + arDet;
				totOvrHt = totOvrHt + ovrHt;
				totPanlty = totPanlty + penalty;
				totRcvry = totRcvry + recovry;

				if (lhpvAmt > 0) {
					totCost = totCost + lhpvAmt;
				} else {
					totCost = totCost + chlnFreight;
				}

			}
			// End of Calculation for summary--------------------------------->

			srNo = row.createCell(0);
			srNo.setCellStyle(styleSubSrNo);
			srNo.setCellValue(s);
			// TODO
			cnmtNo = row.createCell(1);
			cnmtNo.setCellStyle(styleSubCol);
			cnmtNo.setCellValue(cnmtCode);

			cnmtDt = row.createCell(2);
			cnmtDt.setCellStyle(styleSubCol);
			cnmtDt.setCellValue(cnDate);

			bCode = row.createCell(3);
			bCode.setCellStyle(styleSubCol);
			bCode.setCellValue(cnmtBCode);

			consignee = row.createCell(4);
			consignee.setCellStyle(styleSubCol);
			consignee.setCellValue(cnmtConsignee);

			consignor = row.createCell(5);
			consignor.setCellStyle(styleSubCol);
			consignor.setCellValue(cnmtConsignor);

			frmStn = row.createCell(6);
			frmStn.setCellStyle(styleSubCol);
			frmStn.setCellValue(frmStsn);

			toStn = row.createCell(7);
			toStn.setCellStyle(styleSubCol);
			toStn.setCellValue(toStsn);

			guaranteeWt = row.createCell(8);
			guaranteeWt.setCellStyle(styleSubCol);
			guaranteeWt.setCellValue(cnmtGuaranteeWt);

			cnmtFrt = row.createCell(9);
			cnmtFrt.setCellStyle(styleSubCol);
			cnmtFrt.setCellValue(cnmtAmt);

			billNo = row.createCell(10);
			billNo.setCellStyle(styleSubCol);
			billNo.setCellValue(cnmtBillNo);

			bilAmt = row.createCell(11);
			bilAmt.setCellStyle(styleSubCol);
			bilAmt.setCellValue(billAmt);

			chlnNo = row.createCell(12);
			chlnNo.setCellStyle(styleSubCol);
			chlnNo.setCellValue(chlnCode);

			chlnDt = row.createCell(13);
			chlnDt.setCellStyle(styleSubCol);
			chlnDt.setCellValue(chDt);

			chlnFrt = row.createCell(14);
			chlnFrt.setCellStyle(styleSubCol);
			chlnFrt.setCellValue(chlnFreight);

			lhpv = row.createCell(15);
			lhpv.setCellStyle(styleSubCol);
			lhpv.setCellValue(lhpvAmt);

			unLdn = row.createCell(16);
			unLdn.setCellStyle(styleSubCol);
			unLdn.setCellValue(arUnLdng);

			det = row.createCell(17);
			det.setCellStyle(styleSubCol);
			det.setCellValue(arDet);

			oHt = row.createCell(18);
			oHt.setCellStyle(styleSubCol);
			oHt.setCellValue(ovrHt);

			pnlt = row.createCell(19);
			pnlt.setCellStyle(styleSubCol);
			pnlt.setCellValue(penalty);

			extKm = row.createCell(20);
			extKm.setCellStyle(styleSubCol);
			extKm.setCellValue(arExtKm);

			rcvry = row.createCell(21);
			rcvry.setCellStyle(styleSubCol);
			rcvry.setCellValue(recovry);

			costField = row.createCell(22);
			costField.setCellStyle(styleSubCol);
			costField.setCellValue(cost);

			netMrgn = row.createCell(23);
			netMrgn.setCellStyle(styleSubCol);
			netMrgn.setCellValue(net_Margin);

			mrgnPr = row.createCell(24);
			mrgnPr.setCellStyle(styleSubCol);
			mrgnPr.setCellValue(mrgnPer);

			chlnList.add((String) list.get(i).get("chlnCode"));
			cnmtList.add((String) list.get(i).get("cnmtCode"));
			
			list.remove(i);
			i = i-1;
			s++;
			
		}
		
		list.clear();
		list = null;

		/***************************************** SUMMARRY **************************************/
		// -----------------------Calculation----------------------//

		finalCost = totCost + totUnldng + totDet + totOvrHt + totPanlty
				- totRcvry;
		totMrgn = totFrieght - finalCost;
		System.out.println("FilnalCost= " + finalCost);
		System.out.println("Cost= " + totCost);
		System.out.println("totDet= " + totDet);
		System.out.println("totUnloading= " + totUnldng);
		System.out.println("totOvrHt= " + totOvrHt);
		System.out.println("totPanlty= " + totPanlty);
		System.out.println("totFrieght= " + totFrieght);
		System.out.println("totRcvry= " + totRcvry);
		System.out.println("totMrgn= " + totMrgn);
		System.out.println("totMrgn%= " + totMrgn / totFrieght);

		/******************************** SUMMARRY DESIGN ON EXCEL ****************************/
		// TODO

		Integer rowCount = listSize + 6;
		row = spreadsheet.createRow(rowCount - 1);
		XSSFCell smry = row.createCell(1);
		smry = row.createCell(1);
		smry.setCellStyle(styleMainHeader);
		smry.setCellValue("Summarry");
		XSSFCell smry2 = row.createCell(2);
		// smry2= row.createCell(1);
		smry2.setCellStyle(styleMainHeader);
		// smry2.setCellValue("Summarry");
		String merge = "B" + rowCount + ":C" + rowCount;
		System.out.println("Mer : " + merge);
		spreadsheet.addMergedRegion(CellRangeAddress.valueOf(merge));

		row = spreadsheet.createRow(rowCount);
		XSSFCell frt = row.createCell(1);
		frt = row.createCell(1);
		frt.setCellStyle(styleSubHeader);
		frt.setCellValue("Freight");
		XSSFCell frtAmt = row.createCell(2);
		frtAmt = row.createCell(2);
		frtAmt.setCellStyle(styleSubHeader);
		frtAmt.setCellValue(totFrieght);

		row = spreadsheet.createRow(rowCount + 1);
		XSSFCell cost = row.createCell(1);
		cost = row.createCell(1);
		cost.setCellStyle(styleSubHeader);
		cost.setCellValue("Cost");
		XSSFCell costAmt = row.createCell(2);
		costAmt = row.createCell(2);
		costAmt.setCellStyle(styleSubHeader);
		costAmt.setCellValue(totCost);

		row = spreadsheet.createRow(rowCount + 2);
		XSSFCell detnsn = row.createCell(1);
		detnsn = row.createCell(1);
		detnsn.setCellStyle(styleSubHeader);
		detnsn.setCellValue("Detention");
		XSSFCell detAmt = row.createCell(2);
		detAmt = row.createCell(2);
		detAmt.setCellStyle(styleSubHeader);
		detAmt.setCellValue(totDet);

		row = spreadsheet.createRow(rowCount + 3);
		XSSFCell unldng = row.createCell(1);
		unldng = row.createCell(1);
		unldng.setCellStyle(styleSubHeader);
		unldng.setCellValue("Unloading");
		XSSFCell unldngAmt = row.createCell(2);
		unldngAmt = row.createCell(2);
		unldngAmt.setCellStyle(styleSubHeader);
		unldngAmt.setCellValue(totUnldng);

		row = spreadsheet.createRow(rowCount + 4);
		XSSFCell ovrHgt = row.createCell(1);
		ovrHgt = row.createCell(1);
		ovrHgt.setCellStyle(styleSubHeader);
		ovrHgt.setCellValue("Over Height");
		XSSFCell ovrHgtAmt = row.createCell(2);
		ovrHgtAmt = row.createCell(2);
		ovrHgtAmt.setCellStyle(styleSubHeader);
		ovrHgtAmt.setCellValue(totOvrHt);

		row = spreadsheet.createRow(rowCount + 5);
		XSSFCell pnlty = row.createCell(1);
		pnlty = row.createCell(1);
		pnlty.setCellStyle(styleSubHeader);
		pnlty.setCellValue("Penalty");
		XSSFCell pnltyAmt = row.createCell(2);
		pnltyAmt = row.createCell(2);
		pnltyAmt.setCellStyle(styleSubHeader);
		pnltyAmt.setCellValue(totPanlty);

		row = spreadsheet.createRow(rowCount + 6);
		XSSFCell recvry = row.createCell(1);
		recvry = row.createCell(1);
		recvry.setCellStyle(styleSubHeader);
		recvry.setCellValue("Recovery");
		XSSFCell recvryAmt = row.createCell(2);
		recvryAmt = row.createCell(2);
		recvryAmt.setCellStyle(styleSubHeader);
		recvryAmt.setCellValue(totRcvry);

		row = spreadsheet.createRow(rowCount + 7);
		XSSFCell totCst = row.createCell(1);
		totCst = row.createCell(1);
		totCst.setCellStyle(styleSubHeader);
		totCst.setCellValue("Total Cost");
		XSSFCell totCstAmt = row.createCell(2);
		totCstAmt = row.createCell(2);
		totCstAmt.setCellStyle(styleSubHeader);
		totCstAmt.setCellValue(finalCost);

		row = spreadsheet.createRow(rowCount + 8);
		XSSFCell mrgin = row.createCell(1);
		mrgin = row.createCell(1);
		mrgin.setCellStyle(styleSubHeader);
		mrgin.setCellValue("Margin");
		XSSFCell mrginAmt = row.createCell(2);
		mrginAmt = row.createCell(2);
		mrginAmt.setCellStyle(styleSubHeader);
		mrginAmt.setCellValue(totMrgn);

		row = spreadsheet.createRow(rowCount + 9);
		XSSFCell mrginPer = row.createCell(1);
		mrginPer = row.createCell(1);
		mrginPer.setCellStyle(styleSubHeader);
		mrginPer.setCellValue("Margin%");
		XSSFCell mrginPerAmt = row.createCell(2);
		mrginPerAmt = row.createCell(2);
		mrginPerAmt.setCellStyle(styleSubHeader);
		mrginPerAmt.setCellValue((totMrgn / totFrieght) * 100);

		/***********************************/

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=costing.xlsx");
		ServletOutputStream out = response.getOutputStream();
		workbook.write(out);
		out.flush();
		out.close();

		System.out.println("Writesheet.xlsx written successfully");

	}
	
	 @RequestMapping(value="/getStationList",method = RequestMethod.GET)
		public @ResponseBody Object getStationList(){
		 System.out.println("getStationList() CnmtCntlr.java");
			
			List<Station> stationList = stationDAO.getStationList();
			System.out.println("list size="+stationList.size());
			Map<String,Object> map = new HashMap<String,Object>();
			if (!(stationList.isEmpty())) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	  			map.put("list",stationList);	
	  		}else {
	  		  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;	
		}


		@RequestMapping(value="/getCnmtFrmToCwise", method=RequestMethod.POST)
		public @ResponseBody Object getCnmtFrmToCwise(@RequestBody Map<String,Object> userInput){
			System.out.println("getCnmtFrmToCwise()");
			Map<String, Object> map = new HashMap<>();
			
			cnmtFrmToStnList = cnmtDAO.getCnmtFrmToCwise(userInput);
			System.out.println(cnmtFrmToStnList.size());
			if (!cnmtFrmToStnList.isEmpty()) {
				map.put("list", cnmtFrmToStnList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}
	
		
		@RequestMapping(value = "/getCnmtReportXLSX", method = RequestMethod.POST)
		public void getCnmtReportXLSX(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			System.out.println("getCnmtReportXLSX");
			
			//int row=0;
			XSSFWorkbook workbook=new XSSFWorkbook();
			
			//set header font
			
			XSSFFont headerFont=workbook.createFont();
			     headerFont.setBold(true);
			     headerFont.setFontHeightInPoints((short)18);
			     headerFont.setUnderline(XSSFFont.U_SINGLE);
			     headerFont.setFontName(FontFactory.HELVETICA_BOLD);
			     
			     
			 // set header style...
			     
			XSSFCellStyle cellStyle=workbook.createCellStyle();
			      cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			       cellStyle.setFont(headerFont);
			       
			   // set heading row....
			      
			      int rowId=0; 
			      XSSFSheet sheet=workbook.createSheet();
			        XSSFRow headerRow=sheet.createRow(rowId++);
			         XSSFCell headerCell=headerRow.createCell(0);
			                headerCell.setCellStyle(cellStyle);
			          headerCell.setCellValue("Cnmt List");
			          
			     
			          sheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
			      
	/**************************************main header start*****************************************/
			           rowId++;							
						//set main header font
						
			          	XSSFFont mainHeaderFont=workbook.createFont();
			          			mainHeaderFont.setBold(true);
			          			mainHeaderFont.setFontHeightInPoints((short)10);
			          			mainHeaderFont.setFontName(FontFactory.HELVETICA_BOLD);
			          			mainHeaderFont.setColor(IndexedColors.BLACK.getIndex());
						     
						     
						 // set main header style...
						     
						      XSSFCellStyle  mainCellStyle=workbook.createCellStyle();
						      mainCellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
						      mainCellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
						      mainCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
						      mainCellStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
						       mainCellStyle.setFont(mainHeaderFont);
						       
						   // set main heading row....
					String colName[]={"SrNo","BCode","Cnmt No","Date",
										"From Station","To Station","NoOfPkg","Weight",
										"Freight"};
						      XSSFRow mainHeaderRow =sheet.createRow(rowId++);
						       
						       for(int i=0;i<9;i++){
						        XSSFCell  mainHeaderCell=mainHeaderRow.createCell(i);
						          mainHeaderCell.setCellStyle(mainCellStyle);
						          mainHeaderCell.setCellValue(colName[i]);
						          if(i==0 || i==1)
						        	  sheet.setColumnWidth(i,10*256);
						          if(i==3 || i==6 || i ==7 || i==8)
						              sheet.setColumnWidth(i,15*256);
						          if(i==2 || i==4 || i==5)
						        	  sheet.setColumnWidth(i,30*256);
						       }  
						                
						             
			          
/************************************* data present***************************************/
				/*String contentCol[]={"custName","blCustId","blBillNo","blBillDt","blFinalTot","cnmtNo","dedAmt",
						      "dedType","mrNo","mrDate","mrBrhId","mrNetAmt","mrAccessAmt","mrDedAmt"};	*/	
						           XSSFCell contentCell;
						           XSSFCellStyle dataStyle=workbook.createCellStyle();
						             dataStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
						             XSSFCellStyle dataStyle1=workbook.createCellStyle();
						             dataStyle1.setAlignment(XSSFCellStyle.ALIGN_CENTER);
						             XSSFCellStyle dataStyle2=workbook.createCellStyle();
						             XSSFCellStyle dataStyle3=workbook.createCellStyle();
						             dataStyle3.setDataFormat(workbook.createDataFormat().getFormat("0.000"));
						 //TODO            
						            
						 System.out.println("size"+cnmtFrmToStnList.size());
				
						 java.util.Iterator itr = cnmtFrmToStnList.iterator();
						 
						 Long cnmtNoPkg = new Long(0);
						 Double weight = new Double(0);
						 Double fright = new Double(0);
						 
						 int j=1;
						 while (itr.hasNext()) {
							 XSSFRow contentRow=sheet.createRow(rowId++);							 
							 contentCell=contentRow.createCell(0);							 						  
							 contentCell.setCellStyle(dataStyle);								 
							 contentCell.setCellValue(String.valueOf(j));							 
							 Object[] obj = (Object[]) itr.next();
							 for(int i=0;i<8;i++){
								 Double wt=0.0;
								 if(i == 7 || i == 6 || i == 5){
									 if(i == 7)
										 fright = fright + Double.parseDouble(String.valueOf(obj[i]));
									 if(i == 6){
										 wt=(Double.parseDouble(String.valueOf(obj[i])))/1000;
										 weight = weight + wt;
									 }
									 if(i == 5)
										 cnmtNoPkg = cnmtNoPkg + Long.parseLong(String.valueOf(obj[i]));
									 
									 contentCell=contentRow.createCell(i+1);							 						  
									 
									 
									 if( i == 7 || i == 6){
										 contentCell.setCellValue(Double.parseDouble(String.valueOf(obj[i])));
										 contentCell.setCellStyle(dataStyle);
									 }
									 if(i==6){
										 contentCell.setCellValue(wt);
										 contentCell.setCellStyle(dataStyle3);
									 }
									 
									 if(i == 5){
										 contentCell.setCellValue(Long.parseLong(String.valueOf(obj[i])));
										 contentCell.setCellStyle(dataStyle2);
									 }
								 }else{
									 contentCell=contentRow.createCell(i+1);							 						  
									 contentCell.setCellStyle(dataStyle1);								 
									 contentCell.setCellValue(String.valueOf(obj[i]));
								 }
								 
								 
							 } 
							 j++;
						 }
					 
		/**************** set footer**********************************************/		
					 
					 
					 XSSFFont footerFont=workbook.createFont();
				     footerFont.setBold(true);
				     footerFont.setFontName(FontFactory.HELVETICA_BOLD);
				     
				     
				 // set header style...
				     
				XSSFCellStyle footerCellStyle=workbook.createCellStyle();
				     // footerCellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				       footerCellStyle.setFont(footerFont);	 
				       XSSFCellStyle footerCellStyle1=workbook.createCellStyle();
					      footerCellStyle1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
					       footerCellStyle1.setFont(footerFont);
			   // rowId++;
				       sheet.addMergedRegion(new CellRangeAddress(rowId,rowId,0,8));
				       XSSFCell  mainHeaderCell=mainHeaderRow.createCell(rowId);
				          mainHeaderCell.setCellStyle(mainCellStyle);
		XSSFRow footerRow=sheet.createRow(++rowId);
		           XSSFCell cell=footerRow.createCell(0);
		             cell.setCellStyle(footerCellStyle1);
		             cell.setCellValue("TOTAL");
		           cell=footerRow.createCell(6);
		             cell.setCellStyle(footerCellStyle);
		             cell.setCellValue(cnmtNoPkg);
		             cell=footerRow.createCell(7);
		             cell.setCellStyle(footerCellStyle);
		             cell.setCellValue(weight);
		             cell=footerRow.createCell(8);
		             cell.setCellStyle(footerCellStyle);
		             cell.setCellValue(fright);
		             //cell.setCellValue("#############################################################");
		             sheet.addMergedRegion(new CellRangeAddress(rowId,rowId,0,5));
			  
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=CnmtList.xlsx");
         ServletOutputStream out= response.getOutputStream();
         workbook.write(out);
         out.close();

         System.out.println("Writesheet.xlsx written successfully" );

		}
		
		
		@RequestMapping(value = "/getCnmtCodeByCode", method = RequestMethod.POST)
		public @ResponseBody Object getCnmtCodeByCode(@RequestBody String cnmtCode){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			User user = (User) httpSession.getAttribute("currentUser");
			logger.info("UserID = "+user.getUserId()+" : Enter into /getCnmtCodeByCode");
			Session session = this.sessionFactory.openSession();
			try{
				List<BranchStockLeafDet> cnmtCodeList = branchStockLeafDetDAO.getCodeList(session, user, cnmtCode, "cnmt"); 
				if(cnmtCodeList != null && !cnmtCodeList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("cnmtCodeList", cnmtCodeList);
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such cnmt found !");
				}
			}catch(Exception e){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such cnmt found !");
				logger.error("UserID = "+user.getUserId()+" : Exception : "+e);
			}finally{
				session.clear();
				session.close();
			}
			logger.info("Exit from /getCnmtCodeByCode");
			return resultMap;
		}

	
		
		
		// TODO Costing Excel
		@RequestMapping(value = "/getHoCostingReportXLSX", method = RequestMethod.POST)		
		public @ResponseBody Object getHoCostingReportXLSX(@RequestBody Map<String, Object> initParam) throws Exception {
			User user = (User) httpSession.getAttribute("currentUser");
			logger.info("UserID = "+user.getUserId() + " : Enter into /getHoCostingReportXLSX() : InitParam = "+initParam);		
			
			Map<String, Object> resultMap = new HashMap<String, Object>();			
			Session session = this.sessionFactory.openSession();
			
			try{
				List<Map<String, Object>> list = cnmtDAO.getHoCostingReport(session, user, initParam);
				
				Set<String> cnmtList = new HashSet<String>();
				Set<String> chlnList = new HashSet<String>();
				
				if(! list.isEmpty()){
					
					Double totFrieght = 0.0;// cnmt or bill amount
					Double totCost = 0.0;// challan or lhpv amount
					Double totUnldng = 0.0;// unloading
					Double totDet = 0.0;// detention
					Double totOvrHt = 0.0;// overheight
					Double totPanlty = 0.0;//
					Double totMrgn = 0.0;
					Double totRcvry = 0.0;
					Double finalCost = 0.0;
					
					List<Map<String, Object>> cnmtCostingList = new ArrayList<Map<String,Object>>();
					
					for(int i=0; i<list.size(); i++){
						
						Double cnmtAmt = (Double) list.get(i).get("cnmtFrt");												
						
						Double chlnFreight = 0.0;
						if (list.get(i).get("chlnFrt") != null)
							chlnFreight = Double.parseDouble((String) list.get(i).get("chlnFrt"));
						
						Double lhpvAmt = (Double) list.get(i).get("lhpvAmt");
						Double cost = (Double) list.get(i).get("cost");
						Double net_Margin = (Double) list.get(i).get("netMrgn");
						Double lhpvRcvry = (Double) list.get(i).get("rcvry");							
						Float mrgnPer = (Float) list.get(i).get("mrgnPr");
						Double billAmt = (Double) list.get(i).get("billAmt");

						double arDet = 0.0;
						double arUnLdng = 0.0;
						double ovrHt = 0.0;
						double penalty = 0.0;
						double arRcvry = 0.0;
						double recovry = 0.0;
						double arExtKm = 0.0;
						
						if (list.get(i).get("arDet") != null) {
							arDet = Double.parseDouble((String) list.get(i).get("arDet"));
							arUnLdng = Double.parseDouble((String) list.get(i).get(
									"arUnLdng"));
							ovrHt = (Double) list.get(i).get("ovrHt");
							penalty = (Double) list.get(i).get("penalty");
							arRcvry = Double.parseDouble((String) list.get(i)
									.get("arRcvry"));
							arExtKm = (Double) list.get(i).get("arExtKm");
							recovry = arRcvry;
						}
						
						if (lhpvAmt == null) {
							lhpvAmt = 0.0;						
						} else {
							if (lhpvAmt > 0) {
								recovry = lhpvRcvry;
								if (list.get(i).get("chlnToFrt") != null)
									lhpvAmt = (Double) list.get(i).get("chlnToFrt");								
							} else {
								recovry = arRcvry;
							}
						}						
						if (chlnFreight == null) {
							chlnFreight = 0.0;
						}

						// Calacultion for summary--------------------------------->

						if (cnmtList.contains((String) list.get(i).get("cnmtCode"))) {
						} else {
							// totMrgn=totMrgn+net_Margin;
							if (billAmt > 0) {
								totFrieght = totFrieght + billAmt;
							} else {
								totFrieght = totFrieght + cnmtAmt;
							}
						}

						if (chlnList.contains((String) list.get(i).get("chlnCode"))) {
						} else {
							totUnldng = totUnldng + arUnLdng;
							totDet = totDet + arDet;
							totOvrHt = totOvrHt + ovrHt;
							totPanlty = totPanlty + penalty;
							totRcvry = totRcvry + recovry;
							if (lhpvAmt > 0) {
								totCost = totCost + lhpvAmt;
							} else {
								totCost = totCost + chlnFreight;
							}
						}
						
						// Creating row
						Map<String, Object> rowMap = new HashMap<String, Object>();
						
						rowMap.put("cnmtCode", list.get(i).get("cnmtCode"));
						rowMap.put("cnmtDt", list.get(i).get("cnmtDt"));
						rowMap.put("cnmtBCode", list.get(i).get("bCode"));
						rowMap.put("cnmtConsignee", list.get(i).get("consignee"));
						rowMap.put("cnmtConsignor", list.get(i).get("consignor"));
						rowMap.put("cnmtFrmStn", list.get(i).get("cnmtFrmStn"));
						rowMap.put("cnmtToStn", list.get(i).get("cnmtToStn"));						
						rowMap.put("cnmtGuaWt", list.get(i).get("guaranteeWt"));
						rowMap.put("cnmtAmt", cnmtAmt);
						rowMap.put("cnmtBillNo", list.get(i).get("billNo"));
						rowMap.put("cnmtBillAmt", billAmt);
						rowMap.put("cnmtChlnCode", list.get(i).get("chlnCode"));
						rowMap.put("cnmtChlnDt", list.get(i).get("chlnDt"));
						rowMap.put("cnmtChlnFrt", chlnFreight);
						rowMap.put("cnmtLhpvAmt", lhpvAmt);
						
						
						rowMap.put("cnmtUnl", arUnLdng);
						rowMap.put("cnmtDetention", arDet);
						rowMap.put("cnmtOverHeight", ovrHt);
						rowMap.put("cnmtPenelty", penalty);
						rowMap.put("cnmtExtraKm", arExtKm);
						rowMap.put("cnmtRecovery", recovry);
						rowMap.put("cnmtCost", cost);
						rowMap.put("cnmtNetMer", net_Margin);
						rowMap.put("cnmtNetMerPer",  mrgnPer);		
						
						chlnList.add((String) list.get(i).get("chlnCode"));
						cnmtList.add((String) list.get(i).get("cnmtCode"));
						
						cnmtCostingList.add(rowMap);
					}
					
					if(! cnmtCostingList.isEmpty()){
						finalCost = totCost + totUnldng + totDet + totOvrHt + totPanlty	- totRcvry;
						totMrgn = totFrieght - finalCost;
						
						Map<String, Object> smry = new HashMap<String, Object>();
						smry.put("totFrt", totFrieght);
						smry.put("cost", totCost);
						smry.put("totDet", totDet);
						smry.put("totUnl", totUnldng);
						smry.put("totOvHt", totOvrHt);
						smry.put("totPen", totPanlty);
						smry.put("totRcv", totRcvry);
						smry.put("totCost", finalCost);
						smry.put("totMrg", totMrgn);
						smry.put("totMrgPer", totMrgn / totFrieght);
						
						resultMap.put("cnmtCostingList", cnmtCostingList);
						resultMap.put("smry", smry);
						
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record !");
					}
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record !");
				}
			}catch(Exception e){
				logger.info("UserID = "+user.getUserId()+ " : Exception = "+e);
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please try again !");
			}finally{
				session.clear();
				session.close();
			}
			
			logger.info("UserID = "+user.getUserId() + " : Exit from /getHoCostingReportXLSX() : Result = "+resultMap.get("result"));
			return resultMap;
		}
		
		@RequestMapping(value = "/alreadyNarr", method = RequestMethod.POST)
		public @ResponseBody Object alreadyNarr(@RequestBody Narration narr){
			User user = (User) httpSession.getAttribute("currentUser");
			logger.info("UserID = "+user.getUserId() + " : Enter into alreadyNarr()");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			Session session = this.sessionFactory.openSession();
			try{
				Boolean isValid = cnmtDAO.isCodeValid(session, user, narr);
				if(isValid){
					Boolean isAlreay = cnmtDAO.isAlreadyNarrtion(session, user, narr);
					if(! isAlreay){
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);					
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
						resultMap.put("msg", "This code already has narration !");
					}
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
					resultMap.put("msg", "Code is not valid !");
				}
			}catch(Exception e){
				logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
				resultMap.put("msg", "Please try again !");
			}finally{
				session.clear();
				session.close();
			}
			logger.info("UserID = "+user.getUserId() + " : Enter into alreadyNarr() : Result = "+resultMap.get(ConstantsValues.RESULT));
			return resultMap;
		}
		
		@RequestMapping(value = "/createNarr", method = RequestMethod.POST)
		public @ResponseBody Object createNarr(@RequestBody Narration narr){
			User user = (User) httpSession.getAttribute("currentUser");
			logger.info("UserID = "+user.getUserId() + " : Enter into createNarr()");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			Session session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			try{
				Boolean isValidCode = cnmtDAO.isCodeValid(session, user, narr);
				if(isValidCode){
					Boolean isAlreay = cnmtDAO.isAlreadyNarrtion(session, user, narr);
					if(! isAlreay){
						narr.setNarrUserCode(user.getUserCode());
						narr.setNarrUserBCode(user.getUserBranchCode());
						Integer id = cnmtDAO.saveNarration(session, user, narr);
						if(id > 0){
							transaction.commit();
							resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
							resultMap.put("msg", "Narration is saved !");
						}else{
							transaction.rollback();
							resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
							resultMap.put("msg", "Narration is not saved !");
						}
					}else{
						transaction.rollback();
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
						resultMap.put("msg", "This code already has narration !");
					}					
				}else{
					transaction.rollback();
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
					resultMap.put("msg", "This code is not valid !");
				}
			}catch(Exception e){
				transaction.rollback();
				logger.error("UserID = "+user.getUserId() + " : Exception = "+e);	
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
				resultMap.put("msg", "Please try again !");
			}finally{
				session.clear();
				session.close();
			}
			logger.info("UserID = "+user.getUserId() + " : Enter into createNarr() : Result = "+resultMap.get(ConstantsValues.RESULT));
			return resultMap;
		}
		
	@RequestMapping(value = "/getCnmtByGroup", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtByGroup(@RequestBody CustomerGroup custGroup){
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("UserID = "+user.getUserId() + " : Enter into /getCnmtByGroup() : GroupID = "+custGroup.getGroupId());
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			List<String> custCodes = customerGroupDAO.getCustCodesByGroup(session, user, custGroup.getGroupId());
			if(custCodes.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "This group does not have customer !");
			}else{		
				List<String> cnmtForNBill = cnmtDAO.getCnmtByCustCodes(session, user, custCodes, "NBill");
				List<String> cnmtForSBill = cnmtDAO.getCnmtByCustCodes(session, user, custCodes, "SBill");			
				
				resultMap.put("cnmtForNBill", cnmtForNBill);
				resultMap.put("cnmtForSBill", cnmtForSBill);
				
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}
		}catch(Exception e){			
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}finally{
			session.clear();
			session.close();
		}
		
		logger.info("UserID = "+user.getUserId() + " : Exit from /getCnmtByGroup() : Result = "+resultMap.get(ConstantsValues.RESULT));
		return resultMap;
	}
		
}