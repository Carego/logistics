package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.model.Broker;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayBrokerCntlr {
	
	@Autowired
	BrokerDAO brokerDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@RequestMapping(value="/getBrkIsViewNo	",method = RequestMethod.POST)
	public @ResponseBody Object getBrkIsViewNo(){
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		String branchCode = currentUser.getUserBranchCode();
		
		List<Broker> brokers = brokerDAO.getBrokerisViewFalse(branchCode);
		
		if(!brokers.isEmpty()){
			map.put("list",brokers);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,"No more broker to show");
		}
		return map;
	}
	
	@RequestMapping(value = "/getBrokerCode" , method = RequestMethod.GET)  
	  public @ResponseBody Object getBrokerCode(){
	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> brokers = brokerDAO.getBrokerCodes(branchCode);
		String brkCodes [] = new String[brokers.size()];
		
		for(int i=0;i<brokers.size();i++){
			brkCodes[i]=brokers.get(i);	
		}
		
		return brkCodes;
		} 
	
	@RequestMapping(value = "/updateIsViewBroker", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewBroker(@RequestBody String selection) {

		String contids[] = selection.split(",");
		
		int temp = brokerDAO.updateBrokerisViewTrue(contids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/brokerDetails", method = RequestMethod.POST)
	public @ResponseBody Object brokerDetails(@RequestBody String brkCode) {
		
		Broker broker = new Broker();
		List<Broker> bList = brokerDAO.getBrokerList(brkCode);
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (!bList.isEmpty()) {
			broker = bList.get(0);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("broker", broker);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/EditBrokerSubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditBrokerSubmit(@RequestBody Broker broker) {

		int temp=brokerDAO.updateBroker(broker);
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(temp>0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getBrokerCodesList", method = RequestMethod.POST)
	public @ResponseBody Object getBrokerCodesList() {
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> bList = brokerDAO.getBrokerCodes(branchCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
}
