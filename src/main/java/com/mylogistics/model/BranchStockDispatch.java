package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branchstockdispatch")
public class BranchStockDispatch {
	
	@Id
	@Column(name="brsDisId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brsDisId;
	
	@Column(name="brsDisCode")
	private String brsDisCode;
	
	@Column(name="brsDisBranchCode")
	private String brsDisBranchCode;
	
	@Column(name="brsDisCnmt")
	private int brsDisCnmt;
	
	@Column(name="brsDisChln")
	private int brsDisChln;
	
	@Column(name="brsDisSedr")
	private int brsDisSedr;
	
	@Column(name="brsDisDt")
	private Date brsDisDt;
	
	@Column(name="isAdminView")
	private String isAdminView;
	
	@Column(name="isStockRecieved")
	private String isStockRecieved;
	
	@Column(name="dispatchMsg")
	private String dispatchMsg;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="brsDisRcvDt")
	private Date brsDisRcvDt;
	
	@Column(name="brsDisInvNo")
	private String brsDisInvNo;

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	public String getIsAdminView() {
		return isAdminView;
	}

	public void setIsAdminView(String isAdminView) {
		this.isAdminView = isAdminView;
	}

	public String getIsStockRecieved() {
		return isStockRecieved;
	}

	public void setIsStockRecieved(String isStockRecieved) {
		this.isStockRecieved = isStockRecieved;
	}


	public int getBrsDisId() {
		return brsDisId;
	}

	public void setBrsDisId(int brsDisId) {
		this.brsDisId = brsDisId;
	}

	public String getBrsDisCode() {
		return brsDisCode;
	}

	public void setBrsDisCode(String brsDisCode) {
		this.brsDisCode = brsDisCode;
	}

	public String getBrsDisBranchCode() {
		return brsDisBranchCode;
	}

	public void setBrsDisBranchCode(String brsDisBranchCode) {
		this.brsDisBranchCode = brsDisBranchCode;
	}

	public int getBrsDisCnmt() {
		return brsDisCnmt;
	}

	public void setBrsDisCnmt(int brsDisCnmt) {
		this.brsDisCnmt = brsDisCnmt;
	}

	public int getBrsDisChln() {
		return brsDisChln;
	}

	public void setBrsDisChln(int brsDisChln) {
		this.brsDisChln = brsDisChln;
	}

	public int getBrsDisSedr() {
		return brsDisSedr;
	}

	public void setBrsDisSedr(int brsDisSedr) {
		this.brsDisSedr = brsDisSedr;
	}

	public Date getBrsDisDt() {
		return brsDisDt;
	}

	public void setBrsDisDt(Date brsDisDt) {
		this.brsDisDt = brsDisDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
	public String getDispatchMsg() {
		return dispatchMsg;
	}

	public void setDispatchMsg(String dispatchMsg) {
		this.dispatchMsg = dispatchMsg;
	}

	public Date getBrsDisRcvDt() {
		return brsDisRcvDt;
	}

	public void setBrsDisRcvDt(Date brsDisRcvDt) {
		this.brsDisRcvDt = brsDisRcvDt;
	}

	public String getBrsDisInvNo() {
		return brsDisInvNo;
	}

	public void setBrsDisInvNo(String brsDisInvNo) {
		this.brsDisInvNo = brsDisInvNo;
	}
}
