<div ng-show="superAdminLogin">
<div class="container">
	<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
	<div class="col s12 m12 l6" ng-if="superAdminLogin">
	
			
	<div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Welcome Super Admin</span>
              <p></p>
            </div>
            <!-- <div class="card-action center">
            <a href="#/superAdmin" class="btn white-text" ng-click="superAdmin()">superAdmin</a>
			<a href="#/admin" class="btn white-text" ng-click="admin()">admin</a>
			<a href="#/operator" class="btn white-text" ng-click="operator()">operator</a>
            </div> -->
     </div>
	</div>
	</div>
</div>
	<div class="row">
       <div class="col s12">
         <div class="card blue-grey darken-1">
           <div class="card-content white-text">
             <span class="card-title">Stationary Notification</span>
             	<table>
		 	 		<tr>
		 	 			<th>CnmtDaysLast</th>
		 	 		    <th>CnmtAvgMonUsg</th>				
						<th>ChlnDaysLast</th>
						<th>ChlnAvgMonUsg</th>
						<th>SedrDaysLast</th>
						<th>SedrAvgMonUsg</th>
					</tr>
		 	  	  
				  <tr>
		              <td>{{cnmtMonthsLast}} month {{cnmtDaysLast}} days</td>
		              <td>{{cnmtAvgMonUsg | number:2}}</td>
		              <td>{{chlnMonthsLast}} month {{chlnDaysLast}} days</td>
		              <td>{{chlnAvgMonUsg | number:2}}</td>
		              <td>{{sedrMonthsLast}} month {{sedrDaysLast}} days</td>
		              <td>{{sedrAvgMonUsg | number:2}}</td>
		          </tr>
         	  </table>
             <input type="button" value="Create Stationary Order" ng-click="createStnOrder()"/>
           </div>
         </div>
       </div>
     </div> 
     
    <div class="row" ng-if="HBOSNList.length > 0">
       <div class="col s12">
         <div class="card blue-grey darken-1">
           <div class="card-content white-text">
             <span class="card-title">Stationary Order</span>
              	
	        	<table>
              	
	              <tr>
	              	<td></td>
	              	<td>id</td>
					<td>Cnmt Number</td>
					<td>Chln Number</td>
					<td>Sedr Number</td>
					<td>Stationary Status</td>
	              </tr>
	              
	              <tr ng-repeat="HBOSN in HBOSNList">
	              <td><input type="checkbox" name="check" ></td>
	              	<td>{{HBOSN.hBOSN_Id}}</td>
					<td>{{HBOSN.hBOSN_cnmtNo}}</td>
					<td>{{HBOSN.hBOSN_chlnNo}}</td>
					<td>{{HBOSN.hBOSN_sedrNo}}</td>
					<td>{{HBOSN.hBOSN_hasRec}}</td>
					<td><input type="button" value="confirm order" ng-click="confirmOrder(HBOSN)"></td>
					<!-- <td>{{HBOSN.creationTS | date : format : timezone}}</td> -->
					<!-- <td><input type="button" value="Create Stationary Order" ng-click="createStnOrder(HBOSN)"></td>
					<td><input type="button" value="Receive Stationary Order" ng-click="recStnOrder()"></td> -->
					<!-- <td><input type="button" value="Create Dispatch Order" ng-click="createOrder(hbo.bCode)"></td> -->
	              </tr>
	              
	            <!--   <tr>
	              	<td><input type ="date" ng-model="dispatchDate"></td>
	              	 <td><input type="button" ng-click="getListWithDate(dispatchDate)"></td>
	              </tr> -->
	              
           	 </table>
        
             <!-- <input type="button" value="Create Stationary Order" ng-click="createStnOrder()"/>  -->     
           </div>
         </div>
       </div>
     </div> 
     
     
     
    <div id="openStnOrderDB" ng-hide="createStnOrderFlag">
    	
    	 <form name="stnOrderForm" ng-submit="saveStnOrder(hbosn,stnOrderForm)">
	 	  	<div class="row" style="margin-top: 40px;">
	 	  	<div class="input-field col s6">
	        	<input type ="text" name ="hbon.operatorCode" ng-model="hbosn.hBOSN_opCode" ng-click="selectOperatorCode()" required readOnly>
	      			<label>Operator Code</label>
	      	</div>
	      	
	       	<div class="input-field col s6">
	        	<input type ="number" name ="hbon.noCnmt" ng-model="hbosn.hBOSN_cnmtNo" required>
	      			<label>No. Of CNMT</label>
	      	</div>
	      	<div class="input-field col s6">
	        	<input type ="number" name ="hbon.noChln" ng-model="hbosn.hBOSN_chlnNo" required>
	      			<label>No. Of Challan</label>
	      	</div>
	      	<div class="input-field col s6">
	        	<input type ="number" name ="hbon.noSedr" ng-model="hbosn.hBOSN_sedrNo">
	      			<label>No. Of SEDR</label>
	      	</div>
	      	<div class="col s12 center">
	      	<input type="submit">
	      	</div>
	      	</div>
 	  </form>    
    
    </div> 
 
 
<div id ="selectOperatorCodeModel" ng-hide = "operatorCodeFlag">
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Operator Code</th>
				
				<th>User Name</th>			
				
			</tr>
			
			 <tr ng-repeat="operator in operatorList">
		 	  <td><input type="radio"  name="operatorName" value="{{ operator.userCode }}"  ng-click="selectOperator(operator.userCode)"></td>
              <td>{{ operator.userCode }}</td>
              <td>{{ operator.userName }}</td>
          </tr>
         </table>
</div>	
</div>       