package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.MultipleState;

@Repository
public interface MultipleStateDAO {
	
	//Sanjana: saves the multiple state to database
	public int saveMultipleState(MultipleState multipleState);
	
	public List<MultipleState> getMultipleState(String code);

	public MultipleState deleteState(String code);
}
