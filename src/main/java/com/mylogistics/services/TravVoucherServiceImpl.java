package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

public class TravVoucherServiceImpl implements TravVoucherService{

	private List<TravDivService> tdSerList = new ArrayList<TravDivService>();
	
	public void addTDService(TravDivService travDivService){
		tdSerList.add(travDivService);
	}
	
	public List<TravDivService> getAllTDService(){
		return tdSerList;
	}
	
	public void deleteAllTDService(){
		tdSerList.clear();
	}
	
	public void remove(int index){
		tdSerList.remove(index);
	}
}
