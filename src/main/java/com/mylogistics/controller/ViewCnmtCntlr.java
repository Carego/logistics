package com.mylogistics.controller;

import java.sql.Blob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Employee;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ModelService;

@Controller
public class ViewCnmtCntlr {
	
	private ModelService modelService = new ModelService();
	public static Logger logger = Logger.getLogger(ViewCnmtCntlr.class);
	
	@Autowired
	private RateByKmDAO rateByKmDAO;
	
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private StateDAO stateDAO;
	
	
	@RequestMapping(value="/getContractDataForView",method = RequestMethod.POST)
	public @ResponseBody Object getContractDataForView(@RequestBody Map<String,Object> clientMap){
		
		System.out.println("--------->>>>>>"+clientMap.get("custCode"));
		System.out.println("--------->>>>>>"+clientMap.get("cnmtDt"));
		System.out.println("--------->>>>>>"+clientMap.get("cnmtFromSt"));
		System.out.println("from client------>"+clientMap.get("cnmtDt").getClass());
		
		Date cnmtDt = null;
		java.sql.Date sql = null;
        String reqDate = (String) clientMap.get("cnmtDt");
        DateFormat formatter = null;
        try {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            cnmtDt = formatter.parse(reqDate); // birtDate is a string
            sql = new java.sql.Date(cnmtDt.getTime());
        }
        catch (Exception e) {
            System.out.println("Exception :" + e);
        }
        
        System.out.println("%%%%%%%%%%%%%%%%%");
        System.out.println("%%%%%%%%%%%%%%%%%"+sql);
        
		String custCode = (String) clientMap.get("custCode");
		//cnmtDt = (Date) clientMap.get("cnmtDt");
		String cnmtFromSt = (String) clientMap.get("cnmtFromSt");
		Map<String,Object> finalMap = new HashMap<String,Object>();
		List<Map<String, Object>> regDailyContList = new ArrayList<Map<String,Object>>();
	
		List<RegularContract>  regularContractList = regularContractDAO.getRegContForCnmt(custCode,cnmtFromSt);
		List<DailyContract> dailyContractList = dailyContractDAO.getDailyContForCnmt(custCode,cnmtFromSt);
		
		List<RegularContract> finalRegContList = new ArrayList<RegularContract>();
		List<DailyContract> finalDlyContList = new ArrayList<DailyContract>();
		
		if (!regularContractList.isEmpty()) {
			for (int i = 0; i < regularContractList.size(); i++) {
				System.out.println("yoooooooooooooooooo"+regularContractList.get(i).getRegContFromDt());
				System.out.println("Noooooooooooooooooo"+regularContractList.get(i).getRegContToDt());
				int before = regularContractList.get(i).getRegContFromDt().compareTo(sql);
				int after = regularContractList.get(i).getRegContToDt().compareTo(sql);
				if(before <= 0 && after >= 0){
					System.out.println("-------------&&&&&&&&&&&&----------------");
					finalRegContList.add(regularContractList.get(i));
				}else{
					System.out.println("-------------%%%%%%%%%%%%----------------");
				}
			}	
				
			for(int i=0;i<finalRegContList.size();i++){
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("custCode",regularContractList.get(i).getRegContBLPMCode());
				map.put("contCode", regularContractList.get(i).getRegContCode());
				map.put("fromStation", regularContractList.get(i).getRegContFromStation());
				map.put("toStation", regularContractList.get(i).getRegContToStation());
				map.put("fromDate", regularContractList.get(i).getRegContFromDt());
				map.put("toDate", regularContractList.get(i).getRegContToDt());
				map.put("rate", regularContractList.get(i).getRegContRate());
				map.put("cnmtDC", regularContractList.get(i).getRegContDc());
				map.put("cnmtDDL", regularContractList.get(i).getRegContDdl());
				map.put("cnmtCostGrade", regularContractList.get(i).getRegContCostGrade());
				map.put("contType",regularContractList.get(i).getRegContType());
				map.put("proportionate",regularContractList.get(i).getRegContProportionate());
				//map.put("additionalRate",regularContractList.get(i).getRegContAdditionalRate());
				/*map.put("fromWeight",regularContractList.get(i).getRegContFromWt());
				map.put("toWeight",regularContractList.get(i).getRegContToWt());*/
				regDailyContList.add(map);
			}
				
		}
		
		if (!dailyContractList.isEmpty()) {
			for (int i = 0; i < dailyContractList.size(); i++) {
				int before = dailyContractList.get(i).getDlyContStartDt().compareTo(sql);
				int after = dailyContractList.get(i).getDlyContEndDt().compareTo(sql);
				if(before <= 0 && after >= 0){
					System.out.println("-------------&&&&&&&&&&&&----------------");
					finalDlyContList.add(dailyContractList.get(i));
				}else{
					System.out.println("-------------%%%%%%%%%%%%----------------");
				}
			}
			
			for(int i=0;i<finalDlyContList.size();i++){
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("custCode",dailyContractList.get(i).getDlyContBLPMCode());
				map.put("contCode", dailyContractList.get(i).getDlyContCode());
				map.put("fromStation", dailyContractList.get(i).getDlyContFromStation());
				map.put("toStation", dailyContractList.get(i).getDlyContToStation());
				map.put("fromDate", dailyContractList.get(i).getDlyContStartDt());
				map.put("toDate", dailyContractList.get(i).getDlyContEndDt());
				map.put("rate", dailyContractList.get(i).getDlyContRate());
				map.put("cnmtDC", dailyContractList.get(i).getDlyContDc());
				map.put("cnmtDDL", dailyContractList.get(i).getDlyContDdl());
				map.put("cnmtCostGrade", dailyContractList.get(i).getDlyContCostGrade());
				map.put("contType",dailyContractList.get(i).getDlyContType());
				map.put("proportionate",dailyContractList.get(i).getDlyContProportionate());
				//map.put("additionalRate",dailyContractList.get(i).getDlyContAdditionalRate());
				/*map.put("fromWeight",dailyContractList.get(i).getDlyContFromWt());
				map.put("toWeight",dailyContractList.get(i).getDlyContToWt());*/
				regDailyContList.add(map);
			}
			
		}
		
		if(!regDailyContList.isEmpty()){
			finalMap.put("list",regDailyContList);
			finalMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			finalMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		//System.out.println("first value"+regDailyContList);
		return finalMap;
	}
	
	
	@RequestMapping(value="/getCnmtList", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtList(@RequestBody String cnmtCode){
		logger.info("Enter into /getCnmtList() : CNMT Code : "+cnmtCode);		
		List<String> cnmtList= cnmtDAO.getCnmtCode(cnmtCode);		
		logger.info("CNMT List Size : "+cnmtList.size());
		Map<String,Object> map = new HashMap<String,Object>();
		if (!cnmtList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", cnmtList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	 @RequestMapping(value="/getCnmtCodeListForView",method = RequestMethod.POST)
		public @ResponseBody Object getCnmtCodeListForView(){
			System.out.println("enter into getCnmtCodeListForView function----->");
			User currentUser = (User)httpSession.getAttribute("currentUser");
			String branchCode = currentUser.getUserBranchCode();
			List<BranchStockLeafDet> cnmtCodeList= branchStockLeafDetDAO.getCodeList(branchCode,"cnmt");
			System.out.println("size of cnmtCodeList = "+cnmtCodeList.size());
			Map<String,Object> map = new HashMap<String,Object>();
			if (!cnmtCodeList.isEmpty()){
				System.out.println("successfully send cnmt list");
				map.put("list", cnmtCodeList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;	
		}
	 
	 @RequestMapping(value="/getCustomerListForView",method = RequestMethod.GET)
		public @ResponseBody Object getCustomerListForView(){
			System.out.println("enter into getCustomerList function");
			User currentUser = (User) httpSession.getAttribute("currentUser");
			List<Customer> customerList = customerDAO.getCustomerForCnmt(currentUser.getUserBranchCode());
			Map<String,Object> map = new HashMap<String,Object>();
			if (!customerList.isEmpty()){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("list",customerList );
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;	
		}
	 
	 @RequestMapping(value="/getListOfEmployeeForView",method = RequestMethod.POST)
		public @ResponseBody Object getListOfEmployeeForView(){
			
			List<Employee> employeeList = employeeDAO.getAllActiveEmployees();
			System.out.println("list of employee------>>>>"+employeeList);
			Map<String,Object> map = new HashMap<String,Object>();
			if (!employeeList.isEmpty()){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("list",employeeList);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;		
		}
		
		@RequestMapping(value="/getStateCodeDataForView",method = RequestMethod.POST)
		public @ResponseBody Object getStateCodeDataForView(){
		
			List<State> stateList = stateDAO.getStateData();
			System.out.println("list of employee------>>>>"+stateList);
			Map<String,Object> map = new HashMap<String,Object>();
			if (!stateList.isEmpty()){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("list",stateList );
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;	
		}
		
		
	  	
		@RequestMapping(value = "/viewcnmt", method = RequestMethod.POST)
		public @ResponseBody Object customerDetails(@RequestBody String cnmtCode)throws Exception{
			Cnmt cnmt = new Cnmt();	   	   
			List<Cnmt> list = cnmtDAO.getCnmt(cnmtCode);
			List<Map<String, Object>> chlnNSedrByCnmtList = cnmtDAO.getChlnNSedrByCnmt(cnmtCode);
			Map<String,Object> map = new HashMap<String , Object>();
			Map<String,Object> cnmtMap = new HashMap<String, Object>();
			  
			if(!list.isEmpty()){
				
				String contCode = list.get(0).getContractCode(); 
				String chckCode = contCode.substring(0,3);
				if(chckCode.equalsIgnoreCase("dly")){
					List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);	
				}else if(chckCode.equalsIgnoreCase("dly")){
					
				}
				
				cnmtMap.put(CnmtCNTS.CNMT_ID,list.get(0).getCnmtId());
				cnmtMap.put(CnmtCNTS.CNMT_CODE,list.get(0).getCnmtCode() );
				cnmtMap.put(CnmtCNTS.BRANCH_CODE,list.get(0).getBranchCode());
				cnmtMap.put(CnmtCNTS.CNMT_DT,list.get(0).getCnmtDt());
				cnmtMap.put(CnmtCNTS.CUST_CODE,list.get(0).getCustCode());
				cnmtMap.put(CnmtCNTS.CNMT_CONSIGNOR,list.get(0).getCnmtConsignor());
				cnmtMap.put(CnmtCNTS.CNMT_CONSIGNEE,list.get(0).getCnmtConsignee());
				cnmtMap.put(CnmtCNTS.CNMT_NO_OF_PKG,list.get(0).getCnmtNoOfPkg());
				
				System.out.println("value of actual wt = "+list.get(0).getCnmtActualWt());
				
				cnmtMap.put(CnmtCNTS.CNMT_ACTUAL_WT,list.get(0).getCnmtActualWt());
				cnmtMap.put(CnmtCNTS.CNMT_PAY_AT,list.get(0).getCnmtPayAt());
				cnmtMap.put(CnmtCNTS.CNMT_BILL_AT,list.get(0).getCnmtBillAt());
				cnmtMap.put(CnmtCNTS.CNMT_FREIGHT,list.get(0).getCnmtFreight());
				cnmtMap.put(CnmtCNTS.CNMT_VOG,list.get(0).getCnmtVOG());
				cnmtMap.put(CnmtCNTS.CNMT_EXTRA_EXP,list.get(0).getCnmtExtraExp());
				cnmtMap.put(CnmtCNTS.CNMT_DT_OF_DLY,list.get(0).getCnmtDtOfDly());
				cnmtMap.put(CnmtCNTS.CNMT_EMP_CODE,list.get(0).getCnmtEmpCode());
				cnmtMap.put(CnmtCNTS.CNMT_KM,list.get(0).getCnmtKm());
				cnmtMap.put(CnmtCNTS.CNMT_STATE,list.get(0).getCnmtState());
				cnmtMap.put(CnmtCNTS.CONTRACT_CODE,list.get(0).getContractCode());
				cnmtMap.put(CnmtCNTS.CNMT_INVOICE_NO,list.get(0).getCnmtInvoiceNo());
				cnmtMap.put(CnmtCNTS.CREATION_TS,list.get(0).getCreationTS());
				cnmtMap.put(CnmtCNTS.USER_BRANCH_CODE,list.get(0).getbCode());
				
				System.out.println("value of guarantee wt = "+list.get(0).getCnmtGuaranteeWt());
				
				cnmtMap.put(CnmtCNTS.CNMT_GUARANTEE_WT,list.get(0).getCnmtGuaranteeWt());
				cnmtMap.put(CnmtCNTS.IS_VIEW,list.get(0).isView());
				cnmtMap.put(CnmtCNTS.CNMT_TOT,list.get(0).getCnmtTOT());
				cnmtMap.put(CnmtCNTS.CNMT_VEHICLE_TYPE,list.get(0).getCnmtVehicleType());
				cnmtMap.put(CnmtCNTS.CNMT_FrSt,list.get(0).getCnmtFromSt());
				cnmtMap.put(CnmtCNTS.CNMT_TSt,list.get(0).getCnmtToSt());
				cnmtMap.put(CnmtCNTS.CNMT_PROD_TYP,list.get(0).getCnmtProductType());
				cnmtMap.put(CnmtCNTS.CNMT_RATE,list.get(0).getCnmtRate());
				cnmtMap.put(CnmtCNTS.CNMT_BILL_NO,list.get(0).getCnmtBillNo());
			/*	Blob blob = list.get(0).getCnmtImage();
				Blob confBlob = list.get(0).getCnmtConfirmImage();
				if(blob != null){
					InputStream in = blob.getBinaryStream();
					int blobLength = (int) blob.length();  
					byte[] blobAsBytes = blob.getBytes(1, blobLength);
					cnmtMap.put("cnmtImage",blobAsBytes);
				}
				
				if(confBlob != null){
					InputStream in = confBlob.getBinaryStream();
					int blobLength = (int) confBlob.length();  
					byte[] blobAsBytes = confBlob.getBytes(1, blobLength);
					cnmtMap.put("cnmtConfirmImage",blobAsBytes);
				}*/
				
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("cnmt", cnmtMap);
				map.put("chlnNSedrByCnmtList", chlnNSedrByCnmtList);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;		 			
		}
		  
		  
		@RequestMapping(value="/getDailyContractDataForView",method = RequestMethod.POST)
		public @ResponseBody Object getDailyContractDataForView(@RequestBody String contractCode){
				
			System.out.println("--------->>>>>>"+contractCode);
			List<DailyContract> dailyContractList = dailyContractDAO.getDailyContractData(contractCode);
			Map<String, Object> map = new HashMap<String, Object>();
			if (!dailyContractList.isEmpty()) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("contract",dailyContractList.get(0));
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}
		  
		  
		@RequestMapping(value="/getRegularContractDataForView",method = RequestMethod.POST)
		public @ResponseBody Object getRegularContractDataForView(@RequestBody String contractCode){
				
			System.out.println("--------->>>>>>"+contractCode);
			List<RegularContract>  regularContractList = regularContractDAO.getRegContractData(contractCode);
			Map<String, Object> map = new HashMap<String, Object>();
			if (!regularContractList.isEmpty()) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("contract",regularContractList.get(0));
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}
		  
		  
		@RequestMapping(value="/addRateForCnmtByKForView",method = RequestMethod.POST)
		public @ResponseBody Object addRateForCnmtByKForView(@RequestBody Map<String,Object> clientMap){
			System.out.println("enter into addRateForCnmtByKForView function");
			Map<String,Object> map = new HashMap<String,Object>();
			String contCode = (String) clientMap.get("contractCode");
			System.out.println("from client------>"+clientMap.get("cnmtKm"));
			String checkCode =  contCode.substring(0,3);
			int kmValue = (int) clientMap.get("cnmtKm");
			double cnmtKm = (double) kmValue;
			String stateCode = (String) clientMap.get("cnmtState");
			if(checkCode.equalsIgnoreCase("dly")){
				List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
				DailyContract dailyContract = dailyContList.get(0);
				//	double rate = rateByKmDAO.getRbkmRate(stateCode,cnmtKm,contCode);
				List<RateByKm> rbkmList = rateByKmDAO.getRbkmRate(stateCode,cnmtKm,contCode);
				double rate = 0.0;
				String vehTypeCode = "";
				if(!rbkmList.isEmpty()){
					for(int i=0;i<rbkmList.size();i++){
						if(rbkmList.get(i).getRbkmFromKm() <= cnmtKm && rbkmList.get(i).getRbkmToKm() >= cnmtKm){
							rate = rbkmList.get(i).getRbkmRate();
							vehTypeCode = rbkmList.get(i).getRbkmVehicleType();
						}
					}
				}
				double actualRate = cnmtKm * rate;
				System.out.println("**************************------rate = "+actualRate);
				map.put("rate",actualRate);
				map.put("vtCode",vehTypeCode);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					
			}else if(checkCode.equalsIgnoreCase("reg")){
				List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
				RegularContract regularContract = regConList.get(0);
				List<RateByKm> rbkmList = rateByKmDAO.getRbkmRate(stateCode,cnmtKm,contCode);
				double rate = 0.0;
				String vehTypeCode = "";
				if(!rbkmList.isEmpty()){
					for(int i=0;i<rbkmList.size();i++){
						if(rbkmList.get(i).getRbkmFromKm() <= cnmtKm && rbkmList.get(i).getRbkmToKm() >= cnmtKm){
							rate = rbkmList.get(i).getRbkmRate();
							vehTypeCode = rbkmList.get(i).getRbkmVehicleType();
						}
					}
				}
				double actualRate = cnmtKm * rate;
				map.put("rate",actualRate);
				map.put("vtCode",vehTypeCode);
				System.out.println("*************************------rate = "+actualRate);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);			
			}else{
				System.out.println("invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			 
			return map;	
		}
		  
		  
		  
		/*@RequestMapping(value="/addRateForCnmtByQForView",method = RequestMethod.POST)
		public @ResponseBody Object addRateForCnmtByQForView(@RequestBody Map<String,Object> clientMap){
			System.out.println("enter into addRateForCnmtByQForView function");
			Map<String,Object> map = new HashMap<String,Object>();
			String contCode = (String) clientMap.get("contractCode");
			String checkCode =  contCode.substring(0,3);
			String stnCode = (String) clientMap.get("cnmtToSt");
			String productType = (String) clientMap.get("cnmtProductType");
			if(checkCode.equalsIgnoreCase("dly")){
				List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
				DailyContract dailyContract = dailyContList.get(0);
				List<ContToStn> ContToStnList  = contToStnDAO.getContToStnRateQ(contCode, stnCode, productType);
				double rate = 0.0;
				double garunteeWt = 0.0;
				if(!ContToStnList.isEmpty()){
					for(int i=0;i<ContToStnList.size();i++){
				  				rate = ContToStnList.get(i).getCtsRate();
				  				garunteeWt = ContToStnList.get(i).getCtsToWt();
				  								}
					rate = ContToStnList.get(0).getCtsRate();
					garunteeWt = ContToStnList.get(0).getCtsToWt();

				}	
				double actualRate = rate;
				map.put("rate",actualRate);
				map.put("garunteeWt",garunteeWt);
				System.out.println("###########------rate = "+rate);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else if(checkCode.equalsIgnoreCase("reg")){
				List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
				RegularContract regularContract = regConList.get(0);
				List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contCode, stnCode, productType);
				double rate = 0.0;
				double garunteeWt = 0.0;
				if(!ContToStnList.isEmpty()){
					for(int i=0;i<ContToStnList.size();i++){
						  	rate = ContToStnList.get(i).getCtsRate();	
						  garunteeWt = ContToStnList.get(i).getCtsToWt();
					  }
					  
					rate = ContToStnList.get(0).getCtsRate();	
					garunteeWt = ContToStnList.get(0).getCtsToWt();
				}		 	
				double actualRate = rate;
				map.put("rate",actualRate);
				map.put("garunteeWt",garunteeWt);
				System.out.println("###########------rate = "+rate);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							
			}else{
				System.out.println("invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
				
			return map;	
		}*/
		  
		
		@RequestMapping(value="/addRateForCnmtByWForView",method = RequestMethod.POST)
		public @ResponseBody Object addRateForCnmtByWForView(@RequestBody Map<String,Object> clientMap){
			System.out.println("enter into addRateForCnmtByWForView function");
			Map<String,Object> map = new HashMap<String,Object>();
			String contCode = (String) clientMap.get("contractCode");
			System.out.println("from client contCode = "+contCode);
			String checkCode =  contCode.substring(0,3);
			String stnCode = (String) clientMap.get("cnmtToSt");
			System.out.println("from client stnCode = "+stnCode);
			String vehicleType = (String) clientMap.get("cnmtVehicleType");
			System.out.println("from client vehicleType = "+vehicleType);
			if(checkCode.equalsIgnoreCase("dly")){
				List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
				DailyContract dailyContract = dailyContList.get(0);
				List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contCode, stnCode, vehicleType);
				System.out.println("yoooooooooooooooooooooo="+ContToStnList.size());
				double rate = 0.0;
				double garWt = 0.0;
				if(!ContToStnList.isEmpty()){
						/*for(int i=0;i<ContToStnList.size();i++){
				  					rate = ContToStnList.get(i).getCtsRate();
				  					garWt = ContToStnList.get(i).getCtsToWt();
				  			}*/
					rate = ContToStnList.get(0).getCtsRate();
					garWt = ContToStnList.get(0).getCtsToWt();
				}	
				double actualRate = rate;
				map.put("rate",actualRate);
				map.put("garWt",garWt);
				System.out.println("$$$$$$$$$$$$$$------rate = "+rate);
				System.out.println("$$$$$$$$$$$$$$------garWt = "+garWt);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else if(checkCode.equalsIgnoreCase("reg")){
				List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
				RegularContract regularContract = regConList.get(0);
				List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contCode, stnCode, vehicleType);
				System.out.println("kyaaaaaaaaa hhhhhhhh--->"+ContToStnList.size());
				double rate = 0.0;
				double garWt = 0.0;
				if(!ContToStnList.isEmpty()){
					 /* for(int i=0;i<ContToStnList.size();i++){
						  rate = ContToStnList.get(i).getCtsRate();
						  garWt = ContToStnList.get(i).getCtsToWt();
					  }*/
					rate = ContToStnList.get(0).getCtsRate();
					garWt = ContToStnList.get(0).getCtsToWt();
				}	 	
				double actualRate = rate;
				map.put("rate",actualRate);
				map.put("garWt",garWt);
				System.out.println("$$$$$$$$$$$$$------rate = "+rate);
				System.out.println("$$$$$$$$$$$$$$------garWt = "+garWt);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					
			}else{
				System.out.println("invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
				 	
			return map;	
		}
		  
		
		
		@RequestMapping(value = "/uploadConfCnmtImageV", method = RequestMethod.POST)
		public @ResponseBody Object uploadConfCnmtImage(@RequestParam("file") MultipartFile file)throws Exception {
			System.out.println("enter into uploadConfCnmtImage function ---");
			byte [] byteArr = file.getBytes();
			Map<String,Object> map = new HashMap<String, Object>();
			Blob blob = null;
			try{
				//blob = Hibernate.createBlob(fileContent);
				modelService.setConfirmBlob(null);
				blob = new SerialBlob(byteArr);
				System.out.println("blob ==="+blob.length());
				modelService.setConfirmBlob(blob); 
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}catch(Exception e){
				e.printStackTrace();
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;
		}
		
		
		@RequestMapping(value = "/uploadCnmtImageV", method = RequestMethod.POST)
		public @ResponseBody Object uploadCnmtImage(@RequestParam("file") MultipartFile file)throws Exception {

			System.out.println("enter into uploadCnmtImage function ---");

			byte [] byteArr = file.getBytes();
			Map<String,Object> map = new HashMap<String, Object>();
			Blob blob = null;
			try{
				//blob = Hibernate.createBlob(fileContent);
				modelService.setBlob(null); 
				blob = new SerialBlob(byteArr);
				System.out.println("blob ==="+blob.length());
				modelService.setBlob(blob); 
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}catch(Exception e){
				e.printStackTrace();
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;
		}
		
		@RequestMapping(value = "/editCnmt", method = RequestMethod.POST)
		public @ResponseBody Object editCnmt(@RequestBody Cnmt cnmt) throws Exception{
			System.out.println("Entered into editCnmt of controller----");			
			Map<String,String> map = new HashMap<String, String>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			
			Blob blob = modelService.getBlob();
			//Blob confBlob = modelService.getConfirmBlob();
			
			if(blob != null){
				int cmId = cnmt.getCmId();
				if(cmId > 0){
					CnmtImage cnmtImage = cnmtDAO.getCnmtImg(cmId);
					if(cnmtImage != null){					
						//if(confBlob != null)
							//cnmtImage.setCnmtConfirmImage(confBlob);

						cnmtDAO.updateCnImg(cnmtImage, blob);
					}				
				}else{
					//if(blob != null || confBlob != null){
					if(blob != null){
						CnmtImage cnmtImage = new CnmtImage();
						cnmtImage.setCnmtId(cnmt.getCnmtId());
						cnmtImage.setUserCode(currentUser.getUserCode());
						cnmtImage.setbCode(currentUser.getUserBranchCode());					
						//if(confBlob != null)
							//cnmtImage.setCnmtConfirmImage(confBlob);
						
						
						int cnImgId = cnmtDAO.saveCnImg(cnmtImage, blob);
						if(cnImgId > 0){
							cnmt.setCmId(cnImgId);
						}
					}
				}
			}
		
			int temp=cnmtDAO.updateCnmts(cnmt);
			if (temp>=0) {
				modelService.setBlob(null);
				modelService.setConfirmBlob(null);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			System.out.println("Code reached here---------------->");

			return map;
		}
		  
		  
		@RequestMapping(value="/getAllContForViewCnmt",method = RequestMethod.POST)
		public @ResponseBody Object getAllContForViewCnmt(@RequestBody String code){

			System.out.println("--------->>>>>>"+code);
			/*String code = (String) clientMap.get("custCode");
				System.out.println("--------->>>>>>"+code);*/
			Map<String,List<Map<String, Object>>> finalMap = new HashMap<String,List<Map<String, Object>>>();
			List<Map<String, Object>> regDlyContList = new ArrayList<Map<String,Object>>();

			List<RegularContract>  regContList = regularContractDAO.getRegContractCode(code);
			List<DailyContract> dlyContList = dailyContractDAO.getDailyContractCode(code);

			if (!regContList.isEmpty()) {
				System.out.println("size of regContList = "+regContList.size());
				for (int i = 0; i < regContList.size(); i++) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("custCode",regContList.get(i).getRegContBLPMCode());
					map.put("contCode", regContList.get(i).getRegContCode());
					map.put("fromStation", regContList.get(i).getRegContFromStation());
					map.put("toStation", regContList.get(i).getRegContToStation());
					map.put("fromDate", regContList.get(i).getRegContFromDt());
					map.put("toDate", regContList.get(i).getRegContToDt());
					map.put("rate", regContList.get(i).getRegContRate());
					map.put("cnmtDC", regContList.get(i).getRegContDc());
					map.put("cnmtDDL", regContList.get(i).getRegContDdl());
					map.put("cnmtCostGrade", regContList.get(i).getRegContCostGrade());
					map.put("contType",regContList.get(i).getRegContType());
					map.put("proportionate",regContList.get(i).getRegContProportionate());
					//map.put("additionalRate",regContList.get(i).getRegContAdditionalRate());
					regDlyContList.add(map);
				}
			}

			if (!dlyContList.isEmpty()) {
				System.out.println("size of dlyContList = "+dlyContList.size());
				for (int i = 0; i < dlyContList.size(); i++) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("custCode",dlyContList.get(i).getDlyContBLPMCode());
					map.put("contCode", dlyContList.get(i).getDlyContCode());
					map.put("fromStation", dlyContList.get(i).getDlyContFromStation());
					map.put("toStation", dlyContList.get(i).getDlyContToStation());
					map.put("fromDate", dlyContList.get(i).getDlyContStartDt());
					map.put("toDate", dlyContList.get(i).getDlyContEndDt());
					map.put("rate", dlyContList.get(i).getDlyContRate());
					map.put("cnmtDC", dlyContList.get(i).getDlyContDc());
					map.put("cnmtDDL", dlyContList.get(i).getDlyContDdl());
					map.put("cnmtCostGrade", dlyContList.get(i).getDlyContCostGrade());
					map.put("contType",dlyContList.get(i).getDlyContType());
					map.put("proportionate",dlyContList.get(i).getDlyContProportionate());
					//map.put("additionalRate",dlyContList.get(i).getDlyContAdditionalRate());
					regDlyContList.add(map);
				}
			}

			finalMap.put("result",regDlyContList);
			return finalMap;
		}
		  
		  
		@RequestMapping(value="/getContForView",method = RequestMethod.POST)
		public @ResponseBody Object getContForView(@RequestBody Map<String,Object> clientMap){
			System.out.println("enter into getContForView function");
			Map<String,Object> map = new HashMap<String,Object>();
			String contCode = (String) clientMap.get("contractCode");
			System.out.println("---------------"+contCode);
			String checkCode =  contCode.substring(0,3);
			String stnCode = (String) clientMap.get("cnmtToSt");
			System.out.println("--------------->>>"+stnCode);
			if(checkCode.equalsIgnoreCase("dly")){
				List<DailyContract> dailyContList = dailyContractDAO.getDailyContractData(contCode);
				DailyContract dailyContract = dailyContList.get(0);
				if(dailyContract.getDlyContType().equals("K")|| dailyContract.getDlyContType().equals("k")){
					String station = dailyContract.getDlyContToStation();
					if(stnCode.equalsIgnoreCase(station)){
						System.out.println("%%%%%%%%%%%%%%%to station present1");
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						System.out.println("#################to station absent2");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else if(dailyContract.getDlyContType().equals("q")|| dailyContract.getDlyContType().equals("Q")|| 
						dailyContract.getDlyContType().equals("w")|| dailyContract.getDlyContType().equals("W")){
					List<ContToStn> ContToStnList  = contToStnDAO.getContForView(contCode, stnCode);
					if(!ContToStnList.isEmpty()){
						System.out.println("%%%%%%%%%%%%%%%to station present3");
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						System.out.println("#################to station absent4");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}
				return map;
				/*map.put("station",station);
				  		System.out.println("###########------station = "+station);
				  		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);*/
			}else if(checkCode.equalsIgnoreCase("reg")){
				List<RegularContract> regConList = regularContractDAO.getRegContractData(contCode);
				RegularContract regularContract = regConList.get(0);
				if(regularContract.getRegContType().equals("K")|| regularContract.getRegContType().equals("k")){
					String station = regularContract.getRegContToStation();
					if(stnCode.equalsIgnoreCase(station)){
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						System.out.println("%%%%%%%%%%%%%%%to station present5");
					}else{
						System.out.println("#################to station absent6");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else if(regularContract.getRegContType().equals("q")|| regularContract.getRegContType().equals("Q")|| 
						regularContract.getRegContType().equals("w")|| regularContract.getRegContType().equals("W")){
					System.out.println("----------(((-----"+contCode);
					System.out.println("---------)))------>>>"+stnCode);
					List<ContToStn> ContToStnList = contToStnDAO.getContForView(contCode, stnCode);
					if(!ContToStnList.isEmpty()){
						System.out.println("%%%%%%%%%%%%%%%to station present7");
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						System.out.println("#################to station absent8");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}	
				/*map.put("station",station);
			  		System.out.println("###########------station = "+station);
					 	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);*/
				return map;
			}else{
				System.out.println("invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				return map;
			}

			/*return map;*/	
		}

			
 }
		  


