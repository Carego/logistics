'use strict';

var app = angular.module('application');

app.controller('PanValidCntlr',['$scope','$location','$http','$window','FileUploadService','$filter', '$sce',
                            function($scope,$location,$http,$window,FileUploadService,$filter,$sce){
	
	console.log("PanValidCntlr started");
	
	$scope.branchCode = "";
	$scope.branchFaCode = "";
	$scope.dateFrom = "";
	$scope.dateTo = "";
	
	$scope.chlnDetPanValidList = [];//list from server	
	$scope.notValidPanDecList = [];//list maintained at server
	$scope.panInvalidList = [];//list maintained at server
	/*$scope.decInvalidList = [];*///list maintained at server
	
	$scope.notValid = false;
	$scope.invalidPan = false;
	$scope.invalidDec = false;
	
	$scope.lodingFlag = false;
	
	$scope.BranchCodeDBFlag=true;

	$scope.getNotValidList = function() {
		console.log("getNotValidList() called");
		$scope.notValid = true;
		$scope.invalidPan = false;
		$scope.invalidDec = false;
		
		$scope.notValidPanDecList = [];
		$scope.panInvalidList = [];		
		for ( var i = 0; i < $scope.chlnDetPanValidList.length; i++) {			
			if ($scope.chlnDetPanValidList[i].isValidPan === false && $scope.chlnDetPanValidList[i].isInvalidPan === false) {
				$scope.notValidPanDecList.push($scope.chlnDetPanValidList[i]);
			}
		}		
		console.log("chlnDetPanValidList size==>> "+$scope.chlnDetPanValidList.length);
		console.log("notValidPanDecList size==>> "+$scope.notValidPanDecList.length);
	}
	
	$scope.getInvalidPanList = function() {
		console.log("getInvalidPanList() called");
		$scope.notValid = false;
		$scope.invalidPan = true;
		$scope.invalidDec = false;
		
		$scope.notValidPanDecList = [];
		$scope.panInvalidList = [];	
		for ( var i = 0; i < $scope.chlnDetPanValidList.length; i++) {			
			if ($scope.chlnDetPanValidList[i].isInvalidPan === true) {
				$scope.panInvalidList.push($scope.chlnDetPanValidList[i]);
			}
		}
		
		console.log("chlnDetPanValidList size==>> "+$scope.chlnDetPanValidList.length);
		console.log("panInvalidList size==>> "+$scope.panInvalidList.length);
	}
	
	/*$scope.getInvalidDecList = function() {
		console.log("getInvalidDecList() called");
		$scope.notValid = false;
		$scope.invalidPan = false;
		$scope.invalidDec = true;
		
		$scope.notValidPanDecList = [];
		$scope.panInvalidList = [];
		$scope.decInvalidList = [];
		for ( var i = 0; i < $scope.chlnDetPanValidList.length; i++) {
			if (($scope.chlnDetPanValidList[i].isInvalidDec === true) ||
					($scope.chlnDetPanValidList[i].isValidDec === false && $scope.chlnDetPanValidList[i].isInvalidDec === false && $scope.chlnDetPanValidList[i].isValidPan === true && $scope.chlnDetPanValidList[i].isInvalidPan === false) ||
					($scope.chlnDetPanValidList[i].isValidDec === false && $scope.chlnDetPanValidList[i].isInvalidDec === false && $scope.chlnDetPanValidList[i].isValidPan === false && $scope.chlnDetPanValidList[i].isInvalidPan === true)) {
				$scope.decInvalidList.push($scope.chlnDetPanValidList[i]);
			}
		}
		
		console.log("chlnDetPanValidList size==>> "+$scope.chlnDetPanValidList.length);
		console.log("decInvalidList size==>> "+$scope.decInvalidList.length);
	}*/
	
	//validate PanDec
	$scope.validatePanDec = function(notValidPanDec, index) {
		console.log("isValidPan: "+notValidPanDec.isValidPan)
		console.log("isInvalidPan: "+notValidPanDec.isInvalidPan)
		console.log("isValidDec: "+notValidPanDec.isValidDec)
		console.log("isInvalidDec: "+notValidPanDec.isInvalidDec)
		
		/*if (notValidPanDec.isValidPan === true || notValidPanDec.isInvalidPan === true || notValidPanDec.isValidDec === true || notValidPanDec.isInvalidDec === true) {*/
		
		if (/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/.test(notValidPanDec.panNo)) {
			if (notValidPanDec.isValidPan === true || notValidPanDec.isInvalidPan === true) {
				
				if (notValidPanDec.isPanImg === false) {
					notValidPanDec.isValidPan = false;
					notValidPanDec.isInvalidPan = true;
				}
				
				//remove from list notValidPanDecList
				for ( var i = 0; i < $scope.notValidPanDecList.length; i++) {
					if ($scope.notValidPanDecList[i].code === notValidPanDec.code) {
						$scope.notValidPanDecList.splice(i, 1);
						//$scope.alertToast($scope.notValidPanDecList[i].code);
						i=i-1;
						
					}
				}
				
				//update list chlnDetPanValidList
				for ( var i = 0; i < $scope.chlnDetPanValidList.length; i++) {
					if ($scope.chlnDetPanValidList[i].code === notValidPanDec.code) {
						$scope.chlnDetPanValidList[i].isValidPan = notValidPanDec.isValidPan;
						$scope.chlnDetPanValidList[i].isInvalidPan = notValidPanDec.isInvalidPan;
						/*$scope.chlnDetPanValidList[i].isValidDec = notValidPanDec.isValidDec;
						$scope.chlnDetPanValidList[i].isInvalidDec = notValidPanDec.isInvalidDec;*/
					}
				}
				$scope.alertToast("Update Successfully");
				
				//call to server
				$scope.panValidated(notValidPanDec, index);
				
			}else {
				$scope.alertToast("Please select valid option");
			}
		}else {
			$scope.alertToast("Please select valid Pan No");
		}
	}
	
	//Validate Pan
	$scope.validatePan = function(panInvalid, index) {
		console.log("isValidPan: "+panInvalid.isValidPan);
		console.log("isInvalidPan: "+panInvalid.isInvalidPan);
		console.log("isValidDec: "+panInvalid.isValidDec);
		console.log("isInvalidDec: "+panInvalid.isInvalidDec);
		console.log("panNo: "+panInvalid.panNo);
		
		if (/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/.test(panInvalid.panNo)) {
			if (panInvalid.isValidPan === true) {
				
				if (panInvalid.isPanImg === false) {
					panInvalid.isValidPan = false;
					panInvalid.isInvalidPan = true;
				}
				
				//remove from list notValidPanDecList
				for ( var i = 0; i < $scope.panInvalidList.length; i++) {
					if ($scope.panInvalidList[i].code === panInvalid.code) {
						$scope.panInvalidList.splice(i, 1);
						//$scope.alertToast($scope.panInvalidList[i].code);
						i=i-1;
						
					}
				}
				
				//update list chlnDetPanValidList
				for ( var i = 0; i < $scope.chlnDetPanValidList.length; i++) {
					if ($scope.chlnDetPanValidList[i].code === panInvalid.code) {
						$scope.chlnDetPanValidList[i].isValidPan = panInvalid.isValidPan;
					}
				}
				$scope.alertToast("Update Successfully");
				
				//call to server
				$scope.panValidated(panInvalid, index);
				
			}else {
				$scope.alertToast("Please select valid option");
			}
		} else {
			$scope.alertToast("Please select valid Pan No");
		}
	}
	
	$scope.panValidated = function(panInvalid, index) {
		console.log("isValidPan: "+panInvalid.isValidPan);
		console.log("isInvalidPan: "+panInvalid.isInvalidPan);
		var response = $http.post($scope.projectName+'/panValidated', panInvalid);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.alertToast(data.result);
			}else {
				$scope.alertToast(data.result);
				console.log("panValidated Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("panValidated Error: "+data);
		});
	}
	
	//Validate dec
	/*$scope.validateDec = function(decInvalid, index) {
		console.log("isValidPan: "+decInvalid.isValidPan)
		console.log("isInvalidPan: "+decInvalid.isInvalidPan)
		console.log("isValidDec: "+decInvalid.isValidDec)
		console.log("isInvalidDec: "+decInvalid.isInvalidDec)
		if (decInvalid.isValidDec === true) {
			//remove from list notValidPanDecList
			for ( var i = 0; i < $scope.decInvalidList.length; i++) {
				if ($scope.decInvalidList[i].code === decInvalid.code) {
					$scope.decInvalidList.splice(i, 1);
					$scope.alertToast($scope.decInvalidList[i].code);
					i=i-1;
					
				}
			}
			//update list chlnDetPanValidList
			for ( var i = 0; i < $scope.chlnDetPanValidList.length; i++) {
				if ($scope.chlnDetPanValidList[i].code === notValidPanDec.code) {
					$scope.chlnDetPanValidList[i].isValidDec = notValidPanDec.isValidDec;
				}
			}
			$scope.alertToast("Update Successfully");
		}else {
			$scope.alertToast("Please select valid option");
		}
	}*/
	
	//the following four methods will work for all three conditions
	$scope.isValidPanClick = function(notValidPanDec, index){
		notValidPanDec.isValidPan = true;
		notValidPanDec.isInvalidPan = false;
	}
	
	$scope.isInvalidPanClick = function(notValidPanDec, index){
		notValidPanDec.isValidPan = false;
		notValidPanDec.isInvalidPan = true;
	}
	
	$scope.isValidDecClick = function(notValidPanDec, index){
		notValidPanDec.isValidDec = true;
		notValidPanDec.isInvalidDec = false;
	}
	
	$scope.isInvalidDecClick = function(notValidPanDec, index){
		notValidPanDec.isValidDec = false;
		notValidPanDec.isInvalidDec = true;
	}
	
	$scope.editPan = function(index) {
		$('#panNoId'+index).removeAttr("disabled");
	}
	
	$scope.editPanInvalid = function(index) {
		$('#panInvalidNoId'+index).removeAttr("disabled");
	}
	
	$scope.showImage = function(notValidPanDec, index){
		console.log("enter into show image: ")
		var imgService = {
			"imgId"		: notValidPanDec.imgId,
			"panHolder"	: notValidPanDec.panHolder
		}
		
		var response = $http.post($scope.projectName+'/getImgForValidation', imgService);
		response.success(function(data, status, headers, config){
			
			if(data.result === "success"){
				$scope.alertToast(data.result);
				var form = document.createElement("form");
		        form.method = "POST";
		        form.action = $scope.projectName + '/downloadPANImg';
		        form.target = "_blank";
		        document.body.appendChild(form);
		        form.submit();
			}else
				$scope.alertToast(data.msg);
			/*console.log(data.panImage);
			window.open("data:application/pdf;base64, "+data.panImage);*/
	    });
		response.error(function(data, status, headers, config){
			console.log("getImgForValidation Error: "+data);
		});
		
	}
	
	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}
	
	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
				$scope.getListOfEmployee();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.saveBranchCode = function(branch){
		$scope.branchCode = branch.branchCode;		
		$scope.branchFaCode = branch.branchFaCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}
	
	$scope.submitPanValidForm = function(PanValidForm){
		console.log("Enter into submitPanValidForm() ");
		console.log("Form Validation : Valid = "+PanValidForm.$valid);
		if(PanValidForm.$invalid){
			if(PanValidForm.branchFaCode.$invalid){
				$scope.alertToast("Please select Branch !");
				return;
			}else if(PanValidForm.dateFrom.$invalid){
				$scope.alertToast("Please select date from !");
				return;
			}else{
				$scope.alertToast("Please fill required information !");
				return;
			}
		}
		
		$scope.chlnDetPanValidList = [];//list from server	
		$scope.notValidPanDecList = [];//list maintained at server
		$scope.panInvalidList = [];//list maintained at server
		
		var data = {
				"branchCode" 	:	$scope.branchCode,
				"dateFrom"		:	$scope.dateFrom,
				"dateTo"		:	$scope.dateTo,
				"panHldr"		:	$scope.panHldr
		}		
		$scope.lodingFlag = true;
		console.log("Going to hit /getChlnForPanValid ");
		var response = $http.post($scope.projectName+'/getChlnForPanValid', data);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				$scope.lodingFlag = false;
				console.log("getChlnForPanValid Success");				
				$scope.chlnDetPanValidList = data.chlnDetPanValidList;						
				$scope.alertToast("Success");
			}else {
				$scope.lodingFlag = false;
				$scope.alertToast(data.msg);
				console.log(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.lodingFlag = false;
			console.log("getActiveBrNCI Error: "+data);
		});
		
	} 
	
	
	$scope.downInvldPan = function(){
		console.log("enter into downloadAnx function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "InvalidPan.xls");
	};
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){		
		$scope.getBranchData();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	console.log("PanValidCntlr ended");

}]);