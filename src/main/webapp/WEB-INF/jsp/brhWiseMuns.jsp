<div ng-show="superAdminLogin">

	<div class="row">
		<form name="BrhMunsForm" class="col s12 card"
			style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
			ng-submit="submitMuns(BrhMunsForm)">

			<div class="row">

				<div class="input-field col s3">
					<input type="text" name="brhName" id="brhId"
						ng-model="branchName" ng-click="openBranchDB()" readonly>
						 <label>Select Branch</label>
				</div>
				
				<div class="input-field col s3">
		       		<input class="validate" type ="number" id="munsId" name ="munsName" ng-model="brhWiseMuns.bwmMuns" ng-required="true">
		       			<label for="code">Munsiana</label>	
		       	</div>
		       	
		       	<div class="input-field col s3">
		       		<input class="validate" type ="number" id="tdsId" name ="tdsName" ng-model="brhWiseMuns.bwmCsDis" step="0.01" min="0.00" ng-required="true">
		       			<label for="code">Cash Discount%</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		    		<select name="actName" id="actId" ng-model="active" ng-init="active = 'false'" required>
						<option value="true">YES</option>
						<option value="false">No</option>
					</select>
					<label>Active</label>
				</div>
				
			</div>
			
			
			<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" id="saveId" value="Submit">
				</div>
			</div>
			
		</form>
		
	</div>	

	<div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
		 <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	 
 	  	  	  <th> Branch Name </th>
 	  	  	  
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>


</div>