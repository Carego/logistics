package com.mylogistics.model.webservice;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_app")
public class VehicleApp {
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vId", length = 15)
	private Integer vId;
	@Column(name = "vOwnCode", length = 15)
	private String vOwnCode;
	@Column(name = "vOwnName", length = 40)
	private String vOwnName;
	@Column(name = "vBrkCode", length = 15)
	private String vBrkCode;
	@Column(name = "vBrkName", length = 40)
	private String vBrkName;
	@Column(name = "vTypeCode", length = 15)
	private String vTypeCode;
	@Column(name = "vType", length = 40)
	private String vType;
	@Column(name = "vRcNo", length = 40, unique = true)
	private String vRcNo;
	@Column(name = "vRcIssueDt")
	private Date vRcIssueDt;
	@Column(name = "vRcValidDt")
	private Date vRcValidDt;
	@Column(name = "vDriverName", length = 40)
	private String vDriverName;
	@Column(name = "vDriverDlNo", length = 40)
	private String vDriverDlNo;
	@Column(name = "vDriverDlIssueDt")
	private Date vDriverDlIssueDt;
	@Column(name = "vDriverDlValidDt")
	private Date vDriverDlValidDt;
	@Column(name = "vDriverMobNo", length = 13)
	private String vDriverMobNo;
	@Column(name = "vPermitNo", length = 40)
	private String vPermitNo;
	@Column(name = "vPermitIssueDt")
	private Date vPermitIssueDt;
	@Column(name = "vPermitValidDt")
	private Date vPermitValidDt;
	@Column(name = "vPermitStateCode", length = 10)
	private String vPermitStateCode;
	@Column(name = "vPermitStateName", length = 40)
	private String vPermitStateName;
	@Column(name = "vFitDocNo", length = 40)
	private String vFitDocNo;
	@Column(name = "vFitDocIssueDt")
	private Date vFitDocIssueDt;
	@Column(name = "vFitDocValidDt")
	private Date vFitDocValidDt;
	@Column(name = "vFitDocStateCode", length = 10)
	private String vFitDocStateCode;
	@Column(name = "vFitDocStateName", length = 40)
	private String vFitDocStateName;
	@Column(name = "vFinanaceBankName", length = 40)
	private String vFinanceBankName;
	@Column(name = "vPolicyNo", length = 40)
	private String vPolicyNo;
	@Column(name = "vPolicyCompany", length = 40)
	private String vPolicyCompany;
	@Column(name = "vTransitPassNo", length = 40)
	private String vTransitPassNo;
	@Column(name = "isPending")
	private Boolean isPending;
	@Column(name = "isCancel")
	private Boolean isCancel;
	@Column(name = "userCode")
	private String userCode;
	@Column(name = "branchCode")
	private String branchCode;
	@Column(name = "creationTS", insertable = false, updatable = true)
	private Date creationTS;
	
	public Integer getvId() {
		return vId;
	}
	public void setvId(Integer vId) {
		this.vId = vId;
	}
	public String getvOwnCode() {
		return vOwnCode;
	}
	public void setvOwnCode(String vOwnCode) {
		this.vOwnCode = vOwnCode;
	}
	public String getvOwnName() {
		return vOwnName;
	}
	public void setvOwnName(String vOwnName) {
		this.vOwnName = vOwnName;
	}
	public String getvBrkCode() {
		return vBrkCode;
	}
	public void setvBrkCode(String vBrkCode) {
		this.vBrkCode = vBrkCode;
	}
	public String getvBrkName() {
		return vBrkName;
	}
	public void setvBrkName(String vBrkName) {
		this.vBrkName = vBrkName;
	}
	public String getvTypeCode() {
		return vTypeCode;
	}
	public void setvTypeCode(String vTypeCode) {
		this.vTypeCode = vTypeCode;
	}
	public String getvType() {
		return vType;
	}
	public void setvType(String vType) {
		this.vType = vType;
	}
	public Date getvRcIssueDt() {
		return vRcIssueDt;
	}
	public void setvRcIssueDt(Date vRcIssueDt) {
		this.vRcIssueDt = vRcIssueDt;
	}
	public Date getvRcValidDt() {
		return vRcValidDt;
	}
	public void setvRcValidDt(Date vRcValidDt) {
		this.vRcValidDt = vRcValidDt;
	}
	public String getvDriverName() {
		return vDriverName;
	}
	public void setvDriverName(String vDriverName) {
		this.vDriverName = vDriverName;
	}
	public String getvDriverDlNo() {
		return vDriverDlNo;
	}
	public void setvDriverDlNo(String vDriverDlNo) {
		this.vDriverDlNo = vDriverDlNo;
	}
	public Date getvDriverDlIssueDt() {
		return vDriverDlIssueDt;
	}
	public void setvDriverDlIssueDt(Date vDriverDlIssueDt) {
		this.vDriverDlIssueDt = vDriverDlIssueDt;
	}
	public Date getvDriverDlValidDt() {
		return vDriverDlValidDt;
	}
	public void setvDriverDlValidDt(Date vDriverDlValidDt) {
		this.vDriverDlValidDt = vDriverDlValidDt;
	}
	public String getvDriverMobNo() {
		return vDriverMobNo;
	}
	public void setvDriverMobNo(String vDriverMobNo) {
		this.vDriverMobNo = vDriverMobNo;
	}
	public String getvPermitNo() {
		return vPermitNo;
	}
	public void setvPermitNo(String vPermitNo) {
		this.vPermitNo = vPermitNo;
	}
	public Date getvPermitIssueDt() {
		return vPermitIssueDt;
	}
	public void setvPermitIssueDt(Date vPermitIssueDt) {
		this.vPermitIssueDt = vPermitIssueDt;
	}
	public Date getvPermitValidDt() {
		return vPermitValidDt;
	}
	public void setvPermitValidDt(Date vPermitValidDt) {
		this.vPermitValidDt = vPermitValidDt;
	}
	public String getvPermitStateCode() {
		return vPermitStateCode;
	}
	public void setvPermitStateCode(String vPermitStateCode) {
		this.vPermitStateCode = vPermitStateCode;
	}
	public String getvPermitStateName() {
		return vPermitStateName;
	}
	public void setvPermitStateName(String vPermitStateName) {
		this.vPermitStateName = vPermitStateName;
	}
	public String getvFitDocNo() {
		return vFitDocNo;
	}
	public void setvFitDocNo(String vFitDocNo) {
		this.vFitDocNo = vFitDocNo;
	}
	public Date getvFitDocIssueDt() {
		return vFitDocIssueDt;
	}
	public void setvFitDocIssueDt(Date vFitDocIssueDt) {
		this.vFitDocIssueDt = vFitDocIssueDt;
	}
	public Date getvFitDocValidDt() {
		return vFitDocValidDt;
	}
	public void setvFitDocValidDt(Date vFitDocValidDt) {
		this.vFitDocValidDt = vFitDocValidDt;
	}
	public String getvFitDocStateCode() {
		return vFitDocStateCode;
	}
	public void setvFitDocStateCode(String vFitDocStateCode) {
		this.vFitDocStateCode = vFitDocStateCode;
	}
	public String getvFitDocStateName() {
		return vFitDocStateName;
	}
	public void setvFitDocStateName(String vFitDocStateName) {
		this.vFitDocStateName = vFitDocStateName;
	}
	public String getvFinanceBankName() {
		return vFinanceBankName;
	}
	public void setvFinanceBankName(String vFinanceBankName) {
		this.vFinanceBankName = vFinanceBankName;
	}
	public String getvPolicyNo() {
		return vPolicyNo;
	}
	public void setvPolicyNo(String vPolicyNo) {
		this.vPolicyNo = vPolicyNo;
	}
	public String getvPolicyCompany() {
		return vPolicyCompany;
	}
	public void setvPolicyCompany(String vPolicyCompany) {
		this.vPolicyCompany = vPolicyCompany;
	}
	public String getvTransitPassNo() {
		return vTransitPassNo;
	}
	public void setvTransitPassNo(String vTransitPassNo) {
		this.vTransitPassNo = vTransitPassNo;
	}
	public Boolean getIsPending() {
		return isPending;
	}
	public void setIsPending(Boolean isPending) {
		this.isPending = isPending;
	}
	public Boolean getIsCancel() {
		return isCancel;
	}
	public void setIsCancel(Boolean isCancel) {
		this.isCancel = isCancel;
	}
	public String getvRcNo() {
		return vRcNo;
	}
	public void setvRcNo(String vRcNo) {
		this.vRcNo = vRcNo;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getCreationTS() {
		return creationTS;
	}
	public void setCreationTS(Date creationTS) {
		this.creationTS = creationTS;
	}
	
}