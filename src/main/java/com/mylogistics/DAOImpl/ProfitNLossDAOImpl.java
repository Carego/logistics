package com.mylogistics.DAOImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.ProfitNLossDAO;
import com.mylogistics.model.AssestsNLiab;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.ProfitNLoss;
import com.mylogistics.model.dataintegration.AssestsNLiabDemo;
import com.mylogistics.model.dataintegration.ProfitNLossDemo;

public class ProfitNLossDAOImpl implements ProfitNLossDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public ProfitNLossDAOImpl(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;  
	}
	
	@Transactional
	@Override
	public int saveProfitNLoss() {
		
		List<ProfitNLossDemo> plList;
		List<FAParticular> faParticularsList;
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			
			Criteria cr=session.createCriteria(FAParticular.class);
			
			cr.add(Restrictions.eq("faPerType", "profitnloss"));
			faParticularsList = cr.list();
			if (!faParticularsList.isEmpty()) {
				temp = 1;
				String id = String.valueOf(faParticularsList.get(0).getFaPerId());
				
				if (id.length()<2) {
					id="0"+id;
				}
				
				System.out.println("\n\nFAParticular Id: "+id);
				
				cr=session.createCriteria(ProfitNLossDemo.class);
				plList  = cr.list();
				
				for (ProfitNLossDemo profitNLossDemo : plList) {
					ProfitNLoss profitNLoss = new ProfitNLoss();
					profitNLoss.setPnlName(profitNLossDemo.getPnlName());
					profitNLoss.setPnlFaCode(id+profitNLossDemo.getPnlFaCode());
					session.save(profitNLoss);
				}
			}else {
				System.out.println("no entry for profitNLoss in faPerticular Table");
				temp = 0;
			}
			transaction.commit();//fixed
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = 0;
		}
		session.clear();
		session.close();
		return temp; 
	}

	@Transactional
	@Override
	public int saveProfitNLossMstr(String userCode, String bCode) {

		List<ProfitNLoss> plList;
		//List<FAParticular> faParticularsList;
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			
			Criteria cr=session.createCriteria(ProfitNLoss.class);
			
			plList = cr.list();
			if (!plList.isEmpty()) {
				temp = 1;
				for (ProfitNLoss profitNLoss : plList) {
					System.out.println("profitnloss FaCode: "+profitNLoss.getPnlFaCode()+
							"\t"+"profitnloss Name: "+profitNLoss.getPnlName());
					
					FAMaster faMaster = new FAMaster();
					faMaster.setFaMfaCode(profitNLoss.getPnlFaCode());
					faMaster.setFaMfaName(profitNLoss.getPnlName());
					faMaster.setbCode(bCode);
					faMaster.setUserCode(userCode);
					faMaster.setFaMfaType("profitnloss");
					
					session.save(faMaster);
				}
				
			} else {
				System.out.println("no entry for profitnloss in faMaster Table");
				temp = 0;
			}
			transaction.commit();//fixed
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = 0;
		}
		session.clear();
		session.close();
		return temp;
	}
}
