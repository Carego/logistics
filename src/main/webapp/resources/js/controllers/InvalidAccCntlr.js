
'use strict';

var app = angular.module('application');

app.controller('InvalidAccCntlr',['$scope','$location','$http','$filter','$window','FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){


	
	$scope.brkList=[];
	$scope.ownList=[];
	$scope.brokerDB=false;
	$scope.ownerDB=false;
	$scope.brkBnkHlrNmDB = true;
	$scope.brkBnkNmDB = true;
	$scope.brkAccNoNmDB = true;
	$scope.brkIfscNmDB = true;
	
	$scope.ownBnkHlrNmDB = true;
	$scope.ownBnkNmDB = true;
	$scope.ownAccNoNmDB = true;
	$scope.ownIfscNmDB = true;
	
	$scope.ownChqUpldFlag=false;
	$scope.ownerTemp={};
	$scope.brkChqUpldFlag=false;
	$scope.brokerTemp={};
	
	
	$scope.getBrokerList=function(){
		console.log("getBrokerList()");
		$scope.ownerDB=false;
		var response = $http.post($scope.projectName+'/getBrokerInvalidAcc');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.brkList = data.brkList;
				$scope.brokerDB=true;
				console.log("broker List="+$scope.brkList);
			}else{
				$scope.alertToast("Detail not found");
				$scope.alertToast(data.msg);
				console.log(data.exc);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	};
	
	
	
	$scope.showImage=function(brk){
		
		var img={
				"imageType":"BRK",
				"id" : brk.brkId,
				"ownBrkImgType": "CHQ"
		};
		
		
		var response = $http.post($scope.projectName+'/isDownloadImg', img);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				var form = document.createElement("form");
		        form.method = "POST";
		        form.action = $scope.projectName + '/downloadImg';
		        form.target = "_blank";
		        document.body.appendChild(form);
		        form.submit();
			}else
				$scope.alertToast(data.msg);
		});
		response.error(function(data){
			console.log("Error in hitting /downloadImg");
		});
		
		
	}
	
	
	
	
	

	$scope.changeBrkAccHldrName=function(index,brk){
		$scope.index=index;
		$scope.brkBnkHldrName=brk.brkAcntHldrName;
		$scope.brkBnkHlrNmDB = false;
		$('div#brkBnkHlrNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "A/C Holder Name",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#brkBnkHlrNmId').dialog('open');
		
	}
	
	
	$scope.saveChngBrkBnkHldrName=function(brkBnkHldrName){
		$scope.brkList[$scope.index].brkAcntHldrName=brkBnkHldrName;
		$('div#brkBnkHlrNmId').dialog('close');
	}
	
	
	$scope.changeBrkBankName=function(index,brk){
		$scope.index=index;
		$scope.brkBnkName=brk.brkBnkBranch;
		$scope.brkBnkNmDB = false;
		$('div#brkBnkNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Name",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#brkBnkNmId').dialog('open');
		
	}
	
	
	$scope.saveChngBrkBnkName=function(brkBnkName){
		$scope.brkList[$scope.index].brkBnkBranch=brkBnkName;
		$('div#brkBnkNmId').dialog('close');
	}
	
	
	$scope.changeBrkIfscCode=function(index,brk){
		$scope.index=index;
		$scope.brkIfscCode=brk.brkIfsc;
		$scope.brkIfscNmDB = false;
		$('div#brkIfscNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "IFSC Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		       
		    }
			});
					
		$('div#brkIfscNmId').dialog('open');
		
	}
	
	
	$scope.saveChngBrkIfscName=function(brkIfscCode){
		$scope.brkList[$scope.index].brkIfsc=brkIfscCode;
		$('div#brkIfscNmId').dialog('close');
	}
	
	
	$scope.changeBrkAccntNo=function(index,brk){
		$scope.index=index;
		$scope.brkAccNo=brk.brkAccntNo;
		$scope.brkAccNoNmDB = false;
		$('div#brkAccNoNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "A/C No.",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#brkAccNoNmId').dialog('open');
		
	}
	
	
	$scope.saveChngBrkAccNo=function(brkAccNo){
		$scope.brkList[$scope.index].brkAccntNo=brkAccNo;
		$('div#brkAccNoNmId').dialog('close');
	}
	
	
	
	$scope.validBrkBnkDet=function(brk){
		
		var response = $http.post($scope.projectName+'/updateBrkBnkDet', brk);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.brkList=[];
				$scope.getBrokerList();
			}else
				$scope.alertToast("Retry");
		});
		response.error(function(data){
			console.log("Error in hitting /validBrkBnkDet");
		});
		
	}
	
	
	/* ----------------------------------------------------------------*/
	
	$scope.getOwnerList=function(){
		console.log("getOwnerList()");
		$scope.brokerDB=false;
		var response = $http.post($scope.projectName+'/getOwnerInvalidAcc');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.ownList = data.ownList;
				$scope.ownerDB=true;
				console.log("owner List="+$scope.ownList);
			}else{
				$scope.alertToast("Detail not found");
				$scope.alertToast(data.msg);
				console.log(data.exc);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	};
	
	
	
	$scope.showOwnImage=function(own){
		
		var img={
				"imageType":"own",
				"id" : own.ownId,
				"ownBrkImgType": "CHQ"
		};
		
		
		var response = $http.post($scope.projectName+'/isDownloadImg', img);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				var form = document.createElement("form");
		        form.method = "POST";
		        form.action = $scope.projectName + '/downloadImg';
		        form.target = "_blank";
		        document.body.appendChild(form);
		        form.submit();
			}else
				$scope.alertToast(data.msg);
		});
		response.error(function(data){
			console.log("Error in hitting /downloadImg");
		});
		
		
	}
	
	
	$scope.validOwnBnkDet=function(own){
		
		var response = $http.post($scope.projectName+'/updateOwnBnkDet', own);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.ownList=[];
				$scope.getOwnerList();
			}else
				$scope.alertToast("Retry");
		});
		response.error(function(data){
			console.log("Error in hitting /validOwnBnkDet");
		});
		
	}
	
	
	
	$scope.changeOwnAccHldrName=function(index,own){
		$scope.index=index;
		$scope.ownBnkHldrName=own.ownAcntHldrName;
		$scope.ownBnkHlrNmDB = false;
		$('div#ownBnkHlrNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "A/C Holder Name",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#ownBnkHlrNmId').dialog('open');
		
	}
	
	
	$scope.saveChngOwnBnkHldrName=function(ownBnkHldrName){
		$scope.ownList[$scope.index].ownAcntHldrName=ownBnkHldrName;
		$('div#ownBnkHlrNmId').dialog('close');
	}
	
	
	$scope.changeOwnBankName=function(index,own){
		$scope.index=index;
		$scope.ownBnkName=own.ownBnkBranch;
		$scope.ownBnkNmDB = false;
		$('div#ownBnkNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Name",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#ownBnkNmId').dialog('open');
		
	}
	
	
	$scope.saveChngOwnBnkName=function(ownBnkName){
		$scope.ownList[$scope.index].ownBnkBranch=ownBnkName;
		$('div#ownBnkNmId').dialog('close');
	}
	
	
	$scope.changeOwnIfscCode=function(index,own){
		$scope.index=index;
		$scope.ownIfscCode=own.ownIfsc;
		$scope.ownIfscNmDB = false;
		$('div#ownIfscNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "IFSC Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#ownIfscNmId').dialog('open');
		
	}
	
	
	$scope.saveChngOwnIfscName=function(ownIfscCode){
		$scope.ownList[$scope.index].ownIfsc=ownIfscCode;
		$('div#ownIfscNmId').dialog('close');
	}
	
	
	$scope.changeOwnAccntNo=function(index,own){
		$scope.index=index;
		$scope.ownAccNo=own.ownAccntNo;
		$scope.ownAccNoNmDB = false;
		$('div#ownAccNoNmId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "A/C No.",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        
		    }
			});
					
		$('div#ownAccNoNmId').dialog('open');
		
	}
	
	
	$scope.saveChngOwnAccNo=function(ownAccNo){
		$scope.ownList[$scope.index].ownAccntNo=ownAccNo;
		$('div#ownAccNoNmId').dialog('close');
	}
	
	
	
	$scope.openBrkUploadDB = function(brk){
		$scope.brokerTemp=brk;
		$scope.brkChqUpldFlag=true;
		$('div#brkChqUpldDBId').dialog({
			autoOpen: false,
			modal:true,
			title: "Documents Upload",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#brkChqUpldDBId').dialog('open');
	}
	
	
	

	$scope.uploadBrkChqImage = function(brkChqImage){
		console.log("enter into uploadBrkChqImage function");
		var file = brkChqImage;
		console.log("file name="+file.name);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else{
			
			
			  console.log('file is ' + JSON.stringify(file) + "brkId"+$scope.brokerTemp.brkId);
				var uploadUrl = $scope.projectName+"/uploadBrkChqImage";
				FileUploadService.uploadIdFileToUrl(file, uploadUrl,$scope.brokerTemp.brkId);
				$scope.brkChqUpldFlag=false;
				
				console.log("file save on server");
				$('div#brkChqUpldDBId').dialog('close');
			
		}
		
	}
	
	
	
	
	
	$scope.openOwnUploadDB = function(own){
		$scope.ownerTemp=own;
		$scope.ownChqUpldFlag=true;
		$('div#ownChqUpldDBId').dialog({
			autoOpen: false,
			modal:true,
			title: "Documents Upload",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#ownChqUpldDBId').dialog('open');
	}
	
	
	

	$scope.uploadOwnChqImage = function(ownChqImage){
		console.log("enter into uploadOwnChqImage function");
		var file = ownChqImage;
		console.log("file name="+file.name);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else{
			
			
			  console.log('file is ' + JSON.stringify(file) + "brkId"+$scope.ownerTemp.ownId);
				var uploadUrl = $scope.projectName+"/uploadOwnChqImage";
				FileUploadService.uploadIdFileToUrl(file, uploadUrl,$scope.ownerTemp.ownId);
				$scope.ownChqUpldFlag=false;
				
				console.log("file save on server");
				$('div#ownChqUpldDBId').dialog('close');
			
		}
		
	}
	
	
	
}]);