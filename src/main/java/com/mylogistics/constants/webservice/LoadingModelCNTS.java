package com.mylogistics.constants.webservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.FetchMode;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mylogistics.model.webservice.VendorDetail;
import com.mylogistics.model.webservice.VendorDetail.SelectedVendor;
import com.mylogistics.model.webservice.VendorDetail.UnSelectedVendor;

public class LoadingModelCNTS {

	public static String LD_ID = "ldId";
	
	public static String BRANCH_CODE = "branchCode";
	
	public static String BRANCH_NAME = "branchName";
	
	public static String OFFICER_NAME = "officerName";
	
	public static String CUSTOMER_ID = "customerId";
	
	public static String CUSTOMER_NAME = "customerName";
	
	public static String VEHICLE_TYPE = "vehicleType";
	
	public static String SOURCE_NAME = "sourceName";
	
	public static String DESTINATION_NAME = "destinationName";
	
	public static String SOURCE_ID = "sourceId";
	
	public static String DESTINATION_ID = "destinationId";
	
	public static String QUANTITY = "quantity";
	
	public static String METRIC_OR_TON = "metricOrTon";
	
	public static String VENDOR_DETAILS = "vendorDetails";
	
	public static String IS_PENDING = "isPending";
	
	public static String CREATIONTS = "creationTS";
	
	public static String ORDER_NO = "orderNo";
	
}