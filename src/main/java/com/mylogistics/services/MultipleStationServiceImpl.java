package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import com.mylogistics.model.MultipleStation;

public class MultipleStationServiceImpl implements MultipleStationService{
	
	private static List<MultipleStation> stationList = new  ArrayList<MultipleStation>();
	
	 public List<MultipleStation> getAllStation() {
	        return stationList;
	    }
	 
	 public void addStation(MultipleStation multipleStation){
		 stationList.add(multipleStation);
	 }
	 
	 public void deleteAllStation() {
		 stationList.clear();
	 	}
	 
	 public void deleteStation(MultipleStation multipleStation){
	     	System.out.println("enter into deleteStation");
	     	
	     	String temp = multipleStation.getMulStnStnCode();
	     	
	     	if (!stationList.isEmpty()) {
	     		System.out.println("size of multipleStations = "+stationList.size());
					for (int i = 0; i < stationList.size(); i++) {
						String listTemp = stationList.get(i).getMulStnStnCode();
						if (temp.equalsIgnoreCase(listTemp)) {
							stationList.remove(stationList.get(i));
						}
					}
					System.out.println("after deleting size of multipleStations = "+stationList.size());
				}
		 }

}
