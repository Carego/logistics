package com.mylogistics.DAOImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CustomerOldDAO;
import com.mylogistics.model.CustomerOld;

public class CustomerOldDAOImpl implements CustomerOldDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public CustomerOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveCustOld(CustomerOld customerOld){
		 System.out.println("enter into saveCustOld function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(customerOld);
			 transaction.commit();
			 session.flush();
			 temp = 1;
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
}
