package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.UserRightsDAO;
import com.mylogistics.model.User;
import com.mylogistics.model.UserRights;
import com.mylogistics.services.ConstantsValues;

@Controller
public class UserRightsCntlr {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private UserRightsDAO userRightsDAO;
	
	@RequestMapping(value = "/allUserFrRght" , method = RequestMethod.POST)  
	public @ResponseBody Object allUserFrRght() {  
		System.out.println("Enter into allUserFrRght---->");
		Map<String,Object> map = new HashMap<>();
		List<User> userList = userDAO.getAllUser();
		if(!userList.isEmpty()){
			map.put("list",userList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/submitUR" , method = RequestMethod.POST)  
	public @ResponseBody Object submitUR(@RequestBody UserRights userRights){  
		System.out.println("Enter into submitUR---->"+userRights.isUrFT());
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		userRights.setbCode(currentUser.getUserBranchCode());
		userRights.setUserCode(currentUser.getUserCode());
		int urResult = userRightsDAO.saveUserRights(userRights);
		if(urResult > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/exstngUR" , method = RequestMethod.POST)  
	public @ResponseBody Object exstngUR(@RequestBody int userId){  
		System.out.println("Enter into submitUR---->");
		Map<String,Object> map = new HashMap<>();
		UserRights userRights = userRightsDAO.getUserRights(userId);
		if(userRights != null){
			map.put("ur",userRights);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
}
