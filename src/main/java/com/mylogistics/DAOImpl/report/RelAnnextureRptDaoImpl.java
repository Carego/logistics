package com.mylogistics.DAOImpl.report;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mylogistics.DAO.report.RelAnnextureRptDAO;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillFormateNameCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.CustomerGroupCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Bill;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.Station;
import com.mylogistics.services.NumToWords;

public class RelAnnextureRptDaoImpl implements RelAnnextureRptDAO {

@Autowired
private SessionFactory sessionFactory;

private Session session;
private Transaction transaction;
	@Override
	public List<Map<String,Object>> submitRelAnnextureRpt(Date fromDt, Date toDt, String custGroupId) {
		// TODO Auto-generated method stub
		 System.out.println("enter into submitRelAnnextureRpt function");
		 List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
		 List<Bill> billList = new ArrayList<Bill>();
		 List<Integer> custCodeList=new ArrayList<Integer>();
		// int frBl = Integer.parseInt(frBlNo.substring(3));
		// int toBl = Integer.parseInt(toBlNo.substring(3));
		 System.out.println("frDt = "+String.valueOf(fromDt));
		 System.out.println("toDt = "+String.valueOf(toDt));
		 System.out.println("custGroupId="+custGroupId);
		 
		 try{
			 session = this.sessionFactory.openSession();
			Criteria custCr=session.createCriteria(Customer.class);
					custCr.setProjection(Projections.property(CustomerCNTS.CUST_ID));
			       custCr.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID,custGroupId));
			      custCodeList=custCr.list();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.in(BillCNTS.BILL_CUST_ID,custCodeList)); 
			 cr.add(Restrictions.ge(BillCNTS.BILL_DT,fromDt));
			  cr.add(Restrictions.le(BillCNTS.BILL_DT,toDt));
			  cr.add(Restrictions.gt(BillCNTS.BILL_REM_AMT,0.0));
			  cr.add(Restrictions.eq(BillCNTS.BILL_CANCEL,false));
			  
			 billList = cr.list();
			 System.out.println("billList.isEmpty()="+billList.isEmpty());
			 System.out.println(billList.size());
			 if(!billList.isEmpty()){
				 for(int i=0;i<billList.size();i++){
					 System.out.println("billNo="+billList.get(i).getBlBillNo());
					 System.out.println(Integer.parseInt(billList.get(i).getBlBillNo().substring(3)));
						 Map<String,Object> map = new HashMap<String, Object>();
						 map.put("bill", billList.get(i));
						 System.out.println("billList.get(i).getBlCustId()="+billList.get(i).getBlCustId());
					//	 Customer customer = (Customer) session.get(Customer.class,billList.get(i).getBlCustId());
					//	 map.put("cust",customer);
						 if(billList.get(i).getBillForwarding() != null){
							 System.out.println("*************************************");
							 map.put("bfNo",billList.get(i).getBillForwarding().getBfNo());
							 map.put("bfDt",billList.get(i).getBillForwarding().getCreationTS().getTime());
						 }
						 
						 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
						 for(int j=0;j<billList.get(i).getBillDetList().size();j++){
							 Map<String,Object> billDetMap = new HashMap<String, Object>();
							 Cnmt cnmt = (Cnmt) session.get(Cnmt.class,billList.get(i).getBillDetList().get(j).getBdCnmtId());
							 billDetMap.put("billDet",billList.get(i).getBillDetList().get(j));
							 billDetMap.put("cnmt", cnmt);
							 
							 List<Cnmt_Challan> ccList = new ArrayList<Cnmt_Challan>();
							 cr = session.createCriteria(Cnmt_Challan.class);
							 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId,cnmt.getCnmtId()));
							 ccList = cr.list();
							 
							 if(!ccList.isEmpty()){
								 Challan challan = (Challan) session.get(Challan.class,ccList.get(ccList.size() - 1).getChlnId());
								 billDetMap.put("vehType",challan.getChlnVehicleType());
								 if(challan.getChlnArId() > 0){
									 System.out.println("*************************** 1");
									 ArrivalReport arRep = (ArrivalReport) session.get(ArrivalReport.class,challan.getChlnArId());
									 System.out.println("arRep.getArPkg() = "+arRep.getArPkg());
									 System.out.println("arRep.getArRepDt() = "+arRep.getArRepDt());
									 System.out.println("arRep.getArOvrHgt() = "+arRep.getArOvrHgt());
									 billDetMap.put("delQty",arRep.getArPkg());
									 billDetMap.put("delDt",arRep.getArRepDt());
									 billDetMap.put("ovrHgt",arRep.getArOvrHgt());
								 }
								 if(challan.getChlnLryNo() != null && !challan.getChlnLryNo().trim().equals("")){
									 billDetMap.put("truckNo",challan.getChlnLryNo());
								 }else{
									 List<ChallanDetail> chdList = new ArrayList<ChallanDetail>();
									 cr = session.createCriteria(ChallanDetail.class);
									 cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
									 chdList = cr.list();
									 if(!chdList.isEmpty()){
										 billDetMap.put("truckNo",chdList.get(0).getChdRcNo());
									 }
								 }
							 }
							 
							 Station frStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtFromSt()));
							 Station toStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtToSt()));
							 billDetMap.put("frmStn",frStn.getStnName());
							 billDetMap.put("toStn",toStn.getStnName());
							 blDetList.add(billDetMap);
						 }
						 map.put("blDetList",blDetList);
						 System.out.println("billList.get(i).getBlFinalTot()="+billList.get(i).getBlFinalTot());
										 
						 blList.add(map);
					
				 }
			 }
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return blList;
		
	}
}
