package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class JVouchService {
	
	private VoucherService voucherService;
	
	private List<JourVouchDRCR> jvList = new ArrayList<JourVouchDRCR>();

	public VoucherService getVoucherService() {
		return voucherService;
	}

	public void setVoucherService(VoucherService voucherService) {
		this.voucherService = voucherService;
	}

	public List<JourVouchDRCR> getJvList() {
		return jvList;
	}

	public void setJvList(List<JourVouchDRCR> jvList) {
		this.jvList = jvList;
	}
	
	
}
