package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MultiCnmtServiceImpl implements MultiCnmtService{
	private static List<Map<String,Object>> multiCnmtList = new ArrayList<>();
	
	public List<Map<String,Object>> getAllMultiCnmt(){
		return multiCnmtList;
	}

    public void addMultiCnmt(Map<String,Object> map){
    	multiCnmtList.add(map);
    }

    public void deleteMultiCnmt(Map<String,Object> map){
    	if(!multiCnmtList.isEmpty()){
    		//multiCnmtList.remove(multiCnmtCode);
    		for(int i=0;i<multiCnmtList.size();i++){
    			if(String.valueOf(multiCnmtList.get(i).get("cnmt")).equals(String.valueOf(map.get("cnmt")))){
    				multiCnmtList.remove(i);
    				break;
    			}
    		}
    	}
    }

    public void deleteAllMultiCnmt(){
    	multiCnmtList.clear();
    }
	
}
