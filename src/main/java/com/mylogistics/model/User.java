package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class User {
	
	@Id
	@Column(name="userId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userId;
	 
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="password")
	private String password;
	
	@Column(name="userRole")
	private int userRole;

	@Column(name="userStatus")
	private String userStatus;
	
	@Column(name="userBranchCode")
	private String userBranchCode;
	
	@Column(name="userAuthToken")
	private String userAuthToken;
	
	@Column(name="userCodeTemp")
	private String userCodeTemp;
	
	@Column(name="userName" , unique=true)
	private String userName;
	
	@Column(name="userRights")
	private ArrayList<String> userRights;
		
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public Calendar getCreationTS() {
		return creationTS;
	}

	public ArrayList<String> getUserRights() {
		return userRights;
	}

	public void setUserRights(ArrayList<String> userRights) {
		this.userRights = userRights;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getUserAuthToken() {
		return userAuthToken;
	}

	public void setUserAuthToken(String userAuthToken) {
		this.userAuthToken = userAuthToken;
	}

	public String getUserBranchCode() {
		return userBranchCode;
	}

	public void setUserBranchCode(String userBranchCode) {
		this.userBranchCode = userBranchCode;
	}

	public void setUserId(int userid){
		this.userId = userid;
	}
	
	public int getUserId(){
		return userId;
	}
	
	public void setUserCode(String usercode){
		this.userCode = usercode;
	}
	
	public String getUserCode(){
		return userCode;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getPassword(){
		return password;
	}

	public int getUserRole() {
		return userRole;
	}

	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserCodeTemp() {
		return userCodeTemp;
	}

	public void setUserCodeTemp(String userCodeTemp) {
		this.userCodeTemp = userCodeTemp;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}