'use strict';

var app = angular.module('application');

app.controller('ViewAtmCardCntlr',['$scope','$location','$http','$filter', '$log', 
                                 function($scope,$location,$http,$filter,$log){
	
	console.log("ViewAtmCardCntlr Started");

	$scope.atmCardNo = "";
	$scope.atmCardMstr = {};
	$scope.branchList = [];
	$scope.branch = {};
	
	$scope.branchDBFlag = true;
	$scope.tableFlag = false;
	
	$scope.getActiveBrList = function() {
		console.log("getActiveBrList Entered");
		var response = $http.post($scope.projectName+'/getUserBrNBrList');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getActiveBrList result Success");
				$scope.branchList = data.branchList;
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrList result Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in response getActiveBrList: "+data);
		});
	}
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.submitAtmForm = function() {
		console.log("submitAtmForm()");
		$scope.tableFlag = false;
		var res = $http.post($scope.projectName+'/getAtmCardByNo', $scope.atmCardNo);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				$scope.tableFlag = true;
				$scope.alertToast(data.result);
				$log.info(data);
				$scope.atmCardMstr = data.atmCardMstr;
			} else {
				$scope.tableFlag = false;
				$scope.alertToast(data.result);
			}
			console.log("getAtmCardByNo response "+data.result);
		});
		res.error(function(data, status, headers, config) {
			$scope.alertToast("database exception");
			$scope.tableFlag = false;
			console.log("error in response in getAtmCardByNo: "+data);
		})
	}
	
	$scope.transAtmToBr = function(brForm) {
		console.log("transAtmToBr()");
		if(brForm.$invalid){
			console.log("invalidate: "+brForm.$invalid)
			if (brForm.branchName.$invalid) {
				$scope.alertToast("Enter Valid Branch");
			} 
		}else{
			console.log("invalidate: "+brForm.$invalid)
			var transAtmToBrService = {
				"atmCardId"	: $scope.atmCardMstr.atmCardId,
				"branchId"	: $scope.branch.branchId
			}
			var res = $http.post($scope.projectName+'/transAtmToBr', transAtmToBrService)
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("response of transAtmToBr "+data.result);
					$scope.alertToast("ATM Card Transfered Successfully");
					$scope.tableFlag = false;
					
					//release the resources
					$scope.atmCardMstr = {};
					$scope.branch = {};
				} else {
					$scope.alertToast(data.result);
					console.log("response of transAtmToBr "+data.result);
				}
			});
			res.error(function(data, status, headers, config) {
				$scope.alertToast("DataBase Exception");
				console.log("error in response of transAtmToBr "+data);
			});
		}
	}
	
	$scope.deactiveAtmCard = function() {
		console.log("deactiveAtmCard()");
		
		var res = $http.post($scope.projectName+'/deactiveAtmCard', $scope.atmCardMstr.atmCardId);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				$scope.alertToast("ATM Card Deactivated Successfully");
				console.log("response of deactiveAtmCard "+data.result);
				$scope.tableFlag = false;
			} else {
				$scope.alertToast(data.result);
				console.log("response of deactiveAtmCard "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			$scope.alertToast("DataBase Exception");
			console.log("error in response of deactiveAtmCard "+data);
		});
	}
	
	$scope.activeAtmCard = function() {
		console.log("activeAtmCard()");
		
		var res = $http.post($scope.projectName+'/activeAtmCard', $scope.atmCardMstr.atmCardId);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				$scope.alertToast("ATM Card Activated Successfully");
				console.log("response of activeAtmCard "+data.result);
				$scope.tableFlag = false;
			} else {
				$scope.alertToast(data.result);
				console.log("response of activeAtmCard "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			$scope.alertToast("DataBase Exception");
			console.log("error in response of activeAtmCard "+data);
		});
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("ViewAtmCardCntlr Ended");
	
}]);