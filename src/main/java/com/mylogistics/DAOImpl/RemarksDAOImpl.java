package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.RemarksDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.RemarksCNTS;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Remarks;

public class RemarksDAOImpl implements RemarksDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	private static Logger logger= Logger.getLogger(RemarksDAOImpl.class);
	
	@Autowired
	public RemarksDAOImpl(SessionFactory sessionFactory){
		this.sessionFactory=sessionFactory;
	}

	@Override
	public int getVerify(String chlnCode) {
		// TODO Auto-generated method stub
		int res=0;
		try{
			session=this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Challan.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
					.add(Restrictions.gt(ChallanCNTS.CHLN_REM_BAL, 0.0));
			if(!cr.list().isEmpty()){
				cr=session.createCriteria(Remarks.class)
						.add(Restrictions.eq(RemarksCNTS.DOCUMENT_NO, chlnCode))
						.add(Restrictions.eq(RemarksCNTS.SERIES, "CH"))
						.add(Restrictions.eq(RemarksCNTS.IS_ON_HOLD, true));
				res=1;
				if(!cr.list().isEmpty()){
					res=2;
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}

		session.clear();
		session.close();
		return res;
	}
	

	@Override
	public int holdChallan(Map<String,Object> clientMap,Session session){
		logger.error("holdChallan()");
		
		String date=(String) clientMap.get("date");
		Date d=Date.valueOf(date);
		System.out.println("Date="+date);
		int res=0;
		try{
			
			Criteria cr=session.createCriteria(Remarks.class)
					.add(Restrictions.eq(RemarksCNTS.DOCUMENT_NO, clientMap.get("docNo")))
					.add(Restrictions.eq(RemarksCNTS.SERIES, clientMap.get("docSer")));
			
			if(!cr.list().isEmpty()){
				Remarks remarks=(Remarks) cr.list().get(0);
				String des=remarks.getRemark();
				remarks.setOnHold(true);
				remarks.setRemark(des+" "+(String) clientMap.get("remark"));
				session.update(remarks);
				res=1;
			}else{
				Remarks remarks=new Remarks();
				remarks.setDocNo((String) clientMap.get("docNo"));
				remarks.setHoldDt(d);
				remarks.setOnHold(true);
				remarks.setRemark((String) clientMap.get("remark"));
				remarks.setSeries("CH");
				session.save(remarks);
				res=2;
			}
			
			
			
		}catch(Exception e){
			logger.error("Exception ="+ e);
			System.out.println("Exception ="+ e);
			res=3;
		}
		
		return res;
	}
	
	@Override
	public List verifyFrBal(String docNo){
		
		Session session=sessionFactory.openSession();
		List list=new ArrayList<>();
		try{
			 list=session.createCriteria(Remarks.class)
					.add(Restrictions.eq(RemarksCNTS.DOCUMENT_NO, docNo))
					.add(Restrictions.eq(RemarksCNTS.IS_ON_HOLD, true))
					.add(Restrictions.eq(RemarksCNTS.SERIES, "CH")).list();
					
		}catch(Exception e){
			System.out.println("Exception "+e);
		}
		session.clear();
		session.close();
		return list;
	}
	
	@Override
	public Map<String, Object> getHoldChln(){
		List list=new ArrayList<>();
		Map<String,Object> map=new HashMap<String, Object>();
		Session session=sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(Remarks.class)
					.add(Restrictions.eq(RemarksCNTS.IS_ON_HOLD, true))
					.add(Restrictions.eq(RemarksCNTS.SERIES, "CH"));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(RemarksCNTS.DOCUMENT_NO));
			criteria.setProjection(projList);
			list=criteria.list();
			if(!list.isEmpty()){
				map.put("result", "success");
				map.put("list", list);
			}else{
				map.put("result", "error");
			}
		}catch(Exception e){
			System.out.println("Exception ="+e);
		}
		session.clear();
		session.close();
		return map;
	}
	
	@Override
	public List getObjFrUnhldDocs(String chlnCode){
		
		Session session=sessionFactory.openSession();
		Map map=new HashMap<>();
		List list=new ArrayList<>();
		try{
			 list=session.createCriteria(Remarks.class)
					.add(Restrictions.eq(RemarksCNTS.DOCUMENT_NO, chlnCode))
					.add(Restrictions.eq(RemarksCNTS.SERIES, "CH")).list();
			
			
		}catch(Exception e){
			logger.error(e);
		}
		session.clear();
		session.close();
		return list;
	}
	
	@Override
	public int allowDocsFrHold(Remarks remarks,Session session){
		int i=0;
		try{
			remarks.setOnHold(false);
			session.update(remarks);
			i=1;
		}catch(Exception e){
			logger.error(e);
		}
		return i;
	}
	
}
