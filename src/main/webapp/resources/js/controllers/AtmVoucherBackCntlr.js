'use strict';

var app = angular.module('application');

app.controller('AtmVoucherBackCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.vs ={};
	$scope.atmCodeList = [];
	
	$scope.atmCodeDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.printVsFlag = true;
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVouchDetFrAtmBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.atmCodeList = data.atmCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.CASH_WITHDRAW_ATM;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				console.log("size of atmCodeList = "+$scope.atmCodeList.length);
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.openAtmCodeDB = function(){
		console.log("enter into openAtmCodeDB function");
		$scope.atmCodeDBFlag = false;
		$('div#atmCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank Code",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.atmCodeDBFlag = true;
			}
		});

		$('div#atmCodeDB').dialog('open');
	}
	
	
	$scope.saveAtmCode = function(atm){
		console.log("enter into saveAtmCode function");
		$scope.vs.atmCode = atm;
		$('div#atmCodeDB').dialog('close');
	}
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		if(newVoucherForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			if($scope.vs.amount <= 0){
				$scope.alertToast("please enter a valid amount");
			}else{
				console.log("final submittion");
				$scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
			}
		}
	};
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveVsDB').dialog('close');
		$scope.saveVsFlag = true;
	}
	
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitAtmVouch',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("success");
				$('#saveId').removeAttr("disabled");
				$('div#saveVsDB').dialog('close');
				$scope.alertToast("voucher generated successfully");
				$scope.vs.bnkName = data.bnkName;
				$scope.vs.bankCode = data.bnkCode;
				$scope.vs.vhNo = data.vhNo;
				$scope.vs.tvNo = data.tvNo;
				
				$scope.printVsFlag = false;
		    	$('div#printVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.printVsFlag = true;
				    }
					});
				$('div#printVsDB').dialog('open');
				
				
				
				/*$scope.vs ={};
				$scope.atmCodeList = [];
				$scope.getVoucherDetails();*/
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}

	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
		$scope.vs ={};
		$scope.atmCodeList = [];
		$scope.getVoucherDetails();
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
		$('div#printVsDB').dialog('close');
		$scope.vs ={};
		$scope.atmCodeList = [];
		$scope.getVoucherDetails();
	}
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);