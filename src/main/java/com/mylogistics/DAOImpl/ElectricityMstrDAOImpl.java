package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.ElectricityMstrDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.ElectricityMstrCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.Employee;
import com.mylogistics.model.SubElectricityMstr;
import com.mylogistics.model.TelephoneMstr;

public class ElectricityMstrDAOImpl implements ElectricityMstrDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public ElectricityMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int saveElecMstr(Branch branch , Employee employee , ElectricityMstr electricityMstr){
		System.out.println("enter into saveElecMstr function");
		int temp;
		int brId = branch.getBranchId();
		int empId = employee.getEmpId();
		List<Branch> brList = new ArrayList<Branch>();
		List<Employee> empList = new ArrayList<Employee>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			 Criteria cr=session.createCriteria(Branch.class);
			 cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
			 brList = cr.list();
			 Branch actBranch = brList.get(0);
			 if(!brList.isEmpty()){
				 electricityMstr.setBranch(actBranch);
			 }
			 
			 actBranch.getElectricityMstrList().add(electricityMstr);
			 
			 if(empId > 0){
				 Criteria cr1=session.createCriteria(Employee.class);
				 cr1.add(Restrictions.eq(EmployeeCNTS.EMP_ID,empId));
				 empList = cr1.list();
				 Employee actEmployee = empList.get(0);
				 if(!empList.isEmpty()){
					 electricityMstr.setEmployee(actEmployee);
				 }
				 actEmployee.getElectricityMstrList().add(electricityMstr);
				 session.update(actEmployee);
			 }
			
			 session.update(actBranch);
			 transaction.commit();
			 session.flush();
			 temp = 1;
			
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return 1;
	}
	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<ElectricityMstr> getAllElectMstr(){
		System.out.println("enter into getAllElectMstr function");
		List<ElectricityMstr> emList = new ArrayList<ElectricityMstr>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ElectricityMstr.class);
			emList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return emList;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int updateEMBySEM(int emId , SubElectricityMstr subElectricityMstr){
		System.out.println("enter into updateEMBySEM function");
		List<ElectricityMstr> emList = new ArrayList<ElectricityMstr>();
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ElectricityMstr.class);
			cr.add(Restrictions.eq(ElectricityMstrCNTS.EM_ID,emId));
			emList = cr.list();
			
			ElectricityMstr electricityMstr = emList.get(0);
			subElectricityMstr.setElectricityMstr(electricityMstr);
			electricityMstr.getSubElectricityMstr().add(subElectricityMstr);
			
			session.update(electricityMstr);
			
			temp = 1;
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}
}
