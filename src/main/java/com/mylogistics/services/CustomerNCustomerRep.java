package com.mylogistics.services;

public class CustomerNCustomerRep {
	private String custCode;	
	private String custName;
	private String custPanNo;
	private String custTinNo;
	private String custStatus;
	private String custRepCode;	
	private String custRepName;
	private String custRepDesig;
	private String custRepMobNo;
	private String custRepEmailId;
	private String custId;
	private String custRefNo;
	
	public static final String CUST_CODE="custCode";
	public static final String CUST_NAME="custName";
	public static final String CUST_PAN_NO="custPanNo";
	public static final String CUST_TIN_NO="custTinNo";
	public static final String CUST_STATUS="custStatus";
	public static final String CUST_REP_CODE="custRepCode";
	public static final String CUST_REP_NAME="custRepName";
	public static final String CUST_REP_DESIG="custRepDesig";
	public static final String CUST_REP_MOB_NO="custRepMobNo";
	public static final String CUST_REP_EMAIL_ID="custRepEmailId";
	public static final String CUST_ID="custId";
	public static final String CUST_REF_NO="custRefNo";
	
	
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustPanNo() {
		return custPanNo;
	}
	public void setCustPanNo(String custPanNo) {
		this.custPanNo = custPanNo;
	}
	public String getCustTinNo() {
		return custTinNo;
	}
	public void setCustTinNo(String custTinNo) {
		this.custTinNo = custTinNo;
	}
	public String getCustStatus() {
		return custStatus;
	}
	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}
	public String getCustRepCode() {
		return custRepCode;
	}
	public void setCustRepCode(String custRepCode) {
		this.custRepCode = custRepCode;
	}
	public String getCustRepName() {
		return custRepName;
	}
	public void setCustRepName(String custRepName) {
		this.custRepName = custRepName;
	}
	public String getCustRepDesig() {
		return custRepDesig;
	}
	public void setCustRepDesig(String custRepDesig) {
		this.custRepDesig = custRepDesig;
	}
	public String getCustRepMobNo() {
		return custRepMobNo;
	}
	public void setCustRepMobNo(String custRepMobNo) {
		this.custRepMobNo = custRepMobNo;
	}
	public String getCustRepEmailId() {
		return custRepEmailId;
	}
	public void setCustRepEmailId(String custRepEmailId) {
		this.custRepEmailId = custRepEmailId;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustRefNo() {
		return custRefNo;
	}
	public void setCustRefNo(String custRefNo) {
		this.custRefNo = custRefNo;
	}
}
