'use strict';

var app = angular.module('application');

app.controller('ViewEmployeeCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.employee = {};
	$scope.empCodeDBFlag = true;
	$scope.show = true;
	$scope.employ = {};
	
	
	var emplrEsi;
	var basic;
	var hra;
	var pf;
	var gross;
	var netsalary;
	var esi;
	var otherallowance = 0;
	
	$scope.Submit = function(EmpForm,employ){
		console.log("enter into submit function--->"+$scope.employ.empCode);
		if(EmpForm.$invalid){
			console.log("EmpForm.$invalid--->"+EmpForm.$invalid);
			if(EmpForm.empName.$invalid){
				$scope.alertToast("Please enter employee code....");
			}	
		}else{
			var response = $http.post($scope.projectName+'/employeeDetails', $scope.employ.empCode);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.employee = data.employee;
					$scope.show = false;
					/*$scope.employee.empDOB =  $filter('date')($scope.employee.empDOB, 'MM/dd/yyyy hh:mm:ss');
					$scope.employee.empDOAppointment =  $filter('date')($scope.employee.empDOAppointment, 'MM/dd/yyyy hh:mm:ss');
					$scope.employee.empLicenseExp =  $filter('date')($scope.employee.empLicenseExp, 'MM/dd/yyyy hh:mm:ss');
					$scope.employee.creationTS =  $filter('date')($scope.employee.creationTS, 'MM/dd/yyyy hh:mm:ss');*/
					$scope.successToast(data.result);
					
				}else{
					console.log(data);
				}				
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
		
	}
	
	$scope.openEmpCodeDB = function(){
		$scope.empCodeDBFlag = false;
		$('div#empCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Code",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
		});
	
		$('div#empCodeDB').dialog('open');
    }	 
	
	$scope.getAllEmployees = function(){
		console.log("entered into getAllEmployees function------>");
		var response = $http.post($scope.projectName+'/getAllEmployees');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.empCodeList = data.empCodeList;
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	 $scope.saveEmpCode =  function(emp){
			console.log("enter into saveEmpCode----->"+emp.empCode);
			$scope.employ.empCode = emp.empCode;
			$scope.employ.empName = emp.empName;
			$('div#empCodeDB').dialog('close');
			$scope.empCodeDBFlag = true;
			$scope.filterTextbox = "";
	}  
	
	 
	$scope.updateEmp = function(emp){
		console.log("enter into updateEmp function");
		//$scope.actEmp = emp;
		var response = $http.post($scope.projectName+'/updateEmpByOptr',emp);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.employee = {};
				$scope.empCodeDBFlag = true;
				$scope.show = true;
				$scope.employ = {};
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	} 
	 
	
	
	 $scope.calculateOnBasic = function(){	   
		  console.log("enter into calculateOnBasic function"); 
		   basic=$scope.employee.empBasic;
		   pf=0.12*basic;
		   
		   $scope.employee.empPF=pf;
		   $scope.employee.empPFEmplr = pf;
	  }
	
	
	 $scope.calGross = function(){
		   console.log("enter into calGross function");
		   gross = parseFloat($scope.employee.empBasic) + parseFloat($scope.employee.empHRA) + parseFloat($scope.employee.empOtherAllowance);
		   $scope.employee.empGross = gross;
		   
		   if(gross <= 15000){
			   esi=0.0175*gross;
			   emplrEsi = 0.0475*gross;
			   $scope.employee.empESIEmplr = emplrEsi; 
			   $scope.employee.empESI = esi;
		   }
	 }
	 
	 
	 $scope.calNetSal = function(){
		   console.log("enter into calNetSal funciton");
		   netsalary = parseFloat($scope.employee.empGross) - parseFloat($scope.employee.empPF) - parseFloat($scope.employee.empESI) - parseFloat($scope.employee.empTds);
		   $scope.employee.netSalary = netsalary;
	 }
	 
	 
	 
	 $('#empPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPin').keypress(function(e) {
		   if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });

	   $('#empHRAId').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });

	   $('#empPresentPin').keypress(function(e) {
	       if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBasic').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empBasic').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLoanBal').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empLoanBal').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLoanPayment').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empLoanPayment').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	     
	   $('#empTelephoneAmt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empTelephoneAmt').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	      
	   $('#empMobAmt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });

	   $('#empMobAmt').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPhNo').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPhNo').keypress(function(e) {
	       if (this.value.length == 15) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankAcNo').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empBankAcNo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empOtherAllowance').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empOtherAllowance').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPanNo').keypress(function(e) {
	       if (this.value.length == 10) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFirstName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   /*$('#empFirstName').bind('copy paste cut',function(e) { 
		   e.preventDefault(); 
	   });*/
	      
	   $('#empMiddleName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLastName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empDesignation').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empEduQualification').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFatherFirstName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFatherLastName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empNomineeFirstName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empNomineeLastName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	     
	   $('#empAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empESINo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empESIDispensary').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPFNo').keypress(function(e) {
	       if (this.value.length == 18) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLicenseNo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empMailId').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankBranch').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankIFS').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   
	   
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getAllEmployees();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
	
}]);