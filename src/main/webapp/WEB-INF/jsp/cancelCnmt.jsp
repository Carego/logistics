<div ng-show="operatorLogin || superAdminLogin">
	<title>Cancel Cnmt</title>
	
	<div class="row">
	<form name="cnmtDtailForm" ng-submit="cancelCNMT()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);" >
	
	<div class="row">
				<div class="input-field col s4">
					<input type="text" name="cnmtCode" ng-model="cnmtCode" ng-required=true>
					 <label>CNMT No.</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chlnDt" ng-model="chlnDt" ng-required=true>
					<label>Date</label>
				</div>
				<div class="col s4 input-field">
				    		<select name="cca" id="ccaId" ng-model="cca"  ng-init="cca = 'Yes'" ng-required="true">
								<option value='Yes'>Yes</option>
								<option value='No'>No</option>
							</select>
							<label>Consignee Copy Attached</label>
					</div>
			</div>
	
	 <div class="row">
				<div class="col s12 center"> 
					<input class="btn" type="submit" value="Cancel">
				</div>
			 </div>
	</form>
	
	</div>
</div>