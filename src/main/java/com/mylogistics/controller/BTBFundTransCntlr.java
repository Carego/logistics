package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.PetroCardDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.PetroCard;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.BTBFundTransService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BTBFundTransCntlr {

	@Autowired 
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtDAO cashStmtDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private PetroCardDAO petroCardDAO;
	
	private List<Map<String,Object>> toBrhList = new ArrayList<>(); 
	
	@RequestMapping(value="/getFrBank",method = RequestMethod.POST)
	public @ResponseBody Object getFrBank(){
		System.out.println("enter into getFrBank fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Branch branch = branchDAO.getBranchBMstrLt(Integer.parseInt(currentUser.getUserBranchCode()));
		List<Branch> brList = branchDAO.getAllActiveBranches();
		System.out.println("size of brList = "+brList.size());
		System.out.println("branch = "+branch.getBankMstrList().size());
		if(branch!=null){
			List<BankMstr> bankList = branch.getBankMstrList();
			if(!bankList.isEmpty()){
				toBrhList.clear();
				map.put("branch",branch);
				map.put("list",bankList);
				map.put("brList",brList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value="/getToBnkDet",method = RequestMethod.POST)
	public @ResponseBody Object getToBnkDet(@RequestBody int brId){
		System.out.println("enter into getToBnkDet fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		if(brId > 0){
			Branch branch = branchDAO.getBranchBMstrLt(brId);
			List<Employee> empList = branchDAO.getEmployeeList(brId);
			List<BankMstr> bnkList = branch.getBankMstrList();
			List<PetroCard> petroList=petroCardDAO.getAllPetroCardByBrh(String.valueOf(brId));
			if(!bnkList.isEmpty()||empList.isEmpty()){
				map.put("list",bnkList);
				map.put("empList",empList);
				map.put("petroList",petroList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
		
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value="/getChqFrBTB",method = RequestMethod.POST)
	public @ResponseBody Object getChqFrBTB(@RequestBody Map<String,String> clientMap){
		System.out.println("enter into getChqFrBTB fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		if(cType == null || cType == ""){
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}else{
			char CType = cType.charAt(0);
			
			List<ChequeLeaves> chqList = bankMstrDAO.getAPChqLByBnkC(bankCode,CType);
			if(!chqList.isEmpty()){
				map.put("list",chqList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}
		return map;
	}	
	
	
	
	
	@RequestMapping(value="/saveToBrDet",method = RequestMethod.POST)
	public @ResponseBody Object saveToBrDet(@RequestBody Map<String,Object> clientMap)throws Exception{
		System.out.println("enter into saveToBrDet fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		/*System.out.println("String.valueOf(clientMap.get(toDate) ===> "+clientMap.get("toDate"));
		System.out.println("String.valueOf(clientMap.get(toDate) ===> "+CodePatternService.getSqlDate(String.valueOf(clientMap.get("toDate"))));
		System.out.println("String.valueOf(clientMap.get(toDate) ===> "+CodePatternService.getSqlDate(String.valueOf(clientMap.get("toDate"))).getClass());*/
		/*SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		java.util.Date parsed = format.parse(String.valueOf(clientMap.get("toDate")));
		System.out.println("parsed = "+parsed);
		Date toDate = new java.sql.Date(parsed.getTime());
		System.out.println("toDate ====> "+toDate);*/
		
		toBrhList.add(clientMap);
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}
	
	
	@RequestMapping(value="/removeFT",method = RequestMethod.POST)
	public @ResponseBody Object removeFT(@RequestBody Map<String,Object> clientMap){
		System.out.println("enter into removeFT fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		int index = (int) clientMap.get("index");
		System.out.println("index = "+index);
		toBrhList.remove(index);
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		
		return map;
	}
	
	
	@RequestMapping(value="/fetchToBrh",method = RequestMethod.POST)
	public @ResponseBody Object fetchToBrh(){
		System.out.println("enter into fetchToBrh fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		/*if(!toBrhList.isEmpty()){
			map.put("list",toBrhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}*/
		map.put("list",toBrhList);
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}
	
	
	
	@RequestMapping(value="/FundTraSubmit",method = RequestMethod.POST)
	public @ResponseBody Object FundTraSubmit(@RequestBody BTBFundTransService btbFundTransService){
		System.out.println("enter into FundTraSubmit fucntion");
		Map<String,Object> map = new HashMap<String,Object>();
		toBrhList=btbFundTransService.getToFundList();
		System.out.println("to amt ====> "+Double.valueOf(String.valueOf(toBrhList.get(0).get("toAmt"))));
		if(btbFundTransService != null && !toBrhList.isEmpty()){
			int result = cashStmtDAO.saveFundTrans(btbFundTransService , toBrhList);
			if(result > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value="/checkCsCloseDate",method=RequestMethod.POST)
	public @ResponseBody Object checkCsCloseDate(@RequestBody Map<String,Object> checkDt){
		Map<String,Object> map=new HashMap<String,Object>();
		    String dt=(String)checkDt.get("checkDt");
		    System.out.println(dt);
		  int checkCssSize=cashStmtDAO.checkCsIsClose(Date.valueOf(dt));
             if(checkCssSize>0){
            	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
             }
             else{
            	 map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
             }
		return map;
	}
}
