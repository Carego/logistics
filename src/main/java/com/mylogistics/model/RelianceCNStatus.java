package com.mylogistics.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reliancecnstatus")
public class RelianceCNStatus {
	
	@Id
	@Column(name="cnmtCode" , nullable=false)
	private String cnmtCode;
	
	private String fromStation;
	private String toStation;
	private Date indentDate;
	private Date placementDate;
	private Date tripStartDate;
	
	private String challanno;
	private String lorryNo;
	private String carryType;
	private String currentStatus;
	
	private int trasittime;
	
	private String arNo;
	private Date reportingDate;
	private Date unloadingDate;
	private String deviation;
	
	public void setDeviation(String deviation) {
		this.deviation = deviation;
	}
	public String getDeviation() {
		return deviation;
	}
	
	public String getCnmtCode() {
		return cnmtCode;
	}
	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}
	public String getFromStation() {
		return fromStation;
	}
	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}
	public String getToStation() {
		return toStation;
	}
	public void setToStation(String toStation) {
		this.toStation = toStation;
	}
	public Date getIndentDate() {
		return indentDate;
	}
	public void setIndentDate(Date indentDate) {
		this.indentDate = indentDate;
	}
	public Date getPlacementDate() {
		return placementDate;
	}
	public void setPlacementDate(Date placementDate) {
		this.placementDate = placementDate;
	}
	public Date getTripStartDate() {
		return tripStartDate;
	}
	public void setTripStartDate(Date tripStartDate) {
		this.tripStartDate = tripStartDate;
	}
	public String getChallanno() {
		return challanno;
	}
	public void setChallanno(String challanno) {
		this.challanno = challanno;
	}
	public String getLorryNo() {
		return lorryNo;
	}
	public void setLorryNo(String lorryNo) {
		this.lorryNo = lorryNo;
	}
	public String getCarryType() {
		return carryType;
	}
	public void setCarryType(String carryType) {
		this.carryType = carryType;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getArNo() {
		return arNo;
	}
	public void setArNo(String arNo) {
		this.arNo = arNo;
	}

	public Date getUnloadingDate() {
		return unloadingDate;
	}
	public void setUnloadingDate(Date unloadingDate) {
		this.unloadingDate = unloadingDate;
	}
	public Date getReportingDate() {
		return reportingDate;
	}
	public void setReportingDate(Date reportingDate) {
		this.reportingDate = reportingDate;
	}
	public int getTrasittime() {
		return trasittime;
	}
	public void setTrasittime(int trasittime) {
		this.trasittime = trasittime;
	}
}
