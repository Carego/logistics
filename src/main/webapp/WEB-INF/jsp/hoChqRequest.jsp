<style type="text/css">
.printable {
	display: none;
}

@PAGE {
	size: A4;
	margin: 0;
}

@media print {
	.noprint,.menuwrapper,.ui-dialog,footer,#toast-container {
		display: none !important;
	}
	.printable {
		display: block;
		position: fixed;
		width: 670px;
		margin-left: -28%;
		margin-top: 100px;
		z-index: 999999;
		border: 0px solid black;
	}
}

.main {
	margin-left: 15px 15px 0px 15px;
	font-family: callibri, Geneva, sans-serif;
	padding: 10px; text-align:justify;
}

.text1 {
	margin: 5px 5px 5px 5px;
	padding: 0px;
}

.text {
	margin: 70px 5px 5px 5px;
	padding: 0px;
}

.text3 {
	margin: 30px 5px 5px 5px;
	padding: 0px;
}
</style>

<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="hoChqRequestForm" ng-submit=hoChqRequestSubmit(hoChqRequestForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       		<label for="code">Branch</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankMstrId" name ="bankMstrName" value="bank" ng-model="bankMstr.bnkName" ng-click="openBankDB()" disabled="disabled" readonly ng-required="true">
		       			<label for="code">Bank</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="chqReqDtId" name ="chqReqDtName" ng-model="chqReq.cReqDt" ng-required="true" >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chqReqNOBookId" name ="chqReqNOBookName" ng-model="chqReq.cReqNOBook" ng-required="true" ng-blur="noOfBook()" >
		       		<label for="code">No Of Books</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chqReqNOChqPerBookId" name ="chqReqNOChqPerBookName" ng-model="chqReq.cReqNOChqPerBook" ng-required="true" ng-blur="noOfChqPerBook()" >
		       		<label for="code">No Of Chq per book</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		     			<select class="validate" id="chqReqTypeId" name ="chqReqTypeName" ng-init="chqReq.cReqType = 'C'"  ng-model="chqReq.cReqType" ng-required="true" ng-change="selectChqType()" >
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
					<label for="code">Cheque Type</label>		       				
		       		</div>
		       		
		       		<!-- ng-show="chqReqPrintTypeFlag" -->
		       		
		       		<div class="col s4 input-field" >
		     			<select class="validate" id="chqReqPrintTypeId" name ="chqReqPrintTypeName" ng-init="chqReq.cReqPrintType = 'L'" ng-model="chqReq.cReqPrintType" >
							<option value='L'>Lazer Jet</option>
							<option value='D'>Dot Matrix</option>
						</select>
					<label for="code">Print Type</label>
		       		</div>
		    </div>
		    
		    <div class="row">
	     		
	     		<div class="col s4 input-field">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       	 </div>
	       	 
		  </form>
    </div>
    
    <div id ="branchDB" ng-hide="branchDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.branchName" placeholder="Search by branch Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox.branchName ">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="bankMstrDB" ng-hide="bankMstrDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Name </th>
 	  	  	  <th> Bank FaCode </th>
 	  	  	  <th> Bank A/C No. </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="saveBankDB" ng-hide="saveBankFlag" class="noprint"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch</td>
					<td>{{branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Bank</td>
					<td>{{bankMstr.bnkName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{chqReq.cReqDt}}</td>
				</tr>
				
				<tr>
					<td>No of Books</td>
					<td>{{chqReq.cReqNOBook}}</td>
				</tr>
				
				<tr>
					<td>No of chq per Book</td>
					<td>{{chqReq.cReqNOChqPerBook}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{chqReq.cReqType}}</td>
				</tr>
				
				<tr>
					<td>Print Type</td>
					<td>{{chqReq.cReqPrintType}}</td>
				</tr>
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveBankFinal()"/>
		</div>
	</div>
	
	<!--print  -->
    <div id="printChqReqDB" ng-hide="printChqReqDBFlag" >
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="yesPrint()">Yes</a>
			</div>
		</div>
	</div>
    
	<div class="printable">

		<div class="main">
			<div class="text1">
				Ref. No. : {{chqReqRefNoPrint}} <span style="margin-left: 55%;"> Date.
					{{chqReqDtPrint}}</span>
			</div>
	
			<div class="text3">
				<p>The Manager</p>
				<p style="margin-top: -10px; text-transform: uppercase;">
					{{bankNamePrint}} BANK <br /> 
					{{completeAddPrint}}, <br />
					{{addCityPrint}}-{{addPinPrint}}({{addStatePrint}})
				</p>
			</div>
	
			<div class="text3">
				Subject : <span>
					<p style="margin-left: 80px; margin-top: -22px;">Cheque Book
						Requisition for Current A/c No. {{bankAcNoPrint}} maintained in our
						Company's Name</p>
				</span>
			</div>
	
			<div class="text3">
				<p>Dear Sirs,</p>
				<p style="margin-top: -10px; padding-right: 15px;">With
					reference to the above mentioned current account, you are requested
					to immediately arrange to send us the {{chqTypeValuePrint}} cheque
					book(s) totaling {{totalNOChqPrint}} leave(s) payble at par for our office use, and
					send it to the following address.</p>
			</div>
	
			<div class="text3" style="text-transform: uppercase;">
				<p>
					SOUTH EASTERN CARRIERS PRIVATE LIMITED<br /> SCO 44, OLD JUDICIAL
					COMPLEX, CIVIL LINES<br /> GURGOAN-122001(HARYANA)
				</p>
			</div>
	
			<div class="text3">
				<p>Thanking you,</p>
			</div>
	
			<div class="text3">
				<p>For South Eastern Carriers Private Limited</p>
			</div>
	
			<div class="text">
				<p>Authorised Signatory</p>
			</div>
	
		</div>
	</div>
</div>


