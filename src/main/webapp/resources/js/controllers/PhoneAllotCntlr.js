'use strict';

var app = angular.module('application');

app.controller('PhoneAllotCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.billDays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
	$scope.branchList = [];
	$scope.empList = [];
	$scope.tpms = {};
	$scope.billEmpAdd = {};
	$scope.billNewAdd = {};
	
	$scope.showEmpAdd = false;
	$scope.showNewAdd = false;
	$scope.newAddDBFlag = true;
	$scope.branchCodeDBFlag = true;
	$scope.empCodeDBFlag = true;
	$scope.saveTpmsFlag = true;
	$scope.newAddDBFlag = true;
	
	$scope.getBranchList = function(){
		console.log("enter into getBranchList function");
		var response = $http.post($scope.projectName+'/getBrListFrPA');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.brList;
				$scope.getEmpList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	
	$scope.getEmpList = function(){
		console.log("enter into getEmpList function");
		var response = $http.post($scope.projectName+'/getEmpListFrPA');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.empList = data.empList;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.branchCodeDBFlag = false;
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Branch List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#branchCodeDB').dialog('open');
	}
	
	
	$scope.saveBrCode = function(branch){
		console.log("enter into saveBrCode function-->"+branch.branchFaCode);
		$scope.tpms.branch = branch;
		/*$scope.tpms.branch.branchFaCode = branch.branchFaCode */
		$scope.branchCodeDBFlag = true;
		$('div#branchCodeDB').dialog('close');
	}
	
	$scope.openEmpDB = function(){
		console.log("enter into openEmpDB function");
		$scope.empCodeDBFlag = false;
    	$('div#empCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Employee List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#empCodeDB').dialog('open');
	}
	
	$scope.saveEmpCode = function(emp){
		console.log("enter into saveEmpCode function");
		$scope.tpms.employee = emp;
		/*$scope.tpms.branch.branchFaCode = branch.branchFaCode */
		$scope.empCodeDBFlag = true;
		$('div#empCodeDB').dialog('close');
	}
	
	$scope.submit = function(newPhoneAllotForm){
		console.log("enter into submit function = "+newPhoneAllotForm.$invalid);
		if(newPhoneAllotForm.$invalid){
			$scope.alertToast("please enter correct value");
		}else{
			console.log("final submittion");
			
			if($scope.showEmpAdd === true){
				$scope.tpms.address = {};
			}else if($scope.showNewAdd === true){
				$scope.tpms.address = $scope.billNewAdd;
			}else{
				$scope.tpms.address = {};
			}
			
			$scope.saveTpmsFlag = false;
	    	$('div#saveTpmsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveTpmsDB').dialog('open');
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveTpmsFlag = true;
		$('div#saveTpmsDB').dialog('close');
	}
	
	
	$scope.saveTpms = function(){
		console.log("enter into saveTpms function");
		$scope.saveTpmsFlag = true;
		$('div#saveTpmsDB').dialog('close');
		var response = $http.post($scope.projectName+'/saveTpmsm',$scope.tpms);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				if(data.result === "success"){
					$scope.tpms = {};
					$scope.billEmpAdd = {};
					$scope.billNewAdd = {};
					
					$scope.showEmpAdd = false;
					$scope.showNewAdd = false;
					$scope.getBranchList();
				}else{
					console.log(data);
				}
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	
	$scope.empAdd = function(){
		console.log("enter into empAdd function");
		if(angular.isUndefined($scope.tpms.employee)){
			$scope.alertToast("please select a employee code");
		}else{
			$scope.showEmpAdd = true;
			$scope.billEmpAdd.completeAdd = $scope.tpms.employee.empAdd;
			$scope.billEmpAdd.addCity = $scope.tpms.employee.empCity;
			$scope.billEmpAdd.addPin = $scope.tpms.employee.empPin;
			$scope.billEmpAdd.addState = $scope.tpms.employee.empState;
			$scope.billNewAdd = {};
			$scope.showNewAdd = false;
		}
	}
	
	
	$scope.newAdd = function(){
		console.log("enter into newAdd function");
		$scope.newAddDBFlag = false;
    	$('div#newAddDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#newAddDB').dialog('open');
	}
	
	
	$scope.submitNewAdd = function(AddressForm){
		console.log("enter into submitNewAdd function = "+AddressForm.$invalid);
		if(AddressForm.$invalid){
			$scope.alertToast("please fill correct address");
		}else{
			$scope.newAddDBFlag = false;
			$('div#newAddDB').dialog('close');
			
			$scope.showEmpAdd = false;
			$scope.billEmpAdd = {};
			$scope.showNewAdd = true;
		}
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranchList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);