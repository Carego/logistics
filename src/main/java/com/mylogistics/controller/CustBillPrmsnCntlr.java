package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CustBillPrmsnCntlr {

	
	@Autowired
	private CustomerDAO customerDAO;
	
	@RequestMapping(value = "/getCustFrBillPr", method = RequestMethod.POST)
	public @ResponseBody Object getCustFrBillPr() {
		System.out.println("enter into getCustFrBillPr function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> custList = new ArrayList<>();
		custList = customerDAO.getCustNCI();
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getCustBillPDt", method = RequestMethod.POST)
	public @ResponseBody Object getCustBillPDt(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getCustBillPDt function");
		Map<String,Object> map = new HashMap<>();
		int customerId = (Integer) clientMap.get("custId");
		if(customerId > 0){
			Customer customer = customerDAO.getCustById(customerId);
			if(customer != null){
				map.put("cust",customer);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/saveCustBillPer", method = RequestMethod.POST)
	public @ResponseBody Object saveCustBillPer(@RequestBody Customer customer) {
		System.out.println("enter into saveCustBillPer function");
		Map<String,Object> map = new HashMap<>();
		if(customer != null){
			int res = customerDAO.updateBillPer(customer);
			if(res > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}
