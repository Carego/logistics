 <style type="text/css">
    .printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		}
                		
                		body {
						    width: 10in;
						    height:210in;
						  }
            }
            @PAGE {
				size: A4 landscape;
				 		
			}
}
 
 
 
      
 .divTable{
        display:  table;
        width:auto;
        border:1px solid #000;
        border-spacing:0px;/*cellspacing:poor IE support for  this*/
       /* border-collapse:separate;*/
        }

 .divRow{
       display:table-row;
       width:auto;
      }
    
    
     
     
 .CSSTableGenerator {
	margin-top:10px; padding:0px;
	width:100%;
	border:1px solid #000000;
	border-top-left-radius:0px;
      }
.CSSTableGenerator table{
    border-collapse: collapse;
    border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px; padding:0px;
    }


.CSSTableGenerator td{
	vertical-align:middle;
	border:1px solid #000000;
	text-align:center;
	padding:3px;
	font-size:10px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
    }
.CSSTableGenerator tr:first-child td{
    background-color:#ff7f00;
	border:0px solid #000000;
	text-align:center;
	border-width:1px 1px 1px 1px;
	font-size:14px;
	font-family:Arial;
	font-weight:bold;
	color:#ffffff;
    }
     
  tr{
   page-break-before: always;
   page-break-after: always;
  }
     
     
    .divCell01{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:300px;
        height:60px;
        background-color:#ccc;
		border: 0px solid #000;
		border-radius: 0px 0px 0px 0px;
		text-align:center;
       }
     
     
     .divCell03{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:230px;
        height:40px;
        background-color:#ccc;
		border: 1px solid #000;
		border-radius: 0px 15px 0px 0px;
		text-align:center;
        }
    
    
	
    .divCell04 {
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:117px;
        height:40px;
        background-color:#ccc;
		border: 1px solid #000;
		border-radius: 15px 0px 0px 0px;
		text-align:center;
       }
    
    
   
     .divCell{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:350px;
        height:auto;
        background-color:#ccc;
		border: 0px solid #000;
		border-radius: 0px 0px 0px 0px;
        }
        
        
    .divCell07{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:117px;
        height:40px;
        background-color:#ccc;
		border: 1px solid #000;
		border-radius:px;
		border-radius: 0px 0px 0px 15px;
		text-align:center;
       }
    
    
    .divCell08{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:230px;
        height:40px;
        background-color:#ccc;
		border: 1px solid #000;
		border-radius:px;
		text-align:center;
		border-radius: 0px 0px 15px 0px;
		
       }
    


  .divCell29{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:250px; height:30px;
        background-color:#ccc;
		border-bottom:2px solid black;
		margin-left:5px;
        } 
  
  .divCell30{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:100px;
        background-color:#ccc;
        } 


 .divCell31{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:262px;
        height:30px;
        background-color:#ccc;
		border-bottom:2px solid black;
		margin-left:5px;
        } 
  
  .divCell32{
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:80px;
        background-color:#ccc;
        } 




 div.index-05 {
	width:600px;
	height:auto;
    border:0px solid black;
    }

div.index-08 {
	margin-top:25px;
    float:right;
	width:450px;
	height:auto;
	border: 0px solid #000;
    }

div.index-09 {
	margin-top:25px;
	float:left;
	width:450px;
	height:auto;
	border: 0px solid #000;
    }
  
.div
 {
  width:340px;
  height:330px;
  font-size:20px;
  font-family:Clarendon Lt BT;
  -ms-transform:rotate(270deg); /* IE 9 */
  -moz-transform:rotate(270deg); /* Firefox */
  -webkit-transform:rotate(270deg); /* Safari and Chrome */
  -o-transform:rotate(270deg); /* Opera */
  }
.bold{font-size:12px;}
.bold1{font-size:40px; font-weight:bold;}
.bold2{font-size:14px;}
.padding{padding:10px;}
.bold4{font-size:10px; padding:5px;}
.mrg{margin-top:10px;}
.bold5{font-weight:bold; font-size:20px;}
.bold3{font-weight:bold; font-size:16px;}
	
	
</style>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="mrrPrintForm" ng-submit=mrrPrintSubmit(mrrPrintForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch" ng-required="true" ng-click="openBrhDB()" readonly>
		       			<label for="code">Branch</label>	
		       		</div>
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="mrDtId" name ="mrDtName" ng-model="mrDt" ng-required="true" >
		       			<label for="code">Date</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="submit" value="submit" >
		       			<label for="code">To Mr No</label>	
		       		</div>
		    </div>
		   <!--  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>	 --> 
		</form>    
	</div>
	<div id ="openBrhId" ng-hide="openBrhFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
		  <tr ng-repeat="brh in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
<div class="printable" id="printId"> 
 <div class="divTable"> 
	  <div class="divRow">
	    <div class="divCell01"><img src="resources/img/logo.png"/></div>
	     <div class="divCell01 bold5"><u>Money Receipt Report</u></div>
	 
	 
	  <div class="divCell">
	      
	                      <div class="divCell04 bold padding">Branch</div>
                          <div class="divCell03 bold2 padding"><div>{{branch}}</div></div>
                          <div class="divCell07 bold"><div class="padding">Date</div></div>
                          <div class="divCell08 bold2 padding">{{mrDt}}</div>
	      
	 </div>
	 
	    <div class="index-05">
		 <span class="bold1" style=" font-family:Century751 SeBd;">Care Go Logistics Pvt. Ltd.</span><br>
		 <span class="bold4">{{mrList[0].brhAdd}},</span>
		 <br><span class="bold4">Corporate Office - SCO-26, 1st Floor, HUDA Market, Sector-15, Part-II, Gurgaon-122001</span>
	   </div>
	   
	     <div class="CSSTableGenerator" >
                <table >
                    <tr>
                        <td>S.NO.</td>
                        <td>Type</td>
                        <td>M.R. NO.</td> 
                        <td>Costumer Name</td>
                        <td>Bank Name</td> 
                        <td>CHQ/RTGS No.</td>
                        <td>Ref MR</td>
                        <td>Frt Amt</td> 
                        <td>Exc Amt</td>
                        <td>TDS Amt</td>
                        <td>Ded Amt</td> 
                        <td>Rec Amt</td>
                    </tr>
                   
                    <tr ng-repeat="mr in mrList">
                        <td>{{$index + 1}}</td>
                        <td>{{mr.mr.mrType}}</td>
                        <td>{{mr.mr.mrNo}}</td> 
                        <td>{{mr.custName}}</td>
                        <td>{{mr.bnkName}}</td> 
                        <td>
                        	<div ng-if="mr.mrPayBy == 'Q'">{{mr.mrChqNo}}</div>
                        	<div ng-if="mr.mrPayBy == 'R'">{{mr.mrRtgsRefNo}}</div>
                        </td>
                        <td>
                        	<div ng-if="mr.mr.mrType == 'Payment Detail'">
		                        <div ng-repeat="refMr in mr.mr.mrFrPDList">
		                        	(	{{refMr.onAccMr}}-{{refMr.detAmt}}   )
		                        </div>
	                        </div> 
	                        <div ng-if="mr.mr.mrType == 'On Accounting' && mr.mr.mrOthMrNo != NULL">
		                        	(	{{mr.mr.mrOthMrNo}}-{{mr.mr.mrOthMrAmt}}   )
	                        </div> 
	                        <div ng-if="mr.mr.mrType == 'Direct Payment' && mr.mr.mrOthMrNo != NULL">
		                        	(	{{mr.mr.mrOthMrNo}}-{{mr.mr.mrOthMrAmt}}   )
	                        </div>
	                    </td>    
                        <td>{{mr.mr.mrFreight | number:2}}</td> 
                        <td>{{mr.mr.mrAccessAmt | number:2}}</td>
                        <td>{{mr.mr.mrTdsAmt | number:2}}</td>
                        <td>{{mr.mr.mrDedAmt | number:2}}</td> 
                         <td>
                        	<div ng-if="mr.mr.mrType == 'Payment Detail'">
		                        {{mr.mr.mrNetAmt | number:2}}
	                        </div> 
	                        <div ng-if="mr.mr.mrType == 'On Accounting'">
		                        {{mr.mr.mrNetAmt - mr.mr.mrOthMrAmt | number:2}}
	                        </div> 
	                        <div ng-if="mr.mr.mrType == 'Direct Payment'">
		                        {{mr.mr.mrNetAmt | number:2}}
	                        </div>
	                    </td>   
                        <!-- <td>{{mr.mr.mrNetAmt | number:2}}</td> -->
                    </tr>
                    
                    <tr>
                       <td colspan="11"><span style="margin-left:650px; font-size:16px; font-weight:bold;">TOTAL</span></td>
                        <td style="font-size:14px; font-weight:bold;">{{ totAmt | number:2}}</td>
                    </tr>
                    
                </table>
            </div>
            
	      
	       <div class="index-09 bold2">
	              <div class="divCell30 bold3">Prepared By:</div>
	              <div class="divCell29 bold5"></div>
	       </div>
	       
	       <div class="index-08 bold2">
	           <div style="margin-left:100px;">
	              <div class="divCell32 bold3">Manager:</div>
	              <div class="divCell31 bold5"></div>
	           </div>
	       </div>
	      
	</div>
 </div>
	 
	 
	 
 </div>
</div>      
           