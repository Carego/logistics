'use strict';

var app = angular.module('application');

app.controller('LhpvStartTempCntlr',['$scope','$location','$http','$filter','$window','AuthorisationService',
                                  function($scope,$location,$http,$filter,$window,AuthorisationService){
	
	$scope.ltList = [];
	$scope.branch = {};
	
	$scope.selLhpvDBFlag = true;
	
	$scope.getLhpvTemp = function(){
		console.log("enter into $scope.getLhpvTemp function");
		var response = $http.post($scope.projectName+'/getLhpvTempList');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.ltList = data.list;
				   $scope.branch = data.branch;
				   if($scope.ltList.length == 1){
					   console.log("****$scope.ltList.length = "+$scope.ltList.length);
					   if($scope.ltList[0].ltStart === true && $scope.ltList[0].ltEnd === false){
						   $('#saveId').attr("disabled","disabled");
						   $scope.alertToast("You already start LHPV "+$scope.ltList[0].ltLhpvNo);
					   }else{
						   $('#saveId').removeAttr("disabled");
					   }
				   }else if($scope.ltList.length > 1){
					   console.log("****$scope.ltList.length = "+$scope.ltList.length);
					   $('#saveId').removeAttr("disabled");
				   }else{
					   console.log("****$scope.ltList.length = "+$scope.ltList.length);
					   $('#saveId').attr("disabled","disabled");
				   }
			   }else{
				  $scope.errorToast("There is no Pending Lhpv");
				  $scope.ltList = [];
				  $scope.branch = {};
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	
	$scope.selLhpvNo = function(){
		console.log("enter into selLhpvNo function");
		$scope.selLhpvDBFlag = false;
		$('div#selLhpvDBId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select LHPV No.",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selLhpvDBFlag = true;
		    }
			});
		
		$('div#selLhpvDBId').dialog('open');	
	}
	
	
	$scope.saveLhpvNo = function(lhpv){
		console.log("enter into saveLhpvNo funciton");
		$scope.lt = lhpv;
		$('div#selLhpvDBId').dialog('close');	
	}
	
	
	
	$scope.voucherSubmit = function(voucherForm){
		console.log("enter into voucherSubmit function");
		if(voucherForm.$invalid){
			$scope.alertToast("please fill the correct form")
		}else{
			
			var response = $http.post($scope.projectName+'/getLhpvDet');
			  response.success(function(data, status, headers, config){
				   if(data.result === "success"){
					  
				   }else{
					   $scope.errorToast("SERVER ERROR");
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		}
	}
	

	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getLhpvTemp();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);