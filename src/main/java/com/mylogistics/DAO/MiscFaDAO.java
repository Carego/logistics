package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.MiscFa;

@Repository
public interface MiscFaDAO {

	public int saveMiscFa();
	
	public int saveMiscFaMstr(String userCode, String bCode);
	
}
