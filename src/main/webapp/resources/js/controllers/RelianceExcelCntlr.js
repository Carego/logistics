'use strict';

var app = angular.module('application');

app.controller('RelianceExcelCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){

	$scope.loadingFlag = false;
	$scope.cnDetList=[];
	$('#subAllId').removeAttr('disabled');
	
	$scope.downloadExl = function(){
		console.log("enter into downloadExl function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "Rel.xls");
	} 
	
	
	
	
	$scope.submitAll = function(){
		console.log("enter into submitAll function");
		
		if(!angular.isUndefined($scope.cnNo)){
			var req = {
					"cnNo" : $scope.cnNo,
			};
			
			var duplicate = false;
			 if($scope.cnDetList.length > 0){
				 for(var i=0;i<$scope.cnDetList.length;i++){
					if($scope.cnDetList[i].cnmtCode === $scope.cnNo){
						duplicate = true;
						break;
					}
				 }
			 }
			
			 if(duplicate == false){
				 $scope.loadingFlag = true;
					var response = $http.post($scope.projectName+'/getRelianceCnFrUpld',req);
					response.success(function(data, status, headers, config){
						if(data.result === "success"){
							$scope.loadingFlag = false;
							$scope.excelBtn=false;
							$('#printXlsId').removeAttr('disabled');
							
							$scope.cnDetList=data.cnList;
								//$scope.cnDetList.push(data.cnList[0]);

								console.log("$scope.cnDetList.length ===> "+$scope.cnDetList[0].lryNo);
								console.log("$scope.cnDetList.length ===> "+data.cnList[0].lryNo);
						}else{
							$scope.loadingFlag = false;
							$scope.alertToast("No Record Found");
						}
					});
					response.error(function(data, status, headers, config){
						$scope.loadingFlag = false;
						//console.log(data);
					}); 
			 }else
					$scope.alertToast("CNMT Already Exist");
				
			
		}else{
			$scope.alertToast("Please Enter CnNo");
		}
	}
	
	
		$scope.disableButton = function(){
			//$('#printXlsId').attr("disabled","disabled");
			//$scope.excelBtn=true;
			$scope.cnDetList=[];
		}
	
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);