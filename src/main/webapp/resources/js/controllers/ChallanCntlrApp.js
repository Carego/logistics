'use strict';

var app = angular.module('application');

app.controller('ChallanCntlrApp',['$scope','$location','$http','FileUploadService','$filter',
                               function($scope,$location,$http,FileUploadService,$filter){
	$scope.show = true;
	$scope.flag = false;
	$scope.cd = false;
	$scope.finalSubmit = false;
	
	$scope.singleCh = false;
	$scope.multipleCh = false;
	$scope.multipleDefault = true;
	
	$scope.chln = {};
	$scope.chd = {};
	
	$scope.chlnApp = {};
	$scope.cnmtChlnApp = {};
	$scope.cndApp = {};
	
	var singleCnmtData = {};
	$scope.multiCnmtCodeList = [];
	$scope.mobListBk = [];
	$scope.mobListOw = [];
	$scope.vehList = [];
	$scope.chln.chlnFreight = 0;
	$scope.chln.chlnExtra = 0;
	$scope.chln.chlnLoadingAmt = 0;
	$scope.chln.chlnStatisticalChg = 0;
	$scope.chln.chlnTdsAmt = 0;
	$scope.mobile={};
	$scope.brkrcode="";
	$scope.ownrcode="";
	$scope.mobileNo = "";
	$scope.chlnLryRatePer = "";
	$scope.chlnChgWtPer = "";

	$scope.multiCnmtCode = "";
	$scope.chlnTotalWtPer = "";

	var chlnAdvc;
	var autoCode;
	var cnmtCodeList = [];
	
	$scope.pkg = [];
	$scope.wt = [];
	$scope.totalPkg = 0;
	$scope.totalWt = 0;

	$scope.BranchCodeDBFlag=true;
	$scope.ChallanFromStationDBFlag=true;
	$scope.ChallanToStationDBFlag=true;
	$scope.EmployeeCodeDBFlag=true;
	$scope.PayAtDBFlag=true;
	$scope.SedrDBFlag=true;
	$scope.CnmtDBFlag=true;
	$scope.MultiCnmtDBFlag=true;
	$scope.VehicleTypeDBFlag=true;
	$scope.ChdCodeCbFlag=true;
	$scope.ChdBrCodeDBFlag=true;
	$scope.ChdOwnCodeDBFlag=true;
	$scope.ChdPanIssueStDBFlag=true;
	$scope.PanIssueStFlag1=true;
	$scope.ChdPerStateCodeDBFlag=true;
	$scope.PerStateCodeFlag1=true;
	$scope.ChdBrMobNoDBFlag=true;
	$scope.ChdOwnMobNoDBFlag=true;
	$scope.OwnMobNoFlag1=true;
	$scope.BrMobNoFlag1=true;
	$scope.saveMobileFlag=true;
	$scope.saveMobileOwnFlag = true;
	$scope.sameNoFlag = false;
	$scope.Sflag=true;
	$scope.Mflag=true;
	$scope.ChallanCodeDBFlag=true;
	$scope.CDFlag=false;
	$scope.viewChallanDetailsFlag=true;
	$scope.vehDBFlag=true;


	$('#chlnLryRate').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnChgWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnNoOfPkg').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#chlnNoOfPkg').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#chlnTotalWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnFreight').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnTotalFreight').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnLoadingAmt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnExtra').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnAdvance').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnBrRate').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnStatisticalChg').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnTdsAmt').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#chlnCode').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#chdChlnCode').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#mobileNo').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#mobileNo').keypress(function(e) {
		if (this.value.length === 10) {
			e.preventDefault();
		}
	});

	$('#mobileOwnNo').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#mobileOwnNo').keypress(function(e) {
		if (this.value.length === 10) {
			e.preventDefault();
		}
	});

	$('#chdPanNo').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});

	/*$('#chdPanNo').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  })*/;



	  $scope.OpenBranchCodeDB = function(){
		  $scope.getBranchData();
		  $scope.BranchCodeDBFlag=false;
		  $('div#branchCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Branch Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#branchCodeDB').dialog('open');
	  }

	  $scope.OpenChallanFromStationDB = function(){		
		  $scope.getStationData();
		  $scope.ChallanFromStationDBFlag=false;
		  $('div#challanFromStationDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Challan From Station",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#challanFromStationDB').dialog('open');
	  }

	  $scope.OpenChallanToStationDB = function(){
		  $scope.ChallanToStationDBFlag=false;
		  $('div#challanToStationDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Challan From Station",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#challanToStationDB').dialog('open');
	  }

	  $scope.openChallanCodeDB = function(){
		  $scope.getChallanCodes();
		  $scope.ChallanCodeDBFlag=false;
		  $('div#ChallanCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Challan Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#ChallanCodeDB').dialog('open');
	  }


	  $scope.OpenEmployeeCodeDB = function(){
		  $scope.getEmployeeList();
		  $scope.EmployeeCodeDBFlag=false;
		  $('div#employeeCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Employee Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#employeeCodeDB').dialog('open');
	  }

	  $scope.OpenPayAtDB = function(){
		  $scope.getBranchData();
		  $scope.PayAtDBFlag=false;
		  $('div#payAtDB').dialog({
			  autoOpen: false,
			  modal:true,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#payAtDB').dialog('open');
	  }

	  $scope.OpenCnmtDB = function(){
		  $scope.CnmtDBFlag=false;
		  $('div#cnmtDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "CNMT Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#cnmtDB').dialog('open');
	  }

	  $scope.OpenMultiCnmtDB = function(){
		  $scope.MultiCnmtDBFlag=false;
		  $('div#multiCnmtDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Multi CNMT Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#multiCnmtDB').dialog('open');
		  $scope.fetchMultiCnmtList();
	  }

	  $scope.OpenVehicleTypeDB = function(){
		  $scope.getVehicleTypeCode();
		  $scope.VehicleTypeDBFlag=false;
		  $('div#vehicleTypeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Vihicle Type",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#vehicleTypeDB').dialog('open');
	  }


	  $scope.OpenChdCodeCbDB = function(){
		  $scope.ChdCodeCbFlag=false;
		  $('div#chdCodeCb').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Challan Detail of "+$scope.chd.chdChlnCode,
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdCodeCb').dialog('open');
	  }

	  $scope.openBrkMobileNo = function()
	  {

		  console.log("Enter into openMobileNo");
		  $scope.saveMobileFlag=false;
		  $('div#saveMobileID').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Save Mobile No",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });
		  console.log("Line no 342");
		  $('div#saveMobileID').dialog('open');	
	  }


	  $scope.saveBrkMobNo = function(saveMobileForm,mobileNo)
	  {

		  if(saveMobileForm.$invalid)
		  {
			  $scope.alertToast("Mobile number should be of 10 digits");
		  }else{
			  console.log("Enter into save Mob No=" +mobileNo);
			  console.log($scope.mobListBk);

			  if($.inArray(mobileNo, $scope.mobListBk) !== -1){
				  $scope.alertToast("Mobile Number already present");
				  $scope.mobileNo = "";

			  }else{				  
				  
				  $scope.saveMobileFlag = true;
				  $('div#saveMobileID').dialog('close');

				  var code = {
						  "mobileno" : mobileNo,
						  "code"     : $scope.brkrcode
				  }

				  var response = $http.post($scope.projectName+'/updateMoblieNo', code);
				  response.success(function(data, status, headers, config) {
					  if(data.result==="success"){
						  $scope.successToast(data.result);
						  //$scope.chd.chdBrMobNo = mobileNo;
						  $scope.mobileNo = "";
						  $scope.getMobileListBk($scope.brkrcode);
					  }else{
						  console.log(data);
					  }	
				  });
				  response.error(function(data, status, headers, config) {
					  $scope.errorToast(data);
				  });
			  }						
		  }	
	  }




	  $scope.openOwnMobileNo = function()
	  {

		  console.log("Enter into openMobileNo");
		  $scope.saveMobileOwnFlag = false;
		  $('div#saveMobileOwnID').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Save Mobile No",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });
		  $('div#saveMobileOwnID').dialog('open');
		  console.log("End of openOwnMobileNo");
	  }


	  $scope.saveOwnMobNolist = function(saveMobileOwnForm,mobileOwnNo)
	  {
		  console.log("Enter into saveOwnMobNolist funciton");

		  if(saveMobileOwnForm.$invalid){

			  $scope.alertToast("Mobile number should be of 10 digits");

		  }else{
			  console.log("Enter into saveMobNolist =" + mobileOwnNo);
			  console.log($scope.mobListOw);

			  if($.inArray(mobileOwnNo, $scope.mobListOw) !== -1){
				  $scope.alertToast("Mobile Number already present");
				  $scope.mobileOwnNo = "";
			  }else {


				  $scope.saveMobileOwnFlag = true;
				  $('div#saveMobileOwnID').dialog('close');

				  var code = {
						  "mobileno" : mobileOwnNo,
						  "code"     : $scope.ownrcode
				  }

				  var response = $http.post($scope.projectName+'/updateMoblieNo', code);
				  response.success(function(data, status, headers, config) {
					  if(data.result==="success"){
						  $scope.successToast(data.result);
						  //$scope.chd.chdOwnMobNo = mobileOwnNo;
						  $scope.mobileOwnNo = "";
						  $scope.getMobileListOw($scope.ownrcode);
					  }else{
						  console.log(data);
					  }	
				  });
				  response.error(function(data, status, headers, config) {
					  $scope.errorToast(data);
				  });		
			  }
		  }		
	  }



	  $scope.OpenBrokerCodeDB = function(){
		  $scope.getBrokerCode();
		  $scope.ChdBrCodeDBFlag=false;
		  $('div#chdBrCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Broker Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdBrCodeDB').dialog('open');
	  }

	  $scope.OpenOwnerCodeDB = function(){
		  $scope.getOwnerCode();
		  $scope.ChdOwnCodeDBFlag=false;
		  $('div#chdOwnCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Owner Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdOwnCodeDB').dialog('open');
	  }

	  $scope.OpenchdPanIssueStDB = function(){
		  $scope.getStateCodeList();
		  $scope.ChdPanIssueStDBFlag=false;
		  $('div#chdPanIssueStDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Pan Issue State",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPanIssueStDB').dialog('open');
	  }

	  $scope.OpenchdPanIssueStDB1 = function(){
		  $scope.getStateCodeList();
		  $scope.PanIssueStFlag1=false;
		  $('div#chdPanIssueStDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Pan Issue State",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPanIssueStDB1').dialog('open');
	  }

	  $scope.OpenchdPerStateCodeDB = function(){
		  $scope.getStateList();
		  $scope.ChdPerStateCodeDBFlag=false;
		  $('div#chdPerStateCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Per State Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPerStateCodeDB').dialog('open');
	  }

	  $scope.OpenperStateCodeDB1 = function(){
		  $scope.getStateList();
		  $scope.PerStateCodeFlag1=false;
		  $('div#chdPerStateCodeDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Per State Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPerStateCodeDB1').dialog('open');
	  }

	  $scope.OpenBrMobNoDB = function(){
		  $scope.ChdBrMobNoDBFlag=false;
		  $('div#chdBrMobNoDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Broker Mobile Number",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdBrMobNoDB').dialog('open');
	  }

	  $scope.OpenBrMobNoDB1 = function(){
		  $scope.BrMobNoFlag1=false;
		  $('div#chdBrMobNoDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Broker Mobile Numer",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdBrMobNoDB1').dialog('open');
	  }

	  $scope.OpenOwnMobNoDB = function(){
		  $scope.ChdOwnMobNoDBFlag=false;
		  $('div#chdOwnMobNoDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Owner Mobile Number",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdOwnMobNoDB').dialog('open');
	  }

	  $scope.OpenOwnMobNoDB1 = function(){
		  $scope.OwnMobNoFlag1=false;
		  $('div#chdOwnMobNoDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Owner Mobile Number",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdOwnMobNoDB1').dialog('open');
	  }

	  $scope.getBranchData = function(){
		  console.log("getBranchData------>");
		  var response = $http.post($scope.projectName+'/getBranchDataForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.branchList = data.list;
				  //$scope.getStationData();
				  $scope.removeAllMultiCnmt();
			  }else{
				  $scope.alertToast("you don't have any active branch");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getStationData = function(){
		  console.log("getStationData------>");
		  var response = $http.post($scope.projectName+'/getStationDataForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.stationList = data.list;
			  }else{
				  $scope.alertToast("you don't have any station");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getEmployeeList = function(){
		  console.log(" entered into getListOfEmployee------>");
		  var response = $http.post($scope.projectName+'/getListOfEmployeeForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.employeeList = data.list;			
			  }else{
				  $scope.alertToast("You don't have any employee list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getCnmtList = function(){
		  console.log(" entered into getCnmtList------>");
		  $scope.removeAllMultiCnmt();	
	  }

	  $scope.getStateList = function(){
		  console.log("getStateList------>");
		  var response = $http.post($scope.projectName+'/getStateLryPrefixForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.stateList = data.list;			  
				  console.log("data from server-->"+$scope.stateList);
			  }else{
				  $scope.alertToast("you don't have any state list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getVehicleTypeCode = function(){
		  console.log("getVehicleTypeCode------>");
		  var response = $http.post($scope.projectName+'/getVehicleTypeCodeForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.vtList = data.list;				 
			  }else{
				  $scope.alertToast("you don't have any vehicle type");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getBrokerCode = function(){
		  console.log("getBrokerCode------>");
		  var response = $http.post($scope.projectName+'/getBrokerCodeForCD');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.brkList = data.list;				 
			  }else{
				  $scope.alertToast("you don't have any broker");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getOwnerCode = function(){
		  console.log("getOwnerCode------>");
		  var response = $http.post($scope.projectName+'/getOwnerCodeForCD');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.ownList = data.list;				  
			  }else{
				  $scope.alertToast("you don't have any owner");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getStateCodeList = function(){
		  console.log("getStateCodeData------>");
		  var response = $http.post($scope.projectName+'/getStateCodeDataForCD');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.stList = data.list;			  
				  console.log("data from server-->"+$scope.stList);
			  }else{
				  $scope.alertToast("you don't have any state");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getMobileListBk = function(code){
		  console.log("getMobileList------>"+code);
		  $scope.brkrcode = code;
		  for(var i=0;i<$scope.mobListBk.length;i++){
			  $scope.mobListBk.splice(i,1);
		  }
		  var response = $http.post($scope.projectName+'/getMobileListForCD',code);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.mobListBk = data.list;
				  /*$scope.getChallanCodes();*/
				  console.log("data from server-->"+$scope.mobListBk);
			  }else{
				  $scope.alertToast("you don't have any mobile list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getMobileListOw = function(code){
		  console.log("getMobileList------>"+code);
		  $scope.ownrcode = code;
		  var response = $http.post($scope.projectName+'/getMobileListForCD',code);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.mobListOw = data.list;
				  /*$scope.getChallanCodes();*/
				  console.log("data from server-->"+$scope.mobListOw);
			  }else{
				  $scope.alertToast("you don't have any mobile list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.saveEmployeeCode =  function(employee){
		  console.log("enter into saveEmployeeCode----->");
		  $scope.empTempCode = employee.empFaCode;
		  $scope.chln.chlnEmpCode = employee.empCode;
		  $('div#employeeCodeDB').dialog('close');
		  $scope.EmployeeCodeDBFlag=true;
	  }


	  $scope.saveBranchCode = function(branch){
		  $scope.branchCodeTemp = branch.branchName;
		  $scope.chln.branchCode = branch.branchCode;
		  $('div#branchCodeDB').dialog('close');
		  $scope.BranchCodeDBFlag=true;
	  }

	  $scope.savePayAt = function(branch){
		  $scope.payOnCode = branch.branchFaCode;
		  $scope.chln.chlnPayAt = branch.branchCode;
		  $('div#payAtDB').dialog('close');;
		  $scope.PayAtDBFlag=true;
	  }

	  $scope.saveFrmStnCode = function(station){
		  $scope.frmStationCode = station.stnName;
		  $scope.chln.chlnFromStn = station.stnCode;
		  $('div#challanFromStationDB').dialog('close');
		  $scope.ChallanFromStationDBFlag=true;
	  }

	  $scope.saveToStnCode = function(station){
		  $scope.toStationCode = station.stnName;
		  $scope.chln.chlnToStn = station.stnCode;
		  $('div#challanToStationDB').dialog('close');
		  $scope.ChdPerStateCodeDBFlag=true;
	  }



	  $scope.saveVehicleType = function(vt){
		  $scope.chln.chlnVehicleType = vt.vtVehicleType;
		  $('div#vehicleTypeDB').dialog('close');
		  $scope.VehicleTypeDBFlag=true;
		  $scope.chlnRRNo($scope.vtList);
	  }

	  $scope.saveBrokerCode = function(brk){
		 /* $scope.chdBrCodeTemp = brk.brkName;
		  $scope.chd.chdBrCode= brk.brkCode;*/
		  $scope.chdBrCodeTemp = brk.name;
		  $scope.chd.chdBrCode= brk.code;
		  $('div#chdBrCodeDB').dialog('close');
		  $scope.ChdBrCodeDBFlag=true;
		  $scope.getMobileListBk($scope.chd.chdBrCode);
		  $('#chdBrMobNo').removeAttr("disabled");
		  $('#openMobbrk').removeAttr("disabled");
	  }

	  $scope.saveOwnerCode = function(own){
		  /*$scope.chdOwnCodeTemp = own.ownName;
		  $scope.chd.chdOwnCode= own.ownCode;*/
		  $scope.chdOwnCodeTemp = own.name;
		  $scope.chd.chdOwnCode= own.code;
		  $('div#chdOwnCodeDB').dialog('close');
		  $scope.ChdOwnCodeDBFlag=true;
		  $scope.getMobileListOw($scope.chd.chdOwnCode);
		  $('#chdOwnMobNo').removeAttr("disabled");
		  $('#openMobown').removeAttr("disabled");
	  }

	  $scope.savePanIssueStateCode = function(state){
		  $scope.chd.chdPanIssueSt= state.stateCode;
		  $('div#chdPanIssueStDB').dialog('close');
		  $scope.ChdPanIssueStDBFlag=true;
	  }

	  $scope.savePerStateCode = function(state){
		  $scope.chdPerStateTemp = state.stateName;
		  $scope.chd.chdPerState= state.stateCode;
		  $('div#chdPerStateCodeDB').dialog('close');
		  $scope.PerStateCodeFlag1=true;
	  }

	  $scope.saveBrMobNo = function(mobile){
		  $scope.chd.chdBrMobNo = mobile;
		  $('div#chdBrMobNoDB').dialog('close');
		  $scope.ChdBrMobNoDBFlag=true;
	  }

	  $scope.saveOwnMobNo = function(mobile){
		  $scope.chd.chdOwnMobNo= mobile;
		  $('div#chdOwnMobNoDB').dialog('close');
		  $scope.ChdOwnMobNoDBFlag=true;
	  }

	  $scope.saveChallanCode = function(challan){		  
		  console.log("enter into saveChallanCode function----"+challan);
		  $scope.chln.chlnCode=challan;		  
		  $('div#ChallanCodeDB').dialog('close');
		  $scope.ChallanCodeDBFlag=true;
		  autoCode = $scope.chln.chlnCode;
		  $scope.chd.chdChlnCode = autoCode;
		  $('#acd').removeAttr("disabled");	  
		  var response = $http.post($scope.projectName+'/getPendingChallanDetail',challan);
		  response.success(function(data, status, headers, config) {
			  if(data.status === "success"){
				  
				  var pending = data.isPending;				  
				  if(pending === true){
					  $scope.finalSubmit = false;
					  var pendingList = data.pendingCnmt;
					  var msg = "";
					  for(var i=0; i<pendingList.length; i++){
						  msg = msg+pendingList[i]+", ";
					  }
					  msg = msg.substring(0, msg.length-2);
					  $scope.successToast("First Verify Pending Cnmt : "+msg);
					  $scope.finalSubmit = false;
					  return;
				  }else{
					  $scope.finalSubmit = true;
				  }
				  
				  $scope.chlnApp = data.challanApp;
				  $scope.cnmtChlnApp = data.cnmtChlnAppList;
				  $scope.chdApp = data.challnDTApp;
				  // Challan				  
				  $scope.frmStationCode = $scope.chlnApp.chlnFromStnName;
				  $scope.chln.chlnFromStn = $scope.chlnApp.chlnFromStnCode;				  
				  $scope.toStationCode = $scope.chlnApp.chlnToStnName;
				  $scope.chln.chlnToStn = $scope.chlnApp.chlnToStnCode;
				  $scope.chln.chlnEmpCode = $scope.chlnApp.chlnEmpCode;
				  $scope.empTempCode = $scope.chln.chlnEmpCode; 
				  $scope.chln.chlnNoOfPkg = parseFloat($scope.chlnApp.chlnNoOfPkg);
				  $scope.chln.chlnLryRate = parseFloat($scope.chlnApp.chlnLryRate);
				  $scope.chlnLryRatePer = $scope.chlnApp.chlnLryType;
				  $scope.chln.chlnChgWt = parseFloat($scope.chlnApp.chlnChgWt);
				  $scope.chlnChgWtPer = $scope.chlnApp.chlnChgWtType;
				  $scope.chln.chlnFreight = parseFloat($scope.chlnApp.chlnFreight);
				  $scope.chln.chlnLoadingAmt = parseFloat($scope.chlnApp.chlnLoadingAmount);
				  $scope.chln.chlnExtra = parseFloat($scope.chlnApp.chlnExtra);
				  $scope.chln.chlnStatisticalChg = parseFloat($scope.chlnApp.chlnStatisticalChg);
				  $scope.chln.chlnTdsAmt = parseFloat($scope.chlnApp.chlnTdsAmt);
				  $scope.chln.chlnTotalFreight = parseFloat($scope.chlnApp.chlnTotalFreight);
				  $scope.chln.chlnAdvance = parseFloat($scope.chlnApp.chlnAdvance);				  
				  var adv = ($scope.chln.chlnAdvance).toString(); 
				  if(adv.length > 0)
					  $('#chlnAdvance').removeAttr("disabled");
				  $scope.chln.chlnBalance = parseFloat($scope.chlnApp.chlnBalance);
				  $scope.payOnCode = $scope.chlnApp.chlnPayAt;
				  $scope.chln.chlnPayAt = $scope.chlnApp.chlnPayAtCode;				  
				  $scope.chln.chlnTimeAllow = parseFloat($scope.chlnApp.chlnTimeAllow);
				  $scope.chln.chlnDt = $scope.chlnApp.chlnDt;
				  $scope.chln.chlnBrRate = parseFloat($scope.chlnApp.chlnBrRate);				 
				  $scope.chln.chlnTotalWt = parseFloat($scope.chlnApp.chlnTotalWt);
				  $scope.chln.chlnWtSlip = $scope.chlnApp.chlnWtSlip;
				  $scope.chln.chlnLryRepDT = $scope.chlnApp.chlnLryRepDT;
				  $scope.chln.chlnVehicleType = $scope.chlnApp.chlnVehicleType;
				  $scope.chln.chlnRrNo = $scope.chlnApp.chlnRrNo;
				  $scope.chln.chlnTrainNo = $scope.chlnApp.chlnTrainNo;
				  var rrNo = ($scope.chln.chlnTrainNo).toString();
				  if(rrNo.length > 0)
					  $('#rrNo').removeAttr("disabled");
				  var trainNo = ($scope.chln.chlnTrainNo);
				  if(trainNo.length > 0)
					  $('#trainNo').removeAttr("disabled");
				  $scope.chln.chlnLryLoadTime = $scope.chlnApp.chlnLryLoadTime;
				  $scope.chln.chlnLryRptTime = $scope.chlnApp.chlnLryRptTime;
				  $scope.chln.chlnLryNo = $scope.chlnApp.chlnLryNo;
				  // Challan Detail
				  $scope.chdBrCodeTemp = $scope.chdApp.chdBrkName;
				  $scope.chd.chdBrCode= $scope.chdApp.chdBrkCode;
				  $scope.brkrcode = $scope.chd.chdBrCode;				  
				  $scope.chdOwnCodeTemp = $scope.chdApp.chdOwnName;
				  $scope.chd.chdOwnCode = $scope.chdApp.chdOwnCode;
				  $scope.ownrcode = $scope.chd.chdOwnCode;				  
				  $scope.chd.chdBrMobNo = $scope.chdApp.chdBrkMob;
				  $scope.chd.chdOwnMobNo = $scope.chdApp.chdOwnMob;
				  $scope.chd.chdDvrName = $scope.chdApp.chdDvrName;
				  $scope.chd.chdDvrMobNo = $scope.chdApp.chdDvrMob;
				  $scope.chd.chdRcNo = $scope.chdApp.chdRcNo;
				  $scope.chd.chdRcIssueDt = $scope.chdApp.chdRcIssueDt;
				  $scope.chd.chdRcValidDt = $scope.chdApp.chdRcValidDt;
				  $scope.chd.chdDlNo = $scope.chdApp.chdDlNo;
				  $scope.chd.chdDlIssueDt = $scope.chdApp.chdDlIssueDt;
				  $scope.chd.chdDlValidDt = $scope.chdApp.chdDlValidDt;
				  $scope.chd.chdPerNo = $scope.chdApp.chdPerNo;
				  $scope.chd.chdPerIssueDt = $scope.chdApp.chdPerIssueDt;
				  $scope.chd.chdPerValidDt = $scope.chdApp.chdPerValidDt;
				  $scope.chdPerStateTemp = $scope.chdApp.chdPerState;
				  $scope.chd.chdFitDocNo = $scope.chdApp.chdFitDocNo;
				  $scope.chd.chdFitDocIssueDt = $scope.chdApp.chdFitDocIssueDt;
				  $scope.chd.chdFitDocValidDt = $scope.chdApp.chdFitDocValidDt;
				  $scope.chd.chdFitDocPlace = $scope.chdApp.chdFitDocPlace;
				  $scope.chd.chdBankFinance = $scope.chdApp.chdBankFinance;
				  $scope.chd.chdPolicyNo = $scope.chdApp.chdPolicyNo;
				  $scope.chd.chdPolicyCom = $scope.chdApp.chdPolicyCom;
				  $scope.chd.chdTransitPassNo = $scope.chdApp.chdTransitPassNo;
				  $scope.chd.chdPanHdrType = $scope.chdApp.chdPassHDRType;
				  
				  $scope.chd.chdPanIssueSt = $scope.chdApp.chdPanIssueSt;
				  // CNMT Detail				  
				  if($scope.cnmtChlnApp.length == 1){
					  $('#cnmtCodeId').removeAttr("disabled");
					  $('#multiCnmtCodeId').attr("disabled","disabled"); 
					  $('#multi').attr("disabled","disabled"); 
					  $('#single').attr("checked","checked");
					  for(var i=0;i < $scope.multiCnmtCodeList.length;i++){
						  $scope.multiCnmtCodeList.splice(i,1);
					  }
					  $scope.multiCnmtCode = "";
					  $scope.pkg = [];
					  $scope.wt = [];
					  $scope.removeAllMultiCnmt();
					  $scope.cnmtList = $scope.cnmtChlnApp;					  
					  $scope.singleCh = true;
					  $scope.multipleCh = false;
					  for(var i=0; i<$scope.cnmtChlnApp.length; i++){
						  var localCnmt = $scope.cnmtChlnApp[i];
						  $scope.pkg[i] = parseFloat(localCnmt.cnmtNoOfPkg);
						  $scope.wt[i] = parseFloat(localCnmt.actualWt);						  
						  $scope.saveCnmt(localCnmt.cnmtCode, localCnmt.cnmtNoOfPkg, localCnmt.actualWt, 'no');
					  }					  
				  }else{					  
					  $('#multiCnmtCodeId').removeAttr("disabled");
					  $('#cnmtCodeId').attr("disabled","disabled"); 
					  $('#single').attr("disabled","disabled");
					  $('#multi').attr("checked","checked");
					  $scope.cnmtCode = "";
					  var singleCnmtData = {};
					  $scope.pkg = [];
					  $scope.wt = [];
					  $scope.fetchMultiCnmtList();
					  $scope.cnmtList = $scope.cnmtChlnApp;
					  $scope.singleCh = false;
					  $scope.multipleCh = true;					  
					  for(var i=0; i<$scope.cnmtChlnApp.length; i++){
						  var localCnmt = $scope.cnmtChlnApp[i];
						  $scope.pkg[i] = parseFloat(localCnmt.cnmtNoOfPkg);
						  $scope.wt[i] = parseFloat(localCnmt.actualWt);						  
						  $scope.saveMultiCnmt(localCnmt.cnmtCode, localCnmt.cnmtNoOfPkg, localCnmt.actualWt,'no');						  
					  }	
					  $scope.chln.chlnNoOfPkg = parseFloat($scope.totalPkg);
					  $scope.chln.chlnTotalWt = parseFloat($scope.totalWt);
				  }
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });		  
	  }

	  $scope.savepistate = function(state){
		  $('div#chdPanIssueStDB1').dialog('close');
		  console.log("enter into savepistate function--->");
		  var response = $http.post($scope.projectName+'/savePISForCD',state);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.state="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.savepscode = function(state){
		  $('div#chdPerStateCodeDB1').dialog('close');
		  console.log("enter into savepscode function--->");
		  var response = $http.post($scope.projectName+'/savePISForCD',state);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.state="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.saveownmob = function(mobile){
		  $('div#chdOwnMobNoDB1').dialog('close');
		  console.log("enter into saveownmob function--->");
		  var response = $http.post($scope.projectName+'/saveOWNMBForCD',mobile);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.mobile="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.savebrmob = function(mobile){
		  $('div#chdBrMobNoDB1').dialog('close');
		  console.log("enter into savebrmob function--->");
		  /*$scope.chd.chdBrMobNo = mobile;*/
		  var response = $http.post($scope.projectName+'/saveOWNMBForCD',mobile);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.mobile="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.submitCD = function(ChallanDetailForm,chd){
		  $scope.CDFlag=true;
		  console.log("enter into next function ChallanDetailForm.$invalid--->"+ChallanDetailForm.$invalid);
		  if(ChallanDetailForm.$invalid){
			  if(ChallanDetailForm.chdBrCode.$invalid){
				  $scope.alertToast("please enter broker code..");
			  }else if(ChallanDetailForm.chdOwnCode.$invalid){
				  $scope.alertToast("please enter owner code..");
			  }else if(ChallanDetailForm.chdBrMobNo.$invalid){
				  $scope.alertToast("please enter Broker's 10 digit mobile no..");
			  }else if(ChallanDetailForm.chdOwnMobNo.$invalid){
				  $scope.alertToast("please enter Owner's 10 digit mobile no..");
			  }else if(ChallanDetailForm.chdDvrName.$invalid){
				  $scope.alertToast("Please enter driver name..");
			  }else if(ChallanDetailForm.chdDvrMobNo.$invalid){
				  $scope.alertToast("please enter driver's 10 digit mobile no..");
			  }else if(ChallanDetailForm.chdDlNo.$invalid){
				  $scope.alertToast("Please enter DL no..");
			  }else if(ChallanDetailForm.chdDlIssueDt.$invalid){
				  $scope.alertToast("Please enter DL issue date..");
			  }else if(ChallanDetailForm.chdDlValidDt.$invalid){
				  $scope.alertToast("Please enter DL valid date..");
			  }else if(ChallanDetailForm.chdPanHdrType.$invalid){
				  $scope.alertToast("Please enter PAN HDR TYpe..");
			  }/*else if(ChallanDetailForm.chdPanNo.$invalid){
				  $scope.alertToast("Please enter 10 digit PAN No..");
			  }*//*else if(ChallanDetailForm.chdPanHdrName.$invalid){
				  $scope.alertToast("Please enter PAN HDR Name..");
			  }*/else{
				  console.log("***********************");
			  }
		  }else{
			  console.log("enter into submitCD function--->");
			 
			  var reqData = {
					  "ownCode" : $scope.chd.chdOwnCode,
					  "brkCode" : $scope.chd.chdBrCode
			  }
			  
			  var response = $http.post($scope.projectName+'/chkOwnBrkPD',reqData);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $('div#chdCodeCb').dialog('close');
					  $('#chlnAdvance').removeAttr("disabled");
					  $scope.cd = true;
				  }else{
					  $scope.alertToast("These Owner/Broker does not have any PanCard");
					  $scope.cd = false;
				}
			  });
			  response.error(function(data, status, headers, config) {
				  $scope.errorToast(data.result);
			  });
		  }
	  } 

	  $scope.calTotalFreight = function(chln){
		  chln.chlnTotalFreight=chln.chlnFreight+chln.chlnLoadingAmt+chln.chlnExtra-chln.chlnStatisticalChg-chln.chlnTdsAmt;
	  }

	  $scope.calculateFreight = function(){
		  var LryRate = 0;
		  var ChgWt = 0;
		  if($scope.chlnLryRatePer === "Ton"){
			  if($scope.chlnChgWtPer === "Ton"){
				  $scope.chln.chlnFreight = $scope.chln.chlnLryRate*$scope.chln.chlnChgWt;	
			  }else if($scope.chlnChgWtPer === "Kg"){
				  LryRate = $scope.chln.chlnLryRate/1000;
				  $scope.chln.chlnFreight = LryRate * $scope.chln.chlnChgWt;
			  }
		  }else if($scope.chlnLryRatePer === "Kg"){
			  if($scope.chlnChgWtPer === "Ton"){
				  LryRate = $scope.chln.chlnLryRate * 1000;
				  $scope.chln.chlnFreight = LryRate*$scope.chln.chlnChgWt;
			  }else if($scope.chlnChgWtPer === "Kg"){
				  $scope.chln.chlnFreight = $scope.chln.chlnLryRate * $scope.chln.chlnChgWt;
			  }
		  }
	  }

	  $scope.checkAdvance =  function(){
		  console.log("enter into checkAdvance funciton");
		  //chlnAdvc=0.95*parseInt($scope.chln.chlnTotalFreight);
		  chlnAdvc=parseInt($scope.chln.chlnTotalFreight);
		  if($scope.chln.chlnAdvance>chlnAdvc){
			  $scope.chln.chlnAdvance="";
			  $scope.alertToast("You can't enter more than 95% Total Freight");
		  }
	  }

	  $scope.balance = function(chln){
		  chln.chlnBalance=parseInt(chln.chlnTotalFreight)-parseInt(chln.chlnAdvance);
	  }	

	  $scope.chlnRRNo = function(chln){
		  console.log("enter into chlnRRNo fucntion");
		  if($scope.chln.chlnVehicleType === "train"){
			  $('#chlnRrNo').removeAttr("disabled");
			  $('#chlnTrainNo').removeAttr("disabled");
		  }
	  }	

	  $scope.submitChallan = function(chln,cnmtCode,ChallanForm,chd,chlnTotalWtPer){
		  console.log("enter into submitChallan function ChallanForm ="+ChallanForm.$invalid);
		  console.log("selected cnmtCode = "+cnmtCode);
		  console.log("chln.chlnTotalWt ---->"+chln.chlnTotalWt);
		  
		  if($scope.cd === false){
			  //return;
		  }
		  
		  //var cnmtCodeList = [];
		  for(var i=0;i<cnmtCodeList.length;i++){
			  cnmtCodeList.splice(i,1);
		  }		
		 if(angular.isUndefined($scope.chln.chlnLryNo) || $scope.chln.chlnLryNo === "" || $scope.chln.chlnLryNo === null){
			  $scope.alertToast("please select a vehicle");
		 }else{			 
			  if(ChallanForm.$invalid){				   
				  if(ChallanForm.chlnCode.$invalid){
					  $scope.successToast("please enter Challan Code..");
				  }else if(ChallanForm.branchCode.$invalid){
					  $scope.successToast("please enter Branch Code..");
				  }else if(ChallanForm.chlnLryNo1.$invalid){
					  $scope.alertToast("please enter Challan Lorry Prefix..");
				  }else if(ChallanForm.chlnLryNo.$invalid){
					  $scope.alertToast("please enter Challan Lorry No..");
				  }else if(ChallanForm.chlnFromStn.$invalid){
					  $scope.alertToast("Please enter Challan From Station..");
				  }else if(ChallanForm.chlnToStn.$invalid){
					  $scope.alertToast("please enter Challan To Station..");
				  }else if(ChallanForm.chlnEmpCode.$invalid){
					  $scope.alertToast("please enter Employee Code");
				  }else if(ChallanForm.chlnLryRate.$invalid){
					  $scope.alertToast("Please enter Lorry Rate..");
				  }else if(ChallanForm.chlnChgWt.$invalid){
					  $scope.alertToast("Please enter Charge Weight..");
				  }else if(ChallanForm.chlnChgWt1.$invalid){
					  $scope.alertToast("Please enter per Ton/Kg for Charge Weight..");
				  }else if(ChallanForm.chlnNoOfPkg.$invalid){
					  $scope.alertToast("Please enter No.Of Packages ..");
				  }else if(ChallanForm.chlnTotalWt.$invalid){
					  $scope.alertToast("Please enter Total Weight..");
				  }else if(ChallanForm.chlnTotalWt1.$invalid){
					  $scope.alertToast("Please enter per Ton/Kg for Total Weight..");
				  }else if(ChallanForm.chlnFreight.$invalid){
					  $scope.alertToast("Please enter Freight..");
				  }else if(ChallanForm.chlnLoadingAmt.$invalid){
					  $scope.alertToast("Please enter Loading Amount..");
				  }else if(ChallanForm.chlnExtra.$invalid){
					  $scope.alertToast("Please enter Extra..");
				  }else if(ChallanForm.chlnAdvance.$invalid){
					  $scope.alertToast("Please enter Advance..");
				  }else if(ChallanForm.chlnPayAt.$invalid){
					  $scope.alertToast("Please enter Pay At..");
				  }else if(ChallanForm.chlnDt.$invalid){
					  $scope.alertToast("Please enter Challan Date..");
				  }else if(ChallanForm.chlnTimeAllow.$invalid){
					  $scope.alertToast("Please enter Time Allowed..");
				  }else if(ChallanForm.chlnDt.$invalid){
					  $scope.alertToast("Please enter Challan Date..");
				  }else if(ChallanForm.chlnBrRate.$invalid){
					  $scope.alertToast("Please enter BR Rate..");
				  }else if(ChallanForm.chlnWtSlip.$invalid){
					  $scope.alertToast("Please enter Weight Slip..");
				  }else if(ChallanForm.chlnVehicleType.$invalid){
					  $scope.alertToast("Please enter Vehicle Type..");
				  }else if(ChallanForm.chlnStatisticalChg.$invalid){
					  $scope.alertToast("Please enter Statistical Charge..");
				  }else if(ChallanForm.chlnTdsAmt.$invalid){
					  $scope.alertToast("Please enter TDS Amount..");
				  }else if(ChallanForm.chlnLryLoadTime.$invalid){
					  $scope.alertToast("Please enter Lorry Load Time..");
				  }else if(ChallanForm.chlnLryRepDT.$invalid){
					  $scope.alertToast("Please enter Lorry Reporting Date..");
				  }else if(ChallanForm.chlnLryRptTime.$invalid){
					  $scope.alertToast("Please enter Lorry Reporting Time..");
				  }

			  }else{ 
				  if($scope.CDFlag===false){
					  console.log("here v are -------------------------------->");
					  if(chd.chdChlnCode===null || chd.chdChlnCode ==="" || angular.isUndefined(chd.chdChlnCode) || chd.chdBrCode=== null || chd.chdBrCode==="" || angular.isUndefined(chd.chdBrCode) || chd.chdOwnCode===null || chd.chdOwnCode==="" || angular.isUndefined(chd.chdOwnCode)
							  || chd.chdBrMobNo===null || chd.chdBrMobNo==="" || angular.isUndefined(chd.chdBrMobNo) || chd.chdOwnMobNo===null || chd.chdOwnMobNo==="" || angular.isUndefined(chd.chdOwnMobNo) || chd.chdDvrName===null || chd.chdDvrName==="" || angular.isUndefined(chd.chdDvrName)
							  || chd.chdDvrMobNo===null || chd.chdDvrMobNo==="" || angular.isUndefined(chd.chdDvrMobNo) ||  chd.chdDlNo===null || chd.chdDlNo==="" || angular.isUndefined(chd.chdDlNo) || chd.chdDlIssueDt===null || chd.chdDlIssueDt==="" || angular.isUndefined(chd.chdDlIssueDt)
							  || chd.chdDlValidDt===null || chd.chdDlValidDt==="" || angular.isUndefined(chd.chdDlValidDt)
							  || chd.chdPanHdrType===null || chd.chdPanHdrType==="" || angular.isUndefined(chd.chdPanHdrType) || chd.chdPanNo===null || chd.chdPanNo==="" || angular.isUndefined(chd.chdPanNo)
							  || chd.chdPanHdrName===null || chd.chdPanHdrName==="" || angular.isUndefined(chd.chdPanHdrName))
					  {
						  $scope.alertToast("please Enter challan details");
					  }
				  }else{	
					  console.log("enter into submitChallan function ChallanForm ="+ChallanForm.$invalid);
					  //var cnmtCodeList = {};
					  console.log("length of $scope.multiCnmtCodeList.length = "+$scope.multiCnmtCodeList.length);
					  if($scope.multiCnmtCodeList.length > 1){
						  //cnmtCodeList = $scope.multiCnmtCodeList;
						  console.log("multiple cnmt selected");
						  var cnmtActualWt = 0;
						  for(var i=0;i<$scope.multiCnmtCodeList.length;i++){
							  for(var j=0;j<$scope.cnmtList.length;j++){
								  if($scope.cnmtList[j].cnmtCode === $scope.multiCnmtCodeList[i]){
									  cnmtActualWt =  parseInt(cnmtActualWt) + parseInt($scope.cnmtList[j].cnmtActualWt);
								  }	
							  }		
						  }
						  console.log("cnmtActualWt ======> "+cnmtActualWt);
						  console.log("chlnTotalWtPer ----> "+chlnTotalWtPer);
						  
						  var tempChlnTotWt = chln.chlnTotalWt;
						  if(chlnTotalWtPer === "Ton"){
							  //$scope.chlnTotalWtPer = "Kg";
							  tempChlnTotWt =(chln.chlnTotalWt*$scope.kgInTon);
							  //$scope.chln.chlnTotalWt = chln.chlnTotalWt;
							  console.log("%%%%%%%%%%%%%%%%%"+chln.chlnTotalWt);
						  }
						  if(parseFloat(cnmtActualWt) > parseFloat(tempChlnTotWt)){
							  $scope.alertToast("Cnmts Actual weight("+ cnmtActualWt +"kg) must be less than Challan total weight");
						  }else{
							  cnmtCodeList = $scope.multiCnmtCodeList;
							  $scope.viewChallanDetailsFlag=false;
							  $('div#viewChallanDetailsDB').dialog({
								  autoOpen: false,
								  modal:true,
								  title: "Customer Info",
								  show: UDShow,
								  hide: UDHide,
								  position: UDPos,
								  resizable: false,
								  draggable: true,
								  close: function(event, ui) { 
									  $(this).dialog('destroy') ;
									  $(this).hide();
								  }
							  });

							  $('div#viewChallanDetailsDB').dialog('open');
						  }
					  }else{
						  console.log("----------->cnmtCode = "+cnmtCode);
						  if(angular.isUndefined(cnmtCode) || $scope.cnmtCode === "" || cnmtCode === null){
							  console.log("single cnmt selected");
							  $scope.alertToast("please select a cnmt code");
						  }else{
							  cnmtCodeList.push(cnmtCode);
							  var cnmtActualWt = 0;
							  for(var i=0;i<cnmtCodeList.length;i++){
								  for(var j=0;j<$scope.cnmtList.length;j++){
									  if($scope.cnmtList[j].cnmtCode === cnmtCodeList[i]){
										  cnmtActualWt =  parseInt(cnmtActualWt) + parseInt($scope.cnmtList[j].cnmtActualWt);
									  }	
								  }		
							  }

							  console.log("cnmtActualWt ======> "+cnmtActualWt);
							  console.log("chlnTotalWtPer ----> "+chlnTotalWtPer);
							  
							  var tempChlnTotWt = chln.chlnTotalWt;
							  if(chlnTotalWtPer === "Ton"){
								  //$scope.chlnTotalWtPer = "Kg";
								  console.log("#############"+chln.chlnTotalWt);
								  tempChlnTotWt = chln.chlnTotalWt * $scope.kgInTon;
								  //$scope.chln.chlnTotalWt = chln.chlnTotalWt;
								  console.log("%%%%%%%%%%%%%%%%%"+chln.chlnTotalWt);
							  }
							  
							  console.log("cnmtActual weight = "+cnmtActualWt);
							  console.log("tempChlnTotWt weight = "+tempChlnTotWt);
							  
							  if(parseFloat(cnmtActualWt) > parseFloat(tempChlnTotWt)){
								  $scope.alertToast("Cnmts Actual weight must be less than Challan total weight");
							  }else{
								  $scope.viewChallanDetailsFlag=false;
								  $('div#viewChallanDetailsDB').dialog({
									  autoOpen: false,
									  modal:true,
									  title: "Customer Info",
									  show: UDShow,
									  hide: UDHide,
									  position: UDPos,
									  resizable: false,
									  draggable: true,
									  close: function(event, ui) { 
										  $(this).dialog('destroy') ;
										  $(this).hide();
									  }
								  });

								  $('div#viewChallanDetailsDB').dialog('open');
							  }

						  }	
					  }
				  }
			  } 
		 }
	
	  }
	  $scope.saveChallan = function(challan,chlnLryRatePer,chlnChgWtPer,chlnTotalWtPer,chln){
		  console.log("enter into saveChallan function");
		  console.log("$scope.chln.chlnTotalWt ---> "+$scope.chln.chlnTotalWt);
		  if(chlnLryRatePer === "Ton"){ 
			  $scope.chln.chlnLryRate =(chln.chlnLryRate/$scope.kgInTon);
			  console.log("***********"+chln.chlnLryRate);
		  }
		  if(chlnChgWtPer === "Ton"){
			  chlnChgWtPer = "Kg";
			  $scope.chln.chlnChgWt =(chln.chlnChgWt*$scope.kgInTon);
			  console.log("############"+chln.chlnChgWt);
		  }
		  if(chlnTotalWtPer === "Ton"){
			  $scope.chln.chlnTotalWt =(chln.chlnTotalWt*$scope.kgInTon);
			  console.log("%%%%%%%%%%%%%%%%%"+chln.chlnTotalWt);
		  }
		  //$scope.chln.chlnLryNo = $scope.chlnLryNoPer+" "+$scope.chln.chlnLryNo;
		  
		  if($scope.multiCnmtCodeList.length > 1){
			  var Cnmt_Challan = {
					  "challan"      		: $scope.chln,
					  //"cnmtCodeList" 		: cnmtCodeList,
					  "cnmtCodeList" 		: $scope.multiCnmtCodeList,
					  "challanDetail"   	: $scope.chd
			  };
		  }else{
			  var cnmtCodeList = [];
			  console.log("single cnmt = "+singleCnmtData.cnmt);
			  console.log("No of pkg = "+singleCnmtData.pkg);
			  console.log("weight = "+singleCnmtData.wt);
			  cnmtCodeList.push(singleCnmtData);
			  var Cnmt_Challan = {
					  "challan"      		: $scope.chln,
					  "cnmtCodeList" 		: cnmtCodeList,
					  "challanDetail"   	: $scope.chd
			  };
		  }		
		  $('#saveBtnId').attr("disabled","disabled");
		  var response = $http.post($scope.projectName+'/varifyChallan',Cnmt_Challan);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $('#saveBtnId').removeAttr("disabled");
				  
				  $scope.viewChallanDetailsFlag = true;
				  $('div#viewChallanDetailsDB').dialog('close');
				  $scope.successToast(data.result);
				  //$scope.multiCnmtCodeList = {};
				  
				  for(var i=0;i<$scope.multiCnmtCodeList.length;i++){
					  $scope.multiCnmtCodeList.splice(i,1);
				  }
				  $scope.state="";
			/*	  $scope.chln.chlnCode= "";
				  $scope.chln.branchCode= "";
				  $scope.chln.chlnLryNo= "";
				  $scope.chln.chlnFromStn= "";
				  $scope.chln.chlnToStn= "";
				  $scope.chln.chlnEmpCode= "";
				  $scope.chln.chlnLryRate= "";
				  $scope.chln.chlnNoOfPkg= "";
				  $scope.chln.chlnChgWt= "";
				  $scope.chln.chlnFreight= "";
				  $scope.chln.chlnLoadingAmt= "";
				  $scope.chln.chlnTotalWt= "";
				  $scope.chln.chlnExtra= "";
				  $scope.chln.chlnTotalFreight= "";
				  $scope.chln.chlnWtSlip= "";
				  $scope.chln.chlnVehicleType= "";
				  $scope.chln.chlnAdvance= "";
				  $scope.chln.chlnBalance= "";
				  $scope.chln.chlnPayAt= "";
				  $scope.chln.chlnTimeAllow= "";
				  $scope.chln.chlnDt= "";
				  $scope.chln.chlnBrRate= "";
				  $scope.chln.chlnStatisticalChg= "";
				  $scope.chln.chlnTdsAmt= "";
				  $scope.chln.chlnRrNo= "";
				  $scope.chln.chlnTrainNo= "";
				  $scope.chln.chlnLryLoadTime= "";
				  $scope.chln.chlnLryRptTime= "";
				  $scope.chln.chlnLryRepDT= "";*/
				  $scope.chln = {};
				  $scope.chd = {};
				  $scope.cnmtCode = "";
				  $scope.chlnLryNoPer = "";
				  $scope.branchCodeTemp = "";
				  $scope.frmStationCode = "";
				  $scope.toStationCode = "";
				  $scope.empTempCode = "";
				  var singleCnmtData = {};
				  var cnmtCodeList = [];
				  $scope.multiCnmtCodeList = [];
				  $scope.successToast(data.result);
				  $scope.getChallanCodes();
				  
				  /*for(var i=0 ; i < cnmtCodeList.length ; i++){
					cnmtCodeList.splice(i,1);
				}*/
			  }else{
				  console.log(data.result);
				  $scope.successToast(data.msg);				  
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.closeViewChallanDetailsDB = function(){
		  $('div#viewChallanDetailsDB').dialog('close');
	  }


	  $scope.saveCnmt = function(cnmt,pkg,wt, type){
		  console.log("enter into saveCnmt fucntion");
		  $scope.cnmtCode = cnmt;
		  if(angular.isUndefined(pkg) || angular.isUndefined(wt)){
			  $scope.alertToast("Please enter No. of pkg and weight");
		  }else{
			  singleCnmtData = {
					  "cnmt" : cnmt,
					  "pkg"  : pkg,
					  "wt"   : wt
			  };
			  
			  var response = $http.post($scope.projectName+'/getTransitDay',$scope.cnmtCode);
			  response.success(function(data, status, headers, config){
				  console.log("transit day from server ===>"+data.transitDay);
				  console.log("mc = "+data.mc);
				  if(data.result==="success"){
					  console.log(JSON.stringify(data));
					  if(data.mc === "true"){
						  $scope.chln.chlnTimeAllow = "";
					  }else{
						  $scope.chln.chlnTimeAllow = data.transitDay;
					  }
					  $scope.chln.chlnFromStn = data.fromStation;
					  if(type != "no"){
						  $('div#cnmtDB').dialog('close');
						  $scope.CnmtDBFlag=true;
					  }
				  }else{
					  console.log(data);   
				  }	
			  });
			  response.error(function(data, status, headers, config) {
				  $scope.errorToast(data.result);
			  });
		  }
	  }

	  $scope.saveMultiCnmt = function(multiCnmt,pkg,wt, type){
		  console.log("enter into saveMultiCnmt fucntion");
		  //$scope.multiCnmtCodeList.push(multiCnmt.cnmtCode);		  
		  if(angular.isUndefined(pkg) || angular.isUndefined(wt)){
			  $scope.alertToast("Please enter No. of pkg and weight");
		  }else{
			  var req = {
					  "cnmt" : multiCnmt,
					  "pkg"  : pkg,
					  "wt"   : wt
			  };
			  
			  $scope.totalPkg = parseFloat($scope.totalPkg) + parseFloat(pkg);
			  $scope.totalWt = parseFloat($scope.totalWt) + parseFloat(wt);			  
			  $scope.multiCnmtCodeList.push(req);
			  $scope.multiCnmtCode = multiCnmt;			  
			  if(type != "no"){
				  $('div#multiCnmtDB').dialog('close');
				  $scope.MultiCnmtDBFlag = true;
			  }
			  //$scope.addMultiCnmt(multiCnmt.cnmtCode);			  
			  $scope.addMultiCnmt(req);
		  }
		  
	  }

	  $scope.singleCnmt = function(){
		  console.log("enter into singleCnmt fucntion");
		  $('#cnmtCodeId').removeAttr("disabled");
		  $('#multiCnmtCodeId').attr("disabled","disabled"); 
		  for(var i=0;i < $scope.multiCnmtCodeList.length;i++){
			  $scope.multiCnmtCodeList.splice(i,1);
		  }
		  $scope.multiCnmtCode = "";
		  $scope.pkg = [];
		  $scope.wt = [];
		  $scope.removeAllMultiCnmt();
	  }


	  $scope.multipleCnmt = function(){
		  console.log("enter into multipleCnmt fucntion");
		  $('#multiCnmtCodeId').removeAttr("disabled");
		  $('#cnmtCodeId').attr("disabled","disabled"); 
		  $scope.cnmtCode = "";
		  var singleCnmtData = {};
		  $scope.pkg = [];
		  $scope.wt = [];
		  $scope.fetchMultiCnmtList();
	  }

	  $scope.fetchMultiCnmtList = function(){
		  console.log("enter into fetchMultiCnmtList fucntion");
		  var response = $http.get($scope.projectName+'/fetchMultiCnmtList');
		  response.success(function(data, status, headers, config){
			  $scope.multiCnmtCodeList = data.list;
			  var noOfPkg = 0;
			  var wt = 0;
			  for(var i=0; i<data.list.length; i++){
				  noOfPkg = parseFloat(data.list[i].pkg)+noOfPkg;
				  wt = parseFloat(data.list[i].wt) + wt;
			  }
			  $scope.chln.chlnNoOfPkg = noOfPkg;
			  $scope.chln.chlnTotalWt = wt;
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.addMultiCnmt = function(req){
		  console.log("enter into addMultiCnmt fucntion");
		  var count = 0;
		  for(var i=0;i<$scope.multiCnmtCodeList.length;i++){
			  if($scope.multiCnmtCodeList[i].cnmt === req.cnmt){
				  //$scope.multiCnmtCodeList.splice(i,1);
				  //console.log("already selected");
				  count = count + 1;
			  }
			  if(count > 1){
				  $scope.multiCnmtCodeList.splice(i,1);
			  }
		  }
		  if(count > 1){
			  $scope.alertToast("already exist");
		  }else{
			  console.log("$scope.multiCnmtCodeList = "+$scope.multiCnmtCodeList);
			  var response = $http.post($scope.projectName+'/addMultiCnmt',req);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success")
					  $scope.fetchMultiCnmtList(); 	
			  });
			  response.error(function(data, status, headers, config) {
				  $scope.errorToast(data.result);
			  });
		  }
	  }

	  $scope.removeMultiCnmt = function(code) {
		  console.log("enter into removeMultiCnmt function ="+code);
		  var response = $http.post($scope.projectName+'/removeMultiCnmt',code);
		  response.success(function(data, status, headers, config) {
			  if(data.result === "success")
				  $scope.fetchMultiCnmtList();
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.removeAllMultiCnmt = function() {
		  console.log("enter into removeAllMultiCnmt fucntion");
		  var response = $http.post($scope.projectName+'/removeAllMultiCnmt');
		  response.success(function(data, status, headers, config) {
			  if(data.result === "success")
				  $scope.multiCnmtCode = "";
			  	  $scope.fetchMultiCnmtList();
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.uploadChallanImage = function(){
		  console.log("enter into uploadChallanImage function");
		  var file = $scope.challanImage;
		  console.log("size of file ----->"+file.size);
		  if(angular.isUndefined(file) || file === null || file === ""){
			  $scope.alertToast("First choose the file----->");
		  }else if(file.size > $scope.maxFileSize){
			  $scope.alertToast("image size must be less than or equal to 1mb");
		  }else{
			  console.log('file is ' + JSON.stringify(file));
			  var uploadUrl = $scope.projectName+"/uploadChallanImage";
			  FileUploadService.uploadFileToUrl(file, uploadUrl);
			  console.log("file save on server");
		  }
	  }

	  $scope.getChallanCodes = function(){
		  console.log("getChallanCodes------>");
		  var response = $http.post($scope.projectName+'/getPendingChallanCode');
		  response.success(function(data, status, headers, config){
			  if(data.status === "success"){
				  $scope.challanCodesList = data.chlnCodeList;
				  $scope.chln.branchCode = data.branchCode;
				  $scope.branchCodeTemp = data.branchName;				 
			  }else{
				  $scope.alertToast("you don't have any challan");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }
	  
	  
	  $scope.getVehicleMstr = function(){
		console.log("enter into getVehicleMstr function");
		var response = $http.post($scope.projectName+'/getVehicleMstrFC');
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.vehList = data.list;
				 console.log("size of $scope.vehList = "+$scope.vehList.length);				
			 }else{
				 $scope.alertToast("Vehicles are not avaliable");
				 console.log("error in fetching data from getVehicleMstr");
			 }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }
	  
	  
	  $scope.openVehicleDB = function(){
		  $scope.getVehicleMstr();
		  console.log("enter into openVehicleDB funciton");
		  $scope.vehDBFlag=false;
		  $('div#vehDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Select Vehicle",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
				  $scope.vehDBFlag=true;
			  }
		  });

		  $('div#vehDB').dialog('open');
	  }
	  
	  
	  $scope.selectVeh = function(selVeh){
		  console.log("enter into selectVeh function");
		  var response = $http.post($scope.projectName+'/selectVehFC',selVeh);
		  response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('div#vehDB').dialog('close');
				$scope.vehM = data.vehM;
				$scope.chln.chlnLryNo = $scope.vehM.vvRcNo;
				$scope.chdOwnCodeTemp = data.ownName;
				$scope.chd.chdOwnCode = data.ownCode;
				$scope.chdBrCodeTemp = data.brkName;
				$scope.chd.chdBrCode= data.brkCode;
				$scope.brkrcode = $scope.chd.chdBrCode; 
				$scope.ownrcode = $scope.chd.chdOwnCode;
				$scope.mobListBk = [];
				$scope.mobListBk = data.brkMobList;
				if($scope.mobListBk.length > 0){
					$scope.chd.chdBrMobNo = $scope.mobListBk[0];
				}else{
					$('#openMobbrk').removeAttr("disabled");
				}
				$scope.mobListOw = []; 
				$scope.mobListOw = data.ownMobList;
				if($scope.mobListOw.length > 0){
					$scope.chd.chdOwnMobNo = $scope.mobListOw[0];
				}else{
					$('#openMobown').removeAttr("disabled");
				}
				
				$scope.chd.chdDvrName       = $scope.vehM.vvDriverName;
				console.log("$scope.vehM.vvDriverMobNo = "+$scope.vehM.vvDriverMobNo);
				$scope.chd.chdDvrMobNo      = $scope.vehM.vvDriverMobNo;
				$scope.chd.chdRcNo          = $scope.vehM.vvRcNo;
				$scope.chd.chdRcIssueDt     = $scope.vehM.vvRcIssueDt;
				$scope.chd.chdRcValidDt     = $scope.vehM.vvRcValidDt;
				$scope.chd.chdDlNo          = $scope.vehM.vvDriverDLNo;
				$scope.chd.chdDlIssueDt     = $scope.vehM.vvDriverDLIssueDt;
				$scope.chd.chdDlValidDt     = $scope.vehM.vvDriverDLValidDt;
				$scope.chd.chdPerNo         = $scope.vehM.vvPerNo;
				$scope.chd.chdPerIssueDt    = $scope.vehM.vvPerIssueDt;
				$scope.chd.chdPerValidDt    = $scope.vehM.vvPerValidDt;
				$scope.chd.chdFitDocNo      = $scope.vehM.vvFitNo;
				$scope.chd.chdFitDocIssueDt = $scope.vehM.vvFitIssueDt;
				$scope.chd.chdFitDocValidDt = $scope.vehM.vvFitValidDt;
				$scope.chd.chdFitDocPlace   = $scope.vehM.vvFitState;
				$scope.chd.chdBankFinance   = $scope.vehM.vvFinanceBankName;
				$scope.chd.chdPolicyNo      = $scope.vehM.vvPolicyNo;
				$scope.chd.chdPolicyCom     = $scope.vehM.vvPolicyComp;
				$scope.chd.chdTransitPassNo = $scope.vehM.vvTransitPassNo;
				
				
			}else{
				$scope.alertToast("error in fetching data form selectVeh");
			}
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }
	  
	  

	  $scope.checkdate = function(){
		  console.log("enter into checkdate--------->");
		  console.log("challan date entered"+$scope.chln.chlnDt);
		  var today = new Date();
		  var currentDate = $filter('date')(today, 'yyyy-MM-dd')
		  console.log("challan date today"+currentDate);
		  if($scope.chln.chlnDt <= currentDate){
			  $scope.alertToast("correct date");
		  }else{
			  $scope.chln.chlnDt = "";
			  $scope.alertToast("entered date should be less than current date");
		  }

	  }
	  
	  
	  
	  $scope.saveActDt = function(){
		  console.log("enter into saveActDt funciton  = "+$scope.actDate);
		  var newDate = new Date($scope.actDate);
		 // $scope.actDate = newDate;
		  
		  var response = $http.post($scope.projectName+'/saveDemoDt',newDate);
		  response.success(function(data, status, headers, config){
			 
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
		  
	  }
	  
	  if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		  $scope.getCnmtList();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
	 
}]);