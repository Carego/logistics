package com.mylogistics.services.mail;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class MailScheduler {

//	"0 0 * * * *" = the top of every hour of every day.
//	"*/10 * * * * *" = every ten seconds.
//	"0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
//	"0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
//	"0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
//	"0 0 0 25 12 ?" = every Christmas Day at midnight
	
	@Scheduled(cron="0 0/1 12-13 * * *", zone="Asia/Calcutta")
	public void testMethod(){
		//System.out.println("scheduled method running: "+new Date());
		
	}
}
