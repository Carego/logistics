package com.mylogistics.constants;

public class ContractRightsCNTS {
	
	public static final String USER_BRANCH_CODE="bCode";
	
	public static final String USER_CODE="userCode";
	
	public static final String CR_CONT_CODE="contCode";
	
	public static final String CR_CONT_BILL_BASIS="crContBillBasis";
	
	public static final String CR_ID="crId";

}
