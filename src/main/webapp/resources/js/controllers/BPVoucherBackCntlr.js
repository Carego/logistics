'use strict';

var app = angular.module('application');

app.controller('BPVoucherBackCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.vs = {};
	$scope.bpServ = {};
	$scope.bpServ.customer = {};
	$scope.bpServ.faBProm = {};
	
	$scope.bpVList = [];	
	$scope.branchList = [];
	$scope.chqList = [];
	
	var fbpAmtTemp;
	var fbpTurnOverTemp;
	var fbpExpTurnTemp;
	$scope.fbpAmtSum=0;
	$scope.fbpTurnOverSum=0;
	$scope.fbpExpTurnSum=0;
	$scope.bankCodeDBFlag = true;
	$scope.tdsCodeFlag = true;
	$scope.custCodeDBFlag = true;
	$scope.bpVoucherDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.custBranchDBFlag = true;
	$scope.selChqDBFlag = true;
	
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		var response = $http.post($scope.projectName+'/getVDetFrBPVBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.BP_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getAllCust();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getAllCust = function(){
		console.log("enter into getAllCust function");
		var response = $http.post($scope.projectName+'/getAllCustFrBPV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custList = data.list; 
				$scope.getTdsCode();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	} 
	
	
	
	$scope.getTdsCode = function(){
		console.log("enter into getTdsCode function");
		var response = $http.post($scope.projectName+'/getTdsCode');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.fsMTdsList = data.list;
				$scope.fetchBPDetails();
			}else{
				$scope.fetchBPDetails();
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.payTo = "";
			$('#payToId').attr("disabled","disabled");
			$scope.empCode = "";
			$('#custCodeId').attr("disabled","disabled");	
		}else if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			
			$('#payToId').removeAttr("disabled");
			$('#custCodeId').removeAttr("disabled");
		}else{
			console.log("invaid entry");
		}
	}	
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	
	$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
		
		if($scope.vs.payBy === 'Q'){
			
			var chqDet = {
					"bankCode" : bankCode,
					"CType"    : $scope.vs.chequeType
			};
			
			var response = $http.post($scope.projectName+'/getChequeNoFrBPV',chqDet);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
					$('#chequeNoId').removeAttr("disabled");
					$('#payToId').removeAttr("disabled");
					$('#custCodeId').removeAttr("disabled");
					$scope.chqList = data.list;
					$scope.vs.chequeLeaves = data.chequeLeaves;
					$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
				}else{
					console.log("Error in bringing data from getChequeNo");
					$scope.alertToast("Branch does not have any "+$scope.vs.chequeType+" cheque of "+bankCode+" bank");
				}

			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}		
	}
	
	
	 $scope.openChqDB = function(){
		 console.log("enter into openChqDB function");
		    $scope.selChqDBFlag = false;
	    	$('div#selChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select Cheque No.",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selChqDBFlag = true;
			    }
				});
			$('div#selChqDB').dialog('open');
	 }
	
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function");
		 $scope.vs.chequeLeaves = chq;
		 $('div#selChqDB').dialog('close');
	 }
	
	
	$scope.selectTds = function(){
		console.log("enter into selectTds function");
		$scope.tdsCodeFlag = false;
		$('div#tdsCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#tdsCodeDB').dialog('open');
	}
	
	
	$scope.saveTdsCode = function(fsMTds){
		console.log("enter into saveTdsCode function");
		$scope.vs.tdsCode = fsMTds.faMfaCode;
		$scope.tdsCodeFlag = true;
		$('div#tdsCodeDB').dialog('close');
	} 
	
	
	$scope.openCustDB = function(){
		console.log("enter into openCustDB function");
		
		$scope.custCodeDBFlag = false;
		$('div#custCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#custCodeDB').dialog('open');
	} 
	
	
	
	$scope.saveCustCode = function(cust){
		console.log("enter into saveCustCode function");
		$scope.branchList = [];
		$scope.custCode = cust.custFaCode;
		$scope.custCodeDBFlag = true;
		$('div#custCodeDB').dialog('close');
		
		var countCustNo = 0;
		 for(var i=0;i<$scope.bpVList.length;i++){
			 if($scope.bpVList[i].customer.custFaCode === cust.custFaCode){
				 countCustNo = countCustNo +1;
			 }
		 }
		 
		 if(countCustNo > 0){
				$scope.alertToast("you already create the voucher of "+cust.custFaCode);
		 }else{
			 	 $scope.bpServ.customer = cust; 
				 $scope.bpVoucherDBFlag = false;
				 $('div#bpVoucherDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Voucher Details for "+cust.custName+" ("+cust.custFaCode+") Business Promotion",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					    }
						});
					
				$('div#bpVoucherDB').dialog('open');
				
				var recIndex = {
					"index" : cust.custId	
				};
				
				
				var response = $http.post($scope.projectName+'/getCustBrFrBPV',recIndex);
				response.success(function(data, status, headers, config){
						if(data.result === "success"){
							$scope.branchList = data.list;
						}else{
							console.log(data);
						}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
		   }
	}
	
	
	$scope.saveCustBrCode = function(branch){
		console.log("enter into saveCustBrCode function");
		$scope.bpServ.faBProm.fbpBrfaCode = branch.branchFaCode;
		$scope.custBranchDBFlag = true;
		$('div#custBranchDB').dialog('close');
	}
	
	
	$scope.submitBPVoucher = function(bpVoucherForm){
		console.log("enter into submitBPVoucher function = "+bpVoucherForm.$invalid);
		if(bpVoucherForm.$invalid){
			 $scope.alertToast("please enter correct details of Business Promotion");
		}else{
			 $scope.bpVoucherDBFlag = true;
			 $('div#bpVoucherDB').dialog('close');
			 $scope.addBPDetails();
		}
	}
	
	
	
	$scope.addBPDetails = function(){
		console.log("enter into  addBPDetails function");
		var response = $http.post($scope.projectName+'/addBPDetails',$scope.bpServ);
		response.success(function(data, status, headers, config){
				$scope.bpServ = {};
				$scope.bpServ.customer = {};
				$scope.bpServ.faBProm = {};
				
				$scope.fetchBPDetails();
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.fetchBPDetails = function(){
		console.log("enter into fetchBPDetails function");
		var response = $http.post($scope.projectName+'/fetchBPDetails');
		response.success(function(data, status, headers, config){
				$scope.bpVList = data.list;
				
				fbpAmtTemp=0;
				fbpTurnOverTemp=0;
				fbpExpTurnTemp=0;
				for(var i=0; i<data.list.length; i++){
					fbpAmtTemp=fbpAmtTemp+data.list[i].faBProm.fbpAmt;
					fbpTurnOverTemp=fbpTurnOverTemp+data.list[i].faBProm.fbpTurnOver;
					fbpExpTurnTemp=fbpExpTurnTemp+data.list[i].faBProm.fbpExpTurn;
				}
				$scope.fbpAmtSum=fbpAmtTemp;
				$scope.fbpTurnOverSum=fbpTurnOverTemp;
				$scope.fbpExpTurnSum=fbpExpTurnTemp;
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.removeBPV = function(index,bpVouch){
		console.log("enter into removeBPV function");
		$scope.fbpAmtSum=$scope.fbpAmtSum-bpVouch.faBProm.fbpAmt;
		$scope.fbpTurnOverSum=$scope.fbpTurnOverSum-bpVouch.faBProm.fbpTurnOver;
		$scope.fbpExpTurnSum=$scope.fbpExpTurnSum-bpVouch.faBProm.fbpExpTurn;
		var remIndex = {
				"index" : index
		};
		var response = $http.post($scope.projectName+'/removeBPV',remIndex);
		response.success(function(data, status, headers, config){
			$scope.fetchBPDetails();
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.selectCustBranch = function(){
		console.log("enter into selectCustBranch funciton");
		
		 $scope.custBranchDBFlag = false;
		 $('div#custBranchDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#custBranchDB').dialog('open');
	}
	
	
	
	
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("entre into voucherSubmit function = "+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			$scope.alertToast("please enter correct vouher details") 
		}else{
			console.log("final submittion");
		    $scope.saveVsFlag = false;
	    	$('div#saveVsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveVsDB').dialog('open');
		}
	}
	
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	
	$scope.saveVS = function(){
		 console.log("enter into saveVS function");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
		 $('#saveId').attr("disabled","disabled");
		 var response = $http.post($scope.projectName+'/submitBPVoucher',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("success");
					$('#saveId').removeAttr("disabled");
					$scope.vs = {};
					$scope.bpServ = {};
					$scope.bpServ.customer = {};
					$scope.bpServ.faBProm = {};
					
					$scope.custCode = "";
					$scope.bpVList = [];	
					$scope.getVoucherDetails();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);