package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.services.ConstantsValues;

@Controller
public class EmpWiseAssgnCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	
	@RequestMapping(value = "/getBrFrEWAssgn", method = RequestMethod.POST)
	public @ResponseBody Object getBrFrEWAssgn() {
		System.out.println("enter into getBrFrEWAssgn function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Branch> brList = branchDAO.getAllActiveBranches();
		if(!brList.isEmpty()){
			map.put("list",brList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getNewEmp", method = RequestMethod.POST)
	public @ResponseBody Object getNewEmp() {
		System.out.println("enter into getNewEmp function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Employee> empList = employeeDAO.getNewEmp();
		if(!empList.isEmpty()){
			map.put("list",empList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getExistEmp", method = RequestMethod.POST)
	public @ResponseBody Object getExistEmp() {
		System.out.println("enter into getExistEmp function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Employee> empList = employeeDAO.getAllActiveEmployees();
		if(!empList.isEmpty()){
			map.put("list",empList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitEmpWise", method = RequestMethod.POST)
	public @ResponseBody Object submitEmpWise(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into submitEmpWise function");
		Map<String, Object> map = new HashMap<String, Object>();
		int empId = (int) clientMap.get("empId");
		int branchId = (int) clientMap.get("brId");
		int temp = employeeDAO.assignPrBranch(empId,branchId);
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}
