package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchRightsDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerRightsDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.StationRightsDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchRights;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRights;
import com.mylogistics.model.Station;
import com.mylogistics.model.StationRights;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class AllowAdvanceCntlr {
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private CustomerRightsDAO customerRightsDAO;
	
	@Autowired
	private BranchRightsDAO branchRightsDAO;
	
	@Autowired
	private StationRightsDAO stationRightsDAO;
	
	@RequestMapping(value="/getCustomerDetails",method = RequestMethod.POST)
	public @ResponseBody Object getCustomerDetails(){
		System.out.println("Entered into getCustomerDetails function in controller--");
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode=currentUser.getUserBranchCode();
		
		List<Customer> list = customerDAO.getCustomerForCnmt(branchCode);
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(list.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",list);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getBranchDetails",method = RequestMethod.POST)
	public @ResponseBody Object getBranchDetails(){
		System.out.println("Entered into getBranchDetails function in controller--");
		
		List<Branch> list = branchDAO.getAllActiveBranches();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(list.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",list);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getStationForRights",method = RequestMethod.POST)
	public @ResponseBody Object getStationForRights(){
		System.out.println("Entered into getStationForRights function in controller--");
		
		List<Station> list = stationDAO.getStationData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(list.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",list);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/submitCustomerRights", method = RequestMethod.POST)
    public @ResponseBody Object submitCustomerRights(@RequestBody CustomerRights customerRgt){
		
		System.out.println("Entered into submitCustomerRights function in controller---");
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		customerRgt.setUserCode(currentUser.getUserCode());
		customerRgt.setbCode(currentUser.getUserBranchCode());
		               
        Map<String,String> map = new HashMap<String,String>();
        int temp=customerRightsDAO.saveCustRights(customerRgt);
        if (temp>=0) {
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map; 	
	}
	
	@RequestMapping(value = "/submitBranchRights", method = RequestMethod.POST)
    public @ResponseBody Object submitBranchRights(@RequestBody BranchRights branchRgt){
		
		System.out.println("Entered into submitBranchRights function in controller---");
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		branchRgt.setUserCode(currentUser.getUserCode());
		branchRgt.setbCode(currentUser.getUserBranchCode());
		               
        Map<String,String> map = new HashMap<String,String>();
        int temp=branchRightsDAO.saveBrRights(branchRgt);
        if (temp>=0) {
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map; 	
	}
	
	@RequestMapping(value = "/submitStationRights", method = RequestMethod.POST)
    public @ResponseBody Object submitStationRights(@RequestBody StationRights stationRgt){
		
		System.out.println("Entered into submitStationRights function in controller---");
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		stationRgt.setUserCode(currentUser.getUserCode());
		stationRgt.setbCode(currentUser.getUserBranchCode());
		               
        Map<String,String> map = new HashMap<String,String>();
        int temp=stationRightsDAO.saveStRights(stationRgt);
        if (temp>=0) {
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map; 	
	}

}
