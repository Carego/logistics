'use strict';

var app = angular.module('application');

app.controller('ViewCashRecieptCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.vs ={};
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVoucherDetails');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				console.log("Brach Fa Code === " + data.branch.branchFaCode);
			}else{
				console.log("Error in bringing data");
			}

		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);