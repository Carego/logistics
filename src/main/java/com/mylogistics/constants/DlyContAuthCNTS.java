package com.mylogistics.constants;

public class DlyContAuthCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String USER_CODE = "userCode";
	
	public static final String DCA_ID = "dcaId";
	
	public static final String DCA_CUST_CODE = "dcaCustCode";
	
	public static final String DCA_F_DT = "dcaFDt";
	
	public static final String DCA_T_DT = "dcaTDt";
	
	public static final String DCA_STAT_LIST = "dcaStatList";
	
	public static final String DCA_IS_ACTIVE = "dcaIsActive";

}
