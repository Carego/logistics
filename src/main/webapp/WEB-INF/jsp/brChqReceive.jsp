<div ng-show="operatorLogin || superAdminLogin">
	
	<!--By Branch  -->
	<div class="row">
		<form class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branchName" readonly ng-required="true" >
		       	<label for="code">Branch</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="bankMstrId" name ="bankMstrName" value="bank" ng-model="bankMstr.bnkName" ng-click="openBankDB()" readonly ng-required="true">
		       	<label for="code">Bank</label>	
		       	</div>
			
			</div>
		</form>
		
	</div>
	
	<div ng-show="issuedChqBkFlag">
	  
	  <div class="col s6 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<div class="row">
		  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> From Cheque No </th>
	 	  	  	  <th> To Cheque No </th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="issuedChqBk in issuedChqBkList">
			 	  <td><input type="checkbox" ng-model="issuedChqBk.chqBkIsRecByBr" ng-init="issuedChqBk.chqBkIsRecByBr=false"></td>
	              <td>{{issuedChqBk.chqBkFromChqNo}}</td>
	              <td>{{issuedChqBk.chqBkToChqNo}}</td>
	          </tr>
	      </table>
	    </div>
	    
	    <div class="row">
		    <div class="col s12 center">
				<input class="validate" type="button" value="Submit" ng-click="submitIssuedChqBk()">
			</div>
		</div>
		</div>
        
	</div>
	
	<div id ="bankMstrDB" ng-hide="bankMstrDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> A/c No </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
          </tr>
      </table>  
	</div>
	
</div>