package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.DailyContractOldDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.TermNConditionDAO;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.DailyContractOld;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.TermNCondition;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ContToStnService;
import com.mylogistics.services.ContToStnServiceImpl;
import com.mylogistics.services.PBDService;
import com.mylogistics.services.PBDServiceImpl;
import com.mylogistics.services.RBKMService;
import com.mylogistics.services.RBKMServiceImpl;


@Controller
public class DisplayDailyContractCntlr {
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private RateByKmDAO rateByKmDAO;
	
	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;
	
	@Autowired
	private TermNConditionDAO termNConditionDAO;
	
	@Autowired
	private DailyContractOldDAO dailyContractOldDAO;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	private ContToStnService contToStnService = new ContToStnServiceImpl();
	
	private RBKMService rbkmService = new RBKMServiceImpl();

	private PBDService pbdService = new PBDServiceImpl();

	String contractCode=null;
	
	String metric=null;
	
	@RequestMapping(value="/getDlyContIsViewNo	",method = RequestMethod.POST)
	public @ResponseBody Object getDlyContIsViewNo(){
	
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		String branchCode = currentUser.getUserBranchCode();
		List<DailyContract> dlyList = dailyContractDAO.getDlyContractisViewFalse(branchCode);
		
		for(int i=0;i<dlyList.size();i++){
			System.out.println("The values in the list is"+dlyList.get(i).getCreationTS());
		}
		
		if(!dlyList.isEmpty()){
			map.put("list",dlyList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,"No more Contracts to show");
		}
		return map;
	}

	@RequestMapping(value = "/getDailyContCode" , method = RequestMethod.GET)  
	  public @ResponseBody Object getDailyContCode(){
	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> dailyContractsList = dailyContractDAO.getDailyContractCodes(branchCode);
		String dailyContCode [] = new String[dailyContractsList.size()];
		
		for(int i=0;i<dailyContractsList.size();i++){
			dailyContCode[i]=dailyContractsList.get(i);
		}
		
		return dailyContCode;
		
		} 
	
	@RequestMapping(value = "/updateIsViewContract", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewContract(@RequestBody String selection) {
		
		String contids[] = selection.split(",");
		int temp=dailyContractDAO.updateContractisViewTrue(contids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/dailycontractdetails", method = RequestMethod.POST)
	public @ResponseBody Object dailycontractdetails(@RequestBody String dlyContCode) {
		System.out.println("Entr into dailycontractdetails function");
		
		DailyContract dailyContract = new DailyContract();
		
		List<DailyContract> list = dailyContractDAO.getDailyContract(dlyContCode);
		List<DailyContractOld> list2 = dailyContractOldDAO.getOldDailyContract(dlyContCode);
		
		System.out.println("Size of daily contract list is----- "+list.size());
		System.out.println("Size of old daily contract list is----- "+list2.size());
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (!list2.isEmpty() || !list.isEmpty()) {
			dailyContract = list.get(0);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("dailyContract", dailyContract);
			map.put("dailyContractOld", list2);
		} else {
			dailyContract = list.get(0);
			map.put("dailyContract", dailyContract);
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/EditDailyContractSubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditDailyContractSubmit(@RequestBody DailyContract dailyContract) {

		System.out.println("Enter into EditDailyContractSubmit function------");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		int id = dailyContract.getDlyContId();
		contractCode = dailyContract.getDlyContCode();
		
		List<DailyContract> list = dailyContractDAO.getDailyContractData(id);
		
		if(!list.isEmpty()){
			DailyContractOld dailyContractOld = new DailyContractOld();
			
			dailyContractOld.setDlyContId(list.get(0).getDlyContId());
			dailyContractOld.setbCode(list.get(0).getbCode());
			dailyContractOld.setBranchCode(list.get(0).getBranchCode());
			dailyContractOld.setCreationTS(list.get(0).getCreationTS());
			dailyContractOld.setDlyContBLPMCode(list.get(0).getDlyContBLPMCode());
			dailyContractOld.setDlyContCngrCode(list.get(0).getDlyContCngrCode());
			dailyContractOld.setDlyContCode(list.get(0).getDlyContCode());
			dailyContractOld.setDlyContCostGrade(list.get(0).getDlyContCngrCode());
			dailyContractOld.setDlyContCrName(list.get(0).getDlyContCrName());
			dailyContractOld.setDlyContDc(list.get(0).getDlyContDc());
			dailyContractOld.setDlyContDdl(list.get(0).getDlyContDdl());
			dailyContractOld.setDlyContEndDt(list.get(0).getDlyContEndDt());
			dailyContractOld.setDlyContFromStation(list.get(0).getDlyContFromStation());
			dailyContractOld.setDlyContHourOrDay(list.get(0).getDlyContHourOrDay());
			dailyContractOld.setDlyContIsVerify(list.get(0).getDlyContIsVerify());
			//dailyContractOld.setDlyContLoad(list.get(0).getDlyContLoad());
			dailyContractOld.setDlyContProportionate(list.get(0).getDlyContProportionate());
			dailyContractOld.setDlyContRate(list.get(0).getDlyContRate());
			dailyContractOld.setDlyContRemark(list.get(0).getDlyContRemark());
			dailyContractOld.setDlyContStartDt(list.get(0).getDlyContStartDt());
			//dailyContractOld.setDlyContStatisticalCharge(list.get(0).getDlyContStatisticalCharge());
			dailyContractOld.setDlyContToStation(list.get(0).getDlyContToStation());
			//dailyContractOld.setDlyContTransitDay(list.get(0).getDlyContTransitDay());
			dailyContractOld.setDlyContType(list.get(0).getDlyContType());
			//dailyContractOld.setDlyContUnLoad(list.get(0).getDlyContUnLoad());
			dailyContractOld.setUserCode(list.get(0).getUserCode());
			dailyContractOld.setView(false);
			
			int saveOldContract = dailyContractOldDAO.saveDlyContOld(dailyContractOld);
			
			if(saveOldContract>0){
				
				List<RateByKm> rbkmList = rbkmService.getAllRBKM();
				List<PenaltyBonusDetention> pbdList = pbdService.getAllPBD();
				List<ContToStn> cList = contToStnService.getAllCTS();
				
				User currentUser = (User)httpSession.getAttribute("currentUser");
				
				if(!rbkmList.isEmpty()){
				for (int i = 0; i < rbkmList.size(); i++) {
					double rbkmRate=0;
					if(metric.equals("Ton")){
						rbkmRate = rbkmList.get(i).getRbkmRate();
						rbkmRate = (rbkmRate/ConstantsValues.kgInTon);
						//rbkmRate=Double.parseDouble((df.format(rbkmRate)));
						  rbkmRate = (double)Math.round(rbkmRate*100.0)/100.0;
						rbkmList.get(i).setRbkmRate(rbkmRate);
					}
					System.out.println("Rbkm rate is-----"+rbkmRate);
					rbkmList.get(i).setRbkmContCode(dailyContract.getDlyContCode());
					rbkmList.get(i).setbCode(currentUser.getUserBranchCode());
					rbkmList.get(i).setUserCode(currentUser.getUserCode());
				int temp =rateByKmDAO.saveRBKM(rbkmList.get(i));
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Rbkm",ConstantsValues.SUCCESS+"Rbkm");
				}else{
					map.put(ConstantsValues.RESULT+"Rbkm",ConstantsValues.ERROR+"Rbkm");
				}
				}
				}
				
				if(!pbdList.isEmpty()){
				for (int i = 0; i < pbdList.size(); i++) {
					pbdList.get(i).setPbdContCode(dailyContract.getDlyContCode());
					pbdList.get(i).setbCode(currentUser.getUserBranchCode());
					pbdList.get(i).setUserCode(currentUser.getUserCode());
					int savePBD = penaltyBonusDetentionDAO.savePBD(pbdList.get(i));
					System.out.println("after saving pbd---------------"+savePBD);
				}
				}
				
				if(!cList.isEmpty()){
				for (int i = 0; i < cList.size(); i++) {
					double ctsRate=0;
					double ctsFromWt=0;
					double ctsToWt=0;
					if(metric.equals("Ton") && (dailyContract.getDlyContProportionate().equals("P"))){
						ctsRate = cList.get(i).getCtsRate();
						ctsRate = ctsRate/ConstantsValues.kgInTon;
						ctsRate = (double)Math.round(ctsRate*100.0)/100.0;
						cList.get(i).setCtsRate(ctsRate);
						
						ctsFromWt = cList.get(i).getCtsFromWt();
						ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
						ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
						cList.get(i).setCtsFromWt(ctsFromWt);
						
						ctsToWt = cList.get(i).getCtsToWt();
						ctsToWt = ctsToWt*ConstantsValues.kgInTon;
						ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
						cList.get(i).setCtsToWt(ctsToWt);
					}else if(metric.equals("Ton") && (dailyContract.getDlyContProportionate().equals("F"))){
						ctsRate = cList.get(i).getCtsRate();
						cList.get(i).setCtsRate(ctsRate);
						
						ctsFromWt = cList.get(i).getCtsFromWt();
						ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
						ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
						cList.get(i).setCtsFromWt(ctsFromWt);
						
						ctsToWt = cList.get(i).getCtsToWt();
						ctsToWt = ctsToWt*ConstantsValues.kgInTon;
						ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
						cList.get(i).setCtsToWt(ctsToWt);
					}
					System.out.println("proportionate is ----"+dailyContract.getDlyContProportionate());
					System.out.println("cts rate is ----"+ctsRate);
					System.out.println("ctsFromWt is ----"+ctsFromWt);
					System.out.println("ctsToWt is ----"+ctsToWt);
					cList.get(i).setCtsContCode(dailyContract.getDlyContCode());
					cList.get(i).setbCode(currentUser.getUserBranchCode());
					cList.get(i).setUserCode(currentUser.getUserCode());
					int temp=contToStnDAO.saveContToStn(cList.get(i));
					if(temp>0){
						map.put(ConstantsValues.RESULT+"Cts",ConstantsValues.SUCCESS+"Cts");
					}else{
						map.put(ConstantsValues.RESULT+"Cts",ConstantsValues.ERROR+"Cts");
					}
				}
				}	
				int temp=dailyContractDAO.updateDailyContract(dailyContract);
				
				List<Customer> customerList= customerDAO.getCustomer(dailyContract.getDlyContBLPMCode());
				String custName=customerList.get(0).getCustName();
				System.out.println("customer name is-----------"+custName);
				List<FAMaster> fList = faMasterDAO.retrieveFAMaster(dailyContract.getDlyContFaCode());
				FAMaster faMaster = new FAMaster();
				faMaster=fList.get(0);
				faMaster.setFaMfaName(custName);
				int updateFaMstr=faMasterDAO.updateFaMaster(faMaster);
				System.out.println("Updated faMaster------------"+updateFaMstr);
				
				if(temp>0){
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
				}else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
				}else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
		
		return map;
	}
	
	
	@RequestMapping(value = "/saveUpdatedTnc", method = RequestMethod.POST)
	public @ResponseBody Object saveUpdatedTnc(@RequestBody TermNCondition termNCondition) {
		
		Map<String, String> map = new HashMap<String, String>();
		termNCondition.setTncContCode(contractCode);
		
		int temp = termNConditionDAO.updateTnC(termNCondition);

		if (temp >= 0) {
			map.put(ConstantsValues.RESULT+"Tnc", ConstantsValues.SUCCESS+"Tnc");
		} else {
			map.put(ConstantsValues.RESULT+"Tnc", ConstantsValues.ERROR+"Tnc");
		}
	return map;
}
	
	@RequestMapping(value = "/getDlyContCodesList", method = RequestMethod.POST)
	public @ResponseBody Object getDlyContCodesList() {
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> bList = dailyContractDAO.getDailyContractCodes(branchCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getRateByKmData", method = RequestMethod.POST)
	public @ResponseBody Object getRateByKmData(@RequestBody String code) {

		System.out.println("Enter into getRateByKmData function-------"+code);
		List<RateByKm> rateByKms = new ArrayList<RateByKm>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		rateByKms = rateByKmDAO.getRateByKm(code);
		System.out.println("The size of rbkm list is ----"+rateByKms.size());	
		
		if(!rateByKms.isEmpty()){
			map.put("list", rateByKms);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getPBDData", method = RequestMethod.POST)
	public @ResponseBody Object getPBDData(@RequestBody String code) {

		System.out.println("Enter into getPBDData function-------"+code);
		List<PenaltyBonusDetention> pList = new ArrayList<PenaltyBonusDetention>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		pList = penaltyBonusDetentionDAO.getPenaltyBonusDetention(code);
		
		System.out.println("The size of pbd list is ----"+pList.size());	
		
		if(!pList.isEmpty()){
			map.put("list", pList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/deleteRbkm", method = RequestMethod.POST)
	public @ResponseBody Object deleteRbkm(@RequestBody RateByKm rbkm) {

		System.out.println("enter into deleteRbkm function----"+rbkm.getRbkmContCode());
		rateByKmDAO.deleteRBKM(rbkm.getRbkmContCode());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/deletePbd", method = RequestMethod.POST)
	public @ResponseBody Object deletePbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

		System.out.println("enter into deletePbd function----"+penaltyBonusDetention.getPbdContCode());
		penaltyBonusDetentionDAO.deletePBD(penaltyBonusDetention.getPbdContCode());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/addNewRbkm", method = RequestMethod.POST)
	public @ResponseBody Object addNewRbkm(@RequestBody RateByKm rbkm) {

		System.out.println("enter into addNewRbkm function");
		rbkmService.addRBKM(rbkm);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchRbkmListForEditContract", method = RequestMethod.GET)
	public @ResponseBody Object fetchRbkmListForEditContract() {
		System.out.println("enter into fetchRbkmListForEditContract function");

		List<RateByKm> rateByKmList = rbkmService.getAllRBKM();

		Map<String, Object> map = new HashMap<String, Object>();
		if(!rateByKmList.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", rateByKmList);	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/removeNewRbkm", method = RequestMethod.POST)
	public @ResponseBody Object removeNewRbkm(@RequestBody RateByKm rbkm) {

		System.out.println("enter into removeNewRbkm function");
		rbkmService.deleteRBKM(rbkm);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchPbdListForEditContract", method = RequestMethod.GET)
	public @ResponseBody Object fetchPbdListForEditContract() {
		System.out.println("enter into fetchPbdListForEditContract function");

		List<PenaltyBonusDetention> pList = pbdService.getAllPBD();

		Map<String, Object> map = new HashMap<String, Object>();
		
		if(!pList.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", pList);	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/addNewPbd", method = RequestMethod.POST)
	public @ResponseBody Object addNewPbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

		System.out.println("enter into addNewPbd function");
		pbdService.addPBD(penaltyBonusDetention);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeNewPbd", method = RequestMethod.POST)
	public @ResponseBody Object removeNewPbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

		System.out.println("enter into removeNewPbd function");
		pbdService.deletePBD(penaltyBonusDetention);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllPbdForEditDC", method = RequestMethod.POST)
	public @ResponseBody Object removeAllPbdForEditDC() {

		System.out.println("enter into removeAllPbdForEditDC function");
		pbdService.deleteAllPBD();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllRbkmForEditDC", method = RequestMethod.POST)
	public @ResponseBody Object removeAllRbkmForEditDC() {

		System.out.println("enter into removeAllRbkmForEditDC function");
		rbkmService.deleteAllRBKM();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/getCTSData", method = RequestMethod.POST)
	public @ResponseBody Object getCTSData(@RequestBody String code) {

		System.out.println("Enter into getCTSData function-------"+code);
		List<ContToStn> contToStns = new ArrayList<ContToStn>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		contToStns = contToStnDAO.getContToStn(code);
		System.out.println("The size of cts list is ----"+contToStns.size());	
		
		if(!contToStns.isEmpty()){
			map.put("list", contToStns);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/deleteCTS", method = RequestMethod.POST)
	public @ResponseBody Object deleteCTS(@RequestBody ContToStn contToStn) {

		System.out.println("enter into deleteCTS function----"+contToStn.getCtsContCode());
		contToStnDAO.deleteCTS(contToStn.getCtsContCode());
		System.out.println("Deleted successfully CTS");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	

	@RequestMapping(value = "/fetchCTSListForEditContract", method = RequestMethod.GET)
	public @ResponseBody Object fetchCTSListForEditContract() {
		System.out.println("enter into fetchCTSListForEditContract function");

		List<ContToStn> cList = contToStnService.getAllCTS();

		Map<String, Object> map = new HashMap<String, Object>();
		if(!cList.isEmpty()){
			map.put("list", cList);	
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put("list", cList);	
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/addNewCTS", method = RequestMethod.POST)
	public @ResponseBody Object addNewCTS(@RequestBody ContToStn contToStn) {

		System.out.println("enter into addNewCTS function");
		contToStnService.addCTS(contToStn);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeNewCTS", method = RequestMethod.POST)
	public @ResponseBody Object removeNewCTS(@RequestBody ContToStn contToStn) {

		System.out.println("enter into removeNewCTS function");
		contToStnService.deleteCTS(contToStn);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllCTSForEditDC", method = RequestMethod.POST)
	public @ResponseBody Object removeAllCTSForEditDC() {

		System.out.println("enter into removeAllCTSForEditDC function");
		contToStnService.deleteAllCTS();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/sendMetricTypeForEditDC", method = RequestMethod.POST)
	public @ResponseBody Object sendMetricTypeForEditDC(@RequestBody String metricType) {

		System.out.println("enter into sendMetricTypeForEditDC function");
		 metric = metricType;
		System.out.println("Metric type is ----"+metric);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
}
