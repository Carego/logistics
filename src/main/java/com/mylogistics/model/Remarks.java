package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="remarks")
public class Remarks {
	
	@Id
	@Column(name="rmkId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int rmkId;
	
	@Column(name="docSer",length=2)
	@NotNull(message="Document type can't be null" , groups = RemarkInsert.class)
	@NotEmpty(message="Document type can't be blank" , groups = RemarkInsert.class)
	private String docSer;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="docNo", length=30)
	@NotNull(message="Documents no. can't be null" , groups = RemarkInsert.class)
	@NotEmpty(message="Documents no. can't be blank" , groups = RemarkInsert.class)
	private String docNo;
	
	@Column(name="holdDt")
	private Date holdDt;
	
	@Column(name="unHoldDt")
	private Date unHoldDt;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="onHold", columnDefinition="boolean default true")
	private boolean onHold;
	
	
	
	public int getRmkId() {
		return rmkId;
	}



	public void setRmkId(int rmkId) {
		this.rmkId = rmkId;
	}



	public String getSeries() {
		return docSer;
	}



	public void setSeries(String docSer) {
		this.docSer = docSer;
	}



	public Calendar getCreationTS() {
		return creationTS;
	}



	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}



	public String getDocNo() {
		return docNo;
	}



	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}



	public Date getHoldDt() {
		return holdDt;
	}



	public void setHoldDt(Date holdDt) {
		this.holdDt = holdDt;
	}



	public Date getUnHoldDt() {
		return unHoldDt;
	}



	public void setUnHoldDt(Date unHoldDt) {
		this.unHoldDt = unHoldDt;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public boolean isOnHold() {
		return onHold;
	}



	public void setOnHold(boolean onHold) {
		this.onHold = onHold;
	}



	public interface RemarkInsert{
		
	}
}
