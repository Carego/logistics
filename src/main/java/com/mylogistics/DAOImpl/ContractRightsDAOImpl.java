package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ContractRightsDAO;
import com.mylogistics.constants.ContractRightsCNTS;
import com.mylogistics.model.ContractRights;

public class ContractRightsDAOImpl implements ContractRightsDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public ContractRightsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int  saveContractRights(ContractRights contractRights){
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(contractRights);
			 transaction.commit();
			 System.out.println("contractRights Data is inserted successfully");
			 temp = 1;
			 session.flush();
			 
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 	@SuppressWarnings("unchecked")
	 	@Transactional
	 	public List<ContractRights> getContRight(String contCode){
			System.out.println("Enter into getContRight function");	
	 		List<ContractRights> result = new ArrayList<ContractRights>();	 
	 		try{
	 			session = this.sessionFactory.openSession();
	 			Criteria cr=session.createCriteria(ContractRights.class);
	 			cr.add(Restrictions.eq(ContractRightsCNTS.CR_CONT_CODE,contCode));
	 			result =cr.setMaxResults(1).list();
	 			session.flush();
	 		}
	 		catch(Exception e){
	 			e.printStackTrace();
	 		}
	 		session.clear();
	 		session.close();
	 		return  result; 
	 	}
	 	
	 	 @Transactional
	 	public int updateContractRights(ContractRights contractRights){
	 		int temp;
	 		Date date = new Date();
	 		Calendar calendar = Calendar.getInstance();
	 		calendar.setTime(date);
			 try{
				 session = this.sessionFactory.openSession();
				 Criteria cr=session.createCriteria(ContractRights.class);
				 cr.add(Restrictions.eq(ContractRightsCNTS.CR_CONT_CODE,contractRights.getContCode()));
				 transaction = session.beginTransaction();
				 contractRights.setCreationTS(calendar);
				 session.update(contractRights);
				 transaction.commit();
				 session.flush();
				 temp= 1;
			 }catch(Exception e){
				 e.printStackTrace();
				 temp= -1;
			 }
			 session.clear();
			 session.close();
			 return temp;
		 }
	}
