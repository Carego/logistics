package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.TermNConditionDAO;
import com.mylogistics.constants.TermNConditionCNTS;
import com.mylogistics.model.TermNCondition;

public class TermNConditionDAOImpl implements TermNConditionDAO {
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public  TermNConditionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveTnc(TermNCondition termNCondition){
		   int temp;
		   try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 session.save(termNCondition);
				 transaction.commit();
				 System.out.println("termNCondition Data is inserted successfully");
				 session.flush();
				 temp = 1;
			}
			catch(Exception e){
				e.printStackTrace();
				temp = -1;
			}
		   session.clear();
		   session.close();
		   return temp;
	 }
	
	 @SuppressWarnings("unchecked")
	@Transactional
	 public int updateTnC(TermNCondition termNCondition){
 		int temp;
 		Date date = new Date();
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
 		System.out.println("----------tnc"+termNCondition.getTncContCode());
		 try{
			 List<TermNCondition> tList = new ArrayList<TermNCondition>();
			 TermNCondition termNCondition2 = new TermNCondition();
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(TermNCondition.class);
			 cr.add(Restrictions.eq(TermNConditionCNTS.TNC_CONT_CODE,termNCondition.getTncContCode()));
			 tList = cr.list();
			 termNCondition2 = tList.get(0);
			 //termNCondition2.setbCode(termNCondition.getbCode());
			 termNCondition2.setTncBonus(termNCondition.getTncBonus());
			 termNCondition2.setTncContCode(termNCondition.getTncContCode());
			 termNCondition2.setTncDetention(termNCondition.getTncDetention());
			 termNCondition2.setTncLoading(termNCondition.getTncLoading());
			 termNCondition2.setTncPenalty(termNCondition.getTncPenalty());
			 termNCondition2.setTncStatisticalCharge(termNCondition.getTncStatisticalCharge());
			 termNCondition2.setTncTransitDay(termNCondition.getTncTransitDay());
			 termNCondition2.setTncUnLoading(termNCondition.getTncUnLoading());
			 termNCondition2.setCreationTS(calendar);
			 session.saveOrUpdate(termNCondition2);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }		
		 session.clear();
		 session.close();
		 return temp;
	 }
}
