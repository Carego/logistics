package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.SubElectricityMstrDAO;
import com.mylogistics.constants.SubElectricityMstrCNTS;
import com.mylogistics.constants.SubTelephoneMstrCNTS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.SubElectricityMstr;
import com.mylogistics.model.SubTelephoneMstr;

public class SubElectricityMstrDAOImpl implements SubElectricityMstrDAO{


	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public SubElectricityMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<SubElectricityMstr> getSubFileDetails(List<CashStmt> reqCsList){
		System.out.println("enter into getSubFileDetails function");
		List<SubElectricityMstr> semList = new ArrayList<SubElectricityMstr>();
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				for(int i=0;i<reqCsList.size();i++){
					List<SubElectricityMstr> tempList = new ArrayList<SubElectricityMstr>();
					Criteria cr = session.createCriteria(SubElectricityMstr.class);
					cr.add(Restrictions.eq(SubElectricityMstrCNTS.SEM_ID , reqCsList.get(i).getCsSFId()));
					tempList = cr.list(); 
					if(!tempList.isEmpty()){
						semList.add(tempList.get(0));
					}
				} 
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("reqCsList is empty");
		}
		return semList;
	}
}
