'use strict';

var app = angular.module('application');

app.controller('AllowAdvanceCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.custConsignDivFlag = true;
	$scope.customerDBFlag = true;
	$scope.customerRgt = {};
		
	$scope.branchDivFlag = true;
	$scope.branchDBFlag = true;
	$scope.branchRgt = {};
	
	$scope.stationDiv = true;
	$scope.stationDBFlag = true;
	$scope.stationRgt ={};
	
	$scope.submitCustomerFlag = true;
	$scope.submitBranchFlag = true;
	$scope.submitStationFlag = true;
	$scope.hideSubmit = false;
	
	$scope.checkAdvanceType = function(advncType){
		console.log("Entered into checkAdvanceType function----"+advncType);
		if(advncType === "customer" ||advncType === "consignor" || advncType === "consignee"){
			$scope.custConsignDivFlag = false;
			$scope.branchDivFlag = true;
			$scope.stationDiv = true;	
			
			$scope.submitCustomerFlag = false;
			$scope.submitBranchFlag = true;
			$scope.submitStationFlag = true;
			$scope.hideSubmit = true;
			
		}else if(advncType === "branch"){
			console.log("enter into branch"+advncType);
			$scope.branchDivFlag = false;
			$scope.custConsignDivFlag = true;
			$scope.stationDiv = true;	
			
			$scope.submitCustomerFlag = true;
			$scope.submitBranchFlag = false;
			$scope.submitStationFlag = true;
			$scope.hideSubmit = true;
			
		}else if(advncType === "fromStn" || advncType === "toStn"){
			$scope.stationDiv = false;	
			$scope.custConsignDivFlag = true;
			$scope.branchDivFlag = true;
			
			$scope.submitCustomerFlag = true;
			$scope.submitBranchFlag = true;
			$scope.submitStationFlag = false;
			$scope.hideSubmit = true;
		}
	}
	
	$scope.getCustomerDetails = function(){
		console.log("Entered into getCustomerDetails function---->");
		 var response = $http.post($scope.projectName+'/getCustomerDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.customerList = data.list;
					$scope.successToast(data.result);
					$scope.getBranchDetails();
				}else{
					$scope.getBranchDetails();
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	}
	
	$scope.OpenCustomerDB = function(){
		$scope.customerDBFlag = false;
		$('div#customerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customers",
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
		
		$('div#customerDB').dialog('open');
	}
		
	$scope.saveCustomerCode =  function(custName,custCode){
		console.log("enter into saveCustomerCode----->"+custCode);
		$scope.customerRgt.custName = custName;
		$scope.customerRgt.crhCustCode = custCode;
		$('div#customerDB').dialog('close');
		$scope.customerDBFlag = true;
	}
		
	$scope.getBranchDetails = function(){
		console.log("Entered into getBranchDetails function---->");
		 var response = $http.post($scope.projectName+'/getBranchDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.branchList = data.list;
					$scope.successToast(data.result);
					$scope.getStationForRights();
				}else{
					$scope.getStationForRights();
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	}
	
	$scope.OpenBranchDB = function(){
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branches",
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
		
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranchCode =  function(branchName,branchCode){
		console.log("enter into saveBranchCode----->");
		$scope.branchRgt.branchName = branchName;
		$scope.branchRgt.brhBranchCode = branchCode;
		$('div#branchDB').dialog('close');
		$scope.branchDBFlag = true;
	}
	
	$scope.getStationForRights = function(){
		console.log("Entered into getStationForRights function");
		var response = $http.post($scope.projectName+'/getStationForRights');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stationList = data.list;
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	}
	
	$scope.OpenStationDB = function(){
		$scope.stationDBFlag = false;
		$('div#stationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branches",
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
		
		$('div#stationDB').dialog('open');
	}
	
	$scope.saveStationCode =  function(stnName,stnCode){
		console.log("enter into saveStationCode----->");
		$scope.stationRgt.stnName = stnName;
		$scope.stationRgt.strhStCode = stnCode;
		$('div#stationDB').dialog('close');
		$scope.stationDBFlag = true;
	}
	
	$scope.submit = function(AllowAdvanceForm){
		console.log("Entered into submit function");
		if(AllowAdvanceForm.$invalid){
			console.log("AllowAdvanceForm----->"+AllowAdvanceForm.$invalid);
			if(AllowAdvanceForm.advncType.$invalid){
				$scope.alertToast("Please enter advance type...");
			}else if(AllowAdvanceForm.validDate.$invalid){
				$scope.alertToast("Please enter date ...");
			}
		}	
	}
	
	$scope.submitCustomerRights = function(AllowAdvanceForm,customerRgt,advncType,validDate){
		console.log("Entered into submitCustomerRights function");
		if(AllowAdvanceForm.$invalid){
			console.log("AllowAdvanceForm----->"+AllowAdvanceForm.$invalid);
			if(AllowAdvanceForm.advncType.$invalid){
				$scope.alertToast("Please enter advance type...");
			}else if(AllowAdvanceForm.validDate.$invalid){
				$scope.alertToast("Please enter date ...");
			}
		}else{
			if(angular.isUndefined($scope.customerRgt.custName) || $scope.customerRgt.custName === null || $scope.customerRgt.custName === ""){
				$scope.alertToast("Please enter customer name ...");
			}else{
				
				$scope.customerRgt.crhAdvAllow = "yes";
				$scope.customerRgt.crhAdvValidDt = validDate;
				if(advncType ==="customer"){
					$scope.customerRgt.crhIsCustomer = "yes";
					$scope.customerRgt.crhIsConsignor = "no";
					$scope.customerRgt.crhIsConsignee = "no";
				}else if(advncType ==="consignor"){
					$scope.customerRgt.crhIsCustomer = "no";
					$scope.customerRgt.crhIsConsignor = "yes";
					$scope.customerRgt.crhIsConsignee = "no";
				}else if(advncType ==="consignee"){
					$scope.customerRgt.crhIsCustomer = "no";
					$scope.customerRgt.crhIsConsignor = "no";
					$scope.customerRgt.crhIsConsignee = "yes";
				}
				
				var response = $http.post($scope.projectName+'/submitCustomerRights', customerRgt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.customerRgt.custName = "";
						$scope.customerRgt.crhCustCode = "";
						$scope.advncType = "";
						$scope.validDate = "";
						$scope.successToast(data.result);
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}	
	}
	
	$scope.submitBranchRights = function(AllowAdvanceForm,branchRgt,advncType,validDate){
		console.log("Entered into submitBranchRights function ");
		
		if(AllowAdvanceForm.$invalid){
			console.log("AllowAdvanceForm----->"+AllowAdvanceForm.$invalid);
			if(AllowAdvanceForm.advncType.$invalid){
				$scope.alertToast("Please enter advance type...");
			}else if(AllowAdvanceForm.validDate.$invalid){
				$scope.alertToast("Please enter date ...");
			}
		}else{
			if(angular.isUndefined($scope.branchRgt.branchName) || $scope.branchRgt.branchName === null || $scope.branchRgt.branchName === ""){
				$scope.alertToast("Please enter branch name ...");
			}else{
				
				$scope.branchRgt.brhAdvAllow = "yes";
				$scope.branchRgt.brhAdvValidDt = validDate;
				console.log("Valid Date" + $scope.validDate);
				var response = $http.post($scope.projectName+'/submitBranchRights', branchRgt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.branchRgt.branchName = "";
						$scope.branchRgt.brhBranchCode = "";
						$scope.advncType = "";
						$scope.validDate = "";
						$scope.successToast(data.result);
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}
	}
	
	$scope.submitStationRights = function(AllowAdvanceForm,stationRgt,advncType,validDate){
		console.log("Entered into submitStationRights function ");
		
		if(AllowAdvanceForm.$invalid){
			console.log("AllowAdvanceForm----->"+AllowAdvanceForm.$invalid);
			if(AllowAdvanceForm.advncType.$invalid){
				$scope.alertToast("Please enter advance type...");
			}else if(AllowAdvanceForm.validDate.$invalid){
				$scope.alertToast("Please enter date ...");
			}
		}else{
			if(angular.isUndefined($scope.stationRgt.stnName) || $scope.stationRgt.stnName === null || $scope.branchRgt.stnName === ""){
				$scope.alertToast("Please enter station name ...");
			}else{
				
				$scope.stationRgt.strhAdvAllow = "yes";
				$scope.stationRgt.strhAdvValidDt = validDate;
				if(advncType ==="fromStn"){
					$scope.stationRgt.strhIsFromSt = "yes";
					$scope.stationRgt.strhIsToSt = "no";
				}else if(advncType ==="toStn"){
					$scope.stationRgt.strhIsFromSt = "no";
					$scope.stationRgt.strhIsToSt = "yes";
				}
				
				var response = $http.post($scope.projectName+'/submitStationRights', stationRgt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.stationRgt.stnName = "";
						$scope.stationRgt.strhStCode = "";
						$scope.advncType = "";
						$scope.validDate = "";
						$scope.successToast(data.result);
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true ){
		$scope.getCustomerDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	//$scope.getCustomerDetails();
	
}]);