'use strict';

var app = angular.module('application');

app.controller('LhpvPrintCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){

	$scope.brhList = []; 
	$scope.brhFlag = true;
	$scope.branch = {};
	
	$scope.printLhpvFlag = true; 
	console.log("branch="+$scope.currentBranch);
	$scope.getBranch = function(){
		console.log("enter into getBranch function");
		
		var response = $http.post($scope.projectName+'/getBrhFrLP');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.brhList = data.list;
				for (var i=0;i<$scope.brhList.length;i++){
					if($scope.brhList[i].branchName===$scope.currentBranch){
						$scope.branch=$scope.brhList[i];
						break;
					}
				}
			}else{
				console.log("error in fetching Branch Data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}  
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.brhFlag = false;
    	$('div#brhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brhFlag = true;
		    }
			});
		$('div#brhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(branch){
		console.log("enter into saveBranch function");
		$('div#brhId').dialog('close');	
		$scope.branch = branch;
	}
	
	
	$scope.submitLP = function(lhpvPrintForm){
		console.log("enter into submitLP function = "+lhpvPrintForm.$invalid);
		if(lhpvPrintForm.$invalid){
			$scope.alertToast("please fill Branch and Date");
		}else{
			var reqData = {
					"brhId" : $scope.branch.branchId,
					"date"  : $scope.lhDate
			};
			var response = $http.post($scope.projectName+'/submitLP',reqData);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.printLhpvFlag = false;
			    	$('div#printLhpvDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.printLhpvFlag = true;
					    }
						});
					$('div#printLhpvDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printLhpvDB').dialog('close');
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs funciton");
		$window.location.href = $scope.projectName+'/printLhpvBtDy';
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);