package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javassist.compiler.ast.Stmnt;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.omg.PortableServer.POAPackage.NoServant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sun.font.CreatedFontTracker;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.StationaryDAO;
import com.mylogistics.constants.AddressCNTS;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.BranchSStatsCNTS;
import com.mylogistics.constants.BranchStockDisDetCNTS;
import com.mylogistics.constants.BranchStockLeafDetCNTS;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.HBO_StnNotificationCNTS;
import com.mylogistics.constants.RegularContractCNTS;
import com.mylogistics.constants.StationaryCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.controller.StationaryCntlr;
import com.mylogistics.model.Address;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockDisDet;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.ChCnSeDetail;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.HBO_StnNotification;
import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;
import com.mylogistics.model.Stationary;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.StnTransferService;
import com.sun.org.apache.bcel.internal.classfile.ConstantValue;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.ErrorMsg;

public class StationaryDAOImpl implements StationaryDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(StationaryDAOImpl.class);
	
	@Autowired
	public StationaryDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	 @Transactional
	   public int saveStationary(Stationary stationary){
		 int temp = 0;
		   	try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 int id = (Integer) session.save(stationary);
				 transaction.commit();
				 session.flush();
				 temp= id;
				}
		   
			catch(Exception e){
				e.printStackTrace();
				temp= -1;
			}
		   	session.clear();
		   	session.close();
			return temp;
		 }
	 
	 	@SuppressWarnings("unchecked")
		@Transactional
		public int getLastStationaryId(){
			 List<Stationary> list = new ArrayList<Stationary>();
			 int id=0;
			 try{
				 session = this.sessionFactory.openSession();
				 list = session.createQuery("from Stationary order by stId DESC ").setMaxResults(1).list();
				 id = list.get(0).getStId();
				 session.flush();
			 	}catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return id;
		}
	 	
	 	@Transactional
		 public long totalCount(){
	 		 
	 		 long tempCount = -1; 
	 		 try {
	 			session = this.sessionFactory.openSession();
	 			tempCount = (Long) session.createCriteria(Stationary.class).setProjection(Projections.rowCount()).uniqueResult();
	 			session.flush();
	 		} catch (Exception e) {
				e.printStackTrace();
			}
	 		session.clear();
			session.close();
			return tempCount;
		 }	
	 	
	 	@Transactional
	 	public int updateStationary(Stationary stationary){
	 		System.out.println("enter into updateStationary function--->"+stationary.getStId());
	 		int temp;
		   	try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 session.saveOrUpdate(stationary);
				 transaction.commit();
				 session.flush();
				 temp= 1;
			}catch(Exception e){
				e.printStackTrace();
				temp= -1;
			}
		   	session.clear();
		   	session.close();
			return temp;
	 	}
	 	
	 	@SuppressWarnings("unchecked")
		@Transactional
	 	public Stationary getStnById(int stnId){
	 		logger.info("Enter into getStnById() : Stn ID = "+stnId);
	 		Stationary stationary = new Stationary();
	 		try{
	 			 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(Stationary.class);
				 cr.add(Restrictions.eq(StationaryCNTS.ST_ID, stnId));
				 List<Stationary> stnList = cr.list();
				 if(!stnList.isEmpty()){
					 stationary = stnList.get(0);
				 }
				 session.flush();
	 		}catch(Exception e){
	 			logger.error("Exception : "+e);
	 		}
	 		session.clear();
	 		session.close();
	 		logger.info("Exit from getStnById()");
	 		return stationary;
	 	}

		@Override
		public int saveCustStnry(Map<String, Object> custStnryService) {
			System.out.println("StationaryDAOImpl.saveCustStnry()");
			int temp;
			int noOfDgt;
			System.out.println("branchId: "+ custStnryService.get("branchId"));
			System.out.println("frmStnryNo: "+ custStnryService.get("frmStnryNo"));
			System.out.println("toStnryNo: "+ custStnryService.get("toStnryNo"));
			System.out.println("stnryPrefix: "+ custStnryService.get("stnryPrefix"));
			System.out.println("stnrySuffix: "+ custStnryService.get("stnrySuffix"));
			System.out.println("stnryType: "+ custStnryService.get("stnryType"));
			System.out.println("noOfDgt: "+ custStnryService.get("noOfDgt"));
			
			if(custStnryService.get("noOfDgt") != null){
				if(String.valueOf(custStnryService.get("stnryPrefix")).equalsIgnoreCase("")){
					noOfDgt=6;
				}else{
					noOfDgt = Integer.parseInt(String.valueOf(custStnryService.get("noOfDgt")));
				}
			}else{
				noOfDgt=6;
			}
			
			if(noOfDgt>9 ||noOfDgt<=0)
				noOfDgt=9;
			
			User currentUser = (User) httpSession.getAttribute("currentUser");
			
			try {
				int frmStnryNo = Integer.parseInt(String.valueOf(custStnryService.get("frmStnryNo")));
				int toStnryNo = Integer.parseInt(String.valueOf(custStnryService.get("toStnryNo")));
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				for (int stnryNo = frmStnryNo; stnryNo<=toStnryNo; stnryNo++) {
					
					String stnryCode = "";
					
					if (String.valueOf(custStnryService.get("stnryPrefix")).equalsIgnoreCase("") && 
							String.valueOf(custStnryService.get("stnrySuffix")).equalsIgnoreCase("")) {
						if (String.valueOf(custStnryService.get("stnryType")).equalsIgnoreCase("cnmt") &&
								String.valueOf(frmStnryNo).length() == 7) {
							stnryCode = "0"+String.valueOf(stnryNo);
						} else {
							stnryCode = String.valueOf(stnryNo);
						}
					} else if (String.valueOf(custStnryService.get("stnryPrefix")).equalsIgnoreCase("")) {
						stnryCode = String.valueOf(stnryNo+1000000000).substring(String.valueOf(stnryNo+1000000000).length() - noOfDgt)
								+String.valueOf(custStnryService.get("stnrySuffix"));
					} else if (String.valueOf(custStnryService.get("stnrySuffix")).equalsIgnoreCase("")) {
						stnryCode = String.valueOf(custStnryService.get("stnryPrefix"))
								+String.valueOf(stnryNo+1000000000).substring(String.valueOf(stnryNo+1000000000).length() - noOfDgt);
					} else {
						stnryCode = String.valueOf(custStnryService.get("stnryPrefix"))
								+String.valueOf(stnryNo+1000000000).substring(String.valueOf(stnryNo+1000000000).length() - noOfDgt)
								+String.valueOf(custStnryService.get("stnrySuffix"));
					}
					
					//check this no in DB
					Criteria cr = session.createCriteria(BranchStockLeafDet.class);
					cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, stnryCode));
					cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, String.valueOf(custStnryService.get("stnryType"))));
					
					List<BranchStockLeafDet> brsLeafDetList = cr.list();
					
					System.out.println("brsListSize: "+brsLeafDetList.size());
					
					for (BranchStockLeafDet branchStockLeafDet : brsLeafDetList) {
						System.out.print(branchStockLeafDet.getBrsLeafDetSNo()+"\t");
					}
					
					
					//check this no in DB
					List<BranchSStats> branchSStatsList = session.createCriteria(BranchSStats.class)
										.add(Restrictions.eq(BranchSStatsCNTS.BRS_SERIAL_NO, stnryCode))
										.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, String.valueOf(custStnryService.get("stnryType")))).list();

					System.out.println("branchSStatsListSize: "+branchSStatsList.size());
					
					
					if (brsLeafDetList.isEmpty() && branchSStatsList.isEmpty()) {
						//ok enter the stationary
						BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
						branchStockLeafDet.setbCode(String.valueOf(custStnryService.get("branchId")));
						branchStockLeafDet.setBrsLeafDetBrCode(String.valueOf(custStnryService.get("branchId")));
						branchStockLeafDet.setBrsLeafDetSNo(stnryCode);
						branchStockLeafDet.setBrsLeafDetStatus(String.valueOf(custStnryService.get("stnryType")));
						branchStockLeafDet.setUserCode(currentUser.getUserCode());
						
						session.save(branchStockLeafDet);
					} else {
						return -2;
					}
				}
				
				session.flush();
				session.clear();
				transaction.commit();
				
			} catch (Exception e) {
				e.printStackTrace();
				temp = -1;
			} finally {
				session.close();
			}
			temp = 1;
			return temp;
		}
		
		
		public List<Map<String, Object>> getBrhStkDetail(String brhCode){
			
			logger.info("Enter into getBrhStkDetail() : BranchCode = "+brhCode);
			List<Map<String, Object>> stkEnqList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = new HashMap<String, Object>();
			try{
				session = this.sessionFactory.openSession();				
				// CNMT
				List<Long> totalCnmtList = new ArrayList<Long>();
				List<Long> recCnmtList = new ArrayList<Long>();				
				List<Long> unRecCnmtList = new ArrayList<Long>();				
				List<Map<String, Object>> usedCnmtList = new ArrayList<Map<String, Object>>();
				List<Long> unUsedCnmtList = new ArrayList<Long>();				
				
				// Challan
				List<Long> totalChlnList = new ArrayList<Long>();
				List<Long> recChlnList = new ArrayList<Long>();				
				List<Long> unRecChlnList = new ArrayList<Long>();
				List<Map<String, Object>> usedChlnList = new ArrayList<Map<String, Object>>();
				List<Long> unUsedChlnList = new ArrayList<Long>();	
				
				// SEDR				
				List<Long> totalSedrList = new ArrayList<Long>();				
				List<Long> recSedrList = new ArrayList<Long>();												
				List<Long> unRecSedrList = new ArrayList<Long>();								
				List<Map<String, Object>> usedSedrList = new ArrayList<Map<String, Object>>();
				List<Long> unUsedSedrList = new ArrayList<Long>();
				
				List<MasterStationaryStk> mstrList = session.createCriteria(MasterStationaryStk.class)
						.add(Restrictions.eq("issuedBranchCode", Integer.parseInt(brhCode)))
						.addOrder(Order.asc("mstrStnStkStartNo"))
						.list();
				
				if(! mstrList.isEmpty()){
					Iterator<MasterStationaryStk> it = mstrList.iterator();
					while(it.hasNext()){
						MasterStationaryStk stk = (MasterStationaryStk) it.next();
						if(stk.getMstrStnStkStatus().equalsIgnoreCase("cnmt")){
							totalCnmtList.add(stk.getMstrStnStkStartNo());
							if(stk.getIsReceived().equalsIgnoreCase("yes")){
								recCnmtList.add(stk.getMstrStnStkStartNo());
								Long startNo = stk.getMstrStnStkStartNo();
								Long endNo = startNo + 49;
								Long unusedCount = (Long) session.createCriteria(BranchStockLeafDet.class)
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, brhCode))
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "cnmt"))								
										.add(Restrictions.sqlRestriction("CAST(brsLeafDetSNo AS UNSIGNED) >= "+startNo+" AND CAST(brsLeafDetSNo AS UNSIGNED) <="+endNo+" "))
										.addOrder(Order.asc(BranchStockLeafDetCNTS.BRSLD_SNO))
										.setProjection(Projections.rowCount())
										.uniqueResult();
								
								
								if(unusedCount == 50)
									unUsedCnmtList.add(stk.getMstrStnStkStartNo());
								else{
									Long usedCount = (Long) session.createCriteria(BranchSStats.class)
											.add(Restrictions.eq(BranchSStatsCNTS.BRS_BRCODE, brhCode))
											.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, "cnmt"))							
											.add(Restrictions.sqlRestriction("CAST(brsStatsSerialNo AS UNSIGNED) >= "+startNo+" AND  CAST(brsStatsSerialNo AS UNSIGNED) <= "+endNo+" "))								
											.addOrder(Order.asc(BranchSStatsCNTS.BRS_SERIAL_NO))
											.setProjection(Projections.rowCount())
											.uniqueResult();
									
									Map<String, Object> usedMap = new HashMap<String, Object>();
									usedMap.put("startNo", stk.getMstrStnStkStartNo());
									usedMap.put("unused", unusedCount);
									usedMap.put("used", usedCount);								
									usedCnmtList.add(usedMap);
								}
							}else
								unRecCnmtList.add(stk.getMstrStnStkStartNo());
							
						}else if(stk.getMstrStnStkStatus().equalsIgnoreCase("chln")){
							totalChlnList.add(stk.getMstrStnStkStartNo());
							if(stk.getIsReceived().equalsIgnoreCase("yes")){
								recChlnList.add(stk.getMstrStnStkStartNo());
								Long startNo = stk.getMstrStnStkStartNo();
								Long endNo = startNo + 49;
								Long unusedCount = (Long) session.createCriteria(BranchStockLeafDet.class)
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, brhCode))
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "chln"))								
										.add(Restrictions.sqlRestriction("CAST(brsLeafDetSNo AS UNSIGNED) >= "+startNo+" AND CAST(brsLeafDetSNo AS UNSIGNED) <="+endNo+" "))
										.addOrder(Order.asc(BranchStockLeafDetCNTS.BRSLD_SNO))
										.setProjection(Projections.rowCount())
										.uniqueResult();
								
								System.err.println("Unused CHLN = "+unusedCount);
															
								if(unusedCount == 50)
									unUsedChlnList.add(stk.getMstrStnStkStartNo());
								else{
									Long usedCount = (Long) session.createCriteria(BranchSStats.class)
											.add(Restrictions.eq(BranchSStatsCNTS.BRS_BRCODE, brhCode))
											.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, "chln"))							
											.add(Restrictions.sqlRestriction("CAST(brsStatsSerialNo AS UNSIGNED) >= "+startNo+" AND  CAST(brsStatsSerialNo AS UNSIGNED) <= "+endNo+" "))								
											.addOrder(Order.asc(BranchSStatsCNTS.BRS_SERIAL_NO))
											.setProjection(Projections.rowCount())
											.uniqueResult();
									
									Map<String, Object> usedMap = new HashMap<String, Object>();
									usedMap.put("startNo", stk.getMstrStnStkStartNo());
									usedMap.put("unused", unusedCount);
									usedMap.put("used", usedCount);								
									usedChlnList.add(usedMap);
								}
							}else
								unRecChlnList.add(stk.getMstrStnStkStartNo());
						}else if(stk.getMstrStnStkStatus().equalsIgnoreCase("sedr")){
							totalSedrList.add(stk.getMstrStnStkStartNo());
							if(stk.getIsReceived().equalsIgnoreCase("yes")){
								recSedrList.add(stk.getMstrStnStkStartNo());
								Long startNo = stk.getMstrStnStkStartNo();
								Long endNo = startNo + 49;
								Long unusedCount = (Long) session.createCriteria(BranchStockLeafDet.class)
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, brhCode))
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "sedr"))								
										.add(Restrictions.sqlRestriction("CAST(brsLeafDetSNo AS UNSIGNED) >= "+startNo+" AND CAST(brsLeafDetSNo AS UNSIGNED) <="+endNo+" "))
										.addOrder(Order.asc(BranchStockLeafDetCNTS.BRSLD_SNO))
										.setProjection(Projections.rowCount())
										.uniqueResult();
								
								if(unusedCount == 50)
									unUsedSedrList.add(stk.getMstrStnStkStartNo());
								else{
									Long usedCount = (Long) session.createCriteria(BranchSStats.class)
											.add(Restrictions.eq(BranchSStatsCNTS.BRS_BRCODE, brhCode))
											.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, "sedr"))							
											.add(Restrictions.sqlRestriction("CAST(brsStatsSerialNo AS UNSIGNED) >= "+startNo+" AND  CAST(brsStatsSerialNo AS UNSIGNED) <= "+endNo+" "))								
											.addOrder(Order.asc(BranchSStatsCNTS.BRS_SERIAL_NO))
											.setProjection(Projections.rowCount())
											.uniqueResult();
									
									Map<String, Object> usedMap = new HashMap<String, Object>();
									usedMap.put("startNo", stk.getMstrStnStkStartNo());
									usedMap.put("unused", unusedCount);
									usedMap.put("used", usedCount);								
									usedSedrList.add(usedMap);
								}
							}else
								unRecSedrList.add(stk.getMstrStnStkStartNo());
						}
					}
				}
						
				
				
				// Set all CNMT
				map.put("totalCnmt", totalCnmtList);
				map.put("recCnmt", recCnmtList);
				map.put("unRecCnmt", unRecCnmtList);
				map.put("usedCnmt", usedCnmtList);
				map.put("unUsedCnmt", unUsedCnmtList);
				
				// Set all Challan
				map.put("totalChln", totalChlnList);
				map.put("recChln", recChlnList);
				map.put("unRecChln", unRecChlnList);
				map.put("usedChln", usedChlnList);
				map.put("unUsedChln", unUsedChlnList);
				
				// Set all Sedr
				map.put("totalSedr", totalSedrList);
				map.put("recSedr", recSedrList);
				map.put("unRecSedr", unRecSedrList);
				map.put("usedSedr", usedSedrList);
				map.put("unUsedSedr", unUsedSedrList);
				
				stkEnqList.add(map);
			
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
			session.flush();
			session.clear();
			session.close();
			logger.info("Exit from getBrhStkDetail()");
			return stkEnqList;
		}
		
		@Override
		public List<String> fetchStnaryDetail(Map<String, String> initParam){
			logger.info("Enter into fetchStnaryDetail() : StartNo = "+initParam.get("startNo") +" : usedUnused = "+initParam.get("usedUnused")+ " : type = "+initParam.get("type"));
			List<String> usedUnusedList = new ArrayList<String>();
			try{
				session = this.sessionFactory.openSession();
				
				String startNo = initParam.get("startNo");
				String usedUnused = initParam.get("usedUnused");
				String type = initParam.get("type");
				String brhCode = initParam.get("branchCode");
				
				Integer startSeNo = Integer.parseInt(startNo);
				Integer endSeNo = startSeNo + 49;
				if(usedUnused.equalsIgnoreCase("used")){
					usedUnusedList = session.createCriteria(BranchSStats.class)
							.add(Restrictions.eq(BranchSStatsCNTS.BRS_BRCODE, brhCode))
							.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, type))							
							.add(Restrictions.sqlRestriction("CAST(brsStatsSerialNo AS UNSIGNED) >= "+startSeNo+" AND  CAST(brsStatsSerialNo AS UNSIGNED) <= "+endSeNo+" "))								
							.addOrder(Order.asc(BranchSStatsCNTS.BRS_SERIAL_NO))
							.setProjection(Projections.property(BranchSStatsCNTS.BRS_SERIAL_NO))
							.list();
				}else if(usedUnused.equalsIgnoreCase("unused")){
					usedUnusedList = session.createCriteria(BranchStockLeafDet.class)
							.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, brhCode))
							.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, type))								
							.add(Restrictions.sqlRestriction("CAST(brsLeafDetSNo AS UNSIGNED) >= "+startSeNo+" AND CAST(brsLeafDetSNo AS UNSIGNED) <="+endSeNo+" "))
							.addOrder(Order.asc(BranchStockLeafDetCNTS.BRSLD_SNO))
							.setProjection(Projections.property(BranchStockLeafDetCNTS.BRSLD_SNO))
							.list();
				}
				// Remove Duplicate
				Set<String> set = new HashSet<String>(usedUnusedList);				
				usedUnusedList.clear();
				usedUnusedList = new ArrayList<String>(set);
				
				logger.info("Found used/unused Size = "+usedUnusedList.size());
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
			session.flush();
			session.clear();
			session.close();
			logger.info("Exit from fetchStnaryDetail()");
			return usedUnusedList;
		}
		
		
		public List<Map<String, Object>> getMstrStock(Session session){
			logger.info("Enter into getMstrStock() : Session = "+session);
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

			//	Total Stock
			List<MasterStationaryStk> cnmtAval = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> cnmtSentToBranch = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> cnmtRecByBranch = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> cnmtUnrecByBranch = new ArrayList<MasterStationaryStk>();
			
			List<MasterStationaryStk> chlnAval = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> chlnSentToBranch = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> chlnRecByBranch = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> chlnUnrecByBranch = new ArrayList<MasterStationaryStk>();
			
			List<MasterStationaryStk> sedrAval = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> sedrSentToBranch = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> sedrRecByBranch = new ArrayList<MasterStationaryStk>();
			List<MasterStationaryStk> sedrUnrecByBranch = new ArrayList<MasterStationaryStk>();
			
			List<MasterStationaryStk> mstrStkList = session.createCriteria(MasterStationaryStk.class)
					.list();
			if(! mstrStkList.isEmpty()){
				Iterator<MasterStationaryStk> it = mstrStkList.iterator();
				while(it.hasNext()){
					MasterStationaryStk stk = (MasterStationaryStk) it.next();
					System.err.println("ID = "+stk.getMstrStnStkId());
					// Find Available Stock
					if(stk.getIsIssued().equalsIgnoreCase("no") && stk.getMstrStnStkStatus().equalsIgnoreCase("cnmt"))
						cnmtAval.add(stk);
					if(stk.getIsIssued().equalsIgnoreCase("no") && stk.getMstrStnStkStatus().equalsIgnoreCase("chln"))
						chlnAval.add(stk);
					if(stk.getIsIssued().equalsIgnoreCase("no") && stk.getMstrStnStkStatus().equalsIgnoreCase("sedr"))
						sedrAval.add(stk);
					
					// Find Sent To Branch 
					if(stk.getIsIssued().equalsIgnoreCase("yes") && stk.getMstrStnStkStatus().equalsIgnoreCase("cnmt")){
						cnmtSentToBranch.add(stk);
						if(stk.getIsReceived() == null)
							cnmtUnrecByBranch.add(stk);					
						else{
							if(stk.getIsReceived().equalsIgnoreCase("yes"))
								cnmtRecByBranch.add(stk);
							else
								cnmtUnrecByBranch.add(stk);
						}
					}if(stk.getIsIssued().equalsIgnoreCase("yes") && stk.getMstrStnStkStatus().equalsIgnoreCase("chln")){
						chlnSentToBranch.add(stk);
						if(stk.getIsReceived() == null)
							chlnUnrecByBranch.add(stk);
						else{
							if(stk.getIsReceived().equalsIgnoreCase("yes"))
								chlnRecByBranch.add(stk);
							if(stk.getIsReceived().equalsIgnoreCase("no"))
								chlnUnrecByBranch.add(stk);
						}
					}if(stk.getIsIssued().equalsIgnoreCase("yes") && stk.getMstrStnStkStatus().equalsIgnoreCase("sedr")){
						sedrSentToBranch.add(stk);
						if(stk.getIsReceived() == null)
							sedrUnrecByBranch.add(stk);
						else{
							if(stk.getIsReceived().equalsIgnoreCase("yes"))
								sedrRecByBranch.add(stk);
							if(stk.getIsReceived().equalsIgnoreCase("no"))
								sedrUnrecByBranch.add(stk);
						}
					}
				}
			}	
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("cnmtAval", cnmtAval);
			map.put("chlnAval", chlnAval);
			map.put("sedrAval", sedrAval);
			
			map.put("cnmtSentToBranch", cnmtSentToBranch);
			map.put("chlnSentToBranch", chlnSentToBranch);
			map.put("sedrSentToBranch", sedrSentToBranch);
			
			map.put("cnmtRecByBranch", cnmtRecByBranch);
			map.put("chlnRecByBranch", chlnRecByBranch);
			map.put("sedrRecByBranch", sedrRecByBranch);
			
			map.put("cnmtUnrecByBranch", cnmtUnrecByBranch);
			map.put("chlnUnrecByBranch", chlnUnrecByBranch);
			map.put("sedrUnrecByBranch", sedrUnrecByBranch);
			
			resultList.add(map);
			logger.info("Exit from getMstrStock() : List Size = "+resultList.size());
			return resultList;
		}
		
		public List<Map<String, Object>> findByStk(Session session, Map<String, String> initParam){
			logger.info("Enter into findByStk()");
			List<Map<String, Object>> findByStkList = new ArrayList<Map<String, Object>>();
			try{
				Long startNo = Long.parseLong(initParam.get("startNo"));
				Long endNo = Long.parseLong(initParam.get("endNo"));
				String typeName = initParam.get("typeName");
				
				List<Long> totStartNo = new ArrayList<Long>();
				while(startNo <= endNo){
					totStartNo.add(startNo);
					startNo = startNo + 50;
				}
				
				List<MasterStationaryStk> mstrList = session.createCriteria(MasterStationaryStk.class)
						.add(Restrictions.in("mstrStnStkStartNo", totStartNo))
						.add(Restrictions.eq("mstrStnStkStatus", typeName))
						.list();
				
				if(! mstrList.isEmpty()){
					Iterator<MasterStationaryStk> it = mstrList.iterator();
					while(it.hasNext()){
						MasterStationaryStk stk = it.next();
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("startNo", stk.getMstrStnStkStartNo());
						map.put("isDirty", stk.getIsDirty());
						map.put("isIssued", stk.getIsIssued());
						map.put("isRec", stk.getIsReceived());
						map.put("branchCode", stk.getIssuedBranchCode());
						
						startNo = stk.getMstrStnStkStartNo();
						
						List<String> unusedCount = session.createCriteria(BranchStockLeafDet.class)
								.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, typeName))								
								.add(Restrictions.sqlRestriction("CAST(brsLeafDetSNo AS UNSIGNED) >= "+startNo+" AND CAST(brsLeafDetSNo AS UNSIGNED) <="+(startNo+49)+" "))
								.addOrder(Order.asc(BranchStockLeafDetCNTS.BRSLD_SNO))
								.setProjection(Projections.property("brsLeafDetSNo"))
								.list();
						
						List<String> usedCount = session.createCriteria(BranchSStats.class)
								.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, typeName))							
								.add(Restrictions.sqlRestriction("CAST(brsStatsSerialNo AS UNSIGNED) >= "+startNo+" AND  CAST(brsStatsSerialNo AS UNSIGNED) <= "+(startNo+49)+" "))								
								.addOrder(Order.asc(BranchSStatsCNTS.BRS_SERIAL_NO))
								.setProjection(Projections.property("brsStatsSerialNo"))
								.list();
						
						map.put("unusedCount", unusedCount);
						map.put("usedCount", usedCount);
						
						findByStkList.add(map);
						
					}
				}else{
					logger.info("Master List by stock is empty !");
				}
				
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
			logger.info("Exit from findByStk()");
			return findByStkList;
		}
		
		@Override		
		public Map doStnTransfer(Session sess, User user, StnTransferService stnTransferService){
			logger.info("Enter into doStnTransfer() : UserID = "+user.getUserId());
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<String> msg = new ArrayList<String>();
			try{
				List<MasterStationaryStk> mstrStkList = sess.createCriteria(MasterStationaryStk.class)
					.add(Restrictions.in("mstrStnStkStartNo", stnTransferService.getStartList()))
					.add(Restrictions.eq("mstrStnStkStatus", stnTransferService.getTransferType()))
					.list();
				logger.error("Mstr Size : "+mstrStkList.size());
				if(! mstrStkList.isEmpty()){
					Iterator<MasterStationaryStk> mstrIterator = mstrStkList.iterator();
					while(mstrIterator.hasNext()){
						MasterStationaryStk stk = mstrIterator.next();
						stk.setIssuedBranchCode(stnTransferService.getBranchCode());
						sess.update(stk);						
						Long startNo = stk.getMstrStnStkStartNo();
						Long endNo = startNo + 49;			
											
						while(startNo <= endNo){											
							List<BranchStockLeafDet> leafList = sess.createCriteria(BranchStockLeafDet.class)
									.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, String.valueOf(startNo)))
									.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, stk.getMstrStnStkStatus()))
									.list();
							if(! leafList.isEmpty()){
								BranchStockLeafDet leaf = leafList.get(0);
								leaf.setbCode(String.valueOf(stnTransferService.getBranchCode()));
								leaf.setBrsLeafDetBrCode(String.valueOf(stnTransferService.getBranchCode()));
								sess.update(leaf);								
							}
							startNo = startNo + 1;
						}
						System.err.println("START NO : "+startNo);
					}
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					msg.add("Start No. not found in Master Stock !");
				}
				
			}catch(Exception e){
				//sess.clear();
				msg.add("Server Error !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				logger.error("User ID = "+user.getUserId()+" : Exception = "+e);
			}
			resultMap.put("msg", msg);
			logger.info("Exit from doStnTransfer() : UserID = "+user.getUserId());
			return resultMap;			
		}
		
		@Override
		public Map<String, Object> isStnAvailable(Session session, User user, StnTransferService stnService){
			logger.info("Enter into isStnAvailable() : UserID = "+user.getUserId());
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<String> errMsg = new ArrayList<String>();
			String result = "success";
			try{
				List<MasterStationaryStk> mstrStkList = session.createCriteria(MasterStationaryStk.class)
						.add(Restrictions.in("mstrStnStkStartNo", stnService.getStartList()))
						.add(Restrictions.eq("mstrStnStkStatus", stnService.getTransferType()))
						.addOrder(Order.asc("mstrStnStkStartNo"))
						.list();
				if(! mstrStkList.isEmpty()){
					Iterator<MasterStationaryStk> mstrStnStkIterator = mstrStkList.iterator();
					while(mstrStnStkIterator.hasNext()){
						MasterStationaryStk stk = (MasterStationaryStk) mstrStnStkIterator.next();
						if(stk.getIsIssued().equals("no")){
							result = "error";
							errMsg.add(stnService.getTransferType().toUpperCase()+" : "+stk.getMstrStnStkStartNo()+" is not issued to any branch");
						}else{
							Long count = (Long) session.createCriteria(BranchStockLeafDet.class)
									.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, stnService.getTransferType()))								
									.add(Restrictions.sqlRestriction("CAST(brsLeafDetSNo AS UNSIGNED) >= "+stk.getMstrStnStkStartNo()+" AND CAST(brsLeafDetSNo AS UNSIGNED) <="+(stk.getMstrStnStkStartNo()+49)+" "))
									.addOrder(Order.asc(BranchStockLeafDetCNTS.BRSLD_SNO))
									.setProjection(Projections.rowCount())
									.uniqueResult();
							if(count < 50){
								result = "error";
								errMsg.add(stnService.getTransferType().toUpperCase()+" : "+stk.getMstrStnStkStartNo()+" has been used !");
							}
						}
					}
				}else{
					result = "error";
					errMsg.add("No such record !");
				}
			}catch(Exception e){
				result = "error";
				errMsg.add("Server Error !");
				logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
			}
			resultMap.put(ConstantsValues.RESULT, result);
			resultMap.put("msg", errMsg);
			logger.info("Exit from isStnUsed() : UserID = "+user.getUserId()+" : Result = "+resultMap);
			return resultMap;
		}
		
		
		
		@Override
		public int reIssueStsnry(Map<String, String> stnryData){
			int i=0;
			User user=(User) httpSession.getAttribute("user");
			String type=stnryData.get("type");
			
			Session session=null;
			Transaction transaction=null;
			try{
				session=this.sessionFactory.openSession();
				transaction=session.beginTransaction();
				
				if(type.equalsIgnoreCase("cnmt")){
					if(stnryData.get("cnmtNo") != null){
						Criteria criteria=session.createCriteria(Cnmt.class)
								.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, stnryData.get("cnmtNo")));
						
						if(criteria.list().isEmpty()){
							
						Criteria cr=session.createCriteria(BranchSStats.class)
									.add(Restrictions.eq(BranchSStatsCNTS.BRS_SERIAL_NO, stnryData.get("cnmtNo")))
									.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, "cnmt"));
							
							if(!cr.list().isEmpty()){
								
								Criteria crr=session.createCriteria(BranchStockLeafDet.class)
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, stnryData.get("cnmtNo")))
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "cnmt"));
								
								if(crr.list().isEmpty()){
									BranchSStats branchSStats=(BranchSStats) cr.list().get(0);
									
									BranchStockLeafDet det=new BranchStockLeafDet();
									
									det.setbCode(branchSStats.getbCode());
									det.setBrsLeafDetBrCode(branchSStats.getBrsStatsBrCode());
									det.setBrsLeafDetDlyRt(false);
									det.setBrsLeafDetSNo(branchSStats.getBrsStatsSerialNo());
									det.setBrsLeafDetStatus("cnmt");
									det.setUserCode(branchSStats.getUserCode());
									
									session.save(det);
									session.delete(branchSStats);
									
									session.flush();
									transaction.commit();
									i=1;
								}else{
									i=2;
								}
							}else{
								i=3;
							}
						}else{
							i=4;
						}
					}else{
						i=5;
					}
				}else if(type.equalsIgnoreCase("chln")){
					if(stnryData.get("chlnNo") != null){
						Criteria criteria=session.createCriteria(Challan.class)
								.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, stnryData.get("chlnNo")));
						
						if(criteria.list().isEmpty()){
							
						Criteria cr=session.createCriteria(BranchSStats.class)
									.add(Restrictions.eq(BranchSStatsCNTS.BRS_SERIAL_NO, stnryData.get("chlnNo")))
									.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, "chln"));
							
							if(!cr.list().isEmpty()){
								
								Criteria crr=session.createCriteria(BranchStockLeafDet.class)
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, stnryData.get("chlnNo")))
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "chln"));
								
								if(crr.list().isEmpty()){
									BranchSStats branchSStats=(BranchSStats) cr.list().get(0);
									
									BranchStockLeafDet det=new BranchStockLeafDet();
									
									det.setbCode(branchSStats.getbCode());
									det.setBrsLeafDetBrCode(branchSStats.getBrsStatsBrCode());
									det.setBrsLeafDetDlyRt(false);
									det.setBrsLeafDetSNo(branchSStats.getBrsStatsSerialNo());
									det.setBrsLeafDetStatus("chln");
									det.setUserCode(branchSStats.getUserCode());
									
									session.save(det);
									session.delete(branchSStats);
									
									session.flush();
									transaction.commit();
									i=1;
								}else{
									i=2;
								}
							}else{
								i=3;
							}
						}else{
							i=4;
						}
					}else{
						i=5;
					}
				}else if(type.equalsIgnoreCase("sedr")){
					if(stnryData.get("sedrNo") != null){
						Criteria criteria=session.createCriteria(ArrivalReport.class)
								.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE, stnryData.get("sedrNo")));
						
						if(criteria.list().isEmpty()){
							
						Criteria cr=session.createCriteria(BranchSStats.class)
									.add(Restrictions.eq(BranchSStatsCNTS.BRS_SERIAL_NO, stnryData.get("sedrNo")))
									.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, "sedr"));
							
							if(!cr.list().isEmpty()){
								
								Criteria crr=session.createCriteria(BranchStockLeafDet.class)
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, stnryData.get("sedrNo")))
										.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "sedr"));
								
								if(crr.list().isEmpty()){
									BranchSStats branchSStats=(BranchSStats) cr.list().get(0);
									
									BranchStockLeafDet det=new BranchStockLeafDet();
									
									det.setbCode(branchSStats.getbCode());
									det.setBrsLeafDetBrCode(branchSStats.getBrsStatsBrCode());
									det.setBrsLeafDetDlyRt(false);
									det.setBrsLeafDetSNo(branchSStats.getBrsStatsSerialNo());
									det.setBrsLeafDetStatus("sedr");
									det.setUserCode(branchSStats.getUserCode());
									
									session.save(det);
									session.delete(branchSStats);
									
									session.flush();
									transaction.commit();
									i=1;
								}else{
									i=2;
								}
							}else{
								i=3;
							}
						}else{
							i=4;
						}
					}else{
						i=5;
					}
				}else if(type.equalsIgnoreCase("chq")){
					if(stnryData.get("chqNo") != null && stnryData.get("bnkNo") != null){
						
						Criteria criteria=session.createCriteria(BankMstr.class)
								.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, stnryData.get("bnkNo")));
						
						if(!criteria.list().isEmpty()){
							BankMstr bankMstr=(BankMstr) criteria.list().get(0);
							
							Criteria cr=session.createCriteria(ChequeLeaves.class)
									.add(Restrictions.eq(ChequeLeavesCNTS.BANK_MSTR, bankMstr))
									.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_CHQ_NO, stnryData.get("chqNo")));
							
							if(!cr.list().isEmpty()){
								ChequeLeaves leaves=(ChequeLeaves) cr.list().get(0);
								
								leaves.setChqLUsed(false);
								session.update(leaves);
								
								session.flush();
								transaction.commit();
								i=1;
							}else{
								i=2;
							}
						}else{
							i=3;
						}
					}else{
						i=4;
					}
				}else{
					i=0;
				}
				
			}catch(Exception e){
				i=-1;
				transaction.rollback();
				logger.info("Exception in reIssueStsnry()"+e);
				e.printStackTrace();
			}finally{
				session.clear();
				session.close();
			}
			return i;
		}
		
		
		
}