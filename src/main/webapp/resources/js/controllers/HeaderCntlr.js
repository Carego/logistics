'use strict';

var app = angular.module("application");

app.controller('HeaderCntlr',['$scope','$rootScope','$http','$location','DataFactoryService','$window','AuthorisationService',
                              function($scope,$rootScope,$http,$location,DataFactoryService,$window,AuthorisationService){
	
	
	//console.log = function() {}

		$rootScope.authenticated = false;
		$scope.operatorLogin    = DataFactoryService.isPermissionForOperator;
		$scope.adminLogin       = DataFactoryService.isPermissionForAdmin;
		$scope.superAdminLogin  = DataFactoryService.isPermissionForSuperAdmin;  
		$scope.logoutStatus     = DataFactoryService.isLogout;
		$scope.pathTest = $location.path();
		console.log("$scope.operatorLogin-------->"+$scope.operatorLogin);
		console.log("$scope.adminLogin-------->"+$scope.adminLogin);
		console.log("$scope.superAdminLogin-------->"+$scope.superAdminLogin);
		console.log("current path=====>"+$location.path());
		
		$scope.maxFileSize = 2097152;
		$scope.kgInTon = 1000;
		$scope.projectName = "/MyLogistics";
		$scope.superAdminBehaviour = "";
		$scope.currentUserRole = "";
		$scope.brsDisBrCode = "";
		//$scope.dispatchCode = "";
		$scope.cmpnyName = "CARE GO LOGISTICS";
		
		$scope.userRights = {};
		
		$scope.CASH_WITHDRAW = "CashWithdraw";
		$scope.CASH_WITHDRAW_ATM = "CashWithdraw(ATM)";
		$scope.CASH_RECIEPT = "CashReciept";
		$scope.CASH_DEPOSITE = "CashDeposite";
		$scope.CASH_PAYMENT = "CashPayment";
		$scope.JR_VOUCHER = "Journal Voucher";
		$scope.REV_VOUCHER = "Voucher Reversal";
		$scope.TEL_VOUCHER = "Telephone";
		$scope.RENT_VOUCHER = "Rent";
		$scope.ELE_VOUCHER = "Electricity";
		$scope.BP_VOUCHER = "Business Promotion";
		$scope.VEH_VOUCHER = "Vehicle";
		$scope.TRV_VOUCHER = "Travel";
		$scope.IBTV_VOUCHER = "Inter Branch TV C/D";
		$scope.LHPV_ADV_VOUCHER = "LHPV ADVANCE";
		$scope.LHPV_BAL_VOUCHER = "LHPV BALANCE";
		$scope.LHPV_SUP_VOUCHER = "LHPV SUPPLEMENTARY";
		$scope.LHPV_CLOSE_VOUCHER = "LHPV CLOSE";
		$scope.LHPV_TEMP_VOUCHER = "LHPV VOUCHER";
		
		$scope.SUBMIT = "Submitted Successfully";
		$scope.NOT_SUBMIT = "not submitted Please try again";
		$scope.RETRIEVE = "Retrieved Successfully";
		$scope.NOT_RETRIEVE = "value not retrieved please reload the page";
		$scope.RESULT = "result";
		$scope.UPDATE = "updated successfully";
		$scope.NOT_UPDATE = "Value is not updated Please retry";
		$scope.VERIFY = "verified successfully";
		$scope.NOT_VERIFY = "Value is not Verified Please retry";
		$scope.PRINT_STMT = "Would you like to print";
		//$scope.superAdminBehaviour = "";
		
		$scope.currentBranch = "";
		$scope.currentEmpName = "";
		
		$scope.branchRights = false;
		$scope.employeeRights = false;
		$scope.customerRights = false;
		$scope.contractRights = false;
		$scope.cnmtRights = false;
		$scope.challanRights = false;
		$scope.sedrRights = false;
		$scope.supplierRights = false;
		$scope.miscRights = false;
		$scope.ownerRights = false;
		$scope.brokerRights = false;
		$scope.billRights = false;
		$scope.bfRights = false;
		
		$scope.setUserRights = function(list){
			console.log("enter into setUserRights function");
			for(var i=0;i<list.length;i++){
				if(list[i] === "cnmt"){
					$scope.cnmtRights = true;
				}else if(list[i] === "challan"){
					$scope.challanRights = true;
				}else if(list[i] === "sedr"){
					$scope.sedrRights = true;
				}
			}
		}
		
		
		$scope.getBrsDisBrCode = function(){
			return $scope.brsDisBrCode;
		}
		
		$scope.setBrsDisBrCode = function(code){
			$scope.brsDisBrCode = code;
		}
		
		$scope.setDispatchCode = function(dCode){
			$scope.dispatchCode = dCode;
		}
		
		$scope.getDispatchCode = function(){
			return $scope.dispatchCode;
		}
		
		
		$scope.userRole = function(){
			console.log("enter into userRole");
			var response = $http.get($scope.projectName+'/roleOfUser');
			response.success(function(data, status, headers, config){
				console.log("from server last login userRole ="+data.role);
				$scope.currentUserRole = data.role;
				$scope.currentBranch  = data.brhName;
				$scope.currentEmpName = data.empName;
				$scope.userRights     = data.userRights;
				AuthorisationService.setUserCode(data.userCode);
				if(data.role == 11){
					AuthorisationService.setUserRole(11);
					$scope.operatorLogin    = false;
					$scope.adminLogin       = false;
					$scope.superAdminLogin  = true;
					$scope.logoutStatus		= false;
					$rootScope.authenticated = true;
					return true;
			/*		var response = $http.get($scope.projectName+'/getSuperAdminBehaviour');
					response.success(function(data, status, headers, config){
						$scope.superAdminBehaviour = data.behaveAs ;
						console.log("----->getSuperAdminBehaviour ===="+$scope.superAdminBehaviour);
							if($scope.superAdminBehaviour === "superAdmin"){
								console.log("//////////\\\\\\\\\\\\\\\\");
								$scope.operatorLogin    = false;
								$scope.adminLogin       = false;
								$scope.superAdminLogin  = true;
								$scope.logoutStatus		= false;
								//$location.path("/superAdmin");
								//$location.path($scope.pathTest);
								if($scope.pathTest === "/" || $scope.pathTest === "/login"){
									$location.path("/superAdmin");
								}else{
									$location.path($scope.pathTest);
								}
							}else if($scope.superAdminBehaviour === "admin"){
								console.log("current path *************** = "+window.location.href);
								$scope.operatorLogin    = false;
								$scope.adminLogin       = true;
								$scope.superAdminLogin  = false;
								$scope.logoutStatus		= false;
								//$location.path("/admin");
								if($scope.pathTest === "/" || $scope.pathTest === "/login"){
									$location.path("/admin");
								}else{
									$location.path($scope.pathTest);
								}
								
							}else if($scope.superAdminBehaviour === "operator"){
								$scope.operatorLogin    = true;
								$scope.adminLogin       = false;
								$scope.superAdminLogin  = false;
								$scope.logoutStatus		= false;
								//$location.path("/operator");
								//$location.path($scope.pathTest);
								if($scope.pathTest === "/" || $scope.pathTest === "/login"){
									$location.path("/operator");
								}else{
									$location.path($scope.pathTest);
								}
							}else{
								$scope.operatorLogin    = false;
								$scope.adminLogin       = false;
								$scope.superAdminLogin  = true;
								$scope.logoutStatus		= false;
								$location.path("/superAdmin");
							}
					});
					response.error(function(data, status, headers, config) {
						$scope.alertToast(data);
					});
					return true;*/
				}else if(data.role == 22){
					AuthorisationService.setUserRole(22);
					$scope.operatorLogin    = false;
					$scope.adminLogin       = true;
					$scope.superAdminLogin  = false;
					$scope.logoutStatus		= false;
					$rootScope.authenticated = true;
					return true;
				}else if(data.role == 33){
					AuthorisationService.setUserRole(33);
					$scope.operatorLogin    = true;
					$scope.adminLogin       = false;
					$scope.superAdminLogin  = false;
					$scope.logoutStatus		= false;
					$rootScope.authenticated = true;
					return true;
				}else{
					$scope.operatorLogin    = false;
					$scope.adminLogin       = false;
					$scope.superAdminLogin  = false;
					$scope.logoutStatus		= true;
					$rootScope.authenticated = false;
					//$location.path("/login");
					return false;
				}	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
			
		}
		
	
	$scope.currentPath = function(){
		if($location.path() === "/login"  ||  $location.path() === "/"){
			$scope.operatorLogin    = false;
			$scope.adminLogin       = false;
			$scope.superAdminLogin  = false;
			$scope.logoutStatus		= true;
			$rootScope.authenticated = false;
			$scope.superAdminBehaviour = "";
		}else if($location.path() === "/superAdmin"){
			$scope.operatorLogin    = false;
			$scope.adminLogin       = false;
			$scope.superAdminLogin  = true;
			$scope.logoutStatus		= false;
			$scope.superAdminBehaviour = "superAdmin";
		}else if($location.path() === "/admin"){
			$scope.operatorLogin    = false;
			$scope.adminLogin       = true;
			$scope.superAdminLogin  = false;
			$scope.logoutStatus		= false;
			//$scope.superAdminBehaviour = "admin";
		}else if($location.path() === "/operator"){
			$scope.operatorLogin    = true;
			$scope.adminLogin       = false;
			$scope.superAdminLogin  = false;
			$scope.logoutStatus		= false;
			//$scope.superAdminBehaviour = "operator";
		}/*else if($location.path() === "/admin"){
			console.log("$scope.currentUserRole = "+$scope.currentUserRole);
		}*/
	}
	
	/*$scope.$watch(function($scope){
		var currentUser = AuthorisationService.getUser();
		console.log("value of currentUser ----->"+currentUser);
		console.log("value of currentUser.userRole ----->"+currentUser.userRole);
		if(angular.isUndefined(currentUser.userRole) ){
			//$window.location.href = $scope.projectName+"/";
		}else{
			return($scope.currentPath());
		}
	});*/
	

	//window.addEventListener('storage', );
	
	/*$scope.getSuperAdminBehaviour = function(){
		var response = $http.get('/MyLogistics/getSuperAdminBehaviour');
		response.success(function(data, status, headers, config){
			$scope.superAdminBehaviour = data.behaveAs ;
			console.log("----->getSuperAdminBehaviour ===="+$scope.superAdminBehaviour);
		});
		response.error(function(data, status, headers, config) {
			$scope.alertToast(data);
		});
	}*/
	
	
	$scope.NowSuperAdmin = function(){
		$scope.operatorLogin    = false;
		$scope.adminLogin       = false;
		$scope.superAdminLogin  = true;
		$scope.logoutStatus		= false;
		var superAdminBecomes = {
				"operatorLogin"   : $scope.operatorLogin,
				"adminLogin"      : $scope.adminLogin,
				"superAdminLogin" : $scope.superAdminLogin,
		};
		var response = $http.post($scope.projectName+'/superAdminBecomes',superAdminBecomes);
		response.success(function(data, status, headers, config){
			console.log("superAdmin as a superAdmin save on server---->"+data.result);
			localStorage.setItem('userRole',11);
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.NowAdmin = function(){
		$scope.operatorLogin    = false;
		$scope.adminLogin       = true;
		$scope.superAdminLogin  = false;
		$scope.logoutStatus		= false;
		var superAdminBecomes = {
				"operatorLogin"   : $scope.operatorLogin,
				"adminLogin"      : $scope.adminLogin,
				"superAdminLogin" : $scope.superAdminLogin,
		};
		var response = $http.post($scope.projectName+'/superAdminBecomes',superAdminBecomes);
		response.success(function(data, status, headers, config){
			console.log("superAdmin as a Admin save on server---->"+data.result);
			localStorage.setItem('userRole',22);
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});  		
	}
	
	$scope.NowOperator = function(){
		$scope.operatorLogin    = true;
		$scope.adminLogin       = false;
		$scope.superAdminLogin  = false;
		$scope.logoutStatus		= false;
		var superAdminBecomes = {
				"operatorLogin"   : $scope.operatorLogin,
				"adminLogin"      : $scope.adminLogin,
				"superAdminLogin" : $scope.superAdminLogin,
		};
		var response = $http.post($scope.projectName+'/superAdminBecomes',superAdminBecomes);
		response.success(function(data, status, headers, config){
			console.log("superAdmin as a Operator save on server---->"+data.result);
			localStorage.setItem('userRole',33);
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.disabledMenu = function(){
		console.log("enter into disabledMenu function");
		$scope.operatorLogin    = false;
		$scope.adminLogin       = false;
		$scope.superAdminLogin  = false;
		$scope.logoutStatus		= true;
		$rootScope.authenticated = false;
	}
	
	$scope.loginAgain = function(){
		console.log("enter into loginAgain function");
		$window.location.href = $scope.projectName+"/";
		//$location.path("/index");
	}
	
	$scope.alertToast = function(msg){
		toast(msg, 4000);
		
	}
	
	$scope.successToast = function(msg){
		toast(msg, 4000);
    }
	
	$scope.alertLongToast = function(msg){
		toast(msg, 600000);
    }
	   
	$scope.errorToast = function(msg){
		alert(msg);
	} 

   
   $scope.dashboard = function(){
	   console.log("enter into dashboard function");
	   var response = $http.get($scope.projectName+'/roleOfUser');
	   response.success(function(data, status, headers, config){
		   if(data.role == 11){
			   $scope.NowSuperAdmin();
			   $location.path("/superAdmin")
			   localStorage.setItem('userRole',11);
		   }else if(data.role == 22){
			   $location.path("/admin");
			   localStorage.setItem('userRole',22);
		   }else if(data.role == 33){
			   $location.path("/operator");
			   localStorage.setItem('userRole',33);
		   }else{
			   $location.path("/");
		   }
	   });
	   response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
	   });	
   }
   

 //Angular.js UI Started
	
	$scope.oneAtATime = true; //Accordion controller
	
	$scope.logout = function(){
		console.log("enter into logout function");
		$scope.operatorLogin = false;
		$scope.adminLogin = false;
		$scope.superAdminLogin = false;
		$rootScope.authenticated = false;
		$scope.logoutStatus	= true;
		console.log("false");
		var response = $http.post($scope.projectName+'/logout');
		response.success(function(data, status, headers, config){
			console.log("superAdmin as a Operator save on server---->"+data.result);
			AuthorisationService.reset();
			$scope.currentBranch = "";
			$scope.currentEmpName = "";
			$scope.userRights     = {};
			//localStorage.currentUser = AuthorisationService.getUser();
			localStorage.setItem('userRole',0);
			$window.location.href = $scope.projectName+"/";
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});	
	}
	
	
	/*window.addEventListener('focus', function (event) {
		console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	});*/
	
	window.addEventListener('storage', function (event) {
		  console.log("event.key = "+event.key);
		  console.log("event.newValue = "+event.newValue);
		  console.log("event.oldValue = "+event.oldValue);
		  //$location.path($scope.pathTest);
		/*  if(event.newValue == 11){
			  $scope.operatorLogin = false;
			  $scope.adminLogin = false;
			  $scope.superAdminLogin = true;
			  $scope.logoutStatus	= false;
			  //$location.path("/superAdmin");
			  $window.location.href = $scope.projectName+"/";
		  }else if(event.newValue == 22){
			  $scope.operatorLogin = false;
			  $scope.adminLogin = true;
			  $scope.superAdminLogin = false;
			  $scope.logoutStatus	= false;
			  //$location.path("/admin");
			  $window.location.href = $scope.projectName+"/";
		  }else if(event.newValue == 33){
			  $scope.operatorLogin = true;
			  $scope.adminLogin = false;
			  $scope.superAdminLogin = false;
			  $scope.logoutStatus	= false;
			  //$location.path("/operator");
			  $window.location.href = $scope.projectName+"/";
		  }else*/ if(event.newValue == 0){
			  console.log("*************************");
			  $scope.operatorLogin = false;
			  $scope.adminLogin = false;
			  $scope.superAdminLogin = false;
			  $scope.logoutStatus	= true;
			  $rootScope.authenticated = false;
			  $window.location.href = $scope.projectName+"/";
		  }/*else{
			  
		  }*/
		  
	});
	
	
	
	$scope.login = function(user) {
		console.log("enter into login function");
		/*var user = {
				"userCode" : "",
				"userName" : $scope.userName,
				"password" : $scope.password,
				"isAdmin"  : ""	
		};*/
		console.log("------------------->");
			var response = $http.post($scope.projectName+'/user', user);
			response.success(function(data, status, headers, config) {
				console.log("data-----"+data.targetPage)
				console.log("message----"+data.message);
				$scope.currentBranch = data.brhName;
				$scope.currentEmpName = data.empName;
				if(data.message === "incorrect password" || data.message === "User not exist"){
					$scope.successToast(data.message);
					user.userName = "";
					user.password = "";
				}else if(data.message === "this user has been inactivate by Super-Admin" || data.message === "User Authentication fail"){
					$scope.successToast(data.message);
					user.userName = "";
					user.password = "";
				}else if(data.message === "already logged in"){
					AuthorisationService.setUserCode(data.userCode);
					console.log(data.targetPage);
					$scope.alertToast("already logged in")
					if(data.targetPage === "superAdmin"){
						$location.path("/superAdmin");
						$scope.userRole();
					}else if(data.targetPage === "admin"){
						$location.path("/admin");
						$scope.userRole();
					}else if(data.targetPage === "operator"){
						$location.path("/operator");
						$scope.userRole();
					}else{
						$scope.alertToast("Internal Server Error");
					}
				}else if(data.message === "ERROR"){
					$scope.alertToast("Internal Server Error");
				}else{
					//$window.location.href = "/MyLogistics/"+data.targetPage;
					AuthorisationService.setUserCode(data.userCode);
					if(data.targetPage === "operator"){
						$scope.userRights     = data.userRights;
						$scope.userRole();
						//$scope.setUserRights(data.list);
						AuthorisationService.setUser(data.currentUser);
						localStorage.setItem('userRole', AuthorisationService.getUserRole());
						//$localStorage.currentUserRole = AuthorisationService.getUserRole();
						$rootScope.authenticated = true;
						$location.path("/operator");
					}	
					else if(data.targetPage === "admin"){
						$scope.userRole();
						AuthorisationService.setUser(data.currentUser);
						localStorage.setItem('userRole', AuthorisationService.getUserRole());
						//$localStorage.currentUserRole = AuthorisationService.getUserRole();
						$rootScope.authenticated = true;
						$location.path("/admin");			
					}	
					else if(data.targetPage === "superAdmin"){
						$scope.userRole();
						AuthorisationService.setUser(data.currentUser);
						localStorage.setItem('userRole', AuthorisationService.getUserRole());
						//$localStorage.currentUserRole = AuthorisationService.getUserRole();
						$rootScope.authenticated = true;
						$location.path("/superAdmin");
					}	
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}	
	
	
	$scope.checkAuthorisation = function(){
		console.log("enter into checkAuthorisation function");
		var currentUser = AuthorisationService.getUser();
		console.log("currentUser ------>"+currentUser);
		console.log("currentUser.userRole ------>"+currentUser.userRole);
		if(angular.isUndefined(currentUser.userRole) || angular.isUndefined(currentUser)){
			$scope.alertToast("Please login");
		}else{
			if(currentUser.userRole === 11){
				console.log("currentUser is superadmin --------------");
				$scope.userRole();
				$location.path("/superAdmin");
			}else if(currentUser.userRole === 22){
				$scope.userRole();
				$location.path("/admin");		
			}else if(currentUser.userRole === 33){
				$scope.userRole();
				$location.path("/operator");
			}else{
				$scope.alertToast("unauthorised person");
				console.log("unauthorised person");
			}
		}
	}
	
//Angular.js UI End
   
	$scope.checkAuthorisation();
	$scope.userRole();
	$scope.currentPath();
	
	
}]);