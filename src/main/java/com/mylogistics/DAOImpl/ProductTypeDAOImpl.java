package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ProductTypeDAO;
import com.mylogistics.model.ProductType;

public class ProductTypeDAOImpl implements ProductTypeDAO{
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public  ProductTypeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<String> getProductName(){
		List<String> productnameList = new ArrayList<String>();
	    try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(ProductType.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property("ptName"));
			 cr.setProjection(projList);
			 productnameList = cr.list();
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
	    session.clear();
		session.close();
	    return productnameList;
	 }
	
	
	@Transactional
	public int saveProductTypeToDB(ProductType producttype){
		int temp;
		try{
		     session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 session.save(producttype);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		session.clear();
	    session.close();
	    return temp;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<ProductType> getProductTypeData(){
		List<ProductType> producttypeList = new ArrayList<ProductType>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr=session.createCriteria(ProductType.class);
			 producttypeList=cr.list();
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return producttypeList;
	}
	
	@Transactional
	public int updateProductType(ProductType producttype){
		
		 System.out.println("Entered into updateProductType function in DAOImpl");
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);	 	
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  producttype.setCreationTS(calendar);
			  session.update(producttype);
			  transaction.commit();
			  session.flush();
			  temp=1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		 session.clear();
		session.close();
		return temp;		 
	 }
}
