'use strict';

var app = angular.module('application');

app.controller('RvrsLhpvBalCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.vs = {};
	$scope.lhpvBal = {};
	$scope.bnkList = [];
	$scope.chlnList = [];
	$scope.actBOList = [];
	$scope.lhpvBalList = [];
	
	$scope.bankCodeDBFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.brkOwnDBFlag = true;
	$scope.chlnDBFlag = true;
	$scope.lhpvBalDBFlag = true;
	$scope.saveVsFlag = true;
	
	$scope.arHoAlw = false;
	
	$scope.tdsPerFlag = false;
	$scope.lodingFlag = false;
	//$scope.Math = window.Math;
	
	var faCodeTemp = '';
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvBalDet');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   $scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_BAL_VOUCHER;
				   $scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   $scope.bnkList = data.bnkList;
				   
				   if($scope.vs.lhpvStatus.lsClose === true){
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
					   $('#verfChln').attr("disabled","disabled");
				   }else{
					   //$scope.getChlnAndBrOwn();
				   }
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	

	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$('#chequeNoId').removeAttr("disabled");
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'R'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankCodeDBFlag = true;
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	
	/*$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
			
		if($scope.vs.payBy === 'Q'){
			if(angular.isUndefined($scope.vs.chequeType) ||  $scope.vs.chequeType === null || $scope.vs.chequeType === ""){
				$scope.alertToast("please select cheque type");
			}else{
				var chqDet = {
						"bankCode" : bankCode,
						"CType"    : $scope.vs.chequeType
				};
				
				var response = $http.post($scope.projectName+'/getChequeNoFrLHBRvrs',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.chqList = data.list;
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						$scope.alertToast("Branch does not have '"+$scope.vs.chequeType+"' type cheeque of Bank "+bankCode);
						console.log("Error in bringing data from getChequeNo");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}
			
		}
		
 }*/
	
	
	
	/*$scope.openChqNoDB = function(){
		 console.log("enter into openChqNoDB function");
		 $scope.chqNoDBFlag = false;
	    	$('div#chqNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.chqNoDBFlag = true;
			    }
				});
			$('div#chqNoDB').dialog('open');
	 }*/
	
	
	
	 $scope.saveChln = function(chlnCode){
		 console.log("enter into saveChln fucniton");
		 $scope.challan = chlnCode;
		 
		 var duplicate = false;
		 if($scope.lhpvBalList.length > 0){
			 for(var i=0;i<$scope.lhpvBalList.length;i++){
				if($scope.lhpvBalList[i].challan.chlnCode === chlnCode){
					duplicate = true;
					break;
				}
			 }
		 }
		 
		 if(duplicate === false){
			 
			
			 
			 
			 
			/* var response = $http.post($scope.projectName+'/getChlnFrLB',data);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
			*/			
						
						$scope.lhpvBalDBFlag = false;
				    	$('div#lhpvBalDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							title:"LHPV BALANCE FOR CHALLAN ("+$scope.challan+")",
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.lhpvBalDBFlag = true;
						    }
							});
					$('div#lhpvBalDB').dialog('open'); 
		
				/*	}else{
						$scope.alertToast("server error");
						console.log("error in fetching getChlnFrLA data");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});*/
		 }else{
			 $scope.alertToast("Already create the lhpv Balance for challan "+$scope.challan);
		 }
	 }
	 
	
	 
	 $scope.submitLhpvBal = function(lhpvBalForm,lhBal){
		 console.log("enter into submitLhpvBal function = "+lhpvBalForm.$invalid);
		 if(lhpvBalForm.$invalid){
			$scope.alertToast("Please fill correct form");
		 }else{
		/*	 if($scope.lhpvBal.lbLryBalP >= 0 && $scope.actBal > 0){
				 $scope.alertToast("please fill the amount <= "+$scope.actBal);
			 }else{*/
				 $scope.lbFinalTotSum=$scope.lbFinalTotSum+$scope.lhpvBal.lbFinalTot;
				 $scope.lhpvBalList.push(lhBal);
				 $scope.lhpvBal = {};
				 $('div#lhpvBalDB').dialog('close'); 
			 //}
		 }
	 }
	 
	 $scope.removeLhpvBal = function(index,lhpvbal){
		 console.log("enter into removeLhpvBal function");
		 $scope.lbFinalTotSum=$scope.lbFinalTotSum-lhpvbal.lbFinalTot;
		 $scope.lhpvBalList.splice(index,1);
	 }
	 
	 
	 $scope.voucherSubmit = function(voucherForm){
		 console.log("enter into voucherForm function = "+voucherForm.$invalid);
		 if(voucherForm.$invalid){
			$scope.alertToast("Please enter correct form");
		 }else{
			 console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title:"LHPV BALANCE FINAL SUBMISSION",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $('div#saveVsDB').dialog('close');
	 } 
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function ");
			$scope.vs.lhpvBalList = $scope.lhpvBalList;
			$scope.lbFinalTotSum=0;
			faCodeTemp='';
			console.log($scope.vs)
			
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitLhpvBal',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					
					$scope.alertToast("Successfully save LHPV BALANCE -->> "+data.lbNo);
					
					$scope.vs = {};
					$scope.lhpvBal = {};
					$scope.bnkList = [];
					$scope.chlnList = [];
					$scope.actBOList = [];
					$scope.lhpvBalList = [];
					
					$scope.challan = "";
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					$scope.vs.chequeLeaves = {};
					$('#chequeNoId').attr("disabled","disabled");
					
					$scope.getLhpvDet();
				}else{
					console.log("Error in submitVoucher");
					$scope.alertToast("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 
	 $scope.verifyChallan = function() {
			console.log("verifyChallan()");
			if (angular.isUndefined($scope.challan) || $scope.challan === "" || $scope.challan === null) {
				$scope.alertToast("Please Enter Challan No");
			} else {
				
				$scope.lodingFlag = true;
				
				 if($scope.vs.payBy=='Q'){
					 var data = {
							 "payBy" 		: $scope.vs.payBy,
					 		 "bankCode"		: $scope.vs.bankCode,
					 		 "chqNo"		: $scope.vs.chequeLeaves.chqLChqNo,
							 "chlnCode"     : $scope.challan,
							 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
					 };
				 }else if($scope.vs.payBy=='R'){
					 
					 var data = {
							 "payBy" 		: $scope.vs.payBy,
					 		 "bankCode"		: $scope.vs.bankCode,
							 "chlnCode"     : $scope.challan,
							 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
					 };
				 }else if($scope.vs.payBy=='C'){
					 var data = {
							 "payBy" 		: $scope.vs.payBy,
							 "chlnCode"     : $scope.challan,
							 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
					 };
				 }
				
				
				var res = $http.post($scope.projectName+'/verifyChlnForLhpvBalRvrs',data);
				res.success(function(data, status, headers, config) {
					if (data.result === "success") {
						if (angular.isUndefined(faCodeTemp) || faCodeTemp === "" || faCodeTemp === null) {
							faCodeTemp = data.faCode;
							$scope.lbFinalTotSum=0;
							$scope.vs.brkOwnFaCode = data.faCode;
							$scope.lhpvBalList = [];
						} else {
							if (faCodeTemp === data.faCode) {
							} else {
								faCodeTemp = data.faCode;
								$scope.lbFinalTotSum=0;
								$scope.vs.brkOwnFaCode = data.faCode;
								$scope.lhpvBalList = [];
							}
						}
						
						$scope.lhpvBal.challan = data.challan;
						console.log(data.challan);
						console.log($scope.lhpvBal.challan.chlnCode);
						$scope.saveChln($scope.challan);
						
						$scope.lhpvBal.lbLryBalP=(-1)*(data.loryBalP);
						$scope.lhpvBal.lbCashDiscR=(-1)*(data.cashDisc);
						$scope.lhpvBal.lbMunsR=(-1)*(data.munsiana);
						$scope.lhpvBal.lbTdsR=(-1)*(data.tdsAmt);
						$scope.lhpvBal.lbWtShrtgCR=(-1)*(data.wtSrtg);
						$scope.lhpvBal.lbDrRcvrWtCR=(-1)*(data.drvrClaim);
						$scope.lhpvBal.lbLateDelCR=(-1)*(data.lateDlvr);
						$scope.lhpvBal.lbLateAckCR=(-1)*(data.lateAck);
						$scope.lhpvBal.lbOthExtKmP=(-1)*(data.extKM);
						$scope.lhpvBal.lbOthOvrHgtP=(-1)*(data.ovrHgt);
						$scope.lhpvBal.lbOthPnltyP=(-1)*(data.penalty);
						$scope.lhpvBal.lbOthMiscP=(-1)*(data.othrMisc);
						$scope.lhpvBal.lbUnpDetP=(-1)*(data.dtnsn);
						$scope.lhpvBal.lbUnLoadingP=(-1)*(data.unLding);
						$scope.lhpvBal.lbTotPayAmt=(-1)*(data.payAmt);
						$scope.lhpvBal.lbTotRcvrAmt=(-1)*(data.rcvryAmt);
						$scope.lhpvBal.lbFinalTot=(-1)*(data.finlTotAmt);
						//$scope.tdsPer=;
						$scope.lhpvBal.lbDesc=data.desc;
						
						
						
						
					} else {
						$scope.alertToast(data.msg);
					}
					$scope.lodingFlag = false;
				});
				res.error(function(data, status, headers, config) {
					console.log("error in response of verifyChlnForLhpvBal: "+data);
					$scope.lodingFlag = false;
				});
			}
		}
	 
	 
		if($scope.operatorLogin === true || $scope.superAdminLogin === true){
			 $scope.getLhpvDet();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
	
	
}]);