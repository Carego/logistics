package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Bill;
import com.mylogistics.model.BillForwarding;

@Repository
public interface BillForwardingDAO {

	public String saveBF(BillForwarding billForwarding);
	
	public List<BillForwarding> getBfFrBillSbmsn(int custId);
	
	public List<Bill> getBillListById(int bfId);
	
	public int saveBlSbmsnDt(BillForwarding billForwarding,Session session);
	
	public Map<String,Object> getBillFrwdPrint(String bfNo);

	public BillForwarding getPreviousBFbyCustBr(Integer custId, String branchCode);
}
