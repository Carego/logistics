<div ng-show="operatorLogin || superAdminLogin">

	<div class="row" >
		<form ng-submit="billMainSubmit()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">	
			
			<div class="row">
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="custCodeId" name ="custCodeName" ng-model="btMain.btmCustCode" ng-required="true">
		       		<label for="code">Customer Code</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="panNoId" name ="panNoName" ng-model="btMain.btmPanNo" >
		       			<label for="code">Pan No</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="billNoId" name ="billNoName" ng-model="btMain.btmBillNo" >
		       		<label for="code">Bill No</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="Date" id="billDateId" name ="billDate" ng-model="btMain.btmBillDt" >
		       			<label for="code">Bill Date</label>	
		       		</div>
		       		
		       		
		    </div>
			
			<div class="row" ng-if="billTableFlag && btDetailList.length>0">
				<table class="tblrow">
		       		
		       		<caption class="coltag tblrow">Bill Detail</caption> 
		       		
		            <tr class="rowclr">
			            <th class="colclr">SNo</th>
			            <th class="colclr">Cnmt Date</th>
			            <th class="colclr">Cnmt No</th>
			            <th class="colclr">from Stn</th>
			            <th class="colclr">To Stn</th>
			            <th class="colclr">Weigt(MT)</th>
			            <th class="colclr">Rate</th>
			            <th class="colclr">Freight</th>
			            <th class="colclr">Detention</th>
			            <th class="colclr">Bonus</th>
			            <th class="colclr">LdngUnLdng</th>
			            <th class="colclr">Total</th>
			            <th class="colclr">Action</th>
			        </tr>	
			          
			        <tr ng-repeat="btDet in btDetailList track by $index">
			            <td class="rowcel">{{$index+1}}</td>
			            <td class="rowcel">{{btDet.btdDate | date:'dd/MM/yyyy'}}</td>
			            <td class="rowcel">{{btDet.btdCnmtNo}}</td>
			            <td class="rowcel">{{btDet.btdFromStn}}</td>
			            <td class="rowcel">{{btDet.btdToStn}}</td>
			            <td class="rowcel">{{btDet.btdWeightMt}}</td>
			            <td class="rowcel">{{btDet.btdRate}}</td>
			            <td class="rowcel">{{btDet.btdFreight}}</td>
			            <td class="rowcel">{{btDet.btdDetention}}</td>
			            <td class="rowcel">{{btDet.btdBonus}}</td>
			            <td class="rowcel">{{btDet.btdLdngNUnldng}}</td>
			            <td class="rowcel">{{btDet.btdTotal}}</td>
			            <td class="rowcel"><input type="button" value="Remove" ng-click="removebillTempDet(btDet, $index)"/></td>
			        </tr>
		         
		        </table>
	    	</div>
	    	</br>
	    	<div class="row">
					
				<div class="col s4 input-field">
	       				<input class="col s12 btn teal white-text" type ="button" value="Add" ng-click="addBill()" >
	       		</div>
					
				<div class="col s4 input-field">
					<input class="validate" type="number" id="subTotalId" name="subTotalName" ng-model="btMain.btmSubTotal" readonly ng-required="true"> 
					<label for="code">Sub Total</label>
				</div>
				
				<div class="col s4 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" value="Submit" >
	       		</div>
			</div>
	    	
	    </form>
	          
	</div>
	
	<div id ="billDetailDB" ng-hide="billDetailDBFlag" >
			
		<form name="billDetailForm" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">		
			<div class="row">
				
				<div class="col s4 input-field">
		       		<input class="validate" type ="Date" id="btdDateId" name ="btdDateName" ng-model="btDetail.btdDate" >
		       	<label for="code">Cnmt Date</label>	
		       	</div>
		       		
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="btdCnmtNoId" name ="btdCnmtNoName" ng-model="btDetail.btdCnmtNo" >
		       	<label for="code">Cnmt No</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="btdFromStnId" name ="btdFromStnName" ng-model="btDetail.btdFromStn" >
		       	<label for="code">From Stn</label>	
		       	</div>
				
			</div>
			
			<div class="row">
				
				<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="btdToStnId" name ="btdToStnName" ng-model="btDetail.btdToStn" >
		       	<label for="code">To Stn</label>	
		       	</div>
		       		
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdWeightMtId" name ="btdWeightMtName" ng-model="btDetail.btdWeightMt" ng-keyup="weigth()" >
		       	<label for="code">Weigt(MT)</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdRateId" name ="btdRateName" ng-model="btDetail.btdRate" ng-keyup="rate()">
		       	<label for="code">Rate</label>	
		       	</div>
				
			</div>
			
			<div class="row">
				
				<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdFreightId" name ="btdFreightName" ng-model="btDetail.btdFreight" readonly>
		       	<label for="code">Freight</label>	
		       	</div>
		       		
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdDetentionId" name ="btdDetentionName" ng-init="btDetail.btdDetention=0" ng-model="btDetail.btdDetention" ng-keyup="detention()" >
		       	<label for="code">Detention</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdBonusId" name ="btdBonusName" ng-init="btDetail.btdBonus=0" ng-model="btDetail.btdBonus" ng-keyup="bonus()">
		       	<label for="code">Bonus</label>	
		       	</div>
				
			</div>
			
			<div class="row">
				
				<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdLdngNUnldngId" name ="btdLdngNUnldngName" ng-init="btDetail.btdLdngNUnldng=0" ng-model="btDetail.btdLdngNUnldng" ng-keyup="ldngNUnldng()">
		       	<label for="code">LdngUnLdng</label>	
		       	</div>
		       		
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="number" id="btdTotalId" name ="btdTotalName" ng-model="btDetail.btdTotal" readonly ng-required="true">
		       	<label for="code">Total</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
	       			<input class="col s12 btn teal white-text" type ="button" value="Submit" ng-click=billDetailSubmit() >
	       		</div>
		    	
			</div>
		
		</form>	
	</div>
	
	
</div>
