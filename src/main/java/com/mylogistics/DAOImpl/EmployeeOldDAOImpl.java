package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.EmployeeOldDAO;
import com.mylogistics.constants.EmployeeOldCNTS;
import com.mylogistics.model.EmployeeOld;

public class EmployeeOldDAOImpl implements EmployeeOldDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	public EmployeeOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public int saveEmployeeOldToDB(EmployeeOld employeeOld) {
		System.out.println("Entered into saveEmployeeOldToDB function---");
		int tmp;
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(employeeOld);
			transaction.commit();
			tmp = 1;
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			tmp = -1;
		}
		session.clear();
		session.close();
		return tmp;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<EmployeeOld> getModifiedEmp(String empCode) {
		List<EmployeeOld> empList = new ArrayList<EmployeeOld>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(EmployeeOld.class);
			cr.add(Restrictions.eq(EmployeeOldCNTS.EMP_CODE, empCode));
			empList = cr.list();
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return empList;
	}
}
