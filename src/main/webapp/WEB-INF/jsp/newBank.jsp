<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newBankForm" ng-submit=bankSubmit(newBankForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bnkNameId" name ="bnkName" ng-model="bnkMstr.bnkName" ng-click="openBnkNameDB()" readonly ng-required="true" >
		       		<label for="code">Name</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bnkAcNoId" name ="bnkAcNo" ng-model="bnkMstr.bnkAcNo" ng-minlength="8" ng-maxlength="25" ng-required="true">
		       			<label for="code">A/c no</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="col s12 btn teal white-text" type ="button" id="bnkAddId" name ="bnkAdd" value="Fill Address" ng-click="OpenBnkAddDB()" required readonly >
		       			<label for="code">Address</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="bnkOpenDtId" name ="bnkOpenDt" ng-model="bnkMstr.bnkOpenDt" ng-required="true" >
		       			<label for="code">Open Date</label>	
		       		</div>
	
					<!--Add Micr and Ifc code here  -->
			   		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bnkIfscCodeId" name ="bnkIfscCode" ng-model="bnkMstr.bnkIfscCode" ng-required="true" >
		       			<label for="code">Bank Ifsc Code</label>
		       		</div>
		       		
			      	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bnkMicrCodeId" name ="bnkMicrCode" ng-model="bnkMstr.bnkMicrCode"  >
		       			<label for="code">Bank Micr Code</label>	
		       		</div>
		    </div>
		    <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="bnkLocal" id="bnkLocalId" ng-model="bnkMstr.bnkLocal" required  ng-change="selectBnkLocal()">
							<option value='local'>Local</option>
							<option value='multicity'>Multicity</option>
						</select>
						<label>Local Type</label>
					</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="bnkBrMinChqId" name ="bnkBrMinChq" ng-model="bnkMstr.bnkBrMinChq" ng-required="true">
		       			<label for="code">Branch Min Cheque</label>	
		       		</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="bnkBrMaxChqId" name ="bnkBrMaxChq" ng-model="bnkMstr.bnkBrMaxChq" ng-required="true">
		       			<label for="code">Branch Max Cheque</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
	     		
	     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="bnkBalanceAmtId" name ="bnkBalanceAmt" ng-model="bnkMstr.bnkBalanceAmt" ng-required="true">
		       			<label for="code">Balance Amt</label>	
			 	</div>
	     		
	     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="bnkHoMinChqId" name ="bnkHoMinChq" ng-model="bnkMstr.bnkHoMinChq" ng-required="true">
		       			<label for="code">HO Min Cheque</label>	
			 	</div>
	   	
	   		 	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="bnkHoMaxChqId" name ="bnkHoMaxChq" ng-model="bnkMstr.bnkHoMaxChq" ng-required="true">
		       			<label for="code">HO Max Cheque</label>	
   		 	 	</div>	
	   		 </div>
		    
	      	 <div class="row">
	     		
	     		<div class="input-field col s6">
					<input class="col s12 btn teal white-text" type="button" id="addSSId" value="Add Single Signatory" ng-click="OpenSingleSList()"> 
				</div>
				
				<div class="input-field col s6">
					<input class="col s12 btn teal white-text" type="button" id="addJSId" value="Add Joint Signatory" ng-click="OpenJointSList()"> 
				</div>
	     		
	       	 </div>
	       	
	       	 <!--Single signatory  -->
	       	 <div class="row" ng-if=" finalSSList.length > 0">
				<table class="tblrow">
				  <caption class="coltag tblrow">SINGLE SIGNATORY</caption>
		 	  	  <tr class="rowclr">
		 	  	  	  <th class="colclr"> Employee Name</th>
		 	  	 	  <th class="colclr"> Employee Code</th>
		 	  	 	  <th class="colclr"> Action</th>
		 	  	  </tr>
		 	  
				  <tr ng-repeat="finalSS in finalSSList">
		              <td class="rowcel">{{finalSS.empName}}</td>
		              <td class="rowcel">{{finalSS.empCodeTemp}}</td>
		              <td class="rowcel"><input class="col s12 btn teal white-text" type="button" value="Remove" ng-click="removeSingleS($index)"></td>
		          </tr>
		      </table> 
			</div>
			
			<!--Joint signatory  -->
			<div class="row" ng-if=" finalJSList.length > 0">
				<table class="tblrow">
				<caption class="coltag tblrow">JOINT SIGNATORY</caption>
		 	  	  <tr class="rowclr">
		 	  	  	  <th class="colclr"> Employee Name</th>
		 	  	 	  <th class="colclr"> Employee Code</th>
		 	  	 	  <th class="colclr"> Action</th>
		 	  	  </tr>
		 	  
				  <tr ng-repeat="finalJS in finalJSList">
		              <td class="rowcel">{{finalJS.empName}}</td>
		              <td class="rowcel">{{finalJS.empCodeTemp}}</td>
		              <td class="rowcel"><input class="col s12 btn teal white-text" type="button" value="Remove" ng-click="removeJointS($index)"></td>
		          </tr>
		      </table> 
			</div>
			
			 <div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
	       	 
		  </form>
    </div>
    
    <div id ="bnkNameDB" ng-hide="bnkNameDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.bnkName" placeholder="Search by Bank Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Name </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bankNames | filter:filterTextbox.bnkName ">
		 	  <td><input type="radio"  name="bnk" value="{{ bnk }}" ng-model="bnkNm" ng-click="saveBnkName(bnk.bnkName)"></td>
              <td>{{bnk.bnkName}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="bnkAddDB" ng-hide = "bnkAddDBFlag">
		<form name = "bnkAddForm" ng-submit="saveBnkAdd(bnkAddForm)">
			<table>
				<tr>
					<td>Address *</td>
					<td><input type ="text" id="addressId" name ="address" ng-model="bnkAdd.completeAdd" ng-required="true" ng-minlength="3" ng-minlength="255"></td>
				</tr>
				
				<tr>
					<td>Address City *</td>
					<td><input type ="text" id="addCityId" name ="addCity" ng-model="bnkAdd.addCity" ng-required="true" ng-minlength="3" ng-minlength="15"></td>
				</tr>
				
				<tr>
					<td>Address State *</td>
					<td><input type ="text" id="addStateId" name ="addState" ng-model="bnkAdd.addState" ng-required="true" ng-minlength="3" ng-minlength="40"></td>
				</tr>
				
				<tr>
					<td>Address Pin *</td>
					<td><input type ="text" id="addPinId" name ="addPin" ng-model="bnkAdd.addPin" ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
				</tr>
				
				<tr>
					<td><input type="submit"  value="Submit" ></td>
				</tr>
			</table>
		</form>
	</div>

	<!-- Authorise employee one -->
	<!-- <div id="openEmp1DBDIV" ng-hide="openEmp1DBFlag">
		<input type="text" name="filterEmp1Name" ng-model="filterEmp1Name.Emp1Temp"	placeholder="Search by Employee name">
		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

				<th>Employee Code Temp</th>

			</tr>

			<tr	ng-repeat="employee in employees | filter:filterEmp1Name">
				<td><input type="radio" name="emp2Name" class="employee2" value="{{ employee }}" ng-model="emp1"	ng-click="saveEmp1(employee)"></td>
				<td>{{ employee.empCode }}</td>
				<td>{{ employee.empName }}</td>
				<td>{{ employee.branchCode }}</td>
				<td>{{ employee.empCodeTemp }}</td>
			</tr>
		</table>

	</div> -->
	
	<!-- Authorise employee Two -->
	<!-- <div id="openEmp2DBDIV" ng-hide="openEmp2DBFlag">
		<input type="text" name="filterEmp2Name" ng-model="filterEmp2Name.Emp2Temp"	placeholder="Search by Employee name">
		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

				<th>Employee Code Temp</th>

			</tr>

			<tr	ng-repeat="employee in employees | filter:filterEmp2Name">
				<td><input type="radio" name="emp2Name" class="employee2" value="{{ employee }}" ng-model="emp2" ng-click="saveEmp2(employee)"></td>
				<td>{{ employee.empCode }}</td>
				<td>{{ employee.empName }}</td>
				<td>{{ employee.branchCode }}</td>
				<td>{{ employee.empCodeTemp }}</td>
			</tr>
		</table> -->

	</div>
	
	<div id="singleSDB" ng-hide="singleSDBFlag">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
			<tr>
				<th></th>
				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

				<th>Employee FaCode</th>
			</tr>

			<tr ng-repeat="employee in employees | filter:filterTextbox ">
		 	  <td><input type="radio"  name="ssName"  class="station"  value="{{ employee }}"  ng-model="ss1" ng-click="saveSingleS(employee)"></td>
				<td>{{ employee.empCode }}</td>
				<td>{{ employee.empName }}</td>
				<td>{{ employee.branchCode }}</td>
				<td>{{ employee.empFaCode }}</td>
			</tr>
      </table> 
	</div>
	
	<div id="jointSDB" ng-hide="jointSDBFlag">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
			<tr>
				<th></th>
				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

				<th>Employee Code Temp</th>
			</tr>

			<tr ng-repeat="employee in employees | filter:filterTextbox ">
		 	  <td><input type="radio"  name="jsName"  class="station"  value="{{ employee }}"  ng-model="js1" ng-click="saveJointS(employee)"></td>
				<td>{{ employee.empCode }}</td>
				<td>{{ employee.empName }}</td>
				<td>{{ employee.branchCode }}</td>
				<td>{{ employee.empCodeTemp }}</td>
			</tr>
      </table> 
	</div>
	
	
	
	<div id="saveBnkDBDIV" ng-hide="saveBnkFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Name</td>
					<td>{{bnkMstr.bnkName}}</td>
				</tr>
				
				<tr>
					<td>Account No</td>
					<td>{{bnkMstr.bnkAcNo}}</td>
				</tr>
				
				<tr>
					<td>Open Date</td>
					<td>{{bnkMstr.bnkOpenDt}}</td>
				</tr>
				
				<tr>
					<td>IFSC Code</td>
					<td>{{bnkMstr.bnkIfscCode}}</td>
				</tr>
				
				<tr>
					<td>MICR Code</td>
					<td>{{bnkMstr.bnkMicrCode}}</td>
				</tr>
				
				<tr>
					<td>Local Type</td>
					<td>{{bnkMstr.bnkLocal}}</td>
				</tr>
				
				<tr>
					<td>Balance Amt</td>
					<td>{{bnkMstr.bnkBalanceAmt}}</td>
				</tr>
				
				
				<tr>
					<td>Branch Min Cheque</td>
					<td>{{bnkMstr.bnkBrMinChq}}</td>
				</tr>
				
				<tr>
					<td>Branch Max Cheque</td>
					<td>{{bnkMstr.bnkBrMaxChq}}</td>
				</tr>
				
				<tr>
					<td>Ho Min Cheque</td>
					<td>{{bnkMstr.bnkHoMinChq}}</td>
				</tr>
				
				<tr>
					<td>Ho Max Cheque</td>
					<td>{{bnkMstr.bnkHoMaxChq}}</td>
				</tr>
				
				<tr>
					<td><h5>Complete ADDRESS</h5></td>
					<td></td>
				</tr>
				
				<tr>
					<td>Address</td>
					<td>{{bnkAdd.completeAdd}}</td>
				</tr>
				
				<tr>
					<td>City</td>
					<td>{{bnkAdd.addCity}}</td>
				</tr>
				
				<tr>
					<td>State</td>
					<td>{{bnkAdd.addState}}</td>
				</tr>
				
				<tr>
					<td>Pin</td>
					<td>{{bnkAdd.addPin}}</td>
				</tr>
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveBnk()"/>
		</div>
	</div>

</div>
