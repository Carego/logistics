package com.mylogistics.model.webservice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "image_app")
public class ImageApp {
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "decPath")
	private String decPath;
	@Column(name = "panPath")
	private String panPath;
	@Column(name = "type")
	private String type;
	@Column(name = "ownBrkId")
	private Integer ownBrkId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDecPath() {
		return decPath;
	}
	public void setDecPath(String decPath) {
		this.decPath = decPath;
	}
	public String getPanPath() {
		return panPath;
	}
	public void setPanPath(String panPath) {
		this.panPath = panPath;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getOwnBrkId() {
		return ownBrkId;
	}
	public void setOwnBrkId(Integer ownBrkId) {
		this.ownBrkId = ownBrkId;
	}
	
}