package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.exception.custom.BankMstrException;
import com.mylogistics.exception.custom.CashStmtStatusException;
import com.mylogistics.exception.custom.CustomerException;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.MoneyReceiptService;
import com.mylogistics.services.UtilityService;

@Controller
public class OnAccMRCntlr {

	public static final Logger log = Logger.getLogger(OnAccMRCntlr.class);
	
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	@Autowired
	private CustomerDAO customerDAO;
	@Autowired
	private BankMstrDAO bankMstrDAO;
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	@Autowired
	private MoneyReceiptService moneyReceiptService;
	@Autowired
	private UtilityService utilityService;
	@Autowired
	@Qualifier("onAccountingMRValidator")
	private Validator onAccountingMRValidator;

	
	
	@RequestMapping(value = "/getMrDetail", method = RequestMethod.POST)
	public @ResponseBody Object getMrDetail() {
		System.out.println("enter into getMrDetail function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				map.put("list",custList);
				List<Map<String, Object>> hoBankList = new ArrayList<>();
				if(currentUser.getUserBranchCode().equalsIgnoreCase("1")){
					System.out.println("HO user set in session");
				}else{
					hoBankList =  bankMstrDAO.getBankNCAIList("1");
				}
				List<Map<String, Object>> bankList = bankMstrDAO.getBankNCAIList(currentUser.getUserBranchCode());
				//System.out.println("size of bankList = "+bankList.size());
				List<Map<String, Object>> actBankList = new ArrayList<>();
				
				actBankList = hoBankList;
				System.out.println("size of actBankList = "+actBankList.size());
				if(!bankList.isEmpty()){
					for(int i=0;i<bankList.size();i++){
						actBankList.add(bankList.get(i));
					}
				}
				System.out.println("size of actBankList = "+actBankList.size());
				map.put("bnkList",actBankList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	
	@RequestMapping(value = "/mrSubmit", method = RequestMethod.POST)
	public @ResponseBody Object mrSubmit(@RequestBody MoneyReceipt moneyReceipt){
		System.out.println("enter into mrSubmit function");
		Map<String,Object> map = new HashMap<>();
		int res = moneyReceiptDAO.duplicateMR(moneyReceipt);
		System.out.println("result = "+res);
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		/*String mrNo = moneyReceiptDAO.saveOAMR(moneyReceipt);
		if(mrNo != null){
			map.put("mrNo",mrNo);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}*/
		return map;
	}
	
	
	@RequestMapping(value = "/mrFinalSubmit", method = RequestMethod.POST)
	public @ResponseBody Object mrFinalSubmit(@RequestBody MoneyReceipt moneyReceipt){
		System.out.println("enter into mrFinalSubmit function");
		Map<String,Object> map = new HashMap<>();
		if(moneyReceipt.getMrDate() != null){
			String mrNo = moneyReceiptDAO.saveOAMR(moneyReceipt);
			if(mrNo != null){
				map.put("mrNo",mrNo);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/mrFinalSubmitN", method = RequestMethod.POST)
	public @ResponseBody Object mrFinalSubmitN(@RequestBody MoneyReceipt moneyReceipt, BindingResult validationErrors){
		log.info("enter into mrFinalSubmit function");
		Map<String,Object> map = new HashMap<>();
		try {
			onAccountingMRValidator.validate(moneyReceipt, validationErrors);
			if (validationErrors.hasErrors()) {
				utilityService.setValidationList(map, validationErrors.getAllErrors());
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			else {
				String mrNo = moneyReceiptService.saveOAMR(moneyReceipt);
				map.put("mrNo",mrNo);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		} 
		catch (CashStmtStatusException e) {
			log.error(e.getMessage(),  e);
			utilityService.setExceptionMsg(map, CashStmtStatusCNTS.NOT_FOUND);
		}
		catch (CustomerException e) {
			log.error(e.getMessage(),  e);
			utilityService.setExceptionMsg(map, CustomerCNTS.NOT_FOUND);
		}
		catch (BankMstrException e) {
			log.error(e.getMessage(),  e);
			utilityService.setExceptionMsg(map, BankMstrCNTS.NOT_FOUND);
		}
		catch (Exception e) {
			log.error(e.getMessage(),  e);
			utilityService.setExceptionMsg(map, ConstantsValues.EXCEPTION_MSG);		
		}
		
		return map;
	}	
	
	//TODO Back
	@RequestMapping(value = "/getMrDetailBack", method = RequestMethod.POST)
	public @ResponseBody Object getMrDetailBack() {
		System.out.println("enter into getMrDetail function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = null;
//		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				map.put("list",custList);
				List<Map<String, Object>> hoBankList = new ArrayList<>();
				if(currentUser.getUserBranchCode().equalsIgnoreCase("1")){
					System.out.println("HO user set in session");
				}else{
					hoBankList =  bankMstrDAO.getBankNCAIList("1");
				}
				List<Map<String, Object>> bankList = bankMstrDAO.getBankNCAIList(currentUser.getUserBranchCode());
				//System.out.println("size of bankList = "+bankList.size());
				List<Map<String, Object>> actBankList = new ArrayList<>();
				
				actBankList = hoBankList;
				System.out.println("size of actBankList = "+actBankList.size());
				if(!bankList.isEmpty()){
					for(int i=0;i<bankList.size();i++){
						actBankList.add(bankList.get(i));
					}
				}
				System.out.println("size of actBankList = "+actBankList.size());
				map.put("bnkList",actBankList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	
}
