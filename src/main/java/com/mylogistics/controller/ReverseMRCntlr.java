package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class ReverseMRCntlr {

	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	
	@RequestMapping(value = "/getRvMrDetail" , method = RequestMethod.POST)  
	public @ResponseBody Object getRvMrDetail() {  
		System.out.println("Enter into getRvMrDetail---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				List<Branch> brhList = branchDAO.getAllActiveBranches();
				map.put("list",custList);
				map.put("brhList",brhList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getOnAccFrRev" , method = RequestMethod.POST)  
	public @ResponseBody Object getOnAccFrRev(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into getOnAccFrRev---->");
		Map<String, Object> map = new HashMap<>();	
		int custId = Integer.parseInt(String.valueOf(clientMap.get("custId")));
		int brhId = Integer.parseInt(String.valueOf(clientMap.get("brhId")));
		if(custId>0 && brhId>0){
			List<MoneyReceipt> mrList = moneyReceiptDAO.getOnAccFrRev(custId,brhId);
			if(!mrList.isEmpty()){
				map.put("list",mrList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{ 
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/revMrSubmit" , method = RequestMethod.POST)  
	public @ResponseBody Object revMrSubmit(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into getOnAccFrRev---->");
		return moneyReceiptDAO.saveOnAccRev(clientMap);
	}
	
	
	
	@RequestMapping(value = "/getRvMrDetailBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getRvMrDetailBack() {  
		System.out.println("Enter into getRvMrDetailBack---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				List<Branch> brhList = branchDAO.getAllActiveBranches();
				map.put("list",custList);
				map.put("brhList",brhList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
}
