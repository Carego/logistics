package com.mylogistics.controller;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.Acknowledge_ValidationDao;
import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.model.Acknowledge_Validation;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Customer;
import com.mylogistics.services.ConstantsValues;

@Controller
public class AckValidatorCntlr {
	
	@Autowired
	Acknowledge_ValidationDao acknowledge_validationDao;
	
	@Autowired
	CnmtDAO cnmtDao;
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Autowired
	Cnmt_ChallanDAO cnmt_ChallanDAO;
	
	@Autowired
	ChallanDAO challanDAO;
	
	@Autowired
	ArrivalReportDAO arrivalReportDAO;
	
	@Autowired 
	BranchDAO branchDAO;

	
	Blob blob;

	@RequestMapping(value="/getAckValidatorData",method=RequestMethod.POST)
	public @ResponseBody Object getDataFromAckValidation(@RequestBody Map<String, Object> map)
	{
		Date fromDate=Date.valueOf(map.get("fromDate").toString());
		Date toDate=Date.valueOf(map.get("toDate").toString());
		String remarkType=String.valueOf(map.get("remarkType"));
		System.out.println("Form Date   "+fromDate);
		System.out.println("TO Date   "+toDate);
		
		Map<String, Object> responseMap=new HashMap<String, Object>();
	List<Acknowledge_Validation> validationList=	acknowledge_validationDao.getAckValidatorFromTODate(fromDate, toDate,remarkType);
	System.out.println("Size of List  "+validationList.size());
	if(!validationList.isEmpty())
	{
	responseMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	responseMap.put("ackValidationData", validationList);
	}
	else
	{
		responseMap.put(ConstantsValues.RESULT, "No data Found");
	}
	
	return responseMap;	
	}
	
	@RequestMapping(value="/getCnmtPdfFile",method=RequestMethod.POST)
	public @ResponseBody Object getCnmtPdfFile(@RequestBody String cnmtCode)
	{
		System.out.println(cnmtCode);
		HashMap<String, Object> responseMap=new HashMap<>();
	List<Cnmt> cnmtList=cnmtDao.getCnmt(cnmtCode);
     
         Cnmt cnmt=	cnmtList.get(0);
         if(cnmt!=null)
         {
        responseMap.put("cnmt", cnmt);
        String consignorcode=cnmt.getCnmtConsignor();
        String consigneecode=cnmt.getCnmtConsignee();
     List<Customer>  customerList=  customerDAO.getAllCustomer();
     Map<String, String> custMap=new HashMap<>();
   for (Customer customer : customerList) {
	 custMap.put(customer.getCustCode(), customer.getCustName());
      }
   System.out.println("Size of CustMap "+custMap.size());
   
     int cnmtId=   cnmt.getCnmtId();
    List<Integer> chlnIdList= cnmt_ChallanDAO.getChlnIdByCnmtId(cnmtId);
    System.out.println(chlnIdList.size());
    int chlnId=chlnIdList.get((chlnIdList.size()-1));
    System.out.println("Challan Id "+chlnId);
    
    List<Challan> challanList= challanDAO.getChallanById(chlnId);
    System.out.println("Size of Challan List   "+challanList.size());
     Challan challan=challanList.get(0);
     if(challan!=null)
     {
    	int arId= challan.getChlnArId();
    	if(arId>0)
    	{
      ArrivalReport arrivalReport=arrivalReportDAO.getArById(arId);	
      
       String branchId=  arrivalReport.getBranchCode();
       int brhId=Integer.parseInt(branchId);
       Branch branch=  branchDAO.getBranchById(brhId);
       responseMap.put("branchName", branch.getBranchName());
       responseMap.put("arrivalReport", arrivalReport);
    	}
    	 responseMap.put("challan", challan);	 
     }
   responseMap.put("consignee", custMap.get(consigneecode));
   responseMap.put("consignor", custMap.get(consignorcode));
          }
	int cmId=cnmtList.get(0).getCmId();
	List<CnmtImage> cnmtImageList=acknowledge_validationDao.getCnmtImage(cmId);
	String imagePath=cnmtImageList.get(0).getCnmtImgPath();
//	String imagePath="E:\\mypdf.pdf";
	System.out.println(imagePath);
	try {
	byte[]imageData=	Files.readAllBytes(Paths.get(imagePath));
	if(imageData!=null)
	{
	blob=new SerialBlob(imageData);
	responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);

	}
	else
	{
		responseMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);	
	}
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SerialException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	
		return responseMap;
	}
	
	@RequestMapping(value="/openCnmtPdf",method=RequestMethod.POST)
	public @ResponseBody void openCnmtPdfFile(HttpServletResponse response)
	{
		try {
		ServletOutputStream outputStream=	response.getOutputStream();
		outputStream.write(blob.getBytes(1, (int) blob.length()));
		outputStream.close();
		blob = null;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/updateAckValidation",method=RequestMethod.POST)
	public @ResponseBody Object updateAckValidation(@RequestBody Map<String, Object> map)
	{
		System.out.println(map);
	List<Acknowledge_Validation> acknowledge_Validations=	acknowledge_validationDao.getAckByCnmtCode(map.get("cnmtCode").toString());
	LinkedHashMap<String, Object> arrivalReport=(LinkedHashMap<String, Object>)map.get("arrivalReport"); 
	 System.out.println("ArrivalDate is  "+arrivalReport);
	 String chlnCode=map.get("challan").toString();
	String consignor=map.get("consignor").toString();
	String consignee=map.get("consignee").toString();
	Time lryArTime=Time.valueOf(map.get("lryArTime").toString()+":00");
	Date lrarrivalDate=Date.valueOf(map.get("lrarrivalDate").toString());
	Date cnmtDate=Date.valueOf(map.get("cnmtDate").toString());
	Time unloadingTime=Time.valueOf(map.get("uploadingTime").toString()+":00");
	double unloadingAmt=Double.parseDouble(map.get("unloadingAmt").toString());
	Date lrUnloadingdate=Date.valueOf(map.get("lrUnloadingdate").toString());
	double detention=Double.parseDouble(map.get("detention").toString());
	double othAmt=Double.parseDouble(map.get("othAmt").toString());
	int days=Integer.parseInt(map.get("days").toString());
	double recWeight=Double.parseDouble(map.get("recWeight").toString());
	int recPkg=Integer.parseInt(map.get("recPkg").toString());
	String remarks=map.get("remarks").toString();
	String damOrShrtg=map.get("damOrShrtg").toString();
	String weightOrPkg=map.get("weightOrPkg").toString();
    int claimPkg=Integer.parseInt(map.get("claimPkg").toString());
	double claimwght=Double.parseDouble(map.get("claimwght").toString());
	double gdClaimAmt=Double.parseDouble(map.get("gdClaimAmt").toString());
	String reason=map.get("reason").toString();
	Acknowledge_Validation validation=	acknowledge_Validations.get(0);
                System.out.println("AckId is "+validation.getAckId());
                
                if(validation.getRemarks()!=null)
                {
                	if(validation.getRemarks().equalsIgnoreCase("QUALIFIED"))
                	{
                	  validation.setNewRemarks(remarks);	
                	}
                }
                else
                {
                	 validation.setRemarks(remarks);
                }
                if(validation.getReason()!=null)
                {
                
                	  validation.setNewReason(reason);	
                }
                else
                {
                    validation.setReason(reason);
                }
                System.out.println("Remarks  "+remarks);
                if(remarks.equals("QUALIFIED"))
                {
                	System.out.println("i am in Qualified ");
                validation.setCheck("N");
              //  validation.setRemarks(remarks);
                }
                else
                {
                	System.out.println("Remark uis not qualified");
                	 validation.setCheck("Y");
                	// validation.setRemarks(remarks);
                }
         
                
               validation.setArDate(Date.valueOf(arrivalReport.get("arDt").toString()));
               validation.setChlnNo(chlnCode);
               validation.setClaimAmtVOG(gdClaimAmt);
               validation.setClaimPkg(claimPkg);
               validation.setClaimWeight(claimwght);
               validation.setDaysdiff(days);
               validation.setDetention(detention);
               validation.setDmOrShrtg(damOrShrtg);
               validation.setLrArrivalDate(lrarrivalDate);
               validation.setLrArrivalTime(lryArTime);
               validation.setLrUnloadingDt(lrUnloadingdate);
               validation.setOtherAmt(othAmt);
          
               validation.setRecPackage(recPkg);
               validation.setRecWeight(recWeight);
              
               validation.setCnmtDate(cnmtDate);
               validation.setSedrNo(arrivalReport.get("arCode").toString());
               validation.setUnloadingAmt(unloadingAmt);
               validation.setUnloadingTime(unloadingTime);
               validation.setWeightOrPkg(weightOrPkg);
               validation.setCngrCode(Integer.parseInt(map.get("consignorCode").toString()));
               validation.setCngeCode(Integer.parseInt(map.get("consigneeCode").toString()));
           //  validation.setCngeCode(customerDAO.getCustCode(consignee, String.valueOf(validation.getBcode())));
         //    validation.setCngrCode(customerDAO.getCustCode(consignor, String.valueOf(validation.getBcode())));  
            ArrivalReport arReport=  arrivalReportDAO.getArById(Integer.parseInt(arrivalReport.get("arId").toString()));
            arReport.setArRcvWt((int)recWeight);
            arReport.setArPkg(recPkg);
            arReport.setArRemWtShrtg(gdClaimAmt);
            arReport.setArWtShrtg(gdClaimAmt);
            arReport.setArClaim(arReport.getArDrRcvrWt()+arReport.getArLateAck()+arReport.getArLateDelivery()+arReport.getArOther());
           acknowledge_validationDao.updateAckValidation(validation.getAckId(), validation,arReport);
           
          return null;
	}
}
