<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		       	
		    <div class="row">
	     		<div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       			<label for="code">Pay To</label>	
	       		</div>
	       		
	       		<div class="col s4 input-field">
					<input type ="text" name="crNoName" id="crNoId" ng-model="crNo" ng-click="openCrNoDB()" disabled="disabled" readonly ng-required="true">
					<label>Select Consumer No.</label>
				</div>
	   		</div>
	   		
	   		<div class="row">
				 <div ng-show="emAndSemList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Electricity Voucher Details</caption>
						<tr lass="rowclr">
							<th class="colclr">CRN NO</th>
							<th class="colclr">Branch Code</th>
							<th class="colclr">Pay Amount</th>
							<th class="colclr">Bill FrDt</th>
							<th class="colclr">Bill ToDt</th>
						</tr>
						<tr class="tbl" ng-repeat="emAndSem in emAndSemList">
							<td class="rowcel">{{emAndSem.elctMstr.emCrnNo}}</td>
							<td class="rowcel">{{emAndSem.elctMstr.branch.branchFaCode}}</td>
							<td class="rowcel">{{emAndSem.subElectMstr.semPayAmt}}</td>
							<td class="rowcel">{{emAndSem.subElectMstr.semBillFrDt}}</td>
							<td class="rowcel">{{emAndSem.subElectMstr.semBillToDt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeSemV($index,emAndSem)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="2">Total Amount</td>
							<td class="rowcel">{{semPayAmtSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>	
							<td class="rowcel"></td>						
						</tr>
					</table>
				</div>
			</div>	
	
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<!-- <div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div> -->
	   		 </div>
		  </form>
    </div>
   
   <div id ="crNoDB" ng-hide="crNoDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Consumer Number</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="em in emList | filter:filterTextbox">
		 	  <td><input type="radio"  name="em"  value="{{ em }}" ng-model="emCrNo" ng-click="saveCRNo(em)"></td>
              <td>{{ em.emCrnNo }}</td>              
          </tr>
      </table> 
	</div>  
	
	
	<div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="electVoucherDB" ng-hide="electVoucherDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="emVoucherForm" ng-submit="submitEMVoucher(emVoucherForm)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="payAmt" id="payAmtId" ng-model="em_sem.subElectMstr.semPayAmt" ng-required="true">
					<label>Pay Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="date" name="billFrDt" id="billFrDtId" ng-model="em_sem.subElectMstr.semBillFrDt">
					<label>Bill From Date</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="date" name="billToDt" id="billToDtId" ng-model="em_sem.subElectMstr.semBillToDt">
					<label>Bill To Date</label>
				</div>
	     </div>
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
   
   
   <div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
   
   
</div>
