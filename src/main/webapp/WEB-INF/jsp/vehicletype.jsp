<div ng-show="operatorLogin || superAdminLogin">
<title>Vehicle Type</title>
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="VehicleTypeForm" ng-submit="Submit(VehicleTypeForm,vehicletype,grnteeWt)">
			<div class="row">
      			<div class="input-field col s4">
        			<input class="validate" id="VehicleType" type ="text" name ="vtVehicleType" ng-model="vehicletype.vtVehicleType" ng-minlength="1" ng-maxlength="40" ng-required="true">
        			<label for="VehicleType">Vehicle Type</label>	
      			</div>
      			<div class="input-field col s4">
        			<input class="validate" id="VehicleTypeCode" type ="text" name ="vtCode" ng-model="vehicletype.vtCode" ng-minlength="1" ng-maxlength="1" ng-required="true">
        			<label for="VehicleTypeCode">Vehicle Type Code</label>	
      			</div>
      			<div class="input-field col s4">
        			<input class="validate" id="ServiceType" type ="text" name ="vtServiceType" ng-model="vehicletype.vtServiceType" ng-minlength="1" ng-maxlength="40" ng-required="true">
        			<label for="ServiceType">Service Type</label>	
      			</div>
      		</div>
      		
      		<div class="row">
      		
      			<div class="input-field col s4">
        			<input class="validate" id="LoadLimit" type ="number" name ="vtLoadLimit" ng-model="vehicletype.vtLoadLimit" step="0.01" min="0.00">
        			<label for="LoadLimit">Load Limit</label>	
      			</div>
      		
      			<div class="input-field col s4">
        			<input class="validate" id="weight" type ="number" name ="vtGuaranteeWt" ng-model="vehicletype.vtGuaranteeWt" step="0.01" min="0.00">
        			<label for="weight">Guarantee Weight</label>	
      			</div>
      			
      			<div class="input-field col s4">
	      			<select name="grnteeWt" id="grnteeWt" ng-model="grnteeWt">
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select>
					<label>Ton/KG</label>
				</div>
      		</div>
      	
      		<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
			</div>
			</form>
			<div class="col s3"> &nbsp; </div>
</div>

<!-- <form name="VehicleTypeForm" ng-submit="Submit(VehicleTypeForm,vehicletype)">
<table>
	<tr>
		<td>Vehicle Type:</td> 
		<td><input type ="text" name ="vtVehicleType" ng-model="vehicletype.vtVehicleType" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
	</tr>
	
	<tr>
		<td>Service Type:</td> 
		<td><input type ="text" name ="vtServiceType" ng-model="vehicletype.vtServiceType" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
	</tr>
	
	<tr>
		<td>Vehicle Type Code:</td> 
		<td><input type ="text" name ="vtCode" ng-model="vehicletype.vtCode" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
	</tr>
	
	<tr>
		<td>Load Limit :</td> 
		<td><input type ="number" name ="vtLoadLimit" ng-model="vehicletype.vtLoadLimit" ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
	</tr>
	
	<tr>
		<td>Guarantee Weight:</td> 
		<td><input type ="number" name ="vtGuaranteeWt" ng-model="vehicletype.vtGuaranteeWt" ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
	</tr>
	
	
	<tr>
		<td><input type="submit" value="Submit"></td>
	</tr>
	
</table>
</form> -->

<div id="viewVTDB" ng-hide="viewVTDetailsFlag">

<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Vehicle Type <span class="teal-text text-lighten-2">{{vehicletype.vtVehicleType}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Vehicle Type:</td>
	                <td>{{vehicletype.vtVehicleType}}</td>
	            </tr>
	            
	            <tr>
	                <td>Vehicle Type Code:</td>
	                <td>{{vehicletype.vtCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>Service Type:</td>
	                <td>{{vehicletype.vtServiceType}}</td>
	            </tr>
	            <tr>
	                <td>Load Limit:</td>
	                <td>{{vehicletype.vtLoadLimit}}</td>
	            </tr>
	            <tr>
	                <td>Guarantee Weight:</td>
	                <td>{{vehicletype.vtGuaranteeWt}} {{grnteeWt}}</td>
	            </tr>
	      </table>
	      
<input type="button" value="Save" ng-click="saveVehicleType(vehicletype,grnteeWt)">
<input type="button" value="Cancel" ng-click="closeVTDetailsDB()">
	      
	  </div>
 </div>         
</div>
</div>