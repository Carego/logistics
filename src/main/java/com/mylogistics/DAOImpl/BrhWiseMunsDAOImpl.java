package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BrhWiseMunsDAO;
import com.mylogistics.constants.BrhWiseMunsCNTS;
import com.mylogistics.model.User;
import com.mylogistics.model.lhpv.BrhWiseMuns;

public class BrhWiseMunsDAOImpl implements BrhWiseMunsDAO{

	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	public BrhWiseMunsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int saveBrhMuns(BrhWiseMuns brhWiseMuns){
		System.out.println("enter into saveBrhMuns funciton");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<BrhWiseMuns> brhMunsList = new ArrayList<>();
			Criteria cr=session.createCriteria(BrhWiseMuns.class);
			cr.add(Restrictions.eq(BrhWiseMunsCNTS.BWM_BRH_ID,brhWiseMuns.getBwmBrhId()));
			brhMunsList = cr.list();
			
			if(!brhMunsList.isEmpty()){
				BrhWiseMuns actBrM = brhMunsList.get(0) ;
				actBrM.setBwmActive(brhWiseMuns.isBwmActive());
				actBrM.setBwmBrhId(brhWiseMuns.getBwmBrhId());
				actBrM.setBwmMuns(brhWiseMuns.getBwmMuns());
				actBrM.setBwmCsDis(brhWiseMuns.getBwmCsDis());
	
				session.merge(actBrM);
				
				res = actBrM.getBwmBrhId();
			}else{
				brhWiseMuns.setbCode(currentUser.getUserBranchCode());
				brhWiseMuns.setUserCode(currentUser.getUserCode());
				res = (Integer) session.save(brhWiseMuns);
			}
			
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public BrhWiseMuns getBrhMuns(int brId){
		System.out.println("enter into getBrhMuns function = "+brId);
		BrhWiseMuns brhWiseMuns = null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<BrhWiseMuns> brhMunsList = new ArrayList<>();
			Criteria cr=session.createCriteria(BrhWiseMuns.class);
			cr.add(Restrictions.eq(BrhWiseMunsCNTS.BWM_BRH_ID,brId));
			cr.add(Restrictions.eq(BrhWiseMunsCNTS.BWM_ACTIVE,true));
			brhMunsList = cr.list();
			
			if(!brhMunsList.isEmpty()){
				brhWiseMuns = brhMunsList.get(0);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brhWiseMuns;
	}
}
