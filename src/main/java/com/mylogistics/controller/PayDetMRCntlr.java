package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.model.Bill;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class PayDetMRCntlr {

	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	
	@Autowired
	private BillDAO billDAO;
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@RequestMapping(value = "/getPDMrDetail", method = RequestMethod.POST)
	public @ResponseBody Object getMrDetail() {
		System.out.println("enter into getMrDetail function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				map.put("list",custList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getPendingMR", method = RequestMethod.POST)
	public @ResponseBody Object getPendingMR(@RequestBody String custId) {
		System.out.println("enter into getPendingMR function = "+custId);
		Map<String,Object> map = new HashMap<>();
		int cId = Integer.parseInt(custId);
		if(cId > 0){
			List<MoneyReceipt> mrList = new ArrayList<>();
			List<Bill> blList = new ArrayList<>();
			mrList = moneyReceiptDAO.getPendingMR(cId);
			blList = billDAO.getPendingBillFrMr(cId);
			System.out.println("size of blList = "+blList.size());
			if(!mrList.isEmpty()){
				map.put("list",mrList);
				map.put("blList",blList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getCnmtFrPDMR", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtFrPDMR(@RequestBody String blNo) {
		System.out.println("enter into getCnmtFrPDMR function = ");
		Map<String,Object> map = new HashMap<>();
		List<String> cnmtList = new ArrayList<String>();
		cnmtList = cnmtDAO.getCnmtNoByBill(blNo);
		if(!cnmtList.isEmpty()){
			map.put("list",cnmtList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitPDMR", method = RequestMethod.POST)
	public @ResponseBody Object submitPDMR(@RequestBody List<MoneyReceipt> mrList) {
		System.out.println("enter into getPendingMR function = ");
		Map<String,Object> map = new HashMap<>();
		if(!mrList.isEmpty()){
			int res = 0;
			MoneyReceipt moneyReceipt = mrList.get(0);		
			
			if(moneyReceipt.getMrDate() != null){
				List<Map<String,Object>> mapList = 	moneyReceipt.getMrFrPDList();
				if(!mapList.isEmpty()){
					for(int i=0;i<mapList.size();i++){
						if(Integer.parseInt(String.valueOf(mapList.get(i).get("brhId"))) > 0){
							res = 1;
						}else{
							res = 0;
							break;
						}
					}
				}
				if(res > 0){
					String mrNo = moneyReceiptDAO.savePDMR(mrList);					
					if(mrNo != null){
						map.put("mrNo",mrNo);
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getPDMrDetailBack", method = RequestMethod.POST)
	public @ResponseBody Object getMrDetailBack() {
		System.out.println("enter into getMrDetail function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(currentUser.getUserBranchCode());
		if(cashStmtStatus != null){
			map.put("cssDt",cashStmtStatus.getCssDt());
			List<Map<String,Object>> custList = new ArrayList<>();
			custList = customerDAO.getCustNCI();
			if(!custList.isEmpty()){
				map.put("list",custList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
}

