package com.mylogistics.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "emailrepotlist")
public class EMailReportList {

	@Id@GeneratedValue
	@Column(name = "emrlId")	
	private Integer emrlId;
	
	@Column(name = "emrlEmail")
	@Email@NotEmpty@NotNull
	private String emrlEmail;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "emrEMailReportList")
	private List<EMailReport> eMailReportList;

	public Integer getEmrlId() {
		return emrlId;
	}

	public void setEmrlId(Integer emrlId) {
		this.emrlId = emrlId;
	}

	public String getEmrlEmail() {
		return emrlEmail;
	}

	public void setEmrlEmail(String emrlEmail) {
		this.emrlEmail = emrlEmail;
	}

	public List<EMailReport> geteMailReportList() {
		return eMailReportList;
	}

	public void seteMailReportList(List<EMailReport> eMailReportList) {
		this.eMailReportList = eMailReportList;
	}
	
	
	
}