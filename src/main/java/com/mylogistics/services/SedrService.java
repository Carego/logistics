package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mylogistics.model.ArrivalReport;

public class SedrService {

	private String contType;
	
	private ArrivalReport arrivalReport;
	
	private List<Map<String,Object>> cnWSList = new ArrayList<>();

	
	
	public String getContType() {
		return contType;
	}

	public void setContType(String contType) {
		this.contType = contType;
	}

	public ArrivalReport getArrivalReport() {
		return arrivalReport;
	}

	public void setArrivalReport(ArrivalReport arrivalReport) {
		this.arrivalReport = arrivalReport;
	}

	public List<Map<String, Object>> getCnWSList() {
		return cnWSList;
	}

	public void setCnWSList(List<Map<String, Object>> cnWSList) {
		this.cnWSList = cnWSList;
	}

	
	
}
