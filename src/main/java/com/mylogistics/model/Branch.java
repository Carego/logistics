package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeBook;
import com.mylogistics.model.bank.ChequeLeaves;

@Entity
@Table(name = "branch")
public class Branch {

	@Id
	@Column(name="branchId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int branchId;

	@Column(name="branchCode")
	private String branchCode;

	@Column(name="branchName")
	private String branchName;

	@Column(name="branchAdd")
	private String branchAdd;

	@Column(name="branchCity")
	private String branchCity;

	@Column(name="branchState")
	private String branchState;

	@Column(name="branchPin")
	private String branchPin;

	@Column(name="branchOpenDt")
	private Date branchOpenDt;

	@Column(name="branchCloseDt")
	private Date branchCloseDt;

	@Column(name="branchStationCode")
	private String branchStationCode;

	@Column(name="branchDirector")
	private String branchDirector;

	@Column(name="branchMarketingHD")
	private String branchMarketingHD;

	@Column(name="branchOutStandingHD")
	private String branchOutStandingHD;

	@Column(name="branchMarketing")
	private String branchMarketing;

	@Column(name="branchMngr")
	private String branchMngr;

	@Column(name="branchCashier")
	private String branchCashier;

	@Column(name="branchTraffic")
	private String branchTraffic;

	@Column(name="branchRegionalMngr")
	private String branchRegionalMngr;

	@Column(name="branchAreaMngr")
	private String branchAreaMngr;

	@Column(name="branchPhone")
	private String branchPhone;

	@Column(name="branchFax")
	private String branchFax;

	@Column(name="branchEmailId")
	private String branchEmailId;

	@Column(name="branchWebsite")
	private String branchWebsite;

	@Column(name="isOpen")
	private String isOpen;

	@Column(name="userCode")
	private String userCode;

	@Column(name="isNCR")
	private String isNCR;

	@Column(name="bCode")
	private String bCode;

	@Column(name="branchCodeTemp")
	private String branchCodeTemp;

	@Column(name="isView")
	private boolean isView;

	@Column(name="branchFaCode")
	private String branchFaCode;
	
	@Column(name="branchStateGST")
	private String branchStateGST;

	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "brhEmp")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_employee",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="empId"))
	private List<Employee> employeeList = new ArrayList<Employee>();
	
	//Many To Many
	@JsonIgnore
	@JsonManagedReference(value = "custBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_customer",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="custId"))
	private Set<Customer> customers = new HashSet<Customer>();
	
	
	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "eleBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_electricitymstr",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="emId"))
	private List<ElectricityMstr> electricityMstrList = new ArrayList<ElectricityMstr>();

	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "telBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_telephonemstr",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="tmId"))
	private List<TelephoneMstr> telephoneMstrList = new ArrayList<TelephoneMstr>();
	
	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "rentBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_rentmstr",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="rentMId"))
	private List<RentMstr> rentMstrList = new ArrayList<>();
	
	
	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "vehBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_vehiclemstr",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="vehId"))
	private List<VehicleMstr> vehicleMstrList = new ArrayList<VehicleMstr>();


	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "phnBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_phonemstr",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="phId"))
	private List<PhoneMstr> phoneMstrList = new ArrayList<PhoneMstr>();


	//One To Many
	//@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonIgnore
	@JsonManagedReference(value = "bnkBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_bankmstr",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="bnkId"))
	private List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
	
	//One To Many
	@JsonIgnore
	//@JsonManagedReference(value = "eleBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_chequebook",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="chqBkId"))
	private List<ChequeBook> chequeBookList = new ArrayList<>();
	
	//One To Many
	@JsonIgnore
	//@JsonManagedReference(value = "eleBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_chequeleaves",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="chqLId"))
	private List<ChequeLeaves> chequeLeaveList = new ArrayList<>();
	
	/*@JsonIgnore
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="branch_atmcard",
	joinColumns=@JoinColumn(name="branchId"),
	inverseJoinColumns=@JoinColumn(name="atmCardId"))
	private List<AtmCardMstr> atmCardMstrList = new ArrayList<>();*/

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	//getters and setters

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
	private FAParticular fAParticular;

	
	public List<ChequeLeaves> getChequeLeaveList() {
		return chequeLeaveList;
	}


	public void setChequeLeaveList(List<ChequeLeaves> chequeLeaveList) {
		this.chequeLeaveList = chequeLeaveList;
	}


	public List<Employee> getEmployeeList() {
		return employeeList;
	}


	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	
	public List<ElectricityMstr> getElectricityMstrList() {
		return electricityMstrList;
	}


	public Set<Customer> getCustomers() {
		return customers;
	}


	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}


	public List<ChequeBook> getChequeBookList() {
		return chequeBookList;
	}


	public void setChequeBookList(List<ChequeBook> chequeBookList) {
		this.chequeBookList = chequeBookList;
	}


	public void setElectricityMstrList(List<ElectricityMstr> electricityMstrList) {
		this.electricityMstrList = electricityMstrList;
	}


	public List<TelephoneMstr> getTelephoneMstrList() {
		return telephoneMstrList;
	}


	public void setTelephoneMstrList(List<TelephoneMstr> telephoneMstrList) {
		this.telephoneMstrList = telephoneMstrList;
	}


	public String getBranchFaCode() {
		return branchFaCode;
	}


	public void setBranchFaCode(String branchFaCode) {
		this.branchFaCode = branchFaCode;
	}


	public FAParticular getfAParticular() {
		return fAParticular;
	}


	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}


	public int getBranchId() {
		return branchId;
	}


	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getBranchAdd() {
		return branchAdd;
	}


	public void setBranchAdd(String branchAdd) {
		this.branchAdd = branchAdd;
	}


	public String getBranchCity() {
		return branchCity;
	}


	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}


	public String getBranchState() {
		return branchState;
	}


	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}


	public String getBranchPin() {
		return branchPin;
	}


	public void setBranchPin(String branchPin) {
		this.branchPin = branchPin;
	}


	public Date getBranchOpenDt() {
		return branchOpenDt;
	}


	public void setBranchOpenDt(Date branchOpenDt) {
		this.branchOpenDt = branchOpenDt;
	}


	public Date getBranchCloseDt() {
		return branchCloseDt;
	}


	public void setBranchCloseDt(Date branchCloseDt) {
		this.branchCloseDt = branchCloseDt;
	}


	public String getBranchStationCode() {
		return branchStationCode;
	}


	public void setBranchStationCode(String branchStationCode) {
		this.branchStationCode = branchStationCode;
	}


	public String getBranchDirector() {
		return branchDirector;
	}


	public void setBranchDirector(String branchDirector) {
		this.branchDirector = branchDirector;
	}


	public String getBranchMarketingHD() {
		return branchMarketingHD;
	}


	public void setBranchMarketingHD(String branchMarketingHD) {
		this.branchMarketingHD = branchMarketingHD;
	}


	public String getBranchOutStandingHD() {
		return branchOutStandingHD;
	}


	public void setBranchOutStandingHD(String branchOutStandingHD) {
		this.branchOutStandingHD = branchOutStandingHD;
	}


	public String getBranchMarketing() {
		return branchMarketing;
	}


	public void setBranchMarketing(String branchMarketing) {
		this.branchMarketing = branchMarketing;
	}


	public String getBranchMngr() {
		return branchMngr;
	}


	public void setBranchMngr(String branchMngr) {
		this.branchMngr = branchMngr;
	}


	public String getBranchCashier() {
		return branchCashier;
	}


	public void setBranchCashier(String branchCashier) {
		this.branchCashier = branchCashier;
	}


	public String getBranchTraffic() {
		return branchTraffic;
	}


	public void setBranchTraffic(String branchTraffic) {
		this.branchTraffic = branchTraffic;
	}


	public String getBranchRegionalMngr() {
		return branchRegionalMngr;
	}


	public void setBranchRegionalMngr(String branchRegionalMngr) {
		this.branchRegionalMngr = branchRegionalMngr;
	}


	public String getBranchAreaMngr() {
		return branchAreaMngr;
	}


	public void setBranchAreaMngr(String branchAreaMngr) {
		this.branchAreaMngr = branchAreaMngr;
	}


	public String getBranchPhone() {
		return branchPhone;
	}


	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}


	public String getBranchFax() {
		return branchFax;
	}


	public void setBranchFax(String branchFax) {
		this.branchFax = branchFax;
	}


	public String getBranchEmailId() {
		return branchEmailId;
	}


	public void setBranchEmailId(String branchEmailId) {
		this.branchEmailId = branchEmailId;
	}


	public String getBranchWebsite() {
		return branchWebsite;
	}


	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}


	public String getIsOpen() {
		return isOpen;
	}


	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}


	public String getUserCode() {
		return userCode;
	}


	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}


	public String getIsNCR() {
		return isNCR;
	}


	public void setIsNCR(String isNCR) {
		this.isNCR = isNCR;
	}


	public String getbCode() {
		return bCode;
	}


	public void setbCode(String bCode) {
		this.bCode = bCode;
	}


	public String getBranchCodeTemp() {
		return branchCodeTemp;
	}


	public void setBranchCodeTemp(String branchCodeTemp) {
		this.branchCodeTemp = branchCodeTemp;
	}


	public boolean isView() {
		return isView;
	}


	public void setView(boolean isView) {
		this.isView = isView;
	}


	public List<VehicleMstr> getVehicleMstrList() {
		return vehicleMstrList;
	}


	public void setVehicleMstrList(List<VehicleMstr> vehicleMstrList) {
		this.vehicleMstrList = vehicleMstrList;
	}


	public List<PhoneMstr> getPhoneMstrList() {
		return phoneMstrList;
	}


	public void setPhoneMstrList(List<PhoneMstr> phoneMstrList) {
		this.phoneMstrList = phoneMstrList;
	}


	public Calendar getCreationTS() {
		return creationTS;
	}


	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public List<BankMstr> getBankMstrList() {
		return bankMstrList;
	}


	public void setBankMstrList(List<BankMstr> bankMstrList) {
		this.bankMstrList = bankMstrList;
	}


	public List<RentMstr> getRentMstrList() {
		return rentMstrList;
	}


	public void setRentMstrList(List<RentMstr> rentMstrList) {
		this.rentMstrList = rentMstrList;
	}


	public String getBranchStateGST() {
		return branchStateGST;
	}


	public void setBranchStateGST(String branchStateGST) {
		this.branchStateGST = branchStateGST;
	}


	/*public List<AtmCardMstr> getAtmCardMstrList() {
		return atmCardMstrList;
	}


	public void setAtmCardMstrList(List<AtmCardMstr> atmCardMstrList) {
		this.atmCardMstrList = atmCardMstrList;
	}*/
	
	

}
