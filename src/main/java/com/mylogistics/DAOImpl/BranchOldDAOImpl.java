package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchOldDAO;
import com.mylogistics.constants.BranchOldCNTS;
import com.mylogistics.model.BranchOld;

public class BranchOldDAOImpl implements BranchOldDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	public BranchOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public int saveBranchOld(BranchOld branchOld) {
		int temp;
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(branchOld);
			transaction.commit();
			temp = 1;
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<BranchOld> getModifiedBranch(String branchCode) {
		List<BranchOld> branchOldList = new ArrayList<BranchOld>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(BranchOld.class);
			cr.add(Restrictions.eq(BranchOldCNTS.BRANCH_CODE, branchCode));
			branchOldList = cr.list();
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return branchOldList;
	}
}
