<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
			ng-class = "{'form-error': newVoucherForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
		
			<div class="row">
		     		<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Code</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.branchFaCode.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch code is required.</span>
							</div>		
		       		</div>

		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Name</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.branchName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch name is required.</span>
							</div>				
		       		</div>
		       		
		       			<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly ng-required = "true" >
		       			<label for="code" style="color:black;">Date</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.dateTemp.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Date is required.</span>
							</div>	
		       		</div>
		       		<div class="col s1 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Sheet No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.sheetNo.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Sheet is required.</span>
							</div>		
		       		</div>
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Voucher Type</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.voucherType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Voucher type is required.</span>
							</div>		
		       		</div>
		    </div>
		    
		    <div class="row">
		       		<div class="col s2 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label style="color:black;">Payment By</label>
							<div class = "text-left errorMargin" ng-messages = "newVoucherForm.payBy.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Payment by is required.</span>
							</div>		
					</div>
					
					<div class="col s2 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled" ng-required = "isChequePayment">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label style="color:black;">Cheque Type </label>
						<div class = "text-left errorMargin" ng-messages = "newVoucherForm.chequeType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Cheque type is required.</span>
							</div>		
					</div>
					
					<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled"
		       					ng-required = "isChequePayment || isOnlinePayment">
		       			<label for="code" style="color:black;">Bank Code</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.bankCode.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Bank code is required.</span>
							</div>			
		       		</div>
		       		
		       		<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled"
		       					ng-required = "isChequePayment">
		       			<label for="code" style="color:black;">Cheque No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.chequeNo.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Cheque no. is required.</span>
							</div>		
		       		</div>
		       		
		       		<div class="col s2 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled" ng-required = "isChequePayment">
	       			<label for="code" style="color:black;">Pay To</label>
	       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.payToName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Pay to is required.</span>
							</div>	
	       		</div>
	       		
	       		<div class="col s2 input-field">
					<input type ="text" name="phNoName" id="phNoId" ng-model="phNo" ng-click="openPhNoDB()" disabled="disabled" readonly>
					<label style="color:black;">Select Phone No.</label>
					<div class = "text-left errorMargin" ng-if = "tmAndStmList.length == 0">
								<span ng-message = "required">*  Phone is required.</span>
							</div>		
				</div>
		    </div>
	  
	        <div class="row"> 		
				 <div ng-show="tmAndStmList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Telephone Voucher Details</caption>
						<tr class="rowclr">
							<th class="colclr">PhNo</th>
							<th class="colclr">Pay Amt</th>
							<th class="colclr">Bill FrDt</th>
							<th class="colclr">Bill ToDt</th>
							<th class="colclr">Staff Code</th>
							<th class="colclr">Branch Code</th>
							<th class="colclr">Sms Amt</th>
							<th class="colclr">Caller Tune Amt</th>
							<th class="colclr">Download Amt</th>
							<th class="colclr">Game Amt</th>
							<th class="colclr">Pesonal Call</th>
							<th class="colclr">Roaming Amt</th>
							<th class="colclr">Isd Amt</th>
							<th class="colclr">Total Ded</th>
						</tr>
						<tr class="tbl" ng-repeat="tmAndStm in tmAndStmList">
							<td class="rowcel">{{tmAndStm.tMstr.tmPhNo}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmPayAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmBillFrDt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmBillToDt}}</td>
							<td class="rowcel">{{tmAndStm.tMstr.employee.empFaCode}}</td>
							<td class="rowcel">{{tmAndStm.tMstr.branch.branchFaCode}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmSmsDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmCTDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmDDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmGDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmPCDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmRDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmIsdDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmTotDedAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeStmV($index,tmAndStm)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="13">Total Amount</td>
							<td class="rowcel">{{stmTotDedAmtSum}}</td>
							<td class="rowcel"></td>										
						</tr>
					</table>
				</div>
			</div>	
	
	   		 <div class="row" style="margin-top: 5px;">
				<div class="input-field col s12">
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"
 						ng-minlength = "2" ng-maxlength = "200"></textarea>
 					<label style="color:black;">Description</label>
 					<div class = "text-left errorMargin" ng-messages = "newVoucherForm.desc.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Description is required.</span>
								<span ng-message = "minlength">*  Description should be 2 characters long.</span>
								<span ng-message = "maxlength">*  Description should not be 200 character long.</span>
							</div>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
   
   <div id ="phoneNoDB" ng-hide="phoneNoDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Phone Number</th>
 	  	  </tr>
		  <tr ng-repeat="telM in telMList | filter:filterTextbox">
		 	  <td><input type="radio"  name="telM"  value="{{ telM }}" ng-model="telPNo" ng-click="savePhoneNo(telM)"></td>
              <td>{{ telM.tmPhNo }}</td>
          </tr>
      </table> 
	</div>  
	
	
	<div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	  </tr>
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	  </tr>
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="telVoucherDB" ng-hide="telVoucherDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="tmVoucherForm" ng-submit="submitTMVoucher(tmVoucherForm)"
		ng-class = "{'form-error': tmVoucherForm.$invalid && tmFormSubmit}" novalidate = "novalidate">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="payAmt" id="payAmtId" ng-model="tm_stm.stMstr.stmPayAmt" step="0.01" min="0.00" ng-required="true">
					<label style="color:black;">Tot.Pay Amt.</label>
					<div class = "text-left errorMargin" ng-messages = "tmVoucherForm.payAmt.$error" ng-if = "tmFormSubmit">
								<span ng-message = "required">*  Total pay amount is required.</span>
							</div>
				</div>
				<div class="col s4 input-field">
					<input type ="date" name="billFrDt" id="billFrDtId" ng-model="tm_stm.stMstr.stmBillFrDt" step="0.01" min="0.00">
					<label style="color:black;">Bill From Date</label>
				</div>
				<div class="col s4 input-field">
					<input type ="date" name="billToDt" id="billToDtId" ng-model="tm_stm.stMstr.stmBillToDt" step="0.01" min="0.00">
					<label style="color:black;">Bill To Date</label>
				</div>
	     </div>
	     
	      <div class="row">
			    <div class="col s3 input-field">
					<input type ="number" name="stmSmsDedAmt" id="stmSmsDedAmtId" ng-model="tm_stm.stMstr.stmSmsDedAmt" step="0.01" min="0.00">
					<label style="color:black;">Sms Amount</label>
				</div>
				<div class="col s3 input-field">
					<input type ="number" name="stmCTDedAmt" id="stmCTDedAmtId" ng-model="tm_stm.stMstr.stmCTDedAmt" step="0.01" min="0.00">
					<label style="color:black;">Caller Tune Amount</label>
				</div>
				<div class="col s3 input-field">
					<input type ="number" name="stmDDedAmt" id="stmDDedAmtId" ng-model="tm_stm.stMstr.stmDDedAmt" step="0.01" min="0.00">
					<label style="color:black;">Downloading Amount</label>
				</div>
				 <div class="col s3 input-field">
					<input type ="number" name="stmGDedAmt" id="stmGDedAmtId" ng-model="tm_stm.stMstr.stmGDedAmt" step="0.01" min="0.00">
					<label style="color:black;">Game Amount</label>
				</div>
	     </div>
	     
	     <div class="row">
	     	<div class="col s3 input-field">
					<input type ="number" name="stmPCDedAmt" id="stmPCDedAmtId" ng-model="tm_stm.stMstr.stmPCDedAmt" step="0.01" min="0.00">
					<label style="color:black;">Personal Call Amount</label>
				</div>
				<div class="col s3 input-field">
					<input type ="number" name="stmRDedAmt" id="stmRDedAmtId" ng-model="tm_stm.stMstr.stmRDedAmt" step="0.01" min="0.00">
					<label style="color:black;">Roaming Amount</label>
				</div>
			    <div class="col s3 input-field">
					<input type ="number" name="stmIsdDedAmt" id="stmIsdDedAmtId" ng-model="tm_stm.stMstr.stmIsdDedAmt" step="0.01" min="0.00">
					<label style="color:black;">ISD Amount</label>
				</div>
				<div class="col s3 input-field">
					<input type ="number" name="stmTotDedAmt" id="stmTotDedAmtId" ng-model="tm_stm.stMstr.stmTotDedAmt" step="0.01" min="0.00" ng-click="calTotDedAmt()" readonly ng-required="true">
					<label style="color:black;">Total Deduction Amount</label>
					<div class = "text-left errorMargin" ng-messages = "tmVoucherForm.stmTotDedAmt.$error" ng-if = "tmFormSubmit">
								<span ng-message = "required">*  Total deduction amount is required.</span>
							</div>
				</div>
	     </div>
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	</form>
	</div>
   
   <div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
   
</div>