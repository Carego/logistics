package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.StateDAO;
import com.mylogistics.model.State;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;


@Controller
public class StateCntlr {

	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@RequestMapping(value = "/submitState", method = RequestMethod.POST)
    public @ResponseBody Object submitState(@RequestBody State state){
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		state.setUserCode(currentUser.getUserCode());
		state.setbCode(currentUser.getUserBranchCode());
		
		
		String stateCode = null;
		int lastStateId;
		long totalRows = stateDAO.totalCount();
	                                              
		Map<String,String> map = new HashMap<String,String>();
	     
		if(!(totalRows == -1)){
	    	 
			if(totalRows==0){
				stateCode="1";
			}else{
				List<State> stateList =stateDAO.getLastStateRow();			    		
				lastStateId = stateList.get(0).getStateId();
				int stateId = lastStateId+1;
				stateCode = String.valueOf(stateId);
			}
		}      
			
      //  String stateCode = CodePatternService.stateCodeGen(state);
        state.setStateCode(stateCode);
        int temp=stateDAO.saveStateToDB(state);
        if (temp>=0) {
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map; 	
	}
	
	@RequestMapping(value="/getStateDetails",method = RequestMethod.POST)
	public @ResponseBody Object getStateDetails(){
		
		List<State> stateList = stateDAO.getStateData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(stateList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
  			map.put("list",stateList);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	 @RequestMapping(value = "/stateDetails", method = RequestMethod.POST)
	  public @ResponseBody Object stateDetails(@RequestBody String stateCode){
		 
	    State state = new State();
		List<State> list = stateDAO.retrieveState(stateCode);
		  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			state = list.get(0);
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("state",state);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
   }
	 
	  @RequestMapping(value = "/updateState", method = RequestMethod.POST)
	  public @ResponseBody Object updateState(@RequestBody State state){
		 	System.out.println("Entered into updateState of controller----");
		 	
		 	Map<String,String> map = new HashMap<String, String>();
		 	/*String stateCode = CodePatternService.stateCodeGen(state);
	        state.setStateCode(stateCode);*/
	
	        int temp=stateDAO.updateState(state);
	        if (temp>=0) {
	        	
	        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			} else {
				  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}	    	
	    	return map;
	    }
	 
	  @RequestMapping(value="/getStateList",method = RequestMethod.POST)
	  public @ResponseBody Object getStateCodeData(){
			
		  List<String> stateList = stateDAO.getStateName();
			
		  Map<String,Object> map = new HashMap<String,Object>();
			
		  if (!stateList.isEmpty()) {
			  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			  map.put("stateList",stateList);	
		  }else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		  }
		  return map;	
	  }

	  
	  @RequestMapping(value="/getStateGSTList",method = RequestMethod.POST)
	  public @ResponseBody Object getStateGSTCodeData(){
			
		  User user = (User) httpSession.getAttribute("currentUser");
		  System.out.println("Enter into getStateGSTCodeData()" + user.getUserName());
		  Session session = this.sessionFactory.openSession();
		  Map<String,Object> map = new HashMap<String,Object>();
		  
		  try{
			  List<State> stateList = stateDAO.getStateGSTName(session , user);
				
			  if (!stateList.isEmpty()) {
				  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				  map.put("stateList",stateList);	
			  }else {
				  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			  } 
		  }catch(Exception e){
			  System.out.println(e);
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		  }finally{
			  session.clear();
			  session.close();
		  }
		  
		  return map;	
	  }

	  
}
