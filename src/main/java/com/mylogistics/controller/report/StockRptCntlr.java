package com.mylogistics.controller.report;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.LhpvStatusCNTS;
import com.mylogistics.constants.MemoCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Customer;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.mail.SmtpMailSender;

@Controller
public class StockRptCntlr {

	@Autowired
	private ReportDAO reportDAO;
	
//	@Autowired
//	private SmtpMailSender smtpMailSender;
	
	private Map<String, Object> stockRpt = new HashMap<>();
	
	
	
	private String upToDt;

	@RequestMapping(value="/getStockRpt", method=RequestMethod.POST)
	public @ResponseBody Object getStockRpt(@RequestBody Map<String, Object> stockRptService){
		System.out.println("StockRptCntlr.getStockRpt()");
		Map<String, Object> map = new HashMap<>();
		
		System.out.println("branchId: "+stockRptService.get("branchId"));
		System.out.println("custId: "+stockRptService.get("custId"));
		System.out.println("upToDt: "+stockRptService.get("upToDt"));
		System.out.println("grouId:"+stockRptService.get("custGroupId"));
		
		upToDt = String.valueOf(stockRptService.get("upToDt"));
		
		stockRpt = reportDAO.getStockRptN(stockRptService);
		if ((int)stockRpt.get("temp") ==0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		
		}
		
		return map;
	}
	
	@RequestMapping(value="/getStockRptXLSX", method=RequestMethod.POST)
	public void getStockRptXLSX(HttpServletRequest request, HttpServletResponse response) throws Exception{
		System.out.println("StockRptCntlr.getStockRptXLSX()");
		
		List<Map<String, Object>> cnmtMapList = (List<Map<String, Object>>) stockRpt.get("cnmtMapList");
		List<Customer> custListTemp = (List<Customer>) stockRpt.get("custList");
		List<Branch> branchList = (List<Branch>) stockRpt.get("brList");
		List<Customer> custList = new ArrayList<>();
		List<Map<String,Object>> listStockTotal=new ArrayList<Map<String,Object>>();
		Map<Integer, String> branchMap = new HashMap<>();
		System.out.println(custListTemp.get(0).getCustName());
		if (!branchList.isEmpty()) {
			for (Branch branch : branchList) {
				branchMap.put(branch.getBranchId(), branch.getBranchName());
			}
		}
		
		//get only those customer whose bill has been prepared
		Set<Integer> custIdSet = new TreeSet<>();
		for (Map<String, Object> cnmtMap : cnmtMapList) {
		/*	if(!String.valueOf(cnmtMap.get(CnmtCNTS.PR_BL_CODE)).equals("null"))
			{
				cnmtMap.put(CnmtCNTS.CUST_CODE, "0");
			}*/
			custIdSet.add(Integer.parseInt(String.valueOf(cnmtMap.get(CnmtCNTS.CUST_CODE))));
		}
		
		for (Customer customer : custListTemp) {
			System.out.println("Customer Cust Id   "+customer.getCustId());
	
			for (Integer custId : custIdSet) {
				System.out.println("CustId Set "+custId);
				if(customer.getCustId()==1754 || customer.getCustId()==1755 || customer.getCustId()==1756)
				{
					custList.add(customer);
					break;	
				}
				else if (custId == customer.getCustId()) {
					custList.add(customer);
					break;
				}
			}
		} 
	System.out.println("Cust List   "+custList);
	
		String faCode="";
		
		if(!custList.isEmpty())
				faCode = custList.get(0).getCustFaCode();
		
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		
		//Create a blank sheet
		XSSFSheet spreadsheet = workbook.createSheet();
		
		//set column width accordingly
		spreadsheet.setColumnWidth(0, 10*256);
		spreadsheet.setColumnWidth(1, 15*256);
		spreadsheet.setColumnWidth(2, 8*256);
		spreadsheet.setColumnWidth(3, 7*256); //date
		spreadsheet.setColumnWidth(4, 10*256);
		spreadsheet.setColumnWidth(5, 10*256);
		spreadsheet.setColumnWidth(6, 7*256);
		spreadsheet.setColumnWidth(7, 7*256);
		spreadsheet.setColumnWidth(8, 5*256);
		spreadsheet.setColumnWidth(9, 10*256);
		spreadsheet.setColumnWidth(10, 5*256);
		spreadsheet.setColumnWidth(11, 12*256);
		spreadsheet.setColumnWidth(12, 12*256);
		spreadsheet.setColumnWidth(13, 12*256);
		spreadsheet.setColumnWidth(14, 8*256);
		spreadsheet.setColumnWidth(15, 7*256);
		spreadsheet.setColumnWidth(16, 12*256);
		spreadsheet.setColumnWidth(17, 10*256);		
		spreadsheet.setColumnWidth(18, 7*256);
		
		spreadsheet.setColumnWidth(19, 10*256);
		spreadsheet.setColumnWidth(20, 10*256);
		spreadsheet.setColumnWidth(21, 15*256);
		spreadsheet.setColumnWidth(22, 15*256);
		
		spreadsheet.setColumnWidth(23, 7*256);
		spreadsheet.setColumnWidth(24, 7*256);		
		spreadsheet.setColumnWidth(25, 15*256);
		spreadsheet.setColumnWidth(26, 15*256);
		spreadsheet.setColumnWidth(27, 15*256);
		spreadsheet.setColumnWidth(28, 15*256);
		spreadsheet.setColumnWidth(29, 15*256);
		spreadsheet.setColumnWidth(30, 15*256);
		
		if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
			spreadsheet.setColumnWidth(31, 15*256);
		}
		
		//Create row object
		XSSFRow row;
		
		//Heading font
		XSSFFont fontHeading = workbook.createFont();
		fontHeading.setFontHeightInPoints((short)18);
		fontHeading.setFontName(FontFactory.HELVETICA_BOLD);
		fontHeading.setBold(true);
		fontHeading.setUnderline(XSSFFont.U_SINGLE);
		
		//Heading Style
		XSSFCellStyle styleHeading = workbook.createCellStyle();
	    styleHeading.setFont(fontHeading);
	    styleHeading.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    
	    //Heading row
	    int rowId = 0;
	    row = spreadsheet.createRow(rowId++);
	    XSSFCell cellHeading = row.createCell(0);
    	cellHeading.setCellStyle(styleHeading);
    	cellHeading.setCellValue("STOCK REPORT"+
    			"    (Up to Date: "+CodePatternService.getFormatedDateString(upToDt)+")");
    	
    	if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
    		spreadsheet.addMergedRegion(new CellRangeAddress(0,0,0,31));
		}else
			spreadsheet.addMergedRegion(new CellRangeAddress(0,0,0,30));
		
	    //blank row
	    rowId++;
		
	    //Header font
  		XSSFFont fontHeader = workbook.createFont();
  		fontHeader.setFontHeightInPoints((short)11);
  		fontHeader.setColor(IndexedColors.WHITE.getIndex());
  		fontHeader.setFontName(FontFactory.COURIER_BOLD);
  		fontHeader.setBold(true);
		
  		//Header style
  		XSSFCellStyle styleHeader = workbook.createCellStyle();
  		styleHeader.setFont(fontHeader);
  		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
  		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		//subHeader font
  		XSSFFont fontSubHeader = workbook.createFont();
  		fontSubHeader.setFontHeightInPoints((short)10);
  		fontSubHeader.setColor(IndexedColors.BLUE.getIndex());
  		fontSubHeader.setFontName(FontFactory.TIMES);
  		fontSubHeader.setBold(true);
		
  		//subHeader style
  		XSSFCellStyle styleSubHeader = workbook.createCellStyle();
  		styleSubHeader.setFont(fontSubHeader);
  		styleSubHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleSubHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleSubHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleSubHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		//subHeaderNumber style
  		XSSFCellStyle styleSubHeaderNumber = workbook.createCellStyle();
  		styleSubHeaderNumber.setFont(fontSubHeader);
  		styleSubHeaderNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleSubHeaderNumber.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleSubHeaderNumber.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleSubHeaderNumber.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		
  		//table font
  		XSSFFont font = workbook.createFont();
  		font.setFontHeightInPoints((short)10);
  		font.setColor(IndexedColors.BLACK.getIndex());
  		font.setFontName(FontFactory.TIMES);
  		
  		//table style
  		XSSFCellStyle style = workbook.createCellStyle();
  		style.setFont(font);
  		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
  		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
  		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
  		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
  		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
  		style.setWrapText(true);
  		
  		//table style number
  		XSSFCellStyle styleNumber = workbook.createCellStyle();
  		styleNumber.setFont(font);
  		styleNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleNumber.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
  		styleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		//styleNumber.setDataFormat((short)8);
  		
  		System.out.println("Cust List Size   "+custList.size());
		for (Customer customer : custList) {
			
			row = spreadsheet.createRow(rowId++);
			Map<String,Object> map=new HashMap<String,Object>();
			   map.put("custFaCode",customer.getCustFaCode());
			   map.put("custName",customer.getCustName());

			int col = 30;
			if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
	    		col = 31;
			}
			
			   
			for (int i = 0; i <= col; i++) {
				XSSFCell cust = row.createCell(i);
				cust.setCellStyle(styleHeader);
		        if (i == 0) {
		        	cust.setCellValue("A/C CODE: "+customer.getCustFaCode()+"     NAME: "+customer.getCustName()+
		        			"   ("+branchMap.get(Integer.parseInt(customer.getBranchCode())).toUpperCase()+")");
		        } 
		    }
				if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863"))
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,31));
			else
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,30));
					
			//cnmt wise detail
			row = spreadsheet.createRow(rowId++);
  			
			XSSFCell cellBcd = row.createCell(0);
			cellBcd.setCellStyle(styleSubHeader);
			cellBcd.setCellValue("Bcd");
			
			XSSFCell cellInvc = row.createCell(1);
			cellInvc.setCellStyle(styleSubHeader);
			cellInvc.setCellValue("Invoice No.");
			
	       	XSSFCell cellCnmt = row.createCell(2);
	       	cellCnmt.setCellStyle(styleSubHeader);
	       	cellCnmt.setCellValue("CNMT");
	       	
	       	XSSFCell cellCnmtDt = row.createCell(3);
	       	cellCnmtDt.setCellStyle(styleSubHeader);
	       	cellCnmtDt.setCellValue("CNDT");
	       	
	       	XSSFCell cellFrmStn = row.createCell(4);
	       	cellFrmStn.setCellStyle(styleSubHeader);
	       	cellFrmStn.setCellValue("FRM");
	       	
	       	XSSFCell cellToStn = row.createCell(5);
	       	cellToStn.setCellStyle(styleSubHeader);
	       	cellToStn.setCellValue("TO");
	       	
	       	XSSFCell cellAWt = row.createCell(6);
	       	cellAWt.setCellStyle(styleSubHeader);
	       	cellAWt.setCellValue("AWT");
	       	
	       	XSSFCell cellGWt = row.createCell(7);
	       	cellGWt.setCellStyle(styleSubHeader);
	       	cellGWt.setCellValue("GWT");
	       	
	       	XSSFCell cellPkg = row.createCell(8);
	       	cellPkg.setCellStyle(styleSubHeader);
	       	cellPkg.setCellValue("PKG");
	       	
	       	XSSFCell cellBillAt = row.createCell(9);
	       	cellBillAt.setCellStyle(styleSubHeader);
	       	cellBillAt.setCellValue("BL");
	       	
	       	XSSFCell cellDc = row.createCell(10);
	       	cellDc.setCellStyle(styleSubHeader);
	       	cellDc.setCellValue("DC");
	       	
	       	XSSFCell cellUpTo30Frt = row.createCell(11);
	       	cellUpTo30Frt.setCellStyle(styleSubHeader);
	       	cellUpTo30Frt.setCellValue("FRT_UpTo30");
	       	
	       	XSSFCell cellAbove30Frt = row.createCell(12);
	       	cellAbove30Frt.setCellStyle(styleSubHeader);
	       	cellAbove30Frt.setCellValue("FRT_Above30");
	       	
	       	XSSFCell cellCumFrt = row.createCell(13);
	       	cellCumFrt.setCellStyle(styleSubHeader);
	       	cellCumFrt.setCellValue("CUMFRT");
	       	
	       	XSSFCell cellChlnNo = row.createCell(14);
	       	cellChlnNo.setCellStyle(styleSubHeader);
	       	cellChlnNo.setCellValue("CHNO");
	       	
	       	XSSFCell cellChlnDt = row.createCell(15);
	       	cellChlnDt.setCellStyle(styleSubHeader);
	       	cellChlnDt.setCellValue("CHDT");
	       	
	       	XSSFCell cellLryNo = row.createCell(16);
	       	cellLryNo.setCellStyle(styleSubHeader);
	       	cellLryNo.setCellValue("LRY");
	       	
	       	XSSFCell cellSedrNo = row.createCell(17);
	       	cellSedrNo.setCellStyle(styleSubHeader);
	       	cellSedrNo.setCellValue("SEDR");
	       	
	       	XSSFCell cellSedrDt = row.createCell(18);
	       	cellSedrDt.setCellStyle(styleSubHeader);
	       	cellSedrDt.setCellValue("SEDRDT");
	       	
	       	XSSFCell cellMemoNo = row.createCell(19);
	       	cellMemoNo.setCellStyle(styleSubHeader);
	       	cellMemoNo.setCellValue("MemoNo");
	       	
	       	XSSFCell cellMemoDt = row.createCell(20);
	       	cellMemoDt.setCellStyle(styleSubHeader);
	       	cellMemoDt.setCellValue("MemoDT");
	       	
	       	XSSFCell cellMemoFrBra = row.createCell(21);
	       	cellMemoFrBra.setCellStyle(styleSubHeader);
	       	cellMemoFrBra.setCellValue("FrBrh.");
	       	
	       	XSSFCell cellMemoToBra = row.createCell(22);
	       	cellMemoToBra.setCellStyle(styleSubHeader);
	       	cellMemoToBra.setCellValue("ToBrh.");
	       	
	       	XSSFCell cellLhpvNo = row.createCell(23);
	       	cellLhpvNo.setCellStyle(styleSubHeader);
	       	cellLhpvNo.setCellValue("LHPV");
	       	
	       	XSSFCell cellLhpvDt = row.createCell(24);
	       	cellLhpvDt.setCellStyle(styleSubHeader);
	       	cellLhpvDt.setCellValue("LHDT");
	       	
	       	XSSFCell cellBrkName = row.createCell(25);
	       	cellBrkName.setCellStyle(styleSubHeader);
	       	cellBrkName.setCellValue("BRK NAME");
	       	
	       	XSSFCell cellBrkCont = row.createCell(26);
	       	cellBrkCont.setCellStyle(styleSubHeader);
	       	cellBrkCont.setCellValue("BRK Cont.");
	       	
	       	XSSFCell cellOwnName = row.createCell(27);
	       	cellOwnName.setCellStyle(styleSubHeader);
	       	cellOwnName.setCellValue("OWN NAME");
	       	
	       	XSSFCell cellOwnCont = row.createCell(28);
	       	cellOwnCont.setCellStyle(styleSubHeader);
	       	cellOwnCont.setCellValue("OWN Cont.");
	       	
	       	
	       	XSSFCell cellCngnor = row.createCell(29);
	       	cellCngnor.setCellStyle(styleSubHeader);
	       	cellCngnor.setCellValue("CNGNOR");
	       	
	       	XSSFCell cellCngnee = row.createCell(30);
	       	cellCngnee.setCellStyle(styleSubHeader);
	       	cellCngnee.setCellValue("CNGNEE");
 			if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
	       		XSSFCell cellTpNo = row.createCell(31);
	       		cellTpNo.setCellStyle(styleSubHeader);
	       		cellTpNo.setCellValue("TP No.");
	       	}
			
	       	double totalFrt = 0.0;
	     //  	System.out.println("CNMTMAPLIST Size  "+cnmtMapList.size());
	       	for (Map<String, Object> cnmtMap : cnmtMapList) {
	       	//	System.out.println(cnmtMap.values());
	       		String prBlCode=String.valueOf(cnmtMap.get(CnmtCNTS.PR_BL_CODE));
	       		
	       	//	System.out.println(prBlCode);
	       	//	System.out.println("Cust Code  "+cnmtMap.get(CnmtCNTS.CUST_CODE));
	       		if(prBlCode.equals("null"))
	       		{
	       			//System.out.println("inside loop "+prBlCode);
	       			prBlCode="0";
	       		}
	       		else
	       		{
               cnmtMap.put(CnmtCNTS.CUST_CODE, "0");
	       			//System.out.println("In else condition");
	       		}
	       		//System.out.println("OutSide Loop   "+prBlCode);
				if (Integer.parseInt(String.valueOf(cnmtMap.get(CnmtCNTS.CUST_CODE))) == customer.getCustId() || Integer.parseInt(prBlCode) == customer.getCustId() ) {
					totalFrt += Double.parseDouble(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_FREIGHT)));
					//cnmtMap.put("cumFrt", totalFrt);
					
					row = spreadsheet.createRow(rowId++);
					System.out.println(String.valueOf(cnmtMap.get("cnmtBrName")).toLowerCase());
					if (cnmtMap.get("cnmtBrName") != null) {
						XSSFCell cellBcdVal = row.createCell(0);
		       			cellBcdVal.setCellStyle(style);
		       			cellBcdVal.setCellValue(String.valueOf(cnmtMap.get("cnmtBrName")).toLowerCase());
					} else {
						XSSFCell cellBcdVal = row.createCell(0);
		       			cellBcdVal.setCellStyle(style);
					}
	       
					if (cnmtMap.get(CnmtCNTS.CNMT_INVOICE_NO) != null) {
	       				XSSFCell cellCnmtInvc = row.createCell(1);
	       				cellCnmtInvc.setCellStyle(style);
	       				cellCnmtInvc.setCellValue(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_INVOICE_NO)));
					} else {
						XSSFCell cellCnmtInvc = row.createCell(1);
						cellCnmtInvc.setCellStyle(style);
					}
					
	       			if (cnmtMap.get(CnmtCNTS.CNMT_CODE) != null) {
	       				XSSFCell cellCnmtValue = row.createCell(2);
		       			cellCnmtValue.setCellStyle(style);
		       			cellCnmtValue.setCellValue(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_CODE)));
					} else {
						XSSFCell cellCnmtValue = row.createCell(2);
		       			cellCnmtValue.setCellStyle(style);
					}
	       			
	       			
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_DT) != null) {
	    	       		XSSFCell cellCnmtDtValue = row.createCell(3);
		    	       	cellCnmtDtValue.setCellStyle(style);
		    	       	cellCnmtDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_DT))));
					} else {
						XSSFCell cellCnmtDtValue = row.createCell(3);
		    	       	cellCnmtDtValue.setCellStyle(style);
		    	    }
	    	       	
	    	       	if (cnmtMap.get("cnmtFrmStnName") != null) {
	    	       		XSSFCell cellFrmStnValue = row.createCell(4);
		    	       	cellFrmStnValue.setCellStyle(style);
		    	       	cellFrmStnValue.setCellValue(String.valueOf(cnmtMap.get("cnmtFrmStnName")).toLowerCase());
					} else {
						XSSFCell cellFrmStnValue = row.createCell(4);
		    	       	cellFrmStnValue.setCellStyle(style);
		    	    }
	    	       	
	    	       	if (cnmtMap.get("cnmtToStnName") != null) {
	    	       		XSSFCell cellToStnValue = row.createCell(5);
		    	       	cellToStnValue.setCellStyle(style);
		    	       	cellToStnValue.setCellValue(String.valueOf(cnmtMap.get("cnmtToStnName")).toLowerCase());
					} else {
						XSSFCell cellToStnValue = row.createCell(5);
		    	       	cellToStnValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_ACTUAL_WT) != null) {
	    	       		XSSFCell cellAWtValue = row.createCell(6);
		    	       	cellAWtValue.setCellStyle(style);
		    	       	cellAWtValue.setCellValue(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_ACTUAL_WT)));
					} else {
						XSSFCell cellAWtValue = row.createCell(6);
		    	       	cellAWtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_GUARANTEE_WT) != null) {
	    	       		XSSFCell cellGWtValue = row.createCell(7);
		    	       	cellGWtValue.setCellStyle(style);
		    	       	cellGWtValue.setCellValue(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_GUARANTEE_WT)));
					} else {
						XSSFCell cellGWtValue = row.createCell(7);
		    	       	cellGWtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_NO_OF_PKG) != null) {
	    	       		XSSFCell cellPkgValue = row.createCell(8);
		    	       	cellPkgValue.setCellStyle(style);
		    	       	cellPkgValue.setCellValue(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_NO_OF_PKG)));
					} else {
						XSSFCell cellPkgValue = row.createCell(8);
		    	       	cellPkgValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get("cnmtBillAt") != null) {
	    	       		XSSFCell cellBillAtValue = row.createCell(9);
		    	       	cellBillAtValue.setCellStyle(style);
		    	       	cellBillAtValue.setCellValue(String.valueOf(cnmtMap.get("cnmtBillAt")).toLowerCase());
					} else {
						XSSFCell cellBillAtValue = row.createCell(9);
		    	       	cellBillAtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_DC) != null) {
	    	       		XSSFCell cellDcValue = row.createCell(10);
		    	       	cellDcValue.setCellStyle(style);
		    	       	cellDcValue.setCellValue(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_DC)));
					} else {
						XSSFCell cellDcValue = row.createCell(10);
		    	       	cellDcValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_FREIGHT+"UpTo30") != null) {
	    	       		XSSFCell cellUpTo30FrtValue = row.createCell(11);
	    	       		cellUpTo30FrtValue.setCellStyle(styleNumber);
	    	       		cellUpTo30FrtValue.setCellValue(Double.parseDouble(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_FREIGHT+"UpTo30"))));
					} else {
						XSSFCell cellUpTo30FrtValue = row.createCell(11);
						cellUpTo30FrtValue.setCellStyle(styleNumber);
					}
	    	       	
	    	       	if (cnmtMap.get(CnmtCNTS.CNMT_FREIGHT+"Above30") != null) {
	    	       		XSSFCell cellAbove30FrtValue = row.createCell(12);
	    	       		cellAbove30FrtValue.setCellStyle(styleNumber);
	    	       		cellAbove30FrtValue.setCellValue(Double.parseDouble(String.valueOf(cnmtMap.get(CnmtCNTS.CNMT_FREIGHT+"Above30"))));
					} else {
						XSSFCell cellAbove30FrtValue = row.createCell(12);
						cellAbove30FrtValue.setCellStyle(styleNumber);
					}
	    	       	
	    	       	XSSFCell cellCumFrtValue = row.createCell(13);
	    	       	cellCumFrtValue.setCellStyle(styleNumber);
	    	       	cellCumFrtValue.setCellValue(totalFrt);
	    	       	
	    	       	if (cnmtMap.get(ChallanCNTS.CHALLAN_CODE) != null) {
	    	       		XSSFCell cellChlnNoValue = row.createCell(14);
		    	       	cellChlnNoValue.setCellStyle(style);
		    	       	cellChlnNoValue.setCellValue(String.valueOf(cnmtMap.get(ChallanCNTS.CHALLAN_CODE)));
					} else {
						XSSFCell cellChlnNoValue = row.createCell(14);
		    	       	cellChlnNoValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(ChallanCNTS.CHLN_DT) != null) {
	    	       		XSSFCell cellChlnDtValue = row.createCell(15);
		    	       	cellChlnDtValue.setCellStyle(style);
		    	       	cellChlnDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(cnmtMap.get(ChallanCNTS.CHLN_DT))));
					} else {
						XSSFCell cellChlnDtValue = row.createCell(15);
		    	       	cellChlnDtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(ChallanCNTS.CHLN_LRY_NO) != null) {
	    	       		XSSFCell cellLryNoValue = row.createCell(16);
		    	       	cellLryNoValue.setCellStyle(style);
		    	       	cellLryNoValue.setCellValue(String.valueOf(cnmtMap.get(ChallanCNTS.CHLN_LRY_NO)).replaceAll("\\s", ""));
					} else {
						XSSFCell cellLryNoValue = row.createCell(16);
		    	       	cellLryNoValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(ArrivalReportCNTS.AR_CODE) != null) {
	    	       		XSSFCell cellSedrNoValue = row.createCell(17);
		    	       	cellSedrNoValue.setCellStyle(style);
		    	       	cellSedrNoValue.setCellValue(String.valueOf(cnmtMap.get(ArrivalReportCNTS.AR_CODE)));
					} else {
						XSSFCell cellSedrNoValue = row.createCell(17);
		    	       	cellSedrNoValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(ArrivalReportCNTS.AR_DATE) != null) {
	    	       		XSSFCell cellSedrDtValue = row.createCell(18);
		    	       	cellSedrDtValue.setCellStyle(style);
		    	       	cellSedrDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(cnmtMap.get(ArrivalReportCNTS.AR_DATE))));
					} else {
						XSSFCell cellSedrDtValue = row.createCell(18);
		    	       	cellSedrDtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(MemoCNTS.MEMO_MEMO_NO) != null) {
	    	       		XSSFCell cellMemoNoValue = row.createCell(19);
	    	       		cellMemoNoValue.setCellStyle(style);
	    	       		cellMemoNoValue.setCellValue(String.valueOf(cnmtMap.get(MemoCNTS.MEMO_MEMO_NO)));
					} else {
						XSSFCell cellMemoNoValue = row.createCell(19);
						cellMemoNoValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(MemoCNTS.MEMO_MEMO_DATE) != null) {
	    	       		XSSFCell cellMemoDtValue = row.createCell(20);
	    	       		cellMemoDtValue.setCellStyle(style);
	    	       		cellMemoDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(cnmtMap.get(MemoCNTS.MEMO_MEMO_DATE))));
					} else {
						XSSFCell cellMemoDtValue = row.createCell(20);
						cellMemoDtValue.setCellStyle(style);
					}
	    	       	
	    	    	if (cnmtMap.get(MemoCNTS.MEMO_FROM_BRANCH) != null) {
	    	       		XSSFCell cellMemoFrBranchValue = row.createCell(21);
	    	       		cellMemoFrBranchValue.setCellStyle(style);
	    	       		cellMemoFrBranchValue.setCellValue(String.valueOf(cnmtMap.get(MemoCNTS.MEMO_FROM_BRANCH)));
					} else {
						XSSFCell cellMemoFrBranchValue = row.createCell(21);
						cellMemoFrBranchValue.setCellStyle(style);
					}
	    	    	
	    	    	if (cnmtMap.get(MemoCNTS.MEMO_TO_BRANCH) != null) {
	    	       		XSSFCell cellMemoToBranchValue = row.createCell(22);
	    	       		cellMemoToBranchValue.setCellStyle(style);
	    	       		cellMemoToBranchValue.setCellValue(String.valueOf(cnmtMap.get(MemoCNTS.MEMO_TO_BRANCH)));
					} else {
						XSSFCell cellMemoToBranchValue = row.createCell(22);
						cellMemoToBranchValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(LhpvStatusCNTS.LS_NO) != null) {
	    	       		XSSFCell cellLhpvNoValue = row.createCell(23);
		    	       	cellLhpvNoValue.setCellStyle(style);
		    	       	cellLhpvNoValue.setCellValue(String.valueOf(cnmtMap.get(LhpvStatusCNTS.LS_NO)));
					} else {
						XSSFCell cellLhpvNoValue = row.createCell(23);
		    	       	cellLhpvNoValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get(LhpvStatusCNTS.LS_DT) != null) {
	    	       		XSSFCell cellLhpvDtValue = row.createCell(24);
		    	       	cellLhpvDtValue.setCellStyle(style);
		    	       	cellLhpvDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(cnmtMap.get(LhpvStatusCNTS.LS_DT))));
					} else {
						XSSFCell cellLhpvDtValue = row.createCell(24);
		    	       	cellLhpvDtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get("brkName") != null) {
	    	       		XSSFCell cellBrkNameValue = row.createCell(25);
		    	       	cellBrkNameValue.setCellStyle(style);
		    	       	cellBrkNameValue.setCellValue(String.valueOf(cnmtMap.get("brkName")).toLowerCase());
					} else {
						XSSFCell cellBrkNameValue = row.createCell(25);
		    	       	cellBrkNameValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get("brkPhNo") != null) {
	    	       		XSSFCell cellBrkContValue = row.createCell(26);
		    	       	cellBrkContValue.setCellStyle(style);
		    	       	cellBrkContValue.setCellValue(String.valueOf(cnmtMap.get("brkPhNo")));
					} else {
						XSSFCell cellBrkContValue = row.createCell(26);
		    	       	cellBrkContValue.setCellStyle(style);
					}
	    	       	
	    	    	if (cnmtMap.get("ownName") != null) {
	    	       		XSSFCell cellOwnNameValue = row.createCell(27);
		    	       	cellOwnNameValue.setCellStyle(style);
		    	       	cellOwnNameValue.setCellValue(String.valueOf(cnmtMap.get("ownName")).toLowerCase());
					} else {
						XSSFCell cellOwnNameValue = row.createCell(27);
		    	       	cellOwnNameValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get("ownPhNo") != null) {
	    	       		XSSFCell cellOwnContValue = row.createCell(28);
		    	       	cellOwnContValue.setCellStyle(style);
		    	       	cellOwnContValue.setCellValue(String.valueOf(cnmtMap.get("ownPhNo")));
					} else {
						XSSFCell cellOwnContValue = row.createCell(28);
		    	       	cellOwnContValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get("cnmtCngorName") != null) {
	    	       		XSSFCell cellCngnorValue = row.createCell(29);
		    	       	cellCngnorValue.setCellStyle(style);
		    	       	cellCngnorValue.setCellValue(String.valueOf(cnmtMap.get("cnmtCngorName")).toLowerCase());
					} else {
						XSSFCell cellCngnorValue = row.createCell(29);
		    	       	cellCngnorValue.setCellStyle(style);
					}
	    	       	
	    	       	if (cnmtMap.get("cnmtCngeeName") != null) {
	    	       		XSSFCell cellCngneeValue = row.createCell(30);
		    	       	cellCngneeValue.setCellStyle(style);
		    	       	cellCngneeValue.setCellValue(String.valueOf(cnmtMap.get("cnmtCngeeName")).toLowerCase());
					} else {
						XSSFCell cellCngneeValue = row.createCell(30);
		    	       	cellCngneeValue.setCellStyle(style);
					}
			if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
	    	       		if (cnmtMap.get("cnmtTpNo") != null) {
		    	       		XSSFCell cellCngneeValue = row.createCell(31);
			    	       	cellCngneeValue.setCellStyle(style);
			    	       	cellCngneeValue.setCellValue(String.valueOf(cnmtMap.get("cnmtTpNo")));
						} else {
							XSSFCell cellCngneeValue = row.createCell(31);
			    	       	cellCngneeValue.setCellStyle(style);
						}
	    	       	}
				} 
	       
			}
			
			
			row = spreadsheet.createRow(rowId++);
			
			for (int i = 0; i <= 10; i++) {
				XSSFCell cust = row.createCell(i);
				cust.setCellStyle(styleSubHeaderNumber);
		        if (i == 0) {
		        	cust.setCellValue("TOTAL    ");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,10));
			
			for (int i = 11; i <= 12; i++) {
				XSSFCell cust = row.createCell(i);
				cust.setCellStyle(styleSubHeaderNumber);
		        if (i == 11) {
		        	map.put("totalStock",+Math.round(totalFrt*100.0)/100.0);
		        	cust.setCellValue(+Math.round(totalFrt*100.0)/100.0);
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,11,12));
			
				int no = 30;
			if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
				no = 31;
			}
			
			for (int i = 13; i <= no; i++) {
				XSSFCell cellEmpty = row.createCell(i);
				cellEmpty.setCellStyle(styleSubHeader);
		    }
				if(faCode.equalsIgnoreCase("0300642") || faCode.equalsIgnoreCase("0300863")){
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,13,31));
	       	}else
	       		spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,13,30));
			listStockTotal.add(map);
			//blank row
			rowId++;
			
		}
		

		spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,31));
		
		row = spreadsheet.createRow(rowId++);
			  for (int i = 6; i <=13; i++) {
	 		//row = spreadsheet.createRow(rowId++);
		XSSFCell cellFinalTot = row.createCell(i);
		cellFinalTot.setCellStyle(styleSubHeader);
		if(i==6){
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,6,7));
		        cellFinalTot.setCellValue("CustomerFACode");
		}
		if(i==8){
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,8,11));
	        cellFinalTot.setCellValue("CustomerName");
		}if(i==12){
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,12,13));
	        cellFinalTot.setCellValue("totalStock");
			
		}
         
    } 
	
		double sumTotal=0;
		Object obj[]=new Object[3];
	   for(int i=0;i<listStockTotal.size();i++){
		   row = spreadsheet.createRow(rowId++);
		   	int m=0;
			     Map<String,Object> map=listStockTotal.get(i);
			     for(Map.Entry<String,Object> me:map.entrySet()){
				  obj[m++]=me.getValue();
			     }
				 for(int j=6;j<=13;j++){
				XSSFCell cellFinalTot = row.createCell(j);
				cellFinalTot.setCellStyle(styleSubHeaderNumber);
				 if(j==12){
					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,12,13));
					  sumTotal+=Double.parseDouble(String.valueOf(obj[0]));
					  cellFinalTot.setCellValue(Double.parseDouble(String.valueOf(obj[0])));
				 }
				 else if(j==6){
					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,6,7));
					cellFinalTot.setCellValue(String.valueOf(obj[1])); 
				 }else if(j==8){
					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,8,11));
					cellFinalTot.setCellValue(String.valueOf(obj[2])); 
				 }
				}
		 }
		
		 row=spreadsheet.createRow(rowId++);
		 
		 for(int j=11;j<=13;j++){
			 XSSFCell totalCell=row.createCell(j);
			  totalCell.setCellStyle(styleSubHeader);
			 if(j==11){
			 spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,11,11));
			  totalCell.setCellValue("total=");
			 }else if(j==12){
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,12,13));
				  totalCell.setCellValue(sumTotal);
			}
		 }
	   
		/*String filePath = ConstantsValues.FOLDER_LOCATION+"/stockRtp"+CodePatternService.get4DigitRanNo()+".xlsx";
		File file = new File(filePath);
		OutputStream os = new FileOutputStream(file);
		workbook.write(os);
		os.flush();
		os.close();*/
		
		response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=stockRtp.xlsx");
		ServletOutputStream out = response.getOutputStream();
		workbook.write(out);
		out.flush();
        out.close();
        
        /*smtpMailSender.sendEmailWithAttachment("mdarif.cs@gmail.com", "Spring Mail Mail", "this mail sent by the fifth part", filePath);
        System.out.println("stockRtp.xlsx written successfully" );*/
	}
}
