package com.mylogistics.controller.report;

import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FontUnderline;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;


@Controller
public class ShortExcessRptCntlr {
	
	@Autowired private ReportDAO reportDAO;
	@Autowired 
	private MoneyReceiptDAO moneyReceiptDAO;
	private Map<String,Object> excessRept=new HashMap<String,Object>();
	private List<Map<String,Object>> excessRptList=new ArrayList<Map<String,Object>>();
	private Map<String,Object> shortExcessRptMap;
	
	@RequestMapping(value="/getShortExcessReportHtml",method=RequestMethod.POST)
	public @ResponseBody Object getShortExcessReportHtml(@RequestBody Map<String,Object> shortExcessRptMap){
		
		this.shortExcessRptMap=shortExcessRptMap;
	
		if((boolean)shortExcessRptMap.get("isConsolidate")){
		 excessRptList=reportDAO.getShortExcesRpt(shortExcessRptMap);

		}
		else if((boolean)shortExcessRptMap.get("isBranch")){
			excessRptList=reportDAO.getShortExcesRpt(shortExcessRptMap);
		
		}
	   if(excessRptList!=null&&!excessRptList.isEmpty()){
			   excessRept.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);   
		   }
		else if(excessRptList==null){
			excessRept.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		else if(excessRptList.isEmpty()){
			excessRept.put(ConstantsValues.RESULT,ConstantsValues.NOT_FOUND);
		}
		return excessRept;
	}
	
	
	
	
	@RequestMapping(value = "/getShortReportXLSX", method=RequestMethod.POST)
	public void getShortExcessReportXLSX(HttpServletRequest request, HttpServletResponse response) throws Exception{
		System.out.println("getShortReportXLSX");
		
						//int row=0;
					XSSFWorkbook workbook=new XSSFWorkbook();
					
					//set header font
					
					XSSFFont headerFont=workbook.createFont();
					     headerFont.setBold(true);
					     headerFont.setFontHeightInPoints((short)18);
					     headerFont.setUnderline(XSSFFont.U_SINGLE);
					     headerFont.setFontName(FontFactory.HELVETICA_BOLD);
					     
					     
					 // set header style...
					     
					XSSFCellStyle cellStyle=workbook.createCellStyle();
					      cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					       cellStyle.setFont(headerFont);
					       
					   // set heading row....
					      
					      int rowId=0; 
					      XSSFSheet sheet=workbook.createSheet();
					        XSSFRow headerRow=sheet.createRow(rowId++);
					         XSSFCell headerCell=headerRow.createCell(0);
					                headerCell.setCellStyle(cellStyle);
					          headerCell.setCellValue("ShortExcessReport ("+CodePatternService.getSqlDate(String.valueOf(shortExcessRptMap.get("toDt")))+")");
					          
					     
					          sheet.addMergedRegion(new CellRangeAddress(0,0,0,13));
					      
			/**************************************main header start*****************************************/
					           rowId++;							
								//set main header font
								
					          	XSSFFont mainHeaderFont=workbook.createFont();
					          			mainHeaderFont.setBold(true);
					          			mainHeaderFont.setFontHeightInPoints((short)10);
					          			mainHeaderFont.setFontName(FontFactory.HELVETICA_BOLD);
					          			mainHeaderFont.setColor(IndexedColors.BLACK.getIndex());
								     
								     
								 // set main header style...
								     
								      XSSFCellStyle  mainCellStyle=workbook.createCellStyle();
								      mainCellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
								      mainCellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
								      mainCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
								      mainCellStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
								       mainCellStyle.setFont(mainHeaderFont);
								       
								   // set main heading row....
							String colName[]={"custName","CustCode","BillNo","Date",
												"BillAmt","cnmtNo","DetAmt","DeductionType",
												"MrNo","MrDate","MrBranch","MrAmt","MRExcAmt","MRDedAmt"};
								      XSSFRow mainHeaderRow =sheet.createRow(rowId++);
								       
								       for(int i=0;i<14;i++){
								        XSSFCell  mainHeaderCell=mainHeaderRow.createCell(i);
								          mainHeaderCell.setCellStyle(mainCellStyle);
								          mainHeaderCell.setCellValue(colName[i]);
								          sheet.setColumnWidth(i,15*256);
								          
								       }
								                
								             
					          
		/************************************* data present***************************************/
						String contentCol[]={"custName","blCustId","blBillNo","blBillDt","blFinalTot","cnmtNo","dedAmt",
								      "dedType","mrNo","mrDate","mrBrhId","mrNetAmt","mrAccessAmt","mrDedAmt"};		
								           XSSFCell contentCell;
								           XSSFCellStyle dataStyle=workbook.createCellStyle();
								             dataStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
								             XSSFCellStyle dataStyle1=workbook.createCellStyle();
								             
								            
								 System.out.println("size"+excessRptList.size());
							 for(Map <String,Object> dataMap:excessRptList){
								 XSSFRow contentRow=sheet.createRow(rowId++);
								         
								 for(int i=0;i<contentCol.length;i++){
									 contentCell=contentRow.createCell(i);
									 if(i==1||i==10){
										 contentCell.setCellStyle(dataStyle1);
									 }else{									  
									  contentCell.setCellStyle(dataStyle);
									 }
								  Object obj=dataMap.get(contentCol[i]);
								   if(obj instanceof Number){
								    	  contentCell.setCellValue(Double.parseDouble(String.valueOf(obj)));
								   }else{  
									    contentCell.setCellValue(String.valueOf(obj));
								     }
								 } 
							}
							 
				/**************** set footer**********************************************/		
							 
							 
							 XSSFFont footerFont=workbook.createFont();
						     footerFont.setBold(true);
						     footerFont.setFontName(FontFactory.HELVETICA_BOLD);
						     
						     
						 // set header style...
						     
						XSSFCellStyle footerCellStyle=workbook.createCellStyle();
						      footerCellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
						       footerCellStyle.setFont(footerFont);	 
					    rowId++;
							 
				XSSFRow footerRow=sheet.createRow(++rowId);
				           XSSFCell cell=footerRow.createCell(3);
				             cell.setCellStyle(footerCellStyle);
				             cell.setCellValue("#############################################################");
				             sheet.addMergedRegion(new CellRangeAddress(rowId,rowId,3,10));
					  
					response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=shortExcess.xlsx");
	             ServletOutputStream out= response.getOutputStream();
	             workbook.write(out);
	             out.close();
	            
        
		System.out.println("Writesheet.xlsx written successfully" );
	
	}
	
	
	
	
	@RequestMapping(value="/removeBillFromSrtExs",method=RequestMethod.POST)
	public @ResponseBody Object removeBillFromSrtExs(@RequestBody Map<String,Object> clientMap){
		Map<String,String> map=new HashMap<>();
		int custId=Integer.parseInt(clientMap.get("custCode").toString());
		String billNo=clientMap.get("billNo").toString();
		map=moneyReceiptDAO.removeBillFromSrtExs(custId, billNo);
		return map;
	}
	
	
	
}
