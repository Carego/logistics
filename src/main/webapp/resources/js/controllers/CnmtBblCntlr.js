'use restrict'

var app = angular.module('application')

app.controller('CnmtBblCntlr', ['$scope','$http','$log','$filter',
                                function($scope, $http, $log, $filter) {
	console.log("CnmtBblCntlr Started");
	
	$scope.branch = {};
	$scope.branchList = [];
	
	$scope.code = "";
	$scope.rcNo="";
	$scope.codeList = [];
	
	$scope.cust = {};
	$scope.custList = [];
	
	$scope.emp = {};
	$scope.empList = [];
	
	
	$scope.frmStn = {};
	$scope.frmStnList = [];
	$scope.toStn = {};
	$scope.toStnList = [];
	//$scope.stnList = [];
	
	$scope.vehList = [];
	
	$scope.brk = {};
	$scope.brkList = [];
	
	$scope.own = {};
	$scope.ownList = [];
	
	$scope.contList = [];
	$scope.cont = {};
	
	$scope.panHdrType = "";
	$scope.rcNo = "";

	$scope.codeDBFlag = true;
	$scope.custDBFlag = true;
	$scope.frmStnDBFlag = true;
	$scope.toStnDBFlag = true;
	$scope.empDBFlag = true;
	$scope.vehDBFlag = true;
	$scope.brkDBFlag = true;
	$scope.ownDBFlag = true;
	$scope.contCodeDBFlag=true;
	
	var codeIndex = 0;
	
	
	$scope.getFYear = function(){
		var response = $http.get($scope.projectName+'/getCurntFYear');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("Entered Date in current financial year");
				console.log(data.fYear.fyFrmDt);
				console.log(data.fYear.fyToDt);
				$scope.fyFrmDt=data.fYear.fyFrmDt;
				$scope.fyToDt=data.fYear.fyToDt;
			}else{
				$scope.alertToast("There is no any current Financial year");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.alertToast("There is some problem in fetching date");
		});
	}();
	
	
	$scope.veryFyDt=function(){
		console.log("veryFyDt------>Kamal");
		var date = $filter('date')(new Date, "yyyy-MM-dd");
		if($scope.dt<$scope.fyFrmDt || $scope.dt>$scope.fyToDt){
			$scope.dt="";
			$scope.alertToast("Date should be in current Financial year");
		}else if($scope.dt>date){
			$scope.dt="";
			$scope.alertToast("Date can't be greater than today's date");
		}
	}
	
	
	$scope.getUsrBrCnmtCust = function() {
		console.log("getUsrBrCnmtCust()");
		var res = $http.post($scope.projectName+'/getUsrBrCnmtCust');
		res.success(function(data, status, headers, config) {
			
			if (data.result === "success") {
				$log.info(data);
				$scope.branch = data.branch;
				//$scope.codeList = data.codeList;
				$scope.custList = data.custList;
				$scope.saveCust(data.custList[0]);
				//$scope.cust = data.custList[0];
				
				//$scope.getStnList();
				//$scope.getEmpList();
			}else {
				$log.info(data);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getUsrBrCnmtCust "+data);
		});
	}
	
	/*$scope.getStnList = function() {
		console.log("getStnList()");
		var res = $http.post($scope.projectName+'/getStnList');
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getStnList result "+data.result);
				$scope.stnList = data.stnList;
			} else {
				console.log("getStnList result "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getStnList "+data);
		});
	}*/
	
	$scope.getFrmStnList = function() {
		console.log("getFrmStnList()");
		var res = $http.post($scope.projectName+'/getFrmStnList', $scope.cust.custCode);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getFrmStnList result "+data.result);
				$scope.frmStnList = data.frmStnList;
			} else {
				console.log("getFrmStnList result "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getFrmStnList "+data);
		});
	}
	
	$scope.getToStnList = function() {
		console.log("getToStnList()");
		var res = $http.post($scope.projectName+'/getToStnList', $scope.cont.contCode);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getToStnList result "+data.result);
				$scope.toStnList = data.toStnList;
			} else {
				console.log("getToStnList result "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getToStnList "+data);
		});
	}
	
	$scope.getEmpList = function() {
		console.log("getEmpList()");
		var res = $http.post($scope.projectName+'/getEmpList');
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getEmpList response "+data.result);
				$scope.empList = data.empList;
				//$scope.getVehicleMstr();
			} else {
				console.log("getEmpList response "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getEmpList "+data);
		});
	}
	
	$scope.getVehicleMstr = function(){
		console.log("enter into getVehicleMstr function");
		
		if($scope.rcNo.length==4){
		
			var vhNo=$scope.rcNo;  
			
			console.log($scope.rcNo+" enter into getVehicleMstr function="+ vhNo);
			
		var response = $http.post($scope.projectName+'/getBblVehicleMstrFC', vhNo);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				 $scope.vehList = data.list;
				 console.log("size of $scope.vehList = "+$scope.vehList.length);
				// $scope.getBrokerList();
				 
				 
				 console.log("enter into openVehicleDB funciton");
					$scope.vehDBFlag=false;
					$('div#vehDB').dialog({
						autoOpen: false,
						modal:true,
						title: "Select Vehicle",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
							$scope.vehDBFlag=true;
						}
					});
					
					$('div#vehDB').dialog('open');
				 
				 
				 
				 
			  }else{
				 $scope.alertToast("Vehicles are not avaliable");
				 console.log("error in fetching data from getVehicleMstr");
			  }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
		  
		}
	}
	
	/*$scope.getBrokerList = function(){
		console.log("getBrokerCode------>");
		var response = $http.post($scope.projectName+'/getBrkCodeList');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.brkList = data.brkCodeList;
				//$scope.getOwnerList();
			}else{
				$scope.alertToast("you don't have any broker");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.getOwnerList = function(){
		console.log("getOwnerCode------>");
		var response = $http.post($scope.projectName+'/getOwnCodeList');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.ownList = data.ownCodeList;
			}else{
				$scope.alertToast("you don't have any owner");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}	
	*/
	
	
	
	$scope.openCodeDB = function() {
		console.log("openCodeDB()");
		
		if($scope.code.length==4){
			
			var cnmtCode=$scope.code;
			console.log("cnmtCode "+cnmtCode);	
			var res = $http.post($scope.projectName+'/getBblCnmtCode', cnmtCode);
			res.success(function(data, status, headers, config) {
				
				if(data.result === "success"){
					$scope.codeDBFlag = false;
					  $scope.codeList = data.codeList;
					  $('div#codeDB').dialog({
						  autoOpen: false,
						  modal:true,
						  resizable: false,
						  position: UDPos,
						  show: UDShow,
						  hide: UDHide,
						  title: "Cnmt/Chln",
						  draggable: true,
						  close: function(event, ui) { 
							  $(this).dialog('destroy');
							  $(this).hide();
							  $scope.codeDBFlag = true;
							  
						  }
					  });
					  $('div#codeDB').dialog('open');
						}
				 else{
						$scope.alertToast("No Record found");
					}
				});
				
			res.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	
	
	$scope.saveCode = function(code, index) {
		console.log("saveCode()");
		$scope.code = code;
		codeIndex = index;
		$('div#codeDB').dialog('close');
	}
	
	$scope.openCustDB = function() {
		console.log("openCustDB()");
		$scope.custDBFlag = false;
		$('div#custDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custDBFlag = true;
			}
		});
		$('div#custDB').dialog('open');
	}
	
	$scope.saveCust = function(cust) {
		console.log("saveCust()");
		$scope.cust = cust;
		$('div#custDB').dialog('close');
		$scope.getFrmStnList();
	}
	
	$scope.openFrmStnDB = function() {
		console.log("openFrmStnDB()");
		$scope.frmStnDBFlag = false;
		$('div#frmStnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.frmStnDBFlag = true;
			}
		});
		$('div#frmStnDB').dialog('open');
	}
	
	$scope.saveFrmStn = function(frmStn) {
		console.log("saveFrmStn()");
		$scope.frmStn = frmStn;
		$('div#frmStnDB').dialog('close');
		
		if(angular.isUndefined($scope.cust.custName) || $scope.cust.custName === null || $scope.cust.custName === ""||
				angular.isUndefined($scope.dt)  || $scope.dt === null || $scope.dt === ""||
				angular.isUndefined($scope.frmStn.stnName)  || $scope.frmStn.stnName === null || $scope.frmStn.stnName === ""){
				$('#contCodeId').attr("disabled","disabled");
				$scope.alertToast("Please enter above detail first");
			}else{
				var group = {
						"custCode"		:  $scope.cust.custCode,
						"cnmtDt"		: $scope.dt,
						"cnmtFromSt"	: ""+$scope.frmStn.stnId //this code convert int to string
				}
				var response = $http.post($scope.projectName+'/getContractData',group);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
						$scope.contList = data.list;
						$scope.alertToast("relevant contract exists enter contract code");
						$('#contCodeId').removeAttr("disabled");
					}else{
						$scope.alertToast("relevant contract does not exists enter contract code");
						console.log("msg from server----->"+data.result);
						$scope.cont = {};
						$('#contCodeId').attr("disabled","disabled");
					}
					console.log("----->from server = "+$scope.contList.length);
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		
	}
	
	$scope.openContCodeDB = function(){
		console.log("openContCodeDB()");
		$scope.contCodeDBFlag=false;
		$('div#contractCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Contract Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
				$scope.contCodeDBFlag=true;
			}
		});

		$('div#contractCodeDB').dialog('open');
	}
	
	$scope.saveContCode = function(cont){
		console.log("saveContCode()");
		$scope.cont = cont;
		$('div#contractCodeDB').dialog('close');
		$scope.getToStnList();
	}
	
	$scope.openToStnDB = function() {
		console.log("openToStnDB()");
		$scope.toStnDBFlag = false;
		$('div#toStnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.toStnDBFlag = true;
			}
		});
		$('div#toStnDB').dialog('open');
	}
	
	$scope.saveToStn = function(toStn) {
		console.log("saveToStn()");
		$('div#toStnDB').dialog('close');
		
		if (angular.isUndefined($scope.cont.contCode) || $scope.cont.contCode === null || $scope.cont.contCode === "") {
			$scope.alertToast("please enter contract code first");
		} else {
			$scope.toStn = toStn;
			
			//get rate according to contCode, dt, toStn
			var rateService = {
					"contCode"	: $scope.cont.contCode,
					"dt"		: $scope.dt,
					"toStnId"	: $scope.toStn.stnId
			}
			
			var res = $http.post($scope.projectName+'/getRateFrBbl', rateService);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("getRateFrBbl response: "+data.result);
					$scope.cnmtRate = data.rate;
				} else {
					console.log("getRateFrBbl response: "+data.result);
					$scope.alertToast("rate not found");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response getRateFrBbl: "+data);
				$scope.alertToast("Database exception");
			});
		}
		
	}
	
	$scope.openEmpDB = function() {
		console.log("openEmpDB()");
		$scope.getEmpList();
		$scope.empDBFlag = false;
		$('div#empDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.empDBFlag = true;
			}
		});
		$('div#empDB').dialog('open');
	}
	
	$scope.saveEmp = function(emp) {
		console.log("saveEmp()");
		$scope.emp = emp;
		$('div#empDB').dialog('close');
	}
	
	$scope.openVehicleDB = function(){
		console.log("enter into openVehicleDB funciton");
		$scope.vehDBFlag=false;
		$('div#vehDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Select Vehicle",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
				$scope.vehDBFlag=true;
			}
		});
		
		$('div#vehDB').dialog('open');
	}
	
	$scope.saveVehicle = function(vehicle) {
		console.log("saveVehicle()");
		$scope.rcNo = vehicle;
		$('div#vehDB').dialog('close');
		
		//get owner and broker for vehicle
		var res = $http.post($scope.projectName+'/getOwnBrkFrVehicle', $scope.rcNo);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("response of getOwnBrkFrVehicle: "+data.result);
				$scope.own = data.own;
				$scope.brk = data.brk;
			} else {
				console.log("response of getOwnBrkFrVehicle: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response of getOwnBrkFrVehicle: "+data);
		});
	}
	
	//open brokerDB
	$scope.openBrkDB = function() {
		console.log("openBrkDB()");
		$scope.brkDBFlag = false;
		$('div#brkDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brkDBFlag = true;
			}
		});

		$('div#brkDB').dialog('open');
	}
	
	$scope.saveBrk = function(brk) {
		console.log("saveBrk()");
		//empty broker phone text box
		$scope.brk = brk;
		$('div#brkDB').dialog('close');
	}
	
	//openOwnDB
	$scope.openOwnDB = function() {
		console.log("openOwnDB()");
		$scope.ownDBFlag = false;
		$('div#ownDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Owner",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownDBFlag = true;
			}
		});

		$('div#ownDB').dialog('open');
	}
	
	$scope.saveOwn = function(own) {
		console.log("saveOwn()");
		$scope.own = own;
		$('div#ownDB').dialog('close');
	}
	
	$scope.calCnmtFrt = function() {
		console.log("calCnmtFrt()");
		$scope.cnmtFrt = parseFloat($scope.cnmtRate) * parseFloat($scope.wt);
		//$scope.cnmtFrt = Math.round(parseFloat($scope.cnmtFrt)*1000)/1000;
		$scope.cnmtFrt = Math.round(parseFloat($scope.cnmtFrt));
	}
	
	$scope.calChlnFrt = function() {
		console.log("calChlnFrt()");
		$scope.chlnFrt = parseFloat($scope.chlnRate) * parseFloat($scope.wt);
		//$scope.chlnFrt = Math.round(parseFloat($scope.chlnFrt)*1000)/1000;
		$scope.chlnFrt = Math.round(parseFloat($scope.chlnFrt));
	}
	
	$scope.calBal = function() {
		console.log("calBal()");
		$scope.bal = parseFloat($scope.chlnFrt) - parseFloat($scope.adv);
	}
	
	$scope.submitCnmtBblForm = function(cnmtBblForm) {
		console.log("submitCnmtBblForm()");
		
		if (cnmtBblForm.$invalid) {
			console.log("invalidate: "+cnmtBblForm.$invalid);
			if (cnmtBblForm.branchName.$invalid) {
				$scope.alertToast("Please enter valid Branch Name");
			} else if (cnmtBblForm.codeName.$invalid) {
				$scope.alertToast("Please enter valid chln/cnmt Code");
			} else if (cnmtBblForm.custName.$invalid) {
				$scope.alertToast("Please enter valid Customer");
			} else if (cnmtBblForm.dtName.$invalid) {
				$scope.alertToast("Please enter valid Date");
			} else if (cnmtBblForm.frmStnName.$invalid) {
				$scope.alertToast("Please enter valid From Station");
			} else if (cnmtBblForm.contCodeName.$invalid) {
				$scope.alertToast("Please enter valid contract code");
			} else if (cnmtBblForm.toStnName.$invalid) {
				$scope.alertToast("Please enter valid to station name");
			} else if (cnmtBblForm.empName.$invalid) {
				$scope.alertToast("Please enter valid employee name");
			} else if (cnmtBblForm.cnmtRateName.$invalid) {
				$scope.alertToast("Please enter valid cnmt rate");
			} else if (cnmtBblForm.wtName.$invalid) {
				$scope.alertToast("Please enter valid weight");
			} else if (cnmtBblForm.cnmtFrtName.$invalid) {
				$scope.alertToast("Please enter valid cnmt freight");
			} else if (cnmtBblForm.tpNoName.$invalid) {
				$scope.alertToast("Please enter valid TP No");
			} else if (cnmtBblForm.vehName.$invalid) {
				$scope.alertToast("Please enter valid Vehicle");
			} else if (cnmtBblForm.ownName.$invalid) {
				$scope.alertToast("Please enter valid Owner");
			} else if (cnmtBblForm.brkName.$invalid) {
				$scope.alertToast("Please enter valid broker");
			} /*else if (cnmtBblForm.panHdrTypeName.$invalid) {
				$scope.alertToast("Please enter valid pan holder type");
			}*/ else if (cnmtBblForm.chlnRateName.$invalid) {
				$scope.alertToast("Please enter valid chln rate");
			} else if (cnmtBblForm.chlnFrtName.$invalid) {
				$scope.alertToast("Please enter valid challan freight");
			} else if (cnmtBblForm.advName.$invalid) {
				$scope.alertToast("Please enter valid advance");
			} else if (cnmtBblForm.balName.$invalid) {
				$scope.alertToast("Please enter valid balance");
			} 
		}else {
			
			if ($scope.code.length != 10) {
				$scope.alertToast("Please enter valid cnmt/chln no");
			} else {
				console.log("invalidate: "+cnmtBblForm.$invalid);
				var cnmtBblService = {
						"branchId"	: $scope.branch.branchId,
						"code"		: $scope.code,
						"custId"	: $scope.cust.custId,
						"dt"		: $scope.dt,
						"frmStnId"	: $scope.frmStn.stnId,
						"contCode"	: $scope.cont.contCode,
						"toStnId"	: $scope.toStn.stnId,
						"empId"		: $scope.emp.empId,
						"cnmtRate"	: $scope.cnmtRate,
						"wt"		: $scope.wt,
						"cnmtFrt"	: $scope.cnmtFrt,
						"tpNo"		: $scope.tpNo,
						"rcNo"		: $scope.rcNo,
						"ownId"		: $scope.own.ownId,
						"brkId"		: $scope.brk.brkId,
						/*"panHdrType": $scope.panHdrType,*/
						"chlnRate"	: $scope.chlnRate,
						"chlnFrt"	: $scope.chlnFrt,
						"adv"		: $scope.adv,
						"bal"		: $scope.bal
				}
				
				var res = $http.post($scope.projectName+'/saveCnmtBbl', cnmtBblService);
				res.success(function(data, status, headers, config) {
					if (data.result === "success") {
						console.log("response of saveCnmtBbl "+data.result);
						
						//clean resources
						$scope.code = "";
						//$scope.cust = {};
						$scope.emp = {};
						$scope.frmStn = {};
						$scope.toStn = {};
						$scope.brk = {};
						$scope.own = {};
						$scope.cont = {};
						$scope.panHdrType = "";
						$scope.rcNo = "";
						$scope.cnmtRate = "";
						$scope.cnmtFrt = "";
						$scope.wt = "";
						$scope.tpNo = "";
						$scope.chlnFrt = "";
						$scope.chlnRate = "";
						$scope.adv = "";
						$scope.bal = "";
						$scope.dt = "";
						
						//remove cnmt/chln from code list
						$scope.codeList.splice(codeIndex, 1);
						
						$scope.alertToast(data.result);
						$('#contCodeId').attr("disabled","disabled");
					} else {
						console.log("response of saveCnmtBbl "+data.result);
						$scope.alertToast(data.msg);
					}
				});
				res.error(function(data, status, headers, config) {
					console.log("error in response of saveCnmtBbl "+data);
				});
			}
		}
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getUsrBrCnmtCust();
		//$scope.getVehicleMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	console.log("CnmtBblCntlr Ended");
}]);