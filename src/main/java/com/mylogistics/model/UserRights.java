package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userrights")
public class UserRights {
	
	@Id
	@Column(name="urId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int urId;
	
	@Column(name="userId")
	private int userId;
	
	@Column(name="urRent" , columnDefinition="boolean default false")
	private boolean urRent;
	
	@Column(name="urBranch" , columnDefinition="boolean default false")
	private boolean urBranch;
	
	@Column(name="urEmployee" , columnDefinition="boolean default false")
	private boolean urEmployee;
	
	@Column(name="urAssigned" , columnDefinition="boolean default false")
	private boolean urAssigned;
	
	@Column(name="urCustomer" , columnDefinition="boolean default false")
	private boolean urCustomer;
	
	@Column(name="urContract" , columnDefinition="boolean default false")
	private boolean urContract;
	
	@Column(name="urDlyContract" , columnDefinition="boolean default false")
	private boolean urDlyContract;
	
	@Column(name="urCnmt" , columnDefinition="boolean default false")
	private boolean urCnmt;
	
	@Column(name="urCnmtBbl" , columnDefinition="boolean default false")
	private boolean urCnmtBbl;
	
	@Column(name="urChallan" , columnDefinition="boolean default false")
	private boolean urChallan;
	
	@Column(name="urSedr" , columnDefinition="boolean default false")
	private boolean urSedr;
	
	@Column(name="urStnSup" , columnDefinition="boolean default false")
	private boolean urStnSup;
	
	@Column(name="urOwner" , columnDefinition="boolean default false")
	private boolean urOwner;
	
	@Column(name="urBroker" , columnDefinition="boolean default false")
	private boolean urBroker;
	
	@Column(name="urBill" , columnDefinition="boolean default false")
	private boolean urBill;
	
	@Column(name="urMR" , columnDefinition="boolean default false")
	private boolean urMR;
	
	@Column(name="urMisc" , columnDefinition="boolean default false")
	private boolean urMisc;
	
	@Column(name="urVouch" , columnDefinition="boolean default false")
	private boolean urVouch;
	
	@Column(name="urBank" , columnDefinition="boolean default false")
	private boolean urBank;
	
	@Column(name="urFT" , columnDefinition="boolean default false")
	private boolean urFT;
	
	@Column(name="urChq" , columnDefinition="boolean default false")
	private boolean urChq;
	
	@Column(name="urTel" , columnDefinition="boolean default false")
	private boolean urTel;
	
	@Column(name="urEle" , columnDefinition="boolean default false")
	private boolean urEle;
	
	@Column(name="urVeh" , columnDefinition="boolean default false")
	private boolean urVeh;
	
	@Column(name="urAtm" , columnDefinition="boolean default false")
	private boolean urAtm;
	
	@Column(name="urVehVen" , columnDefinition="boolean default false")
	private boolean urVehVen;
	
	@Column(name="urPanVal" , columnDefinition="boolean default false")
	private boolean urPanVal;
	
	@Column(name="urEditCnmt", columnDefinition="boolean default false")
	private boolean urEditCnmt;
	
	@Column(name="urEditChln", columnDefinition="boolean default false")
	private boolean urEditChln;
	
	@Column(name="urEnquiry", columnDefinition="boolean default false")
	private boolean urEnquiry;
	
	@Column(name="urReports", columnDefinition="boolean default false")
	private boolean urReports;
	
	@Column(name="urLedger", columnDefinition="boolean default false")
	private boolean urLedger;
	
	@Column(name="urTrial", columnDefinition="boolean default false")
	private boolean urTrial;
	
	@Column(name="urCustStationary", columnDefinition="boolean default false")
	private boolean urCustStationary;
	
	@Column(name="urEditCS", columnDefinition="boolean default false")
	private boolean urEditCS;
	
	@Column(name="urCancelMr", columnDefinition="boolean default false")
	private boolean urCancelMr;
	
	@Column(name="urEditBill", columnDefinition="boolean default false")
	private boolean urEditBill;
	
	@Column(name="urBackLhpv", columnDefinition="boolean default false")
	private boolean urBackLhpv;
	
	@Column(name="urSedrAlw", columnDefinition="boolean default false")
	private boolean urSedrAlw;
	
	@Column(name="urCancelChln", columnDefinition="boolean default false")
	private boolean urCancelChln;
	
	@Column(name="urCancelCnmt", columnDefinition="boolean default false")
	private boolean urCancelCnmt;
	
	@Column(name="urCancelSedr", columnDefinition="boolean default false")
	private boolean urCancelSedr;
	
	@Column(name="urRelBl", columnDefinition="boolean default false")
	private boolean urRelBl;
	
	@Column(name="alwSrtgDmgAr", columnDefinition="boolean default false")
	private boolean alwSrtgDmgAr;
	
	@Column(name="urBillCancel", columnDefinition="boolean default false")
	private boolean urBillCancel;
	
	@Column(name="urCnList", columnDefinition="boolean default false")
	private boolean urCnList;
	
	@Column(name="urHoLhpv", columnDefinition="boolean default false")
	private boolean urHoLhpv;
	
	@Column(name="urAckValidation", columnDefinition="boolean default false")
	private boolean urAckValidation;
	
	@Column(name="urOldLhpvBal", columnDefinition="boolean default false")
	private boolean urOldLhpvBal;
	
	@Column(name="urGenrateLbExcel", columnDefinition="boolean default false")
	private boolean urGenrateLbExcel;
	
	@Column(name="urGenrateRelExcel", columnDefinition="boolean default false")
	private boolean urGenrateRelExcel;
	
	@Column(name="urFundAllocation", columnDefinition="boolean default false")
	private boolean urFundAllocation;
	
	@Column(name="urCosting", columnDefinition="boolean default false")
	private boolean urCosting;
	
	@Column(name="urPetroExcel", columnDefinition="boolean default false")
	private boolean urPetroExcel;
	
	@Column(name="urShrtExcessUpdate", columnDefinition="boolean default false")
	private boolean urShrtExcessUpdate;
	
	@Column(name="urBillPrint", columnDefinition="boolean default false")
	private boolean urBillPrint;
	
	@Column(name="urLhpvBalCash", columnDefinition="boolean default false")
	private boolean urLhpvBalCash;
	
	@Column(name="urMisngStnry", columnDefinition="boolean default false")
	private boolean urMisngStnry;
	
	@Column(name="urLhpvAlw", columnDefinition="boolean default false")
	private boolean urLhpvAlw;
	
	public boolean isUrRelBl() {
		return urRelBl;
	}

	public boolean isUrShrtExcessUpdate() {
		return urShrtExcessUpdate;
	}

	public void setUrShrtExcessUpdate(boolean urShrtExcessUpdate) {
		this.urShrtExcessUpdate = urShrtExcessUpdate;
	}

	public void setUrRelBl(boolean urRelBl) {
		this.urRelBl = urRelBl;
	}

	public boolean isUrCancelChln() {
		return urCancelChln;
	}

	public void setUrCancelChln(boolean urCancelChln) {
		this.urCancelChln = urCancelChln;
	}

	@Column(name="urRvrsLhpvAdv", columnDefinition="boolean default false")
	private boolean urRvrsLhpvAdv;
	
	@Column(name="urRvrsLhpvBal", columnDefinition="boolean default false")
	private boolean urRvrsLhpvBal;
	
	@Column(name="urBillHO", columnDefinition="boolean default false")
	private boolean urBillHO;

	@Column(name="allowAr", columnDefinition="boolean default false")
	private boolean allowAr;
	
	@Column(name="reIssue", columnDefinition="boolean default false")
	private boolean reIssue;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="urManualBill", columnDefinition="boolean default false")
	private boolean urManualBill;
	
	@Column(name="urRentExcel", columnDefinition="boolean default false")
	private boolean urRentExcel;
	
	@Column(name="urAccValid", columnDefinition="boolean default false")
	private boolean urAccValid;
	
	public boolean isUrVehVen() {
		return urVehVen;
	}

	public void setUrVehVen(boolean urVehVen) {
		this.urVehVen = urVehVen;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public int getUrId() {
		return urId;
	}

	public void setUrId(int urId) {
		this.urId = urId;
	}

	public boolean isUrBranch() {
		return urBranch;
	}

	public void setUrBranch(boolean urBranch) {
		this.urBranch = urBranch;
	}

	public boolean isUrEmployee() {
		return urEmployee;
	}

	public void setUrEmployee(boolean urEmployee) {
		this.urEmployee = urEmployee;
	}

	public boolean isUrAssigned() {
		return urAssigned;
	}

	public void setUrAssigned(boolean urAssigned) {
		this.urAssigned = urAssigned;
	}

	public boolean isUrCustomer() {
		return urCustomer;
	}

	public void setUrCustomer(boolean urCustomer) {
		this.urCustomer = urCustomer;
	}

	public boolean isUrContract() {
		return urContract;
	}

	public void setUrContract(boolean urContract) {
		this.urContract = urContract;
	}

	public boolean isUrCnmt() {
		return urCnmt;
	}

	public void setUrCnmt(boolean urCnmt) {
		this.urCnmt = urCnmt;
	}

	public boolean isUrChallan() {
		return urChallan;
	}

	public void setUrChallan(boolean urChallan) {
		this.urChallan = urChallan;
	}

	public boolean isUrSedr() {
		return urSedr;
	}

	public void setUrSedr(boolean urSedr) {
		this.urSedr = urSedr;
	}

	public boolean isUrStnSup() {
		return urStnSup;
	}

	public void setUrStnSup(boolean urStnSup) {
		this.urStnSup = urStnSup;
	}

	public boolean isUrOwner() {
		return urOwner;
	}

	public void setUrOwner(boolean urOwner) {
		this.urOwner = urOwner;
	}

	public boolean isUrBroker() {
		return urBroker;
	}

	public void setUrBroker(boolean urBroker) {
		this.urBroker = urBroker;
	}

	public boolean isUrBill() {
		return urBill;
	}

	public void setUrBill(boolean urBill) {
		this.urBill = urBill;
	}

	public boolean isUrMisc() {
		return urMisc;
	}

	public void setUrMisc(boolean urMisc) {
		this.urMisc = urMisc;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean isUrVouch() {
		return urVouch;
	}

	public void setUrVouch(boolean urVouch) {
		this.urVouch = urVouch;
	}

	public boolean isUrBank() {
		return urBank;
	}

	public void setUrBank(boolean urBank) {
		this.urBank = urBank;
	}

	public boolean isUrTel() {
		return urTel;
	}

	public void setUrTel(boolean urTel) {
		this.urTel = urTel;
	}

	public boolean isUrEle() {
		return urEle;
	}

	public void setUrEle(boolean urEle) {
		this.urEle = urEle;
	}

	public boolean isUrVeh() {
		return urVeh;
	}

	public void setUrVeh(boolean urVeh) {
		this.urVeh = urVeh;
	}

	public boolean isUrFT() {
		return urFT;
	}

	public void setUrFT(boolean urFT) {
		this.urFT = urFT;
	}

	public boolean isUrChq() {
		return urChq;
	}

	public void setUrChq(boolean urChq) {
		this.urChq = urChq;
	}

	public boolean isUrDlyContract() {
		return urDlyContract;
	}

	public void setUrDlyContract(boolean urDlyContract) {
		this.urDlyContract = urDlyContract;
	}

	public boolean isUrAtm() {
		return urAtm;
	}

	public void setUrAtm(boolean urAtm) {
		this.urAtm = urAtm;
	}

	public boolean isUrRent() {
		return urRent;
	}

	public void setUrRent(boolean urRent) {
		this.urRent = urRent;
	}

	public boolean isUrPanVal() {
		return urPanVal;
	}

	public void setUrPanVal(boolean urPanVal) {
		this.urPanVal = urPanVal;
	}

	public boolean isUrMR() {
		return urMR;
	}

	public void setUrMR(boolean urMR) {
		this.urMR = urMR;
	}

	public boolean isUrEditCnmt() {
		return urEditCnmt;
	}

	public void setUrEditCnmt(boolean urEditCnmt) {
		this.urEditCnmt = urEditCnmt;
	}

	public boolean isUrEditChln() {
		return urEditChln;
	}

	public void setUrEditChln(boolean urEditChln) {
		this.urEditChln = urEditChln;
	}

	public boolean isUrEnquiry() {
		return urEnquiry;
	}

	public void setUrEnquiry(boolean urEnquiry) {
		this.urEnquiry = urEnquiry;
	}

	public boolean isUrReports() {
		return urReports;
	}

	public void setUrReports(boolean urReports) {
		this.urReports = urReports;
	}

	public boolean isUrLedger() {
		return urLedger;
	}

	public void setUrLedger(boolean urLedger) {
		this.urLedger = urLedger;
	}

	public boolean isUrTrial() {
		return urTrial;
	}

	public void setUrTrial(boolean urTrial) {
		this.urTrial = urTrial;
	}

	public boolean isUrCustStationary() {
		return urCustStationary;
	}

	public void setUrCustStationary(boolean urCustStationary) {
		this.urCustStationary = urCustStationary;
	}

	public boolean isUrEditCS() {
		return urEditCS;
	}

	public void setUrEditCS(boolean urEditCS) {
		this.urEditCS = urEditCS;
	}

	public boolean isUrCancelMr() {
		return urCancelMr;
	}

	public void setUrCancelMr(boolean urCancelMr) {
		this.urCancelMr = urCancelMr;
	}

	public boolean isUrEditBill() {
		return urEditBill;
	}

	public void setUrEditBill(boolean urEditBill) {
		this.urEditBill = urEditBill;
	}

	public boolean isUrBackLhpv() {
		return urBackLhpv;
	}

	public void setUrBackLhpv(boolean urBackLhpv) {
		this.urBackLhpv = urBackLhpv;
	}

	public boolean isUrCnmtBbl() {
		return urCnmtBbl;
	}

	public void setUrCnmtBbl(boolean urCnmtBbl) {
		this.urCnmtBbl = urCnmtBbl;
	}

	public boolean isUrSedrAlw() {
		return urSedrAlw;
	}

	public void setUrSedrAlw(boolean urSedrAlw) {
		this.urSedrAlw = urSedrAlw;
	}

	public boolean isUrRvrsLhpvAdv() {
		return urRvrsLhpvAdv;
	}

	public void setUrRvrsLhpvAdv(boolean urRvrsLhpvAdv) {
		this.urRvrsLhpvAdv = urRvrsLhpvAdv;
	}

	public boolean isUrCancelCnmt() {
		return urCancelCnmt;
	}

	public void setUrCancelCnmt(boolean urCancelCnmt) {
		this.urCancelCnmt = urCancelCnmt;
	}

	public boolean isUrCancelSedr() {
		return urCancelSedr;
	}

	public void setUrCancelSedr(boolean urCancelSedr) {
		this.urCancelSedr = urCancelSedr;
	}

	public boolean isUrRvrsLhpvBal() {
		return urRvrsLhpvBal;
	}

	public void setUrRvrsLhpvBal(boolean urRvrsLhpvBal) {
		this.urRvrsLhpvBal = urRvrsLhpvBal;
	}

	public boolean isUrBillHO() {
		return urBillHO;
	}

	public void setUrBillHO(boolean urBillHO) {
		this.urBillHO = urBillHO;
	}

	public boolean isAlwSrtgDmgAr() {
		return alwSrtgDmgAr;
	}

	public void setAlwSrtgDmgAr(boolean alwSrtgDmgAr) {
		this.alwSrtgDmgAr = alwSrtgDmgAr;
	}

	public boolean isUrBillCancel() {
		return urBillCancel;
	}

	public void setUrBillCancel(boolean urBillCancel) {
		this.urBillCancel = urBillCancel;
	}

	public boolean isUrCnList() {
		return urCnList;
	}

	public void setUrCnList(boolean urCnList) {
		this.urCnList = urCnList;
	}

	public boolean isUrManualBill() {
		return urManualBill;
	}

	public void setUrManualBill(boolean urManualBill) {
		this.urManualBill = urManualBill;
	}

	public boolean isReIssue() {
		return reIssue;
	}

	public void setReIssue(boolean reIssue) {
		this.reIssue = reIssue;
	}

	public boolean isAllowAr() {
		return allowAr;
	}

	public void setAllowAr(boolean allowAr) {
		this.allowAr = allowAr;
	}

	public boolean isUrHoLhpv() {
		return urHoLhpv;
	}

	public void setUrHoLhpv(boolean urHoLhpv) {
		this.urHoLhpv = urHoLhpv;
	}

	public boolean isUrAckValidation() {
		return urAckValidation;
	}

	public void setUrAckValidation(boolean urAckValidation) {
		this.urAckValidation = urAckValidation;
	}

	public boolean isUrOldLhpvBal() {
		return urOldLhpvBal;
	}

	public void setUrOldLhpvBal(boolean urOldLhpvBal) {
		this.urOldLhpvBal = urOldLhpvBal;
	}

	public boolean isUrGenrateLbExcel() {
		return urGenrateLbExcel;
	}

	public void setUrGenrateLbExcel(boolean urGenrateLbExcel) {
		this.urGenrateLbExcel = urGenrateLbExcel;
	}

	public boolean isUrGenrateRelExcel() {
		return urGenrateRelExcel;
	}

	public void setUrGenrateRelExcel(boolean urGenrateRelExcel) {
		this.urGenrateRelExcel = urGenrateRelExcel;
	}

	public boolean isUrFundAllocation() {
		return urFundAllocation;
	}

	public void setUrFundAllocation(boolean urFundAllocation) {
		this.urFundAllocation = urFundAllocation;
	}

	public boolean isUrCosting() {
		return urCosting;
	}

	public void setUrCosting(boolean urCosting) {
		this.urCosting = urCosting;
	}

	public boolean isUrPetroExcel() {
		return urPetroExcel;
	}

	public void setUrPetroExcel(boolean urPetroExcel) {
		this.urPetroExcel = urPetroExcel;
	}

	public boolean isUrRentExcel() {
		return urRentExcel;
	}

	public void setUrRentExcel(boolean urRentExcel) {
		this.urRentExcel = urRentExcel;
	}

	public boolean isUrBillPrint() {
		return urBillPrint;
	}

	public void setUrBillPrint(boolean urBillPrint) {
		this.urBillPrint = urBillPrint;
	}

	public boolean isUrLhpvBalCash() {
		return urLhpvBalCash;
	}

	public void setUrLhpvBalCash(boolean urLhpvBalCash) {
		this.urLhpvBalCash = urLhpvBalCash;
	}

	public boolean isUrMisngStnry() {
		return urMisngStnry;
	}

	public void setUrMisngStnry(boolean urMisngStnry) {
		this.urMisngStnry = urMisngStnry;
	}

	public boolean isUrAccValid() {
		return urAccValid;
	}

	public void setUrAccValid(boolean urAccValid) {
		this.urAccValid = urAccValid;
	}

	public boolean isUrLhpvAlw() {
		return urLhpvAlw;
	}

	public void setUrLhpvAlw(boolean urLhpvAlw) {
		this.urLhpvAlw = urLhpvAlw;
	}
	
	
}
