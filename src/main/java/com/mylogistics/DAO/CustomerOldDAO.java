package com.mylogistics.DAO;

import com.mylogistics.model.CustomerOld;

public interface CustomerOldDAO {
	
	public int saveCustOld(CustomerOld customerOld);
}
