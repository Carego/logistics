'use strict';

var app = angular.module('application');

app.controller('DisplayRegularContractCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.regContCodeFlag = true;
	$scope.showContractDetails = true;
	$scope.isViewNo=false;
	$scope.selection=[];
	$scope.availableTags = [];
	$scope.regularContract={};
	$scope.regContByModal=false;
	$scope.regContByAuto=false;
	$scope.branchCodeFlag = true;
	$scope.blpmCodeFlag = true;
	$scope.CngrCodeFlag = true;
	$scope.CrNameFlag = true;
	$scope.FromStationFlag = true;
	$scope.ToStationFlag = true;
	$scope.rbkmStateCodeFlag = true;
	$scope.DetentionVehicleTypeFlag = true;
	$scope.DetentionToStationFlag = true;
	$scope.DetentionFromStationFlag = true;
	$scope.BonusVehicleTypeFlag = true;
	$scope.BonusToStationFlag = true;
	$scope.BonusFromStationFlag = true;
	$scope.PenaltyVehicleTypeFlag = true;
	$scope.PenaltyToStationFlag = true;
	$scope.PenaltyFromStationFlag = true;
	$scope.DetentionFlag = true;
	$scope.BonusFlag = true;
	$scope.PenaltyFlag = true;
	$scope.rbkmVehicleTypeFlag = true;
	$scope.rbkmToStationFlag = true;
	$scope.rbkmFromStationFlag = true;
	$scope.VehicleTypeFlag1 = true;
	$scope.ProductTypeFlag1 = true;
	$scope.RbkmIdFlag = true;
	$scope.ProductTypeFlag = true;
	$scope.VehicleTypeFlag = true;
	$scope.regContFlag = true;
	$scope.OldRegularContractFlag = true;
	$scope.oldregCont=true;
	
	$scope.contToStationFlagW = true;
	$scope.contToStationFlagQ = true;
	$scope.ToStationsDBFlag = true
	$scope.newCtsflagW=false;
	$scope.newCtsflagQ=false;
	$scope.AddProductTypeFlag=true;
	$scope.addStationFlag = true;
	$scope.StateCodeFlag = true;
	
	$scope.pbdD={};
	$scope.pbdB={};
	$scope.pbdP={};
	$scope.tnc={};
	$scope.newRbkm={};
	$scope.rbkm={};
	$scope.cts={};
	$scope.type=[];
	
	
	$(document).ready(function() {
	    $('#regContUnLoad').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContLoad').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContTransitDay').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContStatisticalCharge').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContAdditionalRate').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContFromWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContToWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContInsureUnitNo').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContInsurePolicyNo').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContValue').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#regContWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmFromKm').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmToKm').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayP').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayP').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtP').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayB').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayB').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtB').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayD').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayD').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtD').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtLoadLimit').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtGuaranteeWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    var max = 7;
	    
        $('#pbdAmtD').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            }
        });
        
        $('#pbdToDayD').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#pbdFromDayD').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#pbdAmtB').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            }
        });
        
        $('#pbdToDayB').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        
        $('#pbdFromDayB').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#pbdAmtP').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#pbdFromDayP').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            }
        });
        
        $('#pbdToDayP').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#rbkmFromKm').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#rbkmToKm').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#rbkmRate').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });

        $('#regContRate').keypress(function(e) { 
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContFromWt').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContToWt').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            }
        });
        
        $('#regContTransitDay').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            }
        });
        
        $('#regContStatisticalCharge').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContLoad').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContUnLoad').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
	    
        $('#regContAdditionalRate').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#regContValue').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#regContInsureUnitNo').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#regContInsuredBy').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#regContInsureComp').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#regContInsurePolicyNo').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#regContWt').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        
        $('#vtLoadLimit').keypress(function(e) {
            if (this.value.length == max) {
                e.preventDefault();
            } 
        });
        
        $('#vtGuaranteeWt').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
                e.preventDefault();
            } 
        });
        

        $('#vtVehicleType').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#vtServiceType').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
	    
        $('#vtCode').keypress(function(e) {
            if (this.value.length == 1) {
                e.preventDefault();
            } 
            
        });
	});
	
	$scope.OpenRegContCodeDB = function(){
		$scope.regContCodeFlag = false;
    	$('div#regContCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
			$('div#regContCodeDB').dialog('open');
		}
	
	$scope.OpenBranchCodeDB = function(){
		$scope.branchCodeFlag = false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui){ 
			     $(this).dialog('destroy');
			     $(this).hide();
			    }
			});
	
		$('div#branchCodeDB').dialog('open');
		}
	
	$scope.OpenregContBLPMCodeDB = function(){
		$scope.blpmCodeFlag = false;
		$('div#regContBLPMCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "BLPM Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});

		$('div#regContBLPMCodeDB').dialog('open');
		}
	
	$scope.OpenregContCngrCodeDB=function(){
		$scope.CngrCodeFlag = false;
		$('div#regContCngrCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Consignor Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContCngrCodeDB').dialog('open');
		}
	
	$scope.OpenregContCrNameDB = function(){
		$scope.CrNameFlag = false;
		$('div#regContCrNameDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Representative",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
	$('div#regContCrNameDB').dialog('open');
	}
	
	$scope.OpenregContFromStationDB = function(){
		$scope.FromStationFlag = false;
		$('div#regContFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContFromStationDB').dialog('open');
		}
	
	$scope.OpenregContToStationDB = function(){
		$scope.ToStationFlag = false;
		$('div#regContToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContToStationDB').dialog('open');
	}
	
	/*$scope.OpenregContVehicleTypeDB = function(){
		$scope.VehicleTypeFlag = false;
		$('div#regContVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContVehicleTypeDB').dialog('open');		
	}*/

	/*$scope.OpenregContProductTypeDB = function(){
		$scope.ProductTypeFlag = false;
		$('div#regContProductTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Product Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
	$('div#regContProductTypeDB').dialog('open');
}*/

	$scope.OpenregContRbkmIdDB = function(metricType){
		if(metricType==="" || metricType===null || angular.isUndefined(metricType)){
			$scope.alertToast("Please Enter Metric Type");
		}else{
			$scope.RbkmIdFlag = false;
			$('div#regContRbkmIdDB').dialog({
			autoOpen: false,
			modal:true,
			title: "RBKM ID",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContRbkmIdDB').dialog('open');
		}		
	}

	/*$scope.OpenregContProductTypeDB1 = function(){
		console.log("OpenregContProductTypeDB1");
		$scope.ProductTypeFlag1 = false;
		$('div#regContProductTypeDB1').dialog({
			autoOpen: false,
			modal:true,
			title: "Product Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#regContProductTypeDB1').dialog('open');
	}*/
	
	/*$scope.OpenregContVehicleTypeDB1 = function(){
		$scope.VehicleTypeFlag1 = false;
		$('div#regContVehicleTypeDB1').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#regContVehicleTypeDB1').dialog('open');
		}*/
	
	$scope.OpenrbkmFromStationDB = function(){
		$scope.rbkmFromStationFlag = false;
		$('div#rbkmFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});

			$('div#rbkmFromStationDB').dialog('open');
		}
	
	$scope.OpenrbkmToStationDB = function(){
		$scope.rbkmToStationFlag = false;
		$('div#rbkmToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmToStationDB').dialog('open');
		}
	
	$scope.OpenRbkmVehicleTypeDB = function(){
		$scope.rbkmVehicleTypeFlag = false;
		$('div#rbkmVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenPenaltyDB = function(){
		$scope.PenaltyFlag = false;
		$scope.pbdP.pbdPenBonDet = "P";
		
		$('div#PenaltyDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Penalty",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#PenaltyDB').dialog('open');
	}
	
	$scope.OpenBonusDB = function(){
		$scope.BonusFlag = false;
		$scope.pbdB.pbdPenBonDet = "B";
		
		$('div#BonusDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Bonus",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
			
		$('div#BonusDB').dialog('open');
		}
	
	$scope.OpenDetentionDB = function(){
		$scope.DetentionFlag = false;
		$scope.pbdD.pbdPenBonDet = "D";
		
		$('div#DetentionDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Detention",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#DetentionDB').dialog('open');
	}

	$scope.OpenPenaltyFromStationDB = function(){
		console.log("OpenPenaltyFromStationDB");
		$scope.PenaltyFromStationFlag = false;
		$('div#PenaltyFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyFromStationDB').dialog('open');
		}
		
	
	$scope.OpenPenaltyToStationDB = function(){
		
		$scope.PenaltyToStationFlag = false;
		$('div#PenaltyToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyToStationDB').dialog('open');
		}
	
	$scope.OpenPenaltyVehicleTypeDB = function(){
		$scope.PenaltyVehicleTypeFlag = false;
		$('div#PenaltyVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenBonusFromStationDB = function(){
		$scope.BonusFromStationFlag = false;
		$('div#BonusFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusFromStationDB').dialog('open');
		}
		
	
	$scope.OpenBonusToStationDB = function(){
		$scope.BonusToStationFlag = false;
		$('div#BonusToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusToStationDB').dialog('open');
		}
	
	$scope.OpenBonusVehicleTypeDB = function(){
		$scope.BonusVehicleTypeFlag = false;
		$('div#BonusVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenDetentionFromStationDB = function(){
		$scope.DetentionFromStationFlag = false;
		$('div#DetentionFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionFromStationDB').dialog('open');
		}
		
	
	$scope.OpenDetentionToStationDB = function(){
		$scope.DetentionToStationFlag = false;
		$('div#DetentionToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionToStationDB').dialog('open');
		}
	
	$scope.OpenDetentionVehicleTypeDB = function(){
		$scope.DetentionVehicleTypeFlag = false;
		$('div#DetentionVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenrbkmStateCodeDB = function(){
		$scope.rbkmStateCodeFlag = false;
		$('div#rbkmStateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "State Code",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmStateCodeDB').dialog('open');
		}
	
	$scope.openOldContractDB = function(){
		$scope.OldRegularContractFlag = false;
    	$('div#OldRegularContractDB').dialog({
			autoOpen: false,
			modal:true,
			//title: "Con Code",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
			$('div#OldRegularContractDB').dialog('open');
		}
	
	$scope.contToStnDB = function(regCnt,metricType){
		if(metricType==="" || angular.isUndefined(metricType)){
			$scope.alertToast("please Enter metric type ");	
			//$('input[name=dlyContType]').attr('checked',false);
		}
		else if(regCnt.regContType==='W'){
			$scope.contToStationFlagW = false;
			$scope.cts.ctsProductType="-100";
			$('div#contToStationW').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
				});
			
			$('div#contToStationW').dialog('open');
		}else if(regCnt.regContType==='Q'){
			$scope.contToStationFlagQ = false;
			$scope.cts.ctsProductType="";
			$('div#contToStationQ').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
				});
			
			$('div#contToStationQ').dialog('open');
		}
	}
	
	$scope.openToStationsDB = function(){	
		$scope.ToStationsDBFlag = false;
		$('div#ToStationsDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Stations",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#ToStationsDB').dialog('open');
		}
	
	$scope.openCtsVehicleType = function(){
		$scope.VehicleTypeFlag = false;
		$('div#ctsVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#ctsVehicleTypeDB').dialog('open');		
	}
	
	$scope.OpenProductTypeDB1 = function(){
		$scope.AddProductTypeFlag = false;
		$('div#AddProductTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Product Type",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#AddProductTypeDB').dialog('open');
		}
	
	$scope.openProductTypeDB = function(){
		console.log("open product type DB");
		$scope.ProductTypeFlag = false;
		$('div#productTypeDB').dialog({
		autoOpen: false,
		modal:true,
		resizable: false,
		position: UDPos,
		show: UDShow,
		hide: UDHide,
		title: "Product Type",
		draggable: true,
		close: function(event, ui) { 
	        $(this).dialog('destroy');
	        $(this).hide();
	    }
		});
		$('div#productTypeDB').dialog('open');
	}
	
	$scope.openAddNewStnDB = function(){	
		$scope.addStationFlag = false;
		$('div#addStation').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#addStation').dialog('open');
		}
	
	$scope.openStateCodeDB = function(){	
		$scope.StateCodeFlag = false;
		$('div#StateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State Code",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			$('div#StateCodeDB').dialog('open');
		}
	
	
	$scope.enableModalTextBox = function(){
		$('#regContByMId').removeAttr("disabled");
		 $('#regContByAId').attr("disabled","disabled");
		 $('#ok').attr("disabled","disabled");
		 $scope.regContByA="";
	}
	
	$scope.enableAutoTextBox = function(){
		$('#regContByAId').removeAttr("disabled");
		 $('#regContByMId').attr("disabled","disabled");
		 $('#ok').removeAttr("disabled");
		 $('#oldRegContId').attr("disabled","disabled");
		 $scope.regContByM="";
		 $scope.oldRegCont="";
		 $scope.oldregCont=true;
	}
	
	$scope.savBranchCode = function(branch){
		$scope.regCnt.branchCode = branch.branchCode;
		console.log("---<<>>"+$scope.regCnt.branchCode);
		$('div#branchCodeDB').dialog('destroy');
		$scope.branchCodeFlag = true;
	}
	
	$scope.savBLPMCode = function(blpmCode){
		$scope.regCnt.regContBLPMCode = blpmCode.custCode;
		$scope.regCnt.regContCrName="";
		$('div#regContBLPMCodeDB').dialog("destroy");
		$scope.blpmCodeFlag = true;	
		
		 var response = $http.post($scope.projectName+'/getCRDataForRegCont',blpmCode.custCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.crList = data.list;
			   $('#regContCrNameTemp').removeAttr("disabled");
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	}
	
	$scope.savCngrCode = function(cngrCode){
		$scope.regCnt.regContCngrCode = cngrCode.custCode;
		$('div#regContCngrCodeDB').dialog("destroy");
		$scope.CngrCodeFlag = true;
	}
	
	$scope.savCrName = function(cr){
		$scope.regCnt.regContCrNameTemp =cr.custRepName;
		$scope.regCnt.regContCrName = cr.custRepCode;
		$('div#regContCrNameDB').dialog("destroy");
		$scope.CrNameFlag = true;
	}
	
	$scope.savFrmStnCode = function(Fromstation){
		$scope.regCnt.regContFromStationTemp = Fromstation.stnName;
		$scope.regCnt.regContFromStation = Fromstation.stnCode;
		$('div#regContFromStationDB').dialog("destroy");
		$scope.FromStationFlag = true;
		
	}
	
	$scope.savToStnCode = function(Tostation){
		$scope.regCnt.regContToStationTemp = Tostation.stnName;
		$scope.regCnt.regContToStation =Tostation.stnCode;
		$('div#regContToStationDB').dialog("destroy");
		$scope.ToStationFlag = true;
	}
	
	$scope.savVehicleType = function(VT){
		$scope.regCnt.regContVehicleType = VT.vtCode;
		$('div#regContVehicleTypeDB').dialog("destroy");
		$scope.VehicleTypeFlag = true;
	}
	
	$scope.savProductName = function(pt){
		$scope.Qflag=true;
		$scope.ProductTypeFlag=true;
		$scope.regCnt.regContProductType = pt;
		$('div#regContProductTypeDB').dialog("destroy");
	}
	
	$scope.savRbkmFromStnCode = function(rbkmFromstation){
		$scope.rbkm.rbkmFromStationTemp = rbkmFromstation.stnName;
		$scope.rbkm.rbkmFromStation =rbkmFromstation.stnCode;;
		$('div#rbkmFromStationDB').dialog("destroy");
		$scope.rbkmFromStationFlag = true;
	}
	
	$scope.saveRbkmToStnCode = function(rbkmTostation){
		$scope.rbkm.rbkmToStationTemp = rbkmTostation.stnName;
		$scope.rbkm.rbkmToStation =rbkmTostation.stnCode;;
		$('div#rbkmToStationDB').dialog("destroy");
		$scope.rbkmToStationFlag = true;
	}
	
	$scope.savRbkmVehicleType = function(vType){
		$scope.rbkm.rbkmVehicleType = vType.vtCode;
		$('div#rbkmVehicleTypeDB').dialog("destroy");
		$scope.rbkmVehicleTypeFlag = true;
	}
	
	$scope.saveStateCode = function(state){
		$scope.rbkm.rbkmStateCodeTemp=state.stateName;
		$scope.rbkm.rbkmStateCode=state.stateCode;
		$('div#rbkmStateCodeDB').dialog("destroy");
		$scope.rbkmStateCodeFlag = true;
	}
	
	$scope.savPenaltyFromStn = function(PenaltyFromstation){
		$scope.pbdP.pbdFromStnTemp = PenaltyFromstation.stnName;
		$scope.pbdP.pbdFromStn =PenaltyFromstation.stnCode;;
		$('div#PenaltyFromStationDB').dialog("destroy");
		$scope.PenaltyFromStationFlag = true;
	}
	
	$scope.savPenaltyToStn = function(PenaltyTostation){
		$scope.pbdP.pbdToStnTemp = PenaltyTostation.stnName;
		$scope.pbdP.pbdToStn =PenaltyTostation.stnCode;;
		$('div#PenaltyToStationDB').dialog("destroy");
		$scope.PenaltyToStationFlag = true;
	}
	
	$scope.savPenaltyVehicleType = function(VtPenalty){
		$scope.pbdP.pbdVehicleType=VtPenalty.vtCode;
		$('div#PenaltyVehicleTypeDB').dialog("destroy");
		$scope.PenaltyVehicleTypeFlag = true;
	}
	
	
	$scope.savBonusFromStn = function(BonusFromstation){
		$scope.pbdB.pbdFromStnTemp = BonusFromstation.stnName;
		$scope.pbdB.pbdFromStn =BonusFromstation.stnCode;;
		$('div#BonusFromStationDB').dialog("destroy");
		$scope.BonusFromStationFlag = true;
	}
	
	$scope.savBonusToStn = function(BonusTostation){
		$scope.pbdB.pbdToStnTemp = BonusTostation.stnName;
		$scope.pbdB.pbdToStn =BonusTostation.stnCode;;
		$('div#BonusToStationDB').dialog("destroy");
		$scope.BonusToStationFlag = true;
	}
	
	$scope.savBonusVehicleType = function(vtBonus){
		$scope.pbdB.pbdVehicleType=vtBonus.vtCode;
		$('div#BonusVehicleTypeDB').dialog("destroy");
		$scope.BonusVehicleTypeFlag = true;
	}
	
	$scope.savDetentionFromStn = function(DetentionFromstation){
		$scope.pbdD.pbdFromStnTemp = DetentionFromstation.stnName;
		$scope.pbdD.pbdFromStn =DetentionFromstation.stnCode;;
		$('div#DetentionFromStationDB').dialog("destroy");
		$scope.DetentionFromStationFlag = true;
	}
	
	$scope.savDetentionToStn = function(DetentionTostation){
		$scope.pbdD.pbdToStnTemp = DetentionTostation.stnName;
		$scope.pbdD.pbdToStn =DetentionTostation.stnCode;;
		$('div#DetentionToStationDB').dialog("destroy");
		$scope.DetentionToStationFlag = true;
	}
	
	$scope.savDetentionVehicleType = function(vtDetention){
		$scope.pbdD.pbdVehicleType=vtDetention.vtCode;
		$('div#DetentionVehicleTypeDB').dialog("destroy");
		$scope.DetentionVehicleTypeFlag = true;
	}
	
	$scope.savStateCode = function(statecode){
		$scope.station.stateCode=statecode.stateCode;
		$('div#StateCodeDB').dialog("destroy");
		$scope.StateCodeFlag = true;
	}
	
	$scope.saveOldRC = function(oldRC){
		$scope.oldRegCont=oldRC.creationTS;
		console.log(oldRC.creationTS);
		$('div#OldRegularContractDB').dialog("destroy");
		$scope.OldRegularContractFlag = true;
		$scope.showContractDetails=true;
		$scope.oldregCont=false;
		
		for(var i=0;i<$scope.regCntOld.length;i++){
			if($scope.regCntOld[i].creationTS===oldRC.creationTS){
				console.log($scope.regCntOld[i].creationTS);
				$scope.oldCont= $scope.regCntOld[i];
			}
		}
	}
	
	$scope.setFflagTrue = function(regCnt){
		console.log(regCnt.regContAdditionalRate);
		if(regCnt.regContAdditionalRate===null || regCnt.regContAdditionalRate==="" || angular.isUndefined(regCnt.regContAdditionalRate)){
			$scope.Fflag=false;	
		}else{
			$scope.Fflag=true;
		}
	}
	
	$scope.setTransitFlagTrue = function(regCnt){
		if(regCnt.regContTransitDay ===null || regCnt.regContTransitDay==="" || angular.isUndefined(regCnt.regContTransitDay)){
			$scope.checkTransitFlag=false;
		}else{
			$scope.checkTransitFlag=true;
		}	
	}
	
	$scope.setSCFlagTrue = function(regCnt){
		if(regCnt.regContStatisticalCharge ===null || regCnt.regContStatisticalCharge==="" || angular.isUndefined(regCnt.regContStatisticalCharge)){
			$scope.checkStatisticalChargeFlag=false;
		}else{
			$scope.checkStatisticalChargeFlag=true;
		}
	}
	
	$scope.setLoadFlagTrue = function(regCnt){
		if(regCnt.regContLoad ===null || regCnt.regContLoad==="" || angular.isUndefined(regCnt.regContLoad)){
			$scope.checkLoadFlag=false;	
		}else{
			$scope.checkLoadFlag=true;
		}
	}
	
	$scope.setUnLoadFlagTrue = function(regCnt){
		if(regCnt.regContUnLoad ===null || regCnt.regContUnLoad==="" || angular.isUndefined(regCnt.regContUnLoad)){
			$scope.checkUnLoadFlag=false;	
		}else{
			$scope.checkUnLoadFlag=true;
		}
	}
	
	$scope.saveTncUnload = function(){
		if($scope.checkUnLoad){
			$scope.checkUnLoadFlag=false;
			$scope.tnc.tncUnLoading="yes";
		}
		if(!($scope.checkUnLoad)){
			$scope.checkUnLoadFlag=true;
			$scope.tnc.tncUnLoading="no";
			$scope.regCnt.regContUnLoad="";
		}	
	}

	$scope.saveTncLoad= function(){
		if($scope.checkLoad){
			$scope.checkLoadFlag=false;
			$scope.tnc.tncLoading="yes";
		}
		if(!($scope.checkLoad)){
			$scope.checkLoadFlag=true;
			$scope.regCnt.regContLoad="";
			$scope.tnc.tncLoading="no";
		}	
	}
	
	$scope.saveTncPenalty = function(){
		if($scope.checkPenalty){
			$scope.checkPenaltyFlag=false;
			$scope.tnc.tncPenalty="yes";
		}
		if(!($scope.checkPenalty)){
			$scope.checkPenaltyFlag=true;
			$scope.tnc.tncPenalty="no";
		}	
	}

	$scope.saveTncBonus = function(){
		if($scope.checkBonus){
			$scope.checkBonusFlag=false;
			$scope.tnc.tncBonus="yes";
		}
		if(!($scope.checkBonus)){
			$scope.checkBonusFlag=true;
			$scope.tnc.tncBonus="no";
		}	
	}

	$scope.saveTncDetention = function(){
		if($scope.checkDetention){
			$scope.checkDetentionFlag=false;
			$scope.tnc.tncDetention="yes";
		}
		if(!($scope.checkDetention)){
			$scope.checkDetentionFlag=true;
			$scope.tnc.tncDetention="no";
		}	
	}

	$scope.saveTncTransitDay = function(){
		if($scope.checkTransitDay){
			$scope.checkTransitFlag=false;
			$scope.tnc.tncTransitDay="yes";
		}
		if(!($scope.checkTransitDay)){
			$scope.checkTransitFlag=true;
			$scope.regCnt.regContTransitDay="";
			$scope.tnc.tncTransitDay="no";
		}	
	}	
	
	$scope.clickF = function(){
		$scope.Fflag=false;
		$('#regContAdditionalRate').removeAttr("disabled");
	}
	
	$scope.clickP = function(){
		$scope.Fflag=true;
		$scope.regCnt.regContAdditionalRate="";
		$('#regContAdditionalRate').attr("disabled","disabled");
	}

	$scope.saveTncStatisticalCharge = function(){
		if($scope.checkStatisticalCharge){
			$scope.checkStatisticalChargeFlag=false;
			$scope.tnc.tncStatisticalCharge="yes";
		}
		if(!($scope.checkStatisticalCharge)){
			$scope.checkStatisticalChargeFlag=true;
			$scope.regCnt.regContStatisticalCharge="";
			$scope.tnc.tncStatisticalCharge="no";
		}	
	}
	
	$scope.saveContCode = function(regContCodes){
		$scope.oldRegCont="";
		$scope.regContByM=regContCodes;
		$scope.contractCode=regContCodes;
		$('div#regContCodeDB').dialog('close');
		$scope.showContractDetails = false;
		$scope.isViewNo=true;
		$scope.oldregCont=true;
		
		var response = $http.post($scope.projectName+'/regularcontractdetails',regContCodes);
		response.success(function(data, status, headers, config) {
			$scope.checkPenalty=false;
			$scope.checkBonus=false;
			$scope.checkDetention=false;
			
			$('#oldRegContId').removeAttr("disabled");
			$scope.regCnt = data.regularContract;
			$scope.regCntOld=data.regularContractOld;
			for(var i=0;i<$scope.regCntOld.length;i++){
				$scope.regCntOld[i].creationTS =  $filter('date')($scope.regCntOld[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
			}
			
			/*$scope.regContRatePer="Per Kg";
			$scope.fromWt="Ton";
			$scope.toWt="Ton";
			$scope.weight="Ton";
			$scope.regCnt.regContFromWt=($scope.regCnt.regContFromWt/$scope.kgInTon);
			$scope.regCnt.regContToWt=($scope.regCnt.regContToWt/$scope.kgInTon);
			$scope.regCnt.regContWt=($scope.regCnt.regContWt/$scope.kgInTon);*/
			
			if($scope.regCnt.regContLoad===0){
				$scope.checkLoad=false;
			}else{
				$scope.checkLoad=true;
			}
			
			if($scope.regCnt.regContUnLoad===0){
				$scope.checkUnLoad=false;
			}else{
				$scope.checkUnLoad=true;
			}
			
			if($scope.regCnt.regContTransitDay===0){
				$scope.checkTransitDay=false;
			}else{
				$scope.checkTransitDay=true;
			}
			
			if($scope.regCnt.regContStatisticalCharge===0){
				$scope.checkStatisticalCharge=false;
			}else{
				$scope.checkStatisticalCharge=true;
			}
			
			if($scope.regCnt.regContType==="K"){
				console.log($scope.regCnt.regContType);
				$('#regContRbkmId').removeAttr("disabled");
//				$('#regContProductType').attr("disabled","disabled");
//				 $('#openProductTypeDB').attr("disabled","disabled");
				$scope.onWQClick=false;
				$scope.onKClick=true;
			}else if($scope.regCnt.regContType==="Q"){
				console.log($scope.regCnt.regContType);
//				$('#regContProductType').removeAttr("disabled");
//				 $('#openProductTypeDB').removeAttr("disabled");
				$scope.onWQClick=true;
				$scope.onKClick=false;
				 $('#regContRbkmId').attr("disabled","disabled");
			}else {
				console.log($scope.regCnt.regContType);
				 $('#regContRbkmId').attr("disabled","disabled");
				 $scope.onWQClick=true;
				 $scope.onKClick=false;
				 /*$('#regContProductType').attr("disabled","disabled");
				 $('#openProductTypeDB').attr("disabled","disabled");*/
			}
			
			/*if($scope.regCnt.regContProportionate==="F"){
				console.log($scope.regCnt.regContProportionate);
				$('#regContAdditionalRate').removeAttr("disabled");
			}else if($scope.regCnt.regContProportionate==="P"){
				console.log($scope.regCnt.regContProportionate);
				 $('#regContAdditionalRate').attr("disabled","disabled");
			}*/
			 $scope.getRateByKmData();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.getRateByKmData = function(){
		 console.log("Enter into getRateByKmData function"+$scope.contractCode);
		   var response = $http.post($scope.projectName+'/getRateByKmData',$scope.contractCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.rbkmList = data.list;
			   console.log($scope.rbkmList[0].rbkmStartDate);
			   console.log($scope.rbkmList[0].rbkmEndDate);
			 	$scope.getPBDData();
			 	$scope.rbkmFlag=true;
			   }else{
				console.log(data);  
				$scope.getPBDData();
				$scope.rbkmFlag=false;
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	 $scope.getPBDData= function(){
		 console.log("Enter into getPBDData function"+$scope.contractCode);
		   var response = $http.post($scope.projectName+'/getPBDData',$scope.contractCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				   console.log(data.result);
			   $scope.pbdList = data.list;
			   $scope.pbdFlag=true; 
			   for(var i=0;i<$scope.pbdList.length;i++){
				   console.log($scope.pbdList[i].pbdPenBonDet);
				   if($scope.pbdList[i].pbdPenBonDet==="P"){
					   $scope.checkPenalty=true;
				   }else if($scope.pbdList[i].pbdPenBonDet==="B"){
					   $scope.checkBonus=true;
				   }else if($scope.pbdList[i].pbdPenBonDet==="D"){
					   $scope.checkDetention=true;
				   }  
				   }
			  // $scope.getBranchData();
			   $scope.getCTSData();
			   }else{
				console.log(data); 
				$scope.pbdFlag=false; 
				$scope.checkPenalty=false;
				$scope.checkBonus=false;
				$scope.checkDetention=false;
				//$scope.getBranchData();
				$scope.getCTSData();
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 
	 $scope.getCTSData = function(){
		 console.log("Enter into getCTSData function"+$scope.contractCode);
		   var response = $http.post($scope.projectName+'/getCTSDataForRC',$scope.contractCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.oldCtsList = data.list;
				
			  console.log($scope.oldCtsList.length);
				   if($scope.regCnt.regContType==="Q"){
						console.log($scope.regCnt.regContType);
						$scope.ctsFlagQ=true;
					}else if($scope.regCnt.regContType==="W") {
						console.log($scope.regCnt.regContType);
						$scope.ctsFlagW=true
					}
				   
			   $scope.getBranchData();
			   }else{
				console.log(data);  
				$scope.ctsFlagQ=false;
				$scope.ctsFlagW=false;
				$scope.getBranchData();
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	$scope.getBranchData = function(){
		   console.log("getBranchData------>");
		   var response = $http.post($scope.projectName+'/getBranchDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.branchList = data.list;
			   $scope.getCustomerData();
			   }else{
				   console.log(data.result);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
	$scope.getCustomerData = function(){
		   var response = $http.post($scope.projectName+'/getCustomerDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.customerList = data.list;
			   $scope.getStationData();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	 $scope.getStationData = function(){
		   var response = $http.post($scope.projectName+'/getStationDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.stationList = data.list;
			   $scope.getProductName();
			   for(var i=0;i<$scope.stationList.length;i++){
					$scope.type[i] = $scope.stationList[i].stnName+$scope.stationList[i].stnPin;
					$scope.type.push($scope.stationList[i].stnName+$scope.stationList[i].stnPin);
			   }
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getProductName = function(){
		   var response = $http.post($scope.projectName+'/getProductNameForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.ptList = data.list;
			   $scope.getVehicleTypeCode();
			   }else{
				   console.log(data);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	 
	 $scope.getVehicleTypeCode = function(){
		   var response = $http.post($scope.projectName+'/getVehicleTypeCodeForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.vtList = data.list;
			   $scope.getStateData();
			   }else{
				console.log(data);   
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	  $scope.getStateData = function(){
		   var response = $http.post($scope.projectName+'/getStateDataForRegCont');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.listState = data.list;	  
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	  $scope.removeRbkm = function(rbkm) {
			 console.log("entr into removeRbkm function");
				var response = $http.post($scope.projectName+'/deleteRbkmForRC',rbkm);
				response.success(function(data, status, headers, config) {
					$scope.getRateByKmData();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		    }
	  
	  $scope.removeNewRbkm = function(rbkm) {
			 console.log("entr into removeRbkm function");
				var response = $http.post($scope.projectName+'/removeNewRbkmForRC',rbkm);
				response.success(function(data, status, headers, config) {
					$scope.fetchRbkmList();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		    }
	  
	  $scope.removeAllRbkm = function() {
			var response = $http.post($scope.projectName+'/removeAllRbkmForEditDC');
			response.success(function(data, status, headers, config) {
				$scope.fetchRbkmList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.removeAllPbd = function() {
			var response = $http.post($scope.projectName+'/removeAllPbdForEditDC');
			response.success(function(data, status, headers, config) {
				$scope.fetchPbdList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.removePbd = function(pbd) {
	    	console.log("entr into removePbd function");
			var response = $http.post($scope.projectName+'/deletePbdForRC',pbd);
			response.success(function(data, status, headers, config) {
				 $scope.getPBDData();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.removeNewPbd = function(pbd) {
			var response = $http.post($scope.projectName+'/removeNewPbdForRC',pbd);
			response.success(function(data, status, headers, config) {
				$scope.fetchPbdList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.fetchPbdList = function(){
			 var response = $http.get($scope.projectName+'/fetchPbdListForEditRC');
			 response.success(function(data, status, headers, config){
				 if(data.result==="success"){
					 $scope.getRegContCodesList ();
				 		$scope.newPbdList = data.list;
				 		console.log(data);
				 		$scope.newPbdFlag=true;
				 }else{
					 console.log(data.result);
					 $scope.newPbdFlag=false;
					 $scope.checkPenalty=false;
					 $scope.checkBonus=false;
					 $scope.checkDetention=false;
					 $scope.getRegContCodesList ();
				 }
				 		
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	    
	    $scope.fetchRbkmList = function(){
			 var response = $http.get($scope.projectName+'/fetchRbkmListForEditRC');
			 response.success(function(data, status, headers, config){
				 if(data.result==="success"){
					 $scope.rateByKmList = data.list;
					 $scope.newRbkmFlag=true;
				 }else{
					 console.log(data.result);
					 $scope.newRbkmFlag=false;
				 }
				 
	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	  
	  
	
	$scope.getContractList = function(){
		$scope.code = $("#regContByAId").val();
		if($scope.code===""){
			$scope.alertToast("Please enter code");
		}
		else if($scope.code.substring(0,3)==="reg"){
			$scope.showContractDetails = false;
			$scope.isViewNo=true;
			$scope.contractCode=$scope.code;
			
			var response = $http.post($scope.projectName+'/regularcontractdetails',$scope.code);
			response.success(function(data, status, headers, config) {	
				$scope.regCnt = data.regularContract;
				$scope.checkPenalty=false;
				$scope.checkBonus=false;
				$scope.checkDetention=false;
				
				$scope.regContRatePer="Per Kg";
				$scope.fromWt="Ton";
				$scope.toWt="Ton";
				$scope.weight="Ton";
				$scope.regCnt.regContFromWt=($scope.regCnt.regContFromWt/$scope.kgInTon);
				$scope.regCnt.regContToWt=($scope.regCnt.regContToWt/$scope.kgInTon);
				$scope.regCnt.regContWt=($scope.regCnt.regContWt/$scope.kgInTon);
				
				if($scope.regCnt.regContLoad===0){
					$scope.checkLoad=false;
				}else{
					$scope.checkLoad=true;
				}
				
				if($scope.regCnt.regContUnLoad===0){
					$scope.checkUnLoad=false;
				}else{
					$scope.checkUnLoad=true;
				}
				
				if($scope.regCnt.regContTransitDay===0){
					$scope.checkTransitDay=false;
				}else{
					$scope.checkTransitDay=true;
				}
				
				if($scope.regCnt.regContStatisticalCharge===0){
					$scope.checkStatisticalCharge=false;
				}else{
					$scope.checkStatisticalCharge=true;
				}
				
				if($scope.regCnt.regContType==="K"){
					console.log($scope.regCnt.regContType);
					$('#regContRbkmId').removeAttr("disabled");
					$('#regContProductType').attr("disabled","disabled");
					 $('#openProductTypeDB').attr("disabled","disabled");
				}else if($scope.regCnt.regContType==="Q"){
					console.log($scope.regCnt.regContType);
					$('#regContProductType').removeAttr("disabled");
					 $('#openProductTypeDB').removeAttr("disabled");
					 $('#regContRbkmId').attr("disabled","disabled");
				}else {
					console.log($scope.regCnt.regContType);
					 $('#regContRbkmId').attr("disabled","disabled");
					 $('#regContProductType').attr("disabled","disabled");
					 $('#openProductTypeDB').attr("disabled","disabled");
				}
				
				if($scope.regCnt.regContProportionate==="F"){
					console.log($scope.regCnt.regContProportionate);
					$('#regContAdditionalRate').removeAttr("disabled");
				}else if($scope.regCnt.regContProportionate==="P"){
					console.log($scope.regCnt.regContProportionate);
					 $('#regContAdditionalRate').attr("disabled","disabled");
				}
				 $scope.getRateByKmData();
				
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});	
		}else{
			$scope.alertToast("Enter correct code");
		}
	}
	
	$scope.getContCodeList=function(){
	      $( "#regContByAId" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	}
	
	$scope.getRegularContCode = function(){
		var response = $http.get($scope.projectName+'/getRegularContCode');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			$scope.getRegContIsViewNo();
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getRegContIsViewNo = function(){
		   var response = $http.post($scope.projectName+'/getRegContIsViewNo');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.successToast(data.result);
			   $scope.ContractList = data.list;
			   for(var i=0;i<$scope.ContractList.length;i++){
			   $scope.ContractList[i].creationTS = $filter('date')($scope.ContractList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');	   
			   }
			   }else{
				   $scope.isViewNo=true;
				   $scope.successToast(data.result); 
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	$scope.toggleSelection = function toggleSelection(dlyContCode) {
		   var idx = $scope.selection.indexOf(dlyContCode);
		   
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
			   }
		   else {
			   $scope.selection.push(dlyContCode);
		   }
	   }	 
	
	$scope.verifyContracts = function(){
		 console.log("enter into update with $scope.selection = "+$scope.selection);
		 if(!$scope.selection.length){
			 $scope.alertToast("You have not selected anything");
		 }
		 else{
		 var response = $http.post($scope.projectName+'/updateIsViewRegularContract',$scope.selection.toString());
		 response.success(function(data, status, headers, config){
			   console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
				$scope.getRegContIsViewNo();
	    });	 
		 response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	}
	
	$scope.backToList = function(){
		 $scope.showContractDetails = true;
		 $scope.isViewNo=false;
		 $scope.regContByM="";
		 $scope.regContByA="";
		 $scope.regContByModal=false;
		 $scope.regContByAuto=false;
		 $('#ok').attr("disabled","disabled");
		 $('#regContByAId').attr("disabled","disabled");
		 $('#regContByMId').attr("disabled","disabled");
		 $('#oldRegContId').attr("disabled","disabled");
		 $scope.oldRegCont="";
		 $scope.getRegContIsViewNo();
	 }
	
	
	 $scope.EditRegularContractSubmit = function(EditRegularContractForm,regCnt,metricType,tnc,pbdP,pbdB,pbdD){
		 console.log("Enter into submit edit function-----"+metricType);
		 
		 	if(regCnt.regContStartDt>regCnt.regContEndDt){
				$scope.alertToast("From date cannot be greater than to date");
		 	}else if(regCnt.regContFromWt>regCnt.regContToWt){
				$scope.alertToast("From weight cannot be greater than to weight");
			}else if($scope.checkPenaltyFlag === false){
				if(pbdP.pbdFromStnTemp === null || pbdP.pbdFromStnTemp === "" || angular.isUndefined(pbdP.pbdFromStnTemp) || pbdP.pbdToStnTemp === null || pbdP.pbdToStnTemp === "" || angular.isUndefined(pbdP.pbdToStnTemp) || pbdP.pbdStartDt === null || pbdP.pbdStartDt === "" || angular.isUndefined(pbdP.pbdStartDt) ||
						pbdP.pbdEndDt===null || pbdP.pbdEndDt==="" ||angular.isUndefined(pbdP.pbdEndDt) || pbd.pbdFromDay===null || pbdP.pbdFromDay==="" || angular.isUndefined(pbdP.pbdFromDay) || pbdP.pbdToDay===null || pbdP.pbdToDay==="" || angular.isUndefined(pbdP.pbdToDay) || pbdP.pbdVehicleType===null || pbdP.pbdVehicleType==="" || angular.isUndefined(pbdP.pbdVehicleType) ||
						pbdP.pbdHourNDay===null || pbdP.pbdHourNDay==="" || angular.isUndefined(pbdP.pbdHourNDay) || pbdP.pbdAmt===null || pbdP.pbdAmt==="" || angular.isUndefined(pbdP.pbdAmt))
						{
							$scope.alertToast("Please fill Penalty");
						}
					}else if($scope.checkBonusFlag === false){
						if(pbdB.pbdFromStnTemp===null || pbdB.pbdFromStnTemp==="" || angular.isUndefined(pbdB.pbdFromStnTemp) || pbdB.pbdToStnTemp===null || pbdB.pbdToStnTemp==="" || angular.isUndefined(pbdB.pbdToStnTemp) || pbdB.pbdStartDt===null || pbdB.pbdStartDt==="" || angular.isUndefined(pbdB.pbdStartDt)
						|| pbdB.pbdEndDt===null || pbdB.pbdEndDt==="" ||angular.isUndefined(pbdB.pbdEndDt) || pbdB.pbdFromDay===null || pbdB.pbdFromDay==="" || angular.isUndefined(pbdB.pbdFromDay) || pbdB.pbdToDay===null || pbdB.pbdToDay==="" || angular.isUndefined(pbdB.pbdToDay) || pbdB.pbdVehicleType===null || pbdB.pbdVehicleType==="" || angular.isUndefined(pbdB.pbdVehicleType)
						|| pbdB.pbdHourNDay===null || pbdB.pbdHourNDay==="" || angular.isUndefined(pbdB.pbdHourNDay) || pbdB.pbdAmt===null || pbdB.pbdAmt==="" || angular.isUndefined(pbdB.pbdAmt))
						{
							$scope.alertToast("Please fill Bonus");
						}
					}else if($scope.checkDetentionFlag === false){
						if(pbdD.pbdFromStnTemp===null || pbdD.pbdFromStnTemp==="" || angular.isUndefined(pbdD.pbdFromStnTemp) || pbdD.pbdToStnTemp===null || pbdD.pbdToStnTemp==="" || angular.isUndefined(pbdD.pbdToStnTemp) || pbdD.pbdStartDt===null || pbdD.pbdStartDt==="" || angular.isUndefined(pbdD.pbdStartDt)
						|| pbdD.pbdEndDt===null || pbdD.pbdEndDt==="" ||angular.isUndefined(pbdD.pbdEndDt) || pbdD.pbdFromDay===null || pbdD.pbdFromDay==="" || angular.isUndefined(pbdD.pbdFromDay) || pbdD.pbdToDay===null || pbdD.pbdToDay==="" || angular.isUndefined(pbdD.pbdToDay) || pbdD.pbdVehicleType===null || pbdD.pbdVehicleType==="" || angular.isUndefined(pbdD.pbdVehicleType)
						|| pbdD.pbdHourNDay===null || pbdD.pbdHourNDay==="" || angular.isUndefined(pbdD) || pbdD.pbdAmt===null || pbdD.pbdAmt==="" || angular.isUndefined(pbdD.pbdAmt))
						{
							$scope.alertToast("Please fill Detention");
						}
					}else if($scope.checkLoadFlag === false){
						
						if(regCnt.regContLoad===0 || regCnt.regContLoad === null || regCnt.regContLoad==="" || angular.isUndefined(regCnt.regContLoad))
						{
							$scope.alertToast("Please fill Load");
						}
					} else if($scope.checkStatisticalChargeFlag === false){
						
						if(regCnt.regContStatisticalCharge === 0 || regCnt.regContStatisticalCharge === null || regCnt.regContStatisticalCharge==="" || angular.isUndefined(regCnt.regContStatisticalCharge))
						{
							$scope.alertToast("Please fill StatisticalCharge");
						}
					}else if($scope.checkUnLoadFlag === false){
						if(regCnt.regContUnLoad===0 || regCnt.regContUnLoad === null || regCnt.regContUnLoad==="" || angular.isUndefined(regCnt.regContUnLoad))
						{
							$scope.alertToast("Please fill UnLoad");
						}
					}else if($scope.checkTransitFlag === false){
						if(regCnt.regContTransitDay===0 || regCnt.regContTransitDay === null || regCnt.regContTransitDay === "" || angular.isUndefined(regCnt.regContTransitDay))
						{
							$scope.alertToast("Please fill Transit Day");
						}
					}else if($scope.Fflag===false){
						console.log($scope.Fflag);
						if(regCnt.regContAdditionalRate===0 || regCnt.regContAdditionalRate===null || regCnt.regContAdditionalRate==="" || angular.isUndefined(regCnt.regContAdditionalRate))
						{
							$scope.alertToast("please fill Additional Rate");	
						}
					}else {
						
						$scope.regContFlag = false;
				    	$('div#regContDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						    }
							});
						$('div#regContDB').dialog('open');
			}
		}
	
	 $scope.back= function(){
			$scope.regContFlag = true;
			$('div#regContDB').dialog('close');
		}
	 
	 
	 $scope.saveContract = function(regCnt,tnc,metricType){
		 console.log("Enter into saveContractFunction"+metricType);
		 
		 if(metricType==="Ton"){
				regCnt.regContWt=(regCnt.regContWt*$scope.kgInTon);
				regCnt.regContWt=parseFloat(regCnt.regContWt.toFixed(2));
				console.log(regCnt.regContWt);
				regCnt.regContAdditionalRate=(regCnt.regContAdditionalRate/$scope.kgInTon);
				regCnt.regContAdditionalRate=parseFloat(regCnt.regContAdditionalRate.toFixed(2));
				console.log(regCnt.regContAdditionalRate);
			}
		 
		 var response = $http.post($scope.projectName+'/EditRegularContractSubmit',regCnt);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.successToast(data.result);
					$('div#regContDB').dialog('close');
					$scope.regContByM="";
					$scope.regContByA="";	
					if($scope.checkBonus){
						console.log($scope.checkBonus);
						tnc.tncBonus="yes";
					}
					if($scope.checkPenalty){
						console.log($scope.checkPenalty);
						tnc.tncPenalty="yes";
					}
					if($scope.checkDetention){
						console.log($scope.checkDetention);
						tnc.tncDetention="yes";
					}
					if($scope.checkLoad){
						console.log($scope.checkLoad);
						tnc.tncLoading="yes";
					}
					if($scope.checkUnload){
						console.log($scope.checkUnload);
						tnc.tncUnLoading="yes";
					}
					if($scope.checkTransitDay){
						console.log($scope.checkTransitDay);
						tnc.tncTransitDay="yes";
					}
					if($scope.checkStatisticalCharge){
						console.log($scope.checkStatisticalCharge);
						tnc.tncStatisticalCharge="yes";
					}
					var response = $http.post($scope.projectName+'/saveUpdatedTncForRC',tnc);
					response.success(function(data, status, headers, config) {
						$scope.successToast(data.resultTnc);
						$scope.showContractDetails=true;
						$scope.regContFlag = true;
						
						 $scope.isViewNo=false;
						 $scope.regContByM="";
						 $scope.regContByA="";
						 $scope.regContByModal=false;
						 $scope.regContByAuto=false;
						 $('input[name=contCodesName]').attr('checked',false);
						 $('#ok').attr("disabled","disabled");
						 $('#regContByAId').attr("disabled","disabled");
						 $('#regContByMId').attr("disabled","disabled");
						 $('#oldRegContId').attr("disabled","disabled");
						 $scope.getRegContIsViewNo();
						 $scope.fetch();
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.resultTnc);
					});	
				}else{
					console.log(data.result);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		 
	 }
	 
	 
	 
	 $scope.getRegContCodesList = function(){
			console.log("Enetr into $scope.getRegContCodesList function");
			var response = $http.post($scope.projectName+'/getRegContCodesList');
			response.success(function(data, status, headers, config) {
				$scope.alertToast(data.result);
				$scope.regContCodeList = data.list;
				$scope.getRegularContCode();
				console.log("---"+$scope.regContCodeList);
		    });
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	 //$scope.getRegContCodesList();
	 
	 $scope.savePbdForPenalty = function(PenaltyForm,pbdP){
			$('div#PenaltyDB').dialog("destroy");
			$scope.checkPenaltyFlag=true;
			$scope.PenaltyFlag=true;
			if(PenaltyForm.$invalid){
				if(PenaltyForm.pbdFromStnTempP.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(PenaltyForm.pbdToStnTempP.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(PenaltyForm.pbdVehicleTypeP.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(PenaltyForm.pbdToDayP.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(PenaltyForm.pbdFromDayP.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(PenaltyForm.pbdHourNDayP.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(PenaltyForm.pbdAmtP.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
			}else{
				if($scope.pbdP.pbdFromDay>$scope.pbdP.pbdToDay){
					$scope.alertToast("From dlay cannot be greater than to dlay");
			    }else if($scope.pbdP.pbdStartDt>$scope.pbdP.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
				 }else {
					var response = $http.post($scope.projectName+'/addNewPbdForRC',pbdP);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							console.log("from server--<<<"+data.result);
							$scope.fetchPbdList();
							$scope.pbdP.pbdFromStnTemp = "";
							$scope.pbdP.pbdToStnTemp = "";
							$scope.pbdP.pbdStartDt = "";
							$scope.pbdP.pbdEndDt = "";
							$scope.pbdP.pbdFromDay = "";
							$scope.pbdP.pbdToDay = "";
							$scope.pbdP.pbdVehicleType= "";
							$scope.pbdP.pbdHourNDay = "";
							$scope.pbdP.pbdAmt = "";
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
	 
	 $scope.savePbdForBonus = function(BonusForm,pbdB){
			$('div#BonusDB').dialog("destroy");
			$scope.checkBonusFlag=true;
			$scope.BonusFlag=true;
			if(BonusForm.$invalid){
				if(BonusForm.pbdFromStnTempB.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(BonusForm.pbdToStnTempB.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(BonusForm.pbdVehicleTypeB.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(BonusForm.pbdToDayB.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(BonusForm.pbdFromDayB.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(BonusForm.pbdHourNDayB.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(BonusForm.pbdAmtB.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
			}else{
				if($scope.pbdB.pbdFromDay>$scope.pbdB.pbdToDay){
					$scope.alertToast("From dlay cant be greater than to dlay");
			     }else if($scope.pbdB.pbdStartDt>$scope.pbdB.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
			     }else{
					var response = $http.post($scope.projectName+'/addNewPbdForRC',pbdB);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
						console.log("from server--<<<"+data.result);
						$scope.fetchPbdList();
						$scope.pbdB.pbdFromStnTemp = "";
						$scope.pbdB.pbdToStnTemp = "";
						$scope.pbdB.pbdStartDt = "";
						$scope.pbdB.pbdEndDt = "";
						$scope.pbdB.pbdFromDay = "";
						$scope.pbdB.pbdToDay = "";
						$scope.pbdB.pbdVehicleType = "";
						$scope.pbdB.pbdHourNDay = "";
						$scope.pbdB.pbdAmt = "";
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
		
		$scope.savePbdForDetention = function(DetentionForm,pbdD){
			$('div#DetentionDB').dialog("destroy");
			$scope.checkDetentionFlag=true;
			$scope.DetentionFlag=true;
			if(DetentionForm.$invalid){
				if(DetentionForm.pbdFromStnTempD.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(DetentionForm.pbdToStnTempD.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(DetentionForm.pbdVehicleTypeD.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(DetentionForm.pbdToDayD.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(DetentionForm.pbdFromDayD.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(DetentionForm.pbdHourNDayD.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(DetentionForm.pbdAmtD.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
				}else{
				 if($scope.pbdD.pbdFromDay>$scope.pbdD.pbdToDay){
					$scope.alertToast("From dlay cannot be greater than to dlay");
			     } else if($scope.pbdD.pbdStartDt>$scope.pbdD.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
				    }else{
					var response = $http.post($scope.projectName+'/addNewPbdForRC', pbdD);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
						console.log("from server--<<<"+data.result);
						$scope.fetchPbdList();
						$scope.pbdD.pbdFromStnTemp= "";
						$scope.pbdD.pbdToStnTemp = "";
						$scope.pbdD.pbdStartDt = "";
						$scope.pbdD.pbdEndDt = "";
						$scope.pbdD.pbdFromDay = "";
						$scope.pbdD.pbdToDay = "";
						$scope.pbdD.pbdVehicleType = "";
						$scope.pbdD.pbdHourNDay = "";
						$scope.pbdD.pbdAmt = "";
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
	 
		$scope.saveRbkmId = function(RbkmForm,rbkm){
			$('div#regContRbkmIdDB').dialog("destroy");
			$scope.RbkmIdFlag = true;
			$scope.Kflag=true;
			if(RbkmForm.$invalid){
				console.log("RbkmForm.$invalid"+RbkmForm.$invalid);
				
				if(RbkmForm.rbkmFromStationTemp.$invalid){
					$scope.alertToast("please Enter From station");
				}else if(RbkmForm.rbkmToStationTemp.$invalid){
					$scope.alertToast("please Enter To station");
				}else if(RbkmForm.rbkmStateCodeTemp.$invalid){
					$scope.alertToast("please Enter State code");
				}else if(RbkmForm.rbkmToKm.$invalid){
					$scope.alertToast("to km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmFromKm.$invalid){
					$scope.alertToast("from km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmVehicleType.$invalid){
					$scope.alertToast("please Enter Vehicle Type");
				}else if(RbkmForm.rbkmRate.$invalid){
					$scope.alertToast("please Enter correct rbkmrate Type");
				}
			}else{
			   if($scope.newRbkm.rbkmStartDate>$scope.newRbkm.rbkmEndDate){
			    	$scope.alertToast("Start date cannot be greater than end date");
			   }else if($scope.newRbkm.rbkmFromKm>$scope.newRbkm.rbkmToKm){
			    	$scope.alertToast("from km cannot be greater than to km");
			   } else{
				   var response = $http.post($scope.projectName+'/addNewRbkmForRC',rbkm);
			   
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					console.log("from server ---->"+data.result);
					$scope.fetchRbkmList();
					$scope.rbkm.rbkmFromStationTemp="";
					$scope.rbkm.rbkmToStationTemp="";
					$scope.rbkm.rbkmStateCodeTemp="";
					$scope.rbkm.rbkmStartDate="";
					$scope.rbkm.rbkmEndDate="";
					$scope.rbkm.rbkmFromKm="";
					$scope.rbkm.rbkmToKm="";
					$scope.rbkm.rbkmVehicleType="";
					$scope.rbkm.rbkmRate="";
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}	
}		
		
	    $scope.savevehicletype = function(VehicleForm,vt){
			$scope.VehicleTypeFlag1=true;
			console.log("enter into saveVehicleType function--->");
			$('div#regContVehicleTypeDB1').dialog("destroy");
			vt.vtServiceType=vt.vtServiceType.toUpperCase();
			vt.vtVehicleType=vt.vtVehicleType.toUpperCase();
			$scope.code = vt.vtServiceType+vt.vtVehicleType;
			if($scope.vtList){
				for(var i=0;i<$scope.vtList.length;i++){
					$scope.type[i] = $scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType;
					$scope.type.push($scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType);
				}
				if(VehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(VehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(VehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(VehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleType',vt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getVehicleTypeCode();
						$scope.vt="";
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}else{
				if(VehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(VehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(VehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(VehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleType',vt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getVehicleTypeCode();
						$scope.vt="";	
					}else{
						$scope.errorToast(data.result);	
					}			
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}			
	    }
	    
	    $scope.saveproducttype = function(ProductTypeDB1Form,pt){
			$('div#regContProductTypeDB1').dialog("destroy");
			$scope.ProductTypeFlag1=true;
			$scope.ProductTypeFlag=true;
			$scope.Qflag=true;
			pt.ptName=pt.ptName.toUpperCase();
			console.log("pt==========="+pt.ptName);
			if($.inArray(pt.ptName,$scope.ptList)!== -1){
				$scope.alertToast("Name already exists");
				$scope.pt.ptName="";
			}else{
				var response = $http.post($scope.projectName+'/saveproducttype',pt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getProductName();
						$scope.pt.ptName="";
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}	
		}
	 
	    $scope.saveContToStnW = function(ContToStationFormW,cts){
			console.log("Enter into saveContToStnW function"+ContToStationFormW.$invalid);
			$('div#contToStationW').dialog("destroy");
			$scope.contToStationFlagW = true;
			$scope.ctsflagW=true;
			
			if($scope.ctsList.length>0){
				console.log("if list exists"+$scope.ctsList.length);
				if(ContToStationFormW.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormW.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				} else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else {
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					$scope.ctsTemp= cts.ctsToStn+cts.ctsVehicleType;
					
					for(var i=0;i<$scope.ctsList.length;i++){
						console.log($scope.ctsList[i].ctsToStn);
						$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType;
						$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType);
					}
					if($.inArray($scope.ctsTemp,$scope.type)!==-1){
						$scope.alertToast("Already exists");
						cts.ctsToStn="";
						cts.ctsToStnTemp="";
						cts.ctsRate="";
						cts.ctsFromWt="";
						cts.ctsToWt="";
						cts.ctsVehicleType="";
					}else{
						var response = $http.post($scope.projectName+'/addNewCTSForRC', cts);	   
						response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}			
					}
			}else{
				console.log("if list does not exists");
				if(ContToStationFormW.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormW.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				} else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					var response = $http.post($scope.projectName+'/addNewCTSForRC',cts);	   
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();	
								$scope.ContToStnFlag=true;
								cts.ctsToStn="";
								cts.ctsToStnTemp="";
								cts.ctsRate="";
								cts.ctsFromWt="";
								cts.ctsToWt="";
								cts.ctsVehicleType="";
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}	
				}
			}
	    
	    $scope.saveNewStation=function(StationForm,station){
			console.log("Entered into Submit function");
			$scope.station.stnName = station.stnName.toUpperCase();
			$scope.codeState = station.stnName+station.stnPin;
			console.log($scope.codeState);
			if(StationForm.$invalid){
				if(StationForm.stnName.$invalid){
					$scope.alertToast("Please enter station name between 3-40 characters...");
					$scope.station.stnName="";
				}else if(StationForm.stnDistrict.$invalid){
					$scope.alertToast("Please enter station district between 3-40 characters...");
					$scope.station.stnDistrict="";
				}else if(StationForm.stateCode.$invalid){
					$scope.alertToast("Please enter state code...");
				}else if(StationForm.stnPin.$invalid){
					$scope.alertToast("Please enter station pin of 6 digits...");
					$scope.station.stnPin="";
				}
			}else{
				if($.inArray($scope.codeState,$scope.type)!==-1){
					$scope.alertToast("Station already exists...");
					$scope.station.stnName="";
					$scope.station.stnDistrict="";
					$scope.station.stateCode="";
					$scope.station.stnPin="";
				}else{
					var response = $http.post($scope.projectName+'/submitStation',station);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
							$scope.station.stnName="";
							$scope.station.stnDistrict="";
							$scope.station.stateCode="";
							$scope.station.stnPin="";
							$('input[name=stateName]').attr('checked',false);
							$scope.getStationData();
							$('div#addStation').dialog('close');
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});
				}
			}	
		}
		
	    
	    $scope.saveContToStnQ = function(ContToStationFormQ,cts){
			console.log("Enter into saveContToStnQ function"+ContToStationFormQ.$invalid);
			$('div#contToStationQ').dialog("destroy");
			$scope.contToStationFlagQ = true;
			$scope.ctsflagQ=true;
			//$scope.ContToStnFlag=true;
		
			if($scope.ctsList.length>0){
				console.log("if list exists"+$scope.ctsList.length);
				if(ContToStationFormQ.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormQ.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				}else if(ContToStationFormQ.ctsProductType.$invalid){
					$scope.alertToast("Please enter Product Type");
				}else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsProductType=cts.ctsProductType.toUpperCase();
					$scope.ctsTemp= cts.ctsToStn+cts.ctsProductType;
					
					for(var i=0;i<$scope.ctsList.length;i++){
						console.log($scope.ctsList[i].ctsToStn);
						$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType;
						$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType);
					}
					if($.inArray($scope.ctsTemp,$scope.type)!==-1){
						$scope.alertToast("Already exists");
						cts.ctsToStn="";
						cts.ctsToStnTemp="";
						cts.ctsRate="";
						cts.ctsFromWt="";
						cts.ctsToWt="";
						cts.ctsVehicleType="";
					}else{
						var response = $http.post($scope.projectName+'/addNewCTSForRC', cts);	   
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									cts.ctsProductType="";
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}
					} 
			}else{
				console.log("if list does not exists");
				if(ContToStationFormQ.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormQ.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				}else if(ContToStationFormQ.ctsProductType.$invalid){
					$scope.alertToast("Please enter Product Type");
				}else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsProductType=cts.ctsProductType.toUpperCase();
					var response = $http.post($scope.projectName+'/addNewCTSForRC', cts);	   
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();	
								$scope.ContToStnFlag=true;
								cts.ctsToStn="";
								cts.ctsToStnTemp="";
								cts.ctsRate="";
								cts.ctsFromWt="";
								cts.ctsToWt="";
								cts.ctsVehicleType="";
								cts.ctsProductType="";
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}	
				}
			}
	    
	    $scope.fetchCTSList = function(){
			console.log("Entr into fetchCTSListForEditRC function");
			var response = $http.get($scope.projectName+'/fetchCTSListForEditRC');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.ctsList = data.list;
					console.log($scope.regCnt.regContType);
					if($scope.regCnt.regContType==='W'){
						$scope.newCtsFlagW=true;	 
					}else if($scope.regCnt.regContType==='Q'){
						$scope.newCtsFlagQ=true;	 
					}
				}else{
					console.log(data.result);
					$scope.ctsList = data.list;
					$scope.newCtsFlagW=false;
					$scope.newCtsFlagQ=false;
					 /*if($scope.regCnt.regContType==='W' || $scope.regCnt.regContType==='Q'){
						 console.log($scope.regCnt.regContType); 
						 $scope.ContToStnFlag=false;
					 }*/
				}		
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
	    $scope.removeNewCTS=function(cts) {
			console.log("Entr into removeNewCTSForRC function");
			var response = $http.post($scope.projectName+'/removeNewCTSForRC',cts);
			response.success(function(data, status, headers, config) {
				$scope.fetchCTSList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
		$scope.removeAllCTS = function() {
			console.log("Entr into removeAllCTSForEditRC function");
			var response = $http.post($scope.projectName+'/removeAllCTSForEditRC');
			response.success(function(data, status, headers, config) {
				$scope.fetchCTSList();
				console.log(data.result);
				$scope.ctsflagW=false;
				$scope.ctsflagQ=false;
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
		$scope.removeCTS = function(cts) {
			 console.log("entr into deleteCTSForRC function");
				var response = $http.post($scope.projectName+'/deleteCTSForRC',cts);
				response.success(function(data, status, headers, config) {
					$scope.getCTSData();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		    }
	 
		 $scope.sendMetricType=function(metricType){
				console.log(metricType);
				if(metricType==="" || metricType===null || angular.isUndefined(metricType)){
					$scope.alertToast("Please enter Metric type");
				}else{
					var response = $http.post($scope.projectName+'/sendMetricTypeForEditRC',metricType);
					response.success(function(data, status, headers, config) {
						$scope.alertToast(data.result);
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}
		
	 $scope.fetch=function(){
	    	$scope.removeAllRbkm();
		    $scope.removeAllPbd();
		    $scope.removeAllCTS();
	 }
	 
	  if($scope.adminLogin === true || $scope.superAdminLogin === true){
		  $scope.fetch();
	  }else if($scope.logoutStatus === true){
			 $location.path("/");
	  }else{
			 console.log("****************");
	  }
	    
}]);