package com.mylogistics.controller.report;
import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillDetailCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.Customer;
import com.mylogistics.services.ConstantsValues;

//import javafx.scene.text.FontWeight;

@Controller
public class RelianceOsRptController {
	@Autowired
	private ReportDAO reportDAO;
	
	private Map<String, Object> osRpt = new HashMap<>();
	
	private Map<String, Object> relosRpt = new HashMap<>();
	
	@RequestMapping(value="/getRelOsRpt", method=RequestMethod.POST)
	public @ResponseBody Object getRelOsRpt(@RequestBody Map<String, Object> osRptService){
		System.out.println("OsRptCntlr.getOsRpt()");
		Date date=new Date();

		Map<String, Object> map = new HashMap<>();

		System.out.println("upToDt: "+osRptService.get("upToDt"));
		System.out.println("selectCust:"+osRptService.get("custGroupId"));
	
		osRpt = reportDAO.getRelOsRpt(osRptService);
		
		if ((int)osRpt.get("temp") > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			//map.put("osRpt", osRpt);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		Date date2=new Date();
		System.out.println(date2.getTime()-date.getTime());
		
		return map;
	}
	
	public boolean checkLeap(String s2)
	{
		boolean b =false;
			String[] strings=s2.split("\\.");
			String year=strings[2];
			System.out.println(year);
			if(Integer.parseInt(year)%4==0)
			{
				b=true;
			}
			else
			{
				b=false;	
			}
			return b;    
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getRelOsRptXLSX", method=RequestMethod.POST)
	public void getOsRptXLSX(HttpServletRequest request, HttpServletResponse response) throws Exception{
		System.out.println("OsRptCntlr.getrelOsRptXLSX()");
		List<Map<String, Object>> osRelBillRptList=	(List<Map<String, Object>>) osRpt.get("resosBillRptList");
		List<Customer> custListTemp = (List<Customer>) osRpt.get("custList");
		System.out.println("Size of Os Bill Rpt List "+osRelBillRptList.size());
		System.out.println("Size of Customer List "+custListTemp.size());
		
		
		
		
	// creating the workbook to create a excel sheet
	XSSFWorkbook workbook=new XSSFWorkbook();
	
	
	// creating a excel sheet 
    XSSFSheet sheet=workbook.createSheet("Reliance Outstanding Format");
 
    // creating the first row in the sheet
    XSSFRow row=sheet.createRow(0);

    

    // getting the XSSFFont instance from the workbook
    XSSFFont font=workbook.createFont();
    font.setBold(true);
    
    // getting the XSSFFont instance from the workbook
    XSSFFont font2=workbook.createFont();
    font2.setBold(true);
    font2.setFontHeightInPoints((short) 15);
   // font2.setColor(HSSFColor.BLACK.index);

    
    // getting the cell style instance from the workbook
    XSSFCellStyle cellStyle= workbook.createCellStyle();
    cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
    cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
    cellStyle .setFont(font);
    
    
    XSSFCellStyle cellStyle3= workbook.createCellStyle();
    cellStyle3.setAlignment(CellStyle.ALIGN_RIGHT);
    cellStyle3.setVerticalAlignment(VerticalAlignment.CENTER);
    cellStyle3 .setFont(font2);

    
// getting the cell style instance from the workbook
    XSSFCellStyle cellStyle2= workbook.createCellStyle();
    cellStyle2.setAlignment(CellStyle.ALIGN_CENTER);
    cellStyle2.setWrapText(true);
    cellStyle2 .setFont(font);
    // to format data as Numeric
  //  cellStyle2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
    
    // creating a date format
    
   SimpleDateFormat dateFormat=new SimpleDateFormat("dd.MM.yyyy");
  

/* for (Customer customer : custListTemp) {*/
	

    
    // creating the cell from  the specified row
    XSSFCell cell;
/*  cell=  row.createCell(0);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Sr. No.");*/
  
  cell=  row.createCell(0);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Type of Service Provided");
  
  cell=  row.createCell(1);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Network/ Business");
  
  cell=  row.createCell(2);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Place Where Service is Provided");
  
  cell=  row.createCell(3);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Vendor Code");
  
  cell=  row.createCell(4);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Vendor Name");
  
  cell=  row.createCell(5);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Work Order Number");
  
  cell=  row.createCell(6);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Work Order issued by Company");
  
  cell=  row.createCell(7);
  cell.setCellStyle(cellStyle2);
  cell.setCellValue("Work Order Date(DD.MM.YYYY"+"\n"+" Format Only)");
  
  cell=  row.createCell(8);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Invoice Number");
  
  cell=  row.createCell(9);
  cell.setCellStyle(cellStyle2);
  cell.setCellValue("Invoice Date(DD.MM.YYYY"+"\n"+" Format Only)");
  
  cell=  row.createCell(10);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("From Period");
  
  cell=  row.createCell(11);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("To Period");
  
  cell=  row.createCell(12);
  cell.setCellStyle(cellStyle2);
  cell.setCellValue("Service Receiving Plant/ Site Code as"+"\n"+" per SAP");
  
  cell=  row.createCell(13);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Service Entry/JMS/GRN Number");
  
  cell=  row.createCell(14);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Service Entry/JMS/GRN Date");
  
  cell=  row.createCell(15);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Scroll Number");
  
  cell=  row.createCell(16);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Scroll Date");
  
  cell=  row.createCell(17);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Taxable Amount");
  
  cell=  row.createCell(18);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Tax Amount");
  
  cell=  row.createCell(19);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Total Bill Value");
  
  cell=  row.createCell(20);
  cell.setCellStyle(cellStyle2);
  cell.setCellValue("Fill Amount mentioned"+"\n"+" in WORDS on Invoice");
  
  cell=  row.createCell(21);
  cell.setCellStyle(cellStyle2);
  cell.setCellValue("Physical Bills Submitted in"+"\n"+" Original at");
  
  cell=  row.createCell(22);
  cell.setCellStyle(cellStyle2);
  cell.setCellValue("Physical Bills Handed Over/"+"\n"+" Addressed to");
  
  cell=  row.createCell(23);
  cell.setCellStyle(cellStyle2);
  
  cell.setCellValue("If Bills are sent through corier then"+"\n"+" Name of Courier and Docket Number"+"\n"+" & Docket Date");
  
  cell=  row.createCell(24);
  cell.setCellStyle(cellStyle);
  cell.setCellValue("Remarks(If Any)");
  
  int rowstocreate=1;
/*  ArrayList<String> arrayList=new ArrayList<String>();
  arrayList.add("Hi");
  arrayList.add("bye");
  arrayList.add("Hmm");
  arrayList.add("Hmm");*/
  		
  XSSFRow xssfRow=null;
  
  double taxableamounttotal=0;
 double taxamounttotal=0;
  
for (Map<String, Object> map : osRelBillRptList) {


	 xssfRow= sheet.createRow(rowstocreate);
	 
	/*cell= xssfRow.createCell(0);
	cell.setCellValue("fjj");*/
	
	cell= xssfRow.createCell(0);
	cell.setCellValue("Primary"+"\n"+" Transportation");
	
	cell= xssfRow.createCell(1);
	cell.setCellValue("Network");
	
	cell= xssfRow.createCell(2);
	cell.setCellValue(map.get("toStnName").toString());
	
	cell= xssfRow.createCell(3);
	cell.setCellValue("3234040");

	cell= xssfRow.createCell(4);
	cell.setCellValue("CareGo Logistics Pvt. Ltd.");
	
	cell= xssfRow.createCell(5);
	cell.setCellValue("63508639");
	
	cell= xssfRow.createCell(6);
	cell.setCellValue("Reliance Jio Infocomm Ltd");
	
	cell= xssfRow.createCell(7);
	cell.setCellValue("");
	
	cell= xssfRow.createCell(8);
	cell.setCellValue(map.get(BillCNTS.Bill_NO).toString());
	
	cell= xssfRow.createCell(9);
	
	// Formatting date in our pattern

	java.sql.Date sdate=(java.sql.Date)map.get(BillCNTS.BILL_DT);
	 String date= dateFormat.format(sdate);
	cell.setCellValue(date);	
	
	java.sql.Date date2=java.sql.Date.valueOf(map.get(CnmtCNTS.CNMT_DT).toString());
	String s2=	dateFormat.format(date2);
	String[] strings=s2.split("\\.");
	String month=strings[1];
	int month2=Integer.parseInt(month);
	int year=Integer.parseInt(strings[2]);
	System.out.println(month2);
	boolean  b=checkLeap(s2);
	int maxdayofmonth=0;
	
	if(b && month2==2){
		maxdayofmonth=29;	 
	}else if(month2==2){
		maxdayofmonth=28;
		}
	
	if(month2==1 || month2==3 || month2==5 || month2==7 || month2==8 || month2==10 || month2==12)
	{
		maxdayofmonth=31;
	}
	else if(month2==4 || month2==6 || month2==9 || month2==11)
	{
		maxdayofmonth=30;
	}
	
	
/*    LocalDate localDate=  date2.toLocalDate();
    int monthvalue=localDate.getMonthValue();
    int year=localDate.getYear();
    Month month=localDate.getMonth();
    boolean isleap=localDate.isLeapYear();
    int maxdateofmonth=0;
    if(isleap==true)
    {
    	maxdateofmonth=month.maxLength();
    }
    else
    {
    	maxdateofmonth=month.minLength();
    }*/
	        
	String startDateofMonth;
	String maxDateOfMonth;
	if(month2<10)
	{
	startDateofMonth=String.valueOf("01.0"+month2+"."+year);
	maxDateOfMonth=String.valueOf(maxdayofmonth+".0"+month2+"."+year);
	}
	else
	{
		startDateofMonth=String.valueOf("01."+month2+"."+year);	
		maxDateOfMonth=String.valueOf(maxdayofmonth+"."+month2+"."+year);
	}
	cell= xssfRow.createCell(10);
	cell.setCellValue(startDateofMonth);
	
	cell= xssfRow.createCell(11);
	cell.setCellValue(maxDateOfMonth);
	
	cell= xssfRow.createCell(12);
	cell.setCellValue("");
	
	cell=xssfRow.createCell(13);
	cell.setCellValue(map.get(CnmtCNTS.CNMT_CODE).toString());
	

	String cnmtDate=dateFormat.format(date2);
	cell= xssfRow.createCell(14);
	cell.setCellValue(cnmtDate);
	
	cell= xssfRow.createCell(15);
	cell.setCellValue("");
	
	cell= xssfRow.createCell(16);
	cell.setCellValue("");
	
   String bdtotlAmt=	map.get(BillDetailCNTS.BD_TOT_AMT).toString();
   Double amount=0.0;
   if(bdtotlAmt!=null)
   {
	   amount=  Double.parseDouble(bdtotlAmt);

   }
taxableamounttotal=taxableamounttotal+amount;
	cell= xssfRow.createCell(17);
	cell.setCellValue(amount);
	
	//String taxAmt=null;
	double taxamount=0.0;
	if(bdtotlAmt!=null)
	{
		Double totAmt=Double.parseDouble(bdtotlAmt);
		taxamount=(totAmt/20);
		//taxAmt=String.valueOf(taxamount);
	}
	taxamounttotal=taxamounttotal+taxamount;
	
	cell= xssfRow.createCell(18);
	cell.setCellValue(taxamount);
	
	cell= xssfRow.createCell(19);
	cell.setCellValue(amount);
	
	cell= xssfRow.createCell(20);
	cell.setCellValue(amount);
	
	cell= xssfRow.createCell(21);
	cell.setCellValue("RCP, Ghansoli");
	
	cell= xssfRow.createCell(22);
	cell.setCellValue("RCP, Ghansoli");
	
	cell= xssfRow.createCell(23);
	cell.setCellValue("By Hand");
	
	cell= xssfRow.createCell(24);
	cell.setCellValue("");
	
	rowstocreate++;    // creating row on behalf size of the list
}
     
XSSFRow xssfRow2=sheet.createRow(rowstocreate+1);
XSSFCell xssfCell=xssfRow2.createCell(0);
xssfCell.setCellStyle(cellStyle3);
xssfCell.setCellValue("Total  ");

XSSFCell xssfCell2=xssfRow2.createCell(17);
xssfCell2.setCellStyle(cellStyle3);
xssfCell2.setCellValue(String.valueOf(Math.round(taxableamounttotal)));

XSSFCell xssfCell3=xssfRow2.createCell(18);
xssfCell3.setCellStyle(cellStyle3);
xssfCell3.setCellValue(String.valueOf(Math.round(taxamounttotal)));

XSSFCell xssfCell4=xssfRow2.createCell(19);
xssfCell4.setCellStyle(cellStyle3);
xssfCell4.setCellValue(String.valueOf(Math.round(taxableamounttotal)));

XSSFCell xssfCell5=xssfRow2.createCell(20);
xssfCell5.setCellStyle(cellStyle3);
xssfCell5.setCellValue(String.valueOf(Math.round(taxableamounttotal)));


sheet.addMergedRegion(new CellRangeAddress(rowstocreate+1, rowstocreate+1, 0, 16));
System.out.println("Taxable Amount "+taxableamounttotal);
System.out.println("Tax Amount "+taxamounttotal);


  // Defining the size to a cell automatically
 for (int i = 0; i < 25; i++) {
			sheet.autoSizeColumn(i);
		}
 	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", "attachment; filename=relOsRpt.xlsx");
	ServletOutputStream out = response.getOutputStream();
	workbook.write(out);
	out.flush();
	out.close();
	}
	
	
	
	


	@RequestMapping(value="/getRelOs", method=RequestMethod.POST)
	public @ResponseBody Object getRelOs(@RequestBody Map<String, Object> osRptService){
		System.out.println("OsRptCntlr.getOsRpt()");
		Date date=new Date();

		Map<String, Object> map = new HashMap<>();

		System.out.println("upToDt: "+osRptService.get("upToDt"));
		System.out.println("selectCust:"+osRptService.get("custGroupId"));
	
		relosRpt = reportDAO.getRelOs(osRptService);
		
		if ((int)relosRpt.get("temp") > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			//map.put("osRpt", osRpt);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		Date date2=new Date();
		System.out.println(date2.getTime()-date.getTime());
		
		return map;
	}








	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getRelOsRXLSX", method = RequestMethod.POST)
	public void getOsRXLSX(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		System.out.println("OsRptCntlr.getrelOsRptXLSX()");
		List<Map<String, Object>> osRelBillRptList = (List<Map<String, Object>>) relosRpt
				.get("resosBillRptList");
		List<Customer> custListTemp = (List<Customer>) relosRpt.get("custList");
		System.out.println("Size of Os Bill Rpt List "
				+ osRelBillRptList.size());
		System.out.println("Size of Customer List " + custListTemp.size());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

		// creating the workbook to create a excel sheet
		XSSFWorkbook workbook = new XSSFWorkbook();

		// creating a excel sheet
		XSSFSheet sheet = workbook.createSheet("Reliance Outstanding Format");

		// creating the first row in the sheet
		XSSFRow row = sheet.createRow(0);

		// getting the XSSFFont instance from the workbook
		XSSFFont font = workbook.createFont();
		font.setBold(true);

		// getting the XSSFFont instance from the workbook
		XSSFFont font2 = workbook.createFont();
		font2.setBold(true);
		font2.setFontHeightInPoints((short) 15);
		// font2.setColor(HSSFColor.BLACK.index);

		// getting the cell style instance from the workbook
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setFont(font);

		XSSFCellStyle cellStyle3 = workbook.createCellStyle();
		cellStyle3.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle3.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle3.setFont(font2);

		// getting the cell style instance from the workbook
		XSSFCellStyle cellStyle2 = workbook.createCellStyle();
		cellStyle2.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle2.setWrapText(true);
		cellStyle2.setFont(font);
		// to format data as Numeric
		// cellStyle2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));

		// creating a date format

		/* for (Customer customer : custListTemp) { */

		// creating the cell from the specified row
		XSSFCell cell;
		/*
		 * cell= row.createCell(0); cell.setCellStyle(cellStyle);
		 * cell.setCellValue("Sr. No.");
		 */

		cell = row.createCell(0);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Sr. No.");

		cell = row.createCell(1);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Vendor Code");

		cell = row.createCell(2);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Vendor Name");

		cell = row.createCell(3);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Bill No");

		cell = row.createCell(4);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Bill Date");

		cell = row.createCell(5);
		cell.setCellStyle(cellStyle2);
		cell.setCellValue("Bill Amount");

		cell = row.createCell(6);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("LR NO");

		cell = row.createCell(7);
		cell.setCellStyle(cellStyle2);
		cell.setCellValue("LR Date");

		cell = row.createCell(8);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Supplier Inovice./STN No");

		cell = row.createCell(9);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Truck NO.");

		cell = row.createCell(10);
		cell.setCellStyle(cellStyle2);
		cell.setCellValue("From");

		cell = row.createCell(11);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("To");

		cell = row.createCell(12);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Vehicle Type");

		cell = row.createCell(13);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Frieght Amount");

		cell = row.createCell(14);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Other Charges");

		cell = row.createCell(15);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Total Amount");

		cell = row.createCell(16);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Type of Movement Primary/Secondary");

		cell = row.createCell(17);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("Remarks if any");

		int rowstocreate = 1;

		XSSFRow xssfRow = null;
		
		double totalfrieght=0.0;
		double totalothchrg=0.0;
		double totalamt=0.0;

		for (Map<String, Object> map : osRelBillRptList) {

			xssfRow = sheet.createRow(rowstocreate);

			/*
			 * cell= xssfRow.createCell(0); cell.setCellValue("fjj");
			 */
			
			totalfrieght=Math.round(totalfrieght+Double.parseDouble(map.get(BillDetailCNTS.BD_FREIGHT).toString()));
			totalothchrg=Math.round(totalothchrg+Double.parseDouble(map.get(BillDetailCNTS.BD_OTH_CHG_AMT).toString()));
			totalamt=Math.round(totalamt+Double.parseDouble(map.get(BillDetailCNTS.BD_TOT_AMT).toString()));


			cell = xssfRow.createCell(0);
			cell.setCellValue(rowstocreate);

			cell = xssfRow.createCell(1);
			cell.setCellValue("3234040");

			cell = xssfRow.createCell(2);
			cell.setCellValue("CareGo Logistics Pvt. Ltd.");

			cell = xssfRow.createCell(3);
			cell.setCellValue(map.get(BillCNTS.Bill_NO).toString());
			java.sql.Date date2 = java.sql.Date.valueOf(map.get(
					BillCNTS.BILL_DT).toString());
			cell = xssfRow.createCell(4);
			cell.setCellValue(dateFormat.format(date2));

			cell = xssfRow.createCell(5);
			cell.setCellValue(Double.parseDouble(map.get(
					BillDetailCNTS.BD_TOT_AMT).toString()));

			cell = xssfRow.createCell(6);
			cell.setCellValue(map.get(CnmtCNTS.CNMT_CODE).toString());

			java.sql.Date date = java.sql.Date.valueOf(map
					.get(CnmtCNTS.CNMT_DT).toString());

			cell = xssfRow.createCell(7);
			cell.setCellValue(dateFormat.format(date));

			cell = xssfRow.createCell(8);
			System.out.println(map.get("invoiceNo").toString());
			if (map.get("invoiceNo").toString() != null) {
				cell.setCellValue(map.get("invoiceNo").toString());
			}

			cell = xssfRow.createCell(9);
			cell.setCellValue(map.get("truckNo").toString());

			cell = xssfRow.createCell(10);
			cell.setCellValue(map.get("fromStnName").toString());

			cell = xssfRow.createCell(11);
			cell.setCellValue(map.get("toStnName").toString());

			cell = xssfRow.createCell(12);
			cell.setCellValue(map.get(ChallanCNTS.CHLN_VEHICLE_TYPE).toString());

			cell = xssfRow.createCell(13);
			cell.setCellValue(Math.round(Double.parseDouble(map.get(
					BillDetailCNTS.BD_FREIGHT).toString())));

			cell = xssfRow.createCell(14);
			cell.setCellValue(Math.round(Double.parseDouble(map.get(
					BillDetailCNTS.BD_OTH_CHG_AMT).toString())));

			cell = xssfRow.createCell(15);
			cell.setCellValue(Math.round(Double.parseDouble(map.get(
					BillDetailCNTS.BD_TOT_AMT).toString())));

			cell = xssfRow.createCell(16);
			cell.setCellValue("");

			cell = xssfRow.createCell(17);
			cell.setCellValue("");

			rowstocreate++; // creating row on behalf size of the list
		}

		
		XSSFRow xssfRow2 = sheet.createRow(rowstocreate + 1);
		XSSFCell xssfCell = xssfRow2.createCell(0);
		xssfCell.setCellStyle(cellStyle3);
		xssfCell.setCellValue("Total  ");

		XSSFCell xssfCell2 = xssfRow2.createCell(13);
		xssfCell2.setCellStyle(cellStyle3);
		xssfCell2.setCellValue(String.valueOf(Math.round(totalfrieght)));

		XSSFCell xssfCell3 = xssfRow2.createCell(14);
		xssfCell3.setCellStyle(cellStyle3);
		xssfCell3.setCellValue(String.valueOf(Math.round(totalothchrg)));

		XSSFCell xssfCell4 = xssfRow2.createCell(15);
		xssfCell4.setCellStyle(cellStyle3);
		xssfCell4.setCellValue(String.valueOf(Math.round(totalamt)));

		sheet.addMergedRegion(new CellRangeAddress(rowstocreate + 1,
				rowstocreate + 1, 0, 12));
		
		// Defining the size to a cell automatically
		for (int i = 0; i < 17; i++) {
			sheet.autoSizeColumn(i);
		}
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=relOs.xlsx");
		ServletOutputStream out = response.getOutputStream();
		workbook.write(out);
		out.flush();
		out.close();
	}	
	
}
