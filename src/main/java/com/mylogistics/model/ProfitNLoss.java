package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profitnloss")
public class ProfitNLoss {
	
	@Id
	@Column(name="pnlId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pnlId;
	
	@Column(name="pnlName")
	private String pnlName;
	
	@Column(name="pnlFaCode")
	private String pnlFaCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public String getPnlFaCode() {
		return pnlFaCode;
	}

	public void setPnlFaCode(String pnlFaCode) {
		this.pnlFaCode = pnlFaCode;
	}

	public int getPnlId() {
		return pnlId;
	}

	public void setPnlId(int pnlId) {
		this.pnlId = pnlId;
	}

	public String getPnlName() {
		return pnlName;
	}

	public void setPnlName(String pnlName) {
		this.pnlName = pnlName;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
}