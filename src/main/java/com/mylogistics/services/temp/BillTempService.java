package com.mylogistics.services.temp;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.temp.BillTempDetail;
import com.mylogistics.model.temp.BillTempMain;

public class BillTempService {

	private ArrayList<BillTempDetail> btDetailList = new ArrayList<>();
	
	private BillTempMain btMain;

	public List<BillTempDetail> getBtDetailList() {
		return btDetailList;
	}

	
	public void setBtDetailList(ArrayList<BillTempDetail> btDetailList) {
		this.btDetailList = btDetailList;
	}

	public BillTempMain getBtMain() {
		return btMain;
	}

	public void setBtMain(BillTempMain btMain) {
		this.btMain = btMain;
	}

}
