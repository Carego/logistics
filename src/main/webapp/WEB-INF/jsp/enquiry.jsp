<div ng-show="operatorLogin || superAdminLogin">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		<form name="enquiryForm" ng-submit="submitEnquiry(enquiryForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					       	
		       	<div class="col s3 input-field">
		    		<select name="typeName" id="typeId" ng-model="type" ng-init="type='cnmt'" required ng-change="getTypeCodeList()">
						<option value='cnmt'>CNMT</option>
						<option value='chln'>CHALLAN</option>
						<option value='sedr'>SEDR</option>
						<option value='bill'>BILL</option>
						<option value='mr'>MR</option>
						<option value='loryNo'>LORRY NO</option>
						<option value='chq'>CHEQUE</option>
						<option value='amt'>AMOUNT</option>
					</select>
					<label>Type</label>
				</div>
		       	
		       	<div class="col s3 input-field" ng-show="amtFlag">		       		
		       		<input class="validate" type ="text" id="faCodeFrom" name="faCodeFrom" ng-model="faCodeFrom">
		       		<label for="code">Fa Code From</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field" ng-show="amtFlag">		       		
		       		<input class="validate" type ="text" id="faCodeTo" name="faCodeTo" ng-model="faCodeTo">
		       		<label for="code">Fa Code To</label>	
		       	</div>
		       		
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="codeId" name ="codeName" ng-model="code" ng-required="true">
		       		<label for="code" id="select">Select CNMT No.</label>	
		       	</div>		       	
		       	
		       	<div class="col s3 input-field" ng-show="amtFlag">
					<input type ="date" id="fromDate" name="fromDate" ng-model="fromDate"/>
					<label>From Date</label>
				</div>
				
				<div class="col s3 input-field" ng-show="amtFlag">
					<input type ="date" id="toDate" name="toDate" ng-model="toDate"/>
					<label>To Date</label>
				</div>
		       	
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" id="bnkAddId" name ="bnkAdd" value="Submit" >
	       		</div>
			
			</div>
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="codeDB" ng-hide="codeDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
	 	    <tr>
 	  	  	  <th></th>
 	  	  	  <th>{{type | uppercase}}</th>
 	  	  </tr>
 	  	  <tr ng-repeat="code in codeList | filter:filterTextbox2">
		 	  <td><input type="radio" name="code" value="{{ code }}" ng-click="saveCode(code)"></td>
              <td>{{code}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="faCodeDB" ng-show="faCodeFlag" class="noprint" title = "Fa Code">
    		<input type="text" name="filterTextbox" ng-model="faCode" placeholder="Search by Bank Code ">    	
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> Fa Code </th>
	 	  	  	  <th> Name </th>
	 	  	  	  <th> Type </th>
	 	  	  </tr>	 	  		
			  <tr ng-repeat="faM in faMList | filter:faCode ">
			 	  <td><input type="radio"  name="faM"   value="{{ faM }}" ng-model="faMaster" ng-click="saveFaCode(faM)"></td>
	              <td>{{faM.faMfaCode}}</td>
	              <td>{{faM.faMfaName}}</td>
	              <td>{{faM.faMfaType}}</td>
	          </tr>
	      </table> 
    </div>
    
    <div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>
	<div ng-show="allFlag">
	
	<!-- CashStmt(Cheque) Enquiry Table -->
	<div class="row" ng-show="cashStmtEnquiryTblFlag">
		<table class="table">
       		<caption class="coltag tblrow">Enquiry (Cash Stmt)</caption>			
			<tr>
				<td class="rowcel">
				   	<table>
				    	<tr>
				    		<td class="rowcelheader">S.No.</td>
					        <td class="rowcelheader">BranchCode</td>
					        <td class="rowcelheader">FaCode</td>
					        <td class="rowcelheader">TvNo</td>
					        <td class="rowcelheader">ChequeNo</td>
					        <td class="rowcelheader">ChequeType</td>
					        <td class="rowcelheader">PayTo</td>					        
					        <td class="rowcelheader">Type</td>
					        <td class="rowcelheader">Amount</td>
					        <td class="rowcelheader">DrCr</td>					        
					        <td class="rowcelheader">Date</td>					        
					        <td class="rowcelheader">VouchType</td>
					   		<td class="rowcelheader">Description</td>		        
					     </tr>
					     <tr ng-repeat="cashStmt in cashStmtList">
					     	<td class="rowcelwhite">{{$index+1}}</td>
					    	<td class="rowcelwhite">{{cashStmt.bCode}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csFaCode}}</td>					    					    		    	
					    	<td class="rowcelwhite cursor" ng-if="cashStmt.csType == 'Money Receipt'" ng-click="getBillDetByMr(cashStmt.csTvNo)">{{cashStmt.csTvNo}}</td>
					    	<td class="rowcelwhite" ng-if="cashStmt.csType != 'Money Receipt'">{{cashStmt.csTvNo}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csChqNo}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csChequeType}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csPayTo}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csType}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csAmt}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csDrCr}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csDt}}</td>					    	
					    	<td class="rowcelwhite">{{cashStmt.csVouchType}}</td>
					    	<td class="rowcelwhite">{{cashStmt.csDescription}}</td>					    	
					     </tr>
					     
					     <!-- 
					     <tr ng-repeat="cashStmt in enquiryList">
					        <td class="rowcelwhite">{{cashStmt.csId}}</td>
					     </tr>
					     -->
					</table>
				</td>
			</tr>
		</table>
	</div>
	
		
	<!-- MR Enquiry Table -->
	<div class="row" ng-show="mrEnquiryTblFlag">
		<table class="table">
       		<caption class="coltag tblrow">Enquiry (Money Receipt)</caption>			
			<tr>
				<td class="rowcel">
				   	<table>
				    	<tr  ng-repeat="moneyReceipt in mrList">
				    		<td class="rowcelheader">S.No.</td>
				    		<td class="rowcelheader">MrNo</td>
					        <td class="rowcelheader">MrBranchCode</td>					        
					        <td class="rowcelheader" ng-if="moneyReceipt.mrBankName != null && moneyReceipt.mrBankName != '' ">MrBankName</td>
					        <td class="rowcelheader" ng-if="moneyReceipt.mrPayBy != null && moneyReceipt.mrPayBy != '' ">MrPayMode</td>
					        <td class="rowcelheader" ng-if="moneyReceipt.mrChqNo != null && moneyReceipt.mrChqNo != '' ">MrChequeNo</td>					        
					        <td class="rowcelheader" ng-if="moneyReceipt.mrCustBankName != null && moneyReceipt.mrCustBankName != '' ">MrCustBankName</td>
					        <td class="rowcelheader">MrCustName</td>					        
					        <td class="rowcelheader">MrDate</td>					        
					        <td class="rowcelheader">MrNetAmount</td>					        
					        <td class="rowcelheader">MrRemAmt</td>					        
					        <td class="rowcelheader">MrType</td>					        
					   		<td class="rowcelheader">MrDescription</td>		        
					     </tr>
					     <tr ng-repeat="moneyReceipt in mrList">
					     	<td class="rowcelwhite">{{$index+1}}</td>
					    	<td class="rowcelwhite">{{moneyReceipt.mrNo}}</td>
					    	<td class="rowcelwhite">{{moneyReceipt.mrBrhId}}</td>					    		    	
					    	<td class="rowcelwhite" ng-if="moneyReceipt.mrBankName != null && moneyReceipt.mrBankName != '' ">{{moneyReceipt.mrBankName}}</td>
					    	<td class="rowcelwhite" ng-if="moneyReceipt.mrPayBy != null && moneyReceipt.mrPayBy != '' ">{{moneyReceipt.mrPayBy}}</td>
							<td class="rowcelwhite" ng-if="moneyReceipt.mrChqNo != null && moneyReceipt.mrChqNo != '' ">{{moneyReceipt.mrChqNo}}</td>
					    	<td class="rowcelwhite" ng-if="moneyReceipt.mrCustBankName != null && moneyReceipt.mrCustBankName != '' ">{{moneyReceipt.mrCustBankName}}</td>
					    	<td class="rowcelwhite">{{moneyReceipt.mrCustName}}</td>
					    	<td class="rowcelwhite">{{moneyReceipt.mrDt}}</td>					    	
					    	<td class="rowcelwhite">{{moneyReceipt.mrNetAmt}}</td>					    	
					    	<td class="rowcelwhite">{{moneyReceipt.mrRemAmt}}</td>
					    	<td class="rowcelwhite">{{moneyReceipt.mrType}}</td>  	
					    	<td class="rowcelwhite">{{moneyReceipt.mrDesc}}</td>				    	
					     </tr>
					     
					     <!-- 
					     <tr ng-repeat="cashStmt in enquiryList">
					        <td class="rowcelwhite">{{cashStmt.csId}}</td>
					     </tr>
					     -->
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<!-- Bill Enquiry By MR Table -->
	<div class="row" ng-show="billEnquiryByMr">
		<table class="table">
       		<caption class="coltag tblrow">Bill Enquiry</caption>			
			<tr>
				<td class="rowcel">
				   	<table>
				    	<tr>
				    		<td class="rowcelheader">S.No.</td>
					        <td class="rowcelheader">Bill No</td>
					        <td class="rowcelheader">Date</td>
					        <td class="rowcelheader">Frt. Amt</td>
					        <td class="rowcelheader">Exc. Amt</td>
					        <td class="rowcelheader">TDS Amt</td>
					        <td class="rowcelheader">Ded. Amt</td>					        
					        <td class="rowcelheader">Rec. Amt</td>					        		        
					     </tr>
					     <tr ng-repeat="mrBill in  billDetByMr">
					     	<td class="rowcelwhite">{{$index+1}}</td>
					    	<td class="rowcelwhite">{{mrBill.billNo}}</td>
					    	<td class="rowcelwhite">{{mrBill.billDt}}</td>					    		    	
					    	<td class="rowcelwhite">{{mrBill.frtAmt}}</td>
							<td class="rowcelwhite">{{mrBill.excAmt}}</td>
					    	<td class="rowcelwhite">{{mrBill.tdsAmt}}</td>
					    	<td class="rowcelwhite">{{mrBill.dedAmt}}</td>
					    	<td class="rowcelwhite">{{mrBill.recAmt}}</td>					    						    	
					     </tr>				     
					     <!-- 
					     <tr ng-repeat="cashStmt in enquiryList">
					        <td class="rowcelwhite">{{cashStmt.csId}}</td>
					     </tr>
					     -->
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	
	<!-- LHPVAdv(Cheque) Enquiry Table -->
	<div class="row" ng-show="lhpvAdvEnquiryTblFlag">
		<table class="table">
       		<caption class="coltag tblrow">Enquiry (LHPV Adv)</caption>			
			<tr>
				<td class="rowcel">
				   	<table>
				    	<tr>
				    		<td class="rowcelheader">S.No.</td>
				    		<td class="rowcelheader">LHPV No</td>
					        <td class="rowcelheader">BranchCode</td>
					        <td class="rowcelheader">BrkOwn</td>
					        <td class="rowcelheader">BankCode</td>
					        <td class="rowcelheader">ChequeNo</td>
					        <td class="rowcelheader">ChequeType</td>
					        <td class="rowcelheader">CashDiscR</td>					        
					        <td class="rowcelheader">FinalTot</td>
					        <td class="rowcelheader">LryAdvP</td>
					        <td class="rowcelheader">TdsR</td>					        
					        <td class="rowcelheader">TotPayAmt</td>
					        <td class="rowcelheader">TotRcvrAmt</td>					        
					   		<td class="rowcelheader">Date</td>		        
					     </tr>
					     <tr ng-repeat="lhpvAdv in lhpvAdvList">
					     	<td class="rowcelwhite">{{$index+1}}</td>
					     	<td class="rowcelwhite">{{lhpvAdv.sheetNo}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.bCode}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laBrkOwn}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laBankCode}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laChqNo}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laChqType}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laCashDiscR}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laFinalTot}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laLryAdvP}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laTdsR}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laTotPayAmt}}</td>
					    	<td class="rowcelwhite">{{lhpvAdv.laTotRcvrAmt}}</td>					    	
					    	<td class="rowcelwhite">{{lhpvAdv.laDt}}</td>					    	
					     </tr>
					     
					     <!-- 
					     <tr ng-repeat="cashStmt in enquiryList">
					        <td class="rowcelwhite">{{cashStmt.csId}}</td>
					     </tr>
					     -->
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<!-- LHPVBal(Cheque) Enquiry Table -->
	<div class="row" ng-show="lhpvBalEnquiryTblFlag">
		<table class="table">
       		<caption class="coltag tblrow">Enquiry (LHPV Bal)</caption>			
			<tr>
				<td class="rowcel">
				   	<table>
				    	<tr>
				    		<td class="rowcelheader">S.No.</td>
				    		<td class="rowcelheader">LHPV No</td>
					        <td class="rowcelheader">BranchCode</td>
					        <td class="rowcelheader">BrkOwn</td>
					        <td class="rowcelheader">BankCode</td>
					        <td class="rowcelheader">ChequeNo</td>
					        <td class="rowcelheader">ChequeType</td>
					        <td class="rowcelheader">CashDiscR</td>					        
					        <td class="rowcelheader">FinalTot</td>
					        <td class="rowcelheader">LryAdvP</td>
					        <td class="rowcelheader">TdsR</td>					        
					        <td class="rowcelheader">TotPayAmt</td>
					        <td class="rowcelheader">TotRcvrAmt</td>					        
					   		<td class="rowcelheader">Date</td>		        
					     </tr>
					     <tr ng-repeat="lhpvBal in lhpvBalList">
					     	<td class="rowcelwhite">{{$index+1}}</td>
					     	<td class="rowcelwhite">{{lhpvBal.sheetNo}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.bCode}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbBrkOwn}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbBankCode}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbChqNo}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbChqType}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbCashDiscR}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbFinalTot}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbLryAdvP}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbTdsR}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbTotPayAmt}}</td>
					    	<td class="rowcelwhite">{{lhpvBal.lbTotRcvrAmt}}</td>					    	
					    	<td class="rowcelwhite">{{lhpvBal.lbDt}}</td>					    	
					     </tr>					     
					     <!-- 
					     <tr ng-repeat="cashStmt in enquiryList">
					        <td class="rowcelwhite">{{cashStmt.csId}}</td>
					     </tr>
					     -->
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<!-- LHPVBal(Cheque) Enquiry Table -->
	<div class="row" ng-show="lhpvSupEnquiryTblFlag">
		<table class="table">
       		<caption class="coltag tblrow">Enquiry (LHPV Sup)</caption>			
			<tr>
				<td class="rowcel">
				   	<table>
				    	<tr>
				    		<td class="rowcelheader">S.No.</td>
				    		<td class="rowcelheader">LHPV No</td>
					        <td class="rowcelheader">BranchCode</td>
					        <td class="rowcelheader">BrkOwn</td>
					        <td class="rowcelheader">BankCode</td>
					        <td class="rowcelheader">ChequeNo</td>
					        <td class="rowcelheader">ChequeType</td>
					        <td class="rowcelheader">CashDiscR</td>					        
					        <td class="rowcelheader">FinalTot</td>
					        <td class="rowcelheader">LryAdvP</td>
					        <td class="rowcelheader">TdsR</td>					        
					        <td class="rowcelheader">TotPayAmt</td>
					        <td class="rowcelheader">TotRcvrAmt</td>					        
					   		<td class="rowcelheader">Date</td>		        
					     </tr>
					     <tr ng-repeat="lhpvSup in lhpvSupList">
					     	<td class="rowcelwhite">{{$index+1}}</td>
					     	<td class="rowcelwhite">{{lhpvSup.sheetNo}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.bCode}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspBrkOwn}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspBankCode}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspChqNo}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspChqType}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspCashDiscR}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspFinalTot}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspLryAdvP}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspTdsR}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspTotPayAmt}}</td>
					    	<td class="rowcelwhite">{{lhpvSup.lspTotRcvrAmt}}</td>					    	
					    	<td class="rowcelwhite">{{lhpvSup.lspDt}}</td>					    	
					     </tr>					     
					     <!-- 
					     <tr ng-repeat="cashStmt in enquiryList">
					        <td class="rowcelwhite">{{cashStmt.csId}}</td>
					     </tr>
					     -->
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	
	<!-- Enquiry Table -->
	
	<div class="row" ng-show="otherEnquiryTblFlag">
		<table class="table">
       		<caption class="coltag tblrow">Enquiry</caption> 
			<tr>
				<th class="colclr cursor" ng-click="getDetailBill()">BILL</th>
				<th class="colclr cursor" ng-click="getDetailCnmt()">CNMT</th>
				<th class="colclr cursor" ng-click="getDetailChln()">CHALLAN</th>
			</tr>
			
			<tr ng-repeat="cnmt in enquiryList">
			
				<!-- Bill -->
				<td class="rowcel">
				   	<table>
				    	<tr>
					        <td class="rowcelheader">BillNo</td>
					        <td class="rowcelheader">Dt</td>
					        <td class="rowcelheader">Total</td>
					        <td class="rowcelheader">Typ</td>
					        <td class="rowcelheader">BF No.</td>
					         <td class="rowcelheader">BF Date</td>
					          <td class="rowcelheader">BF Recv Dt</td>
					   		<td class="rowcelheader">MRAmt</td>
					        <td class="rowcelheader" ng-show="detailBillFlag">Wt</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">Rt</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">Frt</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">Unldng</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">Det</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">Oth</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">OP</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">MRNo</td>
					   		<td class="rowcelheader" ng-show="detailBillFlag">MRDate</td>
					   		</tr>
				     	
				     	<!-- Normal bill -->
				     	<tr>
					        <td class="rowcelwhite">{{cnmt.billNormal.blBillNo}}</td>
					        <td class="rowcelwhite">{{cnmt.billNormal.blBillDt | date:'d/M/y'}}</td>
					        <td class="rowcelwhite">{{cnmt.billNormal.blFinalTot}}</td>
					        <td class="rowcelwhite">{{cnmt.billNormal.blType}}</td>
					        <td class="rowcelwhite">{{cnmt.billNormal.bfNo}}</td>
					        <td class="rowcelwhite">{{cnmt.billNormal.bfDt | date:'d/M/y'}}</td>
					        <td class="rowcelwhite">{{cnmt.billNormal.bfRecDt | date:'d/M/y'}}</td>
					     <td class="rowcelwhite">{{cnmt.billNormal.blTotAmtByMr}}</td>
					         <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.bdWt / 1000 | number : 2}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.bdRate * 1000 | number : 2}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.bdFreight | number : 2}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.bdUnloadAmt}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.bdDetAmt}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.bdOthChgAmt}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{cnmt.billNormal.userCode}}</td>
					        <td class="rowcelwhite cursor" ng-show="detailBillFlag" ng-click="showMrList(cnmt.billNormal.mrList, cnmt.billNormal.blBillNo)" ng-repeat="mr in cnmt.billNormal.mrList | limitTo : 1 : 0">{{mr.mrNo}}</td>
					        <td class="rowcelwhite cursor" ng-show="detailBillFlag" ng-click="showMrList(cnmt.billNormal.mrList, cnmt.billNormal.blBillNo)" ng-repeat="mr in cnmt.billNormal.mrList | limitTo : 1 : 0">{{mr.mrDate | date : 'dd-MM-yyyy'}}</td>
					        <td class="rowcelwhite" ng-hide="cnmt.billNormal.mrList.length>0" ></td>
				     		  </tr>
				     	
				     	<!-- Supplementry Bill -->
				     	<tr ng-repeat="billSup in cnmt.billSupList">
					        <td class="rowcelwhite">{{billSup.blBillNo}}</td>
					        <td class="rowcelwhite">{{billSup.blBillDt | date:'d/M/y'}}</td>
					        <td class="rowcelwhite">{{billSup.blFinalTot}}</td>
					        <td class="rowcelwhite">{{billSup.blType}}</td>
					        <td class="rowcelwhite">{{billSup.bfNo}}</td>
					        <td class="rowcelwhite">{{billSup.bfDt | date:'d/M/y'}}</td>
					        <td class="rowcelwhite">{{billSup.bfRecDt | date:'d/M/y'}}</td>
					       <td class="rowcelwhite">{{billSup.blTotAmtByMr}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.bdWt / 1000 | number : 2}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.bdRate * 1000 | number : 2}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.bdFreight | number : 2}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.bdUnloadAmt}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.bdDetAmt}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.bdOthChgAmt}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag">{{billSup.userCode}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag" ng-click="showMrList(cnmt.billNormal.mrList, cnmt.billNormal.blBillNo)" ng-repeat="mr in billSup.mrList | limitTo : 1 : 0">{{mr.mrNo}}</td>
					        <td class="rowcelwhite" ng-show="detailBillFlag" ng-click="showMrList(cnmt.billNormal.mrList, cnmt.billNormal.blBillNo)" ng-repeat="mr in billSup.mrList | limitTo : 1 : 0">{{mr.mrDate | date : 'dd-MM-yyyy'}}</td>
					        <td class="rowcelwhite" ng-hide="billSup.mrList.length>0" ></td>
				     		</tr>
				   	</table>
			   	</td>
			   	
			   	<!-- CNMT -->
			   	<td class="rowcel">
			   		<table>
			            <tr>
			            	<td class="rowcelheader">CnmtNo</td>
			            	<td class="rowcelheader">BrhCode</td>
			      			<td class="rowcelheader">Dt</td>
			      			<td class="rowcelheader">FStn</td>
			      			<td class="rowcelheader">TStn</td>
			      			<td class="rowcelheader">Awt</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">ChWt</td>
			      			<td class="rowcelheader">EDt</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">Pkg</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">Frt</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">Tot</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">BOC</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">DC</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">InvVal</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">OP</td>
			      			<td class="rowcelheader" ng-show="detailCnmtFlag">InvNo</td>
			      			
							<td class="rowcelheader" ng-show="detailCnmtFlag">CustName</td>
							<td class="rowcelheader" ng-show="detailCnmtFlag">CngnorName</td>
							<td class="rowcelheader" ng-show="detailCnmtFlag">CngneeName</td>
							<td class="rowcelheader" ng-show="detailCnmtFlag">CngneeCopy</td>
							<td class="rowcelheader" ng-show="detailCnmtFlag">Narr.</td>
							<!-- <td class="rowcelheader" ng-show="detailCnmtFlag">Cancel</td> -->
			      			
						</tr>
						
						<tr>
			            	<td class="rowcelwhite">{{cnmt.cnmtCode}}</td>
			            	<td class="rowcelwhite">{{cnmt.bCode}}</td>
			      			<td class="rowcelwhite">{{cnmt.cnmtDt | date:'d/M/y'}}</td>
			      			<td class="rowcelwhite">{{cnmt.cnmtFrmStnName | lowercase}}</td>
			      			<td class="rowcelwhite">{{cnmt.cnmtToStnName | lowercase}}</td>
			      			<td class="rowcelwhite">{{cnmt.cnmtActualWt / 1000 | number : 3}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtGuaranteeWt / 1000 | number : 3}}</td>
			      			<td class="rowcelwhite">{{cnmt.creationTS | date:'d/M/y'}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtNoOfPkg}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtFreight | number : 2}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtTOT | number : 2}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtBillAtName}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtDC}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtVOG}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.userCode}}</td>
			      			<td class="rowcelwhite cursor" ng-show="detailCnmtFlag" ng-repeat="invoiceNo in cnmt.cnmtInvoiceNoList | limitTo : 1 : 0" ng-click="showInvoiceList(cnmt.cnmtInvoiceNoList, cnmt.cnmtCode)">{{invoiceNo}}</td>
			      			<td class="rowcelwhite" ng-hide="cnmt.cnmtInvoiceNoList.length>0"></td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.custName | uppercase}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cngnorName | uppercase}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cngneeName | uppercase}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.isCCA}}</td>
			      			<td class="rowcelwhite" ng-show="detailCnmtFlag">{{cnmt.cnmtNarr}}</td>
			      			<td class="rowcelwhite" ng-show="{{cnmt.isCancel}}">CANCEL</td>
			      			
			      		</tr>
						
						
			   		</table>
			   	</td>
			   	
			   	<!-- Challan -->
			   	<td class="rowcel">
			   		<table>
			            <tr>
			                <td class="rowcelheader">ChlnNo</td>			                
			                <td class="rowcelheader">BrhCode</td>
			       			<td class="rowcelheader">Dt</td>
			       			<td class="rowcelheader">FStn</td>
			       			<td class="rowcelheader">TStn</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LryNo</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Awt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Cwt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Pkg</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">PayAt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">EDt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Ldng</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Ext</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Chln. Narr.</td>
			       			
			       			<td class="rowcelheader" ng-show="chlnDetectionFlag && detailChlnFlag">Detection Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnToolTaxFlag && detailChlnFlag">Tool Tax Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnHeightFlag && detailChlnFlag ">Height Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnUnionFlag && detailChlnFlag">Union Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnTwoPointFlag && detailChlnFlag">Two point Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnWeightmentFlag && detailChlnFlag">Weightment Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnCraneFlag && detailChlnFlag">Crane Amt.</td>
			       			<td class="rowcelheader" ng-show="chlnOthersFlag && detailChlnFlag">Others Amt.</td>
			       			
			       			
			       			
			       			
			       			<td class="rowcelheader">Ar</td>
			       			<td class="rowcelheader">ArDt</td>			       			
			       			<td class="rowcelheader">CAdv</td>
			       			<td class="rowcelheader">CBal</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">Ar. Narr.</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LAdv</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LAdvNo</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LAdvDt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LBal</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LBalNo</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LBalDt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LSup</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LSupNo</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">LSupDt</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">TrainRctNo</td>
			       			<td class="rowcelheader" ng-show="detailChlnFlag">TrainNo</td>
			            </tr>
			            
			            <tr ng-repeat="chln in cnmt.chlnList">
			                <td class="rowcelwhite">{{chln.chlnCode}}</td>			                
			                <td class="rowcelwhite">{{chln.bCode}}</td>
			       			<td class="rowcelwhite">{{chln.chlnDt | date:'d/M/y'}}</td>
			       			<td class="rowcelwhite">{{chln.chlnFrmStnName | lowercase}}</td>
			       			<td class="rowcelwhite">{{chln.chlnToStnName | lowercase}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnLryNo | uppercase}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnTotalWt / 1000 | number : 3}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnChgWt / 1000 | number : 3}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnNoOfPkg}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnPayAtName}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.creationTS | date:'d/M/y'}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnLoadingAmt}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnExtra}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnNarr}}</td>
			       			
			       			<td class="rowcelwhite" ng-show="chlnDetectionFlag && detailChlnFlag">{{chln.chlnDetection}}</td>
			       			<td class="rowcelwhite" ng-show="chlnToolTaxFlag && detailChlnFlag">{{chln.chlnToolTax}}</td>
			       			<td class="rowcelwhite" ng-show="chlnHeightFlag && detailChlnFlag">{{chln.chlnHeight}}</td>
			       			<td class="rowcelwhite" ng-show="chlnUnionFlag && detailChlnFlag">{{chln.chlnUnion}}</td>
			       			<td class="rowcelwhite" ng-show="chlnTwoPointFlag && detailChlnFlag">{{chln.chlnTwoPoint}}</td>
			       			<td class="rowcelwhite" ng-show="chlnWeightmentFlag && detailChlnFlag">{{chln.chlnWeightmentChg}}</td>
			       			<td class="rowcelwhite" ng-show="chlnCraneFlag && detailChlnFlag">{{chln.chlnCraneChg}}</td>
			       			<td class="rowcelwhite" ng-show="chlnOthersFlag && detailChlnFlag">{{chln.chlnOthers}}</td>			       			
			       			
			       			
			       			<td class="rowcelwhite cursor" ng-click="showArTbl(chln.ar, chln.chlnCode)">{{chln.ar.arCode}}</td>
			       			<td class="rowcelwhite">{{chln.ar.arDt | date:'d/M/y'}}</td>			       			
			       			<td class="rowcelwhite">{{chln.chlnAdvance}}</td>
			       			<td class="rowcelwhite">{{chln.chlnBalance}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.ar.arNarr}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.advTotByLhpv}}</td>
			       			<td class="rowcelwhite cursor" ng-show="detailChlnFlag" ng-repeat="lhpvAdv in chln.lhpvAdvList | limitTo : 1 : 0" ng-click="showLhpvAdvTbl(chln.lhpvAdvList, chln.chlnCode)">{{lhpvAdv.lsNo}}</td>
			       			<td class="rowcelwhite" ng-hide="chln.lhpvAdvList.length>0"></td>
			       			<td class="rowcelwhite cursor" ng-show="detailChlnFlag" ng-repeat="lhpvAdv in chln.lhpvAdvList | limitTo : 1 : 0" ng-click="showLhpvAdvTbl(chln.lhpvAdvList, chln.chlnCode)">{{lhpvAdv.lsDt | date : 'd/M/y'}}</td>
			       			<td class="rowcelwhite" ng-hide="chln.lhpvAdvList.length>0"></td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.balTotByLhpv}}</td>
			       			<td class="rowcelwhite cursor" ng-show="detailChlnFlag" ng-repeat="lhpvBal in chln.lhpvBalList | limitTo : 1 : 0" ng-click="showLhpvBalTbl(chln.lhpvBalList, chln.chlnCode)">{{lhpvBal.lsNo}}</td>
			       			<td class="rowcelwhite" ng-hide="chln.lhpvBalList.length>0"></td>
			       			<td class="rowcelwhite cursor" ng-show="detailChlnFlag" ng-repeat="lhpvBal in chln.lhpvBalList | limitTo : 1 : 0" ng-click="showLhpvBalTbl(chln.lhpvBalList, chln.chlnCode)">{{lhpvBal.lsDt | date : 'd/M/y'}}</td>
			       			<td class="rowcelwhite" ng-hide="chln.lhpvBalList.length>0"></td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.supTotByLhpv}}</td>
			       			<td class="rowcelwhite cursor" ng-show="detailChlnFlag" ng-repeat="lhpvSup in chln.lhpvSupList | limitTo : 1 : 0" ng-click="showLhpvSupTbl(chln.lhpvSupList, chln.chlnCode)">{{lhpvSup.lsNo}}</td>
			       			<td class="rowcelwhite" ng-hide="chln.lhpvSupList.length>0"></td>
			       			<td class="rowcelwhite cursor" ng-show="detailChlnFlag" ng-repeat="lhpvSup in chln.lhpvSupList | limitTo : 1 : 0" ng-click="showLhpvSupTbl(chln.lhpvSupList, chln.chlnCode)">{{lhpvSup.lsDt | date : 'd/M/y'}}</td>
			       			<td class="rowcelwhite" ng-hide="chln.lhpvSupList.length>0"></td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnRrNo}}</td>
			       			<td class="rowcelwhite" ng-show="detailChlnFlag">{{chln.chlnTrainNo}}</td>
			       			<td class="rowcelwhite" ng-show="{{ chln.cancel }}">Cancel</td>
			            </tr>
			   		</table>
				</td>
			 </tr>
           	
       	</table>
    </div>
    
    
    

	<div id ="showInvoiceDB" ng-hide="showInvoiceDBFlag" >
		<table>
	 	  <tr ng-repeat="invoice in cnmtInvoiceNoList">
		 	  <td class="align">{{invoice}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="billMrDB" ng-hide="billMrDBFlag">
		<input type="text" ng-model="filterTextbox5" placeholder="Search">
 	    <table>
	 	    <tr>
 	  	  	  <th></th>
 	  	  	  <th>Mr No</th>
 	  	  	  <th>Mr Date</th>
 	  	  </tr>
 	  	  <tr ng-repeat="mr in billMrList | filter:filterTextbox5">
		 	  <td><input type="radio" name="mr" value="{{ mr }}" ng-click="getMrDetail(mr)"></td>
              <td>{{mr.mrNo}}</td>
              <td>{{mr.mrDate | date : 'dd-MM-yyyy'}}</td>
          </tr>
      </table> 
	</div>
	
	<!-- Ar -->
	<div ng-show="arTblFlag">
		<table class="table">
 	    	<caption class="coltag tblrow">Arrival Report Detail {{chlnNoAr}}</caption> 
	 	    <tr>
 	  	  	  <th class="rowcelheader">ArNo</th>
 	  	  	  <th class="rowcelheader">BrCd</th>
 	  	  	  <th class="rowcelheader">ArDt</th>
 	  	  	  <th class="rowcelheader">RepDt</th>
 	  	  	  <th class="rowcelheader">Desc</th>
 	  	  	  <th class="rowcelheader">RcvWt</th>
 	  	  	  <th class="rowcelheader">Pkg</th>
 	  	  	  <th class="rowcelheader">ExtKm</th>
 	  	  	  <th class="rowcelheader">OvrHt</th>
 	  	  	  <th class="rowcelheader">Pnlty</th>
 	  	  	  <th class="rowcelheader">WtShrtg</th>
 	  	  	  <th class="rowcelheader">DrClaim</th>
 	  	  	  <th class="rowcelheader">Claim</th>
 	  	  	  <th class="rowcelheader">Unldng</th>
 	  	  	  <th class="rowcelheader">Detn</th>
 	  	  	  <th class="rowcelheader">LtDel</th>
 	  	  	  <th class="rowcelheader">LtAck</th>
 	  	  	  <th class="rowcelheader">RepDt</th>
 	  	  </tr>
 	  	  <tr>
		 	  <td class="rowcelwhite">{{ar.arCode}}</td>
		 	  <td class="rowcelwhite">{{ar.branchCode}}</td>
              <td class="rowcelwhite">{{ar.arDt | date : 'dd/MM/yyyy'}}</td>
              <td class="rowcelwhite">{{ar.arRepDt | date : 'dd/MM/yyyy'}}</td>
              <td class="rowcelwhite">{{ar.arDesc}}</td>
              <td class="rowcelwhite">{{ar.arRcvWt}}</td>
              <td class="rowcelwhite">{{ar.arPkg}}</td>
              <td class="rowcelwhite">{{ar.arExtKm}}</td>
              <td class="rowcelwhite">{{ar.arOvrHgt}}</td>
              <td class="rowcelwhite">{{ar.arPenalty}}</td>
              <td class="rowcelwhite">{{ar.arWtShrtg}}</td>
              <td class="rowcelwhite">{{ar.arDrRcvrWt}}</td>
              <td class="rowcelwhite">{{ar.arClaim}}</td>
              <td class="rowcelwhite">{{ar.arUnloading}}</td>
              <td class="rowcelwhite">{{ar.arDetention}}</td>
              <td class="rowcelwhite">{{ar.arLateDelivery}}</td>
              <td class="rowcelwhite">{{ar.arLateAck}}</td>
              <td class="rowcelwhite">{{ar.arRepDt  | date : 'dd/MM/yyyy'}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	
	<!-- LhpvAdvList -->
	<div ng-show="lhpvAdvTblFlag">
		<table class="table">
 	    	<caption class="coltag tblrow">Lhpv Advance Detail {{chlnNoAdv}}</caption> 
	 	    <tr>
 	  	  	  <th class="rowcelheader">LhpvNo</th>
 	  	  	  <th class="rowcelheader">LhpvDt</th>
 	  	  	  <th class="rowcelheader">Branch</th>
 	  	  	  <th class="rowcelheader">ChqNo</th>
 	  	  	  <th class="rowcelheader">Adv</th>
 	  	  	  <th class="rowcelheader">CashDisc</th>
 	  	  	  <th class="rowcelheader">Muns</th>
 	  	  	  <th class="rowcelheader">TDS</th>
 	  	  	  <th class="rowcelheader">Rcvr</th>
 	  	  </tr>
 	  	  <tr ng-repeat="lhpvAdv in lhpvAdvList">
		 	  <td class="rowcelwhite">{{lhpvAdv.lsNo}}</td>
              <td class="rowcelwhite">{{lhpvAdv.lsDt | date : 'dd/MM/yyyy'}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laBrName}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laChqNo}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laLryAdvP}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laCashDiscR}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laMunsR}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laTdsR}}</td>
              <td class="rowcelwhite">{{lhpvAdv.laTotRcvrAmt}}</td>
          </tr>
      </table> 
	</div>
	
	<!-- LhpvBalList -->
	<div ng-show="lhpvBalTblFlag">
		<table class="table">
 	    	<caption class="coltag tblrow">Lhpv Balance Detail {{chlnNoBal}}</caption> 
	 	    <tr>
 	  	  	  <th class="rowcelheader">LNo</th>
 	  	  	  <th class="rowcelheader">LhpvDt</th>
 	  	  	  <th class="rowcelheader">Branch</th>
 	  	  	  <th class="rowcelheader">ChqNo</th>
 	  	  	  <th class="rowcelheader">Bal</th>
 	  	 	  <th class="rowcelheader">CashDisc</th>
 	  	  	  <th class="rowcelheader">Muns</th>
 	  	  	  <th class="rowcelheader">TDS</th>
 	  	  	  <th class="rowcelheader">WtShrtg</th>
 	  	  	  <th class="rowcelheader">DrClaim</th>
 	  	  	  <th class="rowcelheader">LtDel</th>
 	  	  	  <th class="rowcelheader">LtAck</th>
 	  	  	  <th class="rowcelheader">ExtKm</th>
 	  	  	  <th class="rowcelheader">OvrHt</th>
 	  	  	  <th class="rowcelheader">Pnlty</th>
 	  	  	  <th class="rowcelheader">Misc</th>
 	  	  	  <th class="rowcelheader">UnPtDet</th>
 	  	  	  <th class="rowcelheader">Unld</th>
 	  	  	  <th class="rowcelheader">Rcvr</th>
 	  	  	  
 	  	  	  
 	  	  </tr>
 	  	  <tr ng-repeat="lhpvBal in lhpvBalList">
		 	  <td class="rowcelwhite">{{lhpvBal.lsNo}}</td>
              <td class="rowcelwhite">{{lhpvBal.lsDt | date : 'dd/MM/yyyy'}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbBrName}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbChqNo}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbLryBalP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbCashDiscR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbMunsR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbTdsR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbWtShrtgCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbDrRcvrWtCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbLateDelCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbLateAckCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthExtKmP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthOvrHgtP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthPnltyP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthMiscP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbUnpDetP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbUnLoadingP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbTotRcvrAmt}}</td>
          </tr>
      </table> 
	</div>
	
	<!-- LhpvSupList -->
	<div ng-show="lhpvSupTblFlag">
		<table class="table">
 	    	<caption class="coltag tblrow">Lhpv Supplementry Detail {{chlnNoSup}}</caption> 
	 	    <tr>
 	  	  	  <th class="rowcelheader">Lhpv No</th>
 	  	  	  <th class="rowcelheader">Lhpv Dt</th>
 	  	  	  
 	  	  	  <td class="rowcelwhite">{{lhpvBal.lbWtShrtgCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbDrRcvrWtCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbLateDelCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbLateAckCR}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthExtKmP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthOvrHgtP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthPnltyP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbOthMiscP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbUnpDetP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbUnLoadingP}}</td>
              <td class="rowcelwhite">{{lhpvBal.lbTotRcvrAmt}}</td>
 	  	  </tr>
 	  	  <tr ng-repeat="lhpvSup in lhpvSupList">
		 	  <td class="rowcelwhite">{{lhpvSup.lsNo}}</td>
              <td class="rowcelwhite">{{lhpvSup.lsDt | date : 'dd/MM/yyyy'}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspWtShrtgCR}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspDrRcvrWtCR}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspLateDelCR}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspLateAckCR}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspOthExtKmP}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspOthOvrHgtP}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspOthPnltyP}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspOthMiscP}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspUnpDetP}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspUnLoadingP}}</td>
              <td class="rowcelwhite">{{lhpvSup.lspTotRcvrAmt}}</td>
              
          </tr>
      </table> 
	</div>
</div>
<!--Cancel Ar -->
	<div ng-show="cancelArFlag">
		<table class="table">
 	    	<caption class="coltag tblrow">Arrival Report Detail </caption> 
	 	    <tr>
 	  	  	  <th class="rowcelheader">ArNo</th>
 	  	  	  <th class="rowcelheader">BrCd</th>
 	  	  	  <th class="rowcelheader">ArDt</th>
 	  	  	  <th class="rowcelheader">Edt</th>
 	  	  	 
 	  	  </tr>
 	  	  <tr>
		 	  <td class="rowcelwhite">{{ar.arCode}}</td>
		 	  <td class="rowcelwhite">{{ar.branchCode}}</td>
              <td class="rowcelwhite">{{ar.arDt | date : 'd/M/y'}}</td>
              <td class="rowcelwhite">{{ar.creationTS | date:'d/M/y'}}</td>
              <td class="rowcelwhite">Cancel</td>
              
          </tr>
      </table> 
	</div>

</div>