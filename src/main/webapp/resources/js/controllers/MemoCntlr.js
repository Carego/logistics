'use strict';

var app = angular.module('application');

app.controller('MemoCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	console.log("MemeCntlr starts.......");
	
	$scope.branchDBFlag = true;
	$scope.memoPrintFlag = true;
	$scope.finalSubmitDBFlag = true;
	
	$scope.memo = {};
	$scope.arList = [];
	$scope.memoArNo = "";
	$scope.memoNoToPrint = "";
	$scope.date = "";
	
	$scope.printMemoNo = "";
	
	$scope.printObject = {};
	$scope.printList = [];
	
	$scope.takePrintDB = function(){
		console.log("Enter memo no. to take print !");
		if($scope.memoNoToPrint === ""){
			$scope.alertToast("Enter memo no. to take print !");
			return ;
		}
		
		var response = $http.post($scope.projectName + '/takeMemoDt', $scope.memoNoToPrint);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				$scope.memoNoToPrint = "";
				$scope.printList = data.list;
				
				$scope.memo.branchName = data.branchName;
				$scope.printMemoNo = data.memoNo;
				$scope.date =  $filter('date')(data.memoDate, "dd-MM-yyyy");
				$scope.memo.fromBranch = data.fromBranch;
				$scope.memo.toBranch = data.toBranch;
				
				$scope.memoPrintFlag = false;
				$('div#memoPrint').dialog({
					autoOpen: false,
					modal:true,
					title: "Memo Print",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,			 
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
						$scope.arList = [];
						$scope.memoArNo = "";
						$scope.printMemoNo = "";
						$scope.printObject = {};
						$scope.printList = [];
						
						$scope.getMemoNo();
					}
				});
				$('div#memoPrint').dialog('open');
			}else{
				$scope.alertToast(data.msg);
				$scope.memoNoToPrint = "";
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /takeMemoDt");
		});
		console.log("Exit from takePrintDB()");
	}
	
	$scope.getMemoNo = function(){
		console.log("Enter into getMemoNo()");
		var response = $http.post($scope.projectName + '/getMemoNo');
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				$scope.memo.memoNo = data.memoNo;
				$scope.memo.memoDate = $filter('date')(new Date, "yyyy-MM-dd");
				$scope.date = $filter('date')(new Date, "dd-MM-yyyy");
				$scope.memo.branchName = $scope.currentBranch;
				$scope.memo.fromBranch = $scope.currentBranch;
				$scope.memo.toBranch = "";
			}else{
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getMemoNo");
		});
		console.log("Exit from getMemoNo()");
	}
	
	$scope.getBranchData = function(){
		console.log("Enter into getBranchData() ");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log(data.result);
				$scope.branchList = data.list;				
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers,config) {
			console.log("Error.....");
		});
		console.log("Exit from getBranchData() ");
	}
	
	$scope.saveBranch = function(branch){
		console.log("Enter into saveBranch()");
		$scope.memo.toBranch = branch.branchName;
		$('div#branchDB').dialog('close');
		console.log("Exit from saveBranch()");
	}
	
	$scope.OpenBranchDB = function(){
		console.log("Enter into OpenBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#branchDB').dialog('open');
		console.log("Exit from OpenBranchDB()");
	}
	
	$scope.getArDetail = function(){
		console.log("Enter into getArDetail()");
		
		for(var i=0; i<$scope.arList.length; i++){
			if($scope.arList[i].arCode == $scope.memoArNo){
				$scope.alertToast("AR no. already exists !");
				$scope.memoArNo = "";	
				return;
			}				
		}
		
		if($scope.memoArNo != ""){
			$("#memoArNoFetch").attr("disabled", "disabled");
			var response = $http.post($scope.projectName + '/getArDetailForMemo', $scope.memoArNo);
			response.success(function(data, status, headers, config){
				$("#memoArNoFetch").removeAttr("disabled");
				console.log(data);
				if(data.result === "success"){
					$scope.arList.push(data.list);
					$scope.memoArNo = "";							
				}else{
					$("#memoArNoFetch").remove("disabled");
					$scope.alertToast(data.msg);
					$scope.memoArNo = "";
				}
			});
			response.error(function(data, status, headers, config){
				$("#memoArNoFetch").removeAttr("disabled");
				console.log("Error in hitting /getArDetailForMemo");
			});
		}
		console.log("Exit from getArDetail()");
	}
	
	$scope.saveAr = function(ArForm){
		console.log("Enter into saveAr()");
		if(ArForm.$invalid){
			$scope.alertToast("Please enter remark !");
			return;
		}else{
			if($.trim($scope.memo.toBranch) === ""){
				$scope.alertToast("Select to branch !");
				return;
			}
		}
		
		$scope.finalSubmitDBFlag = false;
		$('div#finalSubmitDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Save AR",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#finalSubmitDB').dialog('open');
		console.log("Exit from saveAr()");
	}
	
	$scope.cancelAr = function(){
		console.log("Enter into cancelAr()");
		$scope.finalSubmitDBFlag = true;
		$('div#finalSubmitDB').dialog('close');
		console.log("Exit from cancelAr()");
	}
	
	$scope.submitFinalAr = function(ArForm){			
		console.log("Enter into submitFinalAr()");
		$scope.finalSubmitDBFlag = true;
		$('div#finalSubmitDB').dialog('close');
			var arList = {
					"memo"		:	$scope.memo,
					"arList"	:	$scope.arList
			}
			$("#finalSubmit").attr("disabled", "disabled");
			var response = $http.post($scope.projectName+ '/submitArMemo', arList);
			response.success(function(data, status, headers, config){
				console.log(data);
				$("#finalSubmit").removeAttr("disabled");
				if(data.result === "success"){
					$scope.alertToast(data.msg);
					
					$scope.printMemoNo = data.printMemoNo;					
					for(var i=0; i<$scope.arList.length; i++){
						var ar = $scope.arList[i];
						for(var j=0; j<ar.cnmtDt.length; j++){
							
							var cnmt = ar.cnmtDt[j];
							$scope.printObject.arCode = ar.arCode;
							$scope.printObject.arDt = ar.arDt;
							$scope.printObject.cnmtCode = cnmt.cnmtCode;
							$scope.printObject.cnmtDt = cnmt.cnmtDt;
							$scope.printObject.fromStation = cnmt.fromStation;
							$scope.printObject.toStation = cnmt.toStation;
							$scope.printObject.cnmtNoOfPkg = cnmt.cnmtNoOfPkg;
							$scope.printObject.cnmtActualWt = cnmt.cnmtActualWt
							$scope.printObject.arRemark = cnmt.arRemark;
							
							$scope.printList.push($scope.printObject);
							
							$scope.printObject = {};
						}
					}
					
					$scope.wantPrint();					
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data, status, headers, config){
				$("#finalSubmit").removeAttr("disabled");
				console.log("Error in hitting /submitArMemo");
			});		
		console.log("Exit from submitFinalAr()");
	}
	
	$scope.wantPrint = function(){
		console.log("Enter into wantPrint()");
		$scope.memoPrintFlag = false;
		$('div#memoPrint').dialog({
			autoOpen: false,
			modal:true,
			title: "Memo Print",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,			 
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
				$scope.arList = [];
				$scope.memoArNo = "";
				$scope.printMemoNo = "";
				$scope.printObject = {};
				$scope.printList = [];
				
				$scope.getMemoNo();
			}
		});
		$('div#memoPrint').dialog('open');
		console.log("Exit from wantPrint()");
	}
	
	$scope.deleteAr = function(index){
		console.log("Enter into deleteAr()");
		$scope.arList.splice(index, 1);
		console.log("Exit from deleteAr()");
	}
	
	$scope.cancelPrint = function(){
		$scope.memoPrintFlag = true;
		$('div#memoPrint').dialog('close');
		
		$scope.arList = [];
		$scope.memoArNo = "";
		$scope.printMemoNo = "";
		$scope.printObject = {};
		$scope.printList = [];
		
		$scope.getMemoNo();
	}
		
	$scope.printVs = function(){
		console.log("Enter into printVs()");
		$scope.memoPrintFlag = true;
		$('div#memoPrint').dialog('close');
		
		var content = document.getElementById("printTable").innerHTML;
		var mywindow = window.open('', '_blank');
	    mywindow.document.write('<html><body>');
	    mywindow.document.write(content);
	    mywindow.document.write('</body></html>');
	    mywindow.document.close();
	    mywindow.print();
	    mywindow.close();
	    
	    $scope.arList = [];
		$scope.memoArNo = "";
		$scope.printMemoNo = "";
		$scope.printObject = {};
		$scope.printList = [];
		
		$scope.getMemoNo();
		console.log("Exit from printVs()");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getMemoNo();
		$scope.getBranchData();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("MemeCntlr ends.......");
	 
}]);