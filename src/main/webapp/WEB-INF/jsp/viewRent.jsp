<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newRentForm" ng-submit=submit(newRentForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       			<label for="code">Branch</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMLandLordNameId" name ="rentMLandLordName" ng-model="rent.rentMLandLordName"  ng-click="openRentMstr()" ng-required="true" readOnly >
		       			<label for="code">Land Lord Name</label>	
		       		</div>
		      </div>
		 </form>
	</div>
	    <div id ="branchDB" ng-hide="branchDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
		    <div id ="rentDB" ng-hide="rentDBFlag">
		  <input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Id </th>
 	  	  	  <th>Land Lord Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="rentMstr in rentMstrList | filter:filterTextbox2">
		 	  <td><input type="radio"  name="rentMstr" value="{{ rentMstr }}" ng-model="rentMstr" ng-click="saveRentMstr(rentMstr)"></td>
              <td>{{selBranchId}}</td>
              <td>{{rentMstr.rentMLandLordName}}</td>
              
          </tr>
      </table> 
	</div>
	
			<form name="newRentForm" ng-submit=updateRentMstr(rent) ng-hide="updateForm" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMLandLordNameId" name ="rentMLandLordName" ng-model="rent.rentMLandLordName" ng-required="true" >
		       			<label for="code">Land Lord Name</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       			<input class="col s12 btn teal white-text" type ="button" name="rentAddress" id="rentAddressId" ng-click="openAddDB()" value="Address">
		       			<label for="code">Land Lord Address</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMRentPMId" name ="rentMRentPMName" ng-model="rent.rentMRentPM" ng-required="true" >
		       			<label for="code">Rent Per Month</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMAdvId" name ="rentMAdvName" ng-model="rent.rentMAdv" ng-required="true" >
		       			<label for="code">Advance</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMSecDepId" name ="rentMSecDepName" ng-model="rent.rentMSecDep" ng-required="true" >
		       			<label for="code">Security Deposite</label>	
		       		</div>
		    </div>
			
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="rentMAgreementFromId" name ="rentMAgreementFromName" ng-model="rent.rentMAgreementFrom" ng-required="true" >
		       			<label for="code">Agreement From Date</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="date" id="rentMAgreementToId" name ="rentMAgreementToName" ng-model="rent.rentMAgreementTo" ng-required="true" >
		       			<label for="code">Agreement To Date</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
				    		<select name="telType" id="telTypeId" ng-model="rent.rentMFor"  ng-required="true">
								<option value="office">Office</option>
								<option value="staff">Staff</option>
							</select>
							<label>Rent For</label>
					</div>
			</div>
			
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMPanNoId" name ="rentMPanNoName" ng-model="rent.rentMPanNo" >
		       			<label for="code">Pan No</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMBankNameId" name ="rentMBankName" ng-model="rent.rentMBankName" >
		       			<label for="code">Bank Name</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMIFSCId" name ="rentMIFSCName" ng-model="rent.rentMIFSC" >
		       			<label for="code">IFSC Code</label>	
		       		</div>
		    </div>
		    
		    		<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentBankBranch" name ="rentBankBranch" ng-model="rent.rentBankBranch" >
		       			<label for="code">Rent Bank Branch</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentAcNo" name ="rentAcNo" ng-model="rent.rentAcNo" >
		       			<label for="code">Rent Account No</label>	
		       		</div>
		    </div>
		    
		    

		    <div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
		</form>
		
		<div id="addDB" ng-hide = "rentAddress">
		<form name = "addForm" ng-submit="saveAdd()">
			<table>
				<tr>
					<td>Address *</td>
					<td><input type ="text" id="addressId" name ="address" ng-model="rent.address.completeAdd" ng-required="true" ng-minlength="3" ng-minlength="255"></td>
				</tr>
				
				<tr>
					<td>Address City *</td>
					<td><input type ="text" id="addCityId" name ="addCity" ng-model="rent.address.addCity" ng-required="true" ng-minlength="3" ng-minlength="15"></td>
				</tr>
				
				<tr>
					<td>Address State *</td>
					<td><input type ="text" id="addStateId" name ="addState" ng-model="rent.address.addState" ng-required="true" ng-minlength="3" ng-minlength="40"></td>
				</tr>
				
				<tr>
					<td>Address Pin *</td>
					<td><input type ="text" id="addPinId" name ="addPin" ng-model="rent.address.addPin" ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
				</tr>
				
				<tr>
					<td><input type="submit"  value="Submit" ></td>
				</tr>
			</table>
		</form>
	</div>
</div>		 