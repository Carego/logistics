package com.mylogistics.model.dataintegration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profitnlossdemo")
public class ProfitNLossDemo {
	
	@Id
	@Column(name="pnlId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pnlId;
	
	@Column(name="pnlName")
	private String pnlName;
	
	@Column(name="pnlFaCode")
	private String pnlFaCode;

	public int getPnlId() {
		return pnlId;
	}

	public void setPnlId(int pnlId) {
		this.pnlId = pnlId;
	}

	public String getPnlName() {
		return pnlName;
	}

	public void setPnlName(String pnlName) {
		this.pnlName = pnlName;
	}

	public String getPnlFaCode() {
		return pnlFaCode;
	}

	public void setPnlFaCode(String pnlFaCode) {
		this.pnlFaCode = pnlFaCode;
	}
}