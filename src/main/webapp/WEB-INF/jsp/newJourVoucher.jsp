<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
			ng-class = "{'form-error': newVoucherForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row">
		     		<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Code</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.branchFaCode.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch is required.</span>
							</div>		
		       		</div>

		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Name</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.branchName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch name is required.</span>
							</div>			
		       		</div>
		       		
		       			<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="vs.cashStmtStatus.cssDt" ng-blur="isInFY()" ng-required="true">
		       			<label for="code" style="color:black;">Date</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.dateTemp.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Date is required.</span>
							</div>	
		       		</div>
		       		
		       		<div class="col s1 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Sheet No</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.sheetNo.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Sheet no. is required.</span>
							</div>		
		       		</div>
		       		 <div class="col s3 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Voucher Type</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.voucherType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Voucher is required.</span>
							</div>		
		       		</div>
		    </div>
		    
		    <div class="row">
	     	       	
		       	<div class="col s6 input-field">
						<input type ="button" value="CERDIT AMOUNT" ng-click="openCreditAmt()">
						<input type ="button" value="DEBIT AMOUNT" ng-click="openDebitAmt()">
				 </div>
				  
	   		</div>
	
	   		 <div class="row">
				<div ng-show="jvList.length > 0">
					<table class="tblrow">
		   				   <caption class="coltag tblrow">Journal Voucher Details</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">S.No.</th>
	                        <th class="colclr">FA CODE</th>
	                        <th class="colclr">CREDIT AMT</th>
	                        <th class="colclr">DEBIT AMT</th>
	                        <th class="colclr">DESCRIPTION</th>
	                        <th class="colclr">ACTION</th>                      
	                     </tr>
	                     <tr class="tbl" ng-repeat="jvM in jvList">
	                     	<td class="rowcel">{{$index + 1}}</td>
	                        <td class="rowcel">{{jvM.faCode}}</td>
	                        <td class="rowcel">{{jvM.crAmt}}</td>
	                        <td class="rowcel">{{jvM.dbAmt}}</td>
	                        <td class="rowcel">{{jvM.desc}}</td>
	                        <td class="rowcel">
	                        	<input type="button" value="REMOVE" ng-click="removeJV($index,jvM)"/>
	                        </td>
	                 	</tr>
	                 	<tr class="tbl" >							
							<td class="rowcel" colspan="2">Total Amount</td>
							<td class="rowcel">{{crAmtSum}}</td>
							<td class="rowcel">{{dbAmtSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>							
						</tr>
	                 </table>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id = "voucherSubmit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
   
   <div id="openCreditDB" ng-hide="openCreditDBFlag">
   		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="crAmtForm" ng-submit="submitCRAmt(crAmtForm)"
   			ng-class = "{'form-error': crAmtForm.$invalid && isCRFormSubmitted}" novalidate = "novalidate">
			 <div class="row">
					<table style = "background-color: #F2FAEF!important;">
						<tr>
							<td style="color:black;">FA Code *</td>
							<td><input class="validate" type ="text" name="faCodeName" id="faCodeId" ng-model="jv.faCode" ng-keyup="getAllFaCode($event.keyCode)" ng-required="true">
								<div class = "text-left errorMargin" ng-messages = "crAmtForm.faCodeName.$error" ng-if = "isCRFormSubmitted">
									<span ng-message = "required">*  FA Code is required.</span>
								</div>
							</td>
						</tr>
						<tr>
							<td style="color:black;">Chq No</td>
							<td><input class="validate" type ="text" name="chqNoName" id="chqNoId" ng-model="jv.chqNo" ng-click="openChqNoDB()" readonly>
							</td>
						</tr>
						<tr>
							<td style="color:black;">Credit Amt *</td>
							<td><input class="validate" type ="number" name="crAmtName" id="crAmtId" ng-model="jv.crAmt" step="0.001" min="0.000" ng-required="true">
								<div class = "text-left errorMargin" ng-messages = "crAmtForm.crAmtName.$error" ng-if = "isCRFormSubmitted">
										<span ng-message = "required">*  Credit amount is required.</span>
									</div>
							</td>
						</tr>
						<tr>
							<td style="color:black;">Description *</td>
							<td>
 								<textarea id="descId" name ="descName" ng-model="jv.desc" ng-required="true"></textarea>
 								<div class = "text-left errorMargin" ng-messages = "crAmtForm.descName.$error" ng-if = "isCRFormSubmitted">
										<span ng-message = "required">*  Description is required.</span>
									</div>
 							</td>
						</tr>
					</table>
		     </div>
		     <div class="row">
				    <div class="input-field col s12 center">
						<input class="validate" type ="submit" value="submit">
					</div>
		     </div>
	     </form>
   </div>
   
    <div id="openDebitDB" ng-hide="openDebitDBFlag">
   		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="dbAmtForm" ng-submit="submitDBAmt(dbAmtForm)"
   			ng-class = "{'form-error': dbAmtForm.$invalid && isDRFormSubmitted}" novalidate = "novalidate">
			 <div class="row">
					<table style = "background-color: #F2FAEF!important;">
						<tr>
							<td style="color:black;">FA Code *</td>
							<td><input class="validate" type ="text" name="faCodeName" id="faCodeId" ng-model="jv.faCode" ng-keyup="getAllFaCode($event.keyCode)" ng-required="true">
								<div class = "text-left errorMargin" ng-messages = "dbAmtForm.faCodeName.$error" ng-if = "isDRFormSubmitted">
										<span ng-message = "required">*  FA Code is required.</span>
									</div>
							</td>
						</tr>
						<tr>
							<td style="color:black;">Chq No</td>
							<td><input class="validate" type ="text" name="chqNoName" id="chqNoId" ng-model="jv.chqNo" ng-click="openChqNoDB()" readonly>
							</td>
						</tr>
						<tr>
							<td style="color:black;">Debit Amt</td>
							<td><input class="validate" type ="number" name="dbAmtName" id="dbAmtId" ng-model="jv.dbAmt" step="0.001" min="0.000" ng-required="true">
								<div class = "text-left errorMargin" ng-messages = "dbAmtForm.dbAmtName.$error" ng-if = "isDRFormSubmitted">
										<span ng-message = "required">*  Debit amount is required.</span>
									</div>
							</td>
						</tr>
						<tr>
							<td style="color:black;">Description</td>
							<td>
 								<textarea id="descId" name ="descName" ng-model="jv.desc" ng-required="true"></textarea>
 								<div class = "text-left errorMargin" ng-messages = "dbAmtForm.descName.$error" ng-if = "isDRFormSubmitted">
										<span ng-message = "required">*  Description is required.</span>
									</div>
 							</td>
						</tr>
					</table>
		     </div>
		     <div class="row">
				    <div class="input-field col s12 center">
						<input class="validate" type ="submit" value="submit">
					</div>
		     </div>
	     </form>
   </div>
   
   <div id ="faCodeDB" ng-hide="faCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox3" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>FA Code</th>
 	  	 	  <th>Name</th>
 	  	 	  <th>Type</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="faMstr in faMList | filter:filterTextbox3">
		 	  <td><input type="radio"  name="faMstr"   value="{{ faMstr }}" ng-model="faMstrId" ng-click="saveFACode(faMstr)"></td>
              <td>{{faMstr.faMfaCode}}</td>
              <td>{{faMstr.faMfaName}}</td>
              <td>{{faMstr.faMfaType}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="openChqNoDB" ng-hide="openChqNoDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox4" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Chq No</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="chqNo in chqNoList | filter:filterTextbox4">
		 	  <td><input type="radio"  name="chqNoList"  ng-click="saveChqNo(chqNo)"></td>
              <td>{{chqNo}}</td>
          </tr>
      </table> 
	</div>  
   
   	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
			
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
 </div>   