package com.mylogistics.controller;

//import java.util.Date;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.BranchOldDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.HBOptr_NotificationDAO;
import com.mylogistics.DAO.HOSLastDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchOld;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.HBOptr_Notification;
import com.mylogistics.model.HOSLast;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayBranchCntlr {
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private HBOptr_NotificationDAO hBOptr_NotificationDAO;
	
	@Autowired
	private HOSLastDAO hOSLastDAO;
	
	@Autowired
	private BranchMUStatsDAO branchMUStatsDAO;
	
	@Autowired
	private BranchOldDAO branchOldDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@RequestMapping(value="/displayBranchForSuperAdmin",method = RequestMethod.POST)
	public @ResponseBody Object displayBranchForSuperAdmin(){
		
		List<Branch> branchList = branchDAO.getAllBranchForSuperAdmin();
		Map<String,Object> map = new HashMap<String,Object>();
		
		if (!(branchList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",branchList);
  		}else {
  			map.put(ConstantsValues.RESULT, "No more branch to show...");
		}
		return map;	
	}
	
	@RequestMapping(value = "/updateIsViewBranch", method = RequestMethod.POST)
    public @ResponseBody Object updateIsViewBranch(@RequestBody String selection) {

		System.out.println("Entered into BranchCntlr function");
        String contids[] = selection.split(",");
        for(int i=0;i<contids.length;i++){
        System.out.println("Entered into BranchCntlr function"+contids[i]);}
        int temp=branchDAO.updateBranchisViewTrue(contids);
        Map<String, Object> map = new HashMap<String, Object>();
        if(temp>0){
            map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }
        else{
            map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }
        return map;
    }
	
	@RequestMapping(value = "/saveEdittedBranch", method = RequestMethod.POST)
    public @ResponseBody Object saveEdittedBranch(@RequestBody Branch branch){
        System.out.println("Entered into saveEdittedBranch function---->");
        Map<String,String> map = new HashMap<String, String>();
        
        int oldBranchId = branch.getBranchId();
        List<Branch> branchlist = branchDAO.getOldBranch(oldBranchId);
        
        if (!(branchlist.isEmpty())) {
       	 	BranchOld branchOld = new BranchOld();
       	 
       	 	branchOld.setBranchId(branchlist.get(0).getBranchId());
       	 	branchOld.setBranchName(branchlist.get(0).getBranchName());
       	 	branchOld.setBranchCode(branchlist.get(0).getBranchCode());
       	 	branchOld.setbCode(branchlist.get(0).getbCode());
       	 	branchOld.setBranchAdd(branchlist.get(0).getBranchAdd());
       	 	branchOld.setBranchCity(branchlist.get(0).getBranchCity());
       	 	branchOld.setBranchState(branchlist.get(0).getBranchState());
       		branchOld.setBranchPin(branchlist.get(0).getBranchPin());
       		branchOld.setBranchAreaMngr(branchlist.get(0).getBranchAreaMngr());
       		branchOld.setBranchCashier(branchlist.get(0).getBranchCashier());
       		branchOld.setBranchCloseDt(branchlist.get(0).getBranchCloseDt());
       		branchOld.setBranchDirector(branchlist.get(0).getBranchDirector());
       		branchOld.setBranchEmailId(branchlist.get(0).getBranchEmailId());
       		branchOld.setBranchFax(branchlist.get(0).getBranchFax());
       		branchOld.setBranchMarketing(branchlist.get(0).getBranchMarketing());
       		branchOld.setBranchMarketingHD(branchlist.get(0).getBranchMarketingHD());
       		branchOld.setBranchMngr(branchlist.get(0).getBranchMngr());
       		branchOld.setBranchOpenDt(branchlist.get(0).getBranchOpenDt());
       		branchOld.setBranchOutStandingHD(branchlist.get(0).getBranchOutStandingHD());
       		branchOld.setBranchPhone(branchlist.get(0).getBranchPhone());
       		branchOld.setBranchRegionalMngr(branchlist.get(0).getBranchRegionalMngr());
       		branchOld.setBranchStationCode(branchlist.get(0).getBranchStationCode());
       		branchOld.setBranchTraffic(branchlist.get(0).getBranchTraffic());
       		branchOld.setBranchWebsite(branchlist.get(0).getBranchWebsite());
       		branchOld.setIsNCR(branchlist.get(0).getIsNCR());
       		branchOld.setView(branchlist.get(0).isView());
       		branchOld.setUserCode(branchlist.get(0).getUserCode());
       		branchOld.setIsOpen(branchlist.get(0).getIsOpen());
       	 
       	 	int tmp = branchOldDAO.saveBranchOld(branchOld);
       	 	if (tmp>=0){
	        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else {
				  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
       }else{
       	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
       }
       
        Date date = new Date(new java.util.Date().getTime());      
        if(branch.getIsOpen().equals("no")){
        	branch.setBranchCloseDt(date);
        }
        
        int temp=branchDAO.updateBranchToDB(branch);
        if (temp>=0) {
        	
        	FAMaster fAMaster = new FAMaster();
        	List<FAMaster> fAMasterlist = faMasterDAO.retrieveFAMaster(branch.getBranchFaCode());
        	  		
    		if (!(fAMasterlist.isEmpty())) {
    			fAMaster = fAMasterlist.get(0);
    			fAMaster.setFaMfaName(branch.getBranchName());
    		    int tmp = faMasterDAO.updateFaMaster(fAMaster);
    		    if(tmp >= 0){
    		    	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
    		    }else{
    		    	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
    		    }
    		}else{
    		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
    		}
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
    	return map;
    }
	

	@RequestMapping(value = "/getOperatorCode", method = RequestMethod.POST)
    public @ResponseBody Object getOperatorCode(){
        System.out.println("enter into getOperatorCode function");
       // List<Employee> empList = employeeDAO.getAll_HB_Employee("gurgaon110011");
        List<User> userList = userDAO.getHBOperator("1");
        //System.out.println("head branch operator---->"+userList.get(0).getUserName());
        Map<String,Object> map = new HashMap<String, Object>();
        if(!userList.isEmpty()){
        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        	map.put("empList", userList);
        }else{
        	map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }   	
    	return map;
    }
	
	@RequestMapping(value = "/saveBranchStock", method = RequestMethod.POST)
    public @ResponseBody Object saveBranchStock(@RequestBody HBOptr_Notification hBON){
        System.out.println("enter into saveBranchStock function ===>"+hBON.getOperatorCode());
        User currentUser = (User) httpSession.getAttribute("currentUser");
        hBON.setbCode(currentUser.getUserBranchCode());
        hBON.setUserCode(currentUser.getUserCode());
        hBON.setView(false);
        Date dispatchDate = new Date(new java.util.Date().getTime());
        hBON.setDispatchDate(dispatchDate);
        /*String branchCode = hBON.getDisBranchCode();
		String dispatchCode = branchCode.substring(0, 4);
		dispatchCode = dispatchCode + "kaushik";
		hBON.setDispatchCode(dispatchCode);*/
        int temp = hBOptr_NotificationDAO.saveHBON(hBON);
        Map<String,Object> map = new HashMap<String, Object>();
        if(temp==1){
        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }else{
        	map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }  	
    	return map;
    }
	
	@RequestMapping(value = "/getLastHOSLast", method = RequestMethod.POST)
    public @ResponseBody Object getLastHOSLast(){
		
		System.out.println("Entered into getLastHOSLast function in controller---- ");
		Map<String,Object> map = new HashMap<String,Object>();
	    List<HOSLast> hOSLastList = hOSLastDAO.getLastArrival();
	    
	    if(!hOSLastList.isEmpty()){
	    	System.out.println("Value of hOSLast chln in controller---->"+hOSLastList.get(0).gethOSChln());
		    System.out.println("Value of hOSLast chln--->"+hOSLastList.get(0).gethOSCnmt());
		    System.out.println("Value of hOSLast chln--->"+hOSLastList.get(0).gethOSSedr());
        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        	HOSLast hOSLast = hOSLastList.get(0);
        	map.put("hOSL", hOSLast);
        }else{
        	map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }  
		return map;
	}
	
	@RequestMapping(value = "/submitHOSLast", method = RequestMethod.POST)
       public @ResponseBody Object submitHOSLast(@RequestBody HOSLast hosLast){ 
	   System.out.println("Entered into submitHOSLast function in controller---- ");
	   System.out.println("value of hOSCnmtDisOrRec---- "+hosLast.gethOSCnmtDisOrRec());
	   System.out.println("value of hOSCnmtDisOrRec---- "+hosLast.gethOSChlnDisOrRec());
	   System.out.println("value of hOSCnmtDisOrRec---- "+hosLast.gethOSSedrDisOrRec());
	   System.out.println("value of hOSCnmtDisOrRec---- "+hosLast.gethOSChlnDaysLast());	
	   
	   double monUsgCNMT = getAvgMonUsgCNMT();
	   double cnmtDaysLast = (30/monUsgCNMT)*hosLast.gethOSCnmt();
	   hosLast.sethOSCnmtDaysLast((int)cnmtDaysLast);
	   
	   double monUsgCHLN = getAvgMonUsgCHLN();
	   double chlnDaysLast = (30/monUsgCHLN)*hosLast.gethOSChln();
	   hosLast.sethOSChlnDaysLast((int)chlnDaysLast);
	   
	   double monUsgSEDR = getAvgMonUsgSEDR();
	   double sedrDaysLast = (30/monUsgSEDR)*hosLast.gethOSSedr();
	   hosLast.sethOSSedrDaysLast((int)sedrDaysLast);
	   
       Map<String,String> map = new HashMap<String,String>();
       
       User currentUser = (User)httpSession.getAttribute("currentUser");
       hosLast.setUserCode(currentUser.getUserCode());
       hosLast.setbCode(currentUser.getUserBranchCode());
       
       int temp=hOSLastDAO.saveHosLastToDB(hosLast);
    	if (temp>=0) {
    		  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	
	public double getAvgMonUsgCNMT(){
		System.out.println("enter into getAvgMonUsgCNMT function");
		double avgMonUsgCNMT = 0;
		avgMonUsgCNMT = branchMUStatsDAO.getAvgMonUsg("cnmt");
		return avgMonUsgCNMT;
	}
	
	
	public double getAvgMonUsgSEDR(){
		System.out.println("enter into getAvgMonUsgSEDR function");
		double avgMonUsgSEDR = 0;
		avgMonUsgSEDR = branchMUStatsDAO.getAvgMonUsg("sedr");
		return avgMonUsgSEDR;
	}
	
	public double getAvgMonUsgCHLN(){
		System.out.println("enter into getAvgMonUsgCNMT function");
		double avgMonUsgCHLN = 0;
		avgMonUsgCHLN = branchMUStatsDAO.getAvgMonUsg("chln");
		return avgMonUsgCHLN;
	}
	
	@RequestMapping(value = "/getModifiedBranchDetail", method = RequestMethod.POST)
	  public @ResponseBody Object getModifiedBranchDetail(@RequestBody String branchCode){
		 System.out.println("Entered into getModifiedBranchDetail function---"+branchCode);
	   
		List<BranchOld> branchOldList = branchOldDAO.getModifiedBranch(branchCode);
		  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(branchOldList.isEmpty())) {
			
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("list",branchOldList);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	   }
	
	@RequestMapping(value = "/getBranchByBrCodeTemp", method = RequestMethod.POST)
	  public @ResponseBody Object getBranchByBrCodeTemp(@RequestBody String branchCodeTemp){
		    	   	   
	    Branch branch = new Branch();
		List<Branch> list = branchDAO.getBranchByBrCodeTemp(branchCodeTemp);
				  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			branch = list.get(0);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("branch",branch);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	 }
}

