package com.mylogistics.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javassist.expr.Cast;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.model.BankCS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.services.CashStmtService;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CashStmtCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private CashStmtDAO cashStmtDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public static Logger logger = Logger.getLogger(CashStmtCntlr.class);
	
	@RequestMapping(value = "/getBrhFrCS" , method = RequestMethod.POST)  
	public @ResponseBody Object getBrhFrCS() {  
		System.out.println("Enter into getBrhFrCS---->");
		Map<String, Object> map = new HashMap<>();	
		List<Branch> brhList = branchDAO.getAllActiveBranches();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitCStmt" , method = RequestMethod.POST)  
	public @ResponseBody Object submitCStmt(@RequestBody Map<String,Object> clientMap){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<>();
		
		// Check User is logged in OR not
		if(user == null){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please login !");
			logger.info("Login Failed !");
			return resultMap;
		}
		
		logger.info("UserID : "+user.getUserId()+" : Enter into /submitCStmt : initParam = "+clientMap);		
		Session session = this.sessionFactory.openSession();
			
		int brhId = (int) clientMap.get("brhId");
		
		ArrayList<Integer> brhList=new ArrayList<>();
		Date date = Date.valueOf((String) clientMap.get("date")); 
		List<BankCS> bankCSs=cashStmtStatusDAO.getBankCsForGurgaon();
				System.out.println("Size "+bankCSs.size());
				BankCS bank=bankCSs.get(bankCSs.size()-1);
				System.out.println("*************** Bank Cs Date   "+bank.getBcsDt());
				int val=bank.getBcsDt().compareTo(date);
				if(val>=0)
				{
					if(brhId!=1)
					{
						brhList.add(1);
						brhList.add(brhId);
					}
					
				}
		
		              //clientMap.get("date");
		
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getCssByDt(session, user.getUserId(), String.valueOf(brhId) , date);	
		
		if(cashStmtStatus == null){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "CashStmtStatus not found !");
			logger.info("UserID = "+user.getUserId()+" : CashStmt not found !");
			return resultMap;
		}else{
			// Bank CS List
			//List<BankCS> bcsList = cashStmtStatusDAO.getBankCsByDt(session, user.getUserId(), date, brhId);
			List<BankCS> bcsList;
			if(brhList.isEmpty())
			{
				bcsList = cashStmtStatusDAO.getBankCsByDt(session, user.getUserId(), date, brhId);
			}
			else
			{
				bcsList = cashStmtStatusDAO.getBankCsByDt(session, user.getUserId(), date, brhList);
					
			}
			
			// Branch Name
			String brhName = branchDAO.getBrNameByBrCode(cashStmtStatus.getbCode());
			
			// CashStmtList
			List<CashStmt> csList = cashStmtStatus.getCashStmtList();	
			System.err.println("CsList Size = "+csList.size());
			
			// List For FaMaster Which are used in CashStmt With FaCode and FaName
			List<Map<String,String>> acHdList = new ArrayList<>();
			
			// If Branch is not Gurgaon, Fetch All Bank List of Gurgaon and add this list to Above bcsList (Not Repeated)
			if(brhId != 1){
				List<BankCS> hoBcsList = new ArrayList<>();
				hoBcsList = cashStmtStatusDAO.getBankCsByDt(session, user.getUserId(), date, 1);
				for(int i=0;i<hoBcsList.size();i++){
					if(! csList.isEmpty()){
						for(int j=0;j<csList.size();j++){
							if(csList.get(j).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) && csList.get(j).getCsFaCode().equalsIgnoreCase(hoBcsList.get(i).getBcsBankCode())){
								if(!bcsList.isEmpty()){
									boolean duplicate = false;
									for(int k=0;k<bcsList.size();k++){
										if(bcsList.get(k).getBcsBankCode().equalsIgnoreCase(hoBcsList.get(i).getBcsBankCode())){
											duplicate = true;
											break;
										}
									}
									if(duplicate == false){
										bcsList.add(hoBcsList.get(i));
									}
								}else{
									bcsList.add(hoBcsList.get(i));
								}
								break;
							}
						}
					}
				}
			}
			
			
			//Lhpv Duplicacy Remove (Only Bank Entry --- Always With Cheque Entry)
			for(int i=0;i<csList.size();i++){
				if(csList.get(i).getCsVouchType().equalsIgnoreCase("BANK") && csList.get(i).getCsType().equalsIgnoreCase("LHPV PAYMENT") && csList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
					int vhNo = csList.get(i).getCsVouchNo();
					int count = 0;
					System.err.println("Vouch NO = "+vhNo);
					for(int j=0;j<csList.size();j++){
						System.err.println("Second Vouch No = "+csList.get(j).getCsVouchNo());
						if(csList.get(j).getCsVouchNo() == vhNo && csList.get(j).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
							count = count + 1;
							System.err.println("$$$$ DUP");
						}
						
						
						
						if(count > 1){
							System.out.println("i m in duplicate lhpv remove");
							count = count -1;
							csList.remove(j);
							j = j-1;
						}
						break;
					}
					
				}
			}
			
			
			if(!csList.isEmpty()){
				
				// Fetch All Famaster
				List<FAMaster> faMList = faMasterDAO.getAllFAM();	
				
				// List For Bill CashStmt
				List<CashStmt> blCsList = new ArrayList<CashStmt>();
				
				// List For MR CashStmt
				List<Map<String,Object>> mrCsList = new ArrayList<>();
				
				// List Form LHPV CashStmt
				List<Map<String,Object>> lhpvCsList = new ArrayList<>();
								
				if(! faMList.isEmpty()){
					
					for(int i=0;i<csList.size();i++){
						//  Find All FaCode With FaName which are used in CashStmt
						Map<String,String> acHdMap = new HashMap<>();
						for(int j=0;j<faMList.size();j++){
							if(csList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
								acHdMap.put("faCode",csList.get(i).getCsFaCode());
								acHdMap.put("faName",faMList.get(j).getFaMfaName());
								// Set FaMaster to List
								acHdList.add(acHdMap);
								break;
							}
						}
						
						// If CashStmt's CsType is BILL, this CashStmt is added to BillCashStmt List
						if(csList.get(i).getCsType().equalsIgnoreCase("BILL")){
							if(!blCsList.isEmpty()){
								boolean isUpdate = false;
								for(int k=0;k<blCsList.size();k++){
									// If More than One CashStmt has same FaCode then Total Amt of these records are consider to ONE (Else block will not work )
									//if(blCsList.get(k).getCsFaCode().equalsIgnoreCase(csList.get(i).getCsFaCode()))
									if(blCsList.get(k).getCsFaCode().equalsIgnoreCase(csList.get(i).getCsFaCode()) && blCsList.get(k).getCsDrCr()==csList.get(i).getCsDrCr()){
										blCsList.get(k).setCsAmt(blCsList.get(k).getCsAmt() + csList.get(i).getCsAmt());
										isUpdate = true;
										break;
									}
								}
								
								if(!isUpdate){
									// CsTvNo is replaced by CS Date
									csList.get(i).setCsTvNo(String.valueOf(csList.get(i).getCsDt()));
									blCsList.add(csList.get(i));
								}
							}else{
								// CsTvNo is replaced by CS Date
								csList.get(i).setCsTvNo(String.valueOf(csList.get(i).getCsDt()));
								blCsList.add(csList.get(i));
							}
						}
						
						// IF CashStmt's CsTyp is Money Receipt and CsVouchType is BANK, and FaCode Constains 07 (Bank Code)
						if(csList.get(i).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) && 
								csList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
								csList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
							
							if(! mrCsList.isEmpty()){
								boolean mrUpdate = false;
								for(int k=0;k<mrCsList.size();k++){
									Map<String,Object> mrBnkMap = mrCsList.get(k);
									if(String.valueOf(mrBnkMap.get("bankCode")).equalsIgnoreCase(csList.get(i).getCsFaCode())){
										System.out.println("*********************** 1");
										List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list");
										int vouchNo = csList.get(i).getCsVouchNo(); 
										for(int l=0;l<csList.size();l++){
											if(csList.get(l).getCsVouchNo() == vouchNo){
												
												boolean subCount = false;
												for(int m=0;m<mrBnkList.size();m++){
													System.out.println("*********************** 2");
													if(mrBnkList.get(m).getCsFaCode().equalsIgnoreCase(csList.get(l).getCsFaCode()) && 
															mrBnkList.get(m).getCsDrCr() == csList.get(l).getCsDrCr()){
														mrBnkList.get(m).setCsAmt(mrBnkList.get(m).getCsAmt() + csList.get(l).getCsAmt());
														System.out.println("*********************** 44 == "+csList.get(l).getCsFaCode());
														subCount = true;
														break;
													}
												}
												
												if(!subCount){
													System.out.println("*********************** 4 == "+csList.get(l).getCsFaCode());
													csList.get(l).setCsTvNo(String.valueOf(csList.get(l).getCsDt()));
													csList.get(l).setCsVouchNo(mrBnkList.get(0).getCsVouchNo());
													mrBnkList.add(csList.get(l));
												}
												
											}
										}
										
										mrBnkMap.remove("list");
										mrBnkMap.put("list",mrBnkList);
										
										mrCsList.remove(k);
										mrCsList.add(k, mrBnkMap);
										
										mrUpdate = true;
										break;
									}
								}
								
								if(!mrUpdate){
									// IF new CashStmt, MR CashStmtList takes Map Object With bankCode and CashStmt 
									System.out.println("*********************** 1 *******");
									Map<String,Object> mrBnkMap = new HashMap<String, Object>();
									List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
									
									int vouchNo = csList.get(i).getCsVouchNo(); 
									mrBnkMap.put("bankCode", csList.get(i).getCsFaCode());
									for(int k=0;k<csList.size();k++){
										if(csList.get(k).getCsVouchNo() == vouchNo){
											csList.get(k).setCsTvNo(String.valueOf(csList.get(k).getCsDt()));
											mrBnkList.add(csList.get(k));
										}
									}
									mrBnkMap.put("list",mrBnkList);
									
									mrCsList.add(mrBnkMap);
								}
							}else{
								// MR CashStmtList takes Map Object With bankCode and CashStmt  
								Map<String,Object> mrBnkMap = new HashMap<String, Object>();
								List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
								
								int vouchNo = csList.get(i).getCsVouchNo(); 
								mrBnkMap.put("bankCode", csList.get(i).getCsFaCode());
								for(int k=0;k<csList.size();k++){
									if(csList.get(k).getCsVouchNo() == vouchNo){
										csList.get(k).setCsTvNo(String.valueOf(csList.get(k).getCsDt()));
										mrBnkList.add(csList.get(k));
									}
								}
								mrBnkMap.put("list",mrBnkList);
								
								mrCsList.add(mrBnkMap);
							}
						}
						
						
						
						if(csList.get(i).getCsType().equalsIgnoreCase("LHPV PAYMENT") && 
							 csList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") &&
							 csList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
							
							if(!lhpvCsList.isEmpty()){
								boolean lhpvUpdate = false;
								for(int k=0;k<lhpvCsList.size();k++){
									Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
									if(String.valueOf(lhpvBnkMap.get("bankCode")).equalsIgnoreCase(csList.get(i).getCsFaCode())){
										System.out.println("################################ 2");
										List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list");
										int vouchNo = csList.get(i).getCsVouchNo(); 
										for(int l=0;l<csList.size();l++){
											if(csList.get(l).getCsVouchNo() == vouchNo){
												
												boolean subCount = false;
												for(int m=0;m<lhpvBnkList.size();m++){
													System.out.println("################################ 3");
													if(lhpvBnkList.get(m).getCsFaCode().equalsIgnoreCase(csList.get(l).getCsFaCode()) && 
															lhpvBnkList.get(m).getCsDrCr() == csList.get(l).getCsDrCr()){
														lhpvBnkList.get(m).setCsAmt(lhpvBnkList.get(m).getCsAmt() + csList.get(l).getCsAmt());
														System.out.println("################################ 4 == "+csList.get(l).getCsFaCode());
														subCount = true;
														break;
													}
												}
												
												if(!subCount){
													System.out.println("################################ 5 == "+csList.get(l).getCsFaCode());
													csList.get(l).setCsTvNo(String.valueOf(csList.get(l).getCsDt()));
													csList.get(l).setCsVouchNo(lhpvBnkList.get(0).getCsVouchNo());
													lhpvBnkList.add(csList.get(l));
												}
												
											}
										}
										
										lhpvBnkMap.remove("list");
										lhpvBnkMap.put("list",lhpvBnkList);
										
										lhpvCsList.remove(k);
										lhpvCsList.add(k, lhpvBnkMap);
										
										lhpvUpdate = true;
										break;
									}
								}
								
								if(!lhpvUpdate){
									System.out.println("################################ 6");
									Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
									List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
									
									int vouchNo = csList.get(i).getCsVouchNo(); 
									lhpvBnkMap.put("bankCode", csList.get(i).getCsFaCode());
									for(int k=0;k<csList.size();k++){
										if(csList.get(k).getCsVouchNo() == vouchNo){
											csList.get(k).setCsTvNo(String.valueOf(csList.get(k).getCsDt()));
											lhpvBnkList.add(csList.get(k));
										}
									}
									lhpvBnkMap.put("list",lhpvBnkList);
									
									mrCsList.add(lhpvBnkMap);
								}
							}else{
								System.out.println("################################## 7");
								Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
								List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
								
								int vouchNo = csList.get(i).getCsVouchNo(); 
								lhpvBnkMap.put("bankCode", csList.get(i).getCsFaCode());
								for(int k=0;k<csList.size();k++){
									if(csList.get(k).getCsVouchNo() == vouchNo){
										csList.get(k).setCsTvNo(String.valueOf(csList.get(k).getCsDt()));
										lhpvBnkList.add(csList.get(k));
									}
								}
								lhpvBnkMap.put("list",lhpvBnkList);
								
								lhpvCsList.add(lhpvBnkMap);
							}
						}

					}
					
					for(int j=0;j<faMList.size();j++){
						Map<String,String> acHdMap = new HashMap<>();
						if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
							acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
							acHdMap.put("faName",faMList.get(j).getFaMfaName());
							acHdList.add(acHdMap);
						}
					}
					
					//Remove multiple bill entries
					for(int k=0;k<csList.size();k++){
						if(csList.get(k).getCsType().equalsIgnoreCase("BILL")){
							csList.remove(k);
							k = k-1;
						}
					}
					
					//Add new consolidated bill entries
					if(!blCsList.isEmpty()){
						for(int k=0;k<blCsList.size();k++){
							csList.add(blCsList.get(k));
						}
					}
					
					
					//Remove multiple mr entries
					for(int k=0;k<csList.size();k++){
						if(csList.get(k).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) &&
								csList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
							csList.remove(k);
							k = k-1;
						}
					}
					
					
					//Add new consolidated mr entries
					if(!mrCsList.isEmpty()){
						System.out.println("size of mrCsList = "+mrCsList.size());
						for(int k=0;k<mrCsList.size();k++){
							Map<String,Object> mrBnkMap = mrCsList.get(k);
							List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list"); 
							if(!mrBnkList.isEmpty()){
								for(int l=0;l<mrBnkList.size();l++){
									csList.add(mrBnkList.get(l));
								}
							}
						}
					}
					
					
					//remove multiple LHPV bank entries
					for(int k=0;k<csList.size();k++){
						if((csList.get(k).getCsType().equalsIgnoreCase("LHPV PAYMENT") || csList.get(k).getCsType().equalsIgnoreCase("LHPV TDS")) &&
								csList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
							csList.remove(k);
							k = k-1;
							System.out.println("remove multiple LHPV bank entries");
						}
					}
					
					
					//Add new consolidated lhpv bank entries
					if(!lhpvCsList.isEmpty()){
						System.out.println("size of lhpvCsList = "+lhpvCsList.size());
						for(int k=0;k<lhpvCsList.size();k++){
							Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
							List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list"); 
							if(!lhpvBnkList.isEmpty()){
								for(int l=0;l<lhpvBnkList.size();l++){
									if(lhpvBnkList.get(l).getCsVouchType().equalsIgnoreCase("BANK")){
										csList.add(lhpvBnkList.get(l));
									}
								}
							}
						}
					}
					
					// Fund Transfer List
					List<Map<String,Object>> bkCsMapList = new ArrayList<>();
					
					for(int i=0;i<bcsList.size();i++){
						Map<String,Object> bkCsMap = new HashMap<>();
						List<CashStmt> bkCsList = new ArrayList<>();
						
						for(int j=0;j<csList.size();j++){
							if(csList.get(j).getCsType().equalsIgnoreCase("Fund Transfer") && bcsList.get(i).getBcsBankCode().equalsIgnoreCase(csList.get(j).getCsFaCode())){
								int vhNo = csList.get(j).getCsVouchNo();
								int count = 0;
								for(int k=0;k<csList.size();k++){
									if(csList.get(k).getCsVouchNo() == vhNo && csList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
										count = count + 1;
									}
								}
								
								if(count > 1){
									bkCsList.add(csList.get(j+1));
								}else{
									for(int k=0;k<csList.size();k++){
										if(csList.get(k).getCsVouchNo() == vhNo && !csList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
											if(csList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
												bkCsList.add(csList.get(k));
											}
										}
									}
								}
								
								
							}else if(bcsList.get(i).getBcsBankCode().equalsIgnoreCase(csList.get(j).getCsFaCode())){
								
								int vhNo = csList.get(j).getCsVouchNo();						
								
								for(int k=0;k<csList.size();k++){									
									if(csList.get(k).getCsVouchNo() == vhNo && !csList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
										System.err.println("First Vouch NO = "+vhNo+" : Second = "+csList.get(k).getCsVouchNo());
										if(csList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
											bkCsList.add(csList.get(k));											
										}
									}else if(csList.get(k).getCsVouchNo() == vhNo && csList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
										if(csList.get(k).getCsVouchType().equalsIgnoreCase("BANK") && csList.get(k).getCsDescription().contains("Single Cheque Cancel")){											
											bkCsList.add(csList.get(k));
										}
									}									
								}
							}
						}
						
						System.out.println("BKCS List Size : "+bkCsList.size());
						
						bkCsMap.put("bank",bcsList.get(i));
						bkCsMap.put("bkCsList", bkCsList);
						//if(!bkCsList.isEmpty()){
							double recBnkAmt = bcsList.get(i).getBcsOpenBal();
							double payBnkAmt = bcsList.get(i).getBcsCloseBal();
							for(int k=0;k<bkCsList.size();k++){
								if(bkCsList.get(k).getCsDrCr() == 'D'){
									payBnkAmt = payBnkAmt + bkCsList.get(k).getCsAmt();
								}else{
									recBnkAmt = recBnkAmt + bkCsList.get(k).getCsAmt();
								}
							}
							bkCsMap.put("recBnkAmt",recBnkAmt);
							bkCsMap.put("payBnkAmt",payBnkAmt);
						//}
						bkCsMapList.add(bkCsMap);
					}
					
					
					if(!bkCsMapList.isEmpty()){
						for(int i=0;i<bkCsMapList.size();i++){
							Map<String,Object> ccMap = bkCsMapList.get(i) ;
							BankCS bankCS = (BankCS) ccMap.get("bank");
							List<CashStmt> bkCsList = (List<CashStmt>) ccMap.get("bkCsList");
							
							System.out.println("*************************");
							System.out.println("#### Bank Code = "+bankCS.getBcsBankCode());
							
						}
					}
					
					// changes by manoj
					if(!csList.isEmpty())
					{
					for (int i=0;i<csList.size();i++) {
						
						if(csList.get(i).getCsVouchType().equalsIgnoreCase("RTGS"))
						{	
						   double totalReceiptAmt=csList.get(i).getCsAmt();
						   for (int j=i+1;j<csList.size();j++) {
				
								if(csList.get(i).getCsVouchType().equalsIgnoreCase(csList.get(j).getCsVouchType())
										&& csList.get(i).getCsFaCode().equalsIgnoreCase(csList.get(j).getCsFaCode()))
								{
								   totalReceiptAmt=totalReceiptAmt+csList.get(j).getCsAmt();
									csList.get(i).setCsAmt(totalReceiptAmt);
									csList.remove(j);
									j=j-1;
									
								}
								
							}
							
						}
						
						if(csList.get(i).getCsVouchType().equalsIgnoreCase("PETRO"))
						{	
						   double totalReceiptAmt=csList.get(i).getCsAmt();
						   for (int j=i+1;j<csList.size();j++) {
				
								if(csList.get(i).getCsVouchType().equalsIgnoreCase(csList.get(j).getCsVouchType())
										&& csList.get(i).getCsFaCode().equalsIgnoreCase(csList.get(j).getCsFaCode()))
								{
								   totalReceiptAmt=totalReceiptAmt+csList.get(j).getCsAmt();
									csList.get(i).setCsAmt(totalReceiptAmt);
									csList.remove(j);
									j=j-1;
									
								}
								
							}
							
						}
						
					}
					}
					
					
					resultMap.put("brhName",brhName);
					resultMap.put("shtNo",cashStmtStatus.getCssCsNo());
					resultMap.put("openBal",cashStmtStatus.getCssOpenBal());
					resultMap.put("closeBal",cashStmtStatus.getCssCloseBal());
					resultMap.put("bkOpenBal",cashStmtStatus.getCssBkOpenBal());
					resultMap.put("bkCloseBal",cashStmtStatus.getCssBkCloseBal());
					
					System.out.println("------ Final CashStmt Lis Size : "+csList.size());
					System.out.println("----- Final acHdList Size : "+acHdList.size());
					System.out.println("----- Final bkCsMapList Size : "+bkCsMapList.size());
										
					resultMap.put("list", csList);
					resultMap.put("acHdList", acHdList);
					resultMap.put("bkCsMapList",bkCsMapList);
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	
				}else{
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				
				List<Map<String,Object>> bkCsMapList = new ArrayList<>();
				
				for(int i=0;i<bcsList.size();i++){
					Map<String,Object> bkCsMap = new HashMap<>();
					bkCsMap.put("bank",bcsList.get(i));
			
					bkCsMapList.add(bkCsMap);
				}
				
				List<FAMaster> faMList = faMasterDAO.getAllFAM();
				
				for(int j=0;j<faMList.size();j++){
					Map<String,String> acHdMap = new HashMap<>();
					if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
						acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
						acHdMap.put("faName",faMList.get(j).getFaMfaName());
						acHdList.add(acHdMap);
					}
				}
				
				// changes by manoj
				if (!csList.isEmpty()) {
					for (int i = 0; i < csList.size(); i++) {

						if (csList.get(i).getCsVouchType()
								.equalsIgnoreCase("RTGS")) {
							double totalReceiptAmt = csList.get(i).getCsAmt();
							for (int j = i + 1; j < csList.size(); j++) {

								if (csList
										.get(i)
										.getCsVouchType()
										.equalsIgnoreCase(
												csList.get(j).getCsVouchType())
										&& csList
												.get(i)
												.getCsFaCode()
												.equalsIgnoreCase(
														csList.get(j)
																.getCsFaCode())) {
									totalReceiptAmt = totalReceiptAmt
											+ csList.get(j).getCsAmt();
									csList.get(i).setCsAmt(totalReceiptAmt);
									csList.remove(j);
									j = j - 1;

								}

							}

						}

					}
				}
				
				resultMap.put("brhName",brhName);
				resultMap.put("shtNo",cashStmtStatus.getCssCsNo());
				resultMap.put("openBal",cashStmtStatus.getCssOpenBal());
				resultMap.put("closeBal",cashStmtStatus.getCssCloseBal());
				resultMap.put("bkOpenBal",cashStmtStatus.getCssBkOpenBal());
				resultMap.put("bkCloseBal",cashStmtStatus.getCssBkCloseBal());
				resultMap.put("list", csList);
				
				System.out.println("******** Final CashStmt Lis Size : "+csList.size());
				
				resultMap.put("acHdList", acHdList);
				resultMap.put("bkCsMapList",bkCsMapList);
				resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				
			}
		}		
		return resultMap;
	}
	
	@RequestMapping(value="/getFaMstr", method=RequestMethod.POST)
	public @ResponseBody Object getFaMstr(){
		
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> faCodeNameList = faMasterDAO.getFaCodeName();
		List<Map<String, Object>> faCodeNameListForBnk = new ArrayList<>();
		
		for (int i = 0; i < faCodeNameList.size(); i++) {
			if (String.valueOf(faCodeNameList.get(i).get("faMfaCode")).substring(0, 2).equalsIgnoreCase("07")) {
				faCodeNameListForBnk.add(faCodeNameList.get(i));
				faCodeNameList.remove(i);
				i=i-1;
			}
		}
			
		
		
		if (!faCodeNameList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("faCodeNameList", faCodeNameList);
			map.put("faCodeNameListForBnk", faCodeNameListForBnk);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getCsForEdit", method=RequestMethod.POST)
	public @ResponseBody Object getCsForEdit(@RequestBody Map<String, Object> editCsService){ 
		
		Map<String, Object> map = new HashMap<>();
		
		System.out.println("cssDt "+editCsService.get("cssDt"));
		System.out.println("csVouchNo "+editCsService.get("csVouchNo"));
		System.out.println("csBranchCode "+editCsService.get("bCode"));
		
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getCssByDt(String.valueOf(editCsService.get("bCode")), CodePatternService.getSqlDate(String.valueOf(editCsService.get("cssDt"))));
		
		if (cashStmtStatus != null) {
			
			List<CashStmt> cashStmtList = cashStmtStatus.getCashStmtList();
			
			System.out.println("cashStmtList is empty: "+cashStmtList.isEmpty());
			System.out.println("cashStmtList is size: "+cashStmtList.size());
			
			if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					if(!String.valueOf(cashStmtList.get(i).getCsVouchNo()).equalsIgnoreCase(String.valueOf(editCsService.get("csVouchNo")))){
						cashStmtList.remove(i);
						i = i-1;
					}
				}
				
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					/*if(sub.equalsIgnoreCase("07") &&  cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
						if(cashStmtList.get(i).getCsType().equalsIgnoreCase("Fund Transfer") ){
							
						}else{
							cashStmtList.remove(i);
							i = i-1;
						}
					}*/
					if(sub.equalsIgnoreCase("07") &&  cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
						if(cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") || 
								cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") || 
								cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw") || 
								cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite")){
							
							cashStmtList.remove(i);
							i = i-1;
						}
					}
				}
			}
			
			if (!cashStmtList.isEmpty()) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("cashStmtList", cashStmtList);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				map.put("resultFromDb", "No Cash Stmt found for Date "+CodePatternService.getFormatedDateString(String.valueOf(editCsService.get("cssDt"))));
			}
			
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("resultFromDb", "No Cash Stmt found for Date "+CodePatternService.getFormatedDateString(String.valueOf(editCsService.get("cssDt"))));
		}
		
		return map;
	}
	
	@RequestMapping(value="/saveCsForEdit", method=RequestMethod.POST)
	public @ResponseBody Object saveCsForEdit(@RequestBody CashStmtService cashStmtService){	
		Map<String, Object> map = new HashMap<>();		
		int temp = cashStmtDAO.saveEditCS(cashStmtService);		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value="/getHoCs", method=RequestMethod.POST)
	public @ResponseBody Object getHoCs(@RequestBody Map<String, Object> initParam){
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("UserID = "+user.getUserId() + " : Enter into /getHoCs() : InitParam = "+initParam);
		
		Map<String, Object> resultMap = new HashMap<>();
		Session session = this.sessionFactory.openSession();
		try{
			
			String branchName = String.valueOf(initParam.get("csBranchName"));
			String branchId = String.valueOf(initParam.get("csBranchId"));
			String fromDate = String.valueOf(initParam.get("csFromDate"));
			String toDate = String.valueOf(initParam.get("csToDate"));
			
			// Convert date from String to Date(SQL)
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date fmDt = new Date(format.parse(fromDate).getTime());
			Date toDt = new Date(format.parse(toDate).getTime());
			
			List<CashStmtStatus> cssList = cashStmtStatusDAO.getCss(session, user, branchId, fmDt, toDt);
			
			if(cssList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "CSS not found !");
			}else{
							
				Set<String> faCodes = new HashSet<String>();
				
				for(int i=0; i<cssList.size(); i++){
					List<CashStmt> csList = cssList.get(i).getCashStmtList();
					for(int j=0; j<csList.size(); j++){
						faCodes.add(csList.get(j).getCsFaCode());
					}
				}
				
				Map<String, String> faMaster = faMasterDAO.getAllFAM(session, user, faCodes);
				System.out.println("FaCode Size = "+faMaster.size());
				
				List<Map<String, Object>> finalList = new ArrayList<Map<String,Object>>();
				
				for(int i=0; i<cssList.size(); i++){
					CashStmtStatus cashStmtStatus = cssList.get(i);
					
					Double cashOpeningAmt = cashStmtStatus.getCssOpenBal();
					Double cashCloseAmt = cashStmtStatus.getCssCloseBal();
					
					Map<String, Object> cssMap = new HashMap<String, Object>();
					
										
					List<BankCS> bankCsList = cashStmtStatusDAO.getBankCs(session, user, Integer.parseInt(branchId), cashStmtStatus.getCssDt());
					
					List<Map<String, Object>> bankList = new ArrayList<Map<String,Object>>();
					List<Map<String, Object>> cashList = new ArrayList<Map<String, Object>>();
					
					
					cssMap.put("branchName", branchName);
					cssMap.put("sheetNo", cashStmtStatus.getCssCsNo());
					cssMap.put("csDate", cashStmtStatus.getCssDt());					
					
					List<CashStmt> csList = cashStmtStatus.getCashStmtList();
					
					System.err.println("CSSize = "+csList.size());
					
					// Cash CS
					System.err.println("CashCs is going on");
					double totCr = 0;
					double totDr = 0;					
					Iterator<CashStmt> it = csList.iterator();
					while(it.hasNext()){
						CashStmt cs = (CashStmt) it.next();					
						if(cs.getCsVouchType().equalsIgnoreCase("CASH")){
							Map<String, Object> map = getCsMap(cs, faMaster);
							cashList.add(map);
							if(cs.getCsDrCr() == 'C')
								totCr = totCr + cs.getCsAmt();
							else if(cs.getCsDrCr() == 'D')
								totDr = totDr + cs.getCsAmt();							
						}
					}
					
					
					
					cssMap.put("cashList", cashList);
					cssMap.put("cashOpeningAmt", cashStmtStatus.getCssOpenBal());
					cssMap.put("cashClosingAmt", cashStmtStatus.getCssCloseBal());
					cssMap.put("cashTotCr", (cashStmtStatus.getCssOpenBal() + totCr));
					cssMap.put("cashTotDr", (cashStmtStatus.getCssCloseBal() + totDr));
					
					System.err.println("Bank Cs is going on.....");
					
					// Bank CS (Bank Transaction)
					for(int j=0; j<bankCsList.size(); j++){
						
						totCr = 0;
						totDr = 0;
						
						BankCS bankCs = bankCsList.get(j);
						
						Map<String, Object> bankMap = new HashMap<String, Object>();
						
						System.err.println("ID = "+bankCs.getBcsBankCode());
						
						bankMap.put("bankName", faMasterDAO.getAllFaMstrByFaNameOrCode(bankCs.getBcsBankCode()).get(0).getFaMfaName());
						bankMap.put("bankCode", bankCs.getBcsBankCode());
						bankMap.put("bankOpeningAmt", bankCs.getBcsOpenBal());
						bankMap.put("bankClosingAmt", bankCs.getBcsCloseBal());
						
						List<Map<String, Object>> bankCsListTemp = new ArrayList<Map<String,Object>>();
						
						boolean sameBank = false;
						int bankVouchNo = 0;
						
						for(int k=0; k<csList.size(); k++){
							
							CashStmt cs = csList.get(k);							
														
							System.err.println("CsID = "+cs.getCsId() + " : Vou = "+cs.getCsVouchNo() + " : csFaCode = "+cs.getCsFaCode() + 
									" : CsVouchType = "+cs.getCsVouchType() + " : Des = "+cs.getCsDescription());
							
							int csVouchNo = cs.getCsVouchNo();
							String csFaCode = cs.getCsFaCode();
							
							if(csFaCode.startsWith("07") && csFaCode.equalsIgnoreCase(bankCs.getBcsBankCode())){
								sameBank = true;
								bankVouchNo = cs.getCsVouchNo();
							}else if(csFaCode.startsWith("07"))
								sameBank = false;
							
							if(sameBank && csVouchNo == bankVouchNo){
								
								if(!cs.getCsFaCode().startsWith("07") && cs.getCsVouchType().equalsIgnoreCase("BANK")){
																		
									Map<String, Object> map = getCsMap(cs, faMaster);
									if(! String.valueOf(map.get("csCr")).equalsIgnoreCase(""))
										totCr = totCr + Double.parseDouble(String.valueOf(map.get("csCr")));
									if(! String.valueOf(map.get("csDr")).equalsIgnoreCase(""))
										totDr = totDr + Double.parseDouble(String.valueOf(map.get("csDr")));
									bankCsListTemp.add(map);
									
								}else if(cs.getCsFaCode().startsWith("07") && cs.getCsVouchType().equalsIgnoreCase("BANK") && cs.getCsType().equalsIgnoreCase("Cheque Cancel")){
									Map<String, Object> map = getCsMap(cs, faMaster);
									
									if(! String.valueOf(map.get("csCr")).equalsIgnoreCase(""))
										totCr = totCr + Double.parseDouble(String.valueOf(map.get("csCr")));
									if(! String.valueOf(map.get("csDr")).equalsIgnoreCase(""))
										totDr = totDr + Double.parseDouble(String.valueOf(map.get("csDr")));
									
									bankCsListTemp.add(map);											
								}
							}
							
						}					

						bankMap.put("totCr", (bankCs.getBcsOpenBal() + totCr));
						bankMap.put("totDr", (bankCs.getBcsCloseBal() + totDr));
						bankMap.put("bankCs", bankCsListTemp);						
						bankList.add(bankMap);
						
					}
					// End of BankCs Transaction
					
					
					cssMap.put("bankList", bankList);
					finalList.add(cssMap);
					
//					Gson gson = new GsonBuilder().setPrettyPrinting().create();
//					String json = gson.toJson(finalList);
//					System.err.println("**********");
//					System.out.println(json);
					
					
					
				}			
				if(! finalList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("csList", finalList);
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "No such record found !");
				}
			}
				
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId() + " : Exit from /getHoCs() : Result = "+resultMap);
		return resultMap;
	}
	
	public Map<String, Object> getCsMap(CashStmt cs, Map<String, String> faMaster){
		Map<String, Object> map = new HashMap<String, Object>();
		System.out.println("$$$ = "+cs.getCsId());
		map.put("csFaCode", cs.getCsFaCode());
		map.put("csFaName", faMaster.get(cs.getCsFaCode()));
		map.put("csType", cs.getCsType());
		map.put("csDescription", cs.getCsDescription());
		map.put("csTvNo", cs.getCsTvNo());
		map.put("csVouchNo", cs.getCsVouchNo());
		if(cs.getCsDrCr() == 'C'){
			map.put("csCr", cs.getCsAmt());
			map.put("csDr", "");
		}else if(cs.getCsDrCr() == 'D'){
			map.put("csDr", cs.getCsAmt());
			map.put("csCr", "");
		}
		return map;
	}
	
}