<div ng-show="operatorLogin || superAdminLogin">
<title>View Station</title>
<!-- <div ng-controller = "StationCntlr" > -->
<style>
th{color:#26A69A;}
</style>
<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
		<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="submitCode(stationName,stationCode)" >
	 <div id ="stationCode" >
			<div class="input-field col s6">
				<input type="text" id="stationName" name="stationName" ng-model="stationName" required readonly ng-click="openStationCodeDB()"/>
   				<input type="hidden" name="stationCode" ng-model="stationCode"/>
   				<label>Enter Station Code</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
	 </div> 
		</form>
		<div class="col s3"> &nbsp; </div>
</div>

<div id="stationDetails" ng-hide="stationDetailsFlag"> 
<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
		<h4>View Station</h4>	
		<table class="table-bordered table-hover table-condensed">
 	 		<tr>
					
				<th>Station Name</th>	
				
				<!-- <th>Station Code</th> -->
				
				<th>Station District</th>
				
				<th>Station Pin</th>
								
				<!-- <th>State Code</th>
			
				<th>Station CreationTS</th> -->
				
			</tr>
 	 		  
		  <tr ng-repeat="station in stationList">
		 	 
		 	  <td>{{ station.stnName }}</td>
              <!-- <td>{{ station.stnCode }}</td> -->
              <td>{{ station.stnDistrict }}</td>
              <td>{{ station.stnPin }}</td>
              <!-- <td>{{ station.stateCode }}</td>
              <td>{{ station.creationTS }}</td> -->
          </tr>
		</table>
	</div>
</div>	
</div>

<div id ="stationCodeDB" ng-hide="stationCodeDBFlag"> 
 	   	<input type="text" name="filterStnCode" ng-model="filterStnCode" placeholder="Search">
		 <table>
		 	
		 	<tr>
		 		<th></th>
				
				<th>Station Name</th>	
				
				<th>Station District</th>
				
				<th>Station Pin</th>
				
				<th>Station Code</th>				
				<!-- <th>State Code</th>
			
				<th>Station CreationTS</th> -->
				
			</tr>
		 
		  <tr ng-repeat="stn in stationList  | filter:filterStnCode">
		 	  <td><input type="radio"  name="stnName" class="stncls"  value="{{ stn }}" ng-model="stnModel" ng-click="saveStationCode(stn)"></td>
              <td>{{ stn.stnName }}</td>
              <td>{{ stn.stnDistrict }}</td>
              <td>{{ stn.stnPin }}</td>
              <td>{{ stn.stnCode }}</td>
              <!-- <td>{{ station.stateCode }}</td>
              <td>{{ station.creationTS }}</td> -->
         </tr>
         </table>
</div>

<div id ="stateCodeDB" ng-hide="stateCodeDBFlag"> 
 	   	<input type="text" name="filterCode" ng-model="filterCode.stateCode" placeholder="Search">
		 <table>
		  <tr ng-repeat="state in stateList  | filter:filterCode">
		 	  <td><input type="radio"  name="stateName" class="statecls"  value="{{ state }}" ng-model="stateModel" ng-click="saveStateCode(state)"></td>
              <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
         </table>
</div>


 <div id="stationCodeDetails" ng-hide="stationCodeDetailsFlag">
	<div class="row">
		<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
			<h4>Here's the details of Station <span class="teal-text text-lighten-2">{{station.stnName}}</span>:</h4>
			
			<form name="StationForm" ng-submit="updateStation(StationForm)">	
			<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Station Name:</td>
	                <td><input type ="text" id="stnName" name ="stnName" ng-model="station.stnName" value={{station.stnName}}  ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            
	            <tr>
	                <td>Station District:</td>
	                <td><input type ="text" id="district" name ="stnDistrict" ng-model="station.stnDistrict" value={{station.stnDistrict}}  ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            
	            <tr>
	                <td>Station Code:</td>
	                <td><input type ="text" name ="stnCode" ng-model="station.stnCode" value={{station.stnCode}} readonly required ></td>
	            </tr>
	            
	            <tr>
	                <td>State Code:</td>
	                <td><input type ="text" name ="stateName" ng-model="stateName" value={{stateName}} readonly required ng-click="openStateCodeDB()" ></td>
	                 <td><input type ="hidden" name ="stateCode" ng-model="station.stateCode"></td>
	            </tr>
	            
	             <tr>
	                <td>Station Pin:</td>
	                <td><input type ="text" id="stnPin" name ="stnPin" ng-model="station.stnPin" value={{station.stnPin}} ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
	            </tr>
	            
	            <tr>
					<td>CreationTS</td>
					<td><input type ="text" name ="creationTS" ng-model="time" value={{time}} required readonly></td>
				</tr>
		</table>
				<input type="submit" value="Submit!">
	</form>
	      
</div>		
</div>
</div> 
		
</div>		

<!--  <table>
 	 		<tr>
								
				<th>State Code</th>
			
				<th>Station Code</th>
				
				<th>Station District</th>
				
				<th>Station Name</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="station in stationList">
		 	  <td>{{ station.stateCode }}</td>
              <td>{{ station.stnCode }}</td>
              <td>{{ station.stnDistrict }}</td>
              <td>{{ station.stnName }}</td>
          </tr>
         </table> -->

<!-- </div> -->
