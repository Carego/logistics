package com.mylogistics.constants;

public class CashStmtStatusCNTS {
	
	public static final String CSS_ID = "cssId";
	
	public static final String CSS_BR_CODE = "cssBrCode";
	
	public static final String CASH_STMT_LIST = "cashStmtList";
	
	public static final String CSS_DT = "cssDt";
	
	public static final String CSS_OPEN_BAL = "cssOpenBal";
	
	public static final String CSS_CLOSE_BAL = "cssCloseBal";
	
	public static final String CSS_CS_NO = "cssCsNo";
	
	public static final String B_Code = "bCode";
	
	public static final String NOT_FOUND = "Cashstmtstatus is not found.";
	

}
