package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.RegularContractOld;

public interface RegularContractOldDAO {

	public int saveRegContOld(RegularContractOld regularContractOld);
	
	//Sanjana: gets regularContractOld data of particular contractCode
		public List<RegularContractOld> getOldRegularContract(String contractCode);
}
