package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.StationRightsDAO;
import com.mylogistics.constants.StationRightsCNTS;
import com.mylogistics.model.StationRights;

public class StationRightsDAOImpl implements StationRightsDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public StationRightsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 public int saveStRights(StationRights stationRights){
		 System.out.println("enter into saveStRights function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(stationRights);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<StationRights> getStationRightsIsAllow(){
		 System.out.println("Entered into getStationRightsIsAllow function---");
		 List<StationRights> list = new ArrayList<StationRights>();
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(StationRights.class);
			   cr.add(Restrictions.eq(StationRightsCNTS.STRH_ADV_ALLOW,"yes"));
			   list =cr.list();
			   session.flush();
		 }catch(Exception e){
				e.printStackTrace();
		 }
		 session.clear();
         session.close();
		 return list;  
	 }
	 
	 @SuppressWarnings("unchecked")
     @Transactional
     public int updateStnRightsIsAllowNo(int contIdsInt[]){
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
         try{
             session = this.sessionFactory.openSession();
             for(int i=0;i<contIdsInt.length;i++){
                transaction = session.beginTransaction();
                StationRights stationRights  = new StationRights();
                List<StationRights> list = new ArrayList<StationRights>();
                Criteria cr=session.createCriteria(StationRights.class);
                cr.add(Restrictions.eq(StationRightsCNTS.STRH_ID,contIdsInt[i]));
                list =cr.list();
                stationRights = list.get(0);
                stationRights.setStrhAdvAllow("no");
                stationRights.setCreationTS(calendar);
                session.update(stationRights);
                transaction.commit();
             }
             session.flush();
             temp = 1;
        }catch(Exception e){
             e.printStackTrace();
             temp = -1;
        }
         session.clear();
        session.close();
        return temp;
     }
}
