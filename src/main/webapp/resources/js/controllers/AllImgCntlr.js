'use strict';

var app = angular.module('application');

app.controller('AllImgCntlr',['$scope','$location','$http','FileUploadService',
                                 function($scope,$location,$http,FileUploadService){
	
	$scope.branchCode = "";
	$scope.imgType = "";
	$scope.code = "";
	$scope.branchList = [];
	$scope.list  = [];
	
	$scope.branchCodeFlag = true;
	$scope.detailTableFlag = true; 
	
	//	Dialog Boxs Starts....
	$scope.OpenBranchDataDB = function(){
		$scope.branchCodeFlag = false;
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		}); 	
		$('div#branchCodeDB').dialog('open');
	}
	
	//	Methods Starts....
	$scope.getBranchData = function(){
		console.log("Enter into getBranchData() ");
		var response = $http.post($scope.projectName+'/getBranchDataForOwner');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.branchList = data.list;
				console.log("Branch Size = "+data.list.length);
			}else{
				console.log(data.result);
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in calling getBranchData()");
			$scope.alertToast(data.result);
		});
		console.log("Exit from getBranchData() ");
	}
	
	$scope.savBranchCode = function(branch){
		console.log("Enter into saveBranchCode() : BranchCode = "+branch.branchCode);
		$scope.branchCodeFlag = true;
		$scope.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		console.log("Exit from saveBranchCode() ");
	}
	
	$scope.SubmitImg = function(ImgForm){
		console.log("Enter into SubmitImg() ");
		console.log("ImgForm Valid = "+ImgForm.$valid);
		if(ImgForm.$invalid){
			if(ImgForm.branchCode.$invalid)
				$scope.alertToast("Select branch !");
			if(ImgForm.imgType.$invalid)
				$scope.alertToast("Select type !");
		}else{
			var initParam = {
					"branchCode"	:	$scope.branchCode,
					"imgType"		:	$scope.imgType,
					"code"			:	$.trim($scope.code)
			}
			
			$("#imgSubmit").attr("disabled", "disabled");
			$scope.detailTableFlag = true;
			
			var response = $http.post($scope.projectName+ '/getAllImg', initParam);
			
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$("#imgSubmit").removeAttr("disabled");
					$scope.list = [];
					$scope.list = data.list;
					$scope.detailTableFlag = false;
					$scope.alertToast("Size = "+data.list.length);
				}else{
					$("#imgSubmit").removeAttr("disabled");
					$scope.list = [];
					$scope.detailTableFlag = true;
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(data, status, headers, config){
				$("#imgSubmit").removeAttr("disabled");
				$scope.list = [];
				$scope.detailTableFlag = true;
				console.log("Error in hitting /getAllImg()");
			});
		}
		console.log("Exit from SubmitImg() ");
	}
	
	$scope.uploadFile = function(imgId, index){		
		var file = $("#image"+index)[0].files[0];
		if(angular.isUndefined(file)){
			$scope.alertToast("Select image to upload !");
			return ;
		}
		$("#submit"+index).attr("disabled", "disabled");
		console.log("File : "+file+" : IMG ID = "+imgId);
		var form = new FormData();
		
		form.append("file", file);
		form.append("imgId", imgId);
		
		var response = $http.post($scope.projectName+'/uploadAllImg', form, {
			   transformRequest: angular.identity,
			   headers: {'Content-Type': undefined}
			});
		response.success(function(data, status, headers, config){
			console.log(data.result);
			$scope.alertToast(data.result);
			if(data.result === "success"){
				
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /uploadAllImg");
		});
		
	}
	
	//	Call At load time....
    if($scope.operatorLogin === true || $scope.superAdminLogin === true){
    	 $scope.getBranchData();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	   
}]);