package com.mylogistics.DAOImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.AllImgDao;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillForwardingCNTS;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.BrokerImgCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.CnmtImageCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.OwnerImgCNTS;
import com.mylogistics.constants.VehicleVendorMstrCNTS;
import com.mylogistics.model.BankStmtUpld;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.ConstantsValues;
import com.mysql.jdbc.Blob;

public class AllImgDaoImpl implements AllImgDao{

	@Autowired
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	@Autowired
	private HttpSession httpSession;
	
	public AllImgDaoImpl(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}

	public static Logger logger = Logger.getLogger(AllImgDaoImpl.class);
	
	private String cnmtImgPath = "/var/www/html/Erp_Image/CNMT";
	private String bankStmtPath = "/var/www/html/Erp_Image/BANK/DailyTrnsfr";
	
	@Override
	public List<Map<String, Object>> getUnsavedCnmtImg(String branchCode, String code){
		logger.info("Enter into getUnsavedCnmtImg() : BranchCode = "+branchCode+" : Code = "+code);
		List<Map<String, Object>> unSavedCnmtImgList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			if(code != null && !code.equalsIgnoreCase("")){
				List<Cnmt> cnmtList = session.createCriteria(Cnmt.class)
						.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, code))
						.list();
				if(! cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					Integer cmId = cnmt.getCmId();
					if(cmId == null)
						cmId = 0;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("code", cnmt.getCnmtCode());
					map.put("imgId", cnmt.getCnmtId());
					if(cmId > 0)
						map.put("isAvailable", "yes");
					else
						map.put("isAvailable", "no");
					unSavedCnmtImgList.add(map);
				}
			}else{
				
				Criteria cnmtImgCriteria = session.createCriteria(CnmtImage.class)
						.add(Restrictions.eq("bCode", branchCode))						
						.add(Restrictions.or(Restrictions.like("cnmtImgPath", "/usr%"), 
									Restrictions.like("cnmtImgPath", "%CGLPL%"), 
									Restrictions.like("cnmtImgPath", "%RGH%"),
									Restrictions.like("cnmtImgPath", "%bbl%")
								));
						//.add(Restrictions.sqlRestriction("creationTs >= '2016-11-01' AND creationTS <= '2016-12-31'"));
					
				List<CnmtImage> imgList = cnmtImgCriteria.list();
				Iterator<CnmtImage> imgIterator = imgList.iterator();
				while(imgIterator.hasNext()){
					CnmtImage cnmtImage = (CnmtImage) imgIterator.next();
					File file  = new File(cnmtImage.getCnmtImgPath());
					System.err.println("File Name = "+file.getAbsolutePath());
					if(! file.exists()){
						Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmtImage.getCnmtId());
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("code", cnmt.getCnmtCode());
						map.put("imgId", cnmt.getCnmtId());
						map.put("isAvailable", "No");
						unSavedCnmtImgList.add(map);
					}
				}
			}
			
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from getUnsavedCnmtImg() : Return size = "+unSavedCnmtImgList.size());
		return unSavedCnmtImgList;
	}
	
	public Map<String, Object> updateImage(byte fileInBytes[], Integer cnmtId){
		logger.info("Enter into updateImage() : File = "+fileInBytes+" : CNMT ID = "+cnmtId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Cnmt cnmt = null;
			if(cnmtId > 0){
				cnmt = (Cnmt) session.get(Cnmt.class, cnmtId);
				List<CnmtImage> imgList = session.createCriteria(CnmtImage.class)
						.add(Restrictions.eq(CnmtImageCNTS.CM_CNID, cnmtId))
						.list();
				if(! imgList.isEmpty()){
					CnmtImage cnmtImage = (CnmtImage)imgList.get(0);
					String path = cnmtImgPath+"/CNMT"+cnmt.getCnmtId()+".pdf";
					FileOutputStream out = new FileOutputStream(path);
					out.write(fileInBytes);
					out.close();
					cnmtImage.setCnmtImgPath(path);
					session.update(cnmtImage);
					cnmt.setCmId(cnmtImage.getCmId());
					session.update(cnmt);
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("msg", "File is uploaded !");
					logger.info("Image is updated successfully......");
				}else{
					String path = cnmtImgPath+"/CNMT"+cnmt.getCnmtId()+".pdf";
					FileOutputStream out = new FileOutputStream(path);
					out.write(fileInBytes);
					out.close();
					CnmtImage cnmtImage = new CnmtImage();
					cnmtImage.setbCode(cnmt.getBranchCode());
					cnmtImage.setCnmtId(cnmt.getCnmtId());
					cnmtImage.setCnmtImgPath(path);
					cnmtImage.setUserCode(cnmt.getUserCode());
					Integer cmId = (Integer)session.save(cnmtImage);
					cnmt.setCmId(cmId);
					session.update(cnmt);
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("msg", "File is uploaded !");
					logger.info("Image is updated successfully......");
				}
			}			
			transaction.commit();			
		}catch(Exception e){
			transaction.rollback();
			logger.info("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "File is not uploaded !");
		}
		session.clear();
		session.close();
		logger.info("Exit from updateImage()");
		return resultMap;
	}
	
	@Override
	public java.sql.Blob downloadImg(Session session, User user, Map<String, String> initParam){
		logger.info("UserID = "+user.getUserId()+" : Enter into downloadImg() : intiParam = "+initParam);
		 java.sql.Blob blob = null;
		 try{
			 String imageType = initParam.get("imageType");
			 if(imageType.equalsIgnoreCase("CNMT")){
				 String cnmtNo = initParam.get("cnmtNo");				 
				 List<Cnmt> cnmtList = session.createCriteria(Cnmt.class)
						 .add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtNo))
						 .list();
				 if(! cnmtList.isEmpty()){
					 Cnmt cnmt = (Cnmt) cnmtList.get(0);
					 Integer imgId = cnmt.getCmId();
					 if(imgId > 0){
						 CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, imgId);
							
						 if(cnmtImage.getArImgPath()!=null)
						 {
							 if(initParam.get("cnmtChlnNo")!=null && !"".equalsIgnoreCase(initParam.get("cnmtChlnNo"))) {
								 List<Challan> chlnList=session.createCriteria(Challan.class)
										 .add(Restrictions.eq("chlnCode", initParam.get("cnmtChlnNo"))).list();
								 if(!chlnList.isEmpty()) {
									Challan chln=chlnList.get(0);
									
									File file = new File(cnmtImage.getArImgPath().get(chln.getChlnId()));
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
									
								 }else {
									 File file = new File(cnmtImage.getCnmtImgPath());
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }else {
								 File file = new File(cnmtImage.getCnmtImgPath());
								 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
								 blob = new SerialBlob(imgInBytes);
							 }
						 }else {
							 File file = new File(cnmtImage.getCnmtImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);
						 }
						 
					 }
				 }
			 }else if(imageType.equalsIgnoreCase("OWN")){
				 Integer id = Integer.parseInt(initParam.get("id"));
				 List<OwnerImg> imgList = session.createCriteria(OwnerImg.class)
						 .add(Restrictions.eq(OwnerImgCNTS.OWN_ID, id))
						 .list();
				 if(! imgList.isEmpty()){
					 OwnerImg img = imgList.get(0);
					 String ownBrkImgType = initParam.get("ownBrkImgType");
					 if(ownBrkImgType.equalsIgnoreCase("PAN")){
						 if(img.getOwnPanImgPath() != null){
							 File file = new File(img.getOwnPanImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);							 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
						 if(img.getOwnDecImgPath() != null){
							 File file = new File(img.getOwnDecImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("CHQ")){
						 if(img.getOwnBnkDetImgPath() != null){
							 File file = new File(img.getOwnBnkDetImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }
				 }
			 }else if(imageType.equalsIgnoreCase("BRK")){
				 Integer id = Integer.parseInt(initParam.get("id"));
				 List<BrokerImg> imgList = session.createCriteria(BrokerImg.class)
						 .add(Restrictions.eq(BrokerImgCNTS.BRK_ID, id))
						 .list();
				 if(! imgList.isEmpty()){
					 BrokerImg img = imgList.get(0);
					 String ownBrkImgType = initParam.get("ownBrkImgType");
					 if(ownBrkImgType.equalsIgnoreCase("PAN")){
						 if(img.getBrkPanImgPath() != null){
							 File file = new File(img.getBrkPanImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
						 if(img.getBrkDecImgPath() != null){
							 File file = new File(img.getBrkDecImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("CHQ")){
						 if(img.getBrkBnkDetImgPath() != null){
							 File file = new File(img.getBrkBnkDetImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }
				 }
			 }else if(imageType.equalsIgnoreCase("chln")){
				 String chlnCode = initParam.get("ownBrkNC");
				 String ownBrkImgType = initParam.get("ownBrkImgType");
				 List<ChallanDetail> chlnDt = session.createCriteria(ChallanDetail.class)
						 .add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCode))
						 .list();
				 if(! chlnDt.isEmpty()){
					 String ownCode = chlnDt.get(0).getChdOwnCode();
					 if(ownCode != null && !ownCode.equalsIgnoreCase("")){
						 List<Owner> ownList = session.createCriteria(Owner.class)
								 .add(Restrictions.eq(OwnerCNTS.OWN_CODE, ownCode))
								 .list();
						 
						 if(! ownList.isEmpty()){
							 Owner owner = (Owner) ownList.get(0);
							 if(owner.getOwnImgId() != null && owner.getOwnImgId() > 0){
								 OwnerImg ownerImg = (OwnerImg) session.get(OwnerImg.class, owner.getOwnImgId());
								 if(ownBrkImgType.equalsIgnoreCase("PAN")){
									 if(ownerImg.getOwnPanImgPath() != null){
										 File file = new File(ownerImg.getOwnPanImgPath());
										 if(file.exists()){
											 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
											 blob = new SerialBlob(imgInBytes);
										 }
									 }
								 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
									 if(ownerImg.getOwnDecImgPath() != null){
										 File file = new File(ownerImg.getOwnDecImgPath());
										 if(file.exists()){
											 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
											 blob = new SerialBlob(imgInBytes);
										 }
									 }
								 }
							 }
						 }
					 }
					 if(blob == null){
						 String brkCode = chlnDt.get(0).getChdBrCode();
						 if(brkCode != null && !brkCode.equalsIgnoreCase("")){
							 List<Broker> brkList = session.createCriteria(Broker.class)
									 .add(Restrictions.eq(BrokerCNTS.BRK_CODE, brkCode))
									 .list();
							 
							 if(! brkList.isEmpty()){
								 Broker broker = (Broker) brkList.get(0);
								 if(broker.getBrkImgId() != null && broker.getBrkImgId() > 0){
									 BrokerImg brokerImg = (BrokerImg) session.get(BrokerImg.class, broker.getBrkImgId());
									 if(ownBrkImgType.equalsIgnoreCase("PAN")){
										 if(brokerImg.getBrkPanImgPath() != null){
											 File file = new File(brokerImg.getBrkPanImgPath());
											 if(file.exists()){
												 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
												 blob = new SerialBlob(imgInBytes);
											 }
										 }
									 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
										 if(brokerImg.getBrkDecImgPath() != null){
											 File file = new File(brokerImg.getBrkDecImgPath());
											 if(file.exists()){
												 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
												 blob = new SerialBlob(imgInBytes);
											 }
										 }
									 }
								 }
							 }
						 }
					 }					 
				 }
			 }else if(imageType.equalsIgnoreCase("lry")){
				 String lryNo = initParam.get("ownBrkNC");
				 String ownBrkImgType = initParam.get("ownBrkImgType");
				 
				 List<VehicleVendorMstr> vehicleList = session.createCriteria(VehicleVendorMstr.class)
						 .add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, lryNo))
						 .list();
				 if(! vehicleList.isEmpty()){
					 VehicleVendorMstr mstr = (VehicleVendorMstr) vehicleList.get(0);
					 Integer imgId = mstr.getOwner().getOwnImgId();
					 
					 if(imgId != null && imgId > 0){
						 OwnerImg ownerImg = (OwnerImg) session.get(OwnerImg.class, imgId);
						 
						 if(ownBrkImgType.equalsIgnoreCase("PAN")){
							 if(ownerImg.getOwnPanImgPath() != null){
								 File file = new File(ownerImg.getOwnPanImgPath());
								 if(file.exists()){
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }
						 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
							 if(ownerImg.getOwnDecImgPath() != null){
								 File file = new File(ownerImg.getOwnDecImgPath());
								 if(file.exists()){
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }
						 }else if(ownBrkImgType.equalsIgnoreCase("chq")){
							 if(ownerImg.getOwnBnkDetImgPath() != null){
								 File file = new File(ownerImg.getOwnBnkDetImgPath());
								 if(file.exists()){
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }
						 }else if(ownBrkImgType.equalsIgnoreCase("rc")){
							 if(mstr.getVvRcImgPath() != null){
								 File file = new File(mstr.getVvRcImgPath());
								 if(file.exists()){
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }
						 }else if(ownBrkImgType.equalsIgnoreCase("pd")){
							 if(mstr.getVvPolicyImgPath() != null){
								 File file = new File(mstr.getVvPolicyImgPath());
								 if(file.exists()){
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }
						 }else if(ownBrkImgType.equalsIgnoreCase("ps")){
							 if(mstr.getVvPerSlpImgPath() != null){
								 File file = new File(mstr.getVvPerSlpImgPath());
								 if(file.exists()){
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
								 }
							 }
						 }
						 
					 }
					 if(blob == null){
						 imgId = mstr.getBroker().getBrkImgId();
						 if(imgId != null && imgId > 0){
							 BrokerImg brokerImg = (BrokerImg) session.get(BrokerImg.class, imgId);
							 if(ownBrkImgType.equalsIgnoreCase("PAN")){
								 if(brokerImg.getBrkPanImgPath() != null){
									 File file = new File(brokerImg.getBrkPanImgPath());
									 if(file.exists()){
										 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
										 blob = new SerialBlob(imgInBytes);
									 }
								 }
							 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
								 if(brokerImg.getBrkDecImgPath() != null){
									 File file = new File(brokerImg.getBrkDecImgPath());
									 if(file.exists()){
										 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
										 blob = new SerialBlob(imgInBytes);
									 }
								 }
							 }
						 }
					 }
					 
				 }
			 }else if(imageType.equalsIgnoreCase("bil")){
				 
				 String bilNo = initParam.get("bilNo");
				 
				 List<Bill> bilList = session.createCriteria(Bill.class)
						 .add(Restrictions.eq(BillCNTS.Bill_NO, bilNo))
						 .list();
				 
				 if(!bilList.isEmpty()){
					 
					 int ar=0;
					 PDDocument document=null;
					 File file2=new File("/var/www/html/Erp_Image/Bill/"+bilNo+".pdf");
					 File[] totalfiles = null;
					 PDFMergerUtility mergerUtility=new PDFMergerUtility();
						
						mergerUtility.setDestinationFileName(file2.getAbsolutePath());
						try {
							file2.createNewFile();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					 
					 
					List<BillDetail> billDetailList=bilList.get(0).getBillDetList();
					
					if(!billDetailList.isEmpty()){
						totalfiles=new File[billDetailList.size()];
						for(BillDetail bd : billDetailList){
							int cnmtId=bd.getBdCnmtId();
							
							
							
							 List<Cnmt> cnmtList = session.createCriteria(Cnmt.class)
									 .add(Restrictions.eq(CnmtCNTS.CNMT_ID, cnmtId))
									 .list();
							 if(! cnmtList.isEmpty()){
								 Cnmt cnmt = (Cnmt) cnmtList.get(0);
								 Integer imgId = cnmt.getCmId();
								 if(imgId > 0){
									 System.out.println("imgId= "+imgId);
									 CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, imgId);
									 System.out.println("path= "+cnmtImage.getCnmtImgPath());
									 File file = new File(cnmtImage.getCnmtImgPath());
									 //totalfiles=file.listFiles();
									 System.out.println("file= "+file);
									 totalfiles[ar]=file.getCanonicalFile();
									 ar++;
									 System.out.println("totalfiles= "+totalfiles);
								 }
							 }
							
							
						}
						
						
						
						
						
						for (int i = 0; i < totalfiles.length; i++) {
						      System.out.println(totalfiles[i].getName());
						try {
							
							document=PDDocument.load(totalfiles[i]);
							int j=document.getNumberOfPages();
							System.out.println(j);
						
							if(totalfiles[i]!=null)
							{
							mergerUtility.addSource(totalfiles[i]);

							System.out.println("Document Merged");
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						
							}
						try {
							mergerUtility.mergeDocuments();
						} catch (COSVisitorException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							document.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						
						File file = new File(file2.getAbsolutePath());
						 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
						 blob = new SerialBlob(imgInBytes);					 
					 
					}
					 
				 }
				 
			 }else if(imageType.equalsIgnoreCase("bf")){
				 String bfNo = initParam.get("bfNo");				 
				 List<BillForwarding> bfList = session.createCriteria(BillForwarding.class)
						 .add(Restrictions.eq(BillForwardingCNTS.BF_NO, bfNo))
						 .list();
				 if(! bfList.isEmpty()){
					 BillForwarding bf = (BillForwarding) bfList.get(0);
					 if(bf.getBfImagePath() != null){
						 File file = new File(bf.getBfImagePath());
						 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
						 blob = new SerialBlob(imgInBytes);					 
					 }
				 }
			 }
		 }catch(Exception e){
			 logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		 }
		 logger.info("UserID = "+user.getUserId()+" : Exit from downloadImg() : IMG = "+blob);
		 return blob;
	}
	
	public Map<String, Object> getOwnBrkForImgRead(Session session, User user, Map<String, String> initParam, Map<String, Object> resultMap){
		logger.info("UserID = "+user.getUserId()+" Enter into getOwnBrkForImgRead()");
		try{
			String imageType = initParam.get("imageType");
			String ownBrkNC = initParam.get("ownBrkNC");
			List<Map<String, Object>> list = null;
			if(imageType.equalsIgnoreCase("OWN")){				
				if(NumberUtils.isNumber(ownBrkNC)){										
					Criteria cr = session.createCriteria(Owner.class)
							.add(Restrictions.like(OwnerCNTS.OWN_FA_CODE, "%"+ownBrkNC));
					
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property(OwnerCNTS.OWN_ID), "id");
					proList.add(Projections.property(OwnerCNTS.OWN_FA_CODE), "faCode");
					proList.add(Projections.property(OwnerCNTS.OWN_NAME), "name");
					cr.setProjection(proList);					
					cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
					
					list = cr.list();
					if(! list.isEmpty()){
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						resultMap.put("list", list);
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record !");
					}
				}else{
					Criteria cr = session.createCriteria(Owner.class)
							.add(Restrictions.like(OwnerCNTS.OWN_NAME, ownBrkNC+"%"));
					
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property(OwnerCNTS.OWN_ID), "id");
					proList.add(Projections.property(OwnerCNTS.OWN_FA_CODE), "faCode");
					proList.add(Projections.property(OwnerCNTS.OWN_NAME), "name");
					cr.setProjection(proList);
					cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
					
					list = cr.list();
					if(! list.isEmpty()){
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						resultMap.put("list", list);
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record !");
					}
				}
			}else if(imageType.equalsIgnoreCase("BRK")){
				if(NumberUtils.isNumber(ownBrkNC)){
					Criteria cr = session.createCriteria(Broker.class)
							.add(Restrictions.like(BrokerCNTS.BRK_FA_CODE, "%"+ownBrkNC));
					
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property(BrokerCNTS.BRK_ID), "id");
					proList.add(Projections.property(BrokerCNTS.BRK_FA_CODE), "faCode");
					proList.add(Projections.property(BrokerCNTS.BRK_NAME), "name");
					cr.setProjection(proList);					
					cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
					
					list = cr.list();
					if(! list.isEmpty()){
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						resultMap.put("list", list);
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record !");
					}
				}else{
					Criteria cr = session.createCriteria(Broker.class)
							.add(Restrictions.like(BrokerCNTS.BRK_NAME, ownBrkNC+"%"));
					
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property(BrokerCNTS.BRK_ID), "id");
					proList.add(Projections.property(BrokerCNTS.BRK_FA_CODE), "faCode");
					proList.add(Projections.property(BrokerCNTS.BRK_NAME), "name");
					cr.setProjection(proList);					
					cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
					
					list = cr.list();
					if(! list.isEmpty()){
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						resultMap.put("list", list);
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "No such record !");
					}
				}
			}
			if(list == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such record !");
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such record !");
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" Exit from getOwnBrkForImgRead()");
		return resultMap;
	}
	
	@Override
	public Map<String, Object> upldExcelFile(byte fileInBytes[],String fileName){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		User crntUser=(User) this.httpSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=null;
		
		try{
			
				Path createpath = Paths.get(bankStmtPath);
				if(! Files.exists(createpath))
					Files.createDirectories(createpath);
				java.sql.Date date;
				
				
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
				Date crntDt=new Date();  
			    System.out.println(formatter.format(crntDt)); 
				
				date=java.sql.Date.valueOf(formatter.format(crntDt));
			//image code
				
			transaction = session.beginTransaction();
			BankStmtUpld bankStmtUpld=new BankStmtUpld();
			System.out.println("bankStmtUpld="+bankStmtUpld);
			bankStmtUpld.setUserCode(crntUser.getUserCode());
			bankStmtUpld.setDate(date);
			Integer bsuId=(Integer) session.save(bankStmtUpld);
			System.out.println("bsuId="+bsuId);
			if(bsuId>0) {
			
					String path = bankStmtPath+"/BankStmt"+bsuId+".xlsx";
					
					bankStmtUpld.setFileName(fileName);
					bankStmtUpld.setFilePath(path);
					
					FileOutputStream out = new FileOutputStream(path);
					out.write(fileInBytes);
					out.close();
				
					
					session.update(bankStmtUpld);
					session.flush();
					session.clear();
					transaction.commit();
					
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("msg", "File is uploaded !");
					logger.info("Image is updated successfully......");
					
					
					
			}else {
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "not saved");
			}
			
			
		}catch(Exception e){
			transaction.rollback();
			e.printStackTrace();
			logger.info("Exception in upldExcelFile : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "File is not uploaded !");
		}
		
		session.close();
		return resultMap;
	}
	
	@Override
	public java.sql.Blob downloadExcel(Session session, int bsuId){
		 java.sql.Blob blob = null;
			try {
				 BankStmtUpld bsu = (BankStmtUpld) session.get(BankStmtUpld.class,bsuId);
									
									File file = new File(bsu.getFilePath());
									 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
									 blob = new SerialBlob(imgInBytes);
			}catch(Exception e) {
				e.printStackTrace();
			}
		
		return blob;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getFileNames(java.sql.Date date){
		
		Map<String, Object> map=new HashMap<>();
		
		Session session=this.sessionFactory.openSession();
		try {
			List<BankStmtUpld> bsuList= session.createCriteria(BankStmtUpld.class)
					.add(Restrictions.eq("date", date)).list();
			
			if(bsuList.isEmpty()) {
				map.put("result", "error");
				map.put("msg", "No record found");
			}else {
				map.put("result", "success");
				map.put("list", bsuList);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			map.put("result", "error");
			map.put("msg", "Exception");
		}
		
		session.clear();
		session.close();
		return map;
	}
	
	
	
	@Override
	public java.sql.Blob downloadImgByCode(Session session, User user, Map<String, String> initParam){
		logger.info("UserID = "+user.getUserId()+" : Enter into downloadImg() : intiParam = "+initParam);
		 java.sql.Blob blob = null;
		 try{
			 String imageType = initParam.get("imageType");
			 if(imageType.equalsIgnoreCase("OWN")){
				 
				 String ownCode = initParam.get("code");
				 Owner owner=(Owner) session.createCriteria(Owner.class)
						 .add(Restrictions.eq("ownCode", ownCode)).list().get(0);
				 
				 List<OwnerImg> imgList = session.createCriteria(OwnerImg.class)
						 .add(Restrictions.eq(OwnerImgCNTS.OWN_ID, owner.getOwnId()))
						 .list();
				 if(! imgList.isEmpty()){
					 OwnerImg img = imgList.get(0);
					 String ownBrkImgType = initParam.get("ownBrkImgType");
					 if(ownBrkImgType.equalsIgnoreCase("PAN")){
						 if(img.getOwnPanImgPath() != null){
							 File file = new File(img.getOwnPanImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);							 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
						 if(img.getOwnDecImgPath() != null){
							 File file = new File(img.getOwnDecImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("CHQ")){
						 if(img.getOwnBnkDetImgPath() != null){
							 File file = new File(img.getOwnBnkDetImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }
				 }
			 }else if(imageType.equalsIgnoreCase("BRK")){
				 String brkCode=initParam.get("code");
				 Broker broker=(Broker) session.createCriteria(Broker.class)
						 .add(Restrictions.eq("brkCode", brkCode)).list().get(0);
				 
				 List<BrokerImg> imgList = session.createCriteria(BrokerImg.class)
						 .add(Restrictions.eq(BrokerImgCNTS.BRK_ID, broker.getBrkId()))
						 .list();
				 if(! imgList.isEmpty()){
					 BrokerImg img = imgList.get(0);
					 String ownBrkImgType = initParam.get("ownBrkImgType");
					 if(ownBrkImgType.equalsIgnoreCase("PAN")){
						 if(img.getBrkPanImgPath() != null){
							 File file = new File(img.getBrkPanImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("DEC")){
						 if(img.getBrkDecImgPath() != null){
							 File file = new File(img.getBrkDecImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }else if(ownBrkImgType.equalsIgnoreCase("CHQ")){
						 if(img.getBrkBnkDetImgPath() != null){
							 File file = new File(img.getBrkBnkDetImgPath());
							 byte imgInBytes[] = FileUtils.readFileToByteArray(file);
							 blob = new SerialBlob(imgInBytes);								 
						 }
					 }
				 }
			 }
		 }catch(Exception e){
			 logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		 }
		 logger.info("UserID = "+user.getUserId()+" : Exit from downloadImg() : IMG = "+blob);
		 return blob;
	}

	
}