'use strict';

var app = angular.module('application');

app.controller('BillCancelCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	$scope.blNo = "";
	$scope.blMap = {};
	$scope.loadingFlag = false;
	$scope.showBill = false;
	$scope.openBrhFlag = true;
	
	$scope.getBlBranch = function(){
		console.log("enter into getBlBranch function");
		var response = $http.post($scope.projectName+'/getBlBranch');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.brhList = data.list;
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.openBrhFlag = false;
		$('div#openBrhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.openBrhFlag = true;
		    }
			});
		
		$('div#openBrhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branch = brh.branchName;
		$scope.brhId = brh.brhId;
		$('div#openBrhId').dialog('close');
	}
	
	
	$scope.getBill = function(){
		console.log("enter into getBill function");
		if(angular.isUndefined($scope.blNo)){
			console.log("please enter a bill no");
		}else{
			$scope.loadingFlag = true;
			var req = {    
					"blNo"  : $scope.blNo,
					"brhId" : $scope.brhId
			};
			var response = $http.post($scope.projectName+'/getBlFrCancel',req);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  if(data.blMap.msg === "success"){
						  $scope.blMap = data.blMap;
						  $scope.showBill = true;
					  }else if(data.blMap.msg === "MR"){
						  $scope.alertToast("Please Cancel the MR of "+$scope.blNo+" bill");
					  }else if(data.blMap.msg === "BF"){
						  $scope.alertToast("Please Cancel the Bill Forwarding of "+$scope.blNo+" bill");
					  }else if(data.blMap.msg === "cancel"){
						  $scope.alertToast("You aleready cancel the "+$scope.blNo+" bill");
					  }
					  $scope.loadingFlag = false;
				  }else{
					  $scope.loadingFlag = false;
					  $scope.alertToast("Server Error");
				  }
			   });
			   response.error(function(data, status, headers, config) {
				   $scope.loadingFlag = false;
				   $scope.errorToast(data.result);
			   });
		}
	}
	
	
	$scope.cancelBill = function(){
		$scope.loadingFlag = true;
		var req = {    
				"blNo"  : $scope.blNo,
				"brhId" : $scope.brhId
		};
		var response = $http.post($scope.projectName+'/cancelBill',req);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				    $scope.blNo = "";
					$scope.blMap = {};
					$scope.loadingFlag = false;
					$scope.showBill = false;
					$scope.alertToast(data.result);
			  }else{
				  $scope.blNo = "";
				  $scope.blMap = {};
			  }
		   });
		   response.error(function(data, status, headers, config) {
			   $scope.loadingFlag = false;
			   $scope.errorToast(data.result);
		   });
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBlBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
}]);