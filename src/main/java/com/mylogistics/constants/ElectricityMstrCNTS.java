package com.mylogistics.constants;

public class ElectricityMstrCNTS {

	public static final String EM_ID = "emId";
	
	public static final String EM_CRN_NO = "emCrnNo";
	
	public static final String EM_SP_NAME = "emSPName";
	
	public static final String EM_PARTICULAR = "emParticular";
	
	public static final String EM_INST_DT = "emInstDt";
	
	public static final String EM_DISC_DT = "emDiscDt";
	
	public static final String EM_ADV_DEP_DT = "emAdvDepDt";
	
	public static final String EM_REF_DT = "emRefDt";
	
	public static final String EM_ADV_DEP_AMT = "emAdvDepAmt";
	
	public static final String EM_REF_AMT = "emRefAmt";
	
	public static final String EM_BILL_DAY = "emBillDay";
	
	public static final String EM_DUE_DAY = "emDueDay";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
