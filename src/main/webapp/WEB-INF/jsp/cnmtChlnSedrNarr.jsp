<div ng-show="operatorLogin || superAdminLogin" title="Branch Stock Information" style="background-color: white;">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
		input.ng-invalid {
   		 	background-color:#B7F3EE;
		}
				
	</style>
	<div style="background-color: white;">		
		<!-- CNMT/CHLN/SEDR Narration Form -->
		<form name="NarrForm" ng-submit="submitNarrForm(NarrForm)" class="col s12 card" style="align: center; padding-top: 40px;">
			<div class="row" style="background-color: white;">
				
				<div class="col s3 input-field" style="width: 17%;">
		    		<select name="type" id="type" ng-model="narr.narrType" ng-init="narr.narrType='cnmt'"  ng-required="true">
						<option value='cnmt' selected="selected">CNMT</option>
						<option value='chln'>CHALLAN</option>
						<option value='sedr'>SEDR</option>						
					</select>
					<label style="color:black;">Type</label>
				</div>
						       
				<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" type ="text" id="code" name="code" ng-model="narr.narrCode" ng-required="true" ng-blur="alreadyNarr()">
					<label style="color:black;">Code</label>
				</div>
				
				<div class="col s3 input-field" style="width: 40%;">					
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="30"  name = "desc" ng-model="narr.narrDesc" ng-required="true"></textarea>
 					<label style="color:black;">Description</label>
				</div>
				
				<div class="col s3 input-field" style="width: 17%;">
	       			<input class="col s12 btn teal white-text" type="submit" id="narrSubmit" name ="narrSubmit" value="Submit" >
	       		</div>
				
			</div>		
		</form>		
	</div>
	
</div>