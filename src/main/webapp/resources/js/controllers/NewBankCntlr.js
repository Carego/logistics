'use strict';

var app = angular.module('application');

app.controller('NewBankCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("NewBankCntlr Started");
	
	$scope.bankNames = [];
	$scope.employees = [];
	$scope.finalSSList = [];
	$scope.finalJSList = [];
	
	$scope.bnkNameDBFlag = true;
	$scope.bnkAddDBFlag = true;
	$scope.openEmp1DBFlag = true;
	$scope.openEmp2DBFlag = true;
	$scope.saveBnkFlag = true;
	$scope.singleSDBFlag = true;
	$scope.jointSDBFlag = true;
	
	$scope.bnkAdd = {};
	/*$scope.bnkEmpCode1 = {};
	$scope.bnkEmpCode2 = {};*/
	$scope.bnkMstr = {};
	
	$scope.bnkMstr.bnkSingleSignatoryList = [];
	$scope.bnkMstr.bnkJointSignatoryList = [];
	
	/*$scope.empName1 = "";
	$scope.empName2 = "";*/

	$scope.getBankNameNEmp = function(){
		console.log("getBankNameNEmp Entered");
		var response = $http.post($scope.projectName+'/getBankNameNEmp');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				$scope.bankNames = data.bankNames;
				$scope.employees = data.employees;
				for ( var i = 0; i < $scope.bankNames.length; i++) {
					console.log("Bank Name: rohit "+$scope.bankNames[i].bnkName);
					console.log("Emp Name: "+$scope.employees[i].empName);
				} 
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.openBnkNameDB = function(){
		console.log("Entered into openBnkNameDB");
		$scope.bnkNameDBFlag = false;
		$('div#bnkNameDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank Name",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bnkNameDBFlag = true;
			}
		});

		$('div#bnkNameDB').dialog('open');
	}
	
	$scope.OpenBnkAddDB = function(){
		$scope.bnkAddDBFlag = false;
			$('div#bnkAddDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Bank Address",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.bnkAddDBFlag = true;
				}
			});

		$('div#bnkAddDB').dialog('open');
	}

	$scope.saveBnkName =  function(bnkName){
		console.log("enter into saveBnkName----->"+bnkName);
		$scope.bnkMstr.bnkName = bnkName;
		$('div#bnkNameDB').dialog('close');
		$scope.bnkNameDBFlag = true;
	}
	
	/*Employee1*/

	/*$scope.openEmp1DB = function(){
		$scope.openEmp1DBFlag = false;
		$('div#openEmp1DBDIV').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Authorise one",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.openEmp1DBFlag = true;
			}
		});

		$('div#openEmp1DBDIV').dialog('open');
	}*/
	
	/*$scope.saveEmp1 =  function(employee){
		console.log("enter into saveEmp1----->"+employee.empName);
		$scope.bnkEmpCode1 = employee;
		$scope.empName1 = employee.empName;
		$('div#openEmp1DBDIV').dialog('close');
		$scope.openEmp1DBFlag = true;	
	}*/
	
	/*Employee2*/

	/*$scope.openEmp2DB = function(){
		$scope.openEmp2DBFlag = false;
		$('div#openEmp2DBDIV').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Authorise one",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.openEmp2DBFlag = true;
			}
		});

		$('div#openEmp2DBDIV').dialog('open');
	}
	
	$scope.saveEmp2 =  function(employee){
		console.log("enter into saveEmp2----->"+employee.empName);
		$scope.bnkEmpCode2 = employee;
		$scope.empName2 = employee.empName;
		$('div#openEmp2DBDIV').dialog('close');
		$scope.openEmp2DBFlag = true;	
	}*/
	
	$scope.saveBnkAdd = function(bnkAddForm) {
		console.log("completeAdd "+$scope.bnkAdd.completeAdd);
		console.log("addCity "+$scope.bnkAdd.addCity);
		console.log("addState "+$scope.bnkAdd.addState);
		console.log("addPin "+$scope.bnkAdd.addPin);
		$('div#bnkAddDB').dialog('close');
		$scope.bnkAddDBFlag = true;
		console.log("CurrentAddressForm.$invalid---"+ bnkAddForm.$invalid);
		if (bnkAddForm.$invalid) {
			if (bnkAddForm.address.$invalid) {
				$scope.alertToast("enter correct address in current address");
			} else if (bnkAddForm.addCity.$invalid) {
				$scope.alertToast("city name should be between 3-15 characters in current address");
			} else if (bnkAddForm.addState.$invalid) {
				$scope.alertToast("enter correct State in current address");
			} else if (bnkAddForm.addPin.$invalid) {
				$scope.alertToast("Pin in Current address should be of 6 digits");
			} 
		} else {
			$scope.alertToast("Your entry is saved Temporarily");
		}
	}

	$('#addPinId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#addPinId').keypress(function(e) {
		if (this.value.length == 6) {
			e.preventDefault();
		}
	});
	
	$('#addressId').keypress(function(e) {
        if (this.value.length == 255) {
            e.preventDefault();
        }
    });
	
	$('#addCityId').keypress(function(e) {
        if (this.value.length == 20) {
            e.preventDefault();
        }
    });
	
	$('#addStateId').keypress(function(e) {
        if (this.value.length == 40) {
            e.preventDefault();
        }
    });
	
	$('#bnkAcNoId').keypress(function(e) {
        if (this.value.length == 20) {
            e.preventDefault();
        }
    });
	
	$('#bnkAcNoId').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57)
        	return false;
    });
	
	$('#bnkBrMinChqId').keypress(function(e) {
        if (this.value.length == 5) {
            e.preventDefault();
        }
    });
	
	$('#bnkBrMinChqId').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57)
        	return false;
    });
	
	$('#bnkBrMaxChqId').keypress(function(e) {
        if (this.value.length == 5) {
            e.preventDefault();
        }
    });
	
	$('#bnkBrMaxChqId').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57)
        	return false;
    });
	
	$('#bnkHoMinChqId').keypress(function(e) {
        if (this.value.length == 5) {
            e.preventDefault();
        }
    });
	
	$('#bnkHoMinChqId').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57)
        	return false;
    });
	
	$('#bnkHoMaxChqId').keypress(function(e) {
        if (this.value.length == 5) {
            e.preventDefault();
        }
    });
	
	$('#bnkHoMaxChqId').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57)
        	return false;
    });
	
	$('#bnkIfscCodeId').keypress(function(e) {
        if (this.value.length == 20) {
            e.preventDefault();
        }
    });
	
	$('#bnkMicrCodeId').keypress(function(e) {
        if (this.value.length == 20) {
            e.preventDefault();
        }
    });
	
	$('#bnkBalanceAmtId').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57)
        	return false;
    });
	
	$('#bnkBalanceAmtId').keypress(function(e) {
        if (this.value.length == 9) {
            e.preventDefault();
        }
    });
	
	/*Final submit*/
	$scope.bankSubmit = function(newBankForm){
		if(newBankForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			if($scope.bnkMstr.bnkAcNo <= 3){
				$scope.alertToast("Account number should be atleast 4 digit");
			}else if ($scope.bnkMstr.bnkIfscCode <= 3) {
				$scope.alertToast("IFSC code should be atleast 4 digit");
			}else{
				console.log("final submittion");
				$scope.saveBnkFlag = false;
				$('div#saveBnkDBDIV').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.saveBnkFlag = true;
					}
				});
				$('div#saveBnkDBDIV').dialog('open');
			}
		}
	};
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveBnkDBDIV').dialog('close');
	}
	
	
	$scope.saveBnk = function(){
		console.log("enter into saveBnk function");
		$('div#saveBnkDBDIV').dialog('close');
		for ( var i = 0; i < $scope.finalSSList.length; i++) {
			var singleSMap = {
					"empName"	: $scope.finalSSList[i].empName,
			};
			$scope.bnkMstr.bnkSingleSignatoryList.push(singleSMap);
		}
		
		for ( var i = 0; i < $scope.finalJSList.length; i++) {
			var jointSMap = {
					"empName"	: $scope.finalJSList[i].empName,
			};
			$scope.bnkMstr.bnkJointSignatoryList.push(jointSMap);
		}
		
		var bMService = {
				"bnkAdd"		: $scope.bnkAdd,
				"bankMstr"		: $scope.bnkMstr
		 };
		console.log("BnkMstr: "+$scope.bnkMstr.bnkName);
		var response = $http.post($scope.projectName+'/submitBankMster',bMService);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast("Success");
				$scope.alertLongToast("bank fa code is: "+data.bankFaCode);
				console.log("submitBankMster: Success");
				$scope.bnkAdd = {};
				$scope.bnkMstr = {};
				$scope.finalSSList = [];
				$scope.finalJSList = [];
				$scope.bnkMstr.bnkJointSignatoryList = [];
				$scope.bnkMstr.bnkSingleSignatoryList = [];
				
			}else{
				console.log("submitBankMster: Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.OpenSingleSList = function(){
		console.log("enter into OpenSingleSList function");
		$scope.singleSDBFlag = false;
		$('div#singleSDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.singleSDBFlag = true;
			}
		});
		$('div#singleSDB').dialog('open');
	}
	
	$scope.saveSingleS = function(employee){
		console.log("enter into saveSingleS function");
		var count = 0;
		
		for(var i=0; i<$scope.finalSSList.length; i++){
			console.log("enter into for loop: "+i);
			if($scope.finalSSList[i].empFaCode === employee.empFaCode){
				count = count + 1;
				console.log("enter into if: "+count);
			}
		}
		
		if(count > 0){
			$scope.alertToast("already exist");
		}else{
			$scope.finalSSList.push(employee);
			$scope.alertToast("value added");
			$('div#singleSDB').dialog('close');
		}
	}
	
	$scope.removeSingleS = function(index){
		console.log("enter into removeSingleS function");
		$scope.finalSSList.splice(index,1);
	}
	
	$scope.OpenJointSList = function(){
		console.log("enter into OpenJointSList function");
		$scope.jointSDBFlag = false;
		$('div#jointSDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.jointSDBFlag = true;
			}
		});
		$('div#jointSDB').dialog('open');
	}
	
	$scope.saveJointS = function(employee){
		var count = 0;
		for(var i=0; i<$scope.finalJSList.length; i++){
			console.log("enter into for loop: "+i);
			if($scope.finalJSList[i].empFaCode === employee.empFaCode){
				count = count + 1;
				console.log("enter into if: "+count);
			}
		}
		
		if(count > 0){
			$scope.alertToast("already exist");
		}else{
			$scope.finalJSList.push(employee);
			$scope.alertToast("value added");
			$('div#jointSDB').dialog('close');
		}
	}
	
	$scope.removeJointS = function(index){
		console.log("enter into removeJointS function");
		$scope.finalJSList.splice(index,1);
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBankNameNEmp();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	console.log("NewBankCntlr Ended");
}]);