'use strict';

var app = angular.module('application');

app.controller('HoldDocsCntlr',['$scope','$location','$http','$filter','AuthorisationService',
                                     function($scope,$location,$http,$filter,AuthorisationService){
	console.log("HoldDocsCntlr");
	$scope.chlnCodeDBFlag=true;
	//$scope.holdFlag=false;
	
	$scope.changeDocs=function(docSer){
		console.log("changeDocs()");
		$scope.docSer=docSer;
		if($scope.docSer !='CH'){
			$scope.onHold="";
		}
		console.log("$scope.docSer="+$scope.docSer);
	}
	
	$scope.checkCode=function(code){
		console.log("checkCode()");
		$scope.docNo=code;
		$scope.onHold="";
		console.log("docNo="+$scope.docNo);
		var varify={
				"docSer":$scope.docSer,
				"docNo":$scope.docNo
		};
		
		var response = $http.post($scope.projectName + '/getDocsFrHld', varify);
		response.success(function(data, status,headers, config) {
			console.log(data);
			console.log(status);
			console.log(headers);
			console.log(config);
					if (data.result === "success") {//inRmrk
						console.log(data);
						$scope.alertToast(data.msg);
						$scope.onHold="Hold";
						//$scope.holdFlag=true;
					}else if (data.result === "inRmrk") {//
						console.log(data);
						$scope.alertToast(data.msg);
						//$scope.onHold="UnHold";
					} else {
						$scope.alertToast(data.msg);
						console.log("No record found");
					}
				});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.holdDocs = function(holdDocsForm){
		console.log("holdDocsForm="+holdDocsForm.$invalid);
		if(holdDocsForm.$invalid){
			$scope.alertToast("Fill form correctly");
		}else{
			var date = $filter('date')(new Date, "yyyy-MM-dd");
			console.log("Date= "+date);
			var sendData={
					"docNo" : $scope.docNo,
					"docSer": $scope.docSer,
					"remark": $scope.remark,
					"date"  : date
			}
			
			var response = $http.post($scope.projectName + '/holdDocs', sendData);
			response.success(function(data, status,headers, config) {
				if(data.result=="success"){
					$scope.alertToast(data.msg);
					$scope.code="";
					$scope.remark="";
				}else if(data.result=="error"){
					$scope.alertToast(data.msg);
				}else{
					$scope.alertToast("There is some problm");
				}
				
			});
			response.error(function(data, status, headers,config) {
				$scope.errorToast(data.result);
			});
			
			
		}
		
	}
	
	
	
}]);
	
	
	