package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.BranchStockDisDet;

public interface BranchStockDisDetDAO {
	
	public int saveBranchStockDisDet(BranchStockDisDet branchStockDisDet);
	
	public List<BranchStockDisDet> getBranchStockDisDetData(String userCode);
	
	/*public List<String> getCnmtStartNo(String dispatchCode);
	
	public List<String> getChlnStartNo(String dispatchCode);
	
	public List<String> getSedrStartNo(String dispatchCode);*/
	
	public List<String> getStartNo(String dispatchCode,String status);
	
	public int updateisVerify(String startNos[], List<Long> totalStartNo);
	
	public boolean isStAlreadyOrdered(List<Long> cnmtList, List<Long> chlnList, List<Long> sedrList);
}
