<div  ng-show="operatorLogin || superAdminLogin">


<div class="row">
	<div class="col s4">
		<input class="validate" type="button" value="Owner"
			ng-click="getOwnerList()">
	</div>

	<div class="col s4">
		<input class="validate" type="button" value="Broker"
			ng-click="getBrokerList()">
	</div>
</div>
<div class="row" ng-show="brokerDB">
	<table class="tblrow">

		<caption class="coltag tblrow">Invalid Accounts For Verification</caption>

		<tr class="rowclr">
			<th class="colclr">S.No</th>
			<th class="colclr">Broker Code</th>
			<th class="colclr">Status</th>
			<th class="colclr">Mobile No</th>
			<th class="colclr">Broker Name</th>
			<th class="colclr">Broker PAN Name</th>
			<th class="colclr">Account Holder Name</th>
			<th class="colclr">Bank Name</th>
			<th class="colclr">IFSC Code</th>
			<th class="colclr">Account No.</th>
			<th class="colclr">View Image</th>
			<th class="colclr">Upload Image</th>
			<th class="colclr">Validate</th>
		</tr>

		<tr ng-repeat="brk in brkList">
			<td class="rowcel">{{$index+1}}</td>
			<td class="rowcel">{{brk.brkCode}}</td>
			<td class="rowcel">{{brk.brkFirmType}}</td>
			<td class="rowcel">{{brk.brkPhNoList}}</td>
			<td class="rowcel">{{brk.brkName}}</td>
			<td class="rowcel">{{brk.brkPanName}}</td>
			<td class="rowcel" ng-click="changeBrkAccHldrName($index,brk)">{{brk.brkAcntHldrName}}</td>
			<td class="rowcel" ng-click="changeBrkBankName($index,brk)">{{brk.brkBnkBranch}}</td>
			<td class="rowcel" ng-click="changeBrkIfscCode($index,brk)">{{brk.brkIfsc}}</td>
			<td class="rowcel" ng-click="changeBrkAccntNo($index,brk)">{{brk.brkAccntNo}}</td>
			<td class="rowcel"><a
				class="btn-floating suffix waves-effect teal" type="button"
				ng-click="showImage(brk)"> <i
					class="mdi-action-visibility white-text"></i>
			</a></td>

			<td class="rowcel"><a
				class="btn-floating suffix waves-effect teal" type="button"
				ng-click="openBrkUploadDB(brk)"> <i
					class="mdi-content-send white-text"></i>
			</a></td>

			<td class="rowcel"><a
				class="btn-floating suffix waves-effect teal" type="button"
				ng-click="validBrkBnkDet(brk)"> <i
					class="mdi-content-send white-text"></i>
			</a></td>

		</tr>
	</table>

</div>



<div class="row" id="brkChqUpldDBId" ng-show="brkChqUpldFlag">
<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="brkChqUpldForm" >
      			
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" name="" file-model="brkChqImage" >
						<label>Choose File for Upload</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadBrkChqImage(brkChqImage)">Upload</a>
					</div>
				
</form>
</div>


<div id="brkBnkHlrNmId" ng-hide="brkBnkHlrNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="brkBnkHldrNameForm"
		ng-submit="saveChngBrkBnkHldrName(brkBnkHldrName)">
		<div class="col s4">
			<input type="text" name="brkBnkHldrName" id="brkBnkHldrId"
				ng-model="brkBnkHldrName">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>


<div id="brkBnkNmId" ng-hide="brkBnkNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="brkBnkNameForm"
		ng-submit="saveChngBrkBnkName(brkBnkName)">
		<div class="col s4">
			<input type="text" name="brkBnkName" id="brkBnkId"
				ng-model="brkBnkName">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>


<div id="brkIfscNmId" ng-hide="brkIfscNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="brkIfscNameForm"
		ng-submit="saveChngBrkIfscName(brkIfscCode)">
		<div class="col s4">
			<input type="text" name="brkIfscName" id="brkIfscId"
				ng-model="brkIfscCode">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>

<div id="brkAccNoNmId" ng-hide="brkAccNoNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="brkAccNoNameForm"
		ng-submit="saveChngBrkAccNo(brkAccNo)">
		<div class="col s4">
			<input type="text" name="brkAccNoName" id="brkAccNoId"
				ng-model="brkAccNo">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>





<div class="row" ng-show="ownerDB">
	<table class="tblrow">

		<caption class="coltag tblrow">Invalid Accounts For Verification</caption>

		<tr class="rowclr">
			<th class="colclr">S.No</th>
			<th class="colclr">Owner Code</th>
			<th class="colclr">Mobile No</th>
			<th class="colclr">Owner Name</th>
			<th class="colclr">Owner PAN Name</th>
			<th class="colclr">Account Holder Name</th>
			<th class="colclr">Bank Name</th>
			<th class="colclr">IFSC Code</th>
			<th class="colclr">Account No.</th>
			<th class="colclr">View Image</th>
			<th class="colclr">Upload Image</th>
			<th class="colclr">Validate</th>
		</tr>

		<tr ng-repeat="own in ownList">
			<td class="rowcel">{{$index+1}}</td>
			<td class="rowcel">{{own.ownCode}}</td>
			<td class="rowcel">{{own.ownPhNoList}}</td>
			<td class="rowcel">{{own.ownName}}</td>
			<td class="rowcel">{{own.ownPanName}}</td>
			<td class="rowcel" ng-click="changeOwnAccHldrName($index,own)">{{own.ownAcntHldrName}}</td>
			<td class="rowcel" ng-click="changeOwnBankName($index,own)">{{own.ownBnkBranch}}</td>
			<td class="rowcel" ng-click="changeOwnIfscCode($index,own)">{{own.ownIfsc}}</td>
			<td class="rowcel" ng-click="changeOwnAccntNo($index,own)">{{own.ownAccntNo}}</td>
			<td class="rowcel"><a
				class="btn-floating suffix waves-effect teal" type="button"
				ng-click="showOwnImage(own)"> <i
					class="mdi-action-visibility white-text"></i>
			</a></td>

			<td class="rowcel"><a
				class="btn-floating suffix waves-effect teal" type="button"
				ng-click="openOwnUploadDB(own)"> <i
					class="mdi-content-send white-text"></i>
			</a></td>

			<td class="rowcel"><a
				class="btn-floating suffix waves-effect teal" type="button"
				ng-click="validOwnBnkDet(own)"> <i
					class="mdi-content-send white-text"></i>
			</a></td>

		</tr>
	</table>

</div>


<div id="ownBnkHlrNmId" ng-hide="ownBnkHlrNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="ownBnkHldrNameForm"
		ng-submit="saveChngOwnBnkHldrName(ownBnkHldrName)">
		<div class="col s4">
			<input type="text" name="ownBnkHldrName" id="ownBnkHldrId"
				ng-model="ownBnkHldrName">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>


<div id="ownBnkNmId" ng-hide="ownBnkNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="ownBnkNameForm"
		ng-submit="saveChngOwnBnkName(ownBnkName)">
		<div class="col s4">
			<input type="text" name="ownBnkName" id="ownBnkId"
				ng-model="ownBnkName">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>


<div id="ownIfscNmId" ng-hide="ownIfscNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="ownIfscNameForm"
		ng-submit="saveChngOwnIfscName(ownIfscCode)">
		<div class="col s4">
			<input type="text" name="ownIfscName" id="ownIfscId"
				ng-model="ownIfscCode">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>

<div id="ownAccNoNmId" ng-hide="ownAccNoNmDB">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		name="ownAccNoNameForm"
		ng-submit="saveChngOwnAccNo(ownAccNo)">
		<div class="col s4">
			<input type="text" name="ownAccNoName" id="ownAccNoId"
				ng-model="ownAccNo">
		</div>
		<div class="input-field col s4 center">
			<input class="validate" type="submit" value="submit">
		</div>
	</form>
</div>

<div class="row" id="ownChqUpldDBId" ng-show="ownChqUpldFlag">
<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="ownChqUpldForm" >
      			
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" name="" file-model="ownChqImage" >
						<label>Choose File for Upload</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadOwnChqImage(ownChqImage)">Upload</a>
					</div>
</form>
</div>

</div>
