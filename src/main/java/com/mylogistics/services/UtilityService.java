package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;

@Component
public class UtilityService {
	
	public void setExceptionMsg (Map<String, Object> map, String msg) {
		map.clear();
		map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		map.put(ConstantsValues.MSG, msg);		
	}
	
	public void setValidationList(Map<String, Object> result, List<ObjectError> objectErrors) {
		if (! objectErrors.isEmpty()) {
			List<String> validationMsg = new ArrayList<>();
			for(ObjectError objectError : objectErrors) {
				validationMsg.add(objectError.getCode());
			}
			result.put("validationList", validationMsg);
		}
	}
	
}