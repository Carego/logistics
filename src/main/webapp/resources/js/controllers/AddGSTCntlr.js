'use strict';

var app = angular.module('application');

app.controller('AddGSTCntlr',['$scope','$location','$http','$window','$filter',
                                function($scope,$location,$http,$window,$filter){


	
	
	$scope.CustomerCodeDBFlag=true;
	$scope.show = "true";
	$scope.customer={};
	
	
	

	

	$scope.OpenCustomerCodeDB = function(){
		$scope.CustomerCodeDBFlag=false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerCodeDB').dialog('open');
	}

	$scope.saveCustomerCode = function(cst){
		$scope.custCode = cst.custCode;
		$scope.custCodeTemp = cst.custFaCode;
		$('div#CustomerCodeDB').dialog('close');
		$scope.CustomerCodeDBFlag=true;
	}

	$scope.getCustomerCodeList = function(){
		console.log("getCustomerCodeData------>");
		var response = $http.post($scope.projectName+'/getCustListFV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custCodeList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in CustomerRepCntlr");
			$scope.errorToast(data);
		});
	}
	

	
	$scope.submitCustomer = function(CustomerForm){
		console.log("enter into Submit function CustomerForm.$invalid--->"+CustomerForm.$invalid);

		if(CustomerForm.$invalid){
			if(CustomerForm.custCodeTemp.$invalid){
				$scope.alertToast("please enter customer code..");
			}else if(CustomerForm.GSTName.$invalid){
				$scope.alertToast("please enter Valid GST Code..");
			}
		}else{
			
			console.log("$scope.custCodeTemp=="+$scope.custCodeTemp+" $scope.custGstNo =="+$scope.custGstNo
					+"$scope.custSAC=="+$scope.custSAC);
			
			var custData = {
					"custFaCode"  : $scope.custCodeTemp,
					"custGstNo"   : $scope.custGstNo,
					"custSAC"     : $scope.custSAC
			};
			
			
			var response = $http.post($scope.projectName+'/updateCustGstNo',custData);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("SuccessFully Updated");
				}else{
					console.log(data);
					$scope.alertToast("There is some problem");
				}
			});
			response.error(function(data, status, headers, config) {
				console.log(data);
				$scope.errorToast("May be The requested resource () is not available");
			});
		}
		
	
	}
	
	
	
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getCustomerCodeList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
	
	

}]);

