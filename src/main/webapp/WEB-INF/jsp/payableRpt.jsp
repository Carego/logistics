<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payable Report</title>
</head>
<body>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<form name="payableForm" ng-submit="payableRpt(fromDt,toDt)">
			<div class="row">
				
				<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="fromDt" name="formDt"  ng-model="fromDt" required>
		       	<label for="code">From Date</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="toDt" name="toDt"  ng-model="toDt" required>
		       	<label for="code">To Date</label>
	            </div>
	            
	             	<div class="col s4 input-field">
		       		<input class="validate" type ="submit">

	            </div>
		    </div>
		</form>
		
			<div class="col s12 center" ng-show="printPayFlag">
			<form method="post" action="getpPayableRptXLSX" enctype="multipart/form-data">
				<input type="submit" id="printXlsId" value="Print XLS" ng-click="printPayFlag=false;">
		  	</form>
		</div>
    </div>	       	
</div>
</body>
</html>