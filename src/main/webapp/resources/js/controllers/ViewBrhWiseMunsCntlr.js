'use strict';

var app = angular.module('application');

app.controller('ViewBrhWiseMunsCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.brhList = [];
	$scope.branchCodeDBFlag = true; 
	$scope.brhWiseMuns = {};
	$scope.actBrId = "";
	$scope.branchName = "";
	
	$scope.displayDB = true;
	
	$scope.getBrhFrMuns = function(){
		console.log("enter into getBrhFrMuns function");
		var response = $http.post($scope.projectName+'/getBrhFrMuns');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.brhList = data.list;
			   }else{
				   $scope.alertToast("Server Error");
				   console.log("error in fetching data from getBrhFrMuns");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.branchCodeDBFlag = false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			title:"Select Branch",
			show: UDShow,
			hide: UDHide,
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	            $scope.branchCodeDBFlag = true;
	        }
		});
	
		$('div#branchCodeDB').dialog('open');
	}
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branchName = brh.branchName;
		$scope.actBrId = brh.brhId;
		$('div#branchCodeDB').dialog('close');
	}
	
	$scope.submit = function(viewMunsForm){
		console.log("enter into submit function = "+ viewMunsForm.$invalid);
		if(viewMunsForm.$invalid){
			$scope.alertToast("please select Branch");
		}else{
			var response = $http.post($scope.projectName+'/viewBrhWiseMuns',$scope.actBrId);
			  response.success(function(data, status, headers, config){
				   if(data.result === "success"){
					   $scope.brhWiseMuns = data.brMuns;
					   $scope.displayDB = false;
					   
				   }else{
					   $scope.alertToast("Server Error");
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		}
	}
	
	if($scope.superAdminLogin === true){
		 $scope.getBrhFrMuns();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 

}]);