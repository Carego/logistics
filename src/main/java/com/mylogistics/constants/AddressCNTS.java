package com.mylogistics.constants;

public class AddressCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
	
	public static final String COMPLETE_ADDRESS = "completeAdd";
	
	public static final String CITY = "addCity";
	
	public static final String ID = "addId";
	
	public static final String STATE = "addState";
	
	public static final String PIN = "addPin";
	
	public static final String TYPE = "addType";
	
	public static final String REF_CODE = "addRefCode";
	
}
