package com.mylogistics.constants;

public class ContractCNTS {
	
	public static final String CONT_ID = "contId";
	
	public static final String CUST_CODE = "custCode";
	
	public static final String CONT_CODE = "contCode";
	
	public static final String CUST_TYPE = "custType";
	
	public static final String CONT_BRANCH = "contBranch";
	
	public static final String CONT_DT = "contDt";
	
	public static final String CONT_START_DT = "contStartDt";
	
	public static final String CONT_END_DT = "contEndDt";
	
	public static final String CONT_TYPE = "contType";
	
	public static final String SERVICE_TAX = "serviceTax";
	
	public static final String SERVICE_TAX_NO = "serviceTaxNo";
	
	public static final String BILLING_BRANCH = "billingBranch";
	
	public static final String PAYMENT_BRANCH = "paymentBranch";
	
	public static final String DETENTION = "detention";
	
	public static final String LATE_PENALTY = "latePenalty";
	
	public static final String LOADING_BY = "loadingBy";
	
	public static final String LOADING_RATE = "loadingRate";
	
	public static final String UNLOADING_BY = "unloadingBy";
	
	public static final String UNLOADING_RATE = "unloadingRate";
	
	public static final String INSURED_BY = "insuredBy";
	
	public static final String SHORT_N_LEAK_ALLOWED = "shortNLeakAllowed";
	
	public static final String REMARK = "remark";
	
	public static final String CUST_ID = "custId";
	
	public static final String IS_VERIFY = "isVerify";

}
