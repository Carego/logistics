'use strict';

var app = angular.module('application');

app.controller('BillSbmsnCntlr',['$scope','$location','$http','$filter','$sce','FileUploadService',
                              function($scope,$location,$http,$filter,$sce,FileUploadService){
	
	$scope.custList = [];
	$scope.customer = {};
	$scope.bfList = [];
	$scope.selBillFrwd = {};
	$scope.billList = []; 
	
	$scope.selCustFlag = true;
	$scope.selBFrwdFlag = true;
	$scope.uploadFlag = false;
	$('#saveId').attr("disabled","disabled");
	
	$scope.getCustomer = function(){
		console.log("enter into getCustomer function");
		var response = $http.post($scope.projectName+'/getCustFrBlSbmsn');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				 $scope.custList = data.list;
			  }else{
				  $scope.alertToast("There is no Customer for bill submission");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.selectCust = function(){
		console.log("enter into selectCust function");
		$scope.selCustFlag = false;
		$('div#selCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCustFlag = true;
		    }
			});
		
		$('div#selCustId').dialog('open');	
	}
	
	
	$scope.saveCustomer = function(cust){
		console.log("enter into saveCustomer function");
		$scope.customer = cust;
		$scope.bfList = [];
		$scope.selBillFrwd = {};
		$scope.billList = [];
		var req = {
				"custId" :  $scope.customer.custId 
		};
		
		var response = $http.post($scope.projectName+'/getBlFrwdFrSbmsn' , req);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.bfList = data.list;
				  $('div#selCustId').dialog('close');	
			  }else{
				 $scope.alertToast(data.msg);
				 $('div#selCustId').dialog('close');
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.selBillFr = function(){
		console.log("enter into selBillFr funciton");
		 
		$scope.selBFrwdFlag = false;
		$('div#selBFrwdId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select BillForwarding No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selBFrwdFlag = true;
		    }
			});
		
		$('div#selBFrwdId').dialog('open');	
	}
	
	
	$scope.saveBFwrd = function(bfwrd){
		console.log("enter into saveBFwrd function");
		$scope.selBillFrwd = bfwrd;
		$scope.billList = [];
		console.log("bfDt="+$scope.selBillFrwd.creationTS);
		$scope.bfDt=$filter('date')($scope.selBillFrwd.creationTS, "yyyy-MM-dd");
		console.log("bfDt="+$scope.bfDt);
		
		if($scope.bfDt<"2018-07-01"){
			$('#saveId').removeAttr("disabled");
		}else{
			$('#saveId').attr("disabled","disabled");
		}
		
		var req = {
			"bfId" : $scope.selBillFrwd.bfId	
		};
		
		var response = $http.post($scope.projectName+'/getBillFrSbmsn' , req);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $('div#selBFrwdId').dialog('close');	
				  $scope.billList = data.list;
				  console.log("size of $scope.billList = "+$scope.billList.length);
				  $scope.uploadFlag = true;
			  }else{
				  $scope.selBillFrwd = {};
				  $('div#selBFrwdId').dialog('close');
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.billFrwdSubmit = function(billFrwdForm){
		console.log("enter into billFrwdSubmit function = "+billFrwdForm.$invalid);
		if(billFrwdForm.$invalid){
			$scope.alertToast("Please fill the correct form");
		}else{
			
			if($scope.selBillFrwd.bfRecDt<$scope.bfDt){
				$scope.alertToast("Submission date can't be less than forwarding date");
				return;
			}
			
			$('#saveId').attr("disabled","disabled");
			
			var response = $http.post($scope.projectName+'/submitBillSbmsn' , $scope.selBillFrwd);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					 $scope.alertToast(data.result);
						$scope.customer = {};
						$scope.bfList = [];
						$scope.selBillFrwd = {};
						$scope.billList = []; 
						$scope.uploadFlag = false;
						$('#saveId').attr("disabled","disabled");
				  }else{
					$('#saveId').removeAttr("disabled");  
					$scope.alertToast(data.msg);
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
					$('#saveId').removeAttr("disabled");
			   });
		}
	}
	
	
	$scope.uploadBfImage = function(img){
		console.log("enter into uploadArImage function = "+$scope.bfImage+""+img);
		var file = img;
		console.log("file ====>> "+file);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
			$scope.uploadImg = false;
		}else{
			console.log('file is ' + JSON.stringify(file) + "bfId"+$scope.selBillFrwd.bfId);
			var uploadUrl = $scope.projectName+"/uploadBfImage";
			FileUploadService.uploadBfFileToUrl(file, uploadUrl,$scope.selBillFrwd.bfId);
			$scope.uploadImg = true;
			 $('#saveId').removeAttr("disabled");
			console.log("file save on server");
		}
	}
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getCustomer();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);