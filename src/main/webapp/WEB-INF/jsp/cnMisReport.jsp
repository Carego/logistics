<div ng-show="adminLogin || superAdminLogin || operatorLogin">

<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	
		<form name="branchForm" ng-submit=submitBranch(branchForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       	<label for="code">Branch</label>	
		       	</div>
		       	
		       	<!-- <div class="col s3 input-field">
		       		<input class="validate" type ="text" id="fromCnmtId" name ="fromCnmtNoName" ng-model="fromCnmtNo" >
		       	<label for="code">from CNMT No(optional)</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="toCnmtId" name ="toCnmtNoName" ng-model="toCnmtNo" ng-required="true" >
		       	<label for="code">to CNMT No</label>	
		       	</div> -->

				<div class="col s6 input-field">
					<input class="validate" type="submit" value="submit">
				</div>
			</div>
			
			<div class="row">
				<div class="col s12 center">
					<input class="validate" type="button" id="getXlBtnId" value="download excel" disabled="disabled" ng-click="downloadExl()">
				</div>
			</div>
		</form>
	
</div>

	

<div class="col s12" id="exportable">
	<table class="table-condensed" >		
		
		<tr>
			<td>BRANCH NAME</td>
			<td>{{branch.branchName}}</td>
		</tr>
		
		<tr>
			<td>Missing Cnmt</td>
		</tr>
			
		<tr ng-repeat="misCnmt in misCnmtList">
		 	<td>{{misCnmt}}</td>
        </tr>
	</table>
	
	
	<!-- <div class="row">
		<div class="col s12 center">
			<input type="submit" value="Submit" ng-click="submit()" />
		</div>
		
		<div class="col s12 center">
			<input type="button" value="download excel" ng-click="downloadExl()" />
		</div>
	</div> -->

</div>

<div id ="branchDB" ng-hide="branchDBFlag" >
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by branch Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>


</div>