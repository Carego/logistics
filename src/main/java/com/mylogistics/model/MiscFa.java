package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "miscfa")
public class MiscFa {

	@Id
	@Column(name="miscFaId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int miscFaId;
	
	@Column(name="miscFaName")
	private String miscFaName;
	
	@Column(name="miscFaCode")
	private String miscFaCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",insertable=false, updatable=true)
	private Calendar creationTS;

	public int getMiscFaId() {
		return miscFaId;
	}

	public void setMiscFaId(int miscFaId) {
		this.miscFaId = miscFaId;
	}

	public String getMiscFaName() {
		return miscFaName;
	}

	public void setMiscFaName(String miscFaName) {
		this.miscFaName = miscFaName;
	}

	public String getMiscFaCode() {
		return miscFaCode;
	}

	public void setMiscFaCode(String miscFaCode) {
		this.miscFaCode = miscFaCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
}
