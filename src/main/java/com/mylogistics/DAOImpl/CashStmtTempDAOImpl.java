package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CashStmtTempDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.services.CsTempNCsListService;

public class CashStmtTempDAOImpl implements CashStmtTempDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public CashStmtTempDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<CashStmtTemp> getPendingCS(int cssId){
		 System.out.println("Entered into getPendingCS function---"+cssId);
		 List<CashStmtTemp> list = new ArrayList<>();
		 List<CashStmtTemp> reqList = new ArrayList<>();
		 List<CashStmtStatus> cssList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(CashStmtStatus.class);
			 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cssId));
			 cssList = cr.list();
			 if(!cssList.isEmpty()){
				 CashStmtStatus cashStmtStatus = cssList.get(0);
				 
				 List<Branch> brList = new ArrayList<>();
				 cr=session.createCriteria(Branch.class);
				 cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,cashStmtStatus.getbCode()));
				 brList = cr.list();
				 Branch branch = brList.get(0);
				 
				 cr=session.createCriteria(CashStmtTemp.class);
				 list = cr.list();
				 if(!list.isEmpty()){
					 for(int i=0;i<list.size();i++){
						 int res = list.get(i).getCsDt().compareTo(cashStmtStatus.getCssDt());
						// System.out.println("value of res = "+res);
						 boolean dateRes = false;
						// if(res == 0 || res < 0){
						 if(res == 0 ){ 
							 dateRes = true;
						 }
						 if(list.get(i).getCsBrhFaCode().equalsIgnoreCase(branch.getBranchFaCode()) && dateRes == true 
								 && list.get(i).isCsIsView() == false){
							 reqList.add(list.get(i));
						 }
					 }
				 }
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return reqList;  
	 }


	@Override
	@Transactional
	public int updateCashStmtTemp(CsTempNCsListService csTempNcsListService) {
		System.out.println("CashStmtTempDAOImpl.updateCashStmtTemp()");
		int temp = 0;
		
		CashStmtTemp cashStmtTemp = csTempNcsListService.getCashStmtTemp();
		List<CashStmt> cashStmtList = csTempNcsListService.getCashStmtList();
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			//update cashStmtTemp
			cashStmtTemp.setCsUpdate(true);
			session.update(cashStmtTemp);
			
			//update CashStmt
			if (!cashStmtList.isEmpty()) {
				for (CashStmt cashStmt : cashStmtList) {
					cashStmt.setCsUpdate(true);
					session.update(cashStmt);
				}
			}
			
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
	}
	
}
