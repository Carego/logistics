package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.constants.RateByKmCNTS;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.RateByKm;

public class RateByKmDAOImpl implements RateByKmDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public  RateByKmDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveRBKM(RateByKm rateByKm){
		   int temp;
		   try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 session.save(rateByKm);
				 transaction.commit();
				 session.flush();
				 temp = 1;
			}
			catch(Exception e){
				e.printStackTrace();
				temp = -1;
			}
		   session.clear();
		   session.close();
		   return temp;
		 }
	
	@Transactional
	 @SuppressWarnings("unchecked")
	public List<RateByKm> getRateByKm(String code){
		 List<RateByKm> rateByKms = new ArrayList<RateByKm>();
		 System.out.println("Enter into getRateByKm function--- "+code);
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(RateByKm.class);   
	         cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE,code));
	         rateByKms =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return rateByKms;
	}
	 
	@Transactional
	 public RateByKm deleteRBKM(String code){
			System.out.println("enter into deleteRBKM function------------"+code);
			
			RateByKm rateByKm = new RateByKm();
			
		    try{
		    	session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(RateByKm.class);
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE,code));
				 
				 rateByKm = (RateByKm) cr.list().get(0);
				 session.delete(rateByKm);
				 transaction.commit();
				 session.flush();
				
				 System.out.println("RateByKm Data is deleted successfully");	 
		    }catch (Exception e){
				e.printStackTrace();
			}
		    session.clear();
		    session.close();
			 return rateByKm;
	   }
	
	
	@Transactional
	public List<RateByKm> getRegContForView(String contCode,String stnCode){
			List<RateByKm> rbkmList = new ArrayList<RateByKm>();
		    try{
		    	 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(RateByKm.class);
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_TO_STATION,stnCode));
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE,contCode));
				 rbkmList = cr.list();
				 /*System.out.println("**********"+rbkmList.size());
				 if(!rbkmList.isEmpty()){
					 rate = rbkmList.get(0).getRbkmRate();
				 }*/
				 transaction.commit();
				 session.flush();
				
				 System.out.println("RateByKm Data is deleted successfully");	 
		    }catch (Exception e){
				e.printStackTrace();
			}
		    session.clear();
		    session.close();
			return rbkmList;
	   }
	
	
	@Transactional
	public List<RateByKm> getRbkmRate(String stateCode , double cnmtKm , String contCode){
			System.out.println("enter into getRbkmRate function------------"+cnmtKm);
			List<RateByKm> rbkmList = new ArrayList<RateByKm>();
			double rate = 0.0;
		    try{
		    	 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(RateByKm.class);
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_STATE_CODE,stateCode));
				 /*cr.add(Restrictions.ge(RateByKmCNTS.RBKM_FROM_KM,cnmtKm)); 
				 cr.add(Restrictions.le(RateByKmCNTS.RBKM_TO_KM,cnmtKm));*/
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE,contCode));
				 rbkmList = cr.list();
				 /*System.out.println("**********"+rbkmList.size());
				 if(!rbkmList.isEmpty()){
					 rate = rbkmList.get(0).getRbkmRate();
				 }*/
				 transaction.commit();
				 session.flush();
				
				 System.out.println("RateByKm Data is deleted successfully");	 
		    }catch (Exception e){
				e.printStackTrace();
			}
		    session.clear();
		    session.close();
			return rbkmList;
	   }
	
	
	
	@Transactional
	public List<RateByKm> getRbkmForChln(String contractCode , String fromStation , String toStation , String stateCode){
			System.out.println("enter into getRbkmForChln function");
			List<RateByKm> rbkmList = new ArrayList<RateByKm>();
		    try{
		    	 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(RateByKm.class);
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_STATE_CODE,stateCode));
				 cr.add(Restrictions.ge(RateByKmCNTS.RBKM_FROM_STATION,fromStation)); 
				 cr.add(Restrictions.le(RateByKmCNTS.RBKM_TO_STATION,toStation));
				 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE,contractCode));
				 rbkmList = cr.list();
				 /*System.out.println("**********"+rbkmList.size());
				 if(!rbkmList.isEmpty()){
					 rate = rbkmList.get(0).getRbkmRate();
				 }*/
				 transaction.commit();
				 session.flush();
		    }catch (Exception e){
				e.printStackTrace();
			}
		    session.clear();
		    session.close();
			return rbkmList;
	   }
	
	/*@Transactional
	public List<RateByKm> getContForView(String contCode , String stnCode){
		 System.out.println("Enter into getContForView function");
		 List<RateByKm> rbkmList = new ArrayList<RateByKm>();
		 String station = null;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(RateByKm.class);
			 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE,contCode));
			 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_TO_STATION,stnCode));
			 rbkmList = cr.list();
			 if(!rbkmList.isEmpty()){
				 station = rbkmList.get(0).getRbkmToStation();
			 }
			 ContToStn contToStn = (ContToStn) cr.list();
			 rate = contToStn.getCtsRate();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			
		 }
		 session.close();
		 return rbkmList;
	 }*/
}
