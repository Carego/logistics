package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchRightsDAO;
import com.mylogistics.constants.BranchRightsCNTS;
import com.mylogistics.model.BranchRights;

public class BranchRightsDAOImpl implements BranchRightsDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public BranchRightsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 public int saveBrRights(BranchRights branchRights){
		 System.out.println("enter into saveBrRights function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(branchRights);
			 transaction.commit();
			 temp = 1;
			 session.flush();
			 
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<BranchRights> getBranchRightsIsAllow(){
		 System.out.println("Entered into getBranchRightsIsAllow function---");
		 List<BranchRights> list = new ArrayList<BranchRights>();
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(BranchRights.class);
			   cr.add(Restrictions.eq(BranchRightsCNTS.BRH_ADV_ALLOW,"yes"));
			   list =cr.list();
			   session.flush();
		 }catch(Exception e){
				e.printStackTrace();
		 }
		 session.clear();
         session.close();
		 return list;  
	 }
	 
	 @SuppressWarnings("unchecked")
     @Transactional
     public int updateBrRightsIsAllowNo(int contIdsInt[]){
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
         try{
             session = this.sessionFactory.openSession();
             for(int i=0;i<contIdsInt.length;i++){
                transaction = session.beginTransaction();
                BranchRights branchRights  = new BranchRights();
                List<BranchRights> list = new ArrayList<BranchRights>();
                Criteria cr=session.createCriteria(BranchRights.class);
                cr.add(Restrictions.eq(BranchRightsCNTS.BRH_ID,contIdsInt[i]));
                list =cr.list();
                branchRights = list.get(0);
                branchRights.setBrhAdvAllow("no");
                branchRights.setCreationTS(calendar);
                session.update(branchRights);
                transaction.commit();
             }
             session.flush();
             temp = 1;
        }catch(Exception e){
             e.printStackTrace();
             temp = -1;
        }
         session.clear();
        session.close();
        return temp;
     }
}
		

