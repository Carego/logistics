package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.MultipleStation;

@Repository
public interface MultipleStationDAO {
	
	//Sanjana: saves the multiple station to database
	public int saveMultipleStation(MultipleStation multipleStation);
	
	public List<MultipleStation> getMultipleStaion(String code);

	public int deleteStation(MultipleStation multipleStation);
}
