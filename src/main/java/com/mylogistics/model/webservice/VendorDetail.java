package com.mylogistics.model.webservice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "vendor_detail_app")
public class VendorDetail {
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vdId")
	private Integer vdId;
	
	@Column(name = "vendorName")
	@NotNull(message = "Vendor name can not be null !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	@NotEmpty(message = "Vendor name can not be empty !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	private String vendorName;
	
	@Column(name = "vendorCode")	
	private String vendorCode;
	
	@Column(name = "typeOfVendor")
	@NotNull(message = "Vendor type can not be null !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	@NotEmpty(message = "Vendor type can not be empty !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	private String typeOfVendor;
	
	@Column(name = "mobileNo")
	@NotNull(message = "Vendor name can not be null !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	@NotEmpty(message = "Vendor name can not be empty !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	@Length(max = 13, min = 10,  message = "Enter valid mobile no !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	private String mobileNo;
	
	@Column(name = "rate")
	@NotNull(message = "Vendor name can not be null !",  groups = {SelectedVendor.class, UnSelectedVendor.class})	
	private Float rate;
	
	@Column(name = "isSelected")
	@NotNull(message = "Vendor isSelected can not be null !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	private Boolean isSelected;
	
	@Column(name = "reason")
	@NotNull(message = "Selected reason can not be null !", groups = SelectedVendor.class)
	@NotEmpty(message = "Selected reason can not be null !", groups = SelectedVendor.class)
	private String reason;
	
	@Column(name = "city")
	@NotNull(message = "City can not be null !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	@NotBlank(message = "City can not be blank !", groups = {SelectedVendor.class, UnSelectedVendor.class})
	private String city;
	
	@ManyToOne
	@JsonIgnore
	private LoadingModel loadingModel;

	public Integer getVdId() {
		return vdId;
	}
	public void setVdId(Integer vdId) {
		this.vdId = vdId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getTypeOfVendor() {
		return typeOfVendor;
	}
	public void setTypeOfVendor(String typeOfVendor) {
		this.typeOfVendor = typeOfVendor;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Float getRate() {
		return rate;
	}
	public void setRate(Float rate) {
		this.rate = rate;
	}
	public Boolean getIsSelected() {
		return isSelected;
	}
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public LoadingModel getLoadingModel() {
		return loadingModel;
	}
	public void setLoadingModel(LoadingModel loadingModel) {
		this.loadingModel = loadingModel;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}


	public interface SelectedVendor{		
	}
	public interface UnSelectedVendor{		
	}
	
}