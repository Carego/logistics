'use strict';

var app = angular.module('application');

app.controller('CnmtChlnSedrCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	console.log("CnmtChlnSedrCntlr starts.......");
	
	$scope.narr = {};
	
	$scope.submitNarrForm = function(NarrForm){
		console.log("Enter into submitNarrForm() : "+NarrForm.$valid);
		
		if(NarrForm.$invalid){
			
		}else{
			var response = $http.post($scope.projectName + '/createNarr', $scope.narr);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				$scope.narr = {};
				$scope.narr.narrType = "cnmt";
				if(data.result === "sucess"){
					$scope.alertToast(data.msg);
				}else{
					$scope.alertToast(data.msg);					
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /createNarr !");
				$scope.alertToast("Please contact us !");
			});
		}
		
		console.log("Exit from submitNarrForm()");
	}
	
	$scope.alreadyNarr = function(){
		console.log("Enter into alreadyNarr() : Type = "+$scope.narr.type+ " : Code = "+$scope.narr.narrCode);
		
		if(angular.isUndefined($scope.narr.narrCode) || $scope.narr.code === ""){
			$scope.alertToast("Enter code !");			
		}else{
			var response = $http.post($scope.projectName + '/alreadyNarr', $scope.narr);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "error"){
					$scope.alertToast(data.msg);
					$scope.narr = {};
					$scope.narr.narrType = "cnmt";
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /alreadyNarr !");
				$scope.alertToast("Please contact us !");
			});
		}
		
		console.log("Exit from alreadyNarr()");
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){		
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("CnmtChlnSedrCntlr ends.......");
	 
}]);