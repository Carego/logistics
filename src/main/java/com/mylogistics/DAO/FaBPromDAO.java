package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmt;
import com.mylogistics.model.FaBProm;
import com.mylogistics.services.BPVoucherService;
import com.mylogistics.services.VoucherService;

@Repository
public interface FaBPromDAO {

	public Map<String,Object> saveBPVoucByCash(VoucherService voucherService, BPVoucherService bpVoucherService);
	
	public Map<String,Object> saveBPVoucByChq(VoucherService voucherService, BPVoucherService bpVoucherService);
	
	public List<FaBProm> getSubFileDetails(List<CashStmt> reqCsList);
}
