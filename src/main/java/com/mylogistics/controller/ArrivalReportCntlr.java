package com.mylogistics.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.IIOException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.Acknowledge_ValidationDao;
import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.model.Acknowledge_Validation;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.BranchMUStatsBk;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Memo;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.MemoService;
//import java.util.Date;
import com.mylogistics.services.SedrService;

@Controller
public class ArrivalReportCntlr {

	@Autowired
	private ArrivalReportDAO arrivalReportDAO;

	@Autowired
	private ChallanDAO challanDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;

	@Autowired
	private BranchSStatsDAO branchSStatsDAO;

	@Autowired
	private BranchMUStatsDAO branchMUStatsDAO;

	@Autowired
	private Cnmt_ChallanDAO cnmt_ChallanDAO;

	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private DailyContractDAO daliyContractDAO;
	
	@Autowired
	private Acknowledge_ValidationDao acknowledge_ValidationDao;

	//private ModelService modelService = new ModelService();
	private List<Map<String,Object>> arList = new ArrayList<>();

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	private String cnmtImgPath = "/var/www/html/Erp_Image/CNMT";
	
	public static Logger logger = Logger.getLogger(ArrivalReportCntlr.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Blob blob=null;

//	@RequestMapping(value="/submitar",method = RequestMethod.POST)
//	public @ResponseBody Object submitAR(@RequestBody SedrService sedrService)throws Exception{
//		System.out.println("enter into submitAR fucntion");
//		logger.info("Enter into /submitar()...");
//		
//		Map<String,String> map = new HashMap<String,String>();
//		User currentUser = (User)httpSession.getAttribute("currentUser");
//
//		
//		Session session=null;
//		Transaction transaction=null;
//		
//		try{
//			session=this.sessionFactory.openSession();
//			transaction=session.beginTransaction();
//			
//			ArrivalReport arrivalReport = sedrService.getArrivalReport();
//			//System.out.println(arrivalReport.getArIssueDt());
//			arrivalReport.setArIssueDt(arrivalReport.getArIssueDt());//By Kamal
//			arrivalReport.setArRemWtShrtg(arrivalReport.getArWtShrtg());
//			arrivalReport.setArRemDrRcvrWt(arrivalReport.getArDrRcvrWt());
//			arrivalReport.setArRemLateDelivery(arrivalReport.getArLateDelivery());
//			arrivalReport.setArRemLateAck(arrivalReport.getArLateAck());
//			arrivalReport.setArRemExtKm(arrivalReport.getArExtKm());
//			arrivalReport.setArRemOvrHgt(arrivalReport.getArOvrHgt());
//			arrivalReport.setArRemDetention(arrivalReport.getArDetention());
//			arrivalReport.setArRemUnloading(arrivalReport.getArUnloading());
//					
//			List<Map<String,Object>> cnWSList = sedrService.getCnWSList();
//			String contType = sedrService.getContType();
//			arrivalReport.setView(false);
//			arrivalReport.setUserCode(currentUser.getUserCode());
//			arrivalReport.setbCode(currentUser.getUserBranchCode());
//			arrivalReport.setBranchCode(currentUser.getUserBranchCode());
//			
//			if(arrivalReport.getArRepType().equalsIgnoreCase("Manual")){
//				logger.info("Enter into Manual Block ....");
//				String arCode =arrivalReport.getArCode();
//				int serialNo = Integer.parseInt(arCode);
//				double daysLeft = getSedrDaysLast();
//				double average = getAvgMonthlyUsageForSedr();
//
//				System.out.println("daysLeft-----------------"+daysLeft);
//				System.out.println("ArCode-----------------"+arCode);
//				System.out.println("SerialNo--------------"+serialNo);
//				String brsLeafDetStatus = "sedr";
//				String actualSNo = String.valueOf(serialNo);
//				BranchStockLeafDet branchStockLeafDet = branchStockLeafDetDAO.deleteBSLD(actualSNo,brsLeafDetStatus,session);
//				//System.out.println("deleteBSLD-------------------"+deleteBSLD);
//
//				if(branchStockLeafDet != null){
//					BranchSStats branchSStats = new BranchSStats();
//					branchSStats.setBrsStatsBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
//					branchSStats.setBrsStatsType("sedr");
//					branchSStats.setBrsStatsSerialNo(branchStockLeafDet.getBrsLeafDetSNo());
//					branchSStats.setUserCode(currentUser.getUserCode());
//					branchSStats.setbCode(currentUser.getUserBranchCode());
//					branchSStats.setBrsStatsDt(arrivalReport.getArDt());
//					int result = branchSStatsDAO.saveBranchSStats(branchSStats,session);
//					System.out.println("after saving result = "+result);
//
//					Calendar cal = Calendar.getInstance();
//					Date currentDate = arrivalReport.getArDt();
//					cal.setTime(currentDate);
//					System.out.println("Current date is-----"+currentDate);
//					int currentMonth = cal.get(Calendar.MONTH)+1;
//					System.out.println("Current month is-----"+currentMonth);
//					int currentYear = cal.get(Calendar.YEAR);
//					System.out.println("Current year is-----"+currentYear);
//					if(result==1){
//						BranchMUStats branchMUStats = new BranchMUStats();
//						branchMUStats.setBmusDaysLast(daysLeft);
//						System.out.println("----daysLeft"+daysLeft);
//						branchMUStats.setBmusStType("sedr");
//						
//						if(Double.isInfinite(average) || Double.isNaN(average)){
//							branchMUStats.setBmusAvgMonthUsage(0.0);
//						}else{
//							branchMUStats.setBmusAvgMonthUsage(average);
//						}
//						
//						System.out.println("----average"+average);
//						branchMUStats.setbCode(currentUser.getUserBranchCode());
//						System.out.println("----branchCode"+currentUser.getUserBranchCode());
//						branchMUStats.setUserCode(currentUser.getUserCode());
//						System.out.println("----userCode"+currentUser.getUserCode());
//						branchMUStats.setBmusBranchCode(branchStockLeafDet.getBrsLeafDetBrCode());
//						System.out.println("----brCode"+branchStockLeafDet.getBrsLeafDetBrCode());
//						branchMUStats.setBmusDt(currentDate);
//
//
//						long rowsCount = branchMUStatsDAO.totalBMUSRows("sedr",session);
//						System.out.println("Rows count for sedr is-----------"+rowsCount);
//						if(rowsCount!=-1){
//							if(rowsCount==0){
//								branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats,session);
//							}else{
//								List<BranchMUStats> listStats = branchMUStatsDAO.getLastData(branchStockLeafDet.getBrsLeafDetBrCode(),"sedr",session);
//								BranchMUStats branchMUStats2 = listStats.get(0);
//								Date lastDate = listStats.get(0).getBmusDt();
//								int lastId = listStats.get(0).getBmusId();
//
//								System.out.println("The last date is ==========="+lastDate);
//								Calendar calender = Calendar.getInstance();
//								calender.setTime(lastDate);
//								int lastMonth = calender.get(Calendar.MONTH)+1;
//								System.out.println("lastMonth---------------"+lastMonth);
//								int lastyear = calender.get(Calendar.YEAR);
//								System.out.println("lastyear---------------"+lastyear);
//								if((currentMonth==lastMonth) && (currentYear==lastyear)){
//									if(Double.isInfinite(average) || Double.isNaN(average)){
//										branchMUStats.setBmusAvgMonthUsage(0.0);
//									}else{
//										branchMUStats.setBmusAvgMonthUsage(average);
//									}
//									branchMUStats.setBmusDt(currentDate);
//									branchMUStats.setBmusDaysLast(daysLeft);
//									branchMUStats.setBmusId(lastId);
//									System.out.println("Before update"+daysLeft+currentDate+average);
//									branchMUStatsDAO.updateBMUS(branchMUStats,session);
//									System.out.println("After update");
//								}else if((currentMonth!=lastMonth) && (currentYear!=lastyear)){
//									System.out.println("Enter into function with id ---------"+lastId);
//									int delete = branchMUStatsDAO.deleteBMUS(lastId,session);
//									System.out.println("After deleting old entry---------------------"+delete);
//									if(delete==1){
//										int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats,session);
//										System.out.println("--------------------After saving new entry"+save);
//										if(save == 1){
//											BranchMUStatsBk bk = new BranchMUStatsBk();
//											bk.setbCode(branchMUStats2.getbCode());
//											if(Double.isInfinite(branchMUStats2.getBmusAvgMonthUsage()) || Double.isNaN(branchMUStats2.getBmusAvgMonthUsage())){
//												bk.setBmusAvgMonthUsage(0.0);
//											}else{
//												bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());	
//											}
//											bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
//											bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
//											bk.setBmusDt(branchMUStats2.getBmusDt());
//											bk.setBmusId(branchMUStats2.getBmusId());
//											bk.setBmusStType(branchMUStats2.getBmusStType());
//											bk.setUserCode(branchMUStats2.getUserCode());
//											branchMUStatsDAO.saveBMUSBK(bk,session);
//											System.out.println("---------------After saving into backup");
//										}
//									}		
//								}else if((currentMonth!=lastMonth) && (currentYear==lastyear)){
//									System.out.println("Enter into function with id ---------"+lastId);
//									int delete = branchMUStatsDAO.deleteBMUS(lastId,session);
//									System.out.println("After deleting old entry---------------------"+delete);
//									if(delete==1){
//										int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats,session);
//										System.out.println("--------------------After saving new entry"+save);
//										if(save == 1){
//											BranchMUStatsBk bk = new BranchMUStatsBk();
//											bk.setbCode(branchMUStats2.getbCode());
//											if(Double.isInfinite(branchMUStats2.getBmusAvgMonthUsage()) || Double.isNaN(branchMUStats2.getBmusAvgMonthUsage())){
//												bk.setBmusAvgMonthUsage(0.0);
//											}else{
//												bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());	
//											}
//											bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
//											bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
//											bk.setBmusDt(branchMUStats2.getBmusDt());
//											bk.setBmusId(branchMUStats2.getBmusId());
//											bk.setBmusStType(branchMUStats2.getBmusStType());
//											bk.setUserCode(branchMUStats2.getUserCode());
//											branchMUStatsDAO.saveBMUSBK(bk,session);
//											System.out.println("---------------After saving into backup");
//										}
//									}			
//								}
//							}	
//						}	
//					} 
//				}
//
//				
//				/*Blob blob = modelService.getBlob();
//				arrivalReport.setArImage(blob);*/
//				int cnRes = 0;
//				
//				//try{
//					Path path = Paths.get(cnmtImgPath);
//					if(! Files.exists(path))
//						Files.createDirectories(path);
////				}catch(Exception e){
////					System.out.println("Error in creating direcotry : "+e);
////					logger.error("Error in creating directory : "+e);
////				}
//				if(!arList.isEmpty()){
//					for(int i=0;i<arList.size();i++){
//						int cnmtId = (int) arList.get(i).get("id");
//						logger.info("SELECT CNMT ID : "+cnmtId);
//						Blob blob = (Blob) arList.get(i).get("blob");
//						if(cnmtId>0 && blob!=null){
//							cnRes = cnmtDAO.addCnmtImg(cnmtId , blob, cnmtImgPath,session);
//						}
//					}
//				}else{
//					logger.info("CNMT image is not uploaded due to Not selecting cnmt in arrivalreport : ");
//				}
//				
//				if(cnRes > 0){
//					int temp = arrivalReportDAO.saveArrivalReportToDB(arrivalReport,session);
//					if (temp>=0) {
//						
//						if(!cnWSList.isEmpty()){
//							for(int i=0;i<cnWSList.size();i++){
//								cnmtDAO.updateCnWS(cnWSList.get(i),session);
//							}
//						}
//						
//						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
//					} else {
//						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//					}
//				}else{
//					
//					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//					throw new IIOException("Exception in addCnmtImg()");
//					
//				}
//				
//			}else if(arrivalReport.getArRepType().equalsIgnoreCase("Computerized")){
//				logger.info("Enter into Computerized Block ....");
//				//Blob blob = modelService.getBlob();
//				//arrivalReport.setArImage(blob);
//				//int temp = arrivalReportDAO.saveArrivalReportToDB(arrivalReport);
//				int cnRes = 0;
//				
//				// Local Test
//				//cnmtImgPath = httpServletRequest.getRealPath("/CNMT");			
//				//try{
//					Path path = Paths.get(cnmtImgPath);
//					if(! Files.exists(path))
//						Files.createDirectories(path);
////				}catch(Exception e){
////					System.out.println("Error in creating direcotry : "+e);
////					logger.error("Error in creating directory : "+e);
////				}
//				
//				if(!arList.isEmpty()){
//					for(int i=0;i<arList.size();i++){
//						int cnmtId = (int) arList.get(i).get("id");
//						Blob blob = (Blob) arList.get(i).get("blob");
//						if(cnmtId>0 && blob!=null){
//							cnRes = cnmtDAO.addCnmtImg(cnmtId , blob, cnmtImgPath,session);
//						}
//					}
//				}
//				
//				if(cnRes > 0){
//					int temp = arrivalReportDAO.saveCompARToDB(arrivalReport,session);
//					System.out.println("value of arCode = "+arrivalReport);
//					if (temp>=0) {
//						
//						if(!cnWSList.isEmpty()){
//							for(int i=0;i<cnWSList.size();i++){
//								cnmtDAO.updateCnWS(cnWSList.get(i),session);
//							}
//						}
//						
//						
//						map.put("compArCode",arrivalReport.getArCode());
//						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
//					} else {
//						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//					}
//				}else{
//					System.out.println("ERROR in cnmt image");
//					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//				}
//				
//			}else{
//				System.out.println("invalid Arrival Report Type");
//				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//			}
//			//modelService.setBlob(null);
//			arList.clear();
//
//			
//			transaction.commit();
//		}catch(Exception e){
//			e.printStackTrace();
//			logger.info("Excepion in saving arrival = "+e);
//			transaction.rollback();
//		}finally{
//			session.clear();
//			session.close();
//		}
//		
//				return map;    
//	}
//	
	
	
	@RequestMapping(value="/submitar",method = RequestMethod.POST)
	public @ResponseBody Object submitAR(@RequestBody SedrService sedrService)throws Exception{
		System.out.println("enter into submitAR fucntion");
		logger.info("Enter into /submitar()...");
		
		ArrivalReport arrivalReport = sedrService.getArrivalReport();
		//System.out.println(arrivalReport.getArIssueDt());
		arrivalReport.setArIssueDt(arrivalReport.getArIssueDt());//By Kamal
		arrivalReport.setArRemWtShrtg(arrivalReport.getArWtShrtg());
		arrivalReport.setArRemDrRcvrWt(arrivalReport.getArDrRcvrWt());
		arrivalReport.setArRemLateDelivery(arrivalReport.getArLateDelivery());
		arrivalReport.setArRemLateAck(arrivalReport.getArLateAck());
		arrivalReport.setArRemExtKm(arrivalReport.getArExtKm());
		arrivalReport.setArRemOvrHgt(arrivalReport.getArOvrHgt());
		arrivalReport.setArRemDetention(arrivalReport.getArDetention());
		arrivalReport.setArRemUnloading(arrivalReport.getArUnloading());
				
		List<Map<String,Object>> cnWSList = sedrService.getCnWSList();
		String contType = sedrService.getContType();
		arrivalReport.setView(false);
		Map<String,String> map = new HashMap<String,String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		arrivalReport.setUserCode(currentUser.getUserCode());
		arrivalReport.setbCode(currentUser.getUserBranchCode());
		arrivalReport.setBranchCode(currentUser.getUserBranchCode());
		
		if(arrivalReport.getArRepType().equalsIgnoreCase("Manual")){
			logger.info("Enter into Manual Block ....");
			String arCode =arrivalReport.getArCode();
			int serialNo = Integer.parseInt(arCode);
			double daysLeft = getSedrDaysLast();
			double average = getAvgMonthlyUsageForSedr();

			System.out.println("daysLeft-----------------"+daysLeft);
			System.out.println("ArCode-----------------"+arCode);
			System.out.println("SerialNo--------------"+serialNo);
			String brsLeafDetStatus = "sedr";
			String actualSNo = String.valueOf(serialNo);
			BranchStockLeafDet branchStockLeafDet = branchStockLeafDetDAO.deleteBSLD(actualSNo,brsLeafDetStatus);
			//System.out.println("deleteBSLD-------------------"+deleteBSLD);

			if(branchStockLeafDet != null){
				BranchSStats branchSStats = new BranchSStats();
				branchSStats.setBrsStatsBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
				branchSStats.setBrsStatsType("sedr");
				branchSStats.setBrsStatsSerialNo(branchStockLeafDet.getBrsLeafDetSNo());
				branchSStats.setUserCode(currentUser.getUserCode());
				branchSStats.setbCode(currentUser.getUserBranchCode());
				branchSStats.setBrsStatsDt(arrivalReport.getArDt());
				int result = branchSStatsDAO.saveBranchSStats(branchSStats);
				System.out.println("after saving result = "+result);

				Calendar cal = Calendar.getInstance();
				Date currentDate = arrivalReport.getArDt();
				cal.setTime(currentDate);
				System.out.println("Current date is-----"+currentDate);
				int currentMonth = cal.get(Calendar.MONTH)+1;
				System.out.println("Current month is-----"+currentMonth);
				int currentYear = cal.get(Calendar.YEAR);
				System.out.println("Current year is-----"+currentYear);
				if(result==1){
					BranchMUStats branchMUStats = new BranchMUStats();
					branchMUStats.setBmusDaysLast(daysLeft);
					System.out.println("----daysLeft"+daysLeft);
					branchMUStats.setBmusStType("sedr");
					
					if(Double.isInfinite(average) || Double.isNaN(average)){
						branchMUStats.setBmusAvgMonthUsage(0.0);
					}else{
						branchMUStats.setBmusAvgMonthUsage(average);
					}
					
					System.out.println("----average"+average);
					branchMUStats.setbCode(currentUser.getUserBranchCode());
					System.out.println("----branchCode"+currentUser.getUserBranchCode());
					branchMUStats.setUserCode(currentUser.getUserCode());
					System.out.println("----userCode"+currentUser.getUserCode());
					branchMUStats.setBmusBranchCode(branchStockLeafDet.getBrsLeafDetBrCode());
					System.out.println("----brCode"+branchStockLeafDet.getBrsLeafDetBrCode());
					branchMUStats.setBmusDt(currentDate);


					long rowsCount = branchMUStatsDAO.totalBMUSRows("sedr");
					System.out.println("Rows count for sedr is-----------"+rowsCount);
					if(rowsCount!=-1){
						if(rowsCount==0){
							branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
						}else{
							List<BranchMUStats> listStats = branchMUStatsDAO.getLastData(branchStockLeafDet.getBrsLeafDetBrCode(),"sedr");
							BranchMUStats branchMUStats2 = listStats.get(0);
							Date lastDate = listStats.get(0).getBmusDt();
							int lastId = listStats.get(0).getBmusId();

							System.out.println("The last date is ==========="+lastDate);
							Calendar calender = Calendar.getInstance();
							calender.setTime(lastDate);
							int lastMonth = calender.get(Calendar.MONTH)+1;
							System.out.println("lastMonth---------------"+lastMonth);
							int lastyear = calender.get(Calendar.YEAR);
							System.out.println("lastyear---------------"+lastyear);
							if((currentMonth==lastMonth) && (currentYear==lastyear)){
								if(Double.isInfinite(average) || Double.isNaN(average)){
									branchMUStats.setBmusAvgMonthUsage(0.0);
								}else{
									branchMUStats.setBmusAvgMonthUsage(average);
								}
								branchMUStats.setBmusDt(currentDate);
								branchMUStats.setBmusDaysLast(daysLeft);
								branchMUStats.setBmusId(lastId);
								System.out.println("Before update"+daysLeft+currentDate+average);
								branchMUStatsDAO.updateBMUS(branchMUStats);
								System.out.println("After update");
							}else if((currentMonth!=lastMonth) && (currentYear!=lastyear)){
								System.out.println("Enter into function with id ---------"+lastId);
								int delete = branchMUStatsDAO.deleteBMUS(lastId);
								System.out.println("After deleting old entry---------------------"+delete);
								if(delete==1){
									int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
									System.out.println("--------------------After saving new entry"+save);
									if(save == 1){
										BranchMUStatsBk bk = new BranchMUStatsBk();
										bk.setbCode(branchMUStats2.getbCode());
										if(Double.isInfinite(branchMUStats2.getBmusAvgMonthUsage()) || Double.isNaN(branchMUStats2.getBmusAvgMonthUsage())){
											bk.setBmusAvgMonthUsage(0.0);
										}else{
											bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());	
										}
										bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
										bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
										bk.setBmusDt(branchMUStats2.getBmusDt());
										bk.setBmusId(branchMUStats2.getBmusId());
										bk.setBmusStType(branchMUStats2.getBmusStType());
										bk.setUserCode(branchMUStats2.getUserCode());
										branchMUStatsDAO.saveBMUSBK(bk);
										System.out.println("---------------After saving into backup");
									}
								}		
							}else if((currentMonth!=lastMonth) && (currentYear==lastyear)){
								System.out.println("Enter into function with id ---------"+lastId);
								int delete = branchMUStatsDAO.deleteBMUS(lastId);
								System.out.println("After deleting old entry---------------------"+delete);
								if(delete==1){
									int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
									System.out.println("--------------------After saving new entry"+save);
									if(save == 1){
										BranchMUStatsBk bk = new BranchMUStatsBk();
										bk.setbCode(branchMUStats2.getbCode());
										if(Double.isInfinite(branchMUStats2.getBmusAvgMonthUsage()) || Double.isNaN(branchMUStats2.getBmusAvgMonthUsage())){
											bk.setBmusAvgMonthUsage(0.0);
										}else{
											bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());	
										}
										bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
										bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
										bk.setBmusDt(branchMUStats2.getBmusDt());
										bk.setBmusId(branchMUStats2.getBmusId());
										bk.setBmusStType(branchMUStats2.getBmusStType());
										bk.setUserCode(branchMUStats2.getUserCode());
										branchMUStatsDAO.saveBMUSBK(bk);
										System.out.println("---------------After saving into backup");
									}
								}			
							}
						}	
					}	
				} 
			}

			
			/*Blob blob = modelService.getBlob();
			arrivalReport.setArImage(blob);*/
			int cnRes = 0;
			
			try{
				Path path = Paths.get(cnmtImgPath);
				if(! Files.exists(path))
					Files.createDirectories(path);
			}catch(Exception e){
				System.out.println("Error in creating direcotry : "+e);
				logger.error("Error in creating directory : "+e);
			}
			if(!arList.isEmpty()){
				for(int i=0;i<arList.size();i++){
					int cnmtId = (int) arList.get(i).get("id");
					logger.info("SELECT CNMT ID : "+cnmtId);
					Blob blob = (Blob) arList.get(i).get("blob");
					int chlnId=challanDAO.getChlnId(arrivalReport.getArchlnCode());
					if(cnmtId>0 && blob!=null){
						cnRes = cnmtDAO.addCnmtImgN(cnmtId ,chlnId, blob, cnmtImgPath);
					}
				}
			}else{
				logger.info("CNMT image is not uploaded due to Not selecting cnmt in arrivalreport : ");
			}
			
			if(cnRes > 0){
				int temp = arrivalReportDAO.saveArrivalReportToDB(arrivalReport);
				if (temp>=0) {
					
					if(!cnWSList.isEmpty()){
						for(int i=0;i<cnWSList.size();i++){
							cnmtDAO.updateCnWS(cnWSList.get(i));
						}
					}
					
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				} else {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
		}else if(arrivalReport.getArRepType().equalsIgnoreCase("Computerized")){
			logger.info("Enter into Computerized Block ....");
			//Blob blob = modelService.getBlob();
			//arrivalReport.setArImage(blob);
			//int temp = arrivalReportDAO.saveArrivalReportToDB(arrivalReport);
			int cnRes = 0;
			
			// Local Test
			//cnmtImgPath = httpServletRequest.getRealPath("/CNMT");			
			try{
				Path path = Paths.get(cnmtImgPath);
				if(! Files.exists(path))
					Files.createDirectories(path);
			}catch(Exception e){
				System.out.println("Error in creating direcotry : "+e);
				logger.error("Error in creating directory : "+e);
			}
			
			if(!arList.isEmpty()){
				for(int i=0;i<arList.size();i++){
					int cnmtId = (int) arList.get(i).get("id");
					Blob blob = (Blob) arList.get(i).get("blob");
					int chlnId=challanDAO.getChlnId(arrivalReport.getArchlnCode());
					if(cnmtId>0 && blob!=null){
						cnRes = cnmtDAO.addCnmtImgN(cnmtId ,chlnId, blob, cnmtImgPath);
					}
				}
			}
			
			if(cnRes > 0){
				int temp = arrivalReportDAO.saveCompARToDB(arrivalReport);
				System.out.println("value of arCode = "+arrivalReport);
				if (temp>=0) {
					
					if(!cnWSList.isEmpty()){
						for(int i=0;i<cnWSList.size();i++){
							cnmtDAO.updateCnWS(cnWSList.get(i));
						}
					}
					
					
					map.put("compArCode",arrivalReport.getArCode());
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				} else {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				System.out.println("ERROR in cnmt image");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
		}else{
			System.out.println("invalid Arrival Report Type");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		//modelService.setBlob(null);
		arList.clear();
		return map;    
	}

	
	

	@RequestMapping(value = "/updateIsViewar", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewar(@RequestBody String selection) {

		String contids[] = selection.split(",");

		int temp = arrivalReportDAO.updateIsViewARTrue(contids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/arDetails", method = RequestMethod.POST)
	public @ResponseBody Object challanDetails(@RequestBody String arCode) {
		//ArrivalReport arrivalReport = new ArrivalReport();
		System.out.println("enter into challanDetails function");
		List<ArrivalReport> arList = arrivalReportDAO.getARList(arCode);
		Map<String, Object> map = new HashMap<String, Object>();
		//Map<String,Object> arMap = new HashMap<String, Object>();

		System.out.println("Controller called after retrieving list");


		/*if(!arList.isEmpty()){

			arMap.put(ArrivalReportCNTS.AR_ID,arList.get(0).getArId());
			arMap.put(ArrivalReportCNTS.AR_CODE,arList.get(0).getArCode());
			arMap.put(ArrivalReportCNTS.AR_DATE,arList.get(0).getArDt());
			arMap.put(ArrivalReportCNTS.AR_RCV_WEIGHT,arList.get(0).getArRcvWt());
			arMap.put(ArrivalReportCNTS.AR_PACKAGE,arList.get(0).getArPkg());
			arMap.put(ArrivalReportCNTS.AR_UNLOADING,arList.get(0).getArUnloading());
			arMap.put(ArrivalReportCNTS.AR_CLAIM,arList.get(0).getArClaim());
			arMap.put(ArrivalReportCNTS.AR_DETENTION,arList.get(0).getArDetention());
			arMap.put(ArrivalReportCNTS.AR_OTHER,arList.get(0).getArOther());
			arMap.put(ArrivalReportCNTS.AR_LATE_DELIVERY,arList.get(0).getArLateDelivery());
			arMap.put(ArrivalReportCNTS.AR_LATE_ACKNOWLEDGEMENT,arList.get(0).getArLateAck());
			arMap.put(ArrivalReportCNTS.AR_REP_DATE,arList.get(0).getArRepDt());
			arMap.put(ArrivalReportCNTS.AR_REP_TIME,arList.get(0).getArRepTime());
			arMap.put(ArrivalReportCNTS.USER_CODE,arList.get(0).getUserCode());
			arMap.put(ArrivalReportCNTS.AR_CHLN_CODE,arList.get(0).getArchlnCode());
			arMap.put(ArrivalReportCNTS.CREATION_TS,arList.get(0).getCreationTS());
			arMap.put(ArrivalReportCNTS.USER_BRANCH_CODE,arList.get(0).getbCode());
			arMap.put(ArrivalReportCNTS.IS_VIEW,arList.get(0).isView());

			Blob blob = arList.get(0).getArImage();
			if(blob == null){
				map.put("image","no");
			}else{
				map.put("image","yes");
			}

			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("arrivalReport", arMap);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/

		if(!arList.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("arrivalReport", arList.get(0));
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		
		return map;	
	}

	@RequestMapping(value = "/saveeditar", method = RequestMethod.POST)
	public @ResponseBody Object EditChallanSubmit(@RequestBody ArrivalReport arrivalReport) {

		int temp=arrivalReportDAO.updateAR(arrivalReport);
		Map<String, Object> map = new HashMap<String, Object>();

		if(temp>0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/uploadarImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadCnmtImage(@RequestParam("file") MultipartFile file, @RequestParam("id") int cnmtId)throws Exception {
		System.out.println("enter into uploadCnmtImage function === "+cnmtId);
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> resMap = new HashMap<>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			//modelService.setBlob(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			resMap.put("id",cnmtId);
			resMap.put("blob",blob);
			arList.add(resMap);
			//modelService.setBlob(blob); 
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/getArChlnCode", method = RequestMethod.POST)
	public @ResponseBody Object getArChlnCode(@RequestBody String brhCode) {
		System.out.println("enter into getArChlnCode function");
		//User currentUser = (User) httpSession.getAttribute("currentUser");
		//List<Challan> chlnList = challanDAO.getAllChallan();
		//List<Challan> chlnList = challanDAO.getAllChlnByBrh(brhCode);
		List<Challan> chlnList = challanDAO.getChlnFrSedrByBrh(brhCode);
		List<Map<String,Object>> finalList = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String, Object>();

		if(!chlnList.isEmpty()){
			for(int i=0;i<chlnList.size();i++){
				Map<String,Object> chlnMap = new HashMap<String, Object>();
				chlnMap.put(ChallanCNTS.CHALLAN_CODE,chlnList.get(i).getChlnCode());
				chlnMap.put(ChallanCNTS.CHLN_TOTAL_WT,chlnList.get(i).getChlnTotalWt());
				chlnMap.put(ChallanCNTS.CHLN_NO_OF_PKG,chlnList.get(i).getChlnNoOfPkg());
				chlnMap.put(ChallanCNTS.CHLN_DT,chlnList.get(i).getChlnDt());
				chlnMap.put(ChallanCNTS.CHLN_TIME_ALLOW,chlnList.get(i).getChlnTimeAllow());
				chlnMap.put(ChallanCNTS.CHLN_FREIGHT,chlnList.get(i).getChlnFreight());
				chlnMap.put(ChallanCNTS.CHLN_CHG_WT,chlnList.get(i).getChlnChgWt());
				chlnMap.put(ChallanCNTS.CHLN_LRY_NO,chlnList.get(i).getChlnLryNo());
				finalList.add(chlnMap);
			}

			map.put("list",finalList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}


	@RequestMapping(value = "/getArChlnByChlnCode", method = RequestMethod.POST)
	public @ResponseBody Object getArChlnByChlnCode(@RequestBody String chlnCode) {
		System.out.println("enter into getArChlnCode function");
		//User currentUser = (User) httpSession.getAttribute("currentUser");
		//List<Challan> chlnList = challanDAO.getAllChallan();
		//List<Challan> chlnList = challanDAO.getAllChlnByBrh(brhCode);
		List<Challan> chlnList = challanDAO.getChallanByCodeFrAr(chlnCode);
		List<Map<String,Object>> finalList = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String, Object>();

		if(!chlnList.isEmpty()){
			for(int i=0;i<chlnList.size();i++){
				Map<String,Object> chlnMap = new HashMap<String, Object>();
				chlnMap.put(ChallanCNTS.CHALLAN_CODE,chlnList.get(i).getChlnCode());
				chlnMap.put(ChallanCNTS.CHLN_TOTAL_WT,chlnList.get(i).getChlnTotalWt());
				chlnMap.put(ChallanCNTS.CHLN_NO_OF_PKG,chlnList.get(i).getChlnNoOfPkg());
				chlnMap.put(ChallanCNTS.CHLN_DT,chlnList.get(i).getChlnDt());
				chlnMap.put(ChallanCNTS.CHLN_TIME_ALLOW,chlnList.get(i).getChlnTimeAllow());
				chlnMap.put(ChallanCNTS.CHLN_FREIGHT,chlnList.get(i).getChlnFreight());
				chlnMap.put(ChallanCNTS.CHLN_CHG_WT,chlnList.get(i).getChlnChgWt());
				chlnMap.put(ChallanCNTS.CHLN_LRY_NO,chlnList.get(i).getChlnLryNo());
				finalList.add(chlnMap);
			}

			map.put("list",finalList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/viewar", method = RequestMethod.POST)
	public @ResponseBody Object customerDetails(@RequestBody String arCode){
		ArrivalReport ar = new ArrivalReport();	   	   
		List<ArrivalReport> list = arrivalReportDAO.getArrivalReport(arCode);
		Map<String,Object> map = new HashMap<String , Object>();
		Map<String,Object> arMap = new HashMap<String, Object>();

		if(!list.isEmpty()){

			arMap.put(ArrivalReportCNTS.AR_ID,list.get(0).getArId());
			arMap.put(ArrivalReportCNTS.AR_CODE,list.get(0).getArCode());
			arMap.put(ArrivalReportCNTS.AR_DATE,list.get(0).getArDt());
			arMap.put(ArrivalReportCNTS.AR_RCV_WEIGHT,list.get(0).getArRcvWt());
			arMap.put(ArrivalReportCNTS.AR_PACKAGE,list.get(0).getArPkg());
			arMap.put(ArrivalReportCNTS.AR_UNLOADING,list.get(0).getArUnloading());
			arMap.put(ArrivalReportCNTS.AR_CLAIM,list.get(0).getArClaim());
			arMap.put(ArrivalReportCNTS.AR_DETENTION,list.get(0).getArDetention());
			arMap.put(ArrivalReportCNTS.AR_OTHER,list.get(0).getArOther());
			arMap.put(ArrivalReportCNTS.AR_LATE_DELIVERY,list.get(0).getArLateDelivery());
			arMap.put(ArrivalReportCNTS.AR_LATE_ACKNOWLEDGEMENT,list.get(0).getArLateAck());
			arMap.put(ArrivalReportCNTS.AR_REP_DATE,list.get(0).getArRepDt());
			arMap.put(ArrivalReportCNTS.AR_REP_TIME,list.get(0).getArRepTime());
			arMap.put(ArrivalReportCNTS.USER_CODE,list.get(0).getUserCode());
			arMap.put(ArrivalReportCNTS.AR_CHLN_CODE,list.get(0).getArchlnCode());
			arMap.put(ArrivalReportCNTS.CREATION_TS,list.get(0).getCreationTS());
			arMap.put(ArrivalReportCNTS.BRANCH_CODE,list.get(0).getBranchCode());

			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("ar", arMap);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;		 			
	}

	@RequestMapping(value="/getArrivalRepList",method = RequestMethod.POST)
	public @ResponseBody Object getArrivalRepList(){

		System.out.println("enter into getArrivalRepList  function");
		List<String> arList= arrivalReportDAO.getArCode();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!arList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", arList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	@RequestMapping(value="/getARCodeList",method = RequestMethod.POST)
	public @ResponseBody Object getARCodeList(){
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<BranchStockLeafDet> arCodeList= branchStockLeafDetDAO.getCodeList(branchCode,"sedr");
		System.out.println("---------->"+arCodeList.size());
		Map<String,Object> map = new HashMap<String,Object>();
		if (!arCodeList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", arCodeList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}


	public double getSedrDaysLast() {
		System.out.println("==============sedrPerDayUsed");
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar =Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("current date is ========"+date);
		calendar.add(Calendar.MONTH, -1);
		Date endDate = new Date(calendar.getTime().getTime());
		System.out.println("end date is ========"+endDate);
		double sedrLeft=0;
		User currentUser = (User)httpSession.getAttribute("currentUser");
		double daysLeft = 0;
		double sedrPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsed(date,endDate,"sedr");
		System.out.println("==============sedrPerDayUsed"+sedrPerDayUsed);
		if(sedrPerDayUsed>0){
			sedrLeft = branchStockLeafDetDAO.getLeftStationary(currentUser.getUserBranchCode(),"sedr");
			daysLeft=sedrLeft/sedrPerDayUsed;
		}
		System.out.println("--------------------------sedrLeft"+sedrLeft);

		System.out.println("==============daysLeft"+daysLeft);
		return daysLeft;
	}

	public double getAvgMonthlyUsageForSedr() {

		System.out.println("------Enter into getAvgMonthlyUsage function");

		Date todayDate = new Date(new java.util.Date().getTime());
		Date firstEntryDate = branchSStatsDAO.getFirstEntryDate("sedr");
		double average=0.00;

		if(firstEntryDate != null){
			Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = new Date(calendar.getTime().getTime());

			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));

			double sedrForOneMnth = branchSStatsDAO.getNoOfStationary(todayDate, oneMonthPrevDate,"sedr");
			double sedrForScndMnth = branchSStatsDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate,"sedr");
			double sedrForThirdMnth = branchSStatsDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate,"sedr");
			double sedrBwFrstAndScndMnth = branchSStatsDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate,"sedr");
			double sedrBwFrstAndOneMnth=branchSStatsDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate,"sedr");
			double sedrBwTodayAndFrstDate=branchSStatsDAO.getNoOfStationary(todayDate, firstEntryDate,"sedr");



			if(days>=90){
				average = ((sedrForOneMnth*3) + (sedrForScndMnth*2) + sedrForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
				average = (((sedrForOneMnth*3) + (sedrForScndMnth*2) + (sedrBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){
				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				average = (((sedrForOneMnth*2) +  (sedrBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				System.out.println("------daysBwFrstAndToday = "+daysBwFrstAndToday);
				if(daysBwFrstAndToday == 0){
					average = sedrBwTodayAndFrstDate*30;
				}else{
					average = ((sedrBwTodayAndFrstDate/daysBwFrstAndToday)*30);
				}	 
			}
		}	

		return average;
	}

	@RequestMapping(value="/getBranchDataForAR",method = RequestMethod.POST)
	public @ResponseBody Object getBranchDataForAR(){
		System.out.println("enter into getBranchDataForAR function");
		//User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!branchList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",branchList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;		
	}

	/*@RequestMapping(value = "/editArrRep", method = RequestMethod.POST)
	public @ResponseBody Object editArrRep(@RequestBody ArrivalReport ar){
		System.out.println("Entered into editArrRep of controller----");

		Map<String,String> map = new HashMap<String, String>();
		Blob blob = modelService.getBlob();
		ar.setArImage(blob);
		int temp=arrivalReportDAO.updateAr(ar);
		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		System.out.println("Code reached here---------------->");

		return map;
	}*/


	@RequestMapping(value = "/checkCnmtImage", method = RequestMethod.POST)
	public @ResponseBody Object checkCnmtImage(@RequestBody String chlnCode){
		System.out.println("Entered into checkCnmtImage of controller----"+chlnCode);        
		Map<String,Object> map = new HashMap<String,Object>();
		List<Challan> chlnList = challanDAO.getChallanList(chlnCode);
		int count = 0;
		if(!chlnList.isEmpty()){
			int chlnId = chlnList.get(0).getChlnId();
			System.out.println("************challan_Id = "+chlnId);
			List<Integer> cnmtIdList = cnmt_ChallanDAO.getCnmtIdByChlnId(chlnId);
			System.out.println("cnmts against this challan --->"+cnmtIdList);
			
			List<Map<String,Object>> resList = new ArrayList<>();
			
			if(!cnmtIdList.isEmpty()){
				for(int i=0;i<cnmtIdList.size();i++){
					Map<String,Object> resMap = new HashMap<>();
					List<Cnmt> cnmtList = cnmtDAO.getCnmtById(cnmtIdList.get(i));
					System.out.println(i+" cnmtList.get().getCmId() = "+cnmtList.get(0).getCmId());
					int cmId = cnmtList.get(0).getCmId();
					if(cmId > 0){
						resMap.put("cnmtImg","yes");
						System.out.println("cnmt "+cnmtList.get(0).getCnmtId()+" have images");
					}else{
						count = count + 1;
						resMap.put("cnmtImg","no");
					}
					
					String contact = cnmtList.get(0).getContractCode();
					String subCont = contact.substring(0,3);
					if(subCont.equalsIgnoreCase("dly")){
						List<DailyContract> dlyList = daliyContractDAO.getDailyContract(contact);
						if(!dlyList.isEmpty()){
							resMap.put("contType",dlyList.get(0).getDlyContType());
						}
					}else if(subCont.equalsIgnoreCase("reg")){
						List<RegularContract> regList = regularContractDAO.getRegularContract(contact);
						if(!regList.isEmpty()){
							resMap.put("contType",regList.get(0).getRegContType());
						}
					}else{
						System.out.println("invalid contract of "+cnmtList.get(0).getCnmtCode()+" cnmt");
					}
					
					/*Blob blob = cnmtList.get(i).getCnmtImage();
					Blob confBlob = cnmtList.get(i).getCnmtConfirmImage();
					System.out.println("blob----->"+blob);
					System.out.println("confBlob------>"+confBlob);*/
					resMap.put("cnmt",cnmtList.get(0));
					/*resMap.put("cnmtCode",cnmtList.get(0).getCnmtCode());
					resMap.put("cnmtId",cnmtList.get(0).getCnmtId());*/
					
					/*if(blob == null && confBlob == null){
						count = count + 1;
						resMap.put("cnmtImg","no");
					}else{
						resMap.put("cnmtImg","yes");
						System.out.println("cnmt "+cnmtList.get(i).getCnmtId()+" have images");
					}*/
					
					resList.add(resMap);
				}
				System.out.println("%%%%%%%%%%value of count = "+count);	
				/*map.put("cnmtNo",cnmtIdList.size());*/
				map.put("blankImage",count);
				map.put("resList",resList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		} else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}  
	
	
	
	@RequestMapping(value = "/calLateDelFrLA", method = RequestMethod.POST)
	public @ResponseBody Object calLateDelFrLA(@RequestBody Map<String,Date> clientMap){
		System.out.println("Entered into calLateDelFrLA of controller----");        
		Map<String,Object> map = new HashMap<String,Object>();
		Date sedrDt = clientMap.get("arDt");
		Date arIssueDt = clientMap.get("arIssueDt");
		Date chlnDt = clientMap.get("chlnDt");//arIssueDt
		
		System.out.println("sedrDt = "+sedrDt);
		System.out.println("chlnDt = "+chlnDt);
		
		long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;
		int diff1 = (int)((sedrDt.getTime() - chlnDt.getTime()) / MILLISECONDS_IN_DAY);
		System.out.println("diff1 = "+diff1);
		
		//int diff2 = (int)((new Date(new java.util.Date().getTime()).getTime() - chlnDt.getTime()) / MILLISECONDS_IN_DAY);
		int diff2 = (int)((arIssueDt.getTime() - chlnDt.getTime()) / MILLISECONDS_IN_DAY);
		System.out.println("diff2 = "+diff2);
		
		map.put("diff1",diff1);
		map.put("diff2",diff2);
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}	

	@RequestMapping(value="/sedrDtlFrCncl", method=RequestMethod.POST)
	public @ResponseBody Object sedrDtlFrCncl(@RequestBody Map<String,String> sedrDtl){
		System.out.println("sedrDtlFrCncl()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = arrivalReportDAO.sedrDtlFrCncl(sedrDtl);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getMemoNo", method=RequestMethod.POST)
	public @ResponseBody Object getMemoNo(){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+" : Enter into /getMemeNo()");
		Session session = this.sessionFactory.openSession();
		try{
			String branchCode = user.getUserBranchCode();
			Memo memo = arrivalReportDAO.getLastMemoByBranch(session, user, branchCode);
			String memoNo = "";
			if(memo == null)
				memoNo = branchCode + "00000";
			else
				memoNo = memo.getMemoNo();
						
			memoNo = String.valueOf(Long.parseLong(memoNo) + 1);
			
			if(branchCode.length() == 1)
				memoNo = "0" + memoNo;
			
			memo = null;
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("memoNo", memoNo);			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Memo no. not found !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from /getMemeNo()");
		return resultMap;
	}
	
	@RequestMapping(value="/getArDetailForMemo", method=RequestMethod.POST)
	public @ResponseBody Object getArDetailForMemo(@RequestBody String arNo){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+" : Enter into /getArDetailForMemo() : ArNo = "+arNo);
		Session session = this.sessionFactory.openSession();
		try{
			List<ArrivalReport> arList = arrivalReportDAO.getArDetailForMemo(session, user, arNo);
			
			if(arList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "AR not found !");
				logger.info("UserID = "+user.getUserId()+" : Ar not found !");
			}else{
				ArrivalReport arrivalReport = arList.get(0);
				
				System.err.println("Memo = "+arrivalReport.getMemo());
				
				if(arrivalReport.getMemo() == null){
					List<Map<String, Object>> cnmtList = cnmtDAO.getCnmtByChlnCodeForMemo(session, user, arrivalReport.getArchlnCode());
					Map<String, Object> finalAr = new HashMap<String, Object>();
					
					finalAr.put("arId", arrivalReport.getArId());
					finalAr.put("arCode", arrivalReport.getArCode());
					finalAr.put("chlnCode", arrivalReport.getArchlnCode());
					finalAr.put("arDt", arrivalReport.getArDt());
					finalAr.put("cnmtDt", cnmtList);
					
					resultMap.put("list", finalAr);
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
				}else{
					resultMap.put("msg", "Already generated !");
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
				
			}				
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "AR not found !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from /submitArMemo()");
		return resultMap;
	}
	
	@RequestMapping(value="/submitArMemo", method=RequestMethod.POST)
	public @ResponseBody Object submitArMemo(@RequestBody MemoService memoService){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+" : Enter into /submitArMemo()");
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			resultMap = arrivalReportDAO.saveArMemo(session, user, memoService, resultMap);
			Object status = resultMap.get(ConstantsValues.RESULT);
			if(String.valueOf(status).equalsIgnoreCase(ConstantsValues.SUCCESS))
				transaction.commit();
			else
				transaction.rollback();
		}catch(Exception e){
			transaction.rollback();
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from /submitArMemo()");
		return resultMap;
	}
	
	@RequestMapping(value="/takeMemoDt", method=RequestMethod.POST)
	public @ResponseBody Object takeMemoDt(@RequestBody String memoNo){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+" : Enter into /takeMemoDt()");
		Session session = this.sessionFactory.openSession();
		try{
			Memo memo = arrivalReportDAO.getMemoWithChild(session, user, memoNo);
			if(memo == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such memo found !");
			}else{
				List<ArrivalReport> arList = memo.getArrivalReports();
				Iterator<ArrivalReport> arIterator = arList.iterator();
				List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
				while(arIterator.hasNext()){
					ArrivalReport ar = (ArrivalReport) arIterator.next();
					List<Map<String, Object>> cnmtList = cnmtDAO.getCnmtByChlnCodeForMemo(session, user, ar.getArchlnCode());
					if(cnmtList != null){
						Iterator<Map<String, Object>> it = cnmtList.iterator();
						while(it.hasNext()){
							Map<String, Object> cnmtMap = it.next();
							Map<String, Object> arMap = new HashMap<String, Object>();
							
							arMap.put("arCode", ar.getArCode());
							arMap.put("arDt", ar.getArDt());
							arMap.put("cnmtCode", cnmtMap.get("cnmtCode"));
							arMap.put("cnmtDt", cnmtMap.get("cnmtDt"));
							arMap.put("fromStation", cnmtMap.get("fromStation"));
							arMap.put("toStation", cnmtMap.get("toStation"));
							arMap.put("cnmtNoOfPkg", cnmtMap.get("cnmtNoOfPkg"));
							arMap.put("cnmtActualWt", cnmtMap.get("cnmtActualWt"));
							arMap.put("arRemark", cnmtMap.get("arRemark"));
							
							list.add(arMap);
						}
					}
				}
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("list", list);
				resultMap.put("branchName", memo.getFromBranch());
				resultMap.put("memoNo", memo.getMemoNo());
				resultMap.put("memoDate", memo.getMemoDate());
				resultMap.put("fromBranch", memo.getFromBranch());
				resultMap.put("toBranch", memo.getToBranch());
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from /takeMemoDt()");
		return resultMap;
	}
	
	
	
	@RequestMapping(value="fetchArData", method=RequestMethod.POST)
	public @ResponseBody Object getArData(@RequestBody Map<String, Object> dateData)
	{
		System.out.println(dateData);
		HashMap<String, Object> map=new HashMap<>();
		Date startDate=Date.valueOf(String.valueOf(dateData.get("fromDate")));
		Date endDate=Date.valueOf(String.valueOf(dateData.get("toDate")));
		List arrivalReports=arrivalReportDAO.getArByDate(startDate,endDate);
		if(arrivalReports!=null && !arrivalReports.isEmpty())
		{
		map.put("arData", arrivalReports);
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}
		else
		{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value="/updateAr",method=RequestMethod.POST)
	public @ResponseBody Object validateAr(@RequestBody Map<String, Object> map  ){
		System.out.println(map);
		HashMap<String, Object>hashMap=new HashMap<>();
		int i=arrivalReportDAO.doValidate(map);
		System.out.println(i);
		if(i>0)
		{
		hashMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
		}
		else{
			hashMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return  hashMap;
	}
	
	
	@RequestMapping(value = "/updateRemAr", method = RequestMethod.POST)
	public @ResponseBody Object updateRemAr(@RequestBody Map<String, Object> map) {
		System.out.println(map);
		HashMap<String, Object> hashMap = new HashMap<>();
		int i = arrivalReportDAO.updateRemAr(map);
		System.out.println(i);
		if (i > 0) {
			hashMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			hashMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return hashMap;
	}
	
	@SuppressWarnings({ "unlikely-arg-type", "unused" })
	@RequestMapping(value = "/viewCnmtFile", method = RequestMethod.POST)
	public @ResponseBody Object viewCnmt(@RequestBody String chlnCode) {
		System.out.println(chlnCode);
		HashMap<String, Object> hashMap = new HashMap<>();
		List<Integer> chlnId = arrivalReportDAO.getChlnId(chlnCode);
		List<Integer> cnmtId = arrivalReportDAO.getCnmtId(chlnId);
		List<CnmtImage> cnmtImageList = arrivalReportDAO
				.getCNmtImageData(cnmtId);
		System.out.println(cnmtImageList);
		if (cnmtImageList != null && !cnmtImageList.isEmpty()) {
			for (CnmtImage cnmtImage : cnmtImageList) {

				File file=null;
				
				if(cnmtImage.getArImgPath()!=null)
				 {
					String path=cnmtImage.getArImgPath().get(chlnId);
					
					if(path!=null)
					  file = new File(path);
					else
						file = new File(cnmtImage.getCnmtImgPath());
						 
				 }else {
					 file = new File(cnmtImage.getCnmtImgPath());
				 }
				
				
				
				System.out.println("Is file exist " + file.exists());
				if (file.exists()) {
					hashMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					System.out.println("I am in success");
					
					try {
					  byte[] pdfdata=new byte[(int) file.length()]; 
					  pdfdata=Files.readAllBytes(Paths.get(file.getAbsolutePath()));
					  
						blob=new SerialBlob(pdfdata);
					} catch ( Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 /* hashMap.put("pdfData", pdfdata); } catch (IOException e)
					 * { // TODO Auto-generated catch block e.printStackTrace();
					 * }
					 */

				} else {
					hashMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}

		}
		return hashMap;
	}	
	
	
	@RequestMapping(value = "/downloadImgCnmtAr", method = RequestMethod.POST)
	public void downloadImg(HttpServletRequest request, HttpServletResponse response){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /downloadImg()");		
			try{
				if(blob != null){
					ServletOutputStream out = response.getOutputStream();
					out.write(blob.getBytes(1, (int) blob.length()));
					out.close();
					blob = null;
				}					
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /downloadImg()");
	}

	
	@RequestMapping(value = "/saveAckValidation", method = RequestMethod.POST)
	public @ResponseBody Object saveAckValidation(@RequestBody String cnmtCode) {
		System.out.println("Cnmt COde " + cnmtCode);

		Acknowledge_Validation acknowledge_Validation = new Acknowledge_Validation();
		Map<String, Object> responseMap = new HashMap<>();

		List<Cnmt> cnmtList = cnmtDAO.getCnmt(cnmtCode);
		int bcode = Integer.parseInt(cnmtList.get(0).getbCode());
		System.out.println(cnmtList.get(0).getCnmtCode() + "   "
				+ cnmtList.get(0).getCnmtId());
		int cnmtId = cnmtList.get(0).getCnmtId();
		int cmId = cnmtList.get(0).getCmId();

		System.out.println("CmID is     " + cmId);
		List<CnmtImage> cnmtImageList = acknowledge_ValidationDao
				.getCnmtImage(cmId);
		System.out.println("File Name is   "
				+ new File(cnmtImageList.get(0).getCnmtImgPath()).getName());
		String cnmtFileName = new File(cnmtImageList.get(0).getCnmtImgPath())
				.getName();
		List<Cnmt_Challan> cnmt_ChallanList = cnmt_ChallanDAO
				.getCnmt_ChallanByCnmtId(cnmtId);
		int challanId = cnmt_ChallanList.get(0).getChlnId();
		System.out.println("Challan Id " + challanId);
		List<Challan> challanList = challanDAO.getChallanById(challanId);
		for (Challan challan : challanList) {

			int arId = challan.getChlnArId();
			System.out.println("Arrival Report");
			ArrivalReport arrivalReport = arrivalReportDAO.getArById(arId);
			System.out.println(arrivalReport.getCreationTS());

			acknowledge_Validation.setBcode(bcode);
			acknowledge_Validation.setCnmtCode(cnmtCode);
			acknowledge_Validation.setCnmtPdfName(cnmtFileName);
			acknowledge_Validation.setCheck(null);
			acknowledge_Validation.setReason(null);
			acknowledge_Validation.setRemarks(null);

			Calendar calendar = arrivalReport.getCreationTS();
			java.util.Date date = calendar.getTime();
			Date scandate = new Date(date.getTime());
			acknowledge_Validation.setScanDate(scandate);
			int i = acknowledge_ValidationDao
					.saveAckValidation(acknowledge_Validation);
			if (i > 0) {
				responseMap
						.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				System.out.println("Data is inserted");
			} else {
				responseMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				System.out.println("Error");
			}
		}

		return responseMap;
	}	
	
}
