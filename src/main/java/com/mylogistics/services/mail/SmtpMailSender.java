package com.mylogistics.services.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service()
public class SmtpMailSender {
 
//	@Autowired
//	private JavaMailSenderImpl mailSender; // MailSender interface defines a strategy for sending simple mails
	
	/*
	
	public void sendEmail(String toMailId, String subject, String msgBody) {

		MimeMessage message = mailSender.createMimeMessage();
			
		try{
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			
			helper.setFrom(mailSender.getUsername());
			helper.setTo(toMailId);
			helper.setSubject(subject);
			helper.setText(msgBody);
			
		}catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}
	
	public void sendEmailWithAttachment(String toMailId, String subject, String msgBody, String filePath) {

		MimeMessage message = mailSender.createMimeMessage();
			
		try{
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			
			helper.setFrom(mailSender.getUsername());
			helper.setTo(toMailId);
			helper.setSubject(subject);
			helper.setText(msgBody);
			
			FileSystemResource fsr = new FileSystemResource(filePath);
			helper.addAttachment(fsr.getFilename(), fsr);
			
		}catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}
	*/
}
