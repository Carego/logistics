<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="revVoucherForm" ng-submit=voucherSubmit(revVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="vouchNoId" name ="vouchNoName" ng-model="vouchNo" ng-click="openCStmtDB()" readonly>
		       			<label for="code">Select Voucher No</label>	
		       		</div>
		    </div>
		   
		   <div class="row">
		   	<div ng-show="reqCsList.length > 0">
					<table class="tblrow">
		   				   <caption class="coltag tblrow">{{reqCsList[0].csType}} Voucher ( {{ reqCsList[0].csVouchNo }} ) Details</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">S.No.</th>
	                        <th class="colclr">FA CODE</th>
	                        <th class="colclr">AMT</th>
	                        <th class="colclr">CHQ TYPE</th>
	                        <th class="colclr">DR/CR</th>
	                        <th class="colclr">Date</th> 
	                        <th class="colclr">MR No</th>
	                        <th class="colclr">PAY To</th>
	                        <th class="colclr">TV NO</th>
	                        <th class="colclr">TYPE</th>
	                        <th class="colclr">V TYPE</th>
	                        <th class="colclr">DESCRIPTION</th>
	                       <!--  <th class="colclr">SUBFILES</th>      -->                
	                     </tr>
	                     <tr class="tbl" ng-repeat="reqCs in reqCsList">
	                     	 <td class="rowcel">{{$index + 1}}</td>
	                        <td class="rowcel">{{reqCs.csFaCode}}</td>
	                        <td class="rowcel">{{reqCs.csAmt}}</td>
	                        <td class="rowcel">{{reqCs.csChequeType}}</td>
	                        <td class="rowcel">{{reqCs.csDrCr}}</td>
	                        <td class="rowcel">{{reqCs.csDt}}</td>
	                        <td class="rowcel">{{reqCs.csMrNo}}</td>
	                        <td class="rowcel">{{reqCs.csPayTo}}</td>
	                        <td class="rowcel">{{reqCs.csTvNo}}</td>
	                        <td class="rowcel">{{reqCs.csType}}</td>
	                        <td class="rowcel">{{reqCs.csVouchType}}</td>
	                        <td class="rowcel">{{reqCs.csDescription}}</td>
	                        <!-- <td class="rowcel">
	                        	<input type="button" value="Details" ng-click="getDetails(reqCs)"/>
	                        </td> -->
	                 	</tr>
	                 	<!-- <tr>
	                 		<td>
	                 			<div class="input-field col s12 center">
	                 				<input type="button" value="Sub File Details" ng-click="getDetails()"/>
	                 			</div>
	                 		</td>
	                 	</tr> -->
	                 </table>
				</div>
		   </div>
		   
		   <div class="row">
		   		<div ng-show="stmList.length > 0">
		   			<table class="tblrow">
		   				   <caption class="coltag tblrow">SUB FILE DETAILS</caption>
		   				 <tr class="rowclr">
							<th class="colclr">PhNo</th>
							<th class="colclr">Pay Amt</th>
							<th class="colclr">Bill FrDt</th>
							<th class="colclr">Bill ToDt</th>
							<th class="colclr">Staff Code</th>
							<th class="colclr">Branch Code</th>
							<th class="colclr">Sms Amt</th>
							<th class="colclr">Caller Tune Amt</th>
							<th class="colclr">Download Amt</th>
							<th class="colclr">Game Amt</th>
							<th class="colclr">Pesonal Call</th>
							<th class="colclr">Roaming Amt</th>
							<th class="colclr">Isd Amt</th>
							<th class="colclr">Total Ded</th>
						</tr>
	                     <tr class="tbl" ng-repeat="stm in stmList">
	                        <td class="rowcel">{{stm.telephoneMstr.tmPhNo}}</td>
	                        <td class="rowcel">{{stm.stmPayAmt}}</td>
	                        <td class="rowcel">{{stm.stmBillFrDt}}</td>
	                        <td class="rowcel">{{stm.stmBillToDt}}</td>
	                        <td class="rowcel">{{stm.telephoneMstr.employee.empFaCode}}</td>
	                        <td class="rowcel">{{stm.telephoneMstr.branch.branchFaCode}}</td>
	                        <td class="rowcel">{{stm.stmSmsDedAmt}}</td>
	                        <td class="rowcel">{{stm.stmCTDedAmt}}</td>
	                        <td class="rowcel">{{stm.stmDDedAmt}}</td>
	                        <td class="rowcel">{{stm.stmGDedAmt}}</td>
	                        <td class="rowcel">{{stm.stmPCDedAmt}}</td>
	                        <td class="rowcel">{{stm.stmRDedAmt}}</td>
							<td class="rowcel">{{stm.stmIsdDedAmt}}</td>
							<td class="rowcel">{{stm.stmTotDedAmt}}</td>
	                 	</tr>
	                 </table>
		   		</div>
		   </div>
		   
		   	
		   <div class="row">
				 <div ng-show="semList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">SUB FILE Details</caption>
						<tr lass="rowclr">
							<th class="colclr">CRN NO</th>
							<th class="colclr">Branch Code</th>
							<th class="colclr">Pay Amount</th>
							<th class="colclr">Bill FrDt</th>
							<th class="colclr">Bill ToDt</th>
						</tr>
						<tr class="tbl" ng-repeat="sem in semList">
							<td class="rowcel">{{sem.electricityMstr.emCrnNo}}</td>
							<td class="rowcel">{{sem.electricityMstr.branch.branchFaCode}}</td>
							<td class="rowcel">{{sem.semPayAmt}}</td>
							<td class="rowcel">{{sem.semBillFrDt}}</td>
							<td class="rowcel">{{sem.semBillToDt}}</td>
						</tr>
					</table>
				</div>
			</div>		
			
			<div class="row">	   
		   		<div ng-show="fbpList.length > 0">
		   			<table class="tblrow">
		   				   <caption class="coltag tblrow">SUB FILE Details</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">CUST CODE</th>
	                        <th class="colclr">CUST NAME</th>
	                        <th class="colclr">AMOUNT</th>
	                        <th class="colclr">ARTICLE</th>
	                        <th class="colclr">PERSON NAME</th>
	                        <th class="colclr">DESIGNATION</th>    
	                        <th class="colclr">PURPOSE</th>    
	                        <th class="colclr">TURNOVER</th>  
	                        <th class="colclr">EXP_TURN</th>   
	                        <th class="colclr">DESCRIPTION</th>                  
	                    </tr>
	                     <tr class="tbl" ng-repeat="fbp in fbpList">
	                        <td class="rowcel">{{fbp.customer.custFaCode}}</td>
	                        <td class="rowcel">{{fbp.customer.custName}}</td>
	                        <td class="rowcel">{{fbp.fbpAmt}}</td>
	                        <td class="rowcel">{{fbp.fbpArticle}}</td>
	                        <td class="rowcel">{{fbp.fbpPaidName}}</td>
	                        <td class="rowcel">{{fbp.fbpPaidDesig}}</td>
	                        <td class="rowcel">{{fbp.fbpPurpose}}</td>
	                        <td class="rowcel">{{fbp.fbpTurnOver}}</td>
	                        <td class="rowcel">{{fbp.fbpExpTurn}}</td>
	                        <td class="rowcel">{{fbp.fbpDesc}}</td>
	                 	</tr>
	                 </table>
	             </div>    	       	   
	   		</div>	
	   		
	   		
	   		<div class="row">
		   		<div ng-show="vDetList.length > 0">
		   			<table class="tblrow">
		   				   <caption class="coltag tblrow">SUB FILE Details</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">VH NO.</th>
	                        <th class="colclr">PETROL</th>
	                        <th class="colclr">REPAIR</th>
	                        <th class="colclr">ROADTAX</th>    
	                        <th class="colclr">INSURANCE</th>    
	                        <th class="colclr">SERVICE</th>    
	                        <th class="colclr">OTHERS</th>  
	                        <th class="colclr">TOTAL EXP</th>                
	                    </tr>
	                     <tr class="tbl" ng-repeat="vDet in vDetList">
	                        <td class="rowcel">{{vDet.sVehMstr.vehicleMstr.vehNo}}</td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faCar in vDet.faCarList">
	                        		<div ng-if="faCar.fcdExpType == 'PETROL'">
	                        			<table>
	                        				<tr>
	                        					<td>F</td>
	                        					<td>{{faCar.fcdFuel}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>R</td>
	                        					<td>{{faCar.fcdReading}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faCar.fcdExpAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faCar.fcdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faCar in vDet.faCarList">
	                        		<div ng-if="faCar.fcdExpType == 'REPAIR'">
	                        			<table>
	                        				<tr>
	                        					<td>R</td>
	                        					<td>{{faCar.fcdReading}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faCar.fcdExpAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faCar.fcdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faCar in vDet.faCarList">
	                        		<div ng-if="faCar.fcdExpType == 'ROADTAX'">
	                        			<table>
	                        				<tr>
	                        					<td>R</td>
	                        					<td>{{faCar.fcdReading}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faCar.fcdExpAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faCar.fcdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                         <td class="rowcel">
	                        	<div ng-repeat="faCar in vDet.faCarList">
	                        		<div ng-if="faCar.fcdExpType == 'INSURANCE'">
	                        			<table>
	                        				<tr>
	                        					<td>R</td>
	                        					<td>{{faCar.fcdReading}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>F DT</td>
	                        					<td>{{faCar.fcdInsFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>T DT</td>
	                        					<td>{{faCar.fcdInsToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faCar.fcdExpAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faCar.fcdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                         <td class="rowcel">
	                        	<div ng-repeat="faCar in vDet.faCarList">
	                        		<div ng-if="faCar.fcdExpType == 'SERVICE'">
	                        			<table>
	                        				<tr>
	                        					<td>R</td>
	                        					<td>{{faCar.fcdReading}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faCar.fcdExpAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faCar.fcdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faCar in vDet.faCarList">
	                        		<div ng-if="faCar.fcdExpType == 'OTHER'">
	                        			<table>
	                        				<tr>
	                        					<td>F</td>
	                        					<td>{{faCar.fcdFuel}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>R</td>
	                        					<td>{{faCar.fcdReading}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>F DT</td>
	                        					<td>{{faCar.fcdInsFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>T DT</td>
	                        					<td>{{faCar.fcdInsToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faCar.fcdExpAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faCar.fcdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">
	                        	{{vDet.sVehMstr.svmTotAmt}}
	                        </td>
	                    </tr>
		   			</table>
		   		</div>
	   		</div>
		   	
		   	
		   	<div class="row">	   		
		   		<div ng-show="tDetList.length > 0">
		   			<table class="tblrow">
		   				   <caption class="coltag tblrow">SUB FILE DETAILS</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">Emp Code</th>
	                        <th class="colclr">TRAVELLING</th>
	                        <th class="colclr">BOARDING</th>
	                        <th class="colclr">LODGING</th>    
	                        <th class="colclr">CONVEYANCE</th>    
	                        <th class="colclr">OTHERS</th>  
	                        <th class="colclr">TOTAL EXP</th>                      
	                    </tr>
	                     <tr class="tbl" ng-repeat="tDet in tDetList">
	                        <td class="rowcel">{{tDet.employee.empFaCode}}</td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faTrav in tDet.ftdList">
	                        		<div ng-if="faTrav.ftdTravType == 'TRAVELLING'">
	                        			<table>
	                        				<tr>
	                        					<td>H N</td>
	                        					<td>{{faTrav.ftdHName}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>H R</td>
	                        					<td>{{faTrav.ftdHRate}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Place</td>
	                        					<td>{{faTrav.ftdPlace}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Fr Dt</td>
	                        					<td>{{faTrav.ftdFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>To Dt</td>
	                        					<td>{{faTrav.ftdToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faTrav.ftdTravAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faTrav.ftdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>	 
	                        <td class="rowcel">
	                        	<div ng-repeat="faTrav in tDet.ftdList">
	                        		<div ng-if="faTrav.ftdTravType == 'BOARDING'">
	                        			<table>
	                        				<tr>
	                        					<td>H N</td>
	                        					<td>{{faTrav.ftdHName}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>H R</td>
	                        					<td>{{faTrav.ftdHRate}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Place</td>
	                        					<td>{{faTrav.ftdPlace}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Fr Dt</td>
	                        					<td>{{faTrav.ftdFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>To Dt</td>
	                        					<td>{{faTrav.ftdToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faTrav.ftdTravAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faTrav.ftdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>  
	                        <td class="rowcel">
	                        	<div ng-repeat="faTrav in tDet.ftdList">
	                        		<div ng-if="faTrav.ftdTravType == 'LODGING'">
	                        			<table>
	                        				<tr>
	                        					<td>H N</td>
	                        					<td>{{faTrav.ftdHName}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>H R</td>
	                        					<td>{{faTrav.ftdHRate}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Place</td>
	                        					<td>{{faTrav.ftdPlace}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Fr Dt</td>
	                        					<td>{{faTrav.ftdFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>To Dt</td>
	                        					<td>{{faTrav.ftdToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faTrav.ftdTravAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faTrav.ftdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faTrav in tDet.ftdList">
	                        		<div ng-if="faTrav.ftdTravType == 'CONVEYANCE'">
	                        			<table>
	                        				<tr>
	                        					<td>H N</td>
	                        					<td>{{faTrav.ftdHName}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>H R</td>
	                        					<td>{{faTrav.ftdHRate}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Place</td>
	                        					<td>{{faTrav.ftdPlace}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Fr Dt</td>
	                        					<td>{{faTrav.ftdFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>To Dt</td>
	                        					<td>{{faTrav.ftdToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faTrav.ftdTravAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faTrav.ftdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">
	                        	<div ng-repeat="faTrav in tDet.ftdList">
	                        		<div ng-if="faTrav.ftdTravType == 'OTHER'">
	                        			<table>
	                        				<tr>
	                        					<td>H N</td>
	                        					<td>{{faTrav.ftdHName}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>H R</td>
	                        					<td>{{faTrav.ftdHRate}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Place</td>
	                        					<td>{{faTrav.ftdPlace}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Fr Dt</td>
	                        					<td>{{faTrav.ftdFrDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>To Dt</td>
	                        					<td>{{faTrav.ftdToDt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Amt</td>
	                        					<td>{{faTrav.ftdTravAmt}}</td>
	                        				</tr>
	                        				<tr>
	                        					<td>Desc</td>
	                        					<td>{{faTrav.ftdDesc}}</td>
	                        				</tr>
	                        			</table>
	                        		</div>
	                        	</div>
	                        </td>
	                        <td class="rowcel">{{tDet.totAmt}}</td>		
	                    </tr>
		   			</table>
		   		</div>
	   		</div>	   		
	   		
	   		
		   	<div class="row">
		   		<div class="input-field col s12 center">
	                 	<input type="button" id="getDetId" value="Sub File Details" ng-click="getDetails()" disabled="disabled"/>
	            </div>
		   	</div>
		   	
		   	
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" />
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    <div id="cStmtDB" ng-hide="cStmtDBFalg"> 
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Voucher No</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="vhNo in vhNoList">
		 	  <td><input type="radio"  name="vhNo"   value="{{ vhNo }}" ng-model="vhNoId" ng-click="saveVouchNo(vhNo)"></td>
              <td>{{ vhNo }}</td>
          </tr>
      </table> 
    </div>
    
 </div>   