package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchStockDisDetDAO;
import com.mylogistics.DAO.BranchStockDispatchDAO;
import com.mylogistics.DAO.HBOptr_NotificationDAO;
import com.mylogistics.DAO.Optr_NotificationDAO;
import com.mylogistics.model.BranchStockDisDet;
import com.mylogistics.model.BranchStockDispatch;
import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.Optr_Notification;
import com.mylogistics.model.User;
import com.mylogistics.services.BrStkDisDetService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BranchStockDispatchCntlr {

	@Autowired
	private BranchDAO  branchDAO;
	
	@Autowired
	private BranchStockDispatchDAO  branchStockDispatchDAO;
	
	@Autowired
	private BranchStockDisDetDAO  branchStockDisDetDAO;
	
	@Autowired
	private HBOptr_NotificationDAO  hBOptr_NotificationDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private Optr_NotificationDAO optr_NotificationDAO;
	
	private static Logger logger = Logger.getLogger(BranchStockDispatchCntlr.class);
	
	
	@RequestMapping(value = "/saveBranchStockDispatch", method = RequestMethod.POST)
    public @ResponseBody Object submitProductType(@RequestBody BrStkDisDetService brStkDisDetService){
		logger.error("Enter into submitProjectType() ");		
       Map<String,String> map = new HashMap<String,String>();
       User currentUser = (User)httpSession.getAttribute("currentUser");
       BranchStockDispatch branchStockDispatch = brStkDisDetService.getBrStkDis();
       
       List<Long> cnmtList = brStkDisDetService.getCnmtList();
       List<Long> chlnList = brStkDisDetService.getChlnList();
       List<Long> sedrList = brStkDisDetService.getSedrList();
       String operatorCode = brStkDisDetService.getOperatorCode();
       
       System.out.println("operator Code : "+operatorCode);
       
       boolean stAlreadyOrdered = branchStockDisDetDAO.isStAlreadyOrdered(cnmtList, chlnList, sedrList);
       
       if (stAlreadyOrdered) {
    	   map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
    	   map.put("msg", "Already Dispatched !");
    	   logger.error("Already dispatched !");
       } else {
    	   int hboId = brStkDisDetService.getId();
           System.out.println("hboId============================"+hboId);
           
           String branchCode = branchStockDispatch.getBrsDisBranchCode();
           int isView=hBOptr_NotificationDAO.updateIsViewYes(hboId);
           System.out.println("**-------------isView----"+isView);
           String brName = branchDAO.getBrNameByBrCode(branchCode);
           String dispatchCode = brName.substring(0, 3);
           
           String last = null;
           long end=0;
           long rowCount = branchStockDispatchDAO.totalCount();
           if (!(rowCount == -1)) 
           {
           if ((rowCount == 0)){
        	   last = "0000001";
        	   dispatchCode = dispatchCode+last;
           }else{
        	   long id = (long)branchStockDispatchDAO.getLastBrnchStckDisptchId();
        	   long id1 = id+1;
        	   end = 10000000 + id1;
        	   last = String.valueOf(end).substring(1, 8);
        	   dispatchCode = dispatchCode+last;
           }
           } 
           else {
    			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
    		}
           branchStockDispatch.setBrsDisCode(dispatchCode);
           branchStockDispatch.setbCode(currentUser.getUserCode());
           branchStockDispatch.setUserCode(currentUser.getUserCode());
           //branchStockDispatch.setIsAdminView("no");
           branchStockDispatch.setIsAdminView("yes");
           branchStockDispatch.setIsStockRecieved("no");
           //branchStockDispatch.setDispatchMsg("not received");
           branchStockDispatch.setDispatchMsg("not received");
           int temp=branchStockDispatchDAO.saveBranchStockDispatch(branchStockDispatch);
           
           
           Optr_Notification optr_Notification = new Optr_Notification();
           optr_Notification.setDispatchCode(branchStockDispatch.getBrsDisCode());
           optr_Notification.setDispatchDate(branchStockDispatch.getBrsDisDt());
           optr_Notification.setOperatorCode(operatorCode);
           optr_Notification.setUserCode(currentUser.getUserCode());           
           optr_Notification.setbCode(branchStockDispatch.getBrsDisBranchCode());           
           optr_Notification.setIsRecieved("no");
           
           optr_NotificationDAO.saveOptr_Notification(optr_Notification);          
           
           if(temp > 0){
        	   int saveCnmt = saveNoCnmt(cnmtList,dispatchCode,branchCode);
        	   if(saveCnmt > 0){
        		   int saveChln = saveNoChln(chlnList,dispatchCode,branchCode);
        		   if(saveChln > 0){
        			   int saveSedr = saveNoSedr(sedrList,dispatchCode,branchCode);
        			   if(saveSedr > 0){
        				   System.out.println("chln,cnmt & sedr saved");
        			   }
        		   }
        	   }
        	   branchStockDispatchDAO.updateMasterStock(Integer.parseInt(branchCode), cnmtList, chlnList, sedrList);
        	   
        	   map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
           }else{
        	   map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
           }
       }
       
       
       
       
       /*User currentUser = (User)httpSession.getAttribute("currentUser");
       branchStockDispatch.setUserCode(currentUser.getUserCode());
       branchStockDispatch.setbCode(currentUser.getUserBranchCode());
       //branchStockDispatch.setIsAdminView("no");
       branchStockDispatch.setIsStockRecieved("no");
       String dispatchCode = branchStockDispatch.getBrsDisCode();
       int temp=branchStockDispatchDAO.saveBranchStockDispatch(branchStockDispatch);
    	if (temp>=0) {
    		 int result = hBOptr_NotificationDAO.updateHBON_Data(dispatchCode);
    		 if(result == 1){ 
    			  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
    		 }	  
		}else{
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		return map;	
	}
	
	/*@RequestMapping(value = "/saveNoCnmt", method = RequestMethod.POST)*/
    public int saveNoCnmt(List<Long> cnmtList,String dispatchCode,String branchCode){ 
    	logger.info("Enter into saveNoCnmt()");
		System.out.println("enter into saveNoCnmt function--->");
       int temp = 0;
       User currentUser = (User)httpSession.getAttribute("currentUser");

       if(!cnmtList.isEmpty()){
    	   for(int i=0;i<cnmtList.size();i++){
    		   BranchStockDisDet brSDisDet = new BranchStockDisDet();
	    	   brSDisDet.setBrsDisDetBrCode(branchCode);
	    	   brSDisDet.setBrsDisDetDisCode(dispatchCode);
	    	   brSDisDet.setBrsDisDetStatus("cnmt");
	    	   //String startNo = get7DigitNo(cnmtList.get(i));
	    	   String startNo = String.valueOf(cnmtList.get(i));
	    	   if(!startNo.isEmpty()){
	    		   brSDisDet.setBrsDisDetStartNo(startNo);
	    		   brSDisDet.setbCode(currentUser.getUserBranchCode());
		    	   brSDisDet.setUserCode(currentUser.getUserCode());
		    	   brSDisDet.setIsVerify("no");
		    	   temp = branchStockDisDetDAO.saveBranchStockDisDet(brSDisDet);
	    	   } 
    	   }
       }else{
    	   temp = 1;
    	   logger.info("CnmtList is empty");
    	   System.out.println("cnmtList is empty");
       }
      
       logger.info("Exit from saveNoCnmt()");
       
		return temp;	
	}
	
	
	/*@RequestMapping(value = "/saveNoChln", method = RequestMethod.POST)*/
    public int saveNoChln(List<Long> chlnList,String dispatchCode,String branchCode){ 
    	logger.info("Enter into saveNoChln()");;
		System.out.println("enter into saveNoChln function--->");
       int temp = 0;
       User currentUser = (User)httpSession.getAttribute("currentUser");
       if(!chlnList.isEmpty()){
    	   for(int i=0;i<chlnList.size();i++){
    		   BranchStockDisDet brSDisDet = new BranchStockDisDet();
	    	   brSDisDet.setBrsDisDetBrCode(branchCode);
	    	   brSDisDet.setBrsDisDetDisCode(dispatchCode);
	    	   brSDisDet.setBrsDisDetStatus("chln");
	    	   //String startNo = get7DigitNo(chlnList.get(i));
	    	   String startNo = String.valueOf(chlnList.get(i));
	    	   if(!startNo.isEmpty()){
		    	   brSDisDet.setBrsDisDetStartNo(startNo);
		    	   brSDisDet.setbCode(currentUser.getUserBranchCode());
		    	   brSDisDet.setUserCode(currentUser.getUserCode());
		    	   brSDisDet.setIsVerify("no");
		    	   temp = branchStockDisDetDAO.saveBranchStockDisDet(brSDisDet);
	    	   }  
    	   }
       }else{
    	   logger.info("ChlnList is empty ");
    	   temp = 1;
    	   System.out.println("chlnList is empty");
       }    
       logger.info("Exit from saveNoChln()");
		return temp;	
	}
	
	/*@RequestMapping(value = "/saveNoSedr", method = RequestMethod.POST)*/
    public int saveNoSedr(List<Long> sedrList,String dispatchCode,String branchCode){ 
		System.out.println("enter into saveNoSedr function--->");
      logger.info("Enter into saveNOSedr() ");
       int temp = 0;
       User currentUser = (User)httpSession.getAttribute("currentUser");
       if(!sedrList.isEmpty()){
    	   for(int i=0;i<sedrList.size();i++){
    		   BranchStockDisDet brSDisDet = new BranchStockDisDet();
	    	   brSDisDet.setBrsDisDetBrCode(branchCode);
	    	   brSDisDet.setBrsDisDetDisCode(dispatchCode);
	    	   brSDisDet.setBrsDisDetStatus("sedr");
	    	  //String startNo = get7DigitNo(sedrList.get(i));
	    	   String startNo = String.valueOf(sedrList.get(i));
	    	   if(!startNo.isEmpty()){
		    	   brSDisDet.setBrsDisDetStartNo(startNo);
		    	   brSDisDet.setbCode(currentUser.getUserBranchCode());
		    	   brSDisDet.setUserCode(currentUser.getUserCode());
		    	   brSDisDet.setIsVerify("no");
		    	   temp = branchStockDisDetDAO.saveBranchStockDisDet(brSDisDet);
	    	   }	   
    	   }
       }else{
    	   logger.info("SedrList is empty !");
    	   temp = 1;
    	   System.out.println("SedrList is empty");
       }
       logger.info("Exit from saveNoSedr()");       
		return temp;	
	}
    
    
    public String get7DigitNo(int number){
    	System.out.println("enter into get7DigitNo function");
    	long no = 10000000+number;
    	String code =  String.valueOf(no).substring(1,8); 
    	return code;
    }
	
}
