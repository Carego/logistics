'use strict';

var app = angular.module('application');

app.controller('EditBillCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	console.log("enter edit bill cntlr.js...");
	$scope.loadingFlag = false;
	$scope.openBrhDBFlag=true;
	$scope.openBillDBFlag=true;
	$scope.openCnmtDBFlag=true;
	$scope.addCnInitial=false;
	$scope.isFormSubmitted = false;
	$scope.editFormSubmitted = false;
	$scope.otherChgIsFormSubmitted = false;
	$scope.actCnmt = {};
	$scope.actChlnList = [];
	$scope.ot={};
	$scope.billDet={};
	$scope.billDet.bdOthChgList = [];
	$scope.blBillNo='';
	$scope.branch='';
	$scope.blBrhId='';
	$scope.editBillFlag=true;
	$scope.editcnmtBillFlag=true;
	$scope.billBasisRead=true;
	$scope.otChgFlag = true;
	var cnmtDate,subTotal=0;
	$scope.otChgTypeList = ["Storage" ,"Redelivery Charge" , "Crain Charge" , "Express Delivery Charge"];
	
	
	$scope.inilizationOfCnmt=function(data){
		console.log("cnmt inilization..");
		console.log(data);
		$scope.addCnInitial=true;
		$scope.billDet.bdLoadAmt = 0;
		$scope.billDet.bdUnloadAmt = 0;
		$scope.billDet.bdBonusAmt = 0;
		$scope.billDet.bdDetAmt = 0;
		 $scope.billBasis=data.billBasis;
		 console.log("$scope.billBasis "+$scope.billBasis);
		$scope.actChlnList = data.chlnList;
		$scope.actCnmt = data.cnmt;
		$scope.frStn=data.frStn;
		$scope.toStn=data.toStn;
		$scope.lryNo = data.lryNo;
		
		 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
		 $scope.billDet.bdLryNo=$scope.lryNo;
		 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
		 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
		 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
		 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
		 $scope.billDet.bdBlBase = $scope.billBasis;
		 if($scope.actChlnList.length > 0){
			 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
		 }
		 
		 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
		 if(data.billBasis === "chargeWt"){
			 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
		 }else if(data.billBasis === "receiveWt"){
			 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
		 }else if(data.billBasis === "actualWt"){
			 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
		 }else{
			 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
		 }
	
	};
	
	$scope.saveCnmt=function(cnmt){
		console.log("saveCnmt..."+cnmt);
		$scope.cnmtCode=cnmt;
		$('div#openCnmtId').dialog('close');
		
	if($scope.billDetSerList.length > 0){	
			var dupCnmt = false;
				for(var i=0;i<$scope.billDetSerList.length;i++){
					if($scope.billDetSerList[i].cnmt.cnmtCode === $scope.cnmtCode){
						dupCnmt = true;
					};
				}
		
				if(dupCnmt === true){
						$scope.alertToast("You already generate a bill for "+$scope.cnmtCode+" Cnmt");
						
					}else{
							var req={
									"cnmtCode" : $scope.cnmtCode,
									"billDt"   : $scope.bill.blBillDt
								};
						if(angular.isUndefined($scope.cnmtCode)){
								$scope.alertToast("please select a CNMT no");
							}else{
								$scope.loadingFlag =true;
									var response=$http.post($scope.projectName+'/getCnmtInfo',req);
										response.success(function(data,status,headers,config){
											if(data.result==="success"){
												$scope.inilizationOfCnmt(data);
												$scope.loadingFlag =false;
												$scope.editcnmtBillFlag = false;
												
													$('div#editcnmtBillId').dialog({
														autoOpen: false,
														modal:true,
														resizable: false,
														title: "Billing Detail Of "+$scope.cnmtCode+"("+$scope.billBasis+")",
														show: UDShow,
														hide: UDHide,
														position: UDPos,
														draggable: true,
														close: function(event, ui) { 
															$(this).dialog('destroy');
															$(this).hide();
															$scope.editcnmtBillFlag = true;
														}
													});
									
													$('div#editcnmtBillId').dialog('open');
					 
											}else{
												if(angular.isUndefined(data.msg)){
													$scope.alertToast("Server Error");  
												}else{
													$scope.loadingFlag =false;
													$scope.alertToast(data.msg);  
												}
											}
										});
										response.error(function(data, status, headers, config) {
											$scope.loadingFlag =false;
											$scope.errorToast(data.result);
										});
							};
					};
		}
	};
	
	$scope.selectBuilty=function(){
	    console.log("select selectCnmt dialog box..");
	    $scope.openCnmtDBFlag=false;
	    $('div#openCnmtId').dialog({
	    	autoOpen:false,
	    	modal:true,
	    	resizable:false,
	    	title:"Select Cnmt",
	    	show:UDShow,
	    	hide: UDHide,
	    	position: UDPos,
	    	draggable: true,
	    	close: function(event,ui){
	    		$(this).dialog('destroy');
	    		$(this).hide();
	    		 $scope.openCnmtDBFlag=true;
	    	}
	    });
	    
	    $('div#openCnmtId').dialog('open');
	
	};
	
	
	$scope.saveOtChg = function(OtherChargeForm,ot){
		console.log("enter into saveOtChg function--->");
		if (OtherChargeForm.$invalid) {
			$scope.otherChgIsFormSubmitted = true;
			return;
		}
		$('div#otChgDB').dialog('close');
		$scope.otherChgIsFormSubmitted = false;
		
		if($scope.billDet.bdOthChgList.length>0){
			if(OtherChargeForm.$invalid){
				if(OtherChargeForm.otChgTypeName.$invalid){
					$scope.alertToast("Please Enter Type");
				}else if(OtherChargeForm.otChgValueName.$invalid){
					$scope.alertToast("Please Enter Value");
				}else{
					console.log("**************");
				}
			}else{
				ot.otChgType=ot.otChgType.toUpperCase();
				var duplicate = false;
				for(var i=0;i<$scope.billDet.bdOthChgList.length;i++){
					if(ot.otChgType === $scope.billDet.bdOthChgList[i].otChgType){
						duplicate = true;
						break;
					}
				}
				console.log("duplicate = "+duplicate);
				if(duplicate === true){
					$scope.alertToast("You already enter the "+ot.otChgType+" amount");
				}else{
					$scope.addOtherCharge(ot);	
				}
			}
		}else{
			console.log("If list does not exist");
			if(OtherChargeForm.$invalid){
				if(OtherChargeForm.otChgTypeName.$invalid){
					$scope.alertToast("Please Enter Type");
				}else if(OtherChargeForm.otChgValueName.$invalid){
					$scope.alertToast("Please Enter Value");
				}else{
					console.log("**********************");
				}
			}else{
				ot.otChgType=ot.otChgType.toUpperCase();
				$scope.addOtherCharge(ot);
			}
		}
	};
	
	$scope.addOtherCharge = function(ot){
		console.log("enter into addOtherCharge function");
		$scope.billDet.bdOthChgList.push(ot); 
		console.log("size of $scope.billDet.bdOthChgList = "+$scope.billDet.bdOthChgList.length);
		$scope.ot={};
		$scope.calBillTot();
	};
	
	$scope.removeOth = function(index){
		console.log("enter into removeOth function = "+index);
		if($scope.billDet.bdOthChgList.length > 0){
			$scope.billDet.bdOthChgList.splice(index,1);
		}
		$scope.calBillTot();
	};
	
	$scope.otChg=function(){
		console.log("other charge function ");
		$scope.otChgFlag = false;
    	$('div#otChgDB').dialog({
    		autoOpen: false,
			modal:true,
			resizable: false,
			title: "Other Charges",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.otChgFlag = true;
		    }
			});
		
		$('div#otChgDB').dialog('open');
	};
	
	
	$scope.calBillTot=function(){
		console.log("enter into calBillTot function");
		
		var tempFreight = 0;
		
		console.log("$scope.billBasis"+$scope.billBasis);
		
		 if($scope.billBasis === "chargeWt"){
			  $scope.billBasisRead=false;
			 tempFreight = parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate);
			 $scope.billDet.bdFreight=tempFreight;
		 }else if($scope.billBasis === "receiveWt"){
			 tempFreight = parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate);
			 $scope.billDet.bdFreight=tempFreight;
		 }else if($scope.billBasis === "actualWt"){
			 $scope.billBasisRead=false;
			 tempFreight = parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate);
			 $scope.billDet.bdFreight=tempFreight;
		 }else{
			 tempFreight = $scope.billDet.bdFreight; 
		 }
		 
		var bdLoadAmt = 0;
		var bdUnloadAmt = 0;
		var bdBonusAmt = 0;
		var bdDetAmt = 0;
	
		$scope.billDet.bdTotAmt = 0;
		if(!angular.isNumber($scope.billDet.bdLoadAmt)){
			bdLoadAmt = 0;
		}else{
			bdLoadAmt = $scope.billDet.bdLoadAmt;
		}
		
		if(!angular.isNumber($scope.billDet.bdUnloadAmt)){
			bdUnloadAmt = 0;
		}else{
			bdUnloadAmt = $scope.billDet.bdUnloadAmt;
		}
		
		if(!angular.isNumber($scope.billDet.bdBonusAmt)){
			bdBonusAmt = 0;
		}else{
			bdBonusAmt = $scope.billDet.bdBonusAmt;
		}
		
		if(!angular.isNumber($scope.billDet.bdDetAmt)){
			bdDetAmt = 0;
		}else{
			bdDetAmt = $scope.billDet.bdDetAmt;
		}
		
		if($scope.billDet.bdOthChgList.length > 0){
			var othAmt = 0;
			for(var i=0;i<$scope.billDet.bdOthChgList.length;i++){
				othAmt = othAmt + parseFloat($scope.billDet.bdOthChgList[i].otChgValue);
			}
			
			$scope.billDet.bdTotAmt = Math.round(parseFloat(tempFreight) + parseFloat(bdLoadAmt) + 
					parseFloat(bdUnloadAmt) + parseFloat(bdBonusAmt) + parseFloat(bdDetAmt)
					+ parseFloat(othAmt));
		
			console.log("inside if ***** $scope.billDet.bdTotAmt = "+$scope.billDet.bdTotAmt);
		}else{ 
			$scope.billDet.bdTotAmt = Math.round(parseFloat(tempFreight) + parseFloat(bdLoadAmt) + 
					parseFloat(bdUnloadAmt) + parseFloat(bdBonusAmt) +
					parseFloat(bdDetAmt));
			
			console.log("inside else #### $scope.billDet.bdTotAmt = "+$scope.billDet.bdTotAmt);
		}
	
	};
	
	$scope.billDetailIniliaze=function(index){
		console.log("initialazation of billDetail...");
		$scope.addCnInitial=false;
		$scope.actCnmt={};
		 var blDetList=$scope.billDetSerList[index];
		 $scope.billBasis=blDetList.billDetail.bdBlBase;
		 console.log("$scope.billBasis "+$scope.billBasis);
		 $scope.actCnmt.cnmtDt=blDetList.cnmt.cnmtDt;
		$scope.frStn=blDetList.frStn;
		$scope.toStn=blDetList.toStn;
		$scope.billDet=blDetList.billDetail;
	};
	
	$scope.editBillDetail=function(index){
		console.log("edit bill detail..."+index);
		 
		$scope.billDetailIniliaze(index);
		 
		$scope.editcnmtBillFlag = false;
			$('div#editcnmtBillId').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Billing Detail Of "+$scope.billDetSerList[index].cnmt.cnmtCode+"("+$scope.billBasis+")",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.editcnmtBillFlag = true;
			    }
				});
						
			$('div#editcnmtBillId').dialog('open');
			
			
	};
	
	$scope.editCnBillForm = function(cnmtBillForm){
		console.log("enter into saveCnBillForm function = "+cnmtBillForm.$invalid);
		if(cnmtBillForm.$invalid){
			$scope.editFormSubmitted = true;
		}else{
			$scope.editFormSubmitted = false;
			$scope.billServ = {};
			$scope.billServ.billDetail = $scope.billDet;
			var othTot = 0.0;
			if($scope.billServ.billDetail.bdOthChgList.length > 0){
				for(var i=0;i<$scope.billServ.billDetail.bdOthChgList.length;i++){
					console.log("$scope.billServ.billDetail.bdOthChgList["+i+"].othChgValue = "+$scope.billServ.billDetail.bdOthChgList[i].otChgValue);
					othTot = Math.round(othTot + parseFloat($scope.billServ.billDetail.bdOthChgList[i].otChgValue));
					console.log("othTot = "+othTot);
				}
			}
			
			$scope.billServ.billDetail.bdOthChgAmt = Math.round(othTot);
			
			$scope.billServ.frStn = $scope.frStn;
			$scope.billServ.toStn = $scope.toStn;
			$scope.billServ.cnmt = $scope.actCnmt;
			if($scope.addCnInitial==true){
			$scope.billDetSerList.push($scope.billServ);
			}
			$('div#editcnmtBillId').dialog('close');
			
			$scope.billDet = {};
			$scope.billDet.bdOthChgList = [];
			$scope.ot={};
			//$scope.selChln = {};
			$scope.actCnmt = {};
			//$scope.actChlnList = [];
			$scope.frStn = "";
			$scope.toStn = "";
			
			$scope.bill.blSubTot = 0;
			var parseYear,parseMonth,subtotalAmount,dateSplit=[];
			if($scope.billDetSerList.length > 0){
				for(var i=0;i<$scope.billDetSerList.length;i++){
					$scope.bill.blSubTot = $scope.bill.blSubTot + parseFloat($scope.billDetSerList[i].billDetail.bdTotAmt);
				}
			}
			

			$scope.bill.blSubTot = Math.round($scope.bill.blSubTot);
			console.log("$scope.bill.blSubTot="+$scope.bill.blSubTot);
			if($scope.bill.blSubTot > 0){
				$scope.bill.blTaxableSerTax = Math.round(parseFloat($scope.bill.blSubTot * ($scope.serTax.stTaxableRt / 100)));
				console.log("$scope.bill.blTaxableSerTax"+$scope.bill.blTaxableSerTax);
				if($scope.bill.blTaxableSerTax > 0){
					$scope.bill.blSerTax = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSerTaxRt / 100)));
					$scope.bill.blSwachBhCess = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSwhBHCessRt / 100)));
			     var checkblKisanKalCess=0;
					for(var i=0;i<$scope.billDetSerList.length;i++){
						console.log("billDetSerList="+$scope.billDetSerList[i].cnmt.cnmtDt);
						cnmtDate=$scope.billDetSerList[i].cnmt.cnmtDt;
						dateSplit=cnmtDate.split("-");
				         console.log("cnmtDt="+cnmtDate);
				        parseYear=parseInt(dateSplit[0]);
				        parseMonth=parseInt(dateSplit[1]);
						console.log("parseYear="+parseYear+"parseMonth="+parseMonth);
						if(parseYear>2015&&parseMonth>5||parseYear>2016){
							checkblKisanKalCess=1;
							subTotal=subTotal+parseFloat($scope.billDetSerList[i].billDetail.bdTotAmt);
							console.log("for subTotal="+subTotal);
							var taxAbleServiceTax=(($scope.serTax.stTaxableRt*subTotal)/100);
							console.log("taxAbleServiceTax="+taxAbleServiceTax);
						 $scope.bill.blKisanKalCess=Math.round($scope.serTax.stKissanCessRt*(taxAbleServiceTax/100));
						}
						else if(checkblKisanKalCess==0){
							$scope.bill.blKisanKalCess=0;
						}
							
					}
					subTotal=0;
				}
				else{
					$scope.bill.blTaxableSerTax = 0;
					$scope.bill.blSerTax = 0;
					$scope.bill.blSwachBhCess = 0;
					$scope.bill.blKisanKalCess=0;
				}
			}else{
				$scope.bill.blTaxableSerTax = 0;
				$scope.bill.blSerTax = 0;
				$scope.bill.blSwachBhCess = 0;
				$scope.bill.blKisanKalCess=0;
			}    
			
			console.log("$scope.bill.blSerTax = "+$scope.bill.blSerTax);
			console.log("$scope.bill.blSwachBhCess = "+$scope.bill.blSwachBhCess);
			console.log("$scope.customer.custSrvTaxBy = "+$scope.customer.custSrvTaxBy);
			console.log("$scope.bill.blKisanKalCess="+$scope.bill.blKisanKalCess);
			if($scope.customer.custSrvTaxBy === 'S'){
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot + $scope.bill.blSerTax + $scope.bill.blSwachBhCess+$scope.bill.blKisanKalCess);
			}else{
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot);
			} 
		}
	};
	
	
	
	$scope.removeBillDet = function(index){
		console.log("enter into removeBillDet function");
		var parseYear,parseMonth,dateSplit=[];
		if($scope.billDetSerList.length > 1){
			$scope.bill.blSubTot=Math.round($scope.bill.blSubTot-$scope.billDetSerList[index].cnmt.cnmtTOT);
			$scope.bill.blTaxableSerTax = Math.round(parseFloat($scope.bill.blSubTot * ($scope.serTax.stTaxableRt / 100)));
			$scope.bill.blSerTax = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSerTaxRt / 100)));
			$scope.bill.blSwachBhCess = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSwhBHCessRt / 100)));
			console.log($scope.bill.blSubTot);
			console.log($scope.bill.blTaxableSerTax);
			console.log($scope.bill.blSerTax);
			cnmtDate=$scope.billDetSerList[index].cnmt.cnmtDt;
			dateSplit=cnmtDate.split("-");
	         console.log("cnmtDt="+cnmtDate);
	        parseYear=parseInt(dateSplit[0]);
	        parseMonth=parseInt(dateSplit[1]);
			console.log("parseYear="+parseYear+"parseMonth="+parseMonth);
			if(parseYear>2015&&parseMonth>5||parseYear>2016){
			 $scope.bill.blKisanKalCess=Math.round($scope.serTax.stKissanCessRt*($scope.bill.blTaxableSerTax/100));
			}
			console.log("payBy="+$scope.customer.custSrvTaxBy);
			if($scope.customer.custSrvTaxBy === 'S'){
				$scope.bill.blFinalTot = Math.round($scope.bill.blFinalTot-($scope.bill.blSerTax + $scope.bill.blSwachBhCess+$scope.bill.blKisanKalCess));
			}else{
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot);
			} 
			$scope.billDetSerList.splice(index,1);
			if($scope.billDetSerList.length===0){
				$scope.cnmtCode='';
			};
		} else {
			$scope.alertToast("Minimum one CNMT is required.");
		}
	};
	
	
	$scope.getEditBlBranch=function(){
		console.log("enter get  branch..");
		var response = $http.post($scope.projectName+'/getBranch');
		   response.success(function(data,status,headers,config){
			   
			    if(data.result === "success"){
			        $scope.branchList=data.branchList;
			    }else{
			    	$scope.alertToast("problem in Branch");
			    }
			   console.log($scope.branchList); 
		   });
		};
	
	$scope.openBrhDB=function(){
		console.log("enter branch dialog box..");
		$scope.openBrhDBFlag=false;
		  $('div#brhId').dialog({
			  autoOpen: false,
			  modal:true,
			  resizable: false,
			  title: "Select Branch",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  draggable: true,
			  close: function(event,ui){
				  $(this).dialog('destroy');
				  $(this).hide();
				$scope.openBrhDBFlag=true;
			  }
		  });
	
		  $('div#brhId').dialog('open');
	};
	
	$scope.openBillDB=function(){
		$scope.openBillDBFlag=false;
		 $('div#openBillId').dialog({
			  autoOpen: false,
			  modal: true,
			  resizable:false,
			  title: "Select Bill",
			  show: UDShow,
			  hide: UDHide,
			  position:UDPos,
			  draggable: true,
			   close: function(event,ui){
				   $(this).dialog('destroy');
				   $(this).hide();
				 $scope.openBillDBFlag=true;
			   }
			   });
		 	$('div#openBillId').dialog('open');
		 };
	
	
	$scope.saveBill=function(bill){
		console.log("save bill..");
		$scope.blBillNo=bill.blBillNo;
		console.log($scope.blBillNo);
		$('div#openBillId').dialog('close');
	};
	
	$scope.saveBranch=function(brh){
		console.log(brh.branchId);
		$scope.blBrhId=brh.branchId;
		$scope.branch=brh.branchName;
		 $('div#brhId').dialog('close');
		  
		 var req={
				 	"branchId" : brh.branchId
		 			};
		 var response=$http.post($scope.projectName+'/getBillNo',req);
		  response.success(function(data,status,headers,config){
			  if(data.result ==="success"){
				  $scope.billList=data.billList;
			  }else{
				  $scope.alertToast("problem in bill");
			  }
		  });
		 
		
	};
	
	$scope.billModify=function(){
		console.log("modify bill..");
		console.log($scope.billDetSerList);
		$scope.bill=$scope.billDetailSerialList[0].blList[0];
		$scope.serTax=$scope.billDetailSerialList[0].serTax;
		$scope.customer=$scope.billDetailSerialList[0].cust;
		$scope.cnmtList=$scope.billDetailSerialList[0].cnmtList;
		$scope.cnmtCode='';
	//	console.log($scope.bill);
	//	console.log($scope.bill.blFinalTot);
	
	};
	
	
	$scope.editBillSubmit=function(editBillForm){
		console.log("editBillSubmit function..");
		console.log(editBillForm);
		 if(editBillForm.$invalid){
			 $scope.alertToast("correct bill form");
		 }else{
			 $("#firstSubmit").attr("disabled", "disabled");
			 var req={
					 
					"branchId":  $scope.blBrhId,
					"billNo"  :  $scope.blBillNo
			 };
			 
			var response=$http.post($scope.projectName+'/getEditBillInfo',req);
			  response.success(function(data,status,headers,config){
				  $("#firstSubmit").removeAttr("disabled");
				  if(data.result === "success"){
					  $scope.billDetailSerialList=data.billList;
					   console.log($scope.billDetailSerialList);
					    if($scope.billDetailSerialList[0].blList.length<2){
					    	$scope.billDetSerList=$scope.billDetailSerialList[0].blDetList;
					    	 console.log($scope.billDetSerList);
					    	 console.log("billDetialSize="+$scope.billDetSerList.length);
					    	$scope.editBillFlag=false;
					    	$scope.billModify();
					    	$scope.alertToast("success");
					    }else{
					    	$scope.alertToast("sorry,Dublicate Bill");
					    }
				  }else{
					  $scope.alertToast("problem in bill or bill is not available");
				  }
			  });
			  response.error(function(data, status, headers, config) {
				  $("#firstSubmit").removeAttr("disabled");
					$scope.errorToast(data.error);
			   });
		 }
		
	};
	
	$scope.billSubmit=function(billForm){
		if (billForm.$invalid) {
			$scope.isFormSubmitted = true;
		} else {
			$scope.isFormSubmitted = false;
			console.log("modify bill submit..");
			console.log(billForm);
			$scope.billService = {};
			$scope.billService.bdSerList = [];
			$scope.billService.bdSerList = $scope.billDetSerList;
			$scope.billService.bill = $scope.bill;
			console.log($scope.billDetSerList);
			console.log("size of $scope.billDetSerList = "+$scope.billDetSerList.length);
			console.log("size of $scope.billService.bdSerList = "+$scope.billService.bdSerList.length);
			$("#secondSubmit").attr("disabled", "disabled");
			var response = $http.post($scope.projectName+'/submitmodifyBillN',$scope.billService);
			
			response.success(function(data, status, headers, config){
				$("#secondSubmit").removeAttr("disabled");
				$scope.editBillFlag=true;
				console.log(data.csMessage);
				if(data.result === "success"){
				  $scope.loadingFlag = false;
				  $scope.alertToast("Modified success");
				  $scope.billService = {};
				  $scope.billService.bdSerList = [];
				  $scope.billDetSerList = [];
				  $scope.bill = {};
				  $scope.customer = {};
				} else if(data.result === "error") {
					$scope.alertToast(data.msg);
				}
			}); 
			response.error(function(data, status, headers, config) {
				$("#secondSubmit").removeAttr("disabled");
				$scope.alertToast("It has not modified");
			}); 
		}
	};
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getEditBlBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 };
	
}]);