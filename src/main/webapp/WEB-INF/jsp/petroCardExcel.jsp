<title>Petro Card Excel </title>
<div ng-show="operatorLogin || superAdminLogin">
<div class="row" ng-hide="generateFundData">
 <form ng-submit="generatePetroAllocation()" class="col s12 card"
style="align: center; padding-top: 40px;
 background-color: rgba(125, 125, 125, 0.3);">
 
 	<div class="row">
			<div class="input-field col s12 center">
				<input type="submit" value="View Current Date Fund Details">
			</div>
		</div>

</form> 
</div>

<!-- <div class="row" ng-hide="showPetroList">
 <table style="border: 1px solid black; border-collapse: collapse;width: 100%">
 <tr style="background-color:white;">
 <th style="width: 20px; color:black;">Branch</th>
 <th style="width: 20px; color:black;">Card Type</th>
 <th style="width: 200px; color:black;">Card No.</th>
 <th style="width: 150px; color:black;">Challan No.</th>
 <th style="width: 150px; color:black;">CNMT No.</th>
 <th style="width: 150px; color:black;">Lorry No.</th>
 <th style="width: 100px; color:black;">Amount</th>
 <th style="width: 150px; color:black;">Beneficiary Email Id</th>
 <th style="width: 150px; color:black;">IFSC Code</th>
 <th style="width: 150px; color:black;">Instrument Amount</th>
 <th style="width: 150px; color:black;">Update</th>
 </tr>
<tr ng-repeat="petroAll in petroList">
<form>
<td style="width: 20px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="petroAll.bCode" readonly="readonly"></td>
<td style="width: 20px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="petroAll.cardType" readonly="readonly"></td>
<td style="width: 200px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="petroAll.cardNo" readonly="readonly"></td>
<td style="width: 150px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="petroAll.cnmtNo" readonly="readonly"></td>
<td style="width: 150px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="petroAll.challanNo" readonly="readonly"></td>
<td style="width: 150px"><input style="width: 200px;background-color: gray;color: white;" type="text" ng-model="petroAll.loryNo" readonly="readonly"></td>
<td style="width: 100px"><input style="width: 180px;background-color: gray;color: white;" type="text" ng-model="petroAll.amount" min="0"></td>
<td style="width: 220px"><input style="width: 220px;background-color: gray;color: white;" type="text" ng-model="fundAll.beneficiaryEmailId" readonly="readonly"></td>
<td style="width: 180px"><input style="width: 180px;background-color: gray;color: white;" type="text" ng-model="fundAll.ifscCode" readonly="readonly"></td>
<td style="width: 250px"><input style="width: 150px;" type="number" ng-model="fundAll.instrumentAmount" min="0"></td>
<td><input type="submit" value="Update" ng-click="updateFundAllocation(petroAll,$index)"></td>
</form>
</tr>
</table> 

</div>-->

<div class="row" ng-hide="showPetroList">

<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Verification Details</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
            	<th class="colclr">Branch</th>
            	<th class="colclr">CNMT No.</th>
            	<th class="colclr">Challan No.</th>
            	<th class="colclr">Chln Date</th>
            	<th class="colclr">Lorry No.</th>
            	<th class="colclr">From</th>
            	<th class="colclr">To</th>
            	<th class="colclr">Card Type</th>
            	<th class="colclr">Card No.</th>
            	<th class="colclr">Chln Freight</th>
            	<th class="colclr">Advance</th>
            	<th class="colclr">TDS</th>
            	<th class="colclr">Amount</th>
<!--             	<th class="colclr">Update</th> -->
            	<th class="colclr">Reject</th>
            	<th class="colclr">Approved</th>
            	 </tr>	
	         
	        <tr ng-repeat="ptro in petroList">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<td class="rowcel">{{ptro.bCode}}</td>
	        	<td class="rowcel">{{ptro.cnmtNo}}</td>
	        	<td class="rowcel">{{ptro.challanNo}}</td>
	        	<td class="rowcel">{{ptro.chlnDt}}</td>
	        	<td class="rowcel">{{ptro.loryNo}}</td>
	        	<td class="rowcel">{{ptro.fromStn}}</td>
	        	<td class="rowcel">{{ptro.toStn}}</td>
	        	<td class="rowcel">{{ptro.cardType}}</td>
	        	<td class="rowcel">{{ptro.cardNo}}</td>
	        	<td class="rowcel">{{ptro.freight}}</td>
	        	<td class="rowcel">{{ptro.advance}}</td>
	        	<td class="rowcel">{{ptro.tdsAmt}}</td>
	        	<td class="rowcel">{{ptro.amount}}</td>
 			<!-- <td class="rowcel" >
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="updateDetail(ptro)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td> -->
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="rejectPayment(ptro,$index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="updateFundAllocation(ptro,$index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
			</tr>
	</table>

</div>



<div class="col s12 center">
			<form method="post" action="downloadPetroAllExcel" enctype="multipart/form-data">
				<input type="submit" id="printXlsId" value="Print XLS">
		  	</form>
		</div>

</div>