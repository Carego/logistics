<div ng-show="adminLogin || superAdminLogin">
<title>Display Customer Rep</title>
<div class="container">
	<div class="row">
		<div class="col s2">
			<input type="radio" name="custRepList" ng-model="custRepByModal"
				ng-click="enableModalTextBox()">
		</div>
		<div class="col s10">
			<input type="text" id="custRepByMId" name="custRepByM"
				ng-model="custRepByM" disabled="disabled"
				ng-click="OpenCustRepCodeDB()">
		</div>
	</div>

	<div class="row">
		<div class="col s2">
			<input type="radio" name="custRepList" ng-model="custRepByAuto"
				ng-click="enableAutoTextBox()">
		</div>
		<div class="col s8">
			<input type="text" id="custRepByAId" name="custRepByA"
				ng-model="custRepByA" disabled="disabled"
				ng-keyup="getCustRepCodeList()">
		</div>
		<div class="col s2">
			<input class="col s12 btn" type="button" id="ok" disabled="disabled"
				value="OK" ng-click="getCustomerRepList()">
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<input id="bkToList" type="button" value="Back To list"
				ng-click="backToList()">
		</div>
	</div>
</div>

<div id="custRepCodeDB" ng-hide="custRepCodeDBFlag">
	<input type="text" name="filterCustomerRepCode"
		ng-model="filterCustomerRepCode"
		Placeholder="Search by Customer Rep Codes">
	<table>
		<tr
			ng-repeat="custRepCodes in custRepCodeList | filter:filterCustomerRepCode ">
			<td><input type="radio" name="custRepCodes"
				value="{{ custRepCodes }}" ng-model="custRepCodes1"
				ng-click="saveCustomerRepCode(custRepCodes)"></td>
			<td>{{ custRepCodes }}</td>
		</tr>
	</table>
</div>
<div ng-hide="showCustomerRep">
	<div class="container">
		<div class="row">
			<div class="input-field col s12 m4 l3">
				<input type="text" id="crCode" name="crCode"
					ng-model="custrep.crCode" value={{custrep.crCode}} readonly><label>Customer
					Rep. Code</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="crName" name="crName"
					ng-model="custrep.crName" value="custrep.crName" ng-minlength="3"
					ng-maxlength="40" ng-required="true"><label>Customer
					Rep. Name</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="crDesignation" name="crDesignation"
					ng-model="custrep.crDesignation" ng-minlength="3" ng-maxlength="40"
					ng-model-options="{ debounce: 200 }"
					ng-keyup="getCustDesigList(custrep)" ng-blur="fillCustDesigVal()"
					ng-required="true"> <label>Customer Rep.
					Designation</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="crMobileNo" name="crMobileNo"
					ng-model="custrep.crMobileNo" value={{custrep.crMobileNo}}
					ng-minlength="4" ng-maxlength="15" ng-required="true"><label>Customer
					Rep. Mobile No.</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="crEmailId" name="crEmailId"
					ng-model="custrep.crEmailId" value={{custrep.crEmailId}}
					ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/"
					ng-maxlength="40" ng-required="true"><label>Customer
					Rep. Email Id</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="custRefNo" name="custRefNo"
					ng-model="custrep.custRefNo" value={{custrep.custRefNo}}
					ng-minlength="3" ng-maxlength="40"><label>Customer
					Rep. Ref. No.</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 center">
			<input type="button" value="update"
				ng-click="editCustomerRepSubmit(custrep)">
		</div>
	</div>

</div>

<div ng-hide="isViewNo" id="CustomerRepTableDB" class="container">
	<input type="text" name="filterCustRep" ng-model="filterCustRep.crCode"
		placeholder="Search by Customer Rep Codes">
	<table class="table-hover table-bordered table-condensed">
		<tr>
			<th></th>

			<th>Customer Rep. Name</th>

			<th>Customer Rep. Code</th>

			<th>User Code</th>

			<th>Time Stamp</th>

		</tr>

		<tr ng-repeat="custrep in custRepList | filter:filterCustRep">
			<td><input id="{{ custrep.crCode }}" type="checkbox"
				value="{{ custrep.crCode }}"
				ng-checked="selection.indexOf(custrep.crCode) > -1"
				ng-click="toggleSelection(custrep.crCode)" /></td>
			<td>{{ custrep.crName }}</td>
			<td>{{ custrep.crCode }}</td>
			<td>{{ custrep.userCode }}</td>
			<td>{{ custrep.creationTS }}</td>
		</tr>

	</table>
	<table>
		<tr>
			<td class="center"><input type="submit" value="Submit"
				ng-click="verifyCustomerRep()"></td>
		</tr>
	</table>
</div>
</div>