package com.mylogistics.model;

//import java.util.Date;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "phonemstr")
public class PhoneMstr {
	
	
	@Id
	@Column(name="phId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int phId;

	//Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	@JsonBackReference(value = "phnBrh")
	@JoinColumn(name = "branchId") 
	private Branch branch;
	
	@Column(name="phNo")
	private String phNo;
	
	@Column(name="phType")
	private String phType;
	
	@Column(name="phTypeAllow")
	private String phTypeAllow;
	
	@Column(name="phHldrName")
	private String phHldrName;
	
	//One To One
	@OneToOne(cascade = CascadeType.ALL)
	private Address phBillingAdd;
	
	//Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	@JsonBackReference(value = "phnEmp")
	@JoinColumn(name = "empId") 
	private Employee employee;
	
	@Column(name="phWEF")
	private Date phWEF;
	
	@Column(name="phWET")
	private Date phWET;
	
	@Column(name="phInstDt")
	private Date phInstDt;
	
	@Column(name="phDiscDt")
	private Date phDiscDt;
	
	@Column(name="phDepAmt")
	private double phDepAmt;
	
	@Column(name="phDepDt")
	private Date phDepDt;
	
	@Column(name="phRefundAmt")
	private double phRefundAmt;
	
	@Column(name="phRefundDt")
	private Date phRefundDt;
	
	@Column(name="phSerPro")
	private String phSerPro;
	
	@Column(name="phPlan")
	private String phPlan;
	
	@Column(name="phModPay")
	private String phModPay;
	
	//doubt for double
	@Column(name="phLmtAmt")
	private double phLmtAmt;
	
	@Column(name="phAcNo")
	private String phAcNo;
	
	@Column(name="phBillDay")
	private String phBillDay;
	
	@Column(name="phDueDay")
	private String phDueDay;

	public int getPhId() {
		return phId;
	}

	public void setPhId(int phId) {
		this.phId = phId;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getPhNo() {
		return phNo;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public String getPhType() {
		return phType;
	}

	public void setPhType(String phType) {
		this.phType = phType;
	}

	public String getPhTypeAllow() {
		return phTypeAllow;
	}

	public void setPhTypeAllow(String phTypeAllow) {
		this.phTypeAllow = phTypeAllow;
	}

	public String getPhHldrName() {
		return phHldrName;
	}

	public void setPhHldrName(String phHldrName) {
		this.phHldrName = phHldrName;
	}

	public Address getPhBillingAdd() {
		return phBillingAdd;
	}

	public void setPhBillingAdd(Address phBillingAdd) {
		this.phBillingAdd = phBillingAdd;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getPhWEF() {
		return phWEF;
	}

	public void setPhWEF(Date phWEF) {
		this.phWEF = phWEF;
	}

	public Date getPhWET() {
		return phWET;
	}

	public void setPhWET(Date phWET) {
		this.phWET = phWET;
	}

	public Date getPhInstDt() {
		return phInstDt;
	}

	public void setPhInstDt(Date phInstDt) {
		this.phInstDt = phInstDt;
	}

	public Date getPhDiscDt() {
		return phDiscDt;
	}

	public void setPhDiscDt(Date phDiscDt) {
		this.phDiscDt = phDiscDt;
	}

	public double getPhDepAmt() {
		return phDepAmt;
	}

	public void setPhDepAmt(double phDepAmt) {
		this.phDepAmt = phDepAmt;
	}

	public Date getPhDepDt() {
		return phDepDt;
	}

	public void setPhDepDt(Date phDepDt) {
		this.phDepDt = phDepDt;
	}

	public double getPhRefundAmt() {
		return phRefundAmt;
	}

	public void setPhRefundAmt(double phRefundAmt) {
		this.phRefundAmt = phRefundAmt;
	}

	public Date getPhRefundDt() {
		return phRefundDt;
	}

	public void setPhRefundDt(Date phRefundDt) {
		this.phRefundDt = phRefundDt;
	}

	public String getPhSerPro() {
		return phSerPro;
	}

	public void setPhSerPro(String phSerPro) {
		this.phSerPro = phSerPro;
	}

	public String getPhPlan() {
		return phPlan;
	}

	public void setPhPlan(String phPlan) {
		this.phPlan = phPlan;
	}

	public String getPhModPay() {
		return phModPay;
	}

	public void setPhModPay(String phModPay) {
		this.phModPay = phModPay;
	}

	public double getPhLmtAmt() {
		return phLmtAmt;
	}

	public void setPhLmtAmt(double phLmtAmt) {
		this.phLmtAmt = phLmtAmt;
	}

	public String getPhAcNo() {
		return phAcNo;
	}

	public void setPhAcNo(String phAcNo) {
		this.phAcNo = phAcNo;
	}

	public String getPhBillDay() {
		return phBillDay;
	}

	public void setPhBillDay(String phBillDay) {
		this.phBillDay = phBillDay;
	}

	public String getPhDueDay() {
		return phDueDay;
	}

	public void setPhDueDay(String phDueDay) {
		this.phDueDay = phDueDay;
	}
}
