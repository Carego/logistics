package com.mylogistics.services;


public class ConstantsValues {
	public static final String ERROR = "error";
	public static final String SUCCESS = "success";
	public static final String RESULT = "result";
	public static final String STATUS = "status";
	
	public static final String TEL_FANAME = "TELEPHONES";
	public static final String ELECT_FANAME = "ELECTRIC & WATER CHARGES";
	public static final String CAR_EXP_FANAME = "CAR EXPENSES";
	public static final String SANDM_EXP_FANAME = "SCOOTER & MOTOR CYCLE EXPENSES";
	public static final String TRAV_EXP_FANAME = "TRAVELLING";
	public static final String B_PROM_FANAME = "BUSINESS PROMOTION";
	public static final String LHPV_FANAME = "LORRY HIRE";//1200101
	public static final String LHPV_TDS = "TDS (LORRY HIRE)";//1110061
	public static final String LHPV_CASH_DIS = "CASH DISCOUNT LORRY HIRE";//1200337
	public static final String LHPV_MUNS = "MUNSHIANA THROUGH LHPV";//1200365
	public static final String LHPV_CLAIM = "CLAIMS THROUGH LHPV";//1200003
	public static final String CASH_IN_HAND = "CASH IN HAND";//
	public static final String RENT_FANAME = "RENT";//1200301
	public static final String LORRY_HIRE = "LORRY HIRE";//1200101
	public static final String CASH_DIS_LH = "CASH DISCOUNT LORRY HIRE";//1200337
	public static final String TDS_LRY_HIRE = "TDS (LORRY HIRE)";//1110061
	public static final String MUNS_LRY_HIRE = "MUNSHIANA THROUGH LHPV";//1200365
	public static final String CLAIM_LRY_HIRE = "CLAIMS THROUGH LHPV";//1200003 
	public static final String LRYADV_AND_OTH = "LORRY ADVANCE AND OTHERS";
	public static final String FREIGHT_COLLECTION = "FREIGHT COLLECTION";
	public static final String FREIGHT_TDS = "TDS (FREIGHT)";
	public static final String FREIGHT_SRV_TAX = "SERVICE TAX{FREIGHT}";
	public static final String CLAIM = "CLAIM PAID";
	public static final String EXCEPTION_MSG = "Your entry is not saved. Please try again.";
	
	public static final String TDS_FANAME = "TDS%";
	
	public static final String FAP_BRANCH = "branch";
	public static final String FAP_EMP = "employee";
	public static final String FAP_CUST = "customer";
	public static final String FAP_DLY_CONT = "dlycontract";
	public static final String FAP_REG_CONT = "regcontract";
	public static final String FAP_BILL = "bill";
	public static final String FAP_BANK = "bank";
	public static final String FAP_OWN = "owner";
	public static final String FAP_BRK = "broker";
	public static final String FAP_CUST_REP = "custrep";
	public static final String FAP_ASS_LIAB = "assestsnliab";
	public static final String FAP_PR_LS = "profitnloss";
	public static final String FAP_MISC = "miscfa";
	
	public static final String FAPC_BRANCH = "01";
	public static final String FAPC_EMP = "02";
	public static final String FAPC_CUST = "03";
	public static final String FAPC_DLY_CONT = "04";
	public static final String FAPC_REG_CONT = "05";
	public static final String FAPC_BILL = "06";
	public static final String FAPC_BANK = "07";
	public static final String FAPC_OWN = "08";
	public static final String FAPC_BRK = "09";
	public static final String FAPC_CUST_REP = "10";
	public static final String FAPC_ASS_LIAB = "11";
	public static final String FAPC_PR_LS = "12";
	public static final String FAPC_MISC = "13";
	
	public static final String REV_VOUCHER = "Voucher Reversal";
	public static final String CASH_WITHDRAW = "CashWithdraw";
	public static final String CASH_DEPOSITE_VOUCH = "CashDeposite";
 	public static final String TEL_VOUCHER = "Telephone";
	public static final String ELE_VOUCHER = "Electricity";
	public static final String BP_VOUCHER = "Business Promotion";
	public static final String VEH_VOUCHER = "Vehicle";
	public static final String VEH_TRAVEL = "Travel";
	public static final String JR_VOUCHER = "Journal Voucher";
	public static final String IBTV_VOUCHER = "Inter Branch TV C/D";
	public static final String MNY_RCT = "Money Receipt";
	public static final String BILL = "BILL";
	
	
	public static final String LHPV_ADV = "LHPV ADVANCE";
	public static final String LHPV_BAL = "LHPV BALANCE";
	public static final String LHPV_SUP = "LHPV SUPPLEMENTARY";
	
	public static final String RTO_ADD = "RTO";
	public static final String VEH_OWN_ADD = "Owner Address For VehicleMstr";
	
	public static final String SUPER_ADMIN_B_CODE = "GUR1400001";
	public static final String SUPER_ADMIN_U_CODE = "ARC2015GUR99999";
	public static final String OTH_ADD = "Other Address";
	public static final String PF_NO = "GNGGN1373559/";
	
	public static final String BRANCH = "branch";
	
	public static final String CASH_STMT_STATUS = "cashStmtStatus";
	
	public static final String BANK_MSTR = "bankMstr";
	
	public static final String BANK_CODE_LIST = "bankCodeList";
	
	public static final String ATM_CODE_LIST = "atmCodeList";
	
	public static final String CHEQUE_LEAVES = "chequeLeaves";
	
	public static final String CONST_TVNO = "0000000000000";
	
	public static final String HB_FaCode = "0100001";
	
	public static final int kgInTon = 1000;
	
	public static final String STATUS_CNMT = "cnmt";
	public static final String STATUS_CHLN = "chln";
	public static final String STATUS_SEDR = "sedr";
	
	public static final String RM_ID = "rmId";
	public static final String RM_FOR = "rentFr";
	public static final String RM_LL = "landLord";
	public static final String BRH_ID = "brhId";
	public static final String BRH_NAME = "brhName";
	public static final String EMP_ID = "empId";
	public static final String BRH_FA_CODE = "brhFaCode";
	public static final String EMP_FA_CODE = "empFaCode";
	public static final String EMP_NAME = "empName";
	
	public static final String OA_MR = "On Accounting";
	public static final String PD_MR = "Payment Detail";
	public static final String DP_MR = "Direct Payment";
	
	public static final String CS_BEGIN_DT = "2015-04-01";
	
	public static final long TIME_UTC_GMT_DIFFERENCE = 19800000;
	
	public static final String TYPE_BARBIL = "bbl";
	
	/*Mail constants*/
	//local machine
	//public static final String FOLDER_LOCATION = "C:/Users/MatterOfTech/Desktop/download";
	
	//local centOs
	//public static final String FOLDER_LOCATION = "/home/arif_logistics/reports";
	public static final String FOLDER_LOCATION = "/home/tcgppl/public_html/tcgppl.com/carego/reports";
	
	public static final String NOT_FOUND = "notFound";
	public static final String EMPTY="empty";
	public static final String MSG = "msg";
	
}
