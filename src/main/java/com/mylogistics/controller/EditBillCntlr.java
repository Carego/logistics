package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.EditBillDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.ServiceTaxDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.model.Bill;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.ServiceTax;
import com.mylogistics.model.Station;
import com.mylogistics.services.BillService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class EditBillCntlr {
	
	public static final Logger logger = Logger.getLogger(EditBillCntlr.class);

@Autowired
private ServiceTaxDAO serviceTaxDAO;	
	
@Autowired
private EditBillDAO editBillDao;

@Autowired
private CnmtDAO cnmtDAO;
	
@Autowired
private SessionFactory sessionFactory;

@Autowired
private DailyContractDAO dailyContractDAO;

@Autowired
private RegularContractDAO regularContractDAO;

@Autowired
private Cnmt_ChallanDAO cnmt_ChallanDAO;

@Autowired
private ChallanDAO challanDAO;

@Autowired
private StationDAO stationDAO;

@Autowired
private CustomerDAO customerDAO;

@RequestMapping(value="/getBranch",method=RequestMethod.POST)
public @ResponseBody Object getAllBranchInfo(){
	
		
	System.out.println("getAllBranchInfo...");
	  List<Branch> branchList=new ArrayList<Branch>();
	  Map<String,Object> map=new HashMap<String,Object>();
	  Session session=sessionFactory.openSession();
		try{			
		     branchList=editBillDao.getAllBranch(session);
		     	if(!branchList.isEmpty()){
		     map.put("branchList",branchList);
		     map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		     	}else{
		     		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		     	}
		     session.close();
		}catch(Exception e){
			e.printStackTrace();
			session.close();
		}
	 return map;
	}

 @RequestMapping(value="/getBillNo",method=RequestMethod.POST)
 public @ResponseBody Object getAllBillInfo(@RequestBody Map<String,Object> billMap){
	 Integer brhId=(Integer)billMap.get("branchId");
	 System.out.println("brhId="+brhId);
	 Map<String,Object> map=new HashMap<String,Object>();
	 List<Bill> billList=new ArrayList<Bill>();
	 Session session=sessionFactory.openSession();
	 try
	 {
		 billList=editBillDao.getAllBill(session,brhId);
		 if(!billList.isEmpty()){
			 map.put("billList",billList);
			 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		 }else{
			 map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		 }
		 session.close();
	 }catch(Exception e){
		 e.printStackTrace();
		 session.close();
	 }
	 
	 return map;
 }
 
 @RequestMapping(value="/getEditBillInfo",method=RequestMethod.POST)
 public @ResponseBody Object getBillInfo(@RequestBody Map<String,Object> clientMap){
	 System.out.println("getBillInfo.....");
	  Integer brhId=(Integer)clientMap.get("branchId");
	  String blBillNO=(String)clientMap.get("billNo");
	  List<Map<String,Object>> billList=new ArrayList<Map<String,Object>>();
	 Map<String, Object> map=new HashMap<String,Object>();
	 System.out.println("brhId="+brhId);
	 System.out.println("blBillNo="+blBillNO);
	 Session session=sessionFactory.openSession();
	 try{
	  ServiceTax serviceTax=serviceTaxDAO.getLastSerTax();
	 billList=(List<Map<String,Object>>)editBillDao.getBillFromBranch(session,blBillNO,brhId,serviceTax);
	  if(!billList.isEmpty()){
		   map.put("billList",billList);
		   map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	  }else{
		  map.put(ConstantsValues.ERROR,ConstantsValues.ERROR);
		  map.put("billList",billList);
		  
	  }
	  session.close();
	 }
	 catch(Exception e){
		 e.printStackTrace();
		 map.put(ConstantsValues.ERROR,ConstantsValues.ERROR);
		 session.close();
	 }
	 
	 System.out.println(billList.size());
	 return map;
 }
 
  @RequestMapping(value="/submitmodifyBill",method=RequestMethod.POST)
 public @ResponseBody Object saveModifyBill(@RequestBody BillService billService){
	  System.out.println("enter saveModifyBill....");
	  Map<String,Object> map=new HashMap<String,Object>();
	    System.out.println(billService.getBill().getBlBillNo());
	    Session session=sessionFactory.openSession();
	    Transaction transaction =session.beginTransaction();
	    try{
	     map=(Map)editBillDao.modifyBill(session,billService);
	       transaction.commit();
	       session.close();
	    }catch(Exception e){
	    	e.printStackTrace();
	    	 transaction.rollback();
	    	 map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
	    	session.close();
	    }
	  return map;
  }
 
  @RequestMapping(value="/getCnmtInfo",method=RequestMethod.POST)
  public @ResponseBody Object getCnmtInfo(@RequestBody Map<String,Object> cnmtMap){
	  System.out.println("getCnmtInfo.....");
	  Map<String,Object> map = new HashMap<>();
		String cnmtCode = (String) cnmtMap.get("cnmtCode");
		Date billDt = Date.valueOf((String) cnmtMap.get("billDt"));
		if(cnmtCode != null){
			List<Cnmt> cnmtList = cnmtDAO.getCnmtList(cnmtCode);
			System.out.println("!cnmtList.isEmpty()="+!cnmtList.isEmpty());
			
			if(!cnmtList.isEmpty()){
				Cnmt cnmt = cnmtList.get(0);
				if(cnmt.getContractCode().substring(0,3).equalsIgnoreCase("reg")){
					List<RegularContract> regList = regularContractDAO.getRegContractData(cnmt.getContractCode());
					System.out.println("regList.get(0).getRegContBillBasis() = "+regList.get(0).getRegContBillBasis());
					if(!regList.isEmpty()){
						map.put("billBasis", regList.get(0).getRegContBillBasis());
					}else{
						map.put("billBasis", "chargeWt");
					}
				}else{
					List<DailyContract> dlyList = dailyContractDAO.getDailyContractData(cnmt.getContractCode());
					if(!dlyList.isEmpty()){
						map.put("billBasis", dlyList.get(0).getDlyContBillBasis());
					}else{
						map.put("billBasis", "chargeWt");
					}
				}
				
				Integer cmId = cnmt.getCmId();
				
				if(cmId > 0){
					List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
					if(!chlnIdList.isEmpty()){
						List<Map<String,Object>> chlnList = new ArrayList<>();
						for(int i=0;i<chlnIdList.size();i++){
							//List<Challan> actChList = new ArrayList<>();
							//actChList = challanDAO.getChallanById(chlnIdList.get(i));
							Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
							chlnList.add(chlnInfo);
							/*if(!actChList.isEmpty()){
								chlnList.add(actChList.get(0));
							}*/
						}
						
						/*System.out.println("chlnList.get(chlnList.size() - 1).getChlnCode() = "+chlnList.get(chlnList.size() - 1).getChlnCode());
						System.out.println("chlnList.get(chlnList.size() - 1).getChlnLryNo() = "+chlnList.get(chlnList.size() - 1).getChlnLryNo());
						if(!chlnList.isEmpty()){
							String lryNo = "";
							if(chlnList.get(chlnList.size() - 1).getChlnLryNo() != null){
								lryNo = chlnList.get(chlnList.size() - 1).getChlnLryNo();
							}else{
								lryNo = challanDetailDAO.getLryNoByChCode(chlnList.get(chlnList.size() - 1).getChlnCode());
							}
							map.put("lryNo",lryNo);
						}else{
							map.put("lryNo","");
						}*/
						
						if(!chlnList.isEmpty()){
							map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
						}else{
							map.put("lryNo","");
						}
						
						
						List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
						List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
						
						if(!stnList1.isEmpty()){
							map.put("frStn",stnList1.get(0).getStnName());
						}else{
							map.put("frStn","");
						}
						
						if(!stnList2.isEmpty()){
							map.put("toStn",stnList2.get(0).getStnName());
						}else{
							map.put("toStn","");
						}
						
						map.put("cnmt",cnmt);
						map.put("chlnList",chlnList);
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					int res = customerDAO.checkBillPer(Integer.parseInt(cnmt.getCustCode()),billDt);
					System.out.println("value of res = "+res);
					if(res > 0){
						List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
						System.out.println("size of chlnIdList = "+chlnIdList.size());
						if(!chlnIdList.isEmpty()){
							List<Map<String,Object>> chlnList = new ArrayList<>();
							
							for(int i=0;i<chlnIdList.size();i++){
								/*List<Challan> actChList = new ArrayList<>();
								actChList = challanDAO.getChallanById(chlnIdList.get(i));
								if(!actChList.isEmpty()){
									chlnList.add(actChList.get(0));
								}*/
								Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
								chlnList.add(chlnInfo);
							}
							
							/*System.out.println("chlnList.get(chlnList.size() - 1).getChlnCode() = "+chlnList.get(chlnList.size() - 1).getChlnCode());
							System.out.println("chlnList.get(chlnList.size() - 1).getChlnLryNo() = "+chlnList.get(chlnList.size() - 1).getChlnLryNo());
							if(!chlnList.isEmpty()){
								String lryNo = "";
								if(chlnList.get(chlnList.size() - 1).getChlnLryNo() != null){
									System.out.println("*********************");
									lryNo = chlnList.get(chlnList.size() - 1).getChlnLryNo();
								}else{
									System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%");
									lryNo = challanDetailDAO.getLryNoByChCode(chlnList.get(chlnList.size() - 1).getChlnCode());
								}
								System.out.println("lryNo ===> "+lryNo);
								map.put("lryNo",lryNo);
							}else{
								map.put("lryNo","");
							}*/
							
							if(!chlnList.isEmpty()){
								map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
							}else{
								map.put("lryNo","");
							}
							
							List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
							List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
							
							if(!stnList1.isEmpty()){
								map.put("frStn",stnList1.get(0).getStnName());
							}else{
								map.put("frStn","");
							}
							
							if(!stnList2.isEmpty()){
								map.put("toStn",stnList2.get(0).getStnName());
							}else{
								map.put("toStn","");
							}
							
							map.put("cnmt",cnmt);
							map.put("chlnList",chlnList);
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}else{
						System.out.println("Inside MSG :");
						map.put("msg","Please upload scan image of "+cnmtCode+" OR allow Customer for bill permission");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}
			}else{
				map.put("msg",cnmtCode+" is invalid cnmt");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg",cnmtCode+" is invalid cnmt");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		System.out.println("map.get(billBasis)  == "+map.get("billBasis"));
	
	  return map;
  }
  
  @RequestMapping(value="/submitmodifyBillN",method=RequestMethod.POST)
	 public @ResponseBody Object saveModifyBillN(@RequestBody BillService billService){
		  logger.info("enter saveModifyBill....");
		  Map<String,Object> resultMap = new HashMap<String,Object>();
		  logger.info(billService.getBill().getBlBillNo());
		  Session session = sessionFactory.openSession();
		  Transaction transaction = session.beginTransaction();
		  try {
			  resultMap = (Map)editBillDao.modifyBillN(session, billService);
			  transaction.commit();
		  } catch(Exception e){
			  e.printStackTrace();
			  transaction.rollback();
			  resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			  resultMap.put("msg", "Somthing went wrong.");
		  } finally {
			  session.close();
		  }
		  return resultMap;
	  }
  
  @RequestMapping(value="/submitmodifyBillGST",method=RequestMethod.POST)
	 public @ResponseBody Object submitmodifyBillGST(@RequestBody BillService billService){
		  logger.info("enter saveModifyBill....");
		  Map<String,Object> resultMap = new HashMap<String,Object>();
		  logger.info(billService.getBill().getBlBillNo());
		  Session session = sessionFactory.openSession();
		  Transaction transaction = session.beginTransaction();
		  try {
			  resultMap = (Map)editBillDao.modifyBillGST(session, billService);
			  transaction.commit();
		  } catch(Exception e){
			  e.printStackTrace();
			  transaction.rollback();
			  resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			  resultMap.put("msg", "Somthing went wrong.");
		  } finally {
			  session.close();
		  }
		  return resultMap;
	  }
  
}
