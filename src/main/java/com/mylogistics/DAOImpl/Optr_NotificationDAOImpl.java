package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.Optr_NotificationDAO;
import com.mylogistics.constants.Optr_NotificationCNTS;
import com.mylogistics.model.Optr_Notification;

public class Optr_NotificationDAOImpl implements Optr_NotificationDAO {

	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	private static Logger logger = Logger.getLogger(Optr_NotificationDAOImpl.class);
	
	@Autowired
	public Optr_NotificationDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveOptr_Notification(Optr_Notification optr_Notification){
		logger.info("Enter into saveOptr_Notification()");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(optr_Notification);
			 transaction.commit();
			 System.out.println("optr_Notification Data is inserted successfully");
			 temp = 1;
			 session.flush();
		 } catch(Exception e){
			 logger.error("Exception : "+e);
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 logger.info("Exit from saveOptr_Notification()");
		 return temp;
	 }
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Optr_Notification> getOptr_NotificationData(String operatorCode){
		List<Optr_Notification> optr_Notifications = new ArrayList<Optr_Notification>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Optr_Notification.class);
			cr.add(Restrictions.eq(Optr_NotificationCNTS.OPN_OPCODE, operatorCode));
			cr.add(Restrictions.eq(Optr_NotificationCNTS.OPN_ISREC, "no"));
			optr_Notifications=cr.list();
		  }
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return optr_Notifications;
	}
	
	
	 @SuppressWarnings("unchecked")
	@Transactional
	 public int updateIsReceiveYes(String dispatchcode){
		int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Optr_Notification optr_Notification = new Optr_Notification();
			 List<Optr_Notification> list = new ArrayList<Optr_Notification>();
			 Criteria cr=session.createCriteria(Optr_Notification.class);
			 cr.add(Restrictions.eq(Optr_NotificationCNTS.OPN_DISCODE,dispatchcode));
			 list =cr.list();
			 optr_Notification = list.get(0);
			 optr_Notification.setIsRecieved("yes");
			 session.update(optr_Notification);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }		
		 session.clear();
		 session.close();
		 return temp;
	 }
}
