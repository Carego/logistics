package com.mylogistics.constants;

public class LhpvAdvCNTS {

	public static final String LHPV_ADV_BRKOWN = "laBrkOwn";
	
	public static final String LHPV_ADV_LAID = "laId";
	
	public static final String LHPV_ADV_DT = "laDt";
	
	public static final String LHPV_ADV_BCODE = "bCode";
	
	public static final String LHPV_ADV_ID = "laId";
	
	public static final String LHPV_ADV_FINAL_TOT = "laFinalTot";
	
	public static final String LHPV_ADV = "laLryAdvP";
	
	public static final String LHPV_ADV_PAY_BY = "laPayMethod";
	
	public static final String LHPV_ADV_CASH_DISC = "laCashDiscR";
	
	public static final String LHPV_ADV_MUNS = "laMunsR";
	
	public static final String LHPV_ADV_TDS = "laTdsR";
	
	public static final String LHPV_ADV_CHQ_NO = "laChqNo";
	
	public static final String LHPV_ADV_TOT_RCVR = "laTotRcvrAmt";
	
	public static final String LHPV_ADV_IS_CLOSE = "laIsClose";
	
	public static final String LHPV_ADV_BANK_CODE = "laBankCode";
	
	public static final String USER_BRANCH_CODE = "bCode";	
	
	public static final String LHPV_ADV_TOTAL_PAY_AMT = "laTotPayAmt";
	
	public static final String LHPV_ADV_CHALLAN = "challan";
	
}
