package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hosstats")
public class HOSStats {
	
	@Id
	@Column(name="hOSStatsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int hOSStatsId;
	
	@Column(name="hOSStatsBranchCode")
	private String hOSStatsBranchCode;
	
	@Column(name="hOSStatsCnmtOdr")
	private int hOSStatsCnmtOdr;
	
	@Column(name="hOSStatsCnmtPer")
	private double hOSStatsCnmtPer;
	
	@Column(name="hOSStatsCnmtAvgPer")
	private double hOSStatsCnmtAvgPer;
	
	@Column(name="hOSStatsCnmtExptOdr")
	private int hOSStatsCnmtExptOdr;
	
	@Column(name="hOSStatsChlnOdr")
	private int hOSStatsChlnOdr;
	
	@Column(name="hOSStatsChlnPer")
	private double hOSStatsChlnPer;
	
	@Column(name="hOSStatsChlnAvgPer")
	private double hOSStatsChlnAvgPer;
	
	@Column(name="hOSStatsChlnExptPer")
	private int hOSStatsChlnExptPer;
	
	@Column(name="hOSStatsSedrOdr")
	private int hOSStatsSedrOdr;
	
	@Column(name="hOSStatsSedrPer")
	private double hOSStatsSedrPer;
	
	@Column(name="hOSStatsAvgPer")
	private double hOSStatsAvgPer;
	
	@Column(name="hOSStatsExptOdr")
	private int hOSStatsExptOdr;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public int gethOSStatsId() {
		return hOSStatsId;
	}

	public void sethOSStatsId(int hOSStatsId) {
		this.hOSStatsId = hOSStatsId;
	}

	public String gethOSStatsBranchCode() {
		return hOSStatsBranchCode;
	}

	public void sethOSStatsBranchCode(String hOSStatsBranchCode) {
		this.hOSStatsBranchCode = hOSStatsBranchCode;
	}

	public int gethOSStatsCnmtOdr() {
		return hOSStatsCnmtOdr;
	}

	public void sethOSStatsCnmtOdr(int hOSStatsCnmtOdr) {
		this.hOSStatsCnmtOdr = hOSStatsCnmtOdr;
	}

	public double gethOSStatsCnmtPer() {
		return hOSStatsCnmtPer;
	}

	public void sethOSStatsCnmtPer(double hOSStatsCnmtPer) {
		this.hOSStatsCnmtPer = hOSStatsCnmtPer;
	}

	public double gethOSStatsCnmtAvgPer() {
		return hOSStatsCnmtAvgPer;
	}

	public void sethOSStatsCnmtAvgPer(double hOSStatsCnmtAvgPer) {
		this.hOSStatsCnmtAvgPer = hOSStatsCnmtAvgPer;
	}

	public int gethOSStatsCnmtExptOdr() {
		return hOSStatsCnmtExptOdr;
	}

	public void sethOSStatsCnmtExptOdr(int hOSStatsCnmtExptOdr) {
		this.hOSStatsCnmtExptOdr = hOSStatsCnmtExptOdr;
	}

	public int gethOSStatsChlnOdr() {
		return hOSStatsChlnOdr;
	}

	public void sethOSStatsChlnOdr(int hOSStatsChlnOdr) {
		this.hOSStatsChlnOdr = hOSStatsChlnOdr;
	}

	public double gethOSStatsChlnPer() {
		return hOSStatsChlnPer;
	}

	public void sethOSStatsChlnPer(double hOSStatsChlnPer) {
		this.hOSStatsChlnPer = hOSStatsChlnPer;
	}

	public double gethOSStatsChlnAvgPer() {
		return hOSStatsChlnAvgPer;
	}

	public void sethOSStatsChlnAvgPer(double hOSStatsChlnAvgPer) {
		this.hOSStatsChlnAvgPer = hOSStatsChlnAvgPer;
	}

	public int gethOSStatsChlnExptPer() {
		return hOSStatsChlnExptPer;
	}

	public void sethOSStatsChlnExptPer(int hOSStatsChlnExptPer) {
		this.hOSStatsChlnExptPer = hOSStatsChlnExptPer;
	}

	public int gethOSStatsSedrOdr() {
		return hOSStatsSedrOdr;
	}

	public void sethOSStatsSedrOdr(int hOSStatsSedrOdr) {
		this.hOSStatsSedrOdr = hOSStatsSedrOdr;
	}

	public double gethOSStatsSedrPer() {
		return hOSStatsSedrPer;
	}

	public void sethOSStatsSedrPer(double hOSStatsSedrPer) {
		this.hOSStatsSedrPer = hOSStatsSedrPer;
	}

	public double gethOSStatsAvgPer() {
		return hOSStatsAvgPer;
	}

	public void sethOSStatsAvgPer(double hOSStatsAvgPer) {
		this.hOSStatsAvgPer = hOSStatsAvgPer;
	}

	public int gethOSStatsExptOdr() {
		return hOSStatsExptOdr;
	}

	public void sethOSStatsExptOdr(int hOSStatsExptOdr) {
		this.hOSStatsExptOdr = hOSStatsExptOdr;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
	
}
