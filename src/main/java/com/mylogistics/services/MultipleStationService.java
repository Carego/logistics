package com.mylogistics.services;

import java.util.List;
import com.mylogistics.model.MultipleStation;

public interface MultipleStationService {
	
	public List<MultipleStation> getAllStation();

    public void addStation(MultipleStation multipleStation);
    
    public void deleteStation(MultipleStation multipleStation);

    public void deleteAllStation();

}
