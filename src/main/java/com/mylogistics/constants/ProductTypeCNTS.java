package com.mylogistics.constants;

public class ProductTypeCNTS {

	public static final String PT_ID = "ptId";
	
	public static final String PT_NAME = "ptName";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
}
