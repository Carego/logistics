 <div ng-show="operatorLogin || superAdminLogin"> 
<title>CNMT COSTING</title>
	
	<div class="row" >
	<form name="cnmtCostForm" ng-submit="costing()" class="col s12 card" style="align: center; padding-top: 40px; background-color: #F2FAEF;"
		ng-class = "{'form-error': cnmtCostForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
	
	<div class="row">
		    
		    	<div class="col s1 input-field">
					<input class="validate" type="radio" id="consolidateRBId" name="isRBName" ng-click="consolidateRB()" > 
					<label for="code" style="color:black;">Consolidate</label>
				</div>
				
				<div class="col s1 input-field">
					<input class="validate" type="radio" id="branchRBId" name="isRBName" ng-click="branchRB()"> 
					<label for="code" style="color:black;">Branch</label>
				</div>
				
				<div class="col s2 input-field">
	       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" disabled="disabled" readonly="readonly">
	       			<label for="code" style="color:black;">Branch</label>	
	       		</div>
	       		
	       		<div class="col s2 input-field">
	       				<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="custCode" ng-click="openCustDB()"  readonly="readonly">
	       			<label for="code" style="color:black;">Cust. Code</label>	
	       		</div>
	       		
	       		<div class="input-field col s3">
					<input type="date" name="frmDt" ng-model="frmDt" ng-required=true>
					<label style="color:black;">From Date</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtCostForm.frmDt.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  From date is required.</span>
							</div>
				</div>
				
				<div class="input-field col s3">
					<input type="date" name="toDt" ng-model="toDt" ng-required=true>
					<label style="color:black;">To Date</label>
					<div class = "text-left errorMargin" ng-messages = "cnmtCostForm.toDt.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  To date is required.</span>
							</div>
				</div>
		    
		    </div>
		    
		    <div class = "row">
		    	<div class="input-field col s3">
			 	<input class="validate" type ="text" name ="fromStationName" ng-model="frmStationCode" ng-keyup="getStationFrom($event.keyCode)">
					<label style="color:black;">From Station</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" type ="text" name ="toStationName" ng-model="toStationCode" ng-keyup="getStationTo($event.keyCode)">
				<label style="color:black;">To Station</label>
			</div>
		    </div>	
	
	 <div class="row">
				<div class="col s12 center"> 
					<input class="btn" type="button" id="Excel" value="Print XLS" ng-click = "sendABC(cnmtCostForm)">
					<input class="btn" type="submit" id="submitId" value="Generate Cost">
				</div>
			 </div>
	</form>
	
	</div>
	 	 <div class="row" >
	 
		  	<div class="col s12 center">
				<form method="post" id="ABC" name="ABCForm"  enctype="multipart/form-data" ng-submit="sendABC(ABCForm)">
					<div class="row">
		    
		    	<div class="col s3 input-field" ng-hide="true">
					<input class="validate" type="text" id="consolidateRId" name="isRBName" ng-model = "isConBrh"  readonly ng-required="true"> 
					<label for="code">Consolidate/Branch</label>
				</div>
				
				<div class="col s3 input-field" ng-hide="true">
	       				<input class="validate" type ="text" id="branchCodeId" name ="branchName" ng-model="branch.branchCode"  readonly="readonly">
	       			<label for="code">Branch</label>	
	       		</div>
	       		
	       		<div class="col s3 input-field" ng-hide="true">
	       				<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="custCode"   readonly="readonly">
	       			<input class="validate" type ="text" id="customerNameId" name ="customerName" ng-model="customerName"   readonly="readonly">
	       			<label for="code">Customer Code</label>	
	       		</div>
		    
		    </div>	
	
	<div class="row">
				
		<div class="input-field col s4" ng-hide="true">
					<input type="date" name="frmDt" ng-model="frmDt" ng-required=true>
					<label>From Date</label>
		</div>
		<div class="input-field col s4" ng-hide="true">
					<input type="date" name="toDt" ng-model="toDt" ng-required=true>
					<label>To Date</label>
		</div>
		
		    	<div class="input-field col s3" ng-hide="true">
			 	<input class="validate" type ="text" id = "printFromStation" name ="fromStationName" ng-model="frmStationCode">
					<label>From Station</label>
			</div>
			
			<div class="input-field col s3" ng-hide="true">
				<input class="validate" type ="text" id = "printToStation" name ="toStationName" ng-model="toStationCode">
				<label style="color:black;">To Station</label>
			</div>
			
			<div class="input-field col s3" ng-hide="true">
				<input type="text" name="contractCode" id="contractCode1"
					ng-model="contractCodeTemp" disabled="disabled"
					ng-click="OpenContractCodeDB()" readonly ng-required="true"> <label>Contract Code</label>
			</div>
		
	</div>
			  	</form>
			</div>
		  </div>
	
	
	<div id ="brhOpenDB" ng-hide="brhOpenFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Id</th>
 	  	  	  <th>Bank Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in brhList | filter:filterTextbox">
		 	  <td><input type="radio"  name="brhName"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBrhCode(brh)"></td>
              <td>{{brh.branchId}}</td>
              <td>{{brh.branchName}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="custOpenDB" ng-hide="custOpenFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Code</th>
 	  	  	  <th>Customer Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in customerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="custName"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustCode(cust)"></td>
              <td>{{cust.custCode}}</td>
              <td>{{cust.custName}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="cnmtFromStationDB" ng-hide="CnmtFromStationDBFlag">
	<input type="text" name="filterCnmtFromStation"
		ng-model="filterCnmtFromStation" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<!-- <th>Station Code</th> -->

			<th>Station Name</th>

			<th>Station District</th>
		</tr>


		<tr ng-repeat="station in stationList | filter:filterCnmtFromStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station }}" ng-model="frmStationCode"
				ng-click="saveFrmStnCode(station)"></td>
			<!-- <td>{{ station.stnCode }}</td> -->
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>

<div id="cnmtToStationDB" ng-hide="CnmtToStationDBFlag">
	<input type="text" name="filterCnmtToStation"
		ng-model="filterCnmtToStation" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<!-- <th>Station Code</th> -->

			<th>Station Name</th>

			<th>Station District</th>
		</tr>
		<tr ng-repeat="station in stationList | filter:filterCnmtToStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station}}" ng-model="toStationCode"
				ng-click="saveToStnCode(station)"></td>
			<!-- <td>{{ station.stnCode }}</td> -->
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>

 </div> 