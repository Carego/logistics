package com.mylogistics.model.bank;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mylogistics.model.Branch;

@Entity
@Table(name = "chequeleaves")
public class ChequeLeaves {
	
	@Id
	@Column(name="chqLId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int chqLId;
	
	//Many To One with BankMstr
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "bnkId") 
	private BankMstr bankMstr;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "branchId")
	private Branch branch;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "chqBkId")
	private ChequeBook chequeBook;
	
	@Column(name="chqLChqNo")
	private String chqLChqNo;
	
	@Column(name="chqLChqType")
	private char chqLChqType; //AccountPay/Bearer/Cancel
	
	@Column(name="chqLCType")
	private char chqLCType; //manual/compute rised
	
	@Column(name="chqLChqAmt")
	private double chqLChqAmt;
	
	@Column(name="chqLChqMaxLt")
	private double chqLChqMaxLt;
	
	@Column(name="chqLIssueDt")
	private Date chqLIssueDt; //cheque date mentioned at the corner of the cheque
	
	@Column(name="chqLIssue")
	private boolean chqLIssue;
	
	@Column(name="chqLUsedDt")
	private Date chqLUsedDt; //current date or system date which we may ignore due to time stamp
	
	@Column(name="chqLUsed")
	private boolean chqLUsed;
	
	@Column(name="chqLChqCancelDt")
	private Date chqLChqCancelDt;
	
	@Column(name="chqLCancel")
	private boolean chqLCancel;
	
	/*//OneToOne with CashStmt
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(mappedBy="cLeaves")
	private CashStmt cashStmt;*/
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public ChequeBook getChequeBook() {
		return chequeBook;
	}

	public void setChequeBook(ChequeBook chequeBook) {
		this.chequeBook = chequeBook;
	}

	public int getChqLId() {
		return chqLId;
	}

	public char getChqLCType() {
		return chqLCType;
	}

	public void setChqLCType(char chqLCType) {
		this.chqLCType = chqLCType;
	}

	public void setChqLId(int chqLId) {
		this.chqLId = chqLId;
	}

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}

	public String getChqLChqNo() {
		return chqLChqNo;
	}

	public void setChqLChqNo(String chqLChqNo) {
		this.chqLChqNo = chqLChqNo;
	}

	public char getChqLChqType() {
		return chqLChqType;
	}

	public void setChqLChqType(char chqLChqType) {
		this.chqLChqType = chqLChqType;
	}

	public double getChqLChqAmt() {
		return chqLChqAmt;
	}

	public void setChqLChqAmt(double chqLChqAmt) {
		this.chqLChqAmt = chqLChqAmt;
	}

	public double getChqLChqMaxLt() {
		return chqLChqMaxLt;
	}

	public void setChqLChqMaxLt(double chqLChqMaxLt) {
		this.chqLChqMaxLt = chqLChqMaxLt;
	}

	public Date getChqLIssueDt() {
		return chqLIssueDt;
	}

	public void setChqLIssueDt(Date chqLIssueDt) {
		this.chqLIssueDt = chqLIssueDt;
	}

	public Date getChqLUsedDt() {
		return chqLUsedDt;
	}

	public void setChqLUsedDt(Date chqLUsedDt) {
		this.chqLUsedDt = chqLUsedDt;
	}

	/*public CashStmt getCashStmt() {
		return cashStmt;
	}

	public void setCashStmt(CashStmt cashStmt) {
		this.cashStmt = cashStmt;
	}*/

	public Date getChqLChqCancelDt() {
		return chqLChqCancelDt;
	}

	public void setChqLChqCancelDt(Date chqLChqCancelDt) {
		this.chqLChqCancelDt = chqLChqCancelDt;
	}

	public boolean isChqLUsed() {
		return chqLUsed;
	}

	public void setChqLUsed(boolean chqLUsed) {
		this.chqLUsed = chqLUsed;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public boolean isChqLIssue() {
		return chqLIssue;
	}

	public void setChqLIssue(boolean chqLIssue) {
		this.chqLIssue = chqLIssue;
	}

	public boolean isChqLCancel() {
		return chqLCancel;
	}

	public void setChqLCancel(boolean chqLCancel) {
		this.chqLCancel = chqLCancel;
	}
	
	
}
