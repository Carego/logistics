package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name ="customer")

public class Customer {
	

	@Id
	@Column(name="custId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int custId;
	 
	@Column(name="custCode")
	private String custCode;
	
	@Column(name="custName")
	private String custName;
	
	@Column(name="custAdd")
	private String custAdd;
	
	@Column(name="custCity")
	private String custCity;
	
	@Column(name="custState")
	private String custState;
	
	@Column(name="custPin")
	private String custPin;
	
	@Column(name="custTdsCircle")
	private String custTdsCircle;
	
	@Column(name="custTdsCity")
	private String custTdsCity;
	
	@Column(name="custTinNo")
	private String custTinNo;
	
	@Column(name="custStatus")
	private String custStatus;
	
	@Column(name="custPanNo")
	private String custPanNo;
	
	@Column(name="custIsDailyContAllow")
	private String custIsDailyContAllow;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="custBillFrDt")
	private Date custBillFrDt;
	
	@Column(name="custBillToDt")
	private Date custBillToDt;
	
	@Column(name="custBillPer" , columnDefinition="boolean default false")
	private boolean custBillPer;
	
	@Column(name="custEditBillAmt",columnDefinition="boolean default true")
    private boolean custEditBillAmt;
	
	@Column(name="isVerify")
	private String isVerify;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="custDirector")
	private String custDirector;
	
	@Column(name="custMktHead")
	private String custMktHead;
	
	@Column(name="custCorpExc")
	private String custCorpExc;
	
	@Column(name="custBrPerson")
	private String custBrPerson;
	
	@Column(name="custCrBase")
	private String custCrBase;
	
	@Column(name="custCrPeriod")
	private int custCrPeriod;
	
	@Column(name="custSrvTaxBy")
	private char custSrvTaxBy;
	
	@Column(name="custBillCycle")
	private char custBillCycle;
	
	@Column(name="custBillValue")
	private int custBillValue;
	
	@Column(name="custCodeTemp")
	private String custCodeTemp;
	
	@ColumnDefault(value="'no'")
	@Column(name="custGroupId")
	private String custGroupId;
	
	@Column(name="custFaCode")
	private String custFaCode;
	
	@Column(name="stateGST")
	private String stateGST;
	
	@Column(name="custGstNo")
	private String custGstNo;
	
	@Column(name="custSAC")
	private String custSAC;
	
	@JsonBackReference(value = "custBrh")
	@LazyCollection(LazyCollectionOption.TRUE)
	@ManyToMany(cascade = {CascadeType.ALL} , mappedBy="customers")
	private Set<Branch> branches = new HashSet<Branch>();
	
	
	/*@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(cascade = {CascadeType.ALL} , mappedBy="customers")
    private Set<Branch> branches = new HashSet<Branch>();*/
	
	//One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "fbpCust")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
      @JoinTable(name="customer_fabprom",
      		joinColumns=@JoinColumn(name="custId"),
              inverseJoinColumns=@JoinColumn(name="fbpId"))
  	private List<FaBProm> fbpList = new ArrayList<FaBProm>();
  	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;
	
	public List<FaBProm> getFbpList() {
		return fbpList;
	}

	public void setFbpList(List<FaBProm> fbpList) {
		this.fbpList = fbpList;
	}

	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}
	
	public int getCustBillValue() {
		return custBillValue;
	}

	public String getCustFaCode() {
		return custFaCode;
	}

	public void setCustFaCode(String custFaCode) {
		this.custFaCode = custFaCode;
	}

	public Set<Branch> getBranches() {
		return branches;
	}

	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}

	public void setCustBillValue(int custBillValue) {
		this.custBillValue = custBillValue;
	}

	public String getCustDirector() {
		return custDirector;
	}

	public void setCustDirector(String custDirector) {
		this.custDirector = custDirector;
	}

	public String getCustMktHead() {
		return custMktHead;
	}

	public void setCustMktHead(String custMktHead) {
		this.custMktHead = custMktHead;
	}

	public String getCustCorpExc() {
		return custCorpExc;
	}

	public void setCustCorpExc(String custCorpExc) {
		this.custCorpExc = custCorpExc;
	}

	public String getCustBrPerson() {
		return custBrPerson;
	}

	public void setCustBrPerson(String custBrPerson) {
		this.custBrPerson = custBrPerson;
	}

	public String getCustCrBase() {
		return custCrBase;
	}

	public void setCustCrBase(String custCrBase) {
		this.custCrBase = custCrBase;
	}

	public int getCustCrPeriod() {
		return custCrPeriod;
	}

	public void setCustCrPeriod(int custCrPeriod) {
		this.custCrPeriod = custCrPeriod;
	}

	public char getCustSrvTaxBy() {
		return custSrvTaxBy;
	}

	public void setCustSrvTaxBy(char custSrvTaxBy) {
		this.custSrvTaxBy = custSrvTaxBy;
	}

	public char getCustBillCycle() {
		return custBillCycle;
	}

	public void setCustBillCycle(char custBillCycle) {
		this.custBillCycle = custBillCycle;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}
	
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	public String getIsVerify() {
		return isVerify;
	}

	public void setIsVerify(String isVerify) {
		this.isVerify = isVerify;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAdd() {
		return custAdd;
	}

	public void setCustAdd(String custAdd) {
		this.custAdd = custAdd;
	}

	public String getCustCity() {
		return custCity;
	}

	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}

	public String getCustState() {
		return custState;
	}

	public void setCustState(String custState) {
		this.custState = custState;
	}

	public String getCustPin() {
		return custPin;
	}

	public void setCustPin(String custPin) {
		this.custPin = custPin;
	}

	public String getCustTdsCircle() {
		return custTdsCircle;
	}

	public void setCustTdsCircle(String custTdsCircle) {
		this.custTdsCircle = custTdsCircle;
	}

	public String getCustTdsCity() {
		return custTdsCity;
	}

	public void setCustTdsCity(String custTdsCity) {
		this.custTdsCity = custTdsCity;
	}

	public String getCustTinNo() {
		return custTinNo;
	}

	public void setCustTinNo(String custTinNo) {
		this.custTinNo = custTinNo;
	}

	public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public String getCustPanNo() {
		return custPanNo;
	}

	public void setCustPanNo(String custPanNo) {
		this.custPanNo = custPanNo;
	}

	public String getCustIsDailyContAllow() {
		return custIsDailyContAllow;
	}

	public void setCustIsDailyContAllow(String custIsDailyContAllow) {
		this.custIsDailyContAllow = custIsDailyContAllow;
	}
	
	public String getCustCodeTemp() {
		return custCodeTemp;
	}

	public void setCustCodeTemp(String custCodeTemp) {
		this.custCodeTemp = custCodeTemp;
	}

	public Date getCustBillFrDt() {
		return custBillFrDt;
	}

	public void setCustBillFrDt(Date custBillFrDt) {
		this.custBillFrDt = custBillFrDt;
	}

	public Date getCustBillToDt() {
		return custBillToDt;
	}

	public void setCustBillToDt(Date custBillToDt) {
		this.custBillToDt = custBillToDt;
	}

	public boolean isCustBillPer() {
		return custBillPer;
	}

	public void setCustBillPer(boolean custBillPer) {
		this.custBillPer = custBillPer;
	}

	public boolean isCustEditBillAmt() {
		return custEditBillAmt;
	}

	public void setCustEditBillAmt(boolean custEditBillAmt) {
		this.custEditBillAmt = custEditBillAmt;
	}

	public String getCustGroupId() {
		return custGroupId;
	}

	public void setCustGroupId(String custGroupId) {
		this.custGroupId = custGroupId;
	}

	public String getStateGST() {
		return stateGST;
	}

	public void setStateGST(String stateGST) {
		this.stateGST = stateGST;
	}

	public String getCustGstNo() {
		return custGstNo;
	}

	public void setCustGstNo(String custGstNo) {
		this.custGstNo = custGstNo;
	}
	
	public void setCustSAC(String custSAC) {
		this.custSAC = custSAC;
	}
	public String getCustSAC() {
		return custSAC;
	}
	
}
