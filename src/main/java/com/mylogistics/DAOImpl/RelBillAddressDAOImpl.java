package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transaction;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.RelBillAddressDAO;
import com.mylogistics.model.AdaniGST;
import com.mylogistics.model.HilAddress;
import com.mylogistics.model.RelBillAddress;


public class RelBillAddressDAOImpl implements RelBillAddressDAO{

	@Autowired
	private SessionFactory sessionFactory;
	//private Session session;
	//private Transaction transaction;
	
	
	public RelBillAddressDAOImpl(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	Logger logger = Logger.getLogger(RelBillAddressDAOImpl.class);
	
	@Override
	public List<RelBillAddress> getAllRelBlAdd(){
		List<RelBillAddress> list=new ArrayList<>();
		Session session;
		session=this.sessionFactory.openSession();
		try{
			list=(List<RelBillAddress>) session.createCriteria(RelBillAddress.class).list();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.clear();
			session.close();
		}
		
		return list;
	}
	
	
	public Object getRelBlAddByGst(String gstNo, Session session){
		
		RelBillAddress relBillAddress = new RelBillAddress();
		
		Criteria criteria=session.createCriteria(RelBillAddress.class)
		.add(Restrictions.eq("", gstNo));
		
		if(!criteria.list().isEmpty()){
			relBillAddress=(RelBillAddress) criteria.list().get(0);
		}
		
		return relBillAddress;
	}
	
	@Override
	public List<AdaniGST> getAllAdaniGst(){
		List<AdaniGST> list=new ArrayList<>();
		Session session;
		session=this.sessionFactory.openSession();
		try{
			list=(List<AdaniGST>) session.createCriteria(AdaniGST.class).list();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.clear();
			session.close();
		}
		
		return list;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HilAddress> getAllHilAddress(){
		List<HilAddress> list=new ArrayList<>();
		Session session;
		session=this.sessionFactory.openSession();
		try{
			list=(List<HilAddress>) session.createCriteria(HilAddress.class).list();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.clear();
			session.close();
		}
		
		return list;
	}
	
	
}
