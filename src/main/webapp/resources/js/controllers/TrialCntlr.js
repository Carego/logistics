'use strict';

var app = angular.module('application');

app.controller('TrialCntlr',['$scope','$location','$http','$filter','$window','$log',  
                                 function($scope,$location,$http,$filter,$window,$log){

	console.log("TrialCntlr Started");
	
	$scope.finYearList = [];
	$scope.branchList = [];
	
	$scope.faCodeNameList = [];
	$scope.trialReportMap = {};
	
	$scope.branch = {};
	
	$scope.isConsolidate = false;
	$scope.isBranch = false;
	$scope.showTableFlag = false;
	
	$scope.finYrDBFlag = true;
	$scope.branchDBFlag = true;
	
	$scope.getFinYears = function() {
		var response = $http.post($scope.projectName+'/getFinYears');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.finYearList = data.finYearList;
				$scope.getBranchNameIdFa();
				for ( var i = 0; i < $scope.finYearList.length; i++) {
					console.log("FinYears: "+$scope.finYearList[i]);
				}
			}else {
				console.log("getBankMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openFinYrDB = function(){
		console.log("Entered into openFinYrDB");
		$scope.finYrDBFlag = false;
		$('div#finYrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Financial Year",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.finYrDBFlag = true;
			}
		});

		$('div#finYrDB').dialog('open');
	}
	
	$scope.saveFinYr =  function(finYr){
		$scope.finYr = finYr;
		$('div#finYrDB').dialog('close');
	}
	
	$scope.consolidateRB = function() {
		console.log("consolidateRB()");
		$scope.branch = {};
		$scope.isConsolidate = true;
		$scope.isBranch = false;
		$('#branchNameId').attr("disabled","disabled");
		
	}
	
	$scope.branchRB = function() {
		console.log("branchRB()");
		$scope.isConsolidate = false;
		$scope.isBranch = true;
		$('#branchNameId').removeAttr("disabled");
	}
	
	$scope.getBranchNameIdFa = function() {
		console.log("getBranchNameIdFa");
		
		var response = $http.post($scope.projectName+'/getBranchNameIdFa');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.branchList = data.branchList;
			}else {
				console.log("getBankMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
		
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.trialSubmit = function(trialForm) {
		console.log("trialSubmit()");
		if (trialForm.$invalid) {
			if (trialForm.finYrName.$invalid) {
				$scope.alertToast("Please Enter Financial year");
			} else if (trialForm.uptoDtName.$invalid) {
				$scope.alertToast("Please Enter Upto Date");
			}  
		} else {
			if ($scope.isBranch) {
				if (angular.isUndefined($scope.branch.branchName) || $scope.branch.branchName === null || $scope.branch.branchName == "") {
					$scope.alertToast("Please enter Branch");
				}else {
					$scope.getTrialReport();
				}
			}else if ($scope.isConsolidate){
				$scope.getTrialReport();
			}else {
				$scope.alertToast("Please select either Branch or Consolidate");
			}
		}
	}
	
	$scope.getTrialReport = function() {
		console.log("getTrialReport()");
		var trialReport = {
				"finYr"			: $scope.finYr,
				"uptoDt"		: $scope.uptoDt,
				"branchId"		: $scope.branch.branchId,
				"branchName"	: $scope.branch.branchName,
				"isBranch"		: $scope.isBranch,
				"isConsolidate"	:$scope.isConsolidate
		}
		
		$('#trialSubmitId').attr("disabled","disabled");
		
		var response = $http.post($scope.projectName+'/getTrialReport', trialReport);
		response.success(function(data, status, headers, config){
			$('#trialSubmitId').removeAttr("disabled");
			
			$log.info(data);
			console.log(JSON.stringify(data));
			
			if (data.result === "success") {
				
				$scope.faCodeNameList = data.faCodeNameList;
				//$scope.trialReportMap = data.trialReportMap;
				$scope.cashInClosing = data.cashInClosing;
				
				$scope.alertToast(data.result);
				$scope.showTableFlag = true;
				$('#printTrialId').removeAttr("disabled");
				$('#printTrialXlsId').removeAttr("disabled");
				
				//calculate total debit and credit
				$scope.totalDr = 0;
				$scope.totalCr = 0;
				for ( var i = 0; i < $scope.faCodeNameList.length; i++) {
					if ($scope.faCodeNameList[i].csDrCrBalance === "D") {
						//console.log("D: "+$scope.faCodeNameList[i].csAmtBalance);
						$scope.totalDr = $scope.totalDr + parseFloat($scope.faCodeNameList[i].csAmtBalance);
						console.log(i+" $scope.totalDr "+$scope.totalDr)
					} else if($scope.faCodeNameList[i].csDrCrBalance === "C") {
						//console.log("C: "+$scope.faCodeNameList[i].csAmtBalance);
						$scope.totalCr = $scope.totalCr + parseFloat($scope.faCodeNameList[i].csAmtBalance);
						console.log(i+" $scope.totalCr "+$scope.totalCr)
					}
				}
				$scope.totalDr = $scope.totalDr + $scope.cashInClosing;
				
			}else {
				$scope.alertToast(data.result);
				$scope.showTableFlag = false;
				$('#printLedgerId').attr("disabled","disabled");
				$('#printLedgerXlsId').attr("disabled","disabled");
				
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getLedgerReport Error: "+data);
			$('#trialSubmitId').removeAttr("disabled");
			$scope.alertToast("databse exception");
		});
	}
	
	//print pdf
	$scope.printTrialPdf = function() {
		console.log("printTrialPdf()");
		$window.location.href = $scope.projectName+'/printTrialReportPdf';
	}
	
	//print xls
	$scope.printTrialXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "trial.xls");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getFinYears();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("TrialCntlr Ended");
}]);