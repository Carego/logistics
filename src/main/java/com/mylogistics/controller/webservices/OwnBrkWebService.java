package com.mylogistics.controller.webservices;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.werservice.OwnBrkDao;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.OwnBrkApp;
import com.mylogistics.services.ConstantsValues;

@RestController
public class OwnBrkWebService {

	@Autowired
	private OwnBrkDao ownBrkDao;
	@Autowired
	private UserDAO userDAO;
	
	public static Logger logger = Logger.getLogger(OwnBrkWebService.class);
	
	
	@RequestMapping(value = "/saveOwnBrkWs", method = RequestMethod.POST)
	public @ResponseBody Object saveOwnBrk(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into /saveOwnBrk()...");
		try{			
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");			
				String ownBrkJSON = null;
				if(initParam.get("ownBrk") == null){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Owner Details Not Found !");
					logger.info("Owner Details Not Found !");
				}else{
					ownBrkJSON = String.valueOf(initParam.get("ownBrk"));				
					ObjectMapper objectMapper = new ObjectMapper();										
					OwnBrkApp ownBrkApp = objectMapper.readValue(ownBrkJSON, OwnBrkApp.class);
					ownBrkApp.setOwnBrkCode("");
					//ownBrkApp.setBranchCode(user.getUserBranchCode());
					ownBrkApp.setUserCode(user.getUserCode());
					resultMap = ownBrkDao.saveOwnBrk(ownBrkApp);
				}			
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is goind wrong !");
			logger.error("Error in saveOwnBrk()... : "+e);
		}
		logger.info("Exit From /saveOwnBrk()...");
		return resultMap;
	}
	
	public Map getUserByUN(String userName, String password){
		System.out.println("getUserByUN()......");
		List<User> userList = new ArrayList<User>();
		Map<String, Object> map = new HashMap<>();
		try{
			//convert pass to md5
			String pass = null;
	        try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
			    byte[] digest = md.digest();
			    StringBuffer sb = new StringBuffer();
			    for (byte b : digest) {
		            sb.append(String.format("%02x", b & 0xff));
		        }
			    pass = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        userList = userDAO.getUserByUN(userName, password);
	        if (!userList.isEmpty()) {
					User user = userList.get(0);					
				if (user.getPassword().equalsIgnoreCase(pass)) {				
					map.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					map.put("msg", "valid password");
					map.put("user", user);
				} else {
					map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					map.put("msg", "invalid password");
				}
			} else {
				map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				map.put("msg", "user not exist");
			}
		}catch(Exception e){
			System.out.println("Error in getUserByUN - WebService : "+e);
		}
		return map;
	}
	
}