'use strict';

var app = angular.module('application');

app.controller('BillFrwdCntlr',['$scope','$location','$http','$filter','$sce',
                              function($scope,$location,$http,$filter,$sce){
	
	$scope.custList = [];
	$scope.customer = {};
	$scope.blList = [];
	$scope.bill = {};
	$scope.billFrwd = {};
	$scope.billFrwd.billList = [];
	
	$scope.selCustFlag = true;
	$scope.selBillFlag = true;
	$scope.displayBfNoFlag = true;
	
	$scope.totAmt = 0;
	
	
	 $scope.getCustomer = function(){
		 console.log("enter into getCustomer function");
		 var response = $http.post($scope.projectName+'/getCustFrBillF');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.custList = data.list;
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	 }
	 
	 
	 $scope.selectCust = function(){
			console.log("enter into selectCust function");
			$scope.selCustFlag = false;
			$('div#selCustId').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Select Customer",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selCustFlag = true;
			    }
				});
			
			$('div#selCustId').dialog('open');	
	}
	
	 
	$scope.saveCustomer = function(cust){
			console.log("enter into saveCustomer function");
			$('div#selCustId').dialog('close');	
			$scope.customer = cust;
			$scope.blList = [];
			$scope.billFrwd.billList = [];
			$scope.bill = {};
			var req = {
					"custId" : $scope.customer.custId 	
			};
			
			var response = $http.post($scope.projectName+'/getBillFrBF',req);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					 $scope.blList = data.list;
					 console.log("size of blList = "+$scope.blList.length);
				  }else{
					 $scope.alertToast(data.msg)
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });	
	}
	
	
	$scope.selBillNo = function(){
		console.log("enter into selBillNo function");
		$scope.selBillFlag = false;
		$('div#selBillId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selBillFlag = true;
		    }
			});
		
		$('div#selBillId').dialog('open');	
	}
	
	
	$scope.saveBill = function(blng){
		$('div#selBillId').dialog('close');
		$scope.bill = blng;
		$scope.totAmt = 0;
		if($scope.billFrwd.billList.length > 0){
			var duplicate = false;
			for(var i=0;i<$scope.billFrwd.billList.length;i++){
				if($scope.billFrwd.billList[i].blBillNo === $scope.bill.blBillNo){
					duplicate = true;
					break;
				}
			}
			
			if(duplicate === true){
				$scope.alertToast("You already add "+$scope.bill.blBillNo+" into BillForwarding");
				for(var i=0;i<$scope.billFrwd.billList.length;i++){
					$scope.totAmt = $scope.totAmt + $scope.billFrwd.billList[i].blFinalTot;
				}
				
			}else{
				$scope.billFrwd.billList.push($scope.bill);
				
				for(var i=0;i<$scope.billFrwd.billList.length;i++){
					$scope.totAmt = $scope.totAmt + $scope.billFrwd.billList[i].blFinalTot;
				}
			}
		}else{
			$scope.billFrwd.billList.push($scope.bill);
			
			for(var i=0;i<$scope.billFrwd.billList.length;i++){
				$scope.totAmt = $scope.totAmt + $scope.billFrwd.billList[i].blFinalTot;
			}
		}
	}
	
	
	$scope.removeBill = function(index){
		console.log("enter into removeBill function = "+index);
		$scope.totAmt = 0;
		if($scope.billFrwd.billList.length > 0){
			$scope.billFrwd.billList.splice(index,1);
			
			for(var i=0;i<$scope.billFrwd.billList.length;i++){
				$scope.totAmt = $scope.totAmt + $scope.billFrwd.billList[i].blFinalTot;
			}
		}
	}
	
	
	$scope.billFrwdSubmit = function(billFrwdForm){
		console.log("enter into billFrwdSubmit function = "+billFrwdForm.$invalid);
		if(billFrwdForm.$invalid){
			$scope.alertToast("Please fill the correct form");
		}else{
			if($scope.billFrwd.billList.length > 0){
				$('#saveId').attr("disabled","disabled");
				$scope.billFrwd.bfCustId = $scope.customer.custId;
				$scope.billFrwd.bfTotAmt = $scope.totAmt;
				var response = $http.post($scope.projectName+'/saveBillFrwd',$scope.billFrwd);
				  response.success(function(data, status, headers, config){
					  if(data.result === "success"){
						  $('#saveId').removeAttr("disabled");
						  	$scope.bfNo = data.bfNo;
						  	
						  	$scope.displayBfNoFlag = false;
							$('div#disBfNoId').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								title: "Bill Forwarding No",
								show: UDShow,
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.displayBfNoFlag = true;
							        $scope.bfNo = "";
							    }
								});
							
							$('div#disBfNoId').dialog('open');	
						  	
						  	
						  	$scope.totAmt = 0;
						    $scope.customer = {};
							$scope.blList = [];
							$scope.billFrwd.billList = [];
							$scope.bill = {};
					  }else{
						  $scope.alertToast("SERVER ERROR");
						  $('#saveId').removeAttr("disabled");
					  }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
				   });	
			}else{
				$scope.alertToast("please select a bill for BillForwarding");
			}
		}
	}
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getCustomer();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);