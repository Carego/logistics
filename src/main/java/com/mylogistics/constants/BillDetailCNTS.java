package com.mylogistics.constants;

public class BillDetailCNTS {

	public static final String BD_ID = "bdId";
	
	public static final String BD_RATE = "bdRate";
	
	public static final String BD_FREIGHT = "bdFreight";
	
	public static final String BD_UNLOAD_AMT = "bdUnloadAmt";
	
	public static final String BD_DET_AMT = "bdDetAmt";
	
	public static final String BD_OTH_CHG_AMT = "bdOthChgAmt";
	
	public static final String BD_TOT_AMT = "bdTotAmt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String BD_LOAD_AMT = "bdLoadAmt";
	
	public static final String BD_BONUS_AMT = "bdBonusAmt";
	
	public static final String BD_CNMT_ID = "bdCnmtId";
	
}
