<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row">
		<form name="editCSForm" ng-submit=getCSSubmit(editCSForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s3 input-field">
					<input class="validate" type="text" id="branchId" name="branchName" ng-model="branch.branchName" ng-required="true" ng-keyup="openBranchDB()" ng-click="openBranchDB()" readonly="readonly">
					<label for="code">Branch</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="validate" type="date" id="cssDtId" name="cssDtName" ng-model="cssDt" ng-required="true">
					<label for="code">Cash Stmt Date</label>
				</div>

				<div class="col s3 input-field">
					<input class="validate" type="number" id="csVouchNoId" name="csVouchNoName" ng-model="csVouchNo" ng-required="true" ng-pattern="/^[0-9]+$/"> 
					<label for="code">Voucher No</label>
				</div>
				
				<!-- <div class="col s3 input-field">
					<input class="validate" type="text" id="csFaCodeId" name="csFaCodeName" ng-model="csFaCode" ng-required="true" ng-keyup="openFaMstrDB()" ng-click="openFaMstrDB()" readonly="readonly">
					<label for="code">FaCode</label>
				</div> -->
				
				<div class="col s3 input-field">
					<input class="col s12 btn teal white-text" type="submit" value="submit">
				</div>
				
			</div>
			
			<div class="row" ng-show="showCSTableFlag">
				
				<div class="col s2 input-field">
		       		<input class="validate" type ="text" id="csFaCodeId" name ="csFaCodeName" ng-model="cashStmt.csFaCode" readonly >
		       	<label for="code">FaCode</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="csDescriptionId" name ="csDescriptionName" ng-model="cashStmt.csDescription" readonly >
		       	<label for="code">Description</label>	
		       	</div>
				
				<div class="col s2 input-field">
					<input class="validate" type ="text" id="csAmtId" name ="csAmtName" ng-model="cashStmt.csAmt" readonly >
		       	<label for="code">Amount</label>
				</div>
				
				<div class="col s1 input-field">
					<input class="validate" type ="text" id="csDrCrId" name ="csDrCrName" ng-model="cashStmt.csDrCr" readonly >
		       	<label for="code">Dr/Cr</label>
				</div>
				
			</div>
			
			<div class="row" ng-show="showCSTableFlag">
				
				<div class="col s2 input-field">
		       		<input class="validate" type ="text" id="faCodeId" name ="faCodeName" ng-model="faCode" ng-click="openFaMstrDB(faCode)" readonly >
		       	<label for="code">FaCode</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="descriptionId" name ="descriptionName" ng-model="description" >
		       	<label for="code">Description</label>	
		       	</div>
				
				<div class="col s2 input-field">
					<input class="validate" type ="number" id="amtId" name ="amtName" ng-model="amt" >
		       	<label for="code">Amount</label>
				</div>
				
				<div class="col s1 input-field">
					<input class="validate" type ="text" id="drCrId" name ="drCrName" ng-model="drCr" readonly >
				<label for="code">Dr/Cr</label>
				</div>
				<div class="col s1 input-field">
					<a class="btn-floating suffix waves-effect teal" type="button" ng-click="editCS()" >
						<i class="mdi-content-add white-text"></i>
					</a>
				</div>
				
			</div>
		</form>
	</div>

	<div class="row" ng-if="csList.length>0">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">New Cash Statement Detail</caption> 
       		
            <tr class="rowclr">
	            <!-- <th class="colclr">SNo</th> -->
	            <th class="colclr">FaCode</th>
	            <th class="colclr">Description</th>
	            <th class="colclr">Amount</th>
	            <th class="colclr">Dr/Cr</th>
	            <th class="colclr">Action</th>
	        </tr>	
	         
	        <tr ng-repeat="cs in csList">
	            <!-- <td class="rowcel">{{$index+1}}</td> -->
	            <td class="rowcel">{{cs.csFaCode}}</td>
	            <td class="rowcel">{{cs.csDescription}}</td>
	            <td class="rowcel">{{cs.csAmt}}</td>
	            <td class="rowcel">{{cs.csDrCr}}</td>
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeCs(cs, $index)" >
						<i class="mdi-action-delete white-text"></i>
					</a>
				</td>
	        </tr>
	        <!-- mdi-content-clear -->
         
         	<tr>
	          	<td colspan="5">
		          	<div class="col s12 center">
		          		<input type="button" id="submitCsId" value="Submit" ng-click="submitCs()">
		          	</div>
	          	</td>
          	</tr>
	
    	</table>
          
	</div>
	
	<div id ="faMstrDB" ng-hide="faMstrDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> Name </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="faCodeName in faCodeNameList | filter:filterTextbox">
		 	  <td><input type="radio"  name="faCodeName" value="{{ faCodeName }}" ng-click="saveFaMstr(faCodeName)"></td>
              <td>{{faCodeName.faMfaCode}}</td>
              <td>{{faCodeName.faMfaName}}</td>
          </tr>
          
      </table> 
	</div>
	
	<div id ="faMstrForBnkDB" ng-hide="faMstrForBnkDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> Name </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="faCodeNameForBnk in faCodeNameListForBnk | filter:filterTextbox1">
		 	  <td><input type="radio"  name="faCodeNameForBnk" value="{{ faCodeNameForBnk }}" ng-click="saveFaMstrForBnk(faCodeNameForBnk)"></td>
              <td>{{faCodeNameForBnk.faMfaCode}}</td>
              <td>{{faCodeNameForBnk.faMfaName}}</td>
          </tr>
          
      </table> 
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchNCIList | filter:filterTextbox2">
		 	  <td><input type="radio"  name="faCodeName" value="{{ branch }}" ng-focus="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
          
          <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeBranchDB()">
	          	</div>
          	</td>
          </tr>
      	  
      </table> 
	</div>
	
	<div id ="cashStmtDB" ng-hide="cashStmtDBFlag">
		<input type="text" name="filterTextbox3" ng-model="filterTextbox3" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> Amount </th>
 	  	  	  <th> Dr/Cr </th>
 	  	  	  <th> VType </th>
 	  	  	  <th> Description </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cashStmt in cashStmtList | filter:filterTextbox3">
		 	  <td><input type="radio"  name="cashStmtName" value="{{ cashStmt }}" ng-click="saveCashStmt(cashStmt)"></td>
              <td>{{cashStmt.csFaCode}}</td>
              <td>{{cashStmt.csAmt}}</td>
              <td>{{cashStmt.csDrCr}}</td>
              <td>{{cashStmt.csVouchType}}</td>
              <td>{{cashStmt.csDescription}}</td>
          </tr>
      </table> 
	</div>

</div>
