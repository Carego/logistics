'use strict';

var app = angular.module('application');

app.controller('DisplayDailyContractCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.dlyContCodeFlag = true;
	$scope.availableTags = [];
	$scope.dlyCont=true;
	$scope.selection=[];
	$scope.showContractDetails = true;
	$scope.isViewNo=false;
	$scope.dlyContByModal=false;
	$scope.dlyContByAuto=false;
	$scope.CngrCodeFlag = true;
	$scope.CrNameFlag = true;
	$scope.FromStationFlag = true;
	$scope.ToStationFlag = true;
	$scope.branchCodeFlag = true;
	$scope.blpmCodeFlag = true;
	$scope.RbkmIdFlag = true;
	$scope.ProductTypeFlag = true;
	$scope.VehicleTypeFlag = true;
	$scope.BonusFlag = true;
	$scope.PenaltyFlag = true;
	$scope.DetentionFlag = true;
	$scope.PenaltyVehicleTypeFlag = true;
	$scope.PenaltyToStationFlag = true;
	$scope.PenaltyFromStationFlag = true;
	$scope.rbkmVehicleTypeFlag = true;
	$scope.rbkmToStationFlag = true;
	$scope.rbkmFromStationFlag = true;
	$scope.rbkmStateCodeFlag = true;
	$scope.BonusFromStationFlag = true;
	$scope.BonusToStationFlag = true;
	$scope.DetentionFromStationFlag = true;
	$scope.DetentionToStationFlag = true;
	$scope.BonusVehicleTypeFlag = true;
	$scope.DetentionVehicleTypeFlag = true;
	$scope.pbdFlag=false;
	$scope.rbkmFlag=false;
	$scope.ProductTypeFlag1 = true;
	$scope.VehicleTypeFlag1 = true;
	$scope.dlyContFlag = true;
	$scope.OldDailyContractFlag = true;
	$scope.olddlyCont=true;
	
	$scope.contToStationFlagW = true;
	$scope.contToStationFlagQ = true;
	$scope.ToStationsDBFlag = true
	$scope.newCtsflagW=false;
	$scope.newCtsflagQ=false;
	$scope.AddProductTypeFlag=true;
	$scope.addStationFlag = true;
	$scope.StateCodeFlag = true;
	
	$scope.type=[];
	$scope.pbdD={};
	$scope.pbdB={};
	$scope.pbdP={};
	$scope.tnc={};
	$scope.newRbkm={};
	$scope.rbkm={};
	$scope.dlyCnt={};
	$scope.oldCont={};
	$scope.cts={};
	
	$(document).ready(function() {
	    $('#dlyContUnLoad').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContProportionate').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContLoad').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContTransitDay').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContStatisticalCharge').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContAdditionalRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContFromWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContToWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#dlyContRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmFromKm').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmToKm').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#rbkmRate').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayP').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayP').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtP').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayB').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayB').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtB').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdFromDayD').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdToDayD').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#pbdAmtD').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtLoadLimit').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtGuaranteeWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	        var max = 7;
	        
	        $('#pbdAmtD').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            }
	        });
	        
	        $('#pbdToDayD').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#pbdFromDayD').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#pbdAmtB').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            }
	        });
	        
	        $('#pbdToDayB').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        
	        $('#pbdFromDayB').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#pbdAmtP').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#pbdFromDayP').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            }
	        });
	        
	        $('#pbdToDayP').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#rbkmFromKm').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#rbkmToKm').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#rbkmRate').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#dlyContRate').keypress(function(e) { 
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#dlyContFromWt').keypress(function(e) {
	            if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#dlyContToWt').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            }
	        });
	        
	        $('#dlyContTransitDay').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            }
	        });
	        
	        $('#dlyContStatisticalCharge').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#dlyContLoad').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#dlyContUnLoad').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#dlyContAdditionalRate').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#vtLoadLimit').keypress(function(e) {
	            if (this.value.length == max) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#vtGuaranteeWt').keypress(function(e) {
	        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	                e.preventDefault();
	            } 
	        });
	        
	        $('#vtVehicleType').keypress(function(e) {
	            if (this.value.length == 30) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#vtServiceType').keypress(function(e) {
	            if (this.value.length == 30) {
	                e.preventDefault();
	            } 
	        });
	        
	        $('#vtCode').keypress(function(e) {
	            if (this.value.length == 1) {
	                e.preventDefault();
	            }  
	        });
	});
	
	
	$scope.OpenDlyContCodeDB = function(){
		$scope.dlyContCodeFlag = false;
    	$('div#dlyContCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
			$('div#dlyContCodeDB').dialog('open');
		}
	
	$scope.OpenBranchCodeDB = function(){
		$scope.branchCodeFlag = false;
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch Code",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
			$('div#branchCodeDB').dialog('open');
		}
	
	$scope.OpendlyContBLPMCodeDB = function(){
		$scope.blpmCodeFlag = false;
		$('div#dlyContBLPMCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "BLPM Code",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
			});

			$('div#dlyContBLPMCodeDB').dialog('open');
		}
	
	$scope.OpendlyContCngrCodeDB=function(){
		$scope.CngrCodeFlag = false;
		$('div#dlyContCngrCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Consignor",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContCngrCodeDB').dialog('open');
		}
	
	$scope.OpendlyContCrNameDB = function(){
		$scope.CrNameFlag = false;
		$('div#dlyContCrNameDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer Representative Name",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
		$('div#dlyContCrNameDB').dialog('open');
		}
	
	$scope.OpendlyContFromStationDB = function(){
		$scope.FromStationFlag = false;
		$('div#dlyContFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContFromStationDB').dialog('open');
			}
	
	$scope.OpendlyContToStationDB = function(){
		$scope.ToStationFlag = false;
		$('div#dlyContToStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContToStationDB').dialog('open');
		}
	
	$scope.OpendlyContRbkmIdDB = function(metricType){
		if(metricType==="" || metricType===null || angular.isUndefined(metricType)){
			$scope.alertToast("Please Enter Metric Type");
		}else{
			$scope.RbkmIdFlag = false;
			$('div#dlyContRbkmIdDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "RBKM ID",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContRbkmIdDB').dialog('open');
		}	
	}
	
	$scope.OpendlyContProductTypeDB = function(){
		$scope.ProductTypeFlag = false;
		$('div#dlyContProductTypeDB').dialog({
		autoOpen: false,
		modal:true,
		resizable: false,
		position: UDPos,
		show: UDShow,
		hide: UDHide,
		title: "Product Type",
		draggable: true,
		close: function(event, ui) { 
	        $(this).dialog('destroy');
	        $(this).hide();
	    }
		});
		$('div#dlyContProductTypeDB').dialog('open');
	}

	
	$scope.OpendlyContVehicleTypeDB = function(){
		$scope.VehicleTypeFlag = false;
		$('div#dlyContVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContVehicleTypeDB').dialog('open');		
		}
	
	$scope.OpenPenaltyDB = function(){
		$scope.PenaltyFlag = false;
		$scope.pbdP.pbdPenBonDet = "P";
		
		$('div#PenaltyDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Penalty",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#PenaltyDB').dialog('open');
	}
	
	$scope.OpenBonusDB = function(){
		$scope.BonusFlag = false;
		$scope.pbdB.pbdPenBonDet = "B";
		
		$('div#BonusDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bonus",
			draggable: true,
			close: function(event, ui) { 
				 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
			
		$('div#BonusDB').dialog('open');
		}
	
	$scope.OpenDetentionDB = function(){
		$scope.DetentionFlag = false;
		$scope.pbdD.pbdPenBonDet = "D";
		
		$('div#DetentionDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Detention",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		
		$('div#DetentionDB').dialog('open');
	}
	
	$scope.OpenPenaltyFromStationDB = function(){
		
		$scope.PenaltyFromStationFlag = false;
		$('div#PenaltyFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyFromStationDB').dialog('open');
		}
		
	
	$scope.OpenPenaltyToStationDB = function(){
		
		$scope.PenaltyToStationFlag = false;
		$('div#PenaltyToStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
				 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyToStationDB').dialog('open');
		}
	
	$scope.OpenPenaltyVehicleTypeDB = function(){
		
		$scope.PenaltyVehicleTypeFlag = false;
		$('div#PenaltyVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenrbkmFromStationDB = function(){
		$scope.rbkmFromStationFlag = false;
		$('div#rbkmFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});

			$('div#rbkmFromStationDB').dialog('open');
		}
	
	$scope.OpenrbkmToStationDB = function(){
		$scope.rbkmToStationFlag = false;
		$('div#rbkmToStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmToStationDB').dialog('open');
		}
	
	
	$scope.OpenRbkmVehicleTypeDB = function(){
		
		$scope.rbkmVehicleTypeFlag = false;
		$('div#rbkmVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenrbkmStateCodeDB = function(){
		$scope.rbkmStateCodeFlag = false;
		$('div#rbkmStateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State Code",
			draggable: true,
			close: function(event, ui) { 
				 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#rbkmStateCodeDB').dialog('open');
		}
	
	$scope.OpenPenaltyFromStationDB = function(){
		
		$scope.PenaltyFromStationFlag = false;
		$('div#PenaltyFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#PenaltyFromStationDB').dialog('open');
		}
	
	$scope.OpenBonusFromStationDB = function(){
		
		$scope.BonusFromStationFlag = false;
		$('div#BonusFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
				 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
			});
			
			$('div#BonusFromStationDB').dialog('open');
		}
		
	
	$scope.OpenBonusToStationDB = function(){
		
		$scope.BonusToStationFlag = false;
		$('div#BonusToStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
				 
		        $(this).dialog('destroy') ;
		        $(this).hide();
		    }
			});
			
			$('div#BonusToStationDB').dialog('open');
		}
	
	$scope.OpenBonusVehicleTypeDB = function(){
		
		$scope.BonusVehicleTypeFlag = false;
		$('div#BonusVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#BonusVehicleTypeDB').dialog('open');
		}
	
	$scope.OpenDetentionFromStationDB = function(){
		
		$scope.DetentionFromStationFlag = false;
		$('div#DetentionFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionFromStationDB').dialog('open');
		}
	
	$scope.OpenDetentionToStationDB = function(){
		
		$scope.DetentionToStationFlag = false;
		$('div#DetentionToStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) {  
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionToStationDB').dialog('open');
		}
	
	$scope.OpenDetentionVehicleTypeDB = function(){
		
		$scope.DetentionVehicleTypeFlag = false;
		$('div#DetentionVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#DetentionVehicleTypeDB').dialog('open');
		}
	
	$scope.OpendlyContProductTypeDB1 = function(){
		$scope.ProductTypeFlag1 = false;
		$('div#dlyContProductTypeDB1').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Product Type",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContProductTypeDB1').dialog('open');
		}
	
	$scope.OpendlyContVehicleTypeDB1 = function(){
		$scope.VehicleTypeFlag1 = false;
		$('div#dlyContVehicleTypeDB1').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#dlyContVehicleTypeDB1').dialog('open');
		}
	
	
	$scope.openOldContractDB = function(){
		$scope.OldDailyContractFlag = false;
    	$('div#OldDailyContractDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
			$('div#OldDailyContractDB').dialog('open');
		}
	
	
	$scope.contToStnDB = function(dlyCnt,metricType){
		if(metricType==="" || angular.isUndefined(metricType)){
			$scope.alertToast("please Enter metric type ");	
			//$('input[name=dlyContType]').attr('checked',false);
		}
		else if(dlyCnt.dlyContType==='W'){
			$scope.contToStationFlagW = false;
			$scope.cts.ctsProductType="-100";
			$('div#contToStationW').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
				});
			
			$('div#contToStationW').dialog('open');
		}else if(dlyCnt.dlyContType==='Q'){
			$scope.contToStationFlagQ = false;
			$scope.cts.ctsProductType="";
			$('div#contToStationQ').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Contract To Station",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
				});
			
			$('div#contToStationQ').dialog('open');
		}
	}
	
	$scope.openToStationsDB = function(){	
		$scope.ToStationsDBFlag = false;
		$('div#ToStationsDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Stations",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
			}
			});
		
		$('div#ToStationsDB').dialog('open');
		}
	
	$scope.openCtsVehicleType = function(){
		$scope.VehicleTypeFlag = false;
		$('div#ctsVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
			}
			});
			$('div#ctsVehicleTypeDB').dialog('open');		
		}
	
	$scope.OpenAddVehicleTypeDB = function(){
		$scope.VehicleTypeFlag1 = false;
		$('div#addVehicleTypeDB1').dialog({
			autoOpen: false,
			modal:true,
			title: "Vehicle Type",
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
			}
			});
			$('div#addVehicleTypeDB1').dialog('open');
		}
	
	$scope.openProductTypeDB = function(){
		$scope.ProductTypeFlag = false;
		$('div#productTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Product Type",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
			}
		});
		$('div#productTypeDB').dialog('open');
	}
	
	$scope.OpenProductTypeDB1 = function(){
		console.log("M here");
		$scope.AddProductTypeFlag = false;
		$('div#AddProductTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Product Type",
			draggable: true,
			close: function(event, ui) { 	
				$(this).dialog('destroy');
				$(this).hide();
			}
			});
			$('div#AddProductTypeDB').dialog('open');
		}

	$scope.openAddNewStnDB = function(){	
		$scope.addStationFlag = false;
		$('div#addStation').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Station",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#addStation').dialog('open');
		}
	
	$scope.openStateCodeDB = function(){	
		$scope.StateCodeFlag = false;
		$('div#StateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State Code",
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			$('div#StateCodeDB').dialog('open');
		}
	
	$scope.saveTncUnload = function(){
		if($scope.checkUnLoad){
			$scope.checkUnLoadFlag=false;
			$scope.tnc.tncUnLoading="yes";
		}
		if(!($scope.checkUnLoad)){
			$scope.checkUnLoadFlag=true;
			$scope.tnc.tncUnLoading="no";
			$scope.dlyCnt.dlyContUnLoad="";
		}	
	}

	$scope.saveTncLoad= function(){
		if($scope.checkLoad){
			$scope.checkLoadFlag=false;
			$scope.tnc.tncLoading="yes";
		}
		if(!($scope.checkLoad)){
			$scope.checkLoadFlag=true;
			$scope.dlyCnt.dlyContLoad="";
			$scope.tnc.tncLoading="no";
		}	
	}
	
	$scope.saveTncPenalty = function(){
		if($scope.checkPenalty){
			$scope.checkPenaltyFlag=false;
			$scope.tnc.tncPenalty="yes";
		}
		if(!($scope.checkPenalty)){
			$scope.checkPenaltyFlag=true;
			$scope.tnc.tncPenalty="no";
		}	
	}

	$scope.saveTncBonus = function(){
		if($scope.checkBonus){
			$scope.checkBonusFlag=false;
			$scope.tnc.tncBonus="yes";
		}
		if(!($scope.checkBonus)){
			$scope.checkBonusFlag=true;
			$scope.tnc.tncBonus="no";
		}	
	}

	$scope.saveTncDetention = function(){
		if($scope.checkDetention){
			$scope.checkDetentionFlag=false;
			$scope.tnc.tncDetention="yes";
		}
		if(!($scope.checkDetention)){
			$scope.checkDetentionFlag=true;
			$scope.tnc.tncDetention="no";
		}	
	}

	$scope.saveTncTransitDay = function(){
		if($scope.checkTransitDay){
			$scope.checkTransitFlag=false;
			$scope.tnc.tncTransitDay="yes";
		}
		if(!($scope.checkTransitDay)){
			$scope.checkTransitFlag=true;
			$scope.dlyCnt.dlyContTransitDay="";
			$scope.tnc.tncTransitDay="no";
		}	
	}	

	$scope.saveTncStatisticalCharge = function(){
		if($scope.checkStatisticalCharge){
			$scope.checkStatisticalChargeFlag=false;
			$scope.tnc.tncStatisticalCharge="yes";
		}
		if(!($scope.checkStatisticalCharge)){
			$scope.checkStatisticalChargeFlag=true;
			$scope.dlyCnt.dlyContStatisticalCharge="";
			$scope.tnc.tncStatisticalCharge="no";
		}	
	}
	
	
	$scope.enableModalTextBox = function(){
		$('#dlyContByMId').removeAttr("disabled");
		 $('#dlyContByAId').attr("disabled","disabled");
		 $('#ok').attr("disabled","disabled");
		 $scope.dlyContByA="";
	}
	
	$scope.enableAutoTextBox = function(){
		$('#dlyContByAId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		 $('#dlyContByMId').attr("disabled","disabled");
		 $('#oldDlyContId').attr("disabled","disabled");
		 $scope.dlyContByM="";
		 $scope.oldDlyCont="";
		 $scope.olddlyCont=true;
	}
	
	$scope.saveContCode = function(dlyContCodes){
		console.log("Enter into saveContCode function");
		$scope.oldDlyCont="";
		$scope.dlyContByM=dlyContCodes;
		$scope.contractCode=dlyContCodes;
		$('div#dlyContCodeDB').dialog('close');
		$scope.showContractDetails = false;
		$scope.isViewNo=true;
		$scope.olddlyCont=true;
		
		var response = $http.post($scope.projectName+'/dailycontractdetails', dlyContCodes);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.checkPenalty=false;
				$scope.checkBonus=false;
				$scope.checkDetention=false;
				$('#oldDlyContId').removeAttr("disabled");
				$scope.dlyCnt = data.dailyContract;
				$scope.dlyCntOld=data.dailyContractOld;
				for(var i=0;i<$scope.dlyCntOld.length;i++){
					$scope.dlyCntOld[i].creationTS =  $filter('date')($scope.dlyCntOld[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				console.log($scope.dlyCnt.dlyContFromStation);
				/*console.log($scope.dlyCnt.dlyContFromWt);
				console.log($scope.dlyCnt.dlyContToWt);*/
				//$scope.dlyContRatePer="Per Kg";
				//$scope.fromWt="Ton";
				//$scope.toWt="Ton";
			/*	$scope.dlyCnt.dlyContFromWt=($scope.dlyCnt.dlyContFromWt/$scope.kgInTon);
				$scope.dlyCnt.dlyContToWt=($scope.dlyCnt.dlyContToWt/$scope.kgInTon);
				console.log($scope.dlyCnt.dlyContFromWt);
				console.log($scope.dlyCnt.dlyContToWt);*/
				
				if($scope.dlyCnt.dlyContLoad===0){
					$scope.checkLoad=false;
				}else{
					$scope.checkLoad=true;
				}
				
				if($scope.dlyCnt.dlyContUnLoad===0){
					$scope.checkUnLoad=false;
				}else{
					$scope.checkUnLoad=true;
				}
				
				if($scope.dlyCnt.dlyContTransitDay===0){
					$scope.checkTransitDay=false;
				}else{
					$scope.checkTransitDay=true;
				}
				
				if($scope.dlyCnt.dlyContStatisticalCharge===0){
					$scope.checkStatisticalCharge=false;
				}else{
					$scope.checkStatisticalCharge=true;
				}
				
				if($scope.dlyCnt.dlyContType==="K"){
					console.log($scope.dlyCnt.dlyContType);
					$('#dlyContRbkmId').removeAttr("disabled");
					$scope.onWQClick=false;
					$scope.onKClick=true;
					//$('#dlyContProductType').attr("disabled","disabled");
					//$('#openProductTypeDB').attr("disabled","disabled");
				}else if($scope.dlyCnt.dlyContType==="Q"){
					console.log($scope.dlyCnt.dlyContType);
					$('#dlyContRbkmId').attr("disabled","disabled");
					//$('#ToStations').removeAttr("disabled");
					$scope.onWQClick=true;
					$scope.onKClick=false;
					 //$('#openProductTypeDB').removeAttr("disabled");
				}else {
					console.log($scope.dlyCnt.dlyContType);
					 $('#dlyContRbkmId').attr("disabled","disabled");
					 $scope.onWQClick=true;
					 $scope.onKClick=false;
					 //$('#dlyContProductType').attr("disabled","disabled");
					 //$('#openProductTypeDB').attr("disabled","disabled");
				}
				
				/*if($scope.dlyCnt.dlyContProportionate==="F"){
					console.log($scope.dlyCnt.dlyContProportionate);
					$('#dlyContAdditionalRate').removeAttr("disabled");
				}else if($scope.dlyCnt.dlyContProportionate==="P"){
					console.log($scope.dlyCnt.dlyContProportionate);
					 $('#dlyContAdditionalRate').attr("disabled","disabled");
				}*/
		
				$scope.getRateByKmData();
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	
	$scope.clickF = function(){
		$scope.Fflag=false;
		$('#dlyContAdditionalRate').removeAttr("disabled");
	}
	
	$scope.clickP = function(){
		$scope.Fflag=true;
		$scope.dlyCnt.dlyContAdditionalRate="";
		$('#dlyContAdditionalRate').attr("disabled","disabled");
	}
	
	
	$scope.savBLPMCode = function(customer){
		console.log("enter into savBLPMCode function");
		$scope.dlyCnt.dlyContBLPMCode = customer.custCode;
		$scope.dlyCnt.dlyContCrName="";
		$('div#dlyContBLPMCodeDB').dialog("destroy");
		$scope.blpmCodeFlag = true;	
		console.log("--"+customer.custCode);
		 var response = $http.post($scope.projectName+'/getCustomerRepresentativeData',customer.custCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.list = data.list;
			   $('#dlyContCrName').removeAttr("disabled");
			   console.log(data.result);
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
	$scope.setTransitFlagTrue = function(dlyCnt){
		if(dlyCnt.dlyContTransitDay === null || dlyCnt.dlyContTransitDay === "" || angular.isUndefined(dlyCnt.dlyContTransitDay)){
			$scope.checkTransitFlag=false;
		}else{
			$scope.checkTransitFlag=true;
		}
	}
	
	$scope.setSCFlagTrue = function(dlyCnt){
		if(dlyCnt.dlyContStatisticalCharge === null || dlyCnt.dlyContStatisticalCharge==="" || angular.isUndefined(dlyCnt.dlyContStatisticalCharge)){
			$scope.checkStatisticalChargeFlag=false;
		}
		else{
			$scope.checkStatisticalChargeFlag=true;
		}
	}
	
	$scope.setLoadFlagTrue = function(dlyCnt){
		if(dlyCnt.dlyContLoad === null || dlyCnt.dlyContLoad==="" || angular.isUndefined(dlyCnt.dlyContLoad)){
			$scope.checkLoadFlag=false;
		}else{
			$scope.checkLoadFlag=true;
		}
	}
	
	$scope.setUnLoadFlagTrue = function(dlyCnt){
		if(dlyCnt.dlyContUnLoad === null || dlyCnt.dlyContUnLoad==="" || angular.isUndefined(dlyCnt.dlyContUnLoad)){
			$scope.checkUnLoadFlag=false;
		}else{
			$scope.checkUnLoadFlag=true;	
		}
	}
	
	$scope.setFflagTrue = function(dlyCnt){
		console.log(dlyCnt.dlyContAdditionalRate);
		if(dlyCnt.dlyContAdditionalRate===null || dlyCnt.dlyContAdditionalRate==="" || angular.isUndefined(dlyCnt.dlyContAdditionalRate)){
			$scope.Fflag=false;	
		}
		else{
			$scope.Fflag=true;
		}
	}

	
	$scope.savCngrCode = function(cngrCode){
		$scope.dlyCnt.dlyContCngrCode = cngrCode.custCode;
		$('div#dlyContCngrCodeDB').dialog("destroy");
		$scope.CngrCodeFlag = true;
	}
	
	$scope.savBranchCode = function(branch){
		$scope.dlyCnt.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog("destroy");
		$scope.branchCodeFlag = true;
	}
	
	$scope.savFrmStnCode = function(Fstation){
		$scope.dlyCnt.dlyContFromStationTemp = Fstation.stnName;
		$scope.dlyCnt.dlyContFromStation = Fstation.stnCode;
		$('div#dlyContFromStationDB').dialog("destroy");
		$scope.FromStationFlag = true;
	}
	
	$scope.savToStnCode = function(Tstation){
		$scope.dlyCnt.dlyContToStationTemp = Tstation.stnName;
		$scope.dlyCnt.dlyContToStation =Tstation.stnCode;
		$('div#dlyContToStationDB').dialog("destroy");
		$scope.ToStationFlag = true;
	}
	
	$scope.savCrName = function(cr){
		$scope.dlyCnt.dlyContCrNameTemp =cr.custRepName;
		$scope.dlyCnt.dlyContCrName = cr.custRepCode;
		$('div#dlyContCrNameDB').dialog("destroy");
		$scope.CrNameFlag = true;
	}
	
	$scope.savProductName = function(value){
		//	$scope.Qflag =true;
			$scope.cts.ctsProductType = value;
			$('div#productTypeDB').dialog("destroy");
			$scope.ProductTypeFlag = true;
		}
	
	$scope.savVehicleType = function(vtype){
		$scope.dlyCnt.dlyContVehicleType = vtype.vtCode;
		$('div#dlyContVehicleTypeDB').dialog("destroy");
		$scope.VehicleTypeFlag = true;
	}
	
	$scope.savPenaltyFromStn = function(PFstation){
		$scope.pbdP.pbdFromStnTemp = PFstation.stnName;
		$scope.pbdP.pbdFromStn =PFstation.stnCode;;
		$('div#PenaltyFromStationDB').dialog("destroy");
		$scope.PenaltyFromStationFlag = true;
	}
	
	$scope.savPenaltyToStn = function(PTstation){
		$scope.pbdP.pbdToStnTemp = PTstation.stnName;
		$scope.pbdP.pbdToStn =PTstation.stnCode;;
		$('div#PenaltyToStationDB').dialog("destroy");
		$scope.PenaltyToStationFlag = true;
	}
	
	$scope.savPenaltyVehicleType = function(VtPenalty){
		$scope.pbdP.pbdVehicleType=VtPenalty.vtCode;
		$('div#PenaltyVehicleTypeDB').dialog("destroy");
		$scope.PenaltyVehicleTypeFlag = true;
	}
	
	$scope.savRbkmFromStnCode = function(rbkmFStation){
		$scope.newRbkm.rbkmFromStationTemp = rbkmFStation.stnName;
		$scope.newRbkm.rbkmFromStation =rbkmFStation.stnCode;;
		$('div#rbkmFromStationDB').dialog("destroy");
		$scope.rbkmFromStationFlag = true;
	}
	
	$scope.savRbkmVehicleType = function(RbkmVType){
		$scope.newRbkm.rbkmVehicleType = RbkmVType.vtCode;
		$('div#rbkmVehicleTypeDB').dialog("destroy");
		$scope.rbkmVehicleTypeFlag = true;
	}
	
	$scope.saveRbkmToStnCode = function(rbkmTStation){
		$scope.newRbkm.rbkmToStationTemp = rbkmTStation.stnName;
		$scope.newRbkm.rbkmToStation =rbkmTStation.stnCode;;
		$('div#rbkmToStationDB').dialog("destroy");
		$scope.rbkmToStationFlag = true;
	}
	
	$scope.saveStateCode = function(state){
		$scope.newRbkm.rbkmStateCodeTemp=state.stateName;
		$scope.newRbkm.rbkmStateCode=state.stateCode;
		$('div#rbkmStateCodeDB').dialog("destroy");
		$scope.rbkmStateCodeFlag = true;
	}
	
	$scope.savBonusFromStn = function(BFstation){
		$scope.pbdB.pbdFromStnTemp = BFstation.stnName;
		$scope.pbdB.pbdFromStn =BFstation.stnCode;;
		$('div#BonusFromStationDB').dialog("destroy");
		$scope.BonusFromStationFlag = true;
	}
	
	$scope.savBonusToStn = function(BTstation){
		$scope.pbdB.pbdToStnTemp = BTstation.stnName;
		$scope.pbdB.pbdToStn =BTstation.stnCode;;
		$('div#BonusToStationDB').dialog("destroy");
		$scope.BonusToStationFlag = true;
	}
	
	$scope.savBonusVehicleType = function(vehicleType){
		$scope.pbdB.pbdVehicleType=vehicleType.vtCode;
		$('div#BonusVehicleTypeDB').dialog("destroy");
		$scope.BonusVehicleTypeFlag = true;
	}
	
	$scope.savDetentionFromStn = function(DFstation){
		$scope.pbdD.pbdFromStnTemp = DFstation.stnName;
		$scope.pbdD.pbdFromStn =DFstation.stnCode;;
		$('div#DetentionFromStationDB').dialog("destroy");
		$scope.DetentionFromStationFlag = true;
	}
	
	$scope.savDetentionToStn = function(DTstation){
		$scope.pbdD.pbdToStnTemp = DTstation.stnName;
		$scope.pbdD.pbdToStn =DTstation.stnCode;;
		$('div#DetentionToStationDB').dialog("destroy");
		$scope.DetentionToStationFlag = true;
	}
	
	$scope.savDetentionVehicleType = function(vtDetention){
		$scope.pbdD.pbdVehicleType=vtDetention.vtCode;
		$('div#DetentionVehicleTypeDB').dialog("destroy");
		$scope.DetentionVehicleTypeFlag = true;
	}
	
	$scope.saveToStations = function(toStations){
		console.log(toStations.stnName);
		$scope.cts.ctsToStnTemp=toStations.stnName;
		$scope.cts.ctsToStn=toStations.stnCode;
		$('div#ToStationsDB').dialog("destroy");
		$scope.ToStationsDBFlag = true;
	}	
	
	$scope.savVehicleType = function(vtype){
		$scope.cts.ctsVehicleType = vtype.vtCode;
		$('div#ctsVehicleTypeDB').dialog("destroy");
		$scope.VehicleTypeFlag = true;
	}
	
	$scope.savStateCode = function(statecode){
		$scope.station.stateCode=statecode.stateCode;
		$('div#StateCodeDB').dialog("destroy");
		$scope.StateCodeFlag = true;
	}
	
	$scope.saveOldDC = function(oldDC){
		$scope.oldDlyCont=oldDC.creationTS;
		console.log(oldDC.creationTS);
		$('div#OldDailyContractDB').dialog("destroy");
		$scope.OldDailyContractFlag = true;
		$scope.showContractDetails=true;
		$scope.olddlyCont=false;
		
		for(var i=0;i<$scope.dlyCntOld.length;i++){
			if($scope.dlyCntOld[i].creationTS===oldDC.creationTS){
				console.log($scope.dlyCntOld[i].creationTS);
				$scope.oldCont= $scope.dlyCntOld[i];
			}
		}
	}
	
	$scope.getContCodeList=function(){
	      $( "#dlyContByAId" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	}
		
	$scope.getContractList = function(){
		$scope.code = $( "#dlyContByAId" ).val();
		if($scope.code===""){
			$scope.alertToast("Please enter code");
		}
		else if($scope.code.substring(0,3)==="dly"){
		$scope.showContractDetails = false;
		$scope.isViewNo=true;
		$scope.contractCode=$scope.code;
		
		var response = $http.post($scope.projectName+'/dailycontractdetails',$scope.code);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.checkPenalty=false;
				$scope.checkBonus=false;
				$scope.checkDetention=false;
				
				$scope.dlyCnt = data.dailyContract;	
				
				$scope.dlyContRatePer="Per Kg";
				$scope.fromWt="Ton";
				$scope.toWt="Ton";
				$scope.dlyCnt.dlyContFromWt=($scope.dlyCnt.dlyContFromWt/$scope.kgInTon);
				$scope.dlyCnt.dlyContToWt=($scope.dlyCnt.dlyContToWt/$scope.kgInTon);
				
				if($scope.dlyCnt.dlyContLoad===0){
					$scope.checkLoad=false;
				}else{
					$scope.checkLoad=true;
				}
				
				if($scope.dlyCnt.dlyContUnLoad===0){
					$scope.checkUnLoad=false;
				}else{
					$scope.checkUnLoad=true;
				}
				
				if($scope.dlyCnt.dlyContTransitDay===0){
					$scope.checkTransitDay=false;
				}else{
					$scope.checkTransitDay=true;
				}
				
				if($scope.dlyCnt.dlyContStatisticalCharge===0){
					$scope.checkStatisticalCharge=false;
				}else{
					$scope.checkStatisticalCharge=true;
				}
				
				if($scope.dlyCnt.dlyContType==="K"){
					console.log($scope.dlyCnt.dlyContType);
					$('#dlyContRbkmId').removeAttr("disabled");
				}else if($scope.dlyCnt.dlyContType==="Q"){
					console.log($scope.dlyCnt.dlyContType);
					$('#dlyContProductType').removeAttr("disabled");
					 $('#openProductTypeDB').removeAttr("disabled");
				}else {
					console.log($scope.dlyCnt.dlyContType);
					 $('#dlyContRbkmId').attr("disabled","disabled");
					 $('#dlyContProductType').attr("disabled","disabled");
					 $('#openProductTypeDB').attr("disabled","disabled");
				}
				
				if($scope.dlyCnt.dlyContProportionate==="F"){
					console.log($scope.dlyCnt.dlyContProportionate);
					$('#dlyContAdditionalRate').removeAttr("disabled");
				}else if($scope.dlyCnt.dlyContProportionate==="P"){
					console.log($scope.dlyCnt.dlyContProportionate);
					 $('#dlyContAdditionalRate').attr("disabled","disabled");
				}
				
				$scope.getRateByKmData();
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}else{
		$scope.alertToast("Enter correct code");
	}
	}
	
	$scope.getDailyContCode = function(){
		var response = $http.get($scope.projectName+'/getDailyContCode');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			$scope.getDlyContIsViewNo();
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getDlyContIsViewNo = function(){
		   var response = $http.post($scope.projectName+'/getDlyContIsViewNo');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				   $scope.removeAllCTS();
			   $scope.successToast(data.result);
			   $scope.ContractList = data.list;
			   for(var i=0;i<$scope.ContractList.length;i++){
			   $scope.ContractList[i].creationTS = $filter('date')($scope.ContractList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
			   }
			   }else{
				   $scope.removeAllCTS();
				   $scope.isViewNo=true;
				   $scope.alertToast(data.result);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	$scope.toggleSelection = function toggleSelection(dlyContCode) {
		   var idx = $scope.selection.indexOf(dlyContCode);
		   
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
			   }else {
			   $scope.selection.push(dlyContCode);
		   }
	   }	  
	   
	 $scope.verifyContracts = function(){
		 console.log("enter into update with $scope.selection = "+$scope.selection);	
		 if(!$scope.selection.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
		 var response = $http.post($scope.projectName+'/updateIsViewContract',$scope.selection.toString());
		 response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.successToast(data.result);
				$scope.getDlyContIsViewNo();	
			}else{
				console.log(data.result);
			}
	    });	 
		 response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	 }
	 
	 $scope.backToList = function(){
	
		 $scope.showContractDetails = true;
		 $scope.isViewNo=false;
		 $scope.dlyContByM="";
		 $scope.dlyContByA="";
		 $scope.dlyContByModal=false;
		 $scope.dlyContByAuto=false;
		 $('#ok').attr("disabled","disabled");
		 $('#dlyContByAId').attr("disabled","disabled");
		 $('#dlyContByMId').attr("disabled","disabled");
		 $('#oldDlyContId').attr("disabled","disabled");
		 $scope.oldDlyCont="";
		 $scope.getDlyContIsViewNo();
	 }
	 
	 
	 $scope.saveRbkmId = function(RbkmForm,newRbkm){
			$('div#dlyContRbkmIdDB').dialog("destroy");
			$scope.RbkmIdFlag = true;
			$scope.Kflag=true;
			if(RbkmForm.$invalid){
				console.log("RbkmForm.$invalid"+RbkmForm.$invalid);
				
				if(RbkmForm.rbkmFromStationTemp.$invalid){
					$scope.alertToast("please Enter From station");
				}else if(RbkmForm.rbkmToStationTemp.$invalid){
					$scope.alertToast("please Enter To station");
				}else if(RbkmForm.rbkmStateCodeTemp.$invalid){
					$scope.alertToast("please Enter State code");
				}else if(RbkmForm.rbkmToKm.$invalid){
					$scope.alertToast("to km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmFromKm.$invalid){
					$scope.alertToast("from km cannot be greater than 7 characters");
				}else if(RbkmForm.rbkmVehicleType.$invalid){
					$scope.alertToast("please Enter Vehicle Type");
				}else if(RbkmForm.rbkmRate.$invalid){
					$scope.alertToast("please Enter correct rbkmrate Type");
				}
			}else{
			   if($scope.newRbkm.rbkmStartDate>$scope.newRbkm.rbkmEndDate){
			    	$scope.alertToast("Start date cannot be greater than end date");
			   }else if($scope.newRbkm.rbkmFromKm>$scope.newRbkm.rbkmToKm){
			    	$scope.alertToast("from km cannot be greater than to km");
			   } else{
				   var response = $http.post($scope.projectName+'/addNewRbkm', newRbkm);
			   
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					console.log("from server ---->"+data.result);
					$scope.fetchRbkmList();
					$scope.newRbkm.rbkmFromStationTemp="";
					$scope.newRbkm.rbkmToStationTemp="";
					$scope.newRbkm.rbkmStateCodeTemp="";
					$scope.newRbkm.rbkmStartDate="";
					$scope.newRbkm.rbkmEndDate="";
					$scope.newRbkm.rbkmFromKm="";
					$scope.newRbkm.rbkmToKm="";
					$scope.newRbkm.rbkmVehicleType="";
					$scope.newRbkm.rbkmRate="";
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}	
}		
	 
	 $scope.saveproducttype = function(ProductTypeDB1Form,pt){
			$('div#dlyContProductTypeDB1').dialog("destroy");
			$scope.ProductTypeFlag1=true;
			$scope.ProductTypeFlag=true;
			$scope.Qflag=true;
			pt.ptName=pt.ptName.toUpperCase();
			console.log("pt==========="+pt.ptName);
			if($.inArray(pt.ptName,$scope.ptList)!== -1){
				$scope.alertToast("Name already exists");
				$scope.pt.ptName="";
			}else{
				var response = $http.post($scope.projectName+'/saveproducttype',pt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getProductName();
						$scope.pt.ptName="";
					}
					else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}	
		}
	 
	 $scope.fetchRbkmList = function(){
		 var response = $http.get($scope.projectName+'/fetchRbkmListForEditContract');
		 response.success(function(data, status, headers, config){
			 if(data.result==="success"){
				 $scope.rateByKmList = data.list;
				 $scope.newRbkmFlag=true;
				 $scope.removeAllPbd();	
			 }else{
				 console.log(data.result);
				 $scope.newRbkmFlag=false;
				 $scope.removeAllPbd();	
			 }	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	 
	 $scope.savePbdForPenalty = function(PenaltyForm,pbdP){
			$('div#PenaltyDB').dialog("destroy");
			$scope.checkPenaltyFlag=true;
			$scope.PenaltyFlag=true;
			if(PenaltyForm.$invalid){
				if(PenaltyForm.pbdFromStnTempP.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(PenaltyForm.pbdToStnTempP.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(PenaltyForm.pbdVehicleTypeP.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(PenaltyForm.pbdToDayP.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(PenaltyForm.pbdFromDayP.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(PenaltyForm.pbdHourNDayP.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(PenaltyForm.pbdAmtP.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
			}else{
				if($scope.pbdP.pbdFromDay>$scope.pbdP.pbdToDay){
					$scope.alertToast("From dlay cannot be greater than to dlay");
			    }else if($scope.pbdP.pbdStartDt>$scope.pbdP.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
				 }else {
					var response = $http.post($scope.projectName+'/addNewPbd', pbdP);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							console.log("from server--<<<"+data.result);
							$scope.fetchPbdList();
							$scope.pbdP.pbdFromStnTemp = "";
							$scope.pbdP.pbdToStnTemp = "";
							$scope.pbdP.pbdStartDt = "";
							$scope.pbdP.pbdEndDt = "";
							$scope.pbdP.pbdFromDay = "";
							$scope.pbdP.pbdToDay = "";
							$scope.pbdP.pbdVehicleType= "";
							$scope.pbdP.pbdHourNDay = "";
							$scope.pbdP.pbdAmt = "";
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
	 
	 $scope.savePbdForBonus = function(BonusForm,pbdB){
			$('div#BonusDB').dialog("destroy");
			$scope.checkBonusFlag=true;
			$scope.BonusFlag=true;
			if(BonusForm.$invalid){
				if(BonusForm.pbdFromStnTempB.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(BonusForm.pbdToStnTempB.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(BonusForm.pbdVehicleTypeB.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(BonusForm.pbdToDayB.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(BonusForm.pbdFromDayB.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(BonusForm.pbdHourNDayB.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(BonusForm.pbdAmtB.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
			}else{
				if($scope.pbdB.pbdFromDay>$scope.pbdB.pbdToDay){
					$scope.alertToast("From dlay cant be greater than to dlay");
			     }else if($scope.pbdB.pbdStartDt>$scope.pbdB.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
			     }else{
					var response = $http.post($scope.projectName+'/addNewPbd', pbdB);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
						console.log("from server--<<<"+data.result);
						$scope.fetchPbdList();
						$scope.pbdB.pbdFromStnTemp = "";
						$scope.pbdB.pbdToStnTemp = "";
						$scope.pbdB.pbdStartDt = "";
						$scope.pbdB.pbdEndDt = "";
						$scope.pbdB.pbdFromDay = "";
						$scope.pbdB.pbdToDay = "";
						$scope.pbdB.pbdVehicleType = "";
						$scope.pbdB.pbdHourNDay = "";
						$scope.pbdB.pbdAmt = "";
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
		
		$scope.savePbdForDetention = function(DetentionForm,pbdD){
			$('div#DetentionDB').dialog("destroy");
			$scope.checkDetentionFlag=true;
			$scope.DetentionFlag=true;
			if(DetentionForm.$invalid){
				if(DetentionForm.pbdFromStnTempD.$invalid){
					$scope.alertToast("please Enter From Station");
				}else if(DetentionForm.pbdToStnTempD.$invalid){
					$scope.alertToast("please Enter to Station");
				}else if(DetentionForm.pbdVehicleTypeD.$invalid){
					$scope.alertToast("please Enter vehicle type");
				}else if(DetentionForm.pbdToDayD.$invalid){
					$scope.alertToast("please Enter correct todlay");
				}else if(DetentionForm.pbdFromDayD.$invalid){
					$scope.alertToast("please Enter correct fromdlay");
				}else if(DetentionForm.pbdHourNDayD.$invalid){
					$scope.alertToast("please Enter Hour/Day");
				}else if(DetentionForm.pbdAmtD.$invalid){
					$scope.alertToast("please Enter correct amount");
				}
				}else{
				 if($scope.pbdD.pbdFromDay>$scope.pbdD.pbdToDay){
					$scope.alertToast("From dlay cannot be greater than to dlay");
			     } else if($scope.pbdD.pbdStartDt>$scope.pbdD.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
				    }else{
					var response = $http.post($scope.projectName+'/addNewPbd', pbdD);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
						console.log("from server--<<<"+data.result);
						$scope.fetchPbdList();
						$scope.pbdD.pbdFromStnTemp= "";
						$scope.pbdD.pbdToStnTemp = "";
						$scope.pbdD.pbdStartDt = "";
						$scope.pbdD.pbdEndDt = "";
						$scope.pbdD.pbdFromDay = "";
						$scope.pbdD.pbdToDay = "";
						$scope.pbdD.pbdVehicleType = "";
						$scope.pbdD.pbdHourNDay = "";
						$scope.pbdD.pbdAmt = "";
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}
			}
	 
	 
	 $scope.fetchPbdList = function(){
		 var response = $http.get($scope.projectName+'/fetchPbdListForEditContract');
		 response.success(function(data, status, headers, config){
			 if(data.result==="success"){
				 $scope.getDlyContCodesList();
			 		$scope.newPbdList = data.list;
			 		console.log(data);
			 		$scope.newPbdFlag=true;
			 }else{
				 console.log(data.result);
				 $scope.newPbdFlag=false;
				 $scope.checkPenalty=false;
				 $scope.checkBonus=false;
				 $scope.checkDetention=false;
				// $scope.getRegContCodesList ();
				 $scope.getDlyContCodesList();
			 }
			 		
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	 
	 $scope.removeNewPbd = function(pbd) {
			var response = $http.post($scope.projectName+'/removeNewPbd',pbd);
			response.success(function(data, status, headers, config) {
				$scope.fetchPbdList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	 
	 $scope.EditDailyContractSubmit = function(EditDailyContractForm,dlyCnt,tnc,pbdP,pbdB,pbdD){
		 console.log("Enter into submit edit function-----");
		 
		 	if(dlyCnt.dlyContStartDt>dlyCnt.dlyContEndDt){
				$scope.alertToast("From date cannot be greater than to date");
		 	}else if(dlyCnt.dlyContFromWt>dlyCnt.dlyContToWt){
				$scope.alertToast("From weight cannot be greater than to weight");
			}else if($scope.checkPenaltyFlag === false){
				if(pbdP.pbdFromStnTemp === null || pbdP.pbdFromStnTemp === "" || angular.isUndefined(pbdP.pbdFromStnTemp) || pbdP.pbdToStnTemp === null || pbdP.pbdToStnTemp === "" || angular.isUndefined(pbdP.pbdToStnTemp) || pbdP.pbdStartDt === null || pbdP.pbdStartDt === "" || angular.isUndefined(pbdP.pbdStartDt) ||
						pbdP.pbdEndDt===null || pbdP.pbdEndDt==="" ||angular.isUndefined(pbdP.pbdEndDt) || pbd.pbdFromDay===null || pbdP.pbdFromDay==="" || angular.isUndefined(pbdP.pbdFromDay) || pbdP.pbdToDay===null || pbdP.pbdToDay==="" || angular.isUndefined(pbdP.pbdToDay) || pbdP.pbdVehicleType===null || pbdP.pbdVehicleType==="" || angular.isUndefined(pbdP.pbdVehicleType) ||
						pbdP.pbdHourNDay===null || pbdP.pbdHourNDay==="" || angular.isUndefined(pbdP.pbdHourNDay) || pbdP.pbdAmt===null || pbdP.pbdAmt==="" || angular.isUndefined(pbdP.pbdAmt))
						{
							$scope.alertToast("Please fill Penalty");
						}
					}else if($scope.checkBonusFlag === false){
						if(pbdB.pbdFromStnTemp===null || pbdB.pbdFromStnTemp==="" || angular.isUndefined(pbdB.pbdFromStnTemp) || pbdB.pbdToStnTemp===null || pbdB.pbdToStnTemp==="" || angular.isUndefined(pbdB.pbdToStnTemp) || pbdB.pbdStartDt===null || pbdB.pbdStartDt==="" || angular.isUndefined(pbdB.pbdStartDt)
						|| pbdB.pbdEndDt===null || pbdB.pbdEndDt==="" ||angular.isUndefined(pbdB.pbdEndDt) || pbdB.pbdFromDay===null || pbdB.pbdFromDay==="" || angular.isUndefined(pbdB.pbdFromDay) || pbdB.pbdToDay===null || pbdB.pbdToDay==="" || angular.isUndefined(pbdB.pbdToDay) || pbdB.pbdVehicleType===null || pbdB.pbdVehicleType==="" || angular.isUndefined(pbdB.pbdVehicleType)
						|| pbdB.pbdHourNDay===null || pbdB.pbdHourNDay==="" || angular.isUndefined(pbdB.pbdHourNDay) || pbdB.pbdAmt===null || pbdB.pbdAmt==="" || angular.isUndefined(pbdB.pbdAmt))
						{
							$scope.alertToast("Please fill Bonus");
						}
					}else if($scope.checkDetentionFlag === false){
						if(pbdD.pbdFromStnTemp===null || pbdD.pbdFromStnTemp==="" || angular.isUndefined(pbdD.pbdFromStnTemp) || pbdD.pbdToStnTemp===null || pbdD.pbdToStnTemp==="" || angular.isUndefined(pbdD.pbdToStnTemp) || pbdD.pbdStartDt===null || pbdD.pbdStartDt==="" || angular.isUndefined(pbdD.pbdStartDt)
						|| pbdD.pbdEndDt===null || pbdD.pbdEndDt==="" ||angular.isUndefined(pbdD.pbdEndDt) || pbdD.pbdFromDay===null || pbdD.pbdFromDay==="" || angular.isUndefined(pbdD.pbdFromDay) || pbdD.pbdToDay===null || pbdD.pbdToDay==="" || angular.isUndefined(pbdD.pbdToDay) || pbdD.pbdVehicleType===null || pbdD.pbdVehicleType==="" || angular.isUndefined(pbdD.pbdVehicleType)
						|| pbdD.pbdHourNDay===null || pbdD.pbdHourNDay==="" || angular.isUndefined(pbdD) || pbdD.pbdAmt===null || pbdD.pbdAmt==="" || angular.isUndefined(pbdD.pbdAmt))
						{
							$scope.alertToast("Please fill Detention");
						}
					}else if($scope.checkLoadFlag === false){
						
						if(dlyCnt.dlyContLoad===0 || dlyCnt.dlyContLoad === null || dlyCnt.dlyContLoad==="" || angular.isUndefined(dlyCnt.dlyContLoad))
						{
							$scope.alertToast("Please fill Load");
						}
					} else if($scope.checkStatisticalChargeFlag === false){
						
						if(dlyCnt.dlyContStatisticalCharge === 0 || dlyCnt.dlyContStatisticalCharge === null || dlyCnt.dlyContStatisticalCharge==="" || angular.isUndefined(dlyCnt.dlyContStatisticalCharge))
						{
							$scope.alertToast("Please fill StatisticalCharge");
						}
					}else if($scope.checkUnLoadFlag === false){
						if(dlyCnt.dlyContUnLoad===0 || dlyCnt.dlyContUnLoad === null || dlyCnt.dlyContUnLoad==="" || angular.isUndefined(dlyCnt.dlyContUnLoad))
						{
							$scope.alertToast("Please fill UnLoad");
						}
					}else if($scope.checkTransitFlag === false){
						if(dlyCnt.dlyContTransitDay===0 || dlyCnt.dlyContTransitDay === null || dlyCnt.dlyContTransitDay === "" || angular.isUndefined(dlyCnt.dlyContTransitDay))
						{
							$scope.alertToast("Please fill Transit Day");
						}
					}else if($scope.Fflag===false){
						console.log($scope.Fflag);
						if(dlyCnt.dlyContAdditionalRate===0 || dlyCnt.dlyContAdditionalRate===null || dlyCnt.dlyContAdditionalRate==="" || angular.isUndefined(dlyCnt.dlyContAdditionalRate))
						{
							$scope.alertToast("please fill Additional Rate");	
						}
					}else {
						
						$scope.dlyContFlag = false;
				    	$('div#dlyContDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						    }
							});
						$('div#dlyContDB').dialog('open');
			}
		}
	 
	 $scope.back= function(){
			$scope.dlyContFlag = true;
			$('div#dlyContDB').dialog('close');
		}
	 
	 $scope.saveContract = function(dlyCnt,tnc){
		 console.log("Enter into saveContractFunction");
			 
			 var response = $http.post($scope.projectName+'/EditDailyContractSubmit',dlyCnt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$('div#dlyContDB').dialog('close');
						$scope.dlyContByM="";
						$scope.dlyContByA="";	
						if($scope.checkBonus){
							console.log($scope.checkBonus);
							tnc.tncBonus="yes";
						}
						if($scope.checkPenalty){
							console.log($scope.checkPenalty);
							tnc.tncPenalty="yes";
						}
						if($scope.checkDetention){
							console.log($scope.checkDetention);
							tnc.tncDetention="yes";
						}
						if($scope.checkLoad){
							console.log($scope.checkLoad);
							tnc.tncLoading="yes";
						}
						if($scope.checkUnload){
							console.log($scope.checkUnload);
							tnc.tncUnLoading="yes";
						}
						if($scope.checkTransitDay){
							console.log($scope.checkTransitDay);
							tnc.tncTransitDay="yes";
						}
						if($scope.checkStatisticalCharge){
							console.log($scope.checkStatisticalCharge);
							tnc.tncStatisticalCharge="yes";
						}
						var response = $http.post($scope.projectName+'/saveUpdatedTnc',tnc);
						response.success(function(data, status, headers, config) {
							$scope.successToast(data.resultTnc);
							$scope.dlyContFlag=true;
							$scope.showContractDetails = true;
							 $scope.isViewNo=false;
							 $scope.dlyContByM="";
							 $scope.dlyContByA="";
							 $scope.dlyContByModal=false;
							 $scope.dlyContByAuto=false;
							 $scope.contCodes=false;
							 $scope.fetch();
							 $('input[name=contCodesName]').attr('checked',false);
							 $('#ok').attr("disabled","disabled");
							 $('#dlyContByAId').attr("disabled","disabled");
							 $('#dlyContByMId').attr("disabled","disabled");
							 $('#oldDlyContId').attr("disabled","disabled");
							 $scope.getDlyContIsViewNo();
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.resultTnc);
						});	
					}else{
						console.log(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		 /*else{
			 var response = $http.post($scope.projectName+'/EditDailyContractSubmit',dlyCnt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$('div#dlyContDB').dialog('close');
						$scope.dlyContByM="";
						$scope.dlyContByA="";	
						if($scope.checkBonus){
							console.log($scope.checkBonus);
							tnc.tncBonus="yes";
						}
						if($scope.checkPenalty){
							console.log($scope.checkPenalty);
							tnc.tncPenalty="yes";
						}
						if($scope.checkDetention){
							console.log($scope.checkDetention);
							tnc.tncDetention="yes";
						}
						if($scope.checkLoad){
							console.log($scope.checkLoad);
							tnc.tncLoading="yes";
						}
						if($scope.checkUnload){
							console.log($scope.checkUnload);
							tnc.tncUnLoading="yes";
						}
						if($scope.checkTransitDay){
							console.log($scope.checkTransitDay);
							tnc.tncTransitDay="yes";
						}
						if($scope.checkStatisticalCharge){
							console.log($scope.checkStatisticalCharge);
							tnc.tncStatisticalCharge="yes";
						}
						var response = $http.post($scope.projectName+'/saveUpdatedTnc',tnc);
						response.success(function(data, status, headers, config) {
							$scope.successToast(data.resultTnc);
							$scope.dlyContFlag=true;
							$scope.showContractDetails = true;
							 $scope.isViewNo=false;
							 $scope.dlyContByM="";
							 $scope.dlyContByA="";
							 $scope.dlyContByModal=false;
							 $scope.dlyContByAuto=false;
							 $scope.contCodes=false;
							 $scope.fetch();
							 $('input[name=contCodesName]').attr('checked',false);
							 $('#ok').attr("disabled","disabled");
							 $('#dlyContByAId').attr("disabled","disabled");
							 $('#dlyContByMId').attr("disabled","disabled");
							 $scope.getDlyContIsViewNo();
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.resultTnc);
						});	
					}else{
						console.log(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		 }*/
	 }
	 
	 $scope.getDlyContCodesList = function(){
			console.log("Enetr into $scope.getDlyContCodesList function");
			var response = $http.post($scope.projectName+'/getDlyContCodesList');
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.alertToast(data.result);
					$scope.dlyContCodeList = data.list;
					$scope.getDailyContCode();	
				}else{
					console.log(data.result);
				}
		    });
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	 
	 
	 $scope.getBranchData = function(){
		   var response = $http.post($scope.projectName+'/getBranchData');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.branchList = data.list;
			   $scope.getCustomerData();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getCustomerData = function(){
		   var response = $http.post($scope.projectName+'/getCustomerData');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.customerList = data.list;
			   //$scope.getVehicleTypeCode();
			   $scope.getStationData();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getStationData = function(){
		   var response = $http.post($scope.projectName+'/getStationData');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.stationList = data.list;
			   $scope.getProductName();
			   for(var i=0;i<$scope.stationList.length;i++){
					$scope.type[i] = $scope.stationList[i].stnName+$scope.stationList[i].stnPin;
					$scope.type.push($scope.stationList[i].stnName+$scope.stationList[i].stnPin);
			   }
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	 $scope.getProductName = function(){
		   var response = $http.post($scope.projectName+'/getProductName');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.ptList = data.list;
			   $scope.getVehicleTypeCode();
			   }else{
				   console.log(data);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	 
	 $scope.getVehicleTypeCode = function(){
		   var response = $http.post($scope.projectName+'/getVehicleTypeCode');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.vtList = data.list;
			   $scope.getStateData();
			 //  $scope.getRateByKmData();
			   }else{
				console.log(data);   
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getRateByKmData = function(){
		 console.log("Enter into getRateByKmData function"+$scope.contractCode);
		   var response = $http.post($scope.projectName+'/getRateByKmData',$scope.contractCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.rbkmList = data.list;
			   console.log($scope.rbkmList[0].rbkmStartDate);
			   console.log($scope.rbkmList[0].rbkmEndDate);
			 	$scope.getPBDData();
			 	$scope.rbkmFlag=true;
			   }else{
				console.log(data);  
				$scope.getPBDData();
				$scope.rbkmFlag=false;
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getPBDData= function(){
		 console.log("Enter into getPBDData function"+$scope.contractCode);
		   var response = $http.post($scope.projectName+'/getPBDData',$scope.contractCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.pbdList = data.list;
			   $scope.pbdFlag=true; 
			   for(var i=0;i<$scope.pbdList.length;i++){
				   console.log($scope.pbdList[i].pbdPenBonDet);
				   if($scope.pbdList[i].pbdPenBonDet==="P"){
					   $scope.checkPenalty=true;
				   }else if($scope.pbdList[i].pbdPenBonDet==="B"){
					   $scope.checkBonus=true;
				   }else if($scope.pbdList[i].pbdPenBonDet==="D"){
					   $scope.checkDetention=true;
				   }  
				   }
			 //  $scope.getBranchData();
			   $scope.getCTSData();
			   }else{
				console.log(data); 
				$scope.pbdFlag=false; 
				$scope.checkPenalty=false;
				$scope.checkBonus=false;
				$scope.checkDetention=false;
				$scope.getCTSData();
				//$scope.getBranchData();
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getCTSData = function(){
		 console.log("Enter into getCTSData function"+$scope.contractCode);
		   var response = $http.post($scope.projectName+'/getCTSData',$scope.contractCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.oldCtsList = data.list;
				
			  console.log($scope.oldCtsList.length);
				   if($scope.dlyCnt.dlyContType==="Q"){
						console.log($scope.dlyCnt.dlyContType);
						$scope.ctsFlagQ=true;
					}else if($scope.dlyCnt.dlyContType==="W") {
						console.log($scope.dlyCnt.dlyContType);
						$scope.ctsFlagW=true
					}
				   
			   $scope.getBranchData();
			   }else{
				console.log(data);  
				$scope.ctsFlagQ=false;
				$scope.ctsFlagW=false;
				$scope.getBranchData();
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.removeRbkm = function(rbkm) {
		 console.log("entr into removeRbkm function");
			var response = $http.post($scope.projectName+'/deleteRbkm',rbkm);
			response.success(function(data, status, headers, config) {
				$scope.getRateByKmData();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	 
	 $scope.removeNewRbkm = function(rbkm) {
		 console.log("entr into removeRbkm function");
			var response = $http.post($scope.projectName+'/removeNewRbkm',rbkm);
			response.success(function(data, status, headers, config) {
				$scope.fetchRbkmList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
		
		$scope.removeAllRbkm = function() {
			var response = $http.post($scope.projectName+'/removeAllRbkmForEditDC');
			response.success(function(data, status, headers, config) {
				$scope.fetchRbkmList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.removeAllPbd = function() {
			var response = $http.post($scope.projectName+'/removeAllPbdForEditDC');
			response.success(function(data, status, headers, config) {
				$scope.fetchPbdList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.removePbd = function(pbd) {
	    	console.log("entr into removePbd function");
			var response = $http.post($scope.projectName+'/deletePbd',pbd);
			response.success(function(data, status, headers, config) {
				//$scope.fetchPbdList();
				 $scope.getPBDData();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	    
	    $scope.getStateData = function(){
			   var response = $http.post($scope.projectName+'/getStateData');
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
				   $scope.listState = data.list;
				   }else{
					   console.log(data);
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		   }
	    
	    $scope.savevehicletype = function(VehicleForm,vt){
			$scope.VehicleTypeFlag1=true;
			console.log("enter into saveVehicleType function--->");
			$('div#dlyContVehicleTypeDB1').dialog("destroy");
			vt.vtServiceType=vt.vtServiceType.toUpperCase();
			vt.vtVehicleType=vt.vtVehicleType.toUpperCase();
			$scope.code = vt.vtServiceType+vt.vtVehicleType;
			if($scope.vtList){
				for(var i=0;i<$scope.vtList.length;i++){
					$scope.type[i] = $scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType;
					$scope.type.push($scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType);
				}
				if(VehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(VehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(VehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(VehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleType',vt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getVehicleTypeCode();
						$scope.vt="";
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}else{
				if(VehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(VehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(VehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(VehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleType',vt);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.successToast(data.result);
						$scope.getVehicleTypeCode();
						$scope.vt="";	
					}else{
						$scope.errorToast(data.result);	
					}			
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}			
	    }
	      
	    $scope.saveNewStation=function(StationForm,station){
			console.log("Entered into Submit function");
			$scope.station.stnName = station.stnName.toUpperCase();
			$scope.codeState = station.stnName+station.stnPin;
			console.log($scope.codeState);
			if(StationForm.$invalid){
				if(StationForm.stnName.$invalid){
					$scope.alertToast("Please enter station name between 3-40 characters...");
					$scope.station.stnName="";
				}else if(StationForm.stnDistrict.$invalid){
					$scope.alertToast("Please enter station district between 3-40 characters...");
					$scope.station.stnDistrict="";
				}else if(StationForm.stateCode.$invalid){
					$scope.alertToast("Please enter state code...");
				}else if(StationForm.stnPin.$invalid){
					$scope.alertToast("Please enter station pin of 6 digits...");
					$scope.station.stnPin="";
				}
			}else{
				if($.inArray($scope.codeState,$scope.type)!==-1){
					$scope.alertToast("Station already exists...");
					$scope.station.stnName="";
					$scope.station.stnDistrict="";
					$scope.station.stateCode="";
					$scope.station.stnPin="";
				}else{
					var response = $http.post($scope.projectName+'/submitStation',station);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
							$scope.station.stnName="";
							$scope.station.stnDistrict="";
							$scope.station.stateCode="";
							$scope.station.stnPin="";
							$('input[name=stateName]').attr('checked',false);
							$scope.getStationData();
							$('div#addStation').dialog('close');
						}else{
							console.log(data);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});
				}
			}	
		}
		
	    
	    $scope.saveContToStnW = function(ContToStationFormW,cts){
			console.log("Enter into saveContToStnW function"+ContToStationFormW.$invalid);
			$('div#contToStationW').dialog("destroy");
			$scope.contToStationFlagW = true;
			$scope.ctsflagW=true;
			
			if($scope.ctsList.length>0){
				console.log("if list exists"+$scope.ctsList.length);
				if(ContToStationFormW.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormW.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				} else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else {
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					$scope.ctsTemp= cts.ctsToStn+cts.ctsVehicleType;
					
					for(var i=0;i<$scope.ctsList.length;i++){
						console.log($scope.ctsList[i].ctsToStn);
						$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType;
						$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType);
					}
					if($.inArray($scope.ctsTemp,$scope.type)!==-1){
						$scope.alertToast("Already exists");
						cts.ctsToStn="";
						cts.ctsToStnTemp="";
						cts.ctsRate="";
						cts.ctsFromWt="";
						cts.ctsToWt="";
						cts.ctsVehicleType="";
					}else{
						var response = $http.post($scope.projectName+'/addNewCTS', cts);	   
						response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}			
					}
			}else{
				console.log("if list does not exists");
				if(ContToStationFormW.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormW.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				} else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					var response = $http.post($scope.projectName+'/addNewCTS',cts);	   
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();	
								$scope.ContToStnFlag=true;
								cts.ctsToStn="";
								cts.ctsToStnTemp="";
								cts.ctsRate="";
								cts.ctsFromWt="";
								cts.ctsToWt="";
								cts.ctsVehicleType="";
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}	
				}
			}
	    
	    
	    $scope.saveContToStnQ = function(ContToStationFormQ,cts){
			console.log("Enter into saveContToStnQ function"+ContToStationFormQ.$invalid);
			$('div#contToStationQ').dialog("destroy");
			$scope.contToStationFlagQ = true;
			$scope.ctsflagQ=true;
			//$scope.ContToStnFlag=true;
		
			if($scope.ctsList.length>0){
				console.log("if list exists"+$scope.ctsList.length);
				if(ContToStationFormQ.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormQ.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				}else if(ContToStationFormQ.ctsProductType.$invalid){
					$scope.alertToast("Please enter Product Type");
				}else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsProductType=cts.ctsProductType.toUpperCase();
					$scope.ctsTemp= cts.ctsToStn+cts.ctsProductType;
					
					for(var i=0;i<$scope.ctsList.length;i++){
						console.log($scope.ctsList[i].ctsToStn);
						$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType;
						$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType);
					}
					if($.inArray($scope.ctsTemp,$scope.type)!==-1){
						$scope.alertToast("Already exists");
						cts.ctsToStn="";
						cts.ctsToStnTemp="";
						cts.ctsRate="";
						cts.ctsFromWt="";
						cts.ctsToWt="";
						cts.ctsVehicleType="";
					}else{
						var response = $http.post($scope.projectName+'/addNewCTS', cts);	   
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									cts.ctsProductType="";
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}
					} 
			}else{
				console.log("if list does not exists");
				if(ContToStationFormQ.ctsToStnTemp.$invalid){
					$scope.alertToast("Please enter To Station");
				}else if(ContToStationFormQ.ctsVehicleType.$invalid){
					$scope.alertToast("Please enter Vehicle Type");
				}else if(ContToStationFormQ.ctsProductType.$invalid){
					$scope.alertToast("Please enter Product Type");
				}else if(cts.ctsFromWt>cts.ctsToWt){
					$scope.alertToast("From Weight cannot be greater than To Weight");
				}else{
					cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
					cts.ctsToStn=cts.ctsToStn.toUpperCase();
					cts.ctsProductType=cts.ctsProductType.toUpperCase();
					var response = $http.post($scope.projectName+'/addNewCTS', cts);	   
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								console.log("from server ---->"+data.result);
								$scope.fetchCTSList();	
								$scope.ContToStnFlag=true;
								cts.ctsToStn="";
								cts.ctsToStnTemp="";
								cts.ctsRate="";
								cts.ctsFromWt="";
								cts.ctsToWt="";
								cts.ctsVehicleType="";
								cts.ctsProductType="";
								$('input[name=toStations]').attr('checked',false);
								$('input[name=VTName]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
					}	
				}
			}
	    
	    $scope.fetchCTSList = function(){
			console.log("Entr into fetchCTSListForEditContract function");
			var response = $http.get($scope.projectName+'/fetchCTSListForEditContract');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.ctsList = data.list;
					console.log($scope.dlyCnt.dlyContType);
					if($scope.dlyCnt.dlyContType==='W'){
						$scope.newCtsFlagW=true;	 
					}else if($scope.dlyCnt.dlyContType==='Q'){
						$scope.newCtsFlagQ=true;	 
					}
				}else{
					console.log(data.result);
					$scope.ctsList = data.list;
					$scope.newCtsFlagW=false;
					$scope.newCtsFlagQ=false;
//					 if($scope.dlyCnt.dlyContType==='W' || $scope.dlyCnt.dlyContType==='Q'){
//						 console.log($scope.dlyCnt.dlyContType); 
//						 $scope.ContToStnFlag=false;
//					 }
				}		
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
		$scope.removeNewCTS = function(cts) {
			console.log("Entr into removeNewCTS function");
			var response = $http.post($scope.projectName+'/removeNewCTS',cts);
			response.success(function(data, status, headers, config) {
				$scope.fetchCTSList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
		$scope.removeAllCTS = function() {
			console.log("Entr into removeAllCts function");
			var response = $http.post($scope.projectName+'/removeAllCTSForEditDC');
			response.success(function(data, status, headers, config) {
				$scope.fetchCTSList();
				console.log(data.result);
				$scope.ctsflagW=false;
				$scope.ctsflagQ=false;
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	
		 $scope.removeCTS = function(cts) {
			 console.log("entr into removeCTS function");
				var response = $http.post($scope.projectName+'/deleteCTS',cts);
				response.success(function(data, status, headers, config) {
					$scope.getCTSData();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		    }
		 
		 $scope.sendMetricType=function(metricType){
				console.log(metricType);
				if(metricType==="" || metricType===null || angular.isUndefined(metricType)){
					$scope.alertToast("Please enter Metric type");
				}else{
					var response = $http.post($scope.projectName+'/sendMetricTypeForEditDC',metricType);
					response.success(function(data, status, headers, config) {
						$scope.alertToast(data.result);
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}
		 
	    $scope.fetch=function(){
	    	$scope.removeAllRbkm();
//		    $scope.removeAllPbd();	
//			$scope.removeAllCTS();
	    }
	    
	   
	    
	    if($scope.adminLogin === true || $scope.superAdminLogin === true){
	    	 $scope.fetch();
		}else if($scope.logoutStatus === true){
				 $location.path("/");
		}else{
				 console.log("****************");
		}
	 
}]);
