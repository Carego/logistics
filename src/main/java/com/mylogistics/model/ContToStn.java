package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;



@Entity
@Table(name = "conttostn")
public class ContToStn {
	
	@Id
	@Column(name="ctsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ctsId;
	
	@Column(name="ctsToStn")
	private String ctsToStn;
	
	@Column(name="ctsRate" ,  columnDefinition="Decimal(14,5)")
	private double ctsRate;

	@Column(name="ctsContCode")
	private String ctsContCode;
	
	@Column(name="ctsVehicleType")
	private String ctsVehicleType;
	
	@Column(name="ctsFromWt")
	private double ctsFromWt;
	
	@Column(name="ctsToWt")
	private double ctsToWt;
	
	@Column(name="ctsProductType")
	private String ctsProductType;
	
	@Column(name="ctsAdditionalRate")
	private double ctsAdditionalRate;
	
	@Column(name="ctsToTransitDay")
	private int ctsToTransitDay;

	@Column(name="ctsToStatChg")
	private double ctsToStatChg;
	
	@Column(name="ctsToLoad")
	private double ctsToLoad;
	
	@Column(name="ctsToUnLoad")
	private double ctsToUnLoad;
	
	@Column(name="ctsFrDt")
	private Date ctsFrDt;
	
	@Column(name="ctsToDt")
	private Date ctsToDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="ctsUpdate", columnDefinition="boolean default false")
	private boolean ctsUpdate;
	
	@Column(name="ctsUpdateUserId", columnDefinition="int default 0")
	private int ctsUpdateUserId;
	
	@Transient
	private String extendDate;

	public int getCtsToTransitDay() {
		return ctsToTransitDay;
	}

	public void setCtsToTransitDay(int ctsToTransitDay) {
		this.ctsToTransitDay = ctsToTransitDay;
	}

	public double getCtsToStatChg() {
		return ctsToStatChg;
	}

	public void setCtsToStatChg(double ctsToStatChg) {
		this.ctsToStatChg = ctsToStatChg;
	}

	public double getCtsToLoad() {
		return ctsToLoad;
	}

	public void setCtsToLoad(double ctsToLoad) {
		this.ctsToLoad = ctsToLoad;
	}

	public double getCtsToUnLoad() {
		return ctsToUnLoad;
	}

	public void setCtsToUnLoad(double ctsToUnLoad) {
		this.ctsToUnLoad = ctsToUnLoad;
	}

	public int getCtsId() {
		return ctsId;
	}

	public void setCtsId(int ctsId) {
		this.ctsId = ctsId;
	}

	public String getCtsToStn() {
		return ctsToStn;
	}

	public void setCtsToStn(String ctsToStn) {
		this.ctsToStn = ctsToStn;
	}

	public double getCtsRate() {
		return ctsRate;
	}

	public void setCtsRate(double ctsRate) {
		this.ctsRate = ctsRate;
	}

	public String getCtsContCode() {
		return ctsContCode;
	}

	public void setCtsContCode(String ctsContCode) {
		this.ctsContCode = ctsContCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	public String getCtsVehicleType() {
		return ctsVehicleType;
	}

	public void setCtsVehicleType(String ctsVehicleType) {
		this.ctsVehicleType = ctsVehicleType;
	}

	public double getCtsFromWt() {
		return ctsFromWt;
	}

	public void setCtsFromWt(double ctsFromWt) {
		this.ctsFromWt = ctsFromWt;
	}

	public double getCtsToWt() {
		return ctsToWt;
	}

	public void setCtsToWt(double ctsToWt) {
		this.ctsToWt = ctsToWt;
	}
	
	public String getCtsProductType() {
		return ctsProductType;
	}

	public void setCtsProductType(String ctsProductType) {
		this.ctsProductType = ctsProductType;
	}

	public double getCtsAdditionalRate() {
		return ctsAdditionalRate;
	}

	public void setCtsAdditionalRate(double ctsAdditionalRate) {
		this.ctsAdditionalRate = ctsAdditionalRate;
	}

	public Date getCtsFrDt() {
		return ctsFrDt;
	}

	public void setCtsFrDt(Date ctsFrDt) {
		this.ctsFrDt = ctsFrDt;
	}

	public Date getCtsToDt() {
		return ctsToDt;
	}

	public void setCtsToDt(Date ctsToDt) {
		this.ctsToDt = ctsToDt;
	}

	public boolean isCtsUpdate() {
		return ctsUpdate;
	}

	public void setCtsUpdate(boolean ctsUpdate) {
		this.ctsUpdate = ctsUpdate;
	}

	public int getCtsUpdateUserId() {
		return ctsUpdateUserId;
	}

	public void setCtsUpdateUserId(int ctsUpdateUserId) {
		this.ctsUpdateUserId = ctsUpdateUserId;
	}

	public String getExtendDate() {
		return extendDate;
	}

	public void setExtendDate(String extendDate) {
		this.extendDate = extendDate;
	}
	
	
}