<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		       	
		    <div class="row">
	     		<div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       			<label for="code">Pay To</label>	
	       		</div>
	       		
	       		<div class="col s4 input-field">
					<input type ="text" name="phNoName" id="phNoId" ng-model="phNo" ng-click="openPhNoDB()" disabled="disabled" readonly ng-required="true">
					<label>Select Phone No.</label>
				</div>
	   		</div>
	  
	        <div class="row"> 		
				 <div ng-show="tmAndStmList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Telephone Voucher Details</caption>
						<tr class="rowclr">
							<th class="colclr">PhNo</th>
							<th class="colclr">Pay Amt</th>
							<th class="colclr">Bill FrDt</th>
							<th class="colclr">Bill ToDt</th>
							<th class="colclr">Staff Code</th>
							<th class="colclr">Branch Code</th>
							<th class="colclr">Sms Amt</th>
							<th class="colclr">Caller Tune Amt</th>
							<th class="colclr">Download Amt</th>
							<th class="colclr">Game Amt</th>
							<th class="colclr">Pesonal Call</th>
							<th class="colclr">Roaming Amt</th>
							<th class="colclr">Isd Amt</th>
							<th class="colclr">Total Ded</th>
						</tr>
						<tr class="tbl" ng-repeat="tmAndStm in tmAndStmList">
							<td class="rowcel">{{tmAndStm.tMstr.tmPhNo}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmPayAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmBillFrDt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmBillToDt}}</td>
							<td class="rowcel">{{tmAndStm.tMstr.employee.empFaCode}}</td>
							<td class="rowcel">{{tmAndStm.tMstr.branch.branchFaCode}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmSmsDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmCTDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmDDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmGDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmPCDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmRDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmIsdDedAmt}}</td>
							<td class="rowcel">{{tmAndStm.stMstr.stmTotDedAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeStmV($index,tmAndStm)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="13">Total Amount</td>
							<td class="rowcel">{{stmTotDedAmtSum}}</td>
							<td class="rowcel"></td>										
						</tr>
					</table>
				</div>
			</div>	
	
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<!-- <div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div> -->
	   		 </div>
		  </form>
    </div>
   
   <div id ="phoneNoDB" ng-hide="phoneNoDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Phone Number</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="telM in telMList | filter:filterTextbox">
		 	  <td><input type="radio"  name="telM"  value="{{ telM }}" ng-model="telPNo" ng-click="savePhoneNo(telM)"></td>
              <td>{{ telM.tmPhNo }}</td>
              
          </tr>
      </table> 
	</div>  
	
	
	<div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="telVoucherDB" ng-hide="telVoucherDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="tmVoucherForm" ng-submit="submitTMVoucher(tmVoucherForm)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="payAmt" id="payAmtId" ng-model="tm_stm.stMstr.stmPayAmt" step="0.01" min="0.00" ng-required="true">
					<label>Total Pay Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="date" name="billFrDt" id="billFrDtId" ng-model="tm_stm.stMstr.stmBillFrDt" step="0.01" min="0.00">
					<label>Bill From Date</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="date" name="billToDt" id="billToDtId" ng-model="tm_stm.stMstr.stmBillToDt" step="0.01" min="0.00">
					<label>Bill To Date</label>
				</div>
	     </div>
	     
	      <div class="row">
			    <div class="col s4 input-field">
					<input type ="number" name="stmSmsDedAmt" id="stmSmsDedAmtId" ng-model="tm_stm.stMstr.stmSmsDedAmt" step="0.01" min="0.00">
					<label>Sms Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="stmCTDedAmt" id="stmCTDedAmtId" ng-model="tm_stm.stMstr.stmCTDedAmt" step="0.01" min="0.00">
					<label>Caller Tune Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="stmDDedAmt" id="stmDDedAmtId" ng-model="tm_stm.stMstr.stmDDedAmt" step="0.01" min="0.00">
					<label>Downloading Amount</label>
				</div>
	     </div>
	     
	     <div class="row">
			    <div class="col s4 input-field">
					<input type ="number" name="stmGDedAmt" id="stmGDedAmtId" ng-model="tm_stm.stMstr.stmGDedAmt" step="0.01" min="0.00">
					<label>Game Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="stmPCDedAmt" id="stmPCDedAmtId" ng-model="tm_stm.stMstr.stmPCDedAmt" step="0.01" min="0.00">
					<label>Personal Call Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="stmRDedAmt" id="stmRDedAmtId" ng-model="tm_stm.stMstr.stmRDedAmt" step="0.01" min="0.00">
					<label>Roaming Amount</label>
				</div>
	     </div>
	     
	     <div class="row">
			    <div class="col s4 input-field">
					<input type ="number" name="stmIsdDedAmt" id="stmIsdDedAmtId" ng-model="tm_stm.stMstr.stmIsdDedAmt" step="0.01" min="0.00">
					<label>ISD Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="stmTotDedAmt" id="stmTotDedAmtId" ng-model="tm_stm.stMstr.stmTotDedAmt" step="0.01" min="0.00" ng-click="calTotDedAmt()" readonly ng-required="true">
					<label>Total Deduction Amount</label>
				</div>
	     </div>
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
   
   
   <div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
   
   
</div>
