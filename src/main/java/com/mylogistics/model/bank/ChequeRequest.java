package com.mylogistics.model.bank;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.mylogistics.model.Branch;

@Entity
@Table(name = "chequerequest")
public class ChequeRequest {

	@Id
	@Column(name="cReqId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cReqId;
	
	@Column(name="cReqRefNo")
	private String cReqRefNo;
	
	@Column(name="cReqDt")
	private Date cReqDt;
	
	@Column(name="cReqNOBook")
	private int cReqNOBook;
	
	@Column(name="cReqNOChqPerBook")
	private int cReqNOChqPerBook;
	
	@Column(name="cReqType")
	private char cReqType;
	
	@Column(name="cReqPrintType")
	private char cReqPrintType;
	
	@Column(name="cReqIsReceived")
	private boolean cReqIsReceived;
	
	@Column(name="cReqIsCancelled")
	private boolean cReqIsCancelled;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "branchId")
	private Branch branch;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "bnkId")
	private BankMstr bankMstr;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getcReqId() {
		return cReqId;
	}

	public void setcReqId(int cReqId) {
		this.cReqId = cReqId;
	}

	public String getcReqRefNo() {
		return cReqRefNo;
	}

	public void setcReqRefNo(String cReqRefNo) {
		this.cReqRefNo = cReqRefNo;
	}

	public Date getcReqDt() {
		return cReqDt;
	}

	public void setcReqDt(Date cReqDt) {
		this.cReqDt = cReqDt;
	}

	public int getcReqNOBook() {
		return cReqNOBook;
	}

	public void setcReqNOBook(int cReqNOBook) {
		this.cReqNOBook = cReqNOBook;
	}

	public int getcReqNOChqPerBook() {
		return cReqNOChqPerBook;
	}

	public void setcReqNOChqPerBook(int cReqNOChqPerBook) {
		this.cReqNOChqPerBook = cReqNOChqPerBook;
	}

	public char getcReqType() {
		return cReqType;
	}

	public void setcReqType(char cReqType) {
		this.cReqType = cReqType;
	}

	public char getcReqPrintType() {
		return cReqPrintType;
	}

	public void setcReqPrintType(char cReqPrintType) {
		this.cReqPrintType = cReqPrintType;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public boolean iscReqIsReceived() {
		return cReqIsReceived;
	}

	public void setcReqIsReceived(boolean cReqIsReceived) {
		this.cReqIsReceived = cReqIsReceived;
	}

	public boolean iscReqIsCancelled() {
		return cReqIsCancelled;
	}

	public void setcReqIsCancelled(boolean cReqIsCancelled) {
		this.cReqIsCancelled = cReqIsCancelled;
	}
}
