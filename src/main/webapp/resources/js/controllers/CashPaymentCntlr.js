'use strict';

var app = angular.module('application');

app.controller('CashPaymentCntlr',['$scope','$location','$http','$filter',
                                  function($scope,$location,$http,$filter){
	
	$scope.vs = {};
	$scope.faName = "";
	$scope.faMList = [];
	$scope.brFaList = [];
	$scope.actFaMList = [];
	
	$scope.amtSum=0;
	$scope.tdsAmtSum=0;
	$scope.faCodeFlag = true;
	$scope.saveVsFlag = true;
	$scope.tdsCodeFlag = true;
	$scope.cashVoucherDBFlag = true;
	$scope.brDBFlag = true;
	$scope.loadingFlag = false; 
	 
    var max = 15;
	
	$('#amtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	 $('#amtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	 
	 
	 $('#tdsAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
     });
	
	 $('#tdsAmtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	 
	 
	 
	 
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getCashPayDetails');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.CASH_PAYMENT;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.getFaCode = function(){		
		console.log("enter into getFaCode function");
		$scope.loadingFlag = true;
		var response = $http.post($scope.projectName+'/getFaCodeFrCPByName', $scope.faName);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.loadingFlag = false;
				$scope.faMList = data.faMList;
				console.log("size of $scope.faMList =" +$scope.faMList.length);
				$scope.getTdsCode();
			}else{
				$scope.loadingFlag = false;
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.loadingFlag = false;
			console.log(data);
		});
	}
	
	
	$scope.getTdsCode = function(){
		console.log("enter into getTdsCode function");
		var response = $http.post($scope.projectName+'/getTdsCode');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.fsMTdsList = data.list;
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.selectTds = function(){
		console.log("enter into selectTds function");
		$scope.tdsCodeFlag = false;
		$('div#tdsCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#tdsCodeDB').dialog('open');
	}
	
	
	$scope.saveTdsCode = function(fsMTds){
		console.log("enter into saveTdsCode function");
		$scope.tdsCode = fsMTds.faMfaCode;
		$scope.tdsCodeFlag = true;
		$('div#tdsCodeDB').dialog('close');
	}
	
	
	$scope.openFaCodeDB = function(keyCode){
		
		if(keyCode === 8)
			return;
		
		var len = ($scope.vs.faCode).length;		
		if(len < 3)
			return;
		
		$scope.faName = $scope.vs.faCode;
		$scope.vs.faCode = "";
		$scope.getFaCode();
		
		console.log("enter into openFaCodeDB function");
		$scope.faCodeFlag = false;
		$('div#faCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#faCodeDB').dialog('open');
	}
	
	
	$scope.saveFaCode = function(faM){
		$scope.faCodeFlag = true;
		$('div#faCodeDB').dialog('close');
		console.log("enter into saveFaCode function");
		$scope.vs.faCode = faM.faMfaCode;
		$scope.faCode = $scope.vs.faCode;
		$scope.brFaList = [];
		var decided = $scope.vs.faCode.substring(0,2);
	    console.log("decided === >" +decided);
	     
	     if(decided === "03"){
	    	var response = $http.post($scope.projectName+'/getCustBrFrCPV',$scope.faCode);
			response.success(function(data, status, headers, config){
				//$scope.custBrFaList = data.list;
				$scope.brFaList = data.list;
				console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[0]);
				if($scope.brFaList.length > 0){
					//$('#custBrId').removeAttr("disabled");
					$('#brId').removeAttr("disabled");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});	
	     }else if(decided === "02"){
		    	var response = $http.post($scope.projectName+'/getEmpBrFrCPV',$scope.faCode);
				response.success(function(data, status, headers, config){
					//$scope.custBrFaList = data.list;
					$scope.brFaList = data.list;
					console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[0]);
					if($scope.brFaList.length > 0){
						//$('#custBrId').removeAttr("disabled");
						$('#brId').removeAttr("disabled");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});	
		 }else if(decided === "01"){
	    	 
	     }else{
	    	 var response = $http.post($scope.projectName+'/getAllBrFrCPV',$scope.faCode);
				response.success(function(data, status, headers, config){
					//$scope.custBrFaList = data.list;
					$scope.brFaList = data.list;
					console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[0]);
					if($scope.brFaList.length > 0){
						//$('#custBrId').removeAttr("disabled");
						$('#brId').removeAttr("disabled");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});	
	     }
		
		 $scope.brCode = "";
	     $scope.tdsCode = "";
			
	     console.log("************************");
	     $scope.cashVoucherDBFlag = false;
		 $('div#cashVoucherDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Amount for ("+$scope.faCode+")" ,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.cashVoucherDBFlag = true;
			    }
				});
			
		$('div#cashVoucherDB').dialog('open');
		
	}
	
	
	$scope.selectBrh = function(){
		console.log("enter into selectBrh function");
		
		
		 $scope.brDBFlag = false;
		 $('div#brDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Branch FACode",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.brDBFlag = true;
			    }
				});
			
		$('div#brDB').dialog('open');
	}
	
	
	$scope.saveBrFA = function(brFa){
		console.log("enter into saveBrFA function");
		$('div#brDB').dialog('close');
		$scope.brCode = brFa;
	}
	
	
	$scope.submitCSPVoucher = function(cashVoucherForm){
		console.log("enter into submitCSPVoucher funciton = "+cashVoucherForm.$invalid);
		var sub = $scope.faCode.substr(0,2);
		console.log("sub = "+sub);
		if(cashVoucherForm.$invalid){
			$scope.alertToast("please fill correct from");
		}else if(sub !== "01"){
			if(angular.isUndefined($scope.brCode) || $scope.brCode === "" || $scope.brCode === null){
				$scope.alertToast("Please select Branch Code");
			}else{
				$('div#cashVoucherDB').dialog('close');
				$scope.amtSum=$scope.amtSum+$scope.amt;
				if($scope.tdsAmt>0){
					$scope.tdsAmtSum=$scope.tdsAmtSum+$scope.tdsAmt;
					}else{
						$scope.tdsAmt=0;
						$scope.tdsAmtSum=$scope.tdsAmtSum+$scope.tdsAmt;
					}
				
				if($scope.amt > 0){
					var faPay = {
							"faCode"      : $scope.faCode,
							"amt"	      : $scope.amt,
							"desBrFaCode" : $scope.brCode,
							"tdsCode"     : $scope.tdsCode,
							"tdsAmt"      : $scope.tdsAmt
					};
					
					$scope.actFaMList.push(faPay);
				}else{
					$scope.alertToast("please enter correct amount");
				}
			}
		}else{
			$('div#cashVoucherDB').dialog('close');
			
			if($scope.amt > 0){
				var faPay = {
						"faCode"      : $scope.faCode,
						"amt"	      : $scope.amt,
						"desBrFaCode" : $scope.brCode,
						"tdsCode"     : $scope.tdsCode,
						"tdsAmt"      : $scope.tdsAmt
				};
				
				$scope.actFaMList.push(faPay);
			}else{
				$scope.alertToast("please enter correct amount");
			}
		}
	}
	
	$scope.removeCSPV = function(index,actFaM){
		console.log("enter into removeCSPV function");
		$scope.amtSum=$scope.amtSum-actFaM.amt;
		$scope.tdsAmtSum=$scope.tdsAmtSum-actFaM.tdsAmt;
		$scope.actFaMList.splice(index,1);
	}
	
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("enter into voucherSubmit function--->"+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			if($scope.vs.amount <= 0){
				$scope.alertToast("please enter a valid amount");
			}else if(!angular.isUndefined($scope.vs.tdsCode)){
					if(angular.isUndefined($scope.vs.tdsAmt)){
						$scope.alertToast("please enter TDS Amount");
					}else{
						if($scope.dateTemp<="2016-03-31"){
							$scope.alertToast("please enter CS Date greater than 2016-03-31");
						}else{
						console.log("final submittion");
						$scope.saveVsFlag = false;
				    	$('div#saveVsDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						    }
							});
						$('div#saveVsDB').dialog('open');
						}
					}
			}else if(!angular.isUndefined($scope.vs.tdsAmt)){
				if(angular.isUndefined($scope.vs.tdsCode)){
					$scope.alertToast("please enter TDS Code");
				}else{
					if($scope.dateTemp<="2016-03-31"){
						$scope.alertToast("please enter CS Date greater than 2016-03-31");
					}else{
					console.log("final submittion");
					$scope.saveVsFlag = false;
			    	$('div#saveVsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					    }
						});
					$('div#saveVsDB').dialog('open');
					}
				}
			}else{
				if($scope.dateTemp<="2016-03-31"){
					$scope.alertToast("please enter CS Date greater than 2016-03-31");
				}else{
				console.log("final submittion");
				$scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
				}
			}
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$('div#saveVsDB').dialog('close');
		$scope.vs.subFList = $scope.actFaMList;
		$scope.actFaMList = []; 
		$scope.amtSum=0;
		$scope.tdsAmtSum=0;
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitCashP',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("success");
				$('#saveId').removeAttr("disabled");
				$scope.alertToast("voucher generated successfully");
				$scope.vs.vhNo = data.vhNo;
				$scope.vs.tvNo = data.tvNo;
				
				$scope.vs = {};
				$scope.cNo = "";
				$scope.getVoucherDetails();
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
		
	}
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);