package com.mylogistics.DAO;

import java.sql.Blob;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.PanService;


@Repository
public interface OwnerDAO {
	
		
	//Sanjana: retrieves owner from database on the basis of entered owner code
	public List<Owner> getOwnerList(String ownCode);
	
	//Sanjana: updates the owner
	public int updateOwner(Owner owner);
	
	//Sanjana: get the list of ownerCode
	public List<String> getOwnerCode();
	
	public List<Owner> getOwner();
	
	public Owner getOwnerData(int id);
	
	//Navneet: get the list of ownerCode based on isBnkDetails
	public List<Map<String, String>> getOwnCodesIsBnkD(String ownCode);
	
	public List<Map<String,String>> getOwnNCF();
	
	public List<Map<String,String>> getOwnNCFByName(String ownName);
	
	public Owner getOwnByCode(String ownCode);
	
	public int saveOwnAndImg(Owner owner , OwnerImg ownerImg, Blob panImg, Blob decImg);
	
	public List<Map<String, Object>> getOwnNameCodeId();
	
	public Owner getOwnByFaCode(String faCode);
	
	public int checkOwnPan(String ownCode);
	
	public Map<String, Object> saveOwnPan(PanService panService, OwnerImg ownerImg, float ownIntRate, boolean isPanImg, boolean isDecImg, Blob panImg, Blob decImg);
	
	public Blob getOwnPanImg(int ownImgId);
	
	public List<Map<String, Object>> getOwnNameCodeIdFa();
	
	public boolean isOwnBrkHasPan(int ownId, int brkId);
	
	public Boolean isOwnerAvailable(String panNo);
	
	public Boolean isOwnerAvailable(Session session,String panNo);
	
	public List<Owner> getOwnerByNameCode(Session session, User user, String ownNameCode, List<String> proList);

	public List<Map<String, Object>> getOwnNamePhCodeIdFa();

	public List<Map<String, Object>> getOwnNameCodeByPanNo(String panNo);

	public Boolean isOwnerAvailable(String ownName, String branchCode);
	
	public int saveOwner(Session session,Owner owner,Blob panImg,Blob decImg,Blob chqImg);
	
	public Map<String,Object> getOwnerDetail(int ownId);
	
	public int updateOwner(Session session,Owner owner,Blob panImg,Blob decImg,Blob chqImg);

	public void updateOwner(Session session, Owner owner);

	public void updateOwnImg(Session session, OwnerImg ownImg);

	public int saveOwnImg(Session session, OwnerImg ownImg);

	public List<Map<String, Object>> getOwnNameCodeIdByName(String ownName);

	public Map<String, String> bnkInfoInValid(int ownId);

	public Map<String, String> bnkInfoValid(Integer ownId);

	public List<Owner> getOwnFrBnkVerify();
	
	public List<Owner> getOwnerInvalidAcc();

	public Map<String, String> uploadChqImg(byte[] fileInBytes, int ownId);
	
	public Map<String, String> uploadPanImg(byte[] fileInBytes, int ownId);
	
	public Map<String, String> uploadDecImg(byte[] fileInBytes, int ownId);

	public Map<String, String> bnkInfoUpdate(Owner own);
	
}
