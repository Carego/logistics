<div ng-show="operatorLogin || superAdminLogin">

	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:12px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #26A69A;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:12px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>

	<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img"/>
		</div>
	</div>

	<div class="row">
		<form name="stockAndOsRptForm" ng-submit="submitStockAndOsRpt(stockAndOsRptForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly>
		       	<label for="code">Branch</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="cust.custName" ng-click="openCustDB()" readonly >
		       	<label for="code">Customer</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="date" id="upToDtId" name ="upToDtName" ng-model="upToDt" required="required">
		       	<label for="code">Date</label>	
		       	</div>
		    </div>
			
			<div class="row">
   			 	<div class="col s12 center">
   			 		<input type="submit" id="submitId" value="Submit">
   			 		<input type="button" id="clearBtnId" value="CLEAR" ng-click="clearAll()">
   			 		<input type="button" id="printXlsId" value="printXls" ng-click="printXls()" disabled="disabled">
   			 		<input type="button" id="emailId" value="E-Mail" ng-click="mail()" disabled="disabled">   			 		
   			 	</div>
      		</div>
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="custDB" ng-hide="custDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Customer Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="cust in custList | filter:filterTextbox2">
		 	  <td><input type="radio" name="cust" value="{{ cust }}" ng-click="saveCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div ng-if="stockAndOsRptList.length>0" id="exportable">
		<table class="table">
 	    	<caption class="coltag tblrow">Stock And O/S Report (Up to Date: {{upToDt | date:'dd/MM/yy'}} )</caption> 
	 	    <tr>
 	  	  	  <th class="rowcelheader">S.No</th>
 	  	  	  <th class="rowcelheader">CustCode</th>
 	  	  	  <th class="rowcelheader">CustName</th>
 	  	  	  <th class="rowcelheader">0-30 days</th>
 	  	  	  <th class="rowcelheader">31-60 days</th>
 	  	  	  <th class="rowcelheader">61-90 days</th>
 	  	  	  <th class="rowcelheader">91-120 days</th>
 	  	  	  <th class="rowcelheader">121 Above days</th>
 	  	  	  <th class="rowcelheader">On A/C</th>
 	  	  	  <th class="rowcelheader">Total O/S</th>
 	  	  	  <th class="rowcelheader">StkUpto30</th>
 	  	  	  <th class="rowcelheader">StkAbv30</th>
 	  	  	  <th class="rowcelheader">Total Stk</th>
 	  	  </tr>
 	  	  <tr ng-repeat="stockAndOsRpt in stockAndOsRptList">
		 	  <td class="rowcelwhite">{{$index+1}}</td>
		 	  <td class="rowcelwhite">{{stockAndOsRpt.custFaCode}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.custName}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.OS0_30 | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.OS31_60 | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.OS61_90 | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.OS91_120 | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.OS121_more | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.onAC | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.OSTotal | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.stkUpTo30 | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.stkAbove30 | number : 2}}</td>
              <td class="rowcelwhite">{{stockAndOsRpt.stkTotal | number : 2}}</td>
          </tr>
          <tr>
 	  	  	  <th class="rowcelheader" colspan="3">TOTAL</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.OS0_30Total | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.OS31_60Total | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.OS61_90Total | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.OS91_120Total | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.OS121_moreTotal | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.onACTotal | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.OSTotalFinal | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.stkUpTo30Total | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.stkAbove30Total | number : 2}}</th>
 	  	  	  <th class="rowcelheader">{{stockAndOsRptTotals.stkTotalFinal | number : 2}}</th>
 	  	  </tr>
      </table> 
	</div>
	
	

</div>
	