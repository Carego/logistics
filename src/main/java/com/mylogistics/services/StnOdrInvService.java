package com.mylogistics.services;

import com.mylogistics.model.Stationary;


public class StnOdrInvService {

	private Stationary stationary;
	//private HBO_StnNotification hbo_StnNotification;
	private String cnmtSeq;
	private String chlnSeq;
	private String sedrSeq;
	
	public Stationary getStationary() {
		return stationary;
	}
	public void setStationary(Stationary stationary) {
		this.stationary = stationary;
	}
	/*public HBO_StnNotification getHbo_StnNotification() {
		return hbo_StnNotification;
	}
	public void setHbo_StnNotification(HBO_StnNotification hbo_StnNotification) {
		this.hbo_StnNotification = hbo_StnNotification;
	}*/
	public String getCnmtSeq() {
		return cnmtSeq;
	}
	public void setCnmtSeq(String cnmtSeq) {
		this.cnmtSeq = cnmtSeq;
	}
	public String getChlnSeq() {
		return chlnSeq;
	}
	public void setChlnSeq(String chlnSeq) {
		this.chlnSeq = chlnSeq;
	}
	public String getSedrSeq() {
		return sedrSeq;
	}
	public void setSedrSeq(String sedrSeq) {
		this.sedrSeq = sedrSeq;
	}
	
	
}
