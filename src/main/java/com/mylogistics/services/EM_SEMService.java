package com.mylogistics.services;

import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.SubElectricityMstr;

public class EM_SEMService {

	private ElectricityMstr elctMstr;
	
	private SubElectricityMstr subElectMstr;

	public ElectricityMstr getElctMstr() {
		return elctMstr;
	}

	public void setElctMstr(ElectricityMstr elctMstr) {
		this.elctMstr = elctMstr;
	}

	public SubElectricityMstr getSubElectMstr() {
		return subElectMstr;
	}

	public void setSubElectMstr(SubElectricityMstr subElectMstr) {
		this.subElectMstr = subElectMstr;
	}
}
