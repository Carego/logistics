'use strict';

var app = angular.module('application');

app.controller('BrReconRptCntlr',['$scope','$location','$http','$filter','$window','$log',
                                 function($scope,$location,$http,$filter,$window,$log){

	console.log("BrReconRptCntlr Started");
	
	$scope.branchList = [];
	
	$scope.cashStmtList = [];
	$scope.tempIntBrTvList = [];
	$scope.faCodeNameList = [];
	$scope.cashStmtList = [];
	$scope.csDataBaseList = [];
	
	$scope.frmBr = {};
	$scope.toBr = {};
	
	$scope.frmBrDBFlag = true;
	$scope.toBrDBFlag = true;
	$scope.csToBrDBFlag = true;
	$scope.faMstrDBFlag = true;
	$scope.cashStmtDBFlag = true;
	$scope.csBrDBFlag = true;
	
	$scope.getActiveBrList = function() {
		console.log("getActiveBrList Entered");
		var response = $http.post($scope.projectName+'/getUserBrNBrList');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getActiveBrList result Success");
				$scope.branchList = data.branchList;
				//$scope.frmBr = data.userBranch;
				$scope.getFaMstr();
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrList result Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in response getActiveBrList: "+data);
		});
	}
	
	$scope.openFrmBrDB = function() {
		console.log("openFrmBrDB()");
		$scope.frmBrDBFlag = false;
		$('div#frmBrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.frmBrDBFlag = true;
			}
		});
		$('div#frmBrDB').dialog('open');
	}
	
	$scope.saveFrmBr = function(frmBr){
		console.log("saveBranch()");
		$scope.frmBr = frmBr;
		$('div#frmBrDB').dialog('close');
	}
	
	$scope.openToBrDB = function() {
		console.log("openToBrDB()");
		$scope.toBrDBFlag = false;
		$('div#toBrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.toBrDBFlag = true;
			}
		});
		$('div#toBrDB').dialog('open');
	}
	
	$scope.saveToBr = function(toBr){
		console.log("saveBranch()");
		$scope.toBr = toBr;
		$('div#toBrDB').dialog('close');
	}
	
	$scope.submitBrReconRpt = function(brReconRptForm) {
		console.log("submitBrReconRpt()");
		if (brReconRptForm.$invalid) {
			console.log("invalidate: "+brReconRptForm.$invalid);
			if (brReconRptForm.frmDtName.$invalid) {
				$scope.alertToast("Please enter valid from Date");
			} else if (brReconRptForm.toDtName.$invalid) {
				$scope.alertToast("Please enter valid to date");
			} else if (brReconRptForm.frmBrName.$invalid) {
				$scope.alertToast("Please enter valid from stn")
			} else if (brReconRptForm.toBrName.$invalid) {
				$scope.alertToast("Please enter valid to stn")
			} else {
				$scope.alertToast("something worng with validation")
			}
		} else {
			console.log("invalidate: "+brReconRptForm.$invalid);
			var reconRptService = {
					"frmBrId"	: $scope.frmBr.branchId,
					"toBrId"	: $scope.toBr.branchId,
					"frmDt"		: $scope.frmDt,
					"toDt"		: $scope.toDt
			}
			
			$('#submitId').attr("disabled","disabled");
			
			var res = $http.post($scope.projectName+'/getReconRpt', reconRptService);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("getReconRpt result: "+data.result);
					$log.info(data);
					//console.log(JSON.stringify(data));
					$scope.cashStmtList = data.cashStmtList;
					$scope.tempIntBrTvList = data.tempIntBrTvList;
					$('#submitId').removeAttr("disabled");
				} else {
					console.log("getReconRpt result: "+data.result);
					$('#submitId').removeAttr("disabled");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("Error in response getReconRpt "+data);
				$('#submitId').removeAttr("disabled");
			});
		}
	}
	
	$scope.openCSToBrDB = function(csTempIndex) {
		console.log("openCSToBrDB()");
		$scope.csTempIndex = csTempIndex;
		
		var res = $http.post($scope.projectName+'/getCsFrmCsTemp', $scope.cashStmtList[$scope.csTempIndex]);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getCsFrmCsTemp result: "+data.result);
				$log.info(data);
				$scope.cashStmtList = data.cashStmtList;
				$scope.openCashStmtDB();
			} else {
				console.log("getCsFrmCsTemp result: "+data.result);
				$scope.alertToast("you have already changed ToBr code");
				$scope.alertToast("Press Submit button to continue");
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getCsFrmCsTemp: "+data);
		});
		
		/*$scope.csToBrDBFlag = false;
		$('div#csToBrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.csToBrDBFlag = true;
			}
		});
		$('div#csToBrDB').dialog('open');*/
	}
	
	$scope.saveCSToBr = function(csToBr){
		/*console.log("saveCSToBr()");
		$scope.cashStmtList[$scope.csTempIndex].csBrhFaCode = csToBr.branchFaCode;
		$scope.cashStmtList[$scope.csTempIndex].csBrhName = csToBr.branchName;*/
		$('div#csToBrDB').dialog('close');
	}
	
	$scope.openCashStmtDB = function() {
		console.log("openCashStmtDB()");
		$scope.cashStmtDBFlag = false;
		$('div#cashStmtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Cash Stmt",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.cashStmtDBFlag = true;
			}
		});
		$('div#cashStmtDB').dialog('open');
	}
	
	$scope.openCSBrDB = function(index) {
		console.log("openCSBrDB");
		$scope.csIndex = index;
		$scope.csBrDBFlag = false;
		$('div#csBrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.csBrDBFlag = true;
			}
		});
		$('div#csBrDB').dialog('open');
	}
	
	$scope.saveCSBr = function(csBr) {
		console.log("saveCSBr()");
		$scope.csBr = csBr;
		$scope.cashStmtList[$scope.csIndex].csFaCode = csBr.branchFaCode;
		$('div#csBrDB').dialog('close');
	}
	
	$scope.saveCS = function(cashStmt) {
		console.log("saveCS()");
		$scope.csDataBaseList.push(cashStmt);
		$scope.alertToast("cashStmt saved temporarily")
		
		$scope.cashStmtList[$scope.csTempIndex].csBrhFaCode = $scope.csBr.branchFaCode;
		$scope.cashStmtList[$scope.csTempIndex].csBrhName = $scope.csBr.branchName;
		//$('div#cashStmtDB').dialog('close');
		
	}
	
	$scope.getFaMstr = function(){
		console.log("getFaMstr Entered");
		var response = $http.post($scope.projectName+'/getFaMstr');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getFaMstr Success");
				$scope.faCodeNameList = data.faCodeNameList;
			}else {
				$scope.alertToast("faMstr: "+data.result);
				console.log("getFaMstr Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getFaMstr Error: "+data);
		});
	}
	
	$scope.openFaMstrDB = function(csTempForFaIndex){
		console.log("Entered into openFaMstrDB");
		$scope.csTempForFaIndex = csTempForFaIndex;
		$scope.faMstrDBFlag = false;
		$('div#faMstrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Financial Codes",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.faMstrDBFlag = true;
			}
		});

		$('div#faMstrDB').dialog('open');
	}
	
	$scope.saveFaMstr = function(faCodeName){
		console.log("saveFaMstr()");
		$scope.cashStmtList[$scope.csTempForFaIndex].csFaCode = faCodeName.faMfaCode;
		$('div#faMstrDB').dialog('close');
	}
	
	$scope.saveCSTemp = function(cashStmtTemp) {
		console.log("saveCSTemp()");
		
		var csTempNcsListService = {
				"cashStmtTemp"	: cashStmtTemp,
				"cashStmtList"	: $scope.csDataBaseList
		}
		
		var res = $http.post($scope.projectName+'/updateCSTemp', csTempNcsListService);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				$scope.alertToast("Updated successfully");
			} else {
				$scope.alertToast("Error");
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response updateCSTemp: "+data);
		});
	}
	
	//print xls
	$scope.printCstXls = function() {
		console.log("printCstXls()");
		var blob = new Blob([document.getElementById('cstId').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "pendingCs.xls");
	}
	
	$scope.printCrTvXls = function() {
		console.log("printCrTvXls()");
		var blob = new Blob([document.getElementById('crTvId').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "crTvCs.xls");
	} 
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("BrReconRptCntlr Ended");
}]);