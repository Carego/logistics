'use strict';

var app = angular.module('application');

app.controller('FundAllocationCntlr',['$scope','$location','$http','$window','$filter',
                            function($scope,$location,$http,$window,$filter){
	
	console.log("**************  Fund Allocation Cntlr ***************");
	
	$scope.fundAllocation={};
	$scope.fd={};
	$scope.allocationDetailList=[];
	$scope.allocationDetailList2=[];
	$scope.fundAllocationDetail=true;
	$scope.fromStationDBFlag=true;
	$scope.toStationDBFlag=true;
	$scope.branchDBFlag=true;
	$scope.generateFundData=false;
	   $scope.showFundList=true;
	   $scope.detailsDBFlag=true;
	   $('#sbmtId').attr("disabled","disabled");
	   $('#saveId').removeAttr("disabled");
	   $scope.staffCodeRequireFlag=false;
	   $scope.readonlyFlag=true;
	   $scope.brkReadonlyFlag=false;
	   //$scope.ownReadonlyFlag=true;
	   $scope.owner=null;
	   $scope.broker=null;
	   $scope.payToDisableFlag=false;
	   
	   $scope.changeTransactionType=function(){
		   if($scope.fundAllocation.transactionType==="ATM"){
			   $scope.staffCodeRequireFlag=true;
			   $scope.readonlyFlag=false;
			   $scope.fundAllocation.payTo="others";
			   $('#sbmtId').removeAttr("disabled");
		   }else{
			   $scope.fundAllocation.payTo="";
			   $('#sbmtId').attr("disabled","disabled");
			   $scope.fundAllocation.beneficiaryAccountNo="";
			   $scope.staffCodeRequireFlag=false;
			   $scope.readonlyFlag=true;
			   $scope.fundAllocation.staffCode=null;
		   }
	   }
	   
	   
	   $scope.getStaffCode=function(){
		   if($scope.fundAllocation.staffCode.length<7){
			   return;
		   }
		   console.log("I M In statffCode()");
		   
				 console.log($scope.fundAllocation.staffCode);
					var res = $http.post($scope.projectName+'/getNameOfStf',$scope.fundAllocation.staffCode);
					res.success(function(data, status, headers, config) {
						if (data.result === "success") {
							console.log(data);
							$scope.alertToast(data.name);
							
						} else {
							$scope.alertToast("Please Enter valid staff code");
							$scope.fundAllocation.staffCode='';
						}
					});
					res.error(function(data, status, headers, config) {
						console.log("error in response getNameOfStaf(): "+data);
					});
						
	   }
	   
	   
	   $scope.checkLength=function(){
		   console.log("check Lencth");
		   if($scope.fundAllocation.staffCode.length!==7){
			   $scope.fundAllocation.staffCode='';
			   $scope.alertToast("Please Enter valid staff code");
		   }
		   
	   }
	   
	   
	$scope.openCnmtChlnForm=function()
	{
		$scope.fundAllocationDetail=false;
		$('div#fundAllocationDetailDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Cnmt and Challan Form",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#fundAllocationDetailDB').dialog('open');
	}

	$scope.pushFundAllDetails=function(cnmtCode,challanNo,chlnDt,fromStCode,toStCode,lryAmount,vehicleNo,brkCode,ownCode,lryRate,freight)
	{
		var allocationDetail={
				"cnmtCode":cnmtCode,
				"challanNo":challanNo,
				"chlnDt":chlnDt,
				"fromStCode":fromStCode,
				"toStCode":toStCode,
				"lryAmount":lryAmount,
				"vehicleNo":vehicleNo,
				"brkCode":brkCode,
				"ownCode":ownCode,
				"lryRate":lryRate,
				"chlnFreight":freight
		};
		
		$scope.allocationDetailList=[];
		$scope.allocationDetailList.push(allocationDetail);
		
		
		/*if($scope.allocationDetailList.length>0){
			return;*/
			/*
			for(var i=0;i<$scope.allocationDetailList.length;i++){
				if($scope.allocationDetailList[i].vehicleNo===vehicleNo){
					$scope.alertToast("This Lorry No. already exist");
					return;
				}
			}
			$scope.allocationDetailList.push(allocationDetail);
		*//*}else{
			$scope.allocationDetailList.push(allocationDetail);
		}*/
		
	 /*      var response=$http.post($scope.projectName+ '/fundDetails', allocationDetail);
	       response.success(function(data,status,headers,config)
	    		   {
	    	   $scope.alertToast(data.fadLength);
	    	   $scope.allocationDetailList=data.list;
	    		   });*/
		$scope.cnmtCode="";
		$scope.challanNo="";
		$scope.chlnDt="";
		$scope.fromStCode="";
		$scope.toStCode="";
		$scope.lryAmount="";
		$scope.vehicleNo="";
		$scope.brkCode="";
		$scope.ownCode="";
		$scope.freight="";
		$scope.lryRate="";
		
	       $('div#fundAllocationDetailDB').dialog('close');
			$scope.fundAllocationDetail=true;
			console.log(allocationDetail);
			$('#sbmtId').removeAttr("disabled");
	}
	
	$scope.saveFundAllocation=function(fundAllocation)
	{
		
		$scope.fund=fundAllocation;
		
	    $scope.detailsDBFlag = false;
    	$('div#detailsDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			title:"LHPV ADVANCE FINAL SUBMISSION",
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.saveVsFlag = true;
		    }
			});
		$('div#detailsDB').dialog('open');
	}
	
	$scope.back=function()
	{
		$('div#detailsDB').dialog('close');
		$scope.detailsDBFlag=true;
	}
	
	$scope.saveFA=function()
	{
		 $('#saveId').attr("disabled","disabled");
		$scope.fd.allocation=$scope.fundAllocation;
		$scope.fd.fadList=$scope.allocationDetailList;
		
		var myfd=$scope.fd;
		
		if($scope.fundAllocation.beneAddress1.length<=40 && $scope.fundAllocation.ifscCode.length===11)
			{
		      
			var ch= $scope.fundAllocation.ifscCode.charAt(4);
			if(ch==='0')
				{
			    var response=$http.post($scope.projectName+ '/saveFundAllocation', myfd);
			       response.success(function(data,status,headers,config)
			    		   {
			    	   $('#sbmtId').attr("disabled","disabled");
			    	   $('#saveId').removeAttr("disabled");
			    	   $scope.alertToast("success");
			    	   $scope.allocationDetailList=[];
			    	   $scope.fundAllocation={};
			    		$('div#detailsDB').dialog('close');
			    		$scope.detailsDBFlag=true;
			    		   });
				}else{
					$scope.alertTost("IFSC 5th letter must be 0");
				}
			
			}else{
				$scope.alertTost("check IFSC or bene Address");
			}
	}
	
	$scope.readBranch=function()
	{
		var response = $http.post($scope.projectName + '/getAllOpenBranches');
		response.success(function(data, status, headers, config) {
			$scope.branchList=data.branchList;
			if($scope.branchList.length>0)
				{
				console.log("openBranchDB()");
				$scope.branchDBFlag = false;
				$('div#branchDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Branch",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.branchDBFlag = true;
					}
				});
				$('div#branchDB').dialog('open');
				
				}
		});		
	}
	
	$scope.saveBranch=function(branch)
	{
		var brName=branch.branchName;
		console.log(brName.length);
		if(brName.length>13)
			{
		     $scope.fundAllocation.beneficiaryCode= brName.substring(0,13);
			}
		else
			{
			$scope.fundAllocation.beneficiaryCode= brName;
			}
		$('div#branchDB').dialog('close');
		$scope.branchDBFlag=true;
	}
	
	$scope.beneAddress1=function(address1)
	{
		if(address1.length>40)
			{
			$scope.bene1="Must be less than or equal to 40 characters";
			}
		else
			$scope.bene1="";
	}
	
	$scope.beneAddress2=function(address2)
	{
		if(address2.length>40)
			{
			$scope.bene2="Must be less than or equal to 40 characters";
			}
		else
			$scope.bene2="";
	}
	
	$scope.beneAddress3=function(address3)
	{
		if(address3.length>40)
			{
			$scope.bene3="Must be less than or equal to 40 characters";
			}
		else
			$scope.bene3="";
	}
	
	$scope.beneAddress4=function(address4)
	{
		if(address4.length>40)
			{
			$scope.bene4="Must be less than or equal to 40 characters";
			}
		else
			$scope.bene4="";
	}
	
	$scope.beneAddress5=function(address5)
	{
		if(address5.length>40)
			{
			$scope.bene5="Must be less than or equal to 40 characters";
			}
		else
			$scope.bene5="";
	}
	
	$scope.custRefNo=function(beneName)
	{
		if(angular.isDefined(beneName))
			{
		if(beneName.length>20)
			{
			$scope.fundAllocation.customerRefNo=beneName.substring(0,20);
			}
		else
			{
			if(beneName.length===0)
				{
				console.log(")")
				$scope.fundAllocation.customerRefNo="";
				}
			else
			$scope.fundAllocation.customerRefNo=beneName;
			}
			}
		else
			$scope.fundAllocation.customerRefNo
	}
	
	$scope.chkIfscCode=function(ifsc)
	{
		$scope.ifscMsg="";
		if(ifsc.length!=11)
			{
			$scope.ifscMsg="Ifsc Code must contains 11 digits";
			}
		
		else 
			{
			
			var ch= ifsc.charAt(4);
			if(ch!='0')
				{
				$scope.ifscMsg="5th letter of Ifsc Code must be 0";
				}
			else
				$scope.ifscMsg="";
			
			}
		
	}
	
	
	
	
	
	$scope.getStationFrom = function(keyCode){
		console.log("Enter into getStationData()");
		if(keyCode === 8 || parseInt($scope.fromStCode.length) < 2)
			return;	
		
		var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.fromStCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result==="success"){
				$scope.frmStationCode = "";
				$scope.stationList = data.stationList;
				$scope.OpenFromStationDB();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getStationDataByNameCode");
		});
		console.log("Exit from getStationData()");
	}
	
	$scope.OpenFromStationDB = function(){
		$scope.fromStationDBFlag=false;
		$('div#fromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#fromStationDB').dialog('open');
	}
	
	
	$scope.saveFrmStnCode = function(station){
		$scope.fromStCode = station.stnName;
		$('div#fromStationDB').dialog('close');
		$scope.fromStationDBFlag=true;
	}
	
	
	$scope.getStationTo = function(keyCode){
		console.log("Enter into getStationData()");
		if(keyCode === 8 || parseInt($scope.toStCode.length) < 2)
			return;	
		
		var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.toStCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result==="success"){
				$scope.toStCode = "";
				$scope.stationList = data.stationList;
				$scope.OpenToStationDB();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getStationDataByNameCode");
		});
		console.log("Exit from getStationData()");
	}
	
	$scope.OpenToStationDB = function(){
		$scope.toStationDBFlag=false;
		$('div#toStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#toStationDB').dialog('open');
	}
	
	
	$scope.saveToStnCode = function(station){
		$scope.toStCode = station.stnName;
		//$scope.toStn=station.stnCode;
		$('div#toStationDB').dialog('close');
		$scope.toStationDBFlag=true;
	}
	
	
	$scope.checkCnmtCode = function(cnmtCode){
		
		 console.log("cnmtCode="+cnmtCode);
			var res = $http.post($scope.projectName+'/verifyCnmtFrAdv',cnmtCode);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log(data.cnmtCode);
					$scope.cnmtCode=data.cnmtCode;
					//$scope.alertToast(data.msg);
				} else {
					$scope.alertToast("Please Enter valid CNMT No.");
					$scope.cnmtCode="";
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response verifyCnmtFrAdv: "+data);
			});
	}
	
	$scope.checkChlnCode = function(chlnCode){
		
		 console.log("chlnCode="+chlnCode);
		 
		 if(chlnCode==null || chlnCode=='')
			 return;
		 
		 var res = $http.post($scope.projectName+'/verifyDuplicateChlnFrAdv',chlnCode);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					$scope.challanNo="";
					$scope.alertToast(" This challan already requested for advance");
				}else{
					var res = $http.post($scope.projectName+'/verifyChlnFrAdv',chlnCode);
					res.success(function(data, status, headers, config) {
						if (data.result === "success") {
							
							if(data.chln != null){
								console.log(data.chln.chlnCode);
								$scope.chln=data.chln;
								$scope.challanNo=$scope.chln.chlnCode;
								$scope.toStCode=data.toStn.stnName;
								$scope.fromStCode=data.fromStn.stnName;
								$scope.chlnDt=$scope.chln.chlnDt;
								$scope.vehicleNo=$scope.chln.chlnLryNo;
								$scope.lryAmount=$scope.chln.chlnRemAdv;
								$scope.frtAmount=$scope.chln.chlnTotalFreight;
								$scope.lryRate=$scope.chln.chlnLryRate;
								$scope.broker=data.brk;
								$scope.owner=data.own;
								$scope.brkCode=$scope.broker.brkCode;
								$scope.ownCode=$scope.owner.ownCode;
								$scope.brkReadonlyFlag=true;
								//$scope.ownReadonlyFlag=true;
							}else{
								console.log(data.chlnCode);
								$scope.brkReadonlyFlag=false;
								$scope.challanNo=data.chlnCode;
							}
							//$scope.challanNo=data.chln.chlnCode;
							//$scope.alertToast(data.msg);
						} else {
							$scope.alertToast("Please Enter valid Challan No.");
							$scope.challanNo="";
						}
					});
					res.error(function(data, status, headers, config) {
						console.log("error in response verifyChlnFrAdv: "+data);
					});
				}
				});
			res.error(function(data, status, headers, config) {
				console.log("error in response verifyChlnFrAdv: "+data);
			});
		 
		 
			
	}
	
	$scope.vehDBFlag=true;
	$scope.vehAdvReqDBFlag=true;
	
	$scope.openVehicleDB = function(){
		  console.log("Enter into openVehicleDB()....");
		  var chlnLryNo = $scope.vehicleNo;		 
		  var len = parseInt(chlnLryNo.length);
		  if(len < 4)
			  return;
//		  $scope.vehicleNo = "";	 
		  console.log("Hitting /getVehicleMstrFCByLryNo ");
		  var response = $http.post($scope.projectName+'/getVehicleMstrFCByLryNo', chlnLryNo);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.filterVehCode = chlnLryNo;
				 // $scope.ownReadonlyFlag=true;
				  $scope.vehList = data.list;
				  $scope.vehDBFlag=false;
				  $('div#vehDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Select Vehicle",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
						  $scope.vehDBFlag=true;
					  }
				  });
				  $('div#vehDB').dialog('open');
			  }else{
				  //$scope.ownReadonlyFlag=false;
				  /*  var req={
							"chlnDt":$scope.chlnDt,
							"from":$scope.fromStCode,
							"to":$scope.toStCode,
							"lryNo":$scope.vehicleNo,
							"chlnNo":$scope.challanNo,
							"cnmtNo":$scope.cnmtCode
						};
				  if($scope.chlnDt==null || $scope.chlnDt.undefined || $scope.chlnDt=='' ){
					  $scope.alertToast("challan date is required ");
					  return;
				  }
				  
				  var response = $http.post($scope.projectName+'/getAlwdVehicleFrAdv', req);
				  response.success(function(data, status, headers, config){
					  if(data.result === "success"){
						  if(data.isAlwd){
							  $scope.fundAllocation.payTo="unRegistered";
							  $scope.readonlyFlag=false;
							  $scope.payToDisableFlag=true;
						  }else{
							  $scope.alertToast("Permission pending ");
							  $scope.readonlyFlag=true;
							  $scope.payToDisableFlag=false;
							  $scope.fundAllocation.payTo="";
						  }
						  
					  }else{
						  $scope.alertToast("Vehicle not found !");
						  $scope.vehAdvReqDBFlag=false;
						  $('div#vehAdvReqDB').dialog({
							  autoOpen: false,
							  modal:true,
							  title: "Request Form",
							  show: UDShow,
							  hide: UDHide,
							  position: UDPos,
							  resizable: false,
							  draggable: true,
							  close: function(event, ui) { 
								  $(this).dialog('destroy') ;
								  $(this).hide();
								  $scope.vehDBFlag=true;
							  }
						  });
						  $('div#vehAdvReqDB').dialog('open');
					    
					  }
					  
				  });
				  
				*/}
		  });	  
		
		 
	  }
	
	
	$scope.selectVeh=function(vehicle){
		 var response = $http.post($scope.projectName+'/getVehclDetFrAdv',vehicle);
		  response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('div#vehDB').dialog('close');
				$scope.vehicleNo=data.vehNo;
				$scope.owner=data.own;
				$scope.broker=data.brk;
				if($scope.broker != null)
				$scope.brkCode=$scope.broker.brkCode;
				$scope.ownCode=$scope.owner.ownCode;
			}else{
				$('div#vehDB').dialog('close');
				$scope.alertToast(data.msg);				
			}
		  });
		  response.error(function(data, status, headers,config) {
			  $('div#vehDB').dialog('close');
			  $scope.errorToast(data);
		  });
		
	}
	
	
	/*$scope.vehAdvRequest=function(){
		
		var req={
			"chlnDt":$scope.chlnDt,
			"from":$scope.fromStCode,
			"to":$scope.toStCode,
			"lryNo":$scope.vehicleNo,
			"chlnNo":$scope.challanNo,
			"cnmtNo":$scope.cnmtCode
		};
		
		 var response = $http.post($scope.projectName+'/sendVehclAdvAlwReq',req);
		  response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('div#vehAdvReqDB').dialog('close');
				$scope.alertToast(data.msg);
				
			}else{
				$('div#vehAdvReqDB').dialog('close');
				$scope.payToDisableFlag=false;
			}
		  });
		  response.error(function(data, status, headers,config) {
			  $('div#vehAdvReqDB').dialog('close');
			  $scope.errorToast(data);
		  });
		
	}*/
	
	
	$scope.brokerDBFlag=true;
	
	$scope.getBrokerByName = function(brkCode){
		  console.log("Enter into openVehicleDB()....");
		  var len = parseInt(brkCode.length);
		  if(len < 4)
			  return;
		  console.log("Hitting /getBrokerByName ");
		  var response = $http.post($scope.projectName+'/getBrokerForAdvByBrkName', brkCode);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  
				  $scope.brkList = data.list;
				  $scope.brokerDBFlag=false;
				  $('div#brokerDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Select Broker",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
						  $scope.vehDBFlag=true;
					  }
				  });
				  $('div#brokerDB').dialog('open');
			  }else{
				  $scope.alertToast("Broker not found !");
			  }
		  });	  
		
		 
	  }
	
	
	$scope.saveBrkCode=function(brk){
		 		$('div#brokerDB').dialog('close');
				$scope.broker=brk;
				console.log("brkCode="+$scope.broker.brkCode);
				$scope.brkCode=$scope.broker.brkCode;
	}
	
	
	$scope.changePayTo=function(){
		if($scope.fundAllocation.payTo=="owner"){
			if($scope.owner.ownBnkDetValid==true){
				$scope.fundAllocation.beneficiaryAccountNo=$scope.owner.ownAccntNo;
				$scope.fundAllocation.beneficiaryName=$scope.owner.ownAcntHldrName;
				$scope.fundAllocation.ifscCode=$scope.owner.ownIfsc;
				$scope.fundAllocation.beneBnkName=$scope.owner.ownBnkBranch;
				$scope.custRefNo($scope.fundAllocation.beneficiaryName);
			}else{
				$scope.alertToast("Owner does not have valid bank detail");
			}
		}
		if($scope.fundAllocation.payTo=="broker"){
			if($scope.broker.brkBnkDetValid==true){
				$scope.fundAllocation.beneficiaryAccountNo=$scope.broker.brkAccntNo;
				$scope.fundAllocation.beneficiaryName=$scope.broker.brkAcntHldrName;
				$scope.fundAllocation.ifscCode=$scope.broker.brkIfsc;
				$scope.fundAllocation.beneBnkName=$scope.broker.brkBnkBranch;
				$scope.custRefNo($scope.fundAllocation.beneficiaryName);
			}else{
				$scope.alertToast("Broker does not have valid bank detail");
			}
		}

	}
	
	
	
	$scope.generateFundAllocation=function()
	{
		 var response=$http.post($scope.projectName+ '/generateFundAllocation');
	       response.success(function(data,status,headers,config)
	    		   {
	    	  console.log(data)
	    	   if(data.msg==="No Fund Details for Current Date")
	    		   {
	    		   $scope.alertToast(data.msg);
	    		   }
	    	   else{
	    		   
	    		   $scope.fundList=data.faList;
	    		   $scope.mapList=data.mapList;
	    		   console.log("Hiiiiiiiiiiiiiiiiiiiiiiiiiii"+$scope.fundList[0].fundAllocationDetails)
	    		   $scope.alertToast(data.msg);
	    		   $scope.generateFundData=true;
	    		   $scope.showFundList=false;
	    	   }
	    	 
	    		   });
	       response.error(function(data,status,headers,config)
	    		   {
	    	   console.log(data)
	    		   });
		
	}
	
	$scope.updateFundAllocation=function(fundAll,index)
	{
		/*console.log(fundAll.instrumentAmount);
		console.log(fundAll.fundAllocationDetails);*/
		var fund={
				"allocation":fundAll.fa,
				"fd":fundAll.fd
		}
		
		 var response=$http.post($scope.projectName+ '/updateFundAllocation',fund);
	       response.success(function(data,status,headers,config)
	    		   {
	    	   if(data.result==="success")
	    		   {
	    		   $scope.alertToast("Success");
	    		   $scope.mapList.splice(index,1);
	    		   if($scope.mapList.length==0)
	    			   {
	    				$scope.generateFundData=false;
	    			    $scope.showFundList=true;
	    			   }
	    		   }
	    	   else if(data.result=="error")
	    		   {
	    		   $scope.alertToast(data.msg);
	    		   }
	    	   else{
	    		   console.log(data.msg);
	    		   $scope.alertToast("There is some problem");
	    	   }
	    		   });
	       response.error(function(data,status,headers,config)
	    		   {
	    	   $scope.alertToast("Server Error");
	    	   
	    		   });
		
	}
	
	
	$scope.rejectPayment=function(fundAll,index){
		console.log("faId=="+fundAll.fa.faId);
		var response=$http.post($scope.projectName+ '/rejectFundAllocation',fundAll.fa);
	       response.success(function(data,status,headers,config)
	    		   {
	    	   if(data.result==="success")
	    		   {
	    		   $scope.alertToast(data.msg);
	    		   $scope.mapList.splice(index,1);
	    		   if($scope.mapList.length==0)
	    			   {
	    				$scope.generateFundData=false;
	    			    $scope.showFundList=true;
	    			   }
	    		   }
	    	  
	    	   else{
	    		   $scope.alertToast(data.msg);
	    	   }
	    		   });
	       response.error(function(data,status,headers,config)
	    		   {
	    	   $scope.alertToast("Server Error");
	    	   
	    		   });
	}
	
	
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "fundAllocation.xls");
	};
	
}]);