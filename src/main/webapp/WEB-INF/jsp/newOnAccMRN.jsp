<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="mrForm" id = "mrForm" ng-class = "{'form-error': mrForm.$invalid && isFormSubmitted}"
			novalidate = "novalidate" class="col s12 card" style="align: center; background-color: #F2FAEF;">
			
			<div class = "row errorMargin" id = "errorMsgDiv">
			</div><br>
			<div class="row">
			
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" ng-blur="chngDt()" readonly ng-required="true" >
		       				<label for="code" style="color:black;">CS Date</label>
		       				<div class = "text-left errorMargin" ng-messages = "mrForm.csDtName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  CS date is required.</span>
							</div>
		       		</div>
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code" style="color:black;">Customer Name</label>
		       			<div class = "text-left errorMargin" ng-messages = "mrForm.custName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Customer is required.</span>
							</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       			<select name="mrByName" id="mrById" ng-model="mr.mrPayBy" ng-init="mr.mrPayBy = 'C'" ng-change="chngMRPay()" ng-required="true">
								<option value='C'>CASH</option>
								<option value='Q'>CHEQUE</option>
								<option value='R'>RTGS</option>
						</select>
						<label style="color:black;">Money Receipt By</label>
						<div class = "text-left errorMargin" ng-messages = "mrForm.mrByName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Pay by is required.</span>
							</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="number" id="netAmtId" name ="netAmtName" ng-model="mr.mrNetAmt" step="0.001" min="0.000" 
		       					ng-required="true">
		       			<label for="code" style="color:black;">Net Amount</label>	
		       			<div class = "text-left errorMargin" ng-messages = "mrForm.netAmtName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Net amount is required.</span>
							</div>	
		       		</div>
					
		    </div>
		  
		    <div class="row">
		    
		    	<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="bnkId" name ="bnkName" ng-model="bank.bnkName" ng-click="selectBank()" ng-disabled="true" readonly
		       				 ng-required = "commonValidationFlag">
		       				<label for="code" style="color:black;">Bank Name</label>	
		       				<div class = "text-left errorMargin" ng-messages = "mrForm.bnkName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Bank name is required.</span>
							</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="custBnkId" name ="custBnkName" ng-model="mr.mrCustBankName" disabled="disabled"
		       				 	ng-minlength = "3" ng-maxlength = "100" ng-required = "commonValidationFlag">
		       			<label for="code" style="color:black;">Customer Bank</label>	
		       			<div class = "text-left errorMargin" ng-messages = "mrForm.custBnkName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Customer bank is required.</span>
								<span ng-message = "minlength">*  Customer bank should be greater than 2.</span>
								<span ng-message = "maxlength">*  Customer bank should be less than 100.</span>
							</div>
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="chqId" name ="chqName" ng-model="mr.mrChqNo" disabled="disabled" 
		       					ng-required = "chequeValidationFlag" ng-minlength = "6">
		       			<label for="code" style="color:black;">Cheque No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "mrForm.chqName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Cheque No. is required.</span>
								<span ng-message = "minlength">*  Cheque No. should be greater than 5</span>
							</div>
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="refId" name ="refName" ng-model="mr.mrRtgsRefNo" disabled="disabled"
		        ng-maxlength = "100" ng-required = "rtgsValidationFlag">
		       			<label for="code" style="color:black;">RTGS Ref No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "mrForm.refName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  RTGS is required.</span>
								<span ng-message = "maxlength">*  RTGS should not be greater than 100.</span>
							</div>
		       		</div>
		       			       		
		    </div>
		    
		    <div class="row">
				<div class="input-field col s12">
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="mr.mrDesc" ng-required="true"
 						ng-minlength = "2" ng-maxlength = "200"></textarea>
 						<div class = "text-left errorMargin" ng-messages = "mrForm.desc.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Description is required.</span>
								<span ng-message = "minlength">*  Description is too short than 2.</span>
								<span ng-message = "maxlength">*  Description is too long than 200.</span>
							</div>
 					<label style="color:black;">Description</label>
				</div>
    		 </div>	
		    
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<button class = "btn btn-primary" type ="submit" id="saveId" ng-click = "mrSubmit(mrForm)"> Submit</button>
	       		</div>
	      </div>		
	     
	</form>
	</div>  
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBankId" ng-hide="selBankFlag">
		  <input type="text" name="filterBank" ng-model="filterBank" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bank Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bankList | filter:filterBank">
		 	  <td><input type="radio"  name="bnk"   value="{{ bnk }}" ng-model="bnkCode" ng-click="saveBank(bnk)"></td>
              <td>{{bnk.bnkName}}</td>
              <td>{{bnk.bnkFaCode}}</td>
          </tr>
      </table> 
	</div>
	
		<div id="duplicateDB" ng-hide="duplicateFlag">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">You already generate a MR of {{ customer.custName }} with {{mr.mrNetAmt}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelMR()">Cancel</a>
				<a class="btn white-text" ng-click="saveMR()">Yes</a>
			</div>
		</div>
	</div>
	
</div>	