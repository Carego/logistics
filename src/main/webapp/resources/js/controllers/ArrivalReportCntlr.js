'use strict';

var app = angular.module('application');

app.controller('ArrivalReportCntlr',['$scope','$location','$http','ARFileUploadService','$filter','AuthorisationService',
                                     function($scope,$location,$http,ARFileUploadService,$filter,AuthorisationService){
	
	$scope.sedrServ = {};
	$scope.sedrServ.cnWSList = [];
	$scope.ar= {};
	$scope.arImage = [];
	$scope.wtSrtg = [];
	$scope.qSrtg = []; 
	$scope.wtSrtgRs = [];
	$scope.qSrtgRs = [];
	
	$scope.ar.branchCode = AuthorisationService.getUserBrhCode();
	
	$scope.show = "true";
	$scope.chlnCodeFlag = true;
	$scope.ARCodesDBFlag = true;
	$scope.BranchCodeDBFlag=true;
	$scope.viewARDetailsFlag=true;
	$scope.chlnTotalWt = 0;
	$scope.chlnPkg = 0;
	var recWt = 0;
	$scope.issueDtHideFlg=true;

	$scope.manualArCodeFlag = true;
	$scope.compArCodeFlag = true;
	$scope.displayArCodeFlag = true;
	$scope.wtSrtCnmtDBFlag = true;
	$scope.qSrtCnmtDBFlag = true;
	$scope.uploadImg = false;
	
	//$scope.difWt = 0.0;
	
	$('#arId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arId').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#arRcvWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#arRcvWt').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});

	$('#arPkg').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arPkg').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});

	$('#arUnloading').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arUnloading').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});


	$('#arClaim').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arClaim').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arDetention').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arDetention').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arOther').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arOther').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arLateDelivery').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arLateDelivery').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arLateAck').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arLateAck').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$scope.OpenARCodeDB = function(){
		$scope.ARCodesDBFlag = false;
		$('div#arCodesDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#arCodesDB').dialog('open');
	}

	$scope.saveARCodes = function(ar){
		//$scope.ar.arCode = ar.brsLeafDetSNo;
		$scope.ar.arCode = ar.brsLeafDetSNo;
		$('div#arCodesDB').dialog('close');
		$scope.ARCodesDBFlag = true;
	}

	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}

	
	$scope.saveBranchCode = function(branch){
		console.log("enter into saveBranchCode function");
		$scope.chlnList = [];
		$scope.arbrCodeTemp = branch.branchName;
		$scope.arbrCode = branch.branchCode;
		//$scope.ar.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag = true;
		$scope.getArChlnCode($scope.arbrCode);
		
	}


	$scope.submitAR = function(ArrivalReportForm,ar,manualArCode,compArCode){
		console.log("enter into submitAR function ArrivalReportForm.$invalid--->"+ArrivalReportForm.$invalid);	
		console.log("length of $scope.resList = "+$scope.resList.length);
		console.log("length of $scope.arImage = "+$scope.arImage.length);
		if($scope.ar.arWtShrtg>0){       //by Kamal code start
			$scope.ar.arSrtgDmg="SHORTAGE";
		}//by kamal code end
		if(ArrivalReportForm.$invalid){
			if(ArrivalReportForm.arRepType.$invalid){
				$scope.alertToast("please enter arrival report type....");
			}else if(ArrivalReportForm.archlnCodeName.$invalid){
				$scope.alertToast("please enter 7 digit AR Chln Code..");
			}else if(ArrivalReportForm.arDt.$invalid){
				$scope.alertToast("please enter AR Date..");
			}else if(ArrivalReportForm.arRcvWt.$invalid){
				$scope.alertToast("please enter Received Weight in integer..");
			}else if(ArrivalReportForm.arPkg.$invalid){
				$scope.alertToast("please enter Package in integer..");
			}else if(ArrivalReportForm.arUnloading.$invalid){
				$scope.alertToast("Please enter Unloading..");
			}else if(ArrivalReportForm.arClaim.$invalid){
				$scope.alertToast("please enter Claim..");
			}else if(ArrivalReportForm.arDetention.$invalid){
				$scope.alertToast("please enter Detention..");
			}else if(ArrivalReportForm.arOther.$invalid){
				$scope.alertToast("Please enter Other..");
			}else if(ArrivalReportForm.arLateDelivery.$invalid){
				$scope.alertToast("Please enter Late Delivery..");
			}else if(ArrivalReportForm.arLateAck.$invalid){
				$scope.alertToast("Please enter Late Acknowledgement..");
			}else if(ArrivalReportForm.arRepDt.$invalid){
				$scope.alertToast("Please enter Reporting Date..");
			}else if(ArrivalReportForm.arRepTime.$invalid){
				$scope.alertToast("Please enter Reporting Time..");
			}else if(ArrivalReportForm.arSrtgDmgType.$invalid){
				$scope.alertToast("Please select Shortage/Damage option..");
			}
		}else{

		 if($scope.ar.arRepDt > $scope.ar.arIssueDt){
			$scope.alertToast("Reporting Date must be less than Arrival Issue Date");
		 }else{
			if($scope.uploadImg === true){
				
				
				/*if((angular.isUndefined(ar.manualArCode) || ar.manualArCode === null || ar.manualArCode === "")&&
						(angular.isUndefined(ar.compArCode) || ar.compArCode === null || ar.compArCode === "")){
					if(ar.arRepType === "Manual"){
						$scope.alertToast("please enter arCode of 7dights....");
					}else if(ar.arRepType === "Computerized"){
						$scope.alertToast("please enter arCode ....")
					}
				}*/
				/*else if(ar.arRepType === "Manual" && ar.manualArCode.length < 7){
					$scope.alertToast("please enter arCode exactly of 7dights....");
				}else{
					if(ar.arRepType === "Manual"){
						console.log("I m manual");
						$scope.ar.arCode = ar.manualArCode;
					}else if(ar.arRepType === "Computerized"){
						$scope.ar.arCode = ar.compArCode;
					}*/
				if($scope.resList.length === $scope.arImage.length){
					if(ar.arRepType === "Manual"){
						if(angular.isUndefined($scope.ar.arCode) || $scope.ar.arCode === "" || $scope.ar.arCode === null){
							$scope.alertToast("please enter arCode ....");
						}else{
							console.log("value of $scope.ar.arCode = "+$scope.ar.arCode);
							console.log("ar.archlnCode = "+ar.archlnCode);
							$scope.viewARDetailsFlag = false;
							$('div#viewARDetailsDB').dialog({
								autoOpen: false,
								modal:true,
								title: "Arrival Report Info",
								show: UDShow,
								hide: UDHide,
								position: UDPos,
								resizable: false,
								draggable: true,
								close: function(event, ui) { 
									$(this).dialog('destroy') ;
									$(this).hide();
								}
							});

							$('div#viewARDetailsDB').dialog('open');
						}
					}else{
						console.log("ar.archlnCode = "+ar.archlnCode);
						$scope.viewARDetailsFlag = false;
						$('div#viewARDetailsDB').dialog({
							autoOpen: false,
							modal:true,
							title: "Arrival Report Info",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							resizable: false,
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy') ;
								$(this).hide();
							}
						});

						$('div#viewARDetailsDB').dialog('open');
					}
				}else{
					$scope.alertToast("please upload all the scan image of cnmt");
				}
				//}
			}else{
				$scope.alertToast("Please upload the cnmt image");
			}

		}
		 

		}
	}

	
	$scope.saveAR = function(ar){
		console.log("Entered into saveAR function----> ");
		
		$('#saveBtnId').attr("disabled","disabled");
		
		$scope.sedrServ.arrivalReport = ar;
		
		
		var response = $http.post($scope.projectName+'/submitar',$scope.sedrServ);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				
			var respo = $http.post($scope.projectName+'/saveAckValidation',$scope.resList[0].cnmt.cnmtCode);
				console.log("response "+respo);
				$('#saveBtnId').removeAttr("disabled");
				$('div#viewARDetailsDB').dialog('close');
				if(!angular.isUndefined(data.compArCode)){
					    $scope.compArCode = data.compArCode;
						$scope.displayArCodeFlag = false;
						$('div#displayArCodeDB').dialog({
							autoOpen: false,
							modal:true,
							title: "Arrival Report Info",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							resizable: false,
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy') ;
								$(this).hide();
							}
						});
	
						$('div#displayArCodeDB').dialog('open');
				}
				console.log(data);
				$scope.arbrCodeTemp = "";
				/*$scope.ar.branchCode="";
				$scope.ar.arCode="";
				$scope.ar.archlnCode="";
				$scope.ar.arDt="";
				$scope.ar.arRcvWt="";
				$scope.ar.arPkg="";
				$scope.ar.arUnloading="";
				$scope.ar.arClaim="";
				$scope.ar.arDetention="";
				$scope.ar.arOther="";
				$scope.ar.arLateDelivery="";
				$scope.ar.arLateAck="";
				$scope.ar.arRepDt="";
				$scope.ar.arRepTime="";*/
				$scope.uploadImg = false;
				$scope.ar = {};
				$scope.arImage = [];
				$scope.resList = [];
				$scope.successToast(data.result);
			}else{
				$('#saveBtnId').removeAttr("disabled");
				$scope.errorToast("Not Saved");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in ArrivalReportCntlr");
			$scope.errorToast(data);
			$('#saveBtnId').removeAttr("disabled");
		});	
	}

	
	$scope.closeCode = function(){
		console.log("enter into closeCode function");
		$('div#displayArCodeDB').dialog('close');
		$scope.displayArCodeFlag = true;
		$scope.compArCode = "";
	}
	
	
	$scope.closeViewARDetailsDB = function(){
		$('div#viewARDetailsDB').dialog('close');
	}

	$scope.uploadArImage = function(res,img){
		console.log("enter into uploadArImage function = "+img);
		var file = img;
		console.log("file ====>> "+file);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadarImage";
			ARFileUploadService.uploadFileToUrl(file, uploadUrl,res);
			$scope.uploadImg = true;
			console.log("file save on server");
		}
	}

	$scope.selectedChlnCode = function(challan){
		console.log("enter into selectedChlnCode function---->"+challan.chlnCode)
		$scope.selChln = challan;
		$scope.chlnCodeFlag  = true;
		$('div#chlnCodeDB').dialog('close');
		var response = $http.post($scope.projectName+'/checkCnmtImage',challan.chlnCode);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				//$scope.cnmtNo        = data.cnmtNo;
				$scope.resList       = data.resList;
				$scope.imageCount    = data.blankImage;
				$scope.ar.archlnCode = challan.chlnCode;
				$scope.chlnTotalWt 	 = challan.chlnTotalWt;
				$scope.chlnPkg       = challan.chlnNoOfPkg;
				$scope.ar.arRcvWt	 = challan.chlnTotalWt/1000;
				$scope.ar.arPkg		 = $scope.chlnPkg;
				console.log("$scope.ar.arRcvWt="+$scope.ar.arPkg);
				//console.log("from server $scope.cnmtNo = "+$scope.cnmtNo);
				console.log("from server $scope.imageCount ="+$scope.imageCount);
				$scope.alertToast("This is the challan of "+$scope.resList.length+" cnmt");
				if($scope.imageCount > 0){
					$scope.alertToast("cnmt of this challan doesn't have images");
					//$scope.ar.archlnCode = "";
				}else{
					console.log("all cnmt have atleast one image");
				}
			}else{
				$scope.ar.archlnCode = "";
				$scope.alertToast("This challan doesn't have any cnmt");
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}

	$scope.selectChlnCode = function(){
		console.log("enter into selectChlnCode function");
		/*if(angular.isUndefined($scope.arbrCodeTemp) || $scope.arbrCodeTemp === "" || $scope.arbrCodeTemp === null){
			$scope.alertToast("Please select Branch Code");
		}else{*/
			$('div#chlnCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Select Challan Code",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
			});
			$('div#chlnCodeDB').dialog('open');
			$scope.chlnCodeFlag = false;
		//}
	}

	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForAR');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
				//$scope.getArChlnCode();
				$scope.getARCodeList();
			}else{
				$scope.alertToast("you don't have any active branch");
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}


	$scope.getArChlnCode = function(chlnCOde){
		console.log("enter into getArChlnCode function");
		console.log("chlnCode="+$scope.ar.archlnCode);
		var response = $http.post($scope.projectName+'/getArChlnByChlnCode',$scope.ar.archlnCode);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.chlnList = data.list;
				console.log("from server --->"+$scope.chlnList[0].chlnCode);
				$scope.selectChlnCode();
				//$scope.getARCodeList();
			}else{
				$scope.alertToast("Please Enter valid challan");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getARCodeList = function(){
		console.log(" entered into getARCodeList------>");
		var response = $http.post($scope.projectName+'/getARCodeList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.arCodeList = data.list;
			}else{
				$scope.alertToast("you don't have any arrival");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}


	$scope.checkRecWt = function(recWtPer){
		console.log("enter into checkRecWt function--->"+recWtPer);
		console.log("$scope.chlnTotalWt ===> "+$scope.chlnTotalWt);
		console.log("$scope.ar.arRcvWt ===> "+$scope.ar.arRcvWt);
		recWt = 0.0;
		var recWtTemp;
		var recWtTotalWtTemp;
		
		if(!angular.isUndefined($scope.ar.arRcvWt) || !($scope.ar.arRcvWt === null)){
			recWt = $scope.ar.arRcvWt;
			if(recWtPer === "Ton"){
				//recWt = recWt*1000;
				recWtTemp=recWt*1000;
				recWt=recWtTemp.toFixed(5);
				console.log("recWt--->"+recWt);
				recWtTotalWtTemp=$scope.chlnTotalWt;
				console.log("recWtTotalWtTemp="+recWtTotalWtTemp);
				$scope.chlnTotalWt=Number(recWtTotalWtTemp).toFixed(5);
				console.log("$scope.chlnTotalWt----->"+$scope.chlnTotalWt);
				if(recWt <= $scope.chlnTotalWt){
					console.log("$scope.ar.arRcvWt is valid");
					
					if($scope.resList.length > 0){
						if($scope.resList.length === 1){
							if($scope.resList[0].contType === "W" || $scope.resList[0].contType === "K"){
								$scope.sedrServ.contType = $scope.resList[0].contType;
								console.log("$scope.resList[0].cnmt.cnmtVOG ---->"+$scope.resList[0].cnmt.cnmtVOG);
								$scope.difWt = ($scope.chlnTotalWt - recWt);
								console.log("difWt = "+$scope.difWt);
								//$scope.ar.arWtShrtg = parseFloat(((parseFloat($scope.resList[0].cnmt.cnmtVOG) / parseFloat($scope.selChln.chlnTotalWt)) * $scope.difWt).toFixed(2));
								$scope.ar.arWtShrtg = parseFloat(((parseFloat($scope.resList[0].cnmt.cnmtVOG) / parseFloat($scope.resList[0].cnmt.cnmtActualWt)) * $scope.difWt).toFixed(2));
								$scope.ar.arDrRcvrWt = parseFloat(((parseFloat($scope.selChln.chlnFreight)/parseFloat($scope.selChln.chlnChgWt)) * $scope.difWt).toFixed(2));
								console.log("$scope.ar.arWtShrtg = "+$scope.ar.arWtShrtg);
								console.log("$scope.ar.arDrRcvrWt = "+$scope.ar.arDrRcvrWt);
								var map = {
										"cnmtCode" : $scope.resList[0].cnmt.cnmtCode,
										"srtg"     : $scope.difWt
								};
								
								$scope.ar.arWtSrtCnmt = [];
								$scope.ar.arWtSrtCnmt.push(map);
							}
						}else if($scope.resList.length > 1){
							if($scope.resList[0].contType === "W" || $scope.resList[0].contType === "K"){
								$scope.sedrServ.contType = $scope.resList[0].contType;
								//$scope.difWt = ($scope.chlnTotalWt - recWt);
								$scope.difWt = parseFloat(parseFloat($scope.chlnTotalWt - recWt)).toFixed(3);
								console.log("$scope.difWt ----> "+$scope.difWt);
								$scope.ar.arDrRcvrWt = parseFloat(((parseFloat($scope.selChln.chlnFreight)/parseFloat($scope.selChln.chlnChgWt)) * $scope.difWt).toFixed(2));
								console.log("$scope.ar.arDrRcvrWt = "+$scope.ar.arDrRcvrWt);
								$scope.sedrServ.cnWSList = [];
								$scope.wtSrtCnmtDBFlag = false;
								$('div#wtSrtCnmtDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									title: "CNMT WT WISE SHORTAGE OF "+$scope.difWt+" Kg",
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
										$(this).dialog('destroy');
										$(this).hide();
										$scope.wtSrtCnmtDBFlag = true;
									}
								});
								$('div#wtSrtCnmtDB').dialog('open')
							}
						}else{
							
						}
					}
				
				}else{
					$scope.alertToast("Received Weight must be less than equal to challan total weight");
					$scope.ar.arRcvWt = null;
					recWt = 0;
				} 
			}else{
				console.log("recWt--->"+recWt);
				if(recWt <= $scope.chlnTotalWt){
					console.log("$scope.ar.arRcvWt <= $scope.chlnTotalWt");
					
					if($scope.resList.length > 0){
						if($scope.resList.length === 1){
							if($scope.resList[0].contType === "W" || $scope.resList[0].contType === "K"){
								$scope.sedrServ.contType = $scope.resList[0].contType;
								console.log("$scope.resList[0].cnmt.cnmtVOG ---->"+$scope.resList[0].cnmt.cnmtVOG);
								$scope.difWt = ($scope.chlnTotalWt - recWt);
								console.log("difWt = "+$scope.difWt);
								$scope.ar.arWtShrtg = parseFloat(((parseFloat($scope.resList[0].cnmt.cnmtVOG) / parseFloat($scope.selChln.chlnTotalWt)) * $scope.difWt).toFixed(2));
								console.log("$scope.ar.arWtShrtg = "+$scope.ar.arWtShrtg);
								$scope.ar.arDrRcvrWt = parseFloat(((parseFloat($scope.selChln.chlnFreight)/parseFloat($scope.selChln.chlnChgWt)) * $scope.difWt).toFixed(2));
								console.log("$scope.ar.arDrRcvrWt = "+$scope.ar.arDrRcvrWt);
								var map = {
										"cnmtCode" : $scope.resList[0].cnmt.cnmtCode,
										"srtg"     : difWt
								};
								
								$scope.ar.arWtSrtCnmt = [];
								$scope.ar.arWtSrtCnmt.push(map);
							}
						}else if($scope.resList.length > 1){
							if($scope.resList[0].contType === "W" || $scope.resList[0].contType === "K"){
								$scope.sedrServ.contType = $scope.resList[0].contType;
								$scope.difWt = ($scope.chlnTotalWt - recWt);
								console.log("$scope.difWt ----> "+$scope.difWt);
								$scope.ar.arDrRcvrWt = parseFloat(((parseFloat($scope.selChln.chlnFreight)/parseFloat($scope.selChln.chlnChgWt)) * $scope.difWt).toFixed(2));
								console.log("$scope.ar.arDrRcvrWt = "+$scope.ar.arDrRcvrWt);
								$scope.sedrServ.cnWSList = [];
								$scope.wtSrtCnmtDBFlag = false;
								$('div#wtSrtCnmtDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									title: "CNMT WT WISE SHORTAGE OF "+$scope.difWt+" Kg",
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
										$(this).dialog('destroy');
										$(this).hide();
										$scope.wtSrtCnmtDBFlag = true;
									}
								});
								$('div#wtSrtCnmtDB').dialog('open')
							}
						}else{
							
						}
					}
					
					
				}else{
					$scope.alertToast("Received Weight must be less than equal to challan total weight");
					$scope.ar.arRcvWt = null;
					recWt = 0;
				} 
			}
		}	 
	}


	$scope.saveWtSrtg = function(){
		console.log("enter into saveWtSrtg function = "+$scope.difWt);
		var tot = 0;
		var totRs = 0;
		console.log("$scope.sedrServ.cnWSList.length = "+$scope.sedrServ.cnWSList.length);
		console.log("$scope.resList.length = "+$scope.resList.length);
		if($scope.sedrServ.cnWSList.length === $scope.resList.length){
			for(var i=0;i<$scope.wtSrtg.length;i++){
				tot = tot + $scope.wtSrtg[i];
			}
			console.log("tot  === "+tot);
			console.log("difWt === "+$scope.difWt);
			if(parseFloat(tot) === parseFloat($scope.difWt)){
				for(var i=0;i<$scope.wtSrtgRs.length;i++){
					totRs = totRs + parseFloat($scope.wtSrtgRs[i]);
					console.log("totRs = "+totRs);
				}
				console.log("totRs = "+totRs);
				$scope.ar.arWtShrtg = parseFloat((totRs).toFixed(2));
				console.log("$scope.ar.arWtShrtg = "+$scope.ar.arWtShrtg);
				$('div#wtSrtCnmtDB').dialog('close')
				
			}else{
				$scope.alertToast("Please enter complete detail of "+$scope.difWt+" kg wt shortage");
			}
		}else{
			$scope.ar.arRcvWt = 0;
			$scope.alertToast("Please enter correct wt shortage")
			$scope.sedrServ.cnWSList = [];
			$scope.wtSrtg = [];
			$scope.wtSrtgRs = [];
			$('div#wtSrtCnmtDB').dialog('close')
		}
	}
	
	
	$scope.saveQSrtg = function(){
		console.log("enter into saveQSrtg funciton = "+$scope.difPkg);
		var tot = 0;
		var totRs = 0;
		
		if($scope.sedrServ.cnWSList.length === $scope.resList.length){
			for(var i=0;i<$scope.qSrtg.length;i++){
				tot = tot + $scope.qSrtg[i];
			}
			console.log("tot  === "+tot);
			console.log("difPkg === "+$scope.difPkg);
			if(tot === $scope.difPkg){
				for(var i=0;i<$scope.qSrtgRs.length;i++){
					totRs = totRs + parseFloat($scope.qSrtgRs[i]);
				}
				console.log("totRs = "+totRs);
				$scope.ar.arWtShrtg = parseFloat((totRs).toFixed(2));
				console.log("$scope.ar.arWtShrtg = "+$scope.ar.arWtShrtg);
				$('div#qSrtCnmtDB').dialog('close')
				
			}else{
				$scope.alertToast("Please enter complete detail of "+$scope.difPkg+" pkg shortage");
			}	
		}else{
			$scope.ar.arRcvWt = 0;
			$scope.alertToast("Please enter correct wt shortage")
			$scope.sedrServ.cnWSList = [];
			$scope.qSrtg = []; 
			$scope.qSrtgRs = [];
			
			$('div#qSrtCnmtDB').dialog('close')
		}
	}
	
	
	
	$scope.calWtSrtgByCN = function(cnCode,vog,actWt,ws){
		console.log("enter into calWtSrtgByCN function");
		var rs = 0.0;
		console.log("actWt=="+actWt);
		if(actWt>0 || actWt<0)//by kamal
			rs = parseFloat(parseFloat(vog/actWt) * parseFloat(ws));
		console.log("rs=="+rs);
		$scope.wtSrtgRs.push(rs);
		var map = {
				"cnmtCode" : cnCode,
				"srtg"     : ws
		};
		$scope.sedrServ.cnWSList.push(map);
	}
	
	
	$scope.calQSrtgByCN = function(cnCode,vog,actPkg,qs){
		console.log("enter into calQSrtgByCN function");
		var rs = 0.0;
		rs = parseFloat(parseFloat(vog/actPkg) * parseFloat(qs));
		$scope.qSrtgRs.push(rs);
		var map = {
				"cnmtCode" : cnCode,
				"srtg"     : qs
		};
		$scope.sedrServ.cnWSList.push(map);
	}
	
	
	
	$scope.checkArPkg = function(){
		console.log("enter into checkArPkg function");
		if(!angular.isUndefined($scope.ar.arPkg) || !($scope.ar.arPkg === null)){
			if($scope.ar.arPkg < $scope.chlnPkg){
				if($scope.resList.length > 0){
					if($scope.resList.length === 1){
						if($scope.resList[0].contType === "Q"){
							console.log("$scope.resList[0].cnmt.cnmtVOG ---->"+$scope.resList[0].cnmt.cnmtVOG);
							$scope.difPkg = ($scope.chlnPkg - $scope.ar.arPkg);
							console.log("difPkg = "+$scope.difPkg);
							$scope.ar.arWtShrtg = parseFloat(((parseFloat($scope.resList[0].cnmt.cnmtVOG) / parseFloat($scope.selChln.chlnNoOfPkg)) * $scope.difPkg).toFixed(2));
							console.log("$scope.ar.arWtShrtg = "+$scope.ar.arWtShrtg);
							$scope.ar.arDrRcvrWt = parseFloat(((parseFloat($scope.selChln.chlnFreight)/parseFloat($scope.selChln.chlnChgWt)) * $scope.difWt).toFixed(2));
							console.log("$scope.ar.arDrRcvrWt = "+$scope.ar.arDrRcvrWt);
							var map = {
									"cnmtCode" : $scope.resList[0].cnmt.cnmtCode,
									"srtg"     : difPkg
							};
							
							$scope.ar.arWtSrtCnmt = [];
							$scope.ar.arWtSrtCnmt.push(map);
						}
					}else if($scope.resList.length > 1){
						if($scope.resList[0].contType === "Q"){
							$scope.difPkg = ($scope.chlnPkg - $scope.ar.arPkg);
							console.log("difPkg = "+$scope.difPkg);
							$scope.ar.arDrRcvrWt = parseFloat(((parseFloat($scope.selChln.chlnFreight)/parseFloat($scope.selChln.chlnChgWt)) * $scope.difWt).toFixed(2));
							console.log("$scope.ar.arDrRcvrWt = "+$scope.ar.arDrRcvrWt);
							$scope.qSrtCnmtDBFlag = false;
							$('div#qSrtCnmtDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								title: "CNMT Quantity Wise Shortage Of "+$scope.difPkg+" Pkg",
								show: UDShow,
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
									$(this).dialog('destroy');
									$(this).hide();
									$scope.qSrtCnmtDBFlag = true;
								}
							});
							$('div#qSrtCnmtDB').dialog('open')
						}
					}else{
						
					}
				}
				console.log("$scope.ar.arPkg is valid");
			}else if(parseInt($scope.ar.arPkg) === parseInt($scope.chlnPkg)){
				console.log("$scope.ar.arPkg = $scope.chlnPkg");
			}else{
				$scope.ar.arPkg = null;
				$scope.alertToast("package must be less than no of package in challan");
			} 
		}	 
	}

	$scope.fillArCode = function(arRepType){
		console.log("Entered into fillArCode function---"+arRepType);
		if(arRepType === "Manual"){
			console.log("I m manual");
			$scope.manualArCodeFlag = false;
			$scope.compArCodeFlag = true;
			$scope.issuDtRqrFlg = true;
			$scope.issueDtHideFlg=false;
			$scope.ar.arIssueDt="";
		}else if(arRepType === "Computerized"){
			console.log("I m computerized");
			$scope.manualArCodeFlag = true;
			$scope.compArCodeFlag = false;
			$scope.issuDtRqrFlg = true;
			$scope.issueDtHideFlg=true;
			$scope.ar.arIssueDt= $scope.curntDt;
			//$scope.arIssueDt=date
		}
	}
	
	
	$scope.getFYear = function(){
		$scope.curntDt=$filter('date')(new Date, "yyyy-MM-dd");
		console.log("date"+$scope.curntDt);
		var response = $http.get($scope.projectName+'/getCurntFYear');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("Entered Date in current financial year");
				console.log(data.fYear.fyFrmDt);
				console.log(data.fYear.fyToDt);
				$scope.fyFrmDt=data.fYear.fyFrmDt;
				$scope.fyToDt=data.fYear.fyToDt;
			}else{
				$scope.alertToast("There is no any current Financial year");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.alertToast("There is some problem in fetching date");
		});
	}();
	
	$scope.isInFY = function(){
		console.log("isInFY------>Kamal");
		if($scope.ar.arIssueDt<$scope.fyFrmDt || $scope.ar.arIssueDt>$scope.fyToDt){
			$scope.ar.arIssueDt="";
			$scope.alertToast("Date should be in current Financial year");
		}
	}

	$scope.unldDtInFY= function(){
		console.log("unldDtInFY------>Kamal");
		if($scope.ar.arDt<$scope.fyFrmDt || $scope.ar.arDt>$scope.fyToDt){
			$scope.ar.arDt="";
			$scope.alertToast("Date should be in current Financial year");
		}
	}

	
	
	
	$scope.calLateDel = function(){
		console.log("enter into calLateDel function");
		$scope.ar.arLateDelivery = 0;
		$scope.ar.arLateAck = 0;
		if($scope.ar.arRepDt<$scope.fyFrmDt || $scope.ar.arRepDt>$scope.fyToDt){
			$scope.ar.arRepDt="";
			$scope.alertToast("Date should be in current Financial year");
		}else if(angular.isUndefined($scope.selChln)){
			$scope.alertToast("Please select Challan Code");
			$scope.ar.arRepDt = "";
		}else if($scope.ar.arRepDt < $scope.selChln.chlnDt){
			$scope.ar.arRepDt = "";
			$scope.alertToast("Reporting Date must be greater than challan Date");
		}else if($scope.ar.arRepDt > $scope.ar.arIssueDt){
			$scope.ar.arRepDt = "";
			$scope.alertToast("Reporting Date must be less than Arrival Issue Date");
		}else if(angular.isUndefined($scope.ar.arRepDt)){
			$scope.alertToast("Please select Reporting Date");
		}else{
			
			var dateInfo = {
					"chlnDt" : $scope.selChln.chlnDt,
					"arDt"   : $scope.ar.arRepDt,
					"arIssueDt" : $scope.ar.arIssueDt
			};
			
			var response = $http.post($scope.projectName+'/calLateDelFrLA',dateInfo);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					var diff1 = data.diff1;
					var diff2 = data.diff2;
					
					$scope.ar.arLateDelivery = (diff1 - $scope.selChln.chlnTimeAllow) * 500;
					// late ack:- we can comment below line if we want remove late ack 
					$scope.ar.arLateAck      = (diff2 - (($scope.selChln.chlnTimeAllow * 2) + 5)) * 50;
					
				}else{
					
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	}
	
	
	
	
	$scope.calClaim = function(){
		console.log("enter into calClaim function");
		$scope.ar.arClaim = 0;
		if(parseFloat($scope.ar.arLateAck) < 0){
			$scope.ar.arLateAck = 0;
		}
		if(parseFloat($scope.ar.arLateDelivery) < 0){
			$scope.ar.arLateDelivery = 0;
		}
		//$scope.ar.arClaim = $scope.ar.arExtKm + $scope.ar.arOvrHgt + $scope.ar.arWtShrtg + $scope.ar.arPenalty + $scope.ar.arOther;
		$scope.ar.arClaim = parseFloat(($scope.ar.arWtShrtg + $scope.ar.arDrRcvrWt + $scope.ar.arLateAck + $scope.ar.arLateDelivery + $scope.ar.arOther).toFixed(2));
		console.log("$scope.ar.arClaim  = "+$scope.ar.arClaim);
	}
	
	
	$scope.fillArSrtgDmgType=function(){
		if($scope.ar.arWtShrtg>0){
			$scope.ar.arSrtgDmg="SHORTAGE";
		}
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchData();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 


	


}]);