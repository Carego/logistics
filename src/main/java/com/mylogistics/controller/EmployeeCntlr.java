package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.User;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class EmployeeCntlr {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@RequestMapping(value = "/submitEmployee", method = RequestMethod.POST)
     public @ResponseBody Object submitEmployee(@RequestBody Employee employee){
		System.out.println("enter into submitEmployee function---->");
		Map<String,Object> map = new HashMap<String,Object>();
		employee.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		employee.setUserCode(currentUser.getUserCode());
		employee.setbCode(currentUser.getUserBranchCode());
		
		/*String empCode = null;
		String lastFiveChar = null;
		int lastEmpId;
		String faCode=null;
		long totalRows = employeeDAO.totalCount();
	                                              
		Map<String,String> map = new HashMap<String,String>();
	     
		if(!(totalRows == -1)){
	    	 
			if(totalRows==0){
				lastFiveChar="00001";
				empCode = "1";
			}else{
				List<Employee> empList =employeeDAO.getLastEmployeeRow();
				System.out.println("EmployeeID is"+ empList.get(0).getEmpId());
			    		
				lastEmpId = empList.get(0).getEmpId();
				int empId = lastEmpId+1;
				empCode = String.valueOf(empId);
				int end = 100000+empId;
				lastFiveChar = String.valueOf(end).substring(1,6);	
			}
			
			employee.setEmpCode(empCode); 
			
			//String branchCode = employee.getBranchCode();
			//String branchName = branchDAO.getBrNameByBrCode(branchCode);
			
			//String empCodeTemp = CodePatternService.empCodeGen(employee,lastFiveChar,branchName);
			String empCodeTemp = CodePatternService.empCodeGen(employee,lastFiveChar);
			System.out.println("Retutning from CodePatternService--->"+empCodeTemp);
			employee.setEmpCodeTemp(empCodeTemp);
			
			
			List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_EMP);
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			employee.setfAParticular(fAParticular);*/
		
			int empId = employeeDAO.saveEmployeeToDB(employee);    
			
			if(empId > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	/*		if (empId>=0) {
				System.out.println("employee saved with id------------------"+empId);
				empId=empId+100000;
				String subEmpId = String.valueOf(empId).substring(1,6);
				faCode=fapIdStr+subEmpId;
				System.out.println("faCode code is------------------"+faCode);
				employee.setEmpFaCode(faCode);
				int temp= employeeDAO.updateEmployeeToDB(employee);
				if(temp >= 0){
					System.out.println("employee data updated successfully");
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
				
				FAMaster fAMaster = new FAMaster();
				fAMaster.setFaMfaCode(faCode);
				fAMaster.setFaMfaType(ConstantsValues.FAP_EMP);
				fAMaster.setFaMfaName(employee.getEmpName());
				fAMaster.setUserCode(currentUser.getUserCode());
				fAMaster.setbCode(currentUser.getUserBranchCode());
				
				int tmp = faMasterDAO.saveFaMaster(fAMaster);
				if(tmp >= 0){
					System.out.println("FAMaster is updated successfully for employee");
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		} else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		return map;
	}
	    
	 @RequestMapping(value = "/employeeDetails", method = RequestMethod.POST)
	  public @ResponseBody Object employeeDetails(@RequestBody String empCode){
		 
	    Employee employee = new Employee();
		List<Employee> list = employeeDAO.retrieveEmployee(empCode);
		//employeeDAO.getCmpltEmp(empCode);  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			employee = list.get(0);
		/*	employee.setPrBranch(null);
			employee.setfAParticular(null);*/
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("employee",employee);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	   }
	    
	  @RequestMapping(value = "/getDesignation" , method = RequestMethod.GET)  
	  public @ResponseBody Object getDesignation(){
			
	  String designation[] = {"Android Developer",
			"Clerk",
			"Content Writer",
		    "Developer",
		    "Director",
		    "Human Resources",
		    "Intern",
		    "IOS Developer",
		    "IT Professional",
		    "Java Developer",
		    "JavaScript Developer",
		    "Manager",
		    "Network Analyst",
		    "Programmer",
		    "Software Engineer",
		    "Software Tester",
		    "SEO Services",
		    "UI Designer",
		    };
		return designation;
		} 
		
		@RequestMapping(value="/getBranchDataForEmp",method = RequestMethod.POST)
		public @ResponseBody Object getBranchDataForEmp(){
			
			System.out.println("Controller called of Get Branch Data");
			List<Branch> branchList = branchDAO.getAllActiveBranches();
			Map<String,Object> map = new HashMap<String,Object>();
			if (!(branchList.isEmpty())) {
				  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	  			map.put("list",branchList );
	  		}else {
	  		  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;		
		}
		
		@RequestMapping(value = "/getAllEmployees", method = RequestMethod.POST)
		  public @ResponseBody Object getAllEmployees(){
			
			List<Employee> empCodeList = employeeDAO.getAllEmployee();
	  		
			Map<String,Object> map = new HashMap<String , Object>();
			if (!(empCodeList.isEmpty())) {
			    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			    map.put("empCodeList",empCodeList);
			}else{
			  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			   return map;	
		 }
	
		
		@RequestMapping(value = "/updateEmpByOptr", method = RequestMethod.POST)
	     public @ResponseBody Object updateEmpByOptr(@RequestBody Employee employee){
			System.out.println("enter into updateEmpByOptr function---->");
			Map<String,Object> map = new HashMap<String,Object>();
			//int res = employeeDAO.updateEmployeeToDB(employee);
			int res = employeeDAO.updateEmpPart(employee);
			if(res > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;
		}	
		
		@RequestMapping(value="/getEmpList", method=RequestMethod.POST)
		public @ResponseBody Object getEmpList(){
			System.out.println("getEmpList()");
			Map<String, Object> map = new HashMap<>();
			
			List<Map<String, Object>> empList = employeeDAO.getEmpList();
			
			if (!empList.isEmpty()) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("empList", empList);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}
		
}
