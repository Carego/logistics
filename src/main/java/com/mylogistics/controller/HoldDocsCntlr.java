package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.RemarksDAO;
import com.mylogistics.model.Remarks;

@Controller
public class HoldDocsCntlr {
	
	@Autowired
	private RemarksDAO remarksDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public static Logger logger = Logger.getLogger(HoldDocsCntlr.class);
	
	@RequestMapping(value = "/getDocsFrHld", method = RequestMethod.POST)
	public @ResponseBody Object getDocsFrHld(@RequestBody Map<String,Object> clientMap){
		
		System.out.println("docSer"+clientMap.get("docSer"));
		System.out.println("docNO"+clientMap.get("docNo"));
		Map<String,Object> map=new HashMap<String, Object>();
		if(clientMap.get("docSer").toString().equalsIgnoreCase("CH")){
			System.out.println("hiiii");
			if(clientMap.get("docNo")!=null && clientMap.get("docNo")!=""){
				System.out.println("some value");
				int i=remarksDAO.getVerify(clientMap.get("docNo").toString());
				if(i==1){
					map.put("result", "success");
					map.put("msg", "You can hold this challan for balance payment");	
				}else if(i==2){
					map.put("result", "inRmrk");
					map.put("msg", "This challan is already on hold");
				}else{
					map.put("result", "error");
					map.put("msg", "Please Enter valid challan");
				}
			}else{
				map.put("result", "error");
				map.put("msg", "Please enter challan code");
			}
			
			
		}else{
			map.put("msg", "please select challan option");
		}
		
		return map;
	}
	
	@RequestMapping(value = "/holdDocs", method = RequestMethod.POST)
	public @ResponseBody Object holdDocs(@RequestBody Map<String,Object> clientMap){
		System.out.println("docNo="+clientMap.get("docNo"));
		System.out.println("docNo="+clientMap.get("docSer"));
		System.out.println("docNo="+clientMap.get("date"));
		System.out.println("docNo="+clientMap.get("remark"));
		logger.error("holdDocs()");
		Map<String, String> response = new HashMap<>();
		Session session=null;
		try{
			 session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			
			int i=remarksDAO.holdChallan(clientMap,session);
			System.out.println("i==="+i);
			System.out.println("session==="+session);
			System.out.println("transaction==="+transaction);
			if(i==1 || i==2){
				
				if(i==1){
					response.put("result", "success");
					response.put("msg", "Challan succefully hold again");
					transaction.commit();
				}else if(i==2){
					response.put("result", "success");
					response.put("msg", "Challan succefully hold ");
					transaction.commit();
				}
			}else{
				transaction.rollback();
				response.put("result", "error");
				response.put("msg", "there is some problm to complete the task");
			}
			
			System.out.println("i==="+i);
			System.out.println("session==="+session);
			System.out.println("transaction==="+transaction);
		}catch(Exception e){
			logger.error("Exception e"+e);
		}finally{
			System.out.println("session==="+session);
			session.clear();
			session.close();
		}
		
		return response;
	}
	
	
	

	@RequestMapping(value = "/getChlnFrUnhold", method = RequestMethod.POST)
	public @ResponseBody Object getChlnFrUnhold(){
		
		logger.error("getChlnFrUnhold()");
		Map<String, Object> map = new HashMap<>();
			
			map=remarksDAO.getHoldChln();
			
		return map;
	}
	
	//getChlnObjFrUnhld
	

	@RequestMapping(value = "/getObjFrUnhldDocs", method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> getObjFrUnhldDocs(@RequestBody String chlnCode){
		logger.error("getObjFrUnhldDocs()");
		
		Map<String, Object> map=new HashMap<>();
		List<Remarks> list=remarksDAO.getObjFrUnhldDocs(chlnCode);
		System.out.println("list="+list);
		if(!list.isEmpty()){
			map.put("result", "success");
			map.put("list", list.get(0));
		}else{
			map.put("result", "error");
		}
		return map;
	}
		//allowDocsFrHold
		
	

	@RequestMapping(value = "/allowDocsFrHold", method = RequestMethod.POST)
	public @ResponseBody Object allowDocsFrHold(@RequestBody Remarks remarks){
		logger.error("holdDocs()");
		Map<String,String> map=new HashMap<>();
		Session session=null;
		try{
			 session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			
			int i=remarksDAO.allowDocsFrHold(remarks,session);
			System.out.println("i==="+i);
			System.out.println("session==="+session);
			System.out.println("transaction==="+transaction);
			if(i==1){
				transaction.commit();
				map.put("result", "success");
			}else{
				transaction.rollback();
				map.put("result", "error");
			}
			
			System.out.println("i==="+i);
			System.out.println("session==="+session);
			System.out.println("transaction==="+transaction);
		}catch(Exception e){
			logger.error("Exception e"+e);
		}finally{
			System.out.println("session==="+session);
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
}
