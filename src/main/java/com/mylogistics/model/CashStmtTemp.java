package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "cashstmttemp")
public class CashStmtTemp {
	
	@Id
	@Column(name="csId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int csId;

	@Column(name="csBrhFaCode")
	@NotNull(message = "CsBrhFaCode can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "CsBrhFaCode can not be empty CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String csBrhFaCode;
	
	@Column(name="csIsView" , columnDefinition="boolean default false")
	private boolean csIsView;
	
	@Column(name="csDescription")
	@NotNull(message = "Description can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "Description can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String csDescription;
	
	@Column(name="csTvNo")
	@NotNull(message = "CsTvNo can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "CsTvNo can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String csTvNo;
	
	@Column(name="csDrCr")
	@NotNull(message = "CsDrCr can not be null !", groups = CashStmtTempBillAdvInsert.class)
	private char csDrCr;
	
	@Column(name="csAmt")
	@Min(value = 1, message = "Cs Amount should be greater than 0 for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private double csAmt;
	
	@Column(name="csVouchNo")
	private int csVouchNo;
	
	@Column(name="csVouchType")
	@NotNull(message = "CsVoucherType can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "CsVoucherType can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String csVouchType;
	
	@Column(name="csPayTo")
	private String csPayTo;
	
	@Column(name="csDt")
	@NotNull(message = "CS Date can not be null for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private Date csDt;

	@Column(name="userCode")
	@NotNull(message = "User code can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "User code can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String userCode;
	
	@Column(name="bCode")
	@NotNull(message = "BCode can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "BCode code can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String bCode;
	
	@Column(name ="csFaCode")
	@NotNull(message = "CsFaCode can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "CsFaCode can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String csFaCode;
	
	@Column(name ="csType")
	@NotNull(message = "CsType can not be blank for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	@NotEmpty(message = "CsType code can not be empty for CashStmtTemp !", groups = CashStmtTempBillAdvInsert.class)
	private String csType;
	
	@Column(name="csChequeType")
	private char csChequeType;
	
	@Column(name="csMrNo")
	private String csMrNo;
	
	@Column(name="csLhpvTempNo")
	private String csLhpvTempNo;
	
	@Column(name = "csIsClose")
	private boolean csIsClose;
	
	@Column(name = "csIsVRev" , columnDefinition = "boolean default false")
	private boolean csIsVRev;
	
	
	@Column(name = "csSFId" , columnDefinition = "int default -1")
	private int csSFId; 
	
	@Column(name = "csTravIdList")
	private ArrayList<Integer> csTravIdList = new ArrayList<Integer>();

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name = "payMode")
	private String payMode;
	
	@Column(name = "csUpdate" , columnDefinition = "boolean default false")
	private boolean csUpdate;
	
	public int getCsSFId() {
		return csSFId;
	}


	public void setCsSFId(int csSFId) {
		this.csSFId = csSFId;
	}


	public ArrayList<Integer> getCsTravIdList() {
		return csTravIdList;
	}


	public void setCsTravIdList(ArrayList<Integer> csTravIdList) {
		this.csTravIdList = csTravIdList;
	}


	public boolean isCsIsVRev() {
		return csIsVRev;
	}


	public void setCsIsVRev(boolean csIsVRev) {
		this.csIsVRev = csIsVRev;
	}
	
	public Date getCsDt() {
		return csDt;
	}


	public void setCsDt(java.sql.Date date) {
		this.csDt = date;
	}


	public int getCsId() {
		return csId;
	}


	public void setCsId(int csId) {
		this.csId = csId;
	}


	/*public CashStmtStatus getCsNo() {
		return csNo;
	}


	public void setCsNo(CashStmtStatus csNo) {
		this.csNo = csNo;
	}*/


	public String getCsDescription() {
		return csDescription;
	}


	public void setCsDescription(String csDescription) {
		this.csDescription = csDescription;
	}


	public String getCsTvNo() {
		return csTvNo;
	}


	public void setCsTvNo(String csTvNo) {
		this.csTvNo = csTvNo;
	}


	public char getCsDrCr() {
		return csDrCr;
	}


	public void setCsDrCr(char csDrCr) {
		this.csDrCr = csDrCr;
	}


	public double getCsAmt() {
		return csAmt;
	}


	public void setCsAmt(double csAmt) {
		this.csAmt = csAmt;
	}


	/*public ChequeLeaves getcLeaves() {
		return cLeaves;
	}


	public void setcLeaves(ChequeLeaves cLeaves) {
		this.cLeaves = cLeaves;
	}*/


	public int getCsVouchNo() {
		return csVouchNo;
	}


	public void setCsVouchNo(int csVouchNo) {
		this.csVouchNo = csVouchNo;
	}


	public String getCsVouchType() {
		return csVouchType;
	}


	public void setCsVouchType(String csVouchType) {
		this.csVouchType = csVouchType;
	}


	public String getCsPayTo() {
		return csPayTo;
	}


	public void setCsPayTo(String csPayTo) {
		this.csPayTo = csPayTo;
	}


	public String getUserCode() {
		return userCode;
	}


	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}


	public String getbCode() {
		return bCode;
	}


	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getCsType() {
		return csType;
	}


	public void setCsType(String csType) {
		this.csType = csType;
	}


	public char getCsChequeType() {
		return csChequeType;
	}


	public void setCsChequeType(char csChequeType) {
		this.csChequeType = csChequeType;
	}


	public String getCsMrNo() {
		return csMrNo;
	}


	public void setCsMrNo(String csMrNo) {
		this.csMrNo = csMrNo;
	}


	public Calendar getCreationTS() {
		return creationTS;
	}


	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public boolean isCsIsClose() {
		return csIsClose;
	}


	public void setCsIsClose(boolean csIsClose) {
		this.csIsClose = csIsClose;
	}


	public String getCsFaCode() {
		return csFaCode;
	}


	public void setCsFaCode(String csFaCode) {
		this.csFaCode = csFaCode;
	}


	public String getCsLhpvTempNo() {
		return csLhpvTempNo;
	}


	public void setCsLhpvTempNo(String csLhpvTempNo) {
		this.csLhpvTempNo = csLhpvTempNo;
	}


	public boolean isCsIsView() {
		return csIsView;
	}


	public void setCsIsView(boolean csIsView) {
		this.csIsView = csIsView;
	}


	public String getCsBrhFaCode() {
		return csBrhFaCode;
	}


	public void setCsBrhFaCode(String csBrhFaCode) {
		this.csBrhFaCode = csBrhFaCode;
	}

	public boolean isCsUpdate() {
		return csUpdate;
	}


	public void setCsUpdate(boolean csUpdate) {
		this.csUpdate = csUpdate;
	}


	public String getPayMode() {
		return payMode;
	}


	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	
	public interface CashStmtTempBillAdvInsert{		
	}
	
}
