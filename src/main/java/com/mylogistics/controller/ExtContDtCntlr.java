package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;
import com.mylogistics.services.ConstantsValues;

@Controller
public class ExtContDtCntlr {

	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	
/*	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired
	private StationDAO stationDAO;*/
	
	
	@RequestMapping(value = "/getCCFrExDt", method = RequestMethod.POST)
	public @ResponseBody Object getCCFrExDt() {
		System.out.println("enter into getCCFrExDt function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> finalList = new ArrayList<String>();
		List<String> regFaList = regularContractDAO.getRegFaCode();
		List<String> dlyFaList = dailyContractDAO.getDlyFaCode();
		/*List<VehicleType> vehList = vehicleTypeDAO.getVehicleType();
		List<Station> stnList = stationDAO.getStationData();*/
		if(!regFaList.isEmpty()){
			for(int i=0;i<regFaList.size();i++){
				finalList.add(regFaList.get(i));
			}
		}

		if(!dlyFaList.isEmpty()){
			for(int i=0;i<dlyFaList.size();i++){
				finalList.add(dlyFaList.get(i));
			}
		}
		
		if(!finalList.isEmpty()){
			map.put("list",finalList);
			/*map.put("vehList",vehList);
			map.put("stnList",stnList);*/
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getContFrExtDt", method = RequestMethod.POST)
	public @ResponseBody Object getContFrExtDt(@RequestBody String contFaCode)throws Exception {
		System.out.println("enter into getCCFrToStn function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(contFaCode != null){
			String subStr = contFaCode.substring(0,2);
			if(subStr.equalsIgnoreCase("05")){
				List<RegularContract> regList = regularContractDAO.getRegContByFaCode(contFaCode);
				List<Customer> custList = customerDAO.getCustomer(regList.get(0).getRegContBLPMCode());
				String branchName = branchDAO.getBrNameByBrCode(regList.get(0).getBranchCode());
				List<Station> stnList = stationDAO.retrieveStation(regList.get(0).getRegContFromStation());
				
				Map<String,Object> contract = new HashMap<String,Object>();
				contract.put("contFaCode",regList.get(0).getRegContFaCode());
				contract.put("contCode",regList.get(0).getRegContCode());
				if(!custList.isEmpty()){
					contract.put("custName",custList.get(0).getCustName());
					contract.put("custFaCode",custList.get(0).getCustFaCode());
				}
				contract.put("contBranch",branchName);
				contract.put("contFrStn",stnList.get(0).getStnName());
				contract.put("contFrStnCode",regList.get(0).getRegContFromStation());
				contract.put("contFrDt",regList.get(0).getRegContFromDt());
				contract.put("contToDt",regList.get(0).getRegContToDt());
				contract.put("contType",regList.get(0).getRegContType());
				contract.put("contValue",regList.get(0).getRegContValue());
				contract.put("contWeight",regList.get(0).getRegContWt());
				contract.put("contDDL",regList.get(0).getRegContDdl());
				contract.put("contInsBy",regList.get(0).getRegContInsuredBy());
				contract.put("contRemarks",regList.get(0).getRegContRemark());
	
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("cotract",contract);
				
			}else if(subStr.equalsIgnoreCase("04")){
				List<DailyContract> dlyList = dailyContractDAO.getDlyContByFaCode(contFaCode);
				List<Customer> custList = customerDAO.getCustomer(dlyList.get(0).getDlyContBLPMCode());
				String branchName = branchDAO.getBrNameByBrCode(dlyList.get(0).getBranchCode());
				List<Station> stnList = stationDAO.retrieveStation(dlyList.get(0).getDlyContFromStation());
				
				Map<String,Object> contract = new HashMap<String,Object>();
				contract.put("contFaCode",dlyList.get(0).getDlyContFaCode());
				contract.put("contCode",dlyList.get(0).getDlyContCode());
				if(!custList.isEmpty()){
					contract.put("custName",custList.get(0).getCustName());
					contract.put("custFaCode",custList.get(0).getCustFaCode());
				}
				contract.put("contBranch",branchName);
				contract.put("contFrStn",stnList.get(0).getStnName());
				contract.put("contFrStnCode",dlyList.get(0).getDlyContFromStation());
				contract.put("contFrDt",dlyList.get(0).getDlyContStartDt());
				contract.put("contToDt",dlyList.get(0).getDlyContEndDt());
				contract.put("contType",dlyList.get(0).getDlyContType());
				//contract.put("contValue",dlyList.get(0).getDlyContValue());
				//contract.put("contWeight",regList.get(0).getRegContWt());
				contract.put("contDDL",dlyList.get(0).getDlyContDdl());
				//contract.put("contInsBy",dlyList.get(0).getDlyContInsuredBy());
				contract.put("contRemarks",dlyList.get(0).getDlyContRemark());
				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("cotract",contract);
				
			}else{
				System.out.println("invalid contract code");
				map.put("msg","invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","contract code is not valid");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value="/extDtOfCont",method = RequestMethod.POST)
	public @ResponseBody Object extDtOfCont(@RequestBody Map<String,Object> clientMap){
		System.out.println("Entered into extDtOfCont function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		String contFaCode = (String) clientMap.get("contCode");
		Date date = Date.valueOf((String) clientMap.get("date"));    
		
		if(contFaCode != null){
			String subStr = contFaCode.substring(0,2);
			if(subStr.equalsIgnoreCase("05")){
				int res = regularContractDAO.extDtOfCont(contFaCode,date);
				if(res > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else if(subStr.equalsIgnoreCase("04")){
				int res = dailyContractDAO.extDtOfCont(contFaCode,date);
				if(res > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				System.out.println("invalid contract code");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
	
		return map;	
	}
	
	
}
