package com.mylogistics.DAOImpl.webservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.dev.ReSave;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.werservice.VehicleWebServiceDao;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.VehicleVendorMstrCNTS;
import com.mylogistics.constants.webservice.VehicleAppCNTS;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Owner;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.model.webservice.VehicleApp;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VehVendorService;

public class VehicleWebServiceDaoImpl implements VehicleWebServiceDao{

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HttpSession httpSession;
	private Session session;
	private Transaction transaction;
	public static Logger logger = Logger.getLogger(VehicleWebServiceDaoImpl.class);
	
	public VehicleWebServiceDaoImpl(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	public Map<String, Object> getOwnsBrksWs(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into getOwners()...");	
		try{
			// Owner List
			List<Map<String, Object>> appOwnList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> penOwnList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> finalOwnList = new ArrayList<Map<String, Object>>();
			
			// Broker List
			List<Map<String, Object>> appBrkList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> penBrkList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> finalBrkList = new ArrayList<Map<String, Object>>();
			
			session = this.sessionFactory.openSession();
			// Fetching Owners
			appOwnList = session.createQuery("SELECT ownId AS ownId, ownCode AS ownCode, ownName AS ownName, ownIsPanImg AS ownIsPanImg FROM Owner")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			logger.info("Owner list found !");
			// Pending Owner
			penOwnList = session.createQuery("SELECT ownBrkId AS ownId, ownBrkCode As ownCode, ownBrkName AS ownName, isPanImg AS ownIsPanImg FROM OwnBrkApp WHERE  isPending = :isPending AND type = :type")
					.setParameter("isPending", true)
					.setParameter("type", "owner")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			logger.info("Pending Owner list found !");
			finalOwnList.addAll(appOwnList);			
			// Add pending Owner
			if(! penOwnList.isEmpty())
				finalOwnList.addAll(penOwnList);
			logger.info("Final Owner List Complete !");
			// Fetching Brokers
			appBrkList = session.createQuery("SELECT brkId AS brkId, brkCode AS brkCode, brkName AS brkName, brkIsPanImg AS brkIsPanImg FROM Broker")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			// Pending Broker
			logger.info("Broker list found !");
			penBrkList = session.createQuery("SELECT ownBrkId AS brkId, ownBrkCode As brkCode, ownBrkName AS brkName, isPanImg AS brkIsPanImg FROM OwnBrkApp WHERE  isPending = :isPending AND type = :type")
							.setParameter("isPending", true)
							.setParameter("type", "broker")
							.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
							.list();
			logger.info("Pending Broker list found !");
			finalBrkList.addAll(appBrkList);			
			// Add pending Owner
			if(! penBrkList.isEmpty())
				finalBrkList.addAll(penBrkList);
			logger.info("Final Broker List Complete !");
			resultMap.put("ownerList", finalOwnList);
			resultMap.put("brokerList", finalBrkList);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}		
		logger.info("Exit from getOwners()...");
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}	
	
	public Map<String, Object> vehicleCheck(String rcNo){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into getOwners() - rcNo : "+rcNo);	
		try{
			session = this.sessionFactory.openSession();
			Criteria vehicleCriteria = session.createCriteria(VehicleVendorMstr.class);
			vehicleCriteria.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, rcNo));
			List list = vehicleCriteria.list();
			if(list.isEmpty()){
				logger.info("Vehicle does not exist !");
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			}else{
				logger.info("Vehicle found !");
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "Vehicle already exists !");
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		logger.info("Exit from vehicleCheck()...");
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	@Override
	public Map<String, Object> addNewVehicle(VehicleApp vehicleApp){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into addNewVehicle()...");
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(vehicleApp);
			transaction.commit();
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			resultMap.put("msg", "Vehicle has successfully added !");
			logger.info("Vehicle has successfully added !");
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		logger.info("Exit from addNewVehicle()...");
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	@Override
	public Boolean findAlreadyVehicle(String vRcNo){
		Boolean already = true;
		logger.info("Enter into findAlreadyVehicle()...");
		try{
			session = this.sessionFactory.openSession();
			List<VehicleApp> vehicleAppList = session.createCriteria(VehicleApp.class)
					.add(Restrictions.eq(VehicleAppCNTS.V_RC_NO, vRcNo))
					.list();
			if(vehicleAppList.isEmpty()){
				already = false;
				logger.info("Vehicle does not exist !");
			}else
				logger.info("Vehicle exists !");
		}catch(Exception e){
			logger.error("Exception : "+e);
		}		
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from findAlreadyVehicle()...");
		return already;
	}
	
	@Override
	public Map<String, Object> getPendingVehDt(String vvRcNo){
		logger.info("Enter into getPendingVehicle()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			
			List<VehicleVendorMstr> vehicleVendorMstrsList = session.createCriteria(VehicleVendorMstr.class)
					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, vvRcNo))
					.list();
			if(! vehicleVendorMstrsList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Vehicle already verified !");
				return resultMap;
			}
			
			List<VehicleApp> vehiclAppList = session.createCriteria(VehicleApp.class)
					.add(Restrictions.eq(VehicleAppCNTS.V_RC_NO, vvRcNo))
					.add(Restrictions.eq(VehicleAppCNTS.IS_PENDING, true))
					.list();
			if(vehiclAppList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Vehicle detail not found !");
				logger.info("Vehicle detail not found !");
			}else{
				VehicleApp vehicleApp = (VehicleApp)vehiclAppList.get(0);				
				if(vehicleApp.getvOwnCode().equalsIgnoreCase("") && vehicleApp.getvBrkCode().equalsIgnoreCase("")){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify Owner/Broker first !");
					resultMap.put("vehicle", vehicleApp);
					logger.info("Verify Owner/Broker first !");
				}else if(vehicleApp.getvOwnCode().equalsIgnoreCase("")){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify Owner first");
					resultMap.put("vehicle", vehicleApp);
					logger.info("Verify Owner first");
				}else if(vehicleApp.getvBrkCode().equalsIgnoreCase("")){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Verify Broker first !");
					resultMap.put("vehicle", vehicleApp);
					logger.info("Verify Broker first !");
				}else{ 
					resultMap.put("vehicle", vehicleApp);
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					logger.info("Vehicle detail found !");
				}
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from getPendingVehicle()...");
		return resultMap;
	}
	
	@Override
	public Map<String, Object> getPendingVehicle(){
		logger.info("Enter into getPendingVehicle()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			Criteria vehRcNoCriteria = session.createCriteria(VehicleApp.class)
				.add(Restrictions.eq(VehicleAppCNTS.IS_PENDING, true));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(VehicleAppCNTS.V_RC_NO));
			vehRcNoCriteria.setProjection(projectionList);
			List<String> rcNoList = vehRcNoCriteria.list();
			if(rcNoList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Pending vehicle not found !");
				logger.info("Pending vehicle not found !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("rcNoList", rcNoList);
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from getPendingVehicle()...");
		return resultMap;
	}
	
	@Override
	public Map<String, Object> verifyVehicle(VehVendorService vehVendorService){
		logger.info("Enter into verifyVehicle()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		try{
			User currentUser = (User) httpSession.getAttribute("currentUser");
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			if (vehVendorService.getOwnCode().equalsIgnoreCase("") &&	vehVendorService.getBrkCode().equalsIgnoreCase("") &&
					vehVendorService.getVehVenMstr() == null &&	vehVendorService.getVvRcNo() == null) {
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Vehicle is not verified !");
				return resultMap;
			} else if (vehVendorService.getVehVenMstr().getVvRcNo() != null) {
				
				Owner owner = null;
				Broker broker = null;
				List<Owner> ownerList = session.createCriteria(Owner.class)
						.add(Restrictions.eq(OwnerCNTS.OWN_CODE, vehVendorService.getOwnCode()))
						.list();
				if(ownerList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Owner not found !");
					return resultMap;
				}else
					owner = (Owner) ownerList.get(0);
				List<Broker> brokerList = session.createCriteria(Broker.class)
						.add(Restrictions.eq(BrokerCNTS.BRK_CODE, vehVendorService.getBrkCode()))
						.list();
				if(brokerList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Broker not found !");
					return resultMap;
				}else
					broker = (Broker) brokerList.get(0);
				
				VehicleVendorMstr vehicleVendorMstr = vehVendorService.getVehVenMstr();				
				
				
				vehicleVendorMstr.setUserCode(currentUser.getUserCode());
				vehicleVendorMstr.setbCode(currentUser.getUserBranchCode());
				vehicleVendorMstr.setBroker(broker);
				vehicleVendorMstr.setOwner(owner);
				
				int tempId = (Integer) session.save(vehicleVendorMstr);
				if (tempId>0) {
					owner.getVehicleVendorMstrList().add(vehicleVendorMstr);
					broker.getVehicleVendorMstrList().add(vehicleVendorMstr);
					session.update(owner);
					session.update(broker);
					List<VehicleApp> vehicleAppList = session.createCriteria(VehicleApp.class)
							.add(Restrictions.eq(VehicleAppCNTS.V_RC_NO, vehVendorService.getVehVenMstr().getVvRcNo()))
							.list();
					if(! vehicleAppList.isEmpty()){
						VehicleApp vehicleApp = (VehicleApp) vehicleAppList.get(0);
						//session.delete(vehicleApp);
						vehicleApp.setIsPending(false);
						session.update(vehicleApp);
					}
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("msg", "Vehicle is successfully verified !");
				}else{
					resultMap.put(ConstantsValues.ERROR, ConstantsValues.ERROR);
					resultMap.put("msg", "Vehicle is not verified!");
					logger.info("Vehicle is not verified !");
				}
			}else{
				resultMap.put(ConstantsValues.ERROR, ConstantsValues.ERROR);
				resultMap.put("msg", "Vehicle RC not found !");
				logger.info("Vehicle RC not found !");
			}
			transaction.commit();
			session.flush();
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		logger.info("Exit from verifyVehicle()...");
		return resultMap;
	}
	
}