'use strict';

var app = angular.module('application');

app.controller('VerifyCustomerCntlr',['$scope','$location','$http','$window',
                                      function($scope,$location,$http,$window){

	$scope.customer = {};
	$scope.selection=[];

	$scope.getIsVerify = function(){
		console.log("getIsVerify------>");
		var response = $http.post('/MyLogistics/verifyCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("---------->list==>"+data.list)
				$scope.customers= data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}


	$scope.toggleSelection = function toggleSelection(custCode) {
		var idx = $scope.selection.indexOf(custCode);

		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}	
		// is newly selected
		else {
			$scope.selection.push(custCode);
		}	
	}	

	$scope.update = function(){
		console.log("enter into update with $scope.selection = "+$scope.selection);
		$scope.show=true;
		var response = $http.post('/MyLogistics/updateIsVerifyCustomer',$scope.selection.toString());
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				/*  console.log("---------->message==>"+data.message);*/
				/*$location.path("/admin/verifyCustomer");*/
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});

	}
	$scope.getIsVerify();
}]);