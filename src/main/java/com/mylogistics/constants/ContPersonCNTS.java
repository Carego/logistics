package com.mylogistics.constants;

public class ContPersonCNTS {
	
	public static final String CP_NAME = "cpName";
	
	public static final String CP_MOBILE = "cpMobile";
	
	public static final String CP_PHONE = "cpPhone";
	
	public static final String CP_PAN_NO = "cpPanNo";
	
	public static final String CP_REF_CODE = "cpRefCode";
	
	public static final String USER_CODE = "userCode";

	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
	

}
