package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ContractRights;


@Repository
public interface ContractRightsDAO {
	
	//Sanjana: save contract rights to database
	public int saveContractRights(ContractRights contractRights);

	//Sanjana: retrieves contract rights from database on the basis of entered cont code
	public List<ContractRights> getContRight(String contCode);
	
	//Sanjana: updates contract rights
	public int updateContractRights(ContractRights contractRights);
}
