package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.DlyContAuthDAO;
import com.mylogistics.constants.DlyContAuthCNTS;
import com.mylogistics.model.DlyContAuth;

public class DlyContAuthDAOImpl implements DlyContAuthDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public DlyContAuthDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 public int saveDCA(DlyContAuth dlyContAuth){
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 int id = (Integer) session.save(dlyContAuth);
			 transaction.commit();
			 temp =  id;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<DlyContAuth> getDCAbyCustCode(String custCode){
		 List<DlyContAuth> dcaList = new ArrayList<DlyContAuth>();
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(DlyContAuth.class);
			   cr.add(Restrictions.eq(DlyContAuthCNTS.DCA_IS_ACTIVE,true));
			   cr.add(Restrictions.eq(DlyContAuthCNTS.DCA_CUST_CODE,custCode));
			   dcaList =cr.list();
			   transaction.commit();
			   session.flush();
		 }catch(Exception e){
				e.printStackTrace();
		 }
		 session.clear();
         session.close();
		 return dcaList;  
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getDCACustCode(){
		 List<String> cList = new ArrayList<String>();
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(DlyContAuth.class);
			   cr.add(Restrictions.eq(DlyContAuthCNTS.DCA_IS_ACTIVE,true));
			   ProjectionList proList = Projections.projectionList();
			   proList.add(Projections.property("dcaCustCode"));
			   cr.setProjection(proList);
			   cList =cr.list();
			   transaction.commit();
			   session.flush();
		 }catch(Exception e){
				e.printStackTrace();
		 }
		 session.clear();
         session.close();
		 return cList;  
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int againAlwFrCont(String custCode){
		 System.out.println("enter into againAlwFrCont function");
		 List<DlyContAuth> dcaList = new ArrayList<>();
		 int result = 0;
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(DlyContAuth.class);
			   cr.add(Restrictions.eq(DlyContAuthCNTS.DCA_CUST_CODE,custCode));
			   cr.add(Restrictions.eq(DlyContAuthCNTS.DCA_IS_ACTIVE,true));
			   dcaList = cr.list();
			   if(!dcaList.isEmpty()){
				   result = 1;
				   for(int i=0;i<dcaList.size();i++){
					   dcaList.get(i).setDcaIsActive(false);
					   session.merge(dcaList.get(i));
				   }
			   }else{
				   result = -1;
			   }
			   
			   transaction.commit();
			   session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 result = -2;
		 }
		 session.clear();
		 session.close();
		 return result;
	 }
}
