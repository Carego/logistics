'use strict';

var app = angular.module('application');

app.controller('SessionCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	$scope.expireMsg = "Session Has Been Expired !";
	$scope.againLogin = "Please login again";
	/*$scope.operatorLogin    = false;
	$scope.adminLogin       = false;
	$scope.superAdminLogin  = false;*/
	
	$scope.disabledMenu();
}]);