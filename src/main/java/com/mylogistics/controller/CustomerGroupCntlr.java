package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CustomerGroupCntlr {
	
	private Session session;
	private Transaction transaction;
	@Autowired
	private CustomerGroupDAO customerGroupDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@RequestMapping(value="/submitCustGroupAndCust",method=RequestMethod.POST)
	public @ResponseBody Object submitCustGroupAndCust(@RequestBody Map<String,Object> map){
		System.out.println("enter submitCustGroupAndCust...");
		Map<String,Object> upDateMap=new HashMap<String,Object>();
		System.out.println(map);
	
		try{
		 	String custGroupId=String.valueOf(map.get("custGroup"));
			List<Integer> custList=(List<Integer>)map.get("custIdList");
			session=sessionFactory.openSession();
		 Transaction transaction=session.beginTransaction();
		   customerGroupDAO.updateCustomerGroupId(session,custGroupId,custList);
		  transaction.commit();
		  upDateMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			if(transaction!=null){
				transaction.rollback();
			}
			e.printStackTrace();
			upDateMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		finally{
			session.flush();
			session.close();
		}
		 
		 return upDateMap;
	}
	
	@RequestMapping(value="/getCustAndGroup",method = RequestMethod.POST)
	public @ResponseBody Object getCustAndGroup(){
		System.out.println("enter into getCustAndGroupCtrl funciton");
		Map<String,Object> map=new HashMap<String,Object>();
		   try{
		     session=sessionFactory.openSession();
		     Transaction transaction=session.beginTransaction();
		     	map= customerGroupDAO.getCustAndCustGroup(session);
		    transaction.commit();
		   }
		   catch(Exception e){
			   if(transaction!=null){
				   transaction.rollback();
			   }
			   e.printStackTrace();
			}
		   finally{
			   session.close();
		   }
		if (!map.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	

	

}
