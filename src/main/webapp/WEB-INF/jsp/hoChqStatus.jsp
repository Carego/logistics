<style type="text/css">
.printable {
	display: none;
}

.hidden {
    display: none;
}

@PAGE {
	size: A4;
	margin: 0;
}

.takemeaway{
	position: absolute;
	top: -99999px;
	left: -99999px;
}

@media print {
	.noprint,.menuwrapper,.ui-dialog,footer,#toast-container {
		display: none !important;
	}
	.printable {
		display: block;
		position: fixed;
		width: 670px;
		margin-left: -30%;
		margin-top: 100px;
		z-index: 999999;
		border: 0px solid black;
	}
}

.main {
	margin-left: 15px 15px 0px 15px;
	font-family: callibri, Geneva, sans-serif;
	padding: 10px;
}

.text1 {
	margin: 5px 5px 5px 5px;
	padding: 0px;
}

.text {
	margin: 70px 5px 5px 5px;
	padding: 0px;
}

.text3 {
	margin: 30px 5px 5px 5px;
	padding: 0px;
}
</style>


<div class="printable">

	<h5>Ho Cheque Status</h5>

</div>
