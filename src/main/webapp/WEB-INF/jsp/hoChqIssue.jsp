<style type="text/css">
.printable {
	display: none;
}

.hidden {
    display: none;
}

@PAGE {
	size: A4;
	margin: 0;
}

.takemeaway{
	position: absolute;
	top: -99999px;
	left: -99999px;
}

@media print {
	.noprint,.menuwrapper,.ui-dialog,footer,#toast-container {
		display: none !important;
	}
	.printable {
		display: block;
		position: fixed;
		width: 670px;
		margin-left: -30%;
		margin-top: -40px;
		z-index: 999999;
		border: 0px solid black;
	}
}

.main {
	margin-left: 15px 15px 0px 15px;
	font-family: arial, Geneva, sans-serif;
	padding: 10px;
	font-size:16px;
	font-weight:bold;
}

.text1 {
	margin: 5px 5px 5px 5px;
	padding: 0px;
}

.text {
	margin: 70px 5px 5px 5px;
	padding: 0px;
}

.text3 {
	margin: 30px 5px 5px 5px;
	padding: 0px;
}
</style>

<div ng-show="operatorLogin || superAdminLogin">
	
	<!--By Branch  -->
	<div class="row noprint" >
	<!-- <form name="byBranchForm" ng-submit=submitByBranch(byBranchForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"> -->
		<form name="hoChqIssueForm" ng-submit=submitChqIssue(hoChqIssueForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       	<label for="code">Branch</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="bankMstrId" name ="bankMstrName" value="bank" ng-model="bankMstr.bnkName" ng-click="openBankDB()" disabled="disabled" readonly ng-required="true">
		       	<label for="code">Bank</label>	
		       	</div>
				
			</div>
			<div class="row">
		     		<div class="col s4 input-field">
		     			<select class="validate" id="chqReqTypeId" name ="chqReqTypeName" ng-init="chqReqType = 'C'"  ng-model="chqReqType" ng-required="true" ng-change="selectChqType()" >
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
					<label for="code">Cheque Type</label>		       				
		       		</div>
		       		
		       		<div class="col s4 input-field" >
		     			<select class="validate" id="chqReqPrintTypeId" name ="chqReqPrintTypeName" ng-init="chqReqPrintType = 'L'" ng-model="chqReqPrintType" ng-change="selectChqPrintType()" >
							<option value='L'>Lazer Jet</option>
							<option value='D'>Dot Matrix</option>
						</select>
					<label for="code">Print Type</label>
					</div>
					
					<div class="col s4 input-field" >
		     			<select class="validate" id="payTypeId" name ="payTypeName" ng-init="payType = 'A'" ng-model="payType" >
							<option value='A'>a/c payee</option>
							<option value='B'>Bearer</option>
						</select>
					<label for="code">Pay Type</label>
		       		</div>
		       		
		    </div>
		    
		    <div class="row">
		     	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="maxAmtLimitId" name ="maxAmtLimitName" ng-model="maxAmtLimit" ng-required="true" >
		       	<label for="code">Max Amt Limit</label>	
		       	</div>
		     	
		     	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="authEmp1Id" name ="authEmp1Name" ng-model="authEmp1.empName" ng-click="openAuthEmp1DB()" readonly ng-required="true" disabled="disabled" >
		       	<label for="code">Auth Emp1</label>	
		       	</div>
		       		
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="authEmp2Id" name ="authEmp2Name" ng-model="authEmp2.empName" ng-click="openAuthEmp2DB()" readonly ng-required="true" disabled="disabled">
		       	<label for="code">Auth Emp2</label>	
		       	</div>
		       		
		    </div>
		    
		    <div class="row" >
				
				<div class="input-field col s12 center">
					<input class="validate" type="submit" value="Submit">
				</div>
			</div>
		</form>
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by branch Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="bankMstrDB" ng-hide="bankMstrDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> A/c No </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
          </tr>
      </table> 
	</div>
	
	<!-- <div id ="chqBookDB" ng-hide="chqBookDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> From Chq No </th>
 	  	  	  <th> To Chq No </th>
 	  	  	  <th> Chq Type </th>
 	  	  	  <th> Print Type </th>
 	  	  	  <th> Total Chq </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chqBook in chqBookList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="chqBookName" value="{{ chqBook }}" ng-model="chqBook" ng-click="saveChqBook(chqBook)"></td>
              <td>{{chqBook.chqBkFromChqNo}}</td>
              <td>{{chqBook.chqBkToChqNo}}</td>
              <td>{{chqBook.chequeRequest.cReqType}}</td>
              <td>{{chqBook.chequeRequest.cReqPrintType}}</td>
              <td>{{chqBook.chqBkNOChq}}</td>
              
              
          </tr>
      </table> 
	</div> -->
	
	<div id ="issueChqDB" ng-hide="issueChqDBFlag" class="noprint">
		<form class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="fromChqNoId" name ="fromChqNoName" ng-model="chqBook.chqBkFromChqNo" readonly >
		       	<label for="code">From Chq No</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="toChqNoId" name ="toChqNoName" value="bank" ng-model="toChqNo" ng-required="true">
		       	<label for="code">To Chq No</label>	
		       	</div>
		       	
			</div>
				
			<div class="row">
					
				<div class="input-field col s12 center" >
		       		<input type="button" value="Submit" ng-click="submitChequeBTN()"/>
		       	</div> 
				
			</div>
		</form>
	</div>
	
	<div class="row" ng-if="filterChqBookList.length>0" class="noprint">
		<form class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">	
			
			<div class="row">
				<table class="tblrow">
		       		
		       		<caption class="coltag tblrow">Cheque book</caption> 
		       		
		            <tr class="rowclr">
			            <th class="colclr">SNo</th>
			            <th class="colclr">From Chq No</th>
			            <th class="colclr">To Chq No</th>
			            <th class="colclr">Chq Type</th>
 	  	  	  			<th class="colclr">Print Type</th>
 						<th class="colclr">Total Chq</th>
			            <th class="colclr">Issuing Chq</th>
			        </tr>	
			         
			        <!-- <tr ng-repeat="chqBook in chqBookList"> -->
			        <tr ng-repeat="chqBook in filterChqBookList">
			            <td class="rowcel">{{$index+1}}</td>
			            <td class="rowcel">{{chqBook.chqBkFromChqNo}}</td>
		                <td class="rowcel">{{chqBook.chqBkToChqNo}}</td>
		                <td class="rowcel">{{chqBook.chequeRequest.cReqType}}</td>
		                <td class="rowcel">{{chqBook.chequeRequest.cReqPrintType}}</td>
		                <td class="rowcel">{{chqBook.chqBkNOChq}}</td>
			            <td class="rowcel"><input type="text" id="totalIssueChqId{{$index}}" ng-model="totalIssueChq[$index]" ng-click="issueChqBookTB(chqBook, $index)" readonly/></td>
			        </tr>
		         
		        </table>
	    	</div>
	    	
	    	<div class="row">
					
				<div class="input-field col s12 center" >
		       		<input type="button" value="Submit" ng-click="submitChequeBook()"/>
		       	</div> 
				
			</div>
	    	
	    </form>
	          
	</div>
	
	<div id="finalSubmitChqBookDB" ng-hide="finalSubmitChqBookFlag" class="noprint"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch</td>
					<td>{{branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Bank</td>
					<td>{{bankMstr.bnkName}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{chqReqType}}</td>
				</tr>
				
				<tr>
					<td>Print Type</td>
					<td>{{chqReqPrintType}}</td>
				</tr>
				
			</table>
			
			<div class="row" ng-if="issueChqBookList.length>0">
				<table class="tblrow">
		       		
		       		<caption class="coltag tblrow">Cheque detail</caption> 
		       		
		            <tr class="rowclr">
			            <th class="colclr">SNo</th>
			            <th class="colclr">From Chq No</th>
			            <th class="colclr">To Chq No</th>
			            <th class="colclr">Total Chq</th>
			        </tr>	
			         
			        <!-- <tr ng-repeat="chqBook in chqBookList"> -->
			        <tr ng-repeat="chqBook in issueChqBookList">
			            <td class="rowcel">{{$index+1}}</td>
			            <td class="rowcel">{{chqBook.chqBkFromChqNo}}</td>
		                <td class="rowcel">{{chqBook.chqBkToChqNo}}</td>
		                <td class="rowcel">{{chqBook.chqBkNOChq}}</td>
			        </tr>
		        </table>
	    	</div>
			<div class="row" >
				<div class="input-field col s12 center">
					<input type="button" value="Cancel" ng-click="cancel()"/>
					<input type="button" value="Save" ng-click="finalSubmitChqBook()"/>
				</div>
			</div>
		</div>
	</div>
	
	<div id="authEmp1DB" ng-hide="authEmp1DBFlag">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
			<tr>
				<th></th>
				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

				<th>Employee FaCode</th>
			</tr>

			<tr ng-repeat="employee in empList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="authEmp1Name"  class="authEmp1Class"  value="{{ employee }}"  ng-model="empLt" ng-click="saveAuthEmp1(employee)"></td>
				<td>{{ employee.empCode }}</td>
				<td>{{ employee.empName }}</td>
				<td>{{ employee.branchCode }}</td>
				<td>{{ employee.empFaCode }}</td>
			</tr>
      </table> 
	</div>
	
	<div id="authEmp2DB" ng-hide="authEmp2DBFlag">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
			<tr>
				<th></th>
				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

				<th>Employee FaCode</th>
			</tr>

			<tr ng-repeat="employee in empList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="authEmp2Name"  class="authEmp2Class"  value="{{ employee }}"  ng-model="emp2Lt" ng-click="saveAuthEmp2(employee)"></td>
				<td>{{ employee.empCode }}</td>
				<td>{{ employee.empName }}</td>
				<td>{{ employee.branchCode }}</td>
				<td>{{ employee.empFaCode }}</td>
			</tr>
      </table> 
	</div>
	
	<!--print  -->
    
    <div id="printChqBookDB" ng-hide="printChqBookDBFlag" class="noprint">
		<form class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
			</div>
			<div class="row">
				<div class="input-field col s12 center">
					<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
					<a class="btn white-text" ng-click="yesPrint()">Yes</a>
				</div>
			</div>
		</form>
	</div>
	
	<!-- dialog for manual back print -->
	
	<div id="manualChqBrBackDB" ng-hide="manualChqBrBackDBFlag" class="noprint">
		<form class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s12 center"><h5 class="white-text">Would you like to print Back Side</h5></div>
			</div>
			<div class="row">
				<div class="input-field col s12 center">
					<a class="btn white-text" ng-click="cancelBackPrint()">Cancel</a>
					<a class="btn white-text" ng-click="yesBackPrint()">Yes</a>
				</div>
			</div>
		</form>
	</div>
    
    <!-- ng-class="{'md-primary': progressClassPrimary, 'md-accent': !progressClassPrimary}" -->
    
	<div id="manualPrintAC" class="takemeaway">

		  <div class="main">
			
			<div class="text3" style="font-weight:bold;">
				
				<p style="margin-left:120px; margin-top:160px; font-size:10px; line-height: 0.5em;">NOT EXCEEDING RS.200,000.00 TWO LAKH ONLY</p>
				<p style="margin-left:230px; line-height: 0.5em; font-size:14px;">A/C Payee</p>
			
			</div>
	     </div>
	
		
	</div>
	
	<div id="lazerJetPrintAC" class="takemeaway">

		 <div class="main">
			
			<div style="margin-left:50%; margin-top:-6%; font-size:11px; font-weight:bold;"> Cheque Issue on 12/08/2015 
			           <p style="margin-left: 50px;">A/C Payee</p>
			</div>
			
			<div class="text3" style="font-weight:bold; margin-top:130%;">
				
				<p style="margin-left:22%; line-height: 0.5em; font-size:10px;">NOT EXCEEDING RS.100,000.00 TWO LAKH ONLY</p>
				<p style="margin-left:225px;line-height: 0.5em; font-size:14px;">A/C Payee</p>
			
			</div>
								
	     </div>
		
	</div>
	
	<div id="dotMatrixPrintAC" class="takemeaway">

		<div class="main">
			
			<div class="text3" style="font-weight:bold;">
				
				<p style="margin-left:22%; margin-top:18%; line-height:0.5em; font-size:10px;">NOT EXCEEDING RS.100,000.00 TWO LAKH ONLY</p>
				<p style="margin-left:30%; line-height: 0.5em; font-size:14px;">A/C Payee</p>
			
			</div>
			
			<div style="margin-left:52%; margin-top:24%; font-size:10px; font-weight:bold;"> Cheque Issue on 12/08/2015 
			           <p style="margin-left:50px;">A/C Payee</p>
			</div>
			 
	     </div>
		
	</div>
	
	<div id="manualPrintBR" class="takemeaway">
                 
		   <div class="main"> 
		   
		        <div style="margin-left:78%; font-size:14px; margin-top:-5%; font-weight:bold;">         
		           <span>08/09/2015</span>
		        </div>   
		         <div class="text1" style="margin-top:5%;">
				     <span style=" margin-left:80px; font-size:14px; font-weight:bold;">SELF </span>
				     
			     </div>
		   
			  <div class="text3" style="font-size:10px; font-weight:bold;">
			     <p style="margin-left:115px; margin-top:130px;">NOT EXCEEDING RS.200,000.00 TWO LAKH ONLY</p>
		      </div>
	      </div>
	      	   	  
     </div>
     
     <div id="manualPrintBackBR" class="takemeaway">
                 
		   <div class="main" style="font-family:Arial, Helvetica, sans-serif;"> 
		   
		     <div style=" margin-top:2%; font-size:11px; font-weight:normal; text-align:center;"> Please Make Payment only to  
			           <p style="font-size:13px;"><span  style="font-weight:bold; font-weight:bold; text-transform:uppercase;">Avaninder</span> <br/>against his Photo ID. only.</p>
			           
			           <p style="font-size:13px; font-weight:bold;">For South Eastern Carriers Pvt Ltd.</p>
			           <p style="margin-left:50px; margin-top:55px;">(Authorised Signatory)</p>
			 </div>
		        
	      </div>
	      	   	  
     </div>
	     
	<div id="lazerJetPrintBR" class="takemeaway">

		 
		   <div class="main">
		         
		         <div style="margin-left:50%; font-size:10px; margin-top:-6%; font-weight:bold;"> Cheque Issue on 12/08/2015 
			           <p style="margin-left:60px;">Bearer</p>
			  </div>
		         
		         <div style="margin-top:76%; margin-bottom:13%; font-size:11px; font-weight:normal; text-align:center;"> please Make Payment only to  
			           <p><span  style="font-weight:bold; font-weight:bold;">Avaninder</span> <br/>against his Photo ID. only.</p>
			           
			           <p>For South Eastern Carriers Pvt Ltd.</p>
			           <p style="margin-left:50px; margin-top:50px;">(Authorised Signatory)</p>
			 </div>
			 
			         <div style="margin-left:85%; font-size:14px; margin-top:-3%; font-weight:bold;"> 08/09/2015</div>
		         <div class="text1" style="margin-top:5%;"> 
	                 <span style="font-size:14px; font-weight:bold; margin-left:50px;">SELF </span>
				     
			     </div>
		   
		   
			  <div class="text3" style="font-size:10px; font-weight:bold; margin-left:22%; margin-top:13%;">
			     <p>NOT EXCEEDING RS.200,000.00 TWO LAKH ONLY</p>
		      </div>
		     
	      </div>
		
	</div>
	
	<div id="dotMatrixPrintBR" class="takemeaway">

		   <div class="main">
		         <div style="margin-left: 85%; font-size:14px; font-weight:bold; margin-top:-6%;">
		         <span > 08/09/2015</span>
		         </div>
		         <div class="text1" style="margin-top:6%;">
				     <span style=" font-size:14px; font-weight:bold; margin-left:50px;">SELF </span>
			     </div>
		   
			  <div class="text3" style="font-size:10px; font-weight:bold;">
			     <p style="margin-left:21%; margin-top:85px;">NOT EXCEEDING RS.200,000.00 TWO LAKH ONLY</p>
		      </div>
		      
		       <div style="margin-left:51%; margin-top:24%; font-size:11px; font-weight:bold;"> Cheque Issue on 12/08/2015 
			           <p style="margin-left:50px;">Bearer</p>
			  </div>
			  
			  <div style=" margin-top:75%; font-size:11px; font-weight:normal; text-align:center;"> please Make Payment only to  
			           <p><span  style="font-weight:bold; font-weight:bold;">Avaninder</span> <br/>against his Photo ID. only.</p>
			           
			           <p>For South Eastern Carriers Pvt Ltd.</p>
			           <p style="margin-left:50px; margin-top:50px;">(Authorised Signatory)</p>
			 </div>
	      
	      </div>
		
	</div>
	
</div>
