package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "chcnsedetail")
public class ChCnSeDetail {
	
	@Id
	@Column(name="chCnSeId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int chCnSeId;
	
	@Column(name="chCnSeCode")
	private String chCnSeCode;
	
	@Column(name="chCnSeStOdrCode")
	private String chCnSeStOdrCode;
	
	@Column(name="chCnSeStartNo")
	private String chCnSeStartNo;
	
	@Column(name="chCnSeNob")
	private int chCnSeNob;
	
	@Column(name="chCnSeEndNo")
	private String chCnSeEndNo;
	
	@Column(name="chCnSeStatus")
	private String chCnSeStatus;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getChCnSeId() {
		return chCnSeId;
	}

	public void setChCnSeId(int chCnSeId) {
		this.chCnSeId = chCnSeId;
	}

	public String getChCnSeCode() {
		return chCnSeCode;
	}

	public void setChCnSeCode(String chCnSeCode) {
		this.chCnSeCode = chCnSeCode;
	}

	public String getChCnSeStOdrCode() {
		return chCnSeStOdrCode;
	}

	public void setChCnSeStOdrCode(String chCnSeStOdrCode) {
		this.chCnSeStOdrCode = chCnSeStOdrCode;
	}

	public int getChCnSeNob() {
		return chCnSeNob;
	}

	public void setChCnSeNob(int chCnSeNob) {
		this.chCnSeNob = chCnSeNob;
	}

	public String getChCnSeStatus() {
		return chCnSeStatus;
	}

	public void setChCnSeStatus(String chCnSeStatus) {
		this.chCnSeStatus = chCnSeStatus;
	}

	public String getChCnSeStartNo() {
		return chCnSeStartNo;
	}

	public void setChCnSeStartNo(String chCnSeStartNo) {
		this.chCnSeStartNo = chCnSeStartNo;
	}

	public String getChCnSeEndNo() {
		return chCnSeEndNo;
	}

	public void setChCnSeEndNo(String chCnSeEndNo) {
		this.chCnSeEndNo = chCnSeEndNo;
	}
	
}
