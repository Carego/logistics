'use strict';

var app = angular.module('application');

app.controller('ViewContractCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.CodesDBFlag = true;
	$scope.dlyCont=true;
	$scope.regCont=true;
	
	$scope.openCodesDB = function(){
		$scope.CodesDBFlag = false;
		$('div#CodesDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Contract Codes",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
				
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			
			$('div#CodesDB').dialog('open');
		}
	
	$scope.saveCode = function(codes){
		$scope.ContCode = codes.contCode;
		$scope.ContFaCode = codes.contFaCode;
		$scope.CodesDBFlag = true;
		$('div#CodesDB').dialog("destroy");
	}
	
	
	 $scope.getDlyRegContractCodes = function(){
		 console.log("I have got the lists of codes");
		   var response = $http.post($scope.projectName+'/getDlyRegContractCodes');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.codeList = data.list;
			   }else{
				   console.log(data);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	 $scope.viewContract = function(ContCode){
			
		   $scope.enteredCode = $( "#displayDetails" ).val();
		   if($scope.enteredCode===""){
			   $scope.alertToast("You have not entered anything");
		   }
		   else{
			   console.log("The entered contcode is "+ContCode);
				$scope.code = ContCode.substring(0,3);
				console.log("The substring is "+$scope.code);
				if($scope.code==="dly"){
				$scope.dlyCont = false;
				$scope.regCont = true;
				var response = $http.post($scope.projectName+'/dailycontractdetails', ContCode);
				response.success(function(data, status, headers, config) {
					$scope.dailyContract = data.dailyContract;
					$scope.fromStation=$scope.dailyContract.dlyContFromStation;
					$scope.contractType=$scope.dailyContract.dlyContType;
					console.log($scope.contractType);
					$scope.dailyContract.creationTS = $filter('date')($scope.dailyContract.creationTS, 'MM/dd/yyyy hh:mm:ss');
					$scope.getRBKMDataViewCont();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
				else if($scope.code==="reg"){
					$scope.regCont = false;
					$scope.dlyCont = true;
				var response = $http.post($scope.projectName+'/regularcontractdetails',ContCode);
				response.success(function(data, status, headers, config) {
					$scope.regularContract = data.regularContract;	
					$scope.regularContract.creationTS = $filter('date')($scope.regularContract.creationTS, 'MM/dd/yyyy hh:mm:ss');
					$scope.fromStation=$scope.regularContract.regContFromStation;
					$scope.contractType=$scope.regularContract.regContType;
					console.log($scope.contractType)
					$scope.getRBKMDataViewCont();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		   } 
		   }
	   }
	 
	 $scope.getRBKMDataViewCont = function(){
		 console.log("Enter into getRBKMDataViewDC function"+ $scope.enteredCode);
		   var response = $http.post($scope.projectName+'/getRBKMDataViewCont', $scope.enteredCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.rbkmList = data.list;
			 	$scope.getPBDDataViewCont();
			 	$scope.rbkmFlag=true;
			   }else{
				console.log(data);  
				$scope.getPBDDataViewCont();
				$scope.rbkmFlag=false;
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getPBDDataViewCont= function(){
		 console.log("Enter into getPBDDataViewCont function"+ $scope.enteredCode);
		   var response = $http.post($scope.projectName+'/getPBDDataViewCont', $scope.enteredCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.pbdList = data.list;
			   $scope.pbdFlag=true; 
			   $scope.getCTSDataViewCont();
			   }else{
				console.log(data); 
				$scope.pbdFlag=false; 
				$scope.getCTSDataViewCont();
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 $scope.getCTSDataViewCont = function(){
		 console.log("Enter into getCTSDataViewCont function"+ $scope.enteredCode);
		   var response = $http.post($scope.projectName+'/getCTSDataViewCont', $scope.enteredCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.oldCtsList = data.list;
				
			  console.log($scope.oldCtsList.length);
				   if($scope.contractType==="Q"){
						$scope.ctsFlagQ=true;
						$scope.ctsFlagW=false;
					}else if($scope.contractType==="W") {
						$scope.ctsFlagW=true;
						$scope.ctsFlagQ=false;
					}
			   }else{
				console.log(data);  
				$scope.ctsFlagQ=false;
				$scope.ctsFlagW=false;
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getDlyRegContractCodes();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 

}]);