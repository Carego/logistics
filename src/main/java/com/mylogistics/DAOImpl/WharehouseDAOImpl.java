package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.WharehouseDAO;
import com.mylogistics.model.Wharehouse;

public class WharehouseDAOImpl implements WharehouseDAO{

	
	private SessionFactory sessionFactory;
	
	public static Logger logger=Logger.getLogger(WharehouseDAOImpl.class);
	
	@Autowired
	public WharehouseDAOImpl(SessionFactory sessionFactory){
		this.sessionFactory=sessionFactory;
	}
	
	@Override
	public List<Wharehouse> getWharehouseList() {
		// TODO Auto-generated method stub
		List<Wharehouse> list=new ArrayList<>();
		Session session=sessionFactory.openSession();
		try{
			
			Criteria criteria=session.createCriteria(Wharehouse.class);
			ProjectionList projectionList=Projections.projectionList();
			projectionList.add(Projections.property("id"),"id");
			projectionList.add(Projections.property("whSrNo"),"whSrNo");
			projectionList.add(Projections.property("whCount"),"whCount");
			projectionList.add(Projections.property("state"),"state");
			projectionList.add(Projections.property("location"),"location");
			projectionList.add(Projections.property("address1"),"address1");
			projectionList.add(Projections.property("address2"),"address2");
			
			criteria.setProjection(projectionList);
			
			list=criteria.list();
		
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in fetching warehouse list"+e);
		}finally{
			session.close();
		}
			
		
		return list;
	}

}
