<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<!-- <option value='O'>By Online</option> -->
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		       	
		    <div class="row">
	     		<div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       			<label for="code">Pay To</label>	
	       		</div>
	       		
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="tdsCodeId" name ="tdsCode" ng-model="vs.tdsCode" ng-click="selectTds()">
		       			<label for="code">TDS Code</label>	
		       	</div>
		      
		        <div class="col s4 input-field">
		       				<input class="validate" type ="number" id="tdsAmtId" name ="tdsAmt" ng-model="vs.tdsAmt"  STEP = "0.01" >
		       			<label for="code">TDS Amount</label>	
		       	</div>
	   		</div>
	   		
	   		
	   		 <div class="row">
		   		 <div class="col s4 input-field">
							<input type ="text" name="custCode" id="custCodeId" ng-model="custCode" ng-click="openCustDB()" disabled="disabled" readonly ng-required="true">
						<label>Select Customer Code</label>
				  </div>
	   		 </div>
	   			   		
	   		<div class="row">	   
		   		<div ng-show="bpVList.length > 0">
		   			<table class="tblrow">
		   				   <caption class="coltag tblrow">Business Promotion Details</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">CUST CODE</th>
	                        <th class="colclr">CUST NAME</th>
	                        <th class="colclr">AMOUNT</th>
	                        <th class="colclr">ARTICLE</th>
	                        <th class="colclr">PERSON NAME</th>
	                        <th class="colclr">DESIGNATION</th>    
	                        <th class="colclr">PURPOSE</th>    
	                        <th class="colclr">TURNOVER</th>  
	                        <th class="colclr">EXP_TURN</th>   
	                        <th class="colclr">DESCRIPTION</th>    
	                        <th class="colclr">ACTION</th>                      
	                    </tr>
	                     <tr class="tbl" ng-repeat="bpVouch in bpVList">
	                        <td class="rowcel">{{bpVouch.customer.custFaCode}}</td>
	                        <td class="rowcel">{{bpVouch.customer.custName}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpAmt}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpArticle}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpPaidName}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpPaidDesig}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpPurpose}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpTurnOver}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpExpTurn}}</td>
	                        <td class="rowcel">{{bpVouch.faBProm.fbpDesc}}</td>
	                        <td class="rowcel">
	                        	<input type="button" value="REMOVE" ng-click="removeBPV($index,bpVouch)"/>
	                        </td>
	                 	</tr>
	                 	<tr class="tbl" >							
							<td class="rowcel" colspan="2">Total Amount</td>
							<td class="rowcel">{{fbpAmtSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>
							<td class="rowcel">{{fbpTurnOverSum}}</td>
							<td class="rowcel">{{fbpExpTurnSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>							
						</tr>

	                 </table>
	             </div>    	       	   
	   		</div>	   
	   			   		
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<!-- <div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div> -->
	   		 </div>
		  </form>
    </div>
  
  
   <div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="tdsCodeDB" ng-hide=tdsCodeFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Tds Code ">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> TDS Code </th>
	 	  	  	  <th> Name </th>
	 	  	  	  <th> Type </th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="fsMTds in fsMTdsList | filter:filterTextbox">
			 	  <td><input type="radio"  name="fsMTds"   value="{{ fsMTds }}" ng-model="fsMT" ng-click="saveTdsCode(fsMTds)"></td>
	              <td>{{fsMTds.faMfaCode}}</td>
	              <td>{{fsMTds.faMfaName}}</td>
	              <td>{{fsMTds.faMfaType}}</td>
	          </tr>
	      </table> 
    </div>
    
    
     <div id="custCodeDB" ng-hide=custCodeDBFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by customer Code ">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th>Name</th>
	 	  	  	  <th>Code</th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="cust in custList | filter:filterTextbox">
			 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custId" ng-click="saveCustCode(cust)"></td>
	              <td>{{cust.custName}}</td>
	              <td>{{cust.custFaCode}}</td>
	          </tr>
	      </table> 
    </div>



	 <div id="custBranchDB" ng-hide="custBranchDBFlag"> 
	  	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Branch Code ">
	  	<table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th>Name</th>
	 	  	  	  <th>Code</th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="branch in branchList | filter:filterTextbox">
			 	  <td><input type="radio"  name="branch"   value="{{ branch }}" ng-model="barcnhId" ng-click="saveCustBrCode(branch)"></td>
	              <td>{{branch.branchName}}</td>
	              <td>{{branch.branchFaCode}}</td>
	          </tr>
	      </table> 
	  
	  
	  
	  
	  </div> 
  
   <div id="bpVoucherDB" ng-hide="bpVoucherDBFlag">
   <form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="bpVoucherForm" ng-submit="submitBPVoucher(bpVoucherForm)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="fbpAmt" id="fbpAmtId" ng-model="bpServ.faBProm.fbpAmt" ng-required="true">
					<label>Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="text" name="fbpArticle" id="fbpArticleId" ng-model="bpServ.faBProm.fbpArticle">
					<label>Article</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="text" name="fbpPaidName" id="fbpPaidNameId" ng-model="bpServ.faBProm.fbpPaidName">
					<label>Person Name</label>
				</div>
	     </div>
	     
	     <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="text" name="fbpPaidDesig" id="fbpPaidDesigId" ng-model="bpServ.faBProm.fbpPaidDesig">
					<label>Designation</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="text" name="fbpPurpose" id="fbpPurposeId" ng-model="bpServ.faBProm.fbpPurpose">
					<label>Purpose</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="fbpTurnOver" id="fbpTurnOverId" ng-model="bpServ.faBProm.fbpTurnOver">
					<label>Turnover</label>
				</div>
	     </div>
	     
	      <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="fbpExpTurn" id="fbpExpTurnId" ng-model="bpServ.faBProm.fbpExpTurn">
					<label>Exp_Turn</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type ="text" name="fbpBrfaCode" id="fbpBrfaCodeId" ng-model="bpServ.faBProm.fbpBrfaCode" ng-click="selectCustBranch()" readonly>
					<label>Branch Code</label>
				</div>
		   </div>	
		   	
	     
	     <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="bpServ.faBProm.fbpDesc"></textarea>
 					<label>Description</label>
				</div>
    	  </div>	
    		 
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
   
   </div>
  
  

  	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>

 </div>  