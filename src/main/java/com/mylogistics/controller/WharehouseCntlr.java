package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.WharehouseDAO;
import com.mylogistics.model.Wharehouse;

@Controller
public class WharehouseCntlr {

	@Autowired
	private WharehouseDAO wharehouseDAO;
	
	@RequestMapping(value="/getRelWharehouse",method = RequestMethod.POST)
	public @ResponseBody Object getRelWharehouse(){
		
		Map<String, Object> map=new HashMap<>();
		List<Wharehouse> list=wharehouseDAO.getWharehouseList();
		if(!list.isEmpty()){
			map.put("whList", list);
			map.put("result", "success");
		}else{
			map.put("result", "error");
		}
		return map;
	}
	
	
	
}
