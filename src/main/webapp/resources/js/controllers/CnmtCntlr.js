'use strict';

var app = angular.module('application');

app.controller('CnmtCntlr',['$scope','$location','$http','$window','FileUploadService','$filter',
                            function($scope,$location,$http,$window,FileUploadService,$filter){
	$scope.cnmt = {};
	$scope.cnmtPending = {};
	$scope.customerName="";
	$scope.cnmt.relType=null;
	$scope.cnmt.relChln=null;
	$scope.cnmt.prBlCode=null;
	$scope.cnmt.whCode="";
	
	$scope.show = true;
	$scope.whShowFlag=false;
	$scope.relChlnShowFlag=false;
	$scope.CustomerCodeDBFlag=true;
	$scope.CNMTConsignorDBFlag=true;
	$scope.BranchCodeDBFlag=true;
	$scope.CNMTConsigneeDBFlag=true;
	$scope.ContractCodeDBFlag=true;
	$scope.CNMTPayAtDBFlag=true;
	$scope.CNMTBillAtDBFlag=true;
	$scope.EmployeeCodeDBFlag=true;
	$scope.StateCodeDBFlag=true;
	$scope.Cflag=true;
	$scope.Fflag=true;
	$scope.K_Type = false;
	$scope.CnmtCodeDBFlag=true;
	$scope.viewCnmtDetailsFlag=true;
	$scope.saveInvoiceNoFlag=true;
	$scope.CnmtFromStationDBFlag=true;
	$scope.CnmtToStationDBFlag=true;
	$scope.ProductTypeFlag = true;
	$scope.VehicleTypeDBFlag=true;
	$scope.warehouseList=[];
	

	$scope.autoToStnFlag = false;

	$scope.cnmtRate = 0;
	$scope.actualCnmtRate = 0;
	$scope.garWt=0;
	$scope.garunteeWt = 0; 

	$scope.cnmtInvList = [];
	$scope.contractList = [];

	$scope.contract = {};
	$scope.vtList = {};
	$scope.stationList = [];
	
	var dailyRate = false;

	$('#cnmtRate').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#cnmtRate').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length === 7)){
			e.preventDefault();
		}
	});

	$('#cnmtFreight').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#cnmtFreight').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length === 7)){
			e.preventDefault();
		}
	});

	$('#cnmtActualWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#cnmtGuaranteeWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#cnmtNoOfPkg').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#cnmtNoOfPkg').keypress(function(e) {
		if (this.value.length === 9) {
			e.preventDefault();
		}
	});

	$('#cnmtVOG').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#cnmtVOG').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length === 9)){
			e.preventDefault();
		}
	});

	$('#cnmtExtraExp').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#cnmtKmId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#cnmtKmId').keypress(function(e) {
		if (this.value.length === 15) {
			e.preventDefault();
		}
	});

	$('#invoiceNo').keypress(function(e) {
		if (this.value.length === 25) {
			e.preventDefault();
		}
	});


	/*$scope.custCodeRB = function(){

		$scope.Cflag=false;
		$scope.Fflag=true;

		$scope.cnmt.cnmtPayAt="";
		$('#cnmtPayAt').attr("disabled","disabled");
		$('#cnmtBillAt').removeAttr("disabled");
		$('#custCode').removeAttr("disabled");
	}

	$scope.frieghtOnRB = function(){

		$scope.Cflag=true;
		$scope.Fflag=false;

		$scope.cnmt.cnmtBillAt="";
		$('#cnmtPayAt').removeAttr("disabled");
		$('#cnmtBillAt').attr("disabled","disabled");
		$('#custCode').removeAttr("disabled");
	}*/



	$scope.OpenCustomerCodeDB = function(keyCode){
		console.log("Enter into OpenCustomerCodeDB()");
		if(keyCode === 8 || parseInt($scope.customerCode.length) < 2)
			return;
		
			var response = $http.post($scope.projectName + '/getCustomerListByNameFa', $scope.customerCode);
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){
					
					$scope.customerList = data.customerList;
					
					$scope.customerCode = "";
					$scope.CustomerCodeDBFlag=false;
					$('div#customerCodeDB').dialog({
						autoOpen: false,
						modal:true,
						title: "Customer Code",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});
					$('div#customerCodeDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
					$scope.customerCode = "";
				}
				
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCustomerListByNaFa");
			});
		console.log("Exit from OpenCustomerCodeDB()");
	}
	
	$scope.getConsignorList = function(keyCode){
		console.log("Enter into getConsignorList()");
		if(keyCode === 8 || parseInt($scope.consignorTmpCode.length) < 2)
			return;
		var response = $http.post($scope.projectName + '/getCustomerListByNameFa', $scope.consignorTmpCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				$scope.customerList = data.customerList;
				$scope.OpenCnmtConsignorDB();				
			}else{
				$scope.alertToast(data.msg);
				$scope.customerCode = "";
			}			
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCustomerListByNaFa");
		});
		console.log("Exit from getConsignorList()");
	}
	
	$scope.OpenCnmtConsignorDB = function(){
		$scope.CNMTConsignorDBFlag=false;
		$('div#CNMTConsignorDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Consignor Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#CNMTConsignorDB').dialog('open');
	}

	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}

	$scope.OpenCnmtCodeDB = function(keyCode){
		console.log("Enter into OpenCnmtCodeDB()");
		
		if(keyCode === 8 || parseInt($scope.cnmt.cnmtCode.length) < 2)
			return;
			
			var response = $http.post($scope.projectName + '/getCnmtCodeByCode', $scope.cnmt.cnmtCode);
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){
					
					$scope.cnmt.cnmtCode = ""; 
					$scope.cnmtCodeList = data.cnmtCodeList;
					
					$scope.CnmtCodeDBFlag=false;
					$('div#cnmtCodeDB').dialog({
						autoOpen: false,
						modal:true,
						title: "CNMT Code",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#cnmtCodeDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
					$scope.cnmt.cnmtCode = ""; 
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCnmtCodeByCode");
			});
		console.log("Exit from OpenCnmtCodeDB()");
	}

	$scope.OpenCnmtConsigneeDB = function(){
		$scope.CNMTConsigneeDBFlag=false;
		$('div#CNMTConsigneeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Consignee Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#CNMTConsigneeDB').dialog('open');
	}

	$scope.OpenContractCodeDB = function(){
		$scope.ContractCodeDBFlag=false;
		$('div#contractCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Contract Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#contractCodeDB').dialog('open');
	}

	$scope.openPayAtDB = function(){
		$scope.CNMTPayAtDBFlag=false;

		$('div#CNMTPayAtDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Pay At Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#CNMTPayAtDB').dialog('open');
	}

	$scope.OpenCnmtBillAtDB = function(){
		if(angular.isUndefined($scope.branchList)){
			var response = $http.post($scope.projectName+'/getBranchDataForCnmt');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.branchList = data.list;				
				}else{
					$scope.alertToast("you don't have any active branch");
					console.log(data);
				}
			});
			response.error(function(data, status, headers,config) {
				$scope.errorToast(data);
			});
		}

		$scope.CNMTBillAtDBFlag=false;
		$('div#CNMTBillAtDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Bill At Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#CNMTBillAtDB').dialog('open');
	}

	$scope.OpenEmployeeCodeDB = function(){
		$scope.EmployeeCodeDBFlag=false;
		$('div#employeeCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Employee Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#employeeCodeDB').dialog('open');
	}

	$scope.OpenStateCodeDB = function(){		
		$scope.StateCodeDBFlag=false;
		$('div#stateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "State Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#stateCodeDB').dialog('open');
	}

	$scope.openInvoiceNo = function(){
		console.log("Enter into openInvoiceNo");
		$scope.saveInvoiceNoFlag=false;
		$scope.invoiceNo = "";
		$('div#saveInvoiceNo').dialog({
			autoOpen: false,
			modal:true,
			title: "Save Invoice No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		console.log("Line no 342");
		$('div#saveInvoiceNo').dialog('open');	
	}

	$scope.OpenCnmtFromStationDB = function(){
		$scope.CnmtFromStationDBFlag=false;
		$('div#cnmtFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Cnmt From Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#cnmtFromStationDB').dialog('open');
	}

	$scope.OpenCnmtToStationDB = function(){
		$scope.CnmtToStationDBFlag=false;
		$('div#cnmtToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Cnmt To Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#cnmtToStationDB').dialog('open');
	}

	$scope.OpencnmtProductTypeDB = function(){
		if(angular.isUndefined($scope.cnmt.cnmtToSt)){
			$scope.alertToast("enter To Station Code and No. OF Packages");
		}else{
			$scope.ProductTypeFlag = false;
			$('div#cnmtProductTypeDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Product Type",
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
			});
			$('div#cnmtProductTypeDB').dialog('open');
		}
	}


	$scope.OpenVehicleTypeDB = function(){
		$scope.VehicleTypeDBFlag=false;
		$('div#vehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vihicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#vehicleTypeDB').dialog('open');
	}


	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		if(angular.isUndefined($scope.branchList)){
			var response = $http.post($scope.projectName+'/getBranchDataForCnmt');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.branchList = data.list;	
				}else{
					$scope.alertToast("you don't have any active branch");
					console.log(data);
				}
			});
			response.error(function(data, status, headers,config) {
				$scope.errorToast(data);
			});
		}
		
		$scope.openPayAtDB();
		
	}

	$scope.saveBranchCode = function(branch){
		$scope.cnmt.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}

	$scope.saveCnmtCode = function(cnmt){
		$scope.cnmt.cnmtCode = cnmt.brsLeafDetSNo;
		$('div#cnmtCodeDB').dialog('close');
		$scope.CnmtCodeDBFlag=true;
		console.log("cnmt.brsLeafDetDlyRt = "+cnmt.brsLeafDetDlyRt);
		if(cnmt.brsLeafDetDlyRt === true){
			dailyRate = true;
			$('#cnmtRateId').attr("readonly",false);
		}else{
			dailyRate = false;
			$('#cnmtRateId').attr("readonly",true);
		}		
	}

	$scope.savePayAtBranchCode = function(payat){
		$scope.payOnCode = payat.branchFaCode;
		$scope.cnmt.cnmtPayAt = payat.branchCode;
		$('div#CNMTPayAtDB').dialog('close');
		$scope.CNMTPayAtDBFlag=true;
	}

	$scope.saveBillAtBranchCode = function(billat){
		$scope.billOnCode = billat.branchFaCode;
		$scope.cnmt.cnmtBillAt = billat.branchCode;
		$('div#CNMTBillAtDB').dialog('close');
		$scope.CNMTBillAtDBFlag=true;
	}

	$scope.saveToStnCode = function(station){
		console.log("enter into saveToStnCode----->"+station.stnName);
		$scope.toStationCode = station.stnName;
		/*var group = {
				"custCode" :  $scope.cnmt.custCode,
				"cnmtDt"	  : $scope.cnmt.cnmtDt,
				"cnmtFromSt" : $scope.cnmt.cnmtFromSt
		}
	var response = $http.post($scope.projectName+'/getContractData',group);
	response.success(function(data, status, headers, config) {
		if(data.result === "success"){
			$scope.contractList = data.list;
			$scope.alertToast("relevant contract exists enter contract code");
			$('#contractCode1').removeAttr("disabled");
		}else{
			$scope.alertToast("relevant contract does not exists");
		}
		 */
		$scope.cnmt.cnmtToSt = station.stnCode;
		if($scope.autoToStnFlag == true){
			console.log(" automatically to stn fill");
		}else{
			$('div#cnmtToStationDB').dialog('close');
			$scope.CnmtToStationDBFlag=true;
		}
		$scope.autoToStnFlag == false;
		$scope.CnmtToStationDBFlag=true;
		console.log("inside tostn---------->????"+$scope.contract.contType);
		if($scope.contract.contType === "w" || $scope.contract.contType === "W"){
			$scope.autoToStnFlag == true;
			if(angular.isUndefined($scope.cnmt.cnmtToSt)|| $scope.cnmt.cnmtToSt === null || $scope.cnmt.cnmtToSt === ""||
				angular.isUndefined($scope.cnmt.cnmtVehicleType) || $scope.cnmt.cnmtVehicleType === null || $scope.cnmt.cnmtVehicleType === "" ){
				$scope.alertToast("please vehicle type");
			}else{
				$scope.addRateForCnmtByW($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtVehicleType,$scope.cnmt.contractCode);
			}
		}else if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			$scope.autoToStnFlag == true;
			if(angular.isUndefined($scope.cnmt.cnmtToSt)|| $scope.cnmt.cnmtToSt === null || $scope.cnmt.cnmtToSt === ""||
				angular.isUndefined($scope.cnmt.cnmtProductType) || $scope.cnmt.cnmtProductType === null || $scope.cnmt.cnmtProductType === ""){
				$scope.alertToast("please fill product type");
			}else{
				$scope.addRateForCnmtByQ($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtProductType,$scope.cnmt.contractCode);
			}
		}else if($scope.contract.contType === "k" || $scope.contract.contType === "K"){
			if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState === null || $scope.cnmt.cnmtState === "" ||
				angular.isUndefined($scope.cnmt.cnmtKm)|| $scope.cnmt.cnmtKm === null || $scope.cnmt.cnmtKm==="" || $scope.cnmt.cnmtKm === 0 ){
				$scope.alertToast("fill state and km");
			}
		}else{
			console.log("try again");
		}
	}


	$scope.changeKm = function(){
		console.log(" enter into changeKm function");
		if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState === null || $scope.cnmt.cnmtState === "" || 
			angular.isUndefined($scope.cnmt.cnmtKm)|| $scope.cnmt.cnmtKm === null || $scope.cnmt.cnmtKm==="" ){
			$scope.alertToast("check state and km are filled");
		}else{
			$scope.addRateForCnmtByK($scope.cnmt.cnmtKm,$scope.cnmt.cnmtState,$scope.cnmt.contractCode);
		}
	}



	$scope.saveVehicleType = function(vt){
		$scope.vehicleName = vt.vtVehicleType;
		$scope.cnmt.cnmtVehicleType = vt.vtCode;
		$('div#vehicleTypeDB').dialog('close');
		$scope.VehicleTypeDBFlag=true;
		if($scope.contract.contType === "w" || $scope.contract.contType === "W"){
			if(angular.isUndefined($scope.cnmt.cnmtToSt)|| $scope.cnmt.cnmtToSt === null || $scope.cnmt.cnmtToSt === ""||
				angular.isUndefined($scope.cnmt.cnmtVehicleType) || $scope.cnmt.cnmtVehicleType === null || $scope.cnmt.cnmtVehicleType === "" ){
				console.log("######################fill station and vehicle type");
			}else{
				$scope.addRateForCnmtByW($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtVehicleType,$scope.cnmt.contractCode);
			}
		}else{
			console.log("no need");
		}
	}

	$scope.savProductName = function(pt){
		$scope.cnmt.cnmtProductType = pt;
		$('div#cnmtProductTypeDB').dialog('close');
		$scope.ProductTypeFlag=true;
		if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			if(angular.isUndefined($scope.cnmt.cnmtToSt)|| $scope.cnmt.cnmtToSt === null || $scope.cnmt.cnmtToSt === ""||
				angular.isUndefined($scope.cnmt.cnmtProductType) || $scope.cnmt.cnmtProductType === null || $scope.cnmt.cnmtProductType === ""){
				console.log("************************fill station and product type");
			}else{
				$scope.addRateForCnmtByQ($scope.cnmt.cnmtToSt,$scope.cnmt.cnmtProductType,$scope.cnmt.contractCode);
			}
		}else{
			console.log("no need again");
		}	
	}

	$scope.saveCustomerCode = function(customer){
		console.log("hiiiiiiiiiiiiiiii---->"+customer.custFaCode);
		$scope.Cflag=true;
		$scope.Fflag=true;
		$scope.customerCode = customer.custFaCode;
		$scope.cnmt.custCode = customer.custCode;
		$scope.customerName = customer.custName;
		
		$('div#CustomerCodeDB').dialog('close');
		$scope.CustomerCodeDBFlag=true;
		if(angular.isUndefined($scope.cnmt.custCode) || $scope.cnmt.custCode === null || $scope.cnmt.custCode === ""||
			angular.isUndefined($scope.cnmt.cnmtDt)  || $scope.cnmt.cnmtDt === null || $scope.cnmt.cnmtDt === ""||
			angular.isUndefined($scope.cnmt.cnmtFromSt)  || $scope.cnmt.cnmtFromSt === null || $scope.cnmt.cnmtFromSt === ""){
			$('#contractCode1').attr("disabled","disabled");
			console.log("************************not possible");
		}else{
			//fire database call here and enable contract code on success
			var group = {
					"custCode" :  $scope.cnmt.custCode,
					"cnmtDt"	  : $scope.cnmt.cnmtDt,
					"cnmtFromSt" : $scope.cnmt.cnmtFromSt
			}
			var response = $http.post($scope.projectName+'/getContractData',group);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.contractList = data.list;
					$scope.alertToast("relevant contract exists enter contract code");
					$('#contractCode1').removeAttr("disabled");
				}else{
					//$scope.alertToast("relevant contract does not exists");
					console.log("msg from server----->"+data.result);
					$scope.cnmt.contractCode="";
					$('#contractCode1').attr("disabled","disabled");
					$scope.cnmtDC = "";
					$scope.cnmtDDL = "";
					$scope.cnmtCostGrade = "";
				}

				console.log("----->from server = "+$scope.contractList);
				/*if($scope.contractList.length === 0){
				$('#contractCode1').attr("disabled","disabled");
				$scope.cnmt.contractCode = "";
				$scope.cnmtDC = "";
				$scope.cnmtDDL = "";
				$scope.cnmtRate = "";
				$scope.cnmtCostGrade = "";
				$scope.cnmtVehicleType = "";
				$scope.cnmt.cnmtGuaranteeWt = "";
			}*/

				/*	for(var i=0 ; i<$scope.contractList.length ; i++){
				console.log("from server $scope.contractList.contCode --->"+$scope.contractList[i].contCode);
				if($scope.contractList[i].custCode === customer.custCode){
					console.log("--------code match--------");
					if(angular.isUndefined($scope.cnmt.cnmtDt) && ($scope.cnmt.cnmtFromSt===null || angular.isUndefined($scope.cnmt.cnmtFromSt) || $scope.cnmt.cnmtFromSt==="")){
						$('#contractCode1').attr("disabled","disabled");
					}else{
						$('#contractCode1').removeAttr("disabled");
					}
				}else{
					$('#contractCode1').attr("disabled","disabled");
				}
			}*/

			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	}
	
	
	$scope.getFYear = function(){
		var response = $http.get($scope.projectName+'/getCurntFYear');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("Entered Date in current financial year");
				console.log(data.fYear.fyFrmDt);
				console.log(data.fYear.fyToDt);
				$scope.fyFrmDt=data.fYear.fyFrmDt;
				$scope.fyToDt=data.fYear.fyToDt;
			}else{
				$scope.ar.arIssueDt="";
				$scope.alertToast("There is no any current Financial year");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.alertToast("There is some problem in fetching date");
		});
	}();
	
	$scope.cnmtDlyDtInFY = function(){
		console.log("cnmtDlyDtInFY------>Kamal");
		if($scope.cnmt.cnmtDtOfDly<$scope.fyFrmDt || $scope.cnmt.cnmtDtOfDly>$scope.fyToDt){
			$scope.cnmt.cnmtDtOfDly="";
			$scope.alertToast("Date should be in current Financial year");
		}
	}
	
	$scope.invcDtInFy = function(){
		console.log("invcDtInFy------>Kamal");
		if($scope.invoiceDt<$scope.fyFrmDt || $scope.invoiceDt>$scope.fyToDt){
			$scope.invoiceDt="";
			$scope.alertToast("Date should be in current Financial year");
		}
	}

	$scope.dateSelect = function(cnmtDate){
		console.log("enter into dateSelect function");
		if($scope.cnmt.cnmtDt<$scope.fyFrmDt || $scope.cnmt.cnmtDt>$scope.fyToDt){
			$scope.cnmt.cnmtDt="";
			$scope.alertToast("Date should be in current Financial year");
		}else{
			if(angular.isUndefined($scope.cnmt.custCode) || $scope.cnmt.custCode === null || $scope.cnmt.custCode === ""||
					angular.isUndefined($scope.cnmt.cnmtDt)  || $scope.cnmt.cnmtDt === null || $scope.cnmt.cnmtDt === ""||
					angular.isUndefined($scope.cnmt.cnmtFromSt)  || $scope.cnmt.cnmtFromSt === null || $scope.cnmt.cnmtFromSt === ""){
					console.log("************************not possible again");
					$('#contractCode1').attr("disabled","disabled");
				}else{
					$('#contractCode1').removeAttr("disabled");
					var group = {
							"custCode" : $scope.cnmt.custCode,
							"cnmtDt"	  : $scope.cnmt.cnmtDt,
							"cnmtFromSt" : $scope.cnmt.cnmtFromSt
					}
					var response = $http.post($scope.projectName+'/getContractData',group);
					response.success(function(data, status, headers, config) {

						if(data.result === "success"){
							$scope.contractList = data.list;
							$scope.alertToast("relevant contract exists enter contract code");
							$('#contractCode1').removeAttr("disabled");
						}else{
							//$scope.alertToast("relevant contract does not exists");
							console.log("msg from server----->"+data.result);
							$scope.cnmt.contractCode="";
							$('#contractCode1').attr("disabled","disabled");
							$scope.cnmtDC = "";
							$scope.cnmtDDL = "";
							$scope.cnmtCostGrade = "";
						}
						console.log("----->from server = "+$scope.contractList);
						/*if($scope.contractList.length === 0){
							$('#contractCode1').attr("disabled","disabled");
							$scope.cnmt.contractCode = "";
							$scope.cnmtDC = "";
							$scope.cnmtDDL = "";
							$scope.cnmtRate = "";
							$scope.cnmtCostGrade = "";
							$scope.cnmtVehicleType = "";
							$scope.cnmt.cnmtGuaranteeWt = "";
						}*/

						/*for(var i=0 ; i<$scope.contractList.length ; i++){
							console.log("from server $scope.contractList.contCode --->"+$scope.contractList[i].contCode);
							if($scope.contractList[i].custCode === customer.custCode){
								console.log("--------code match--------");
								if(angular.isUndefined($scope.cnmt.cnmtDt) && ($scope.cnmt.cnmtFromSt===null || angular.isUndefined($scope.cnmt.cnmtFromSt) || $scope.cnmt.cnmtFromSt==="")){
									$('#contractCode1').attr("disabled","disabled");
								}else{
									$('#contractCode1').removeAttr("disabled");
								}
							}else{
								$('#contractCode1').attr("disabled","disabled");
							}
						}*/

					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});

				}
		}
	}


	$scope.saveFrmStnCode = function(station){
		$scope.frmStationCode = station.stnName;
		$scope.cnmt.cnmtFromSt = station.stnCode;
		$('div#cnmtFromStationDB').dialog('close');
		$scope.CnmtFromStationDBFlag=true;
		if(angular.isUndefined($scope.cnmt.custCode) || $scope.cnmt.custCode === null || $scope.cnmt.custCode === ""||
			angular.isUndefined($scope.cnmt.cnmtDt)  || $scope.cnmt.cnmtDt === null || $scope.cnmt.cnmtDt === ""||
			angular.isUndefined($scope.cnmt.cnmtFromSt)  || $scope.cnmt.cnmtFromSt === null || $scope.cnmt.cnmtFromSt === ""){
			$('#contractCode1').attr("disabled","disabled");
			console.log("************************not possible once again");
		}else{
			var group = {
					"custCode" :  $scope.cnmt.custCode,
					"cnmtDt"	  : $scope.cnmt.cnmtDt,
					"cnmtFromSt" : $scope.cnmt.cnmtFromSt
			}
			var response = $http.post($scope.projectName+'/getContractData',group);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.contractList = data.list;
					$scope.alertToast("relevant contract exists enter contract code");
					$('#contractCode1').removeAttr("disabled");
				}else{
					$scope.alertToast("relevant contract does not exists enter contract code");
					console.log("msg from server----->"+data.result);
					$scope.cnmt.contractCode="";
					$('#contractCode1').attr("disabled","disabled");
					$scope.cnmtDC = "";
					$scope.cnmtDDL = "";
					$scope.cnmtCostGrade = "";
				}
				console.log("----->from server = "+$scope.contractList.length);
				/*if($scope.contractList.length == 0){
					$('#contractCode1').attr("disabled","disabled");
					$scope.cnmt.contractCode = "";
					$scope.cnmtDC = "";
					$scope.cnmtDDL = "";
					$scope.cnmtRate = "";
					$scope.cnmtCostGrade = "";
					$scope.cnmtVehicleType = "";
					$scope.cnmt.cnmtGuaranteeWt = "";
				}*/

				/*for(var i=0 ; i<$scope.contractList.length ; i++){
					console.log("from server $scope.contractList.contCode --->"+$scope.contractList[i].contCode);
					if($scope.contractList[i].custCode === customer.custCode){
						console.log("--------code match--------");
						if(angular.isUndefined($scope.cnmt.cnmtDt) && ($scope.cnmt.cnmtFromSt===null || angular.isUndefined($scope.cnmt.cnmtFromSt) || $scope.cnmt.cnmtFromSt==="")){
							$('#contractCode1').attr("disabled","disabled");
						}else{
							$('#contractCode1').removeAttr("disabled");
						}
					}else{
						$('#contractCode1').attr("disabled","disabled");
					}
				}*/

			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}

	}


	$scope.saveContractCode = function(contract){
		console.log("enter into saveContractCode function = "+contract.cnmtCostGrade);
		console.log("enter into saveContractCode function = "+contract.cnmtDC);
		$scope.contract = contract;
		$scope.cnmt.contractCode = contract.contCode;
		$scope.contractCodeTemp = contract.FACode;
		console.log("$scope.cnmt.contractCode.substring(0,2) = "+$scope.cnmt.contractCode.substring(0,2));
		if($scope.cnmt.contractCode.substring(0,3) === "dly"){
			$('#cnmtRateId').attr("readonly",false);
		}else if(dailyRate === true){
			$('#cnmtRateId').attr("readonly",false);
		}else{
			$('#cnmtRateId').attr("readonly",true);
		}
		
		
		if(contract.cnmtDC === "01"){
			console.log("enter into saveContractCode function = "+contract.cnmtDC);
			$('#cnmtBillAt').removeAttr("disabled");
			$scope.cnmt.cnmtDC = "1 bill";
		}else if(contract.cnmtDC === "02"){
			$('#cnmtBillAt').removeAttr("disabled");
			$scope.cnmt.cnmtDC = "2 bill";
		}else if(contract.cnmtDC === "10"){
			$('#cnmtPayAt').removeAttr("disabled");
			$scope.cnmt.cnmtDC = "Direct payment through CNMT";
		}else if(contract.cnmtDC === "20"){
			$('#cnmtPayAt').removeAttr("disabled");
			$scope.cnmt.cnmtDC = "Twice Payment";
		}else if(contract.cnmtDC === "11"){
			$('#cnmtBillAt').removeAttr("disabled");
			$scope.cnmt.cnmtDC = "Partially through CNMT";
		}
		$scope.cnmtDC      = contract.cnmtDC;
		$scope.cnmtDDL     = contract.cnmtDDL;
		$scope.cnmtCostGrade = contract.cnmtCostGrade;
		$scope.cnmt.cnmtGuaranteeWt = contract.toWeight;
		$scope.garunteeWt = contract.toWeight;
		
		$('div#contractCodeDB').dialog('close');
		$scope.ContractCodeDBFlag=true;
		//$scope.contType(contract);	

		if(contract.contType === "q" || contract.contType === "Q"){
			$('#cnmtKmId').attr("disabled","disabled");
			$('#cnmtStateId').attr("disabled","disabled");
			$scope.K_Type = false;
		}else if(contract.contType === "w" || contract.contType === "W"){
			$('#cnmtKmId').attr("disabled","disabled");
			$('#cnmtStateId').attr("disabled","disabled");
			$scope.K_Type = false;

		}else if(contract.contType === "k" || contract.contType === "K"){
			$('#cnmtKmId').removeAttr("disabled");
			$('#cnmtStateId').removeAttr("disabled");
			//$scope.cnmt.cnmtToSt = contract.toStation;
			$scope.K_Type = true;
			console.log("oyeee pagal--------------------->>>>>"+contract.toStation);
			$scope.autoToStnFlag = true;
			var stnForK = {};
			
			var response = $http.post($scope.projectName + '/getStationByStnCode', contract.toStation);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.saveToStnCode(data.station);
				}
			});
			/*
			for(var i=0;i<$scope.stationList.length;i++){
				if($scope.stationList[i].stnCode === contract.toStation){
					stnForK = $scope.stationList[i];
					break;
				}
			}			
			$scope.saveToStnCode(stnForK);
			*/

		}
	}		


	$scope.addRateForCnmtByK = function(cnmtKm,cnmtState,contractCode){
		console.log("enter into addRateForCnmtByK function");
		var rateKList = {
				"cnmtKm" : cnmtKm,
				"cnmtState"	  : cnmtState,
				"contractCode" : contractCode
		}
		var response = $http.post($scope.projectName+'/addRateForCnmtByK',rateKList);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.cnmt.cnmtRate = data.rate;
				$scope.actualCnmtRate = $scope.cnmt.cnmtRate;
				$scope.chlnLryRatePer = "Kg";
				//$scope.actualCnmtRate = data.rate;
				console.log("data.vtCode ======> "+data.vtCode);
				$scope.cnmt.cnmtVehicleType = data.vtCode;
				console.log("$scope.vtList.lenght ======> "+$scope.vtList.length);
				for(var i=0;i<$scope.vtList.length;i++){
					console.log("****************"+$scope.vtList[i].vtCode);
					if($scope.vtList[i].vtCode === data.vtCode.toLowerCase()){
						$scope.vehicleName = $scope.vtList[i].vtVehicleType;
						console.log("%%%%%%%%%%%%%%%%%%%%%$scope.vehicleName = "+$scope.vehicleName);
						break;
					}
				}
				
				
			}else{
				console.log("msg from server-->"+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}


	$scope.addRateForCnmtByQ = function(cnmtToSt,cnmtProductType,contractCode){
		console.log("enter into addRateForCnmtByQ function");
		var rateQList = {
				"cnmtToSt" : cnmtToSt,
				"cnmtProductType"	  : cnmtProductType,
				"contractCode" : contractCode,
				"cnmtVehicleType" : $scope.cnmt.cnmtVehicleType,
				"cnmtDt" : $scope.cnmt.cnmtDt
				
		}
		var response = $http.post($scope.projectName+'/addRateForCnmtByQ',rateQList);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				//mohsin
				$scope.cnmt.cnmtRate = data.rate*1000;
				$scope.actualCnmtRate = $scope.cnmt.cnmtRate;
				$scope.chlnLryRatePer = "";
				//$('#chlnLryRate1').attr("disabled","disabled");
				$scope.cnmt.cnmtGuaranteeWt = data.garunteeWt/$scope.kgInTon;
				console.log("value is-->"+$scope.cnmt.cnmtRate);
				console.log("value is-->"+$scope.cnmt.cnmtGuaranteeWt);
			}else{
				console.log("msg from server-->"+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}



	$scope.addRateForCnmtByW = function(cnmtToSt,cnmtVehicleType,contractCode){
		console.log("enter into addRateForCnmtByW function---->"+cnmtVehicleType);
		console.log("enter into addRateForCnmtByW function--cnmtToSt-->"+cnmtToSt);
		console.log("enter into addRateForCnmtByW function--contractCode-->"+contractCode);
		var rateWList = {
				"cnmtToSt"        : cnmtToSt,
				"cnmtVehicleType" : cnmtVehicleType,
				"contractCode"    : contractCode,
				"cnmtDt"          : $scope.cnmt.cnmtDt
		}
		var response = $http.post($scope.projectName+'/addRateForCnmtByW',rateWList);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.cnmt.cnmtRate = data.rate*1000;
				$scope.actualCnmtRate = $scope.cnmt.cnmtRate;
				if($scope.contract.proportionate==="p" || $scope.contract.proportionate==="P"){
					$scope.chlnLryRatePer = "Kg";  
				}else if($scope.contract.proportionate==="f" || $scope.contract.proportionate==="F"){
					$scope.chlnLryRatePer = "";
					$('#chlnLryRate1').attr("disabled","disabled");
				}
				var num =data.garWt/$scope.kgInTon;
				$scope.cnmt.cnmtGuaranteeWt = Math.round(num*100000000)/100000000;
				//$scope.cnmt.cnmtGuaranteeWt = data.garWt/$scope.kgInTon;
				console.log("$scope.cnmt.cnmtGuaranteeWt"+$scope.cnmt.cnmtGuaranteeWt);
				$scope.garWt = data.garWt/$scope.kgInTon;
				//$scope.cnmt.cnmtGuaranteeWt = data.garunteeWt/$scope.kgInTon;
				console.log("value is-->"+$scope.cnmt.cnmtRate);
				//console.log("value is-->"+$scope.cnmt.cnmtGuaranteeWt);
			}else{
				$scope.alertToast("This vehicle is not avaliable in contract");
				console.log("msg from server-->"+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}


	$scope.chngRate = function(type){
		console.log("enter into chngRate function=---->"+$scope.actualCnmtRate);
		if(type === "Kg"){
			$scope.cnmt.cnmtRate = $scope.actualCnmtRate;
		}else if(type === "Ton"){
			$scope.cnmt.cnmtRate = ($scope.actualCnmtRate * 1000);
		}
	}	

	$scope.saveConsignorCustomerCode = function(consignor){
		console.log("-<<<>>>"+consignor.custCode);
		$scope.consignorTmpCode = consignor.custFaCode;
		$scope.cnmt.cnmtConsignor = consignor.custCode;
		$('div#CNMTConsignorDB').dialog('close');
		$scope.CNMTConsignorDBFlag=true;
	}

	$scope.saveConsigneeCustomerCode = function(consignee){
		console.log("----->"+consignee.custCode);
		$scope.consigneeTmpCode = consignee.custFaCode;
		$scope.cnmt.cnmtConsignee = consignee.custCode;
		$('div#CNMTConsigneeDB').dialog('close');
		$scope.CNMTConsigneeDBFlag=true;
// for reliance primary and secoundary		
		if($scope.cnmt.cnmtConsignee==="1754"){
			$scope.cnmt.relType="S";
			$scope.cnmt.prBlCode="1755";
			console.log("in warehouse list");
//			
			var response = $http.post($scope.projectName+'/getRelWharehouse');
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.warehouseList=data.whList;
					//console.log("$scope.warehouseList="+$scope.warehouseList);

					$scope.whShowFlag=true;
					$('div#warehouseId').dialog({
						autoOpen: false,
						modal:true,
						title: "Please select Warehouse",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#warehouseId').dialog('open');

					
				}else{
					console.log("msg from server-->"+data.result);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});

			
		}else if($scope.cnmt.cnmtConsignee==="1755"){
			$scope.cnmt.relType="S";
			$scope.cnmt.prBlCode="1755";
		}
		
	};

	
// save warehouse	
	$scope.saveWarehouseCode=function(wh){
		console.log("saveWarehouseCode()"+wh[0]);
		$('div#warehouseId').dialog('close');
		$scope.cnmt.relType="P";
		$scope.cnmt.prBlCode="1754";
		$scope.cnmt.whCode=wh[0];
		
	};
	
	
	
	
	$scope.getContractData = function(){
		console.log("enter into getContractData function--->");
		var response = $http.post($scope.projectName+'/getContractData');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.dailyContract= data.dailyContract;	
				$scope.regularContract = data.regularContract;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {

			$scope.errorToast(data);
		});
	}

	$scope.getEmployeeList = function(){
		console.log(" entered into getListOfEmployee------>");
		var response = $http.post($scope.projectName+'/getListOfEmployee');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.employeeList = data.list;
				$scope.OpenEmployeeCodeDB();				
			}else{
				$scope.alertToast("you don't have any employee");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getStateList = function(){
		console.log("getStateCodeData------>");
		var response = $http.post($scope.projectName+'/getStateCodeData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stateList = data.list;
				$scope.OpenStateCodeDB();
				console.log("data from server-->"+$scope.stateList);
			}else{
				$scope.alertToast("you don't have any state");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.saveEmployeeCode =  function(employee){
		console.log("enter into saveEmployeeCode----->");
		$scope.empTempCode = employee.empFaCode;
		$scope.cnmt.cnmtEmpCode = employee.empCode;
		$('div#employeeCodeDB').dialog('close');
		$scope.EmployeeCodeDBFlag=true;
	}

	$scope.saveStateCode =  function(state){
		console.log("enter into saveStateCode----->");
		$scope.cnmt.cnmtState = state.stateCode;
		$('div#stateCodeDB').dialog('close');
		$scope.StateCodeDBFlag=true;
		if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState === null || $scope.cnmt.cnmtState ==="" || 
			angular.isUndefined($scope.cnmt.cnmtKm)|| $scope.cnmt.cnmtKm === null || $scope.cnmt.cnmtKm==="" ||$scope.cnmt.cnmtKm===0 ){
			$scope.alertToast("check state and km are filled");
		}else{
			$scope.addRateForCnmtByK($scope.cnmt.cnmtKm,$scope.cnmt.cnmtState,$scope.cnmt.contractCode);
		}
	}

	/*$scope.contType = function(contract){
		console.log("enter into contType fucntion");
		if(contract.contType === "k" || contract.contType === "K"){
			$('#cnmtKmId').removeAttr("disabled");
			$('#cnmtStateId').removeAttr("disabled");
			$scope.K_Type = true;
		}else{
			$('#cnmtKmId').attr("disabled","disabled");
			$('#cnmtStateId').attr("disabled","disabled");
			$scope.K_Type = false;
		}
	}*/

	/*$scope.chkGarWt = function(){
		console.log("entered into chkGarWt function ");
		if($scope.cnmt.cnmtGuaranteeWt > $scope.garunteeWt){
			console.log("helo helo function ");
			$scope.alertToast("gurantee weight should be less than To Weight of contract..");
			$scope.cnmt.cnmtGuaranteeWt = $scope.garunteeWt;
		}
	}*/


	$scope.calculateFreight = function(){
		if($scope.contract.contType === "k" || $scope.contract.contType === "K"){
			var LryRate = 0;
			var ActWt = 0;
			if($scope.chlnLryRatePer === "Ton"){
				if($scope.cnmtGuaranteeWtPer === "Ton"){
					$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate*$scope.cnmt.cnmtGuaranteeWt;	
				}else if($scope.cnmtGuaranteeWtPer === "Kg"){
					LryRate = $scope.cnmt.cnmtRate/1000;
					$scope.cnmt.cnmtFreight = LryRate * $scope.cnmt.cnmtGuaranteeWt;
					$scope.cnmt.cnmtFreight = parseFloat($scope.cnmt.cnmtFreight).toFixed(2);
				}
			}else if($scope.chlnLryRatePer === "Kg"){
				if($scope.cnmtGuaranteeWtPer === "Ton"){
					LryRate = $scope.cnmt.cnmtRate * 1000;
					$scope.cnmt.cnmtFreight = LryRate*$scope.cnmt.cnmtGuaranteeWt;
				}else if($scope.cnmtGuaranteeWtPer === "Kg"){
					$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate * $scope.cnmt.cnmtGuaranteeWt;
					$scope.cnmt.cnmtFreight = parseFloat($scope.cnmt.cnmtFreight).toFixed(2);
				}
			}
		}else if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			$('#chlnLryRate1').attr("disabled","disabled");
			$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate * $scope.cnmt.cnmtNoOfPkg;

		}else if($scope.contract.contType === "w" || $scope.contract.contType === "W"){
			if($scope.contract.proportionate==="p" || $scope.contract.proportionate==="P"){
				if($scope.cnmt.cnmtGuaranteeWt > $scope.garWt){
					$scope.alertToast("garuntee weight cant be greater than ToWeight of contract");
					$scope.cnmt.cnmtGuaranteeWt = $scope.garWt;
				}else{
					var LryRate = 0;
					var ActWt = 0;
					if($scope.chlnLryRatePer === "Ton"){
						if($scope.cnmtGuaranteeWtPer === "Ton"){
							$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate*$scope.cnmt.cnmtGuaranteeWt;	
						}else if($scope.cnmtGuaranteeWtPer === "Kg"){
							LryRate = $scope.cnmt.cnmtRate/1000;
							$scope.cnmt.cnmtFreight = LryRate * $scope.cnmt.cnmtGuaranteeWt;
						}
					}else if($scope.chlnLryRatePer === "Kg"){
						if($scope.cnmtGuaranteeWtPer === "Ton"){
							LryRate = $scope.cnmt.cnmtRate;// * 1000;
							$scope.cnmt.cnmtFreight = LryRate*$scope.cnmt.cnmtGuaranteeWt;
						}else if($scope.cnmtGuaranteeWtPer === "Kg"){
							$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate * $scope.cnmt.cnmtGuaranteeWt;
						}
					}

					// $scope.cnmt.cnmtFreight = $scope.cnmtRate;
				}  
			}else if($scope.contract.proportionate==="f" || $scope.contract.proportionate==="F"){
				console.log("value of $scope.garWt = "+$scope.garWt);
				console.log("value of $scope.cnmt.cnmtGuaranteeWt = "+$scope.cnmt.cnmtGuaranteeWt);
				if($scope.cnmt.cnmtGuaranteeWt > $scope.garWt){
					console.log("helo m inside f function");
					var LryRate = 0;
					var ActWt = 0;
					if(angular.isUndefined($scope.cnmt.cnmtState) || $scope.cnmt.cnmtState === "" || $scope.cnmt.cnmtState === null){
						$scope.alertToast("please enter To Station");
					}else{
						var addRateMap = {
								"contCode" : $scope.cnmt.contractCode,
								"toStn"    : $scope.cnmt.cnmtToSt,
								"vType"    : $scope.cnmt.cnmtVehicleType,
								"cnmtDt"   : $scope.cnmt.cnmtDt
						};
						
						var response = $http.post($scope.projectName+'/getAddRateFrCnmt',addRateMap);
						response.success(function(data, status, headers, config){
							if(data.result === "success"){
								$scope.contract.additionalRate = data.aRate;
								$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate + (($scope.cnmt.cnmtGuaranteeWt - $scope.garWt) * $scope.contract.additionalRate );	
							}else{
								console.log(data.result);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data);
						});
					}
				
				}else{
					console.log("helo m outside f function");
					$scope.cnmt.cnmtFreight = $scope.cnmt.cnmtRate;
				}  
			}else{
				console.log("try again");
			}
		}
	}


	$scope.calculateTOT = function(){
		$scope.cnmt.cnmtTOT = $scope.cnmt.cnmtFreight;
	}


	$scope.chkPkg = function(){
		if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){
			$scope.alertToast("please enter No. Of Packages..");
		}
	}


	$scope.submitCnmt = function(CnmtForm,cnmt){
		console.log("enter into submitCnmt function CnmtForm.$invalid--->"+CnmtForm.$invalid);
		if(CnmtForm.$invalid){
			if(CnmtForm.cnmtCode.$invalid){
				$scope.alertToast("please enter cnmt code..");
			}else if(CnmtForm.custCode.$invalid){
				$scope.alertToast("please enter customer code..");
			}else if(CnmtForm.cnmtDt.$invalid){
				$scope.alertToast("please enter cnmt date..");
			}else if(CnmtForm.cnmtFromSt.$invalid){
				$scope.alertToast("please enter from station..");
			}else if(CnmtForm.cnmtToSt.$invalid){
				$scope.alertToast("please enter to station..");
			}else if(CnmtForm.cnmtConsignor.$invalid){
				$scope.alertToast("please enter cnmt consignor..");
			}else if(CnmtForm.cnmtConsignee.$invalid){
				$scope.alertToast("Please enter cnmt consignee..");
			}else if(CnmtForm.contractCode.$invalid){
				$scope.alertToast("Please enter contract code..");
			}else if(CnmtForm.cnmtProductType.$invalid){
				$scope.alertToast("Please enter product type..");
			}/*else if(CnmtForm.cnmtKm.$invalid){
				$scope.alertToast("Please enter kilometer..");
			}else if(CnmtForm.cnmtState.$invalid){
				$scope.alertToast("Please enter state..");
			}*/else if(CnmtForm.cnmtNoOfPkg.$invalid){
				$scope.alertToast("please enter no. of package from 1-9..");
			}else if(CnmtForm.cnmtActualWt1.$invalid){
				$scope.alertToast("please Select Per Ton/kg for actual weight..");
			}else if(CnmtForm.cnmtGuaranteeWt1.$invalid){
				$scope.alertToast("please Select Per Ton/kg for guaruntee weight..");
			}else if(CnmtForm.chlnLryRate1.$invalid){
				$scope.alertToast("please Select Per Ton/kg for rate..");
			}else if(CnmtForm.cnmtVOG.$invalid){
				$scope.alertToast("Please enter value of goods..");
			}else if(CnmtForm.cnmtExtraExp.$invalid){
				$scope.alertToast("Please enter extra expenses..");
			}else if(CnmtForm.cnmtDtOfDly.$invalid){
				$scope.alertToast("Please enter correct date..");
			}else if(CnmtForm.cnmtEmpCode.$invalid){
				$scope.alertToast("Please enter employee code..");
			}else if(CnmtForm.cnmtBillAt.$invalid){
				$scope.alertToast("Please enter Bill On code..");
			}/*else if(CnmtForm.cnmtInvoiceNo.$invalid){
				$scope.alertToast("Please enter invoice no..");
			}*/
		}else{
			if($scope.Cflag === false){
				if(cnmt.custCode===null || angular.isUndefined(cnmt.custCode) || cnmt.custCode===""){
					$scope.alertToast("please fill customer code and bill at");
				}
			}else if($scope.Fflag === false){
				if(cnmt.custCode === null || angular.isUndefined(cnmt.custCode) || cnmt.custCode===""){
					$scope.alertToast("please fill customer code and pay at");
				}
			}else if(!(cnmt.custCode === null || angular.isUndefined(cnmt.custCode) || cnmt.custCode==="")){
				if(cnmt.contractCode === null || angular.isUndefined(cnmt.contractCode) || cnmt.contractCode===""){
					$scope.alertToast("please fill contract code,bill at and Select Per Ton/kg for actual weight again..");
				}else{
					if($scope.K_Type === true){
						if(angular.isUndefined(cnmt.cnmtKm) || cnmt.cnmtKm === "" || cnmt.cnmtKm === null){
							$scope.alertToast("please fill kilometer");
						}else if(angular.isUndefined(cnmt.cnmtState) || cnmt.cnmtState === "" || cnmt.cnmtState === null){
							$scope.alertToast("please fill state");
						}else{

							$scope.viewCnmtDetailsFlag=false;
							$('div#viewCnmtDetailsDB').dialog({
								autoOpen: false,
								modal:true,
								title: "Customer Info",
								show: UDShow,
								hide: UDHide,
								position: UDPos,
								resizable: false,
								draggable: true,
								close: function(event, ui) { 
									$(this).dialog('destroy') ;
									$(this).hide();
								}
							});

							$('div#viewCnmtDetailsDB').dialog('open');

						} 	 
					}else{
						$scope.viewCnmtDetailsFlag=false;
						$('div#viewCnmtDetailsDB').dialog({
							autoOpen: false,
							modal:true,
							title: "Customer Info",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							resizable: false,
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy') ;
								$(this).hide();
							}
						});

						$('div#viewCnmtDetailsDB').dialog('open');

					}
				}
			}else{
				$scope.viewCnmtDetailsFlag=false;
				$('div#viewCnmtDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					title: "Customer Info",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});

				$('div#viewCnmtDetailsDB').dialog('open');

			}
		}
	}


	$scope.saveCnmt = function(cnmt,cnmtActualWtPer,cnmtGuaranteeWtPer,chlnLryRatePer){
		console.log("enter into saveCnmt function"+cnmt.cnmtActualWt);
		if(cnmtActualWtPer === "Ton"){			
			cnmt.cnmtActualWt =(cnmt.cnmtActualWt*$scope.kgInTon);
			console.log(cnmt.cnmtActualWt);
		}
		if(cnmtGuaranteeWtPer === "Ton"){
			cnmt.cnmtGuaranteeWt =(cnmt.cnmtGuaranteeWt*$scope.kgInTon);   
			console.log(cnmt.cnmtGuaranteeWt+"$scope.kgInTon"+$scope.kgInTon);
		}
		if($scope.contract.contType === "q" || $scope.contract.contType === "Q"){//(2017-01-16) for quantity type product rate change
			$scope.cnmt.cnmtRate =($scope.cnmt.cnmtRate/$scope.kgInTon);
			console.log("Rate="+$scope.cnmt.cnmtRate);
		}else if(chlnLryRatePer === "Kg"){         //Ton changed into Kg by kamal to correct cnmt rate
			$scope.cnmt.cnmtRate =($scope.cnmt.cnmtRate/$scope.kgInTon);
			console.log("Rate="+$scope.cnmt.cnmtRate);
		}
		console.log("exit from saveCnmt function");
		//cnmt.cnmtInvoiceNo = $scope.cnmtInvList;
		
		$('#saveBtnId').attr("disabled","disabled");
			
		var response = $http.post($scope.projectName+'/submitcnmt', cnmt);
		console.log("here i m after submit cnmt-------->>>");
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				
				$('#saveBtnId').removeAttr("disabled");
				
				console.log("here i m after success-------->>>");
				$scope.viewCnmtDetailsFlag = true;
				$('div#viewCnmtDetailsDB').dialog('close');
				console.log(data);
				//$scope.getCnmtInvoiceList();
				/*for(var i=0;i<$scope.cnmtInvList.length;i++){
					 		$scope.cnmtInvList.splice(i,1);
					 	}*/
				console.log("%%%%%%%%%%%%%%%% $scope.cnmtInvList.length = "+$scope.cnmtInvList.length);
				/*$scope.cnmt.cnmtRate="";
				$scope.cnmt.cnmtCode="";
				$scope.cnmt.branchCode="";
				$scope.cnmt.custCode="";
				$scope.cnmt.cnmtDt="";
				$scope.cnmt.cnmtConsignor="";
				$scope.cnmt.cnmtConsignee="";
				$scope.cnmt.contractCode="";
				$scope.cnmt.cnmtActualWt="";
				$scope.cnmt.cnmtGuaranteeWt="";
				$scope.cnmtDC="";
				$scope.cnmt.cnmtNoOfPkg="";
				$scope.cnmt.cnmtFromSt="";
				$scope.cnmt.cnmtToSt="";
				$scope.cnmt.cnmtProductType="";
				$scope.cnmt.cnmtTOT="";
				$scope.cnmtDDL="";
				$scope.cnmt.cnmtPayAt="";
				$scope.cnmt.cnmtBillAt="";
				//$scope.cnmt.cnmtRate="";
				$scope.cnmt.cnmtFreight="";
				$scope.cnmt.cnmtVOG="";
				$scope.cnmt.cnmtExtraExp="";
				$scope.cnmtCostGrade="";
				$scope.cnmt.cnmtDtOfDly="";
				$scope.cnmt.cnmtEmpCode="";
				$scope.cnmt.cnmtKm = 0;*/
				/*$scope.cnmtConfirmImage = new File();
				$scope.cnmtImage = new File();*/
				/*$scope.cnmtConfirmImage = null;
				$scope.cnmtImage = null;*/
				/*$scope.cnmt.cnmtVehicleType="";
				$scope.cnmt.cnmtState="";*/
				
				$scope.cnmt = {};
				dailyRate = false;
				$scope.customerCode = "";
				$scope.contractCodeTemp = "";
				$scope.frmStationCode="";
				$scope.toStationCode="";
				$scope.consignorTmpCode="";
				$scope.consigneeTmpCode="";
				$scope.payOnCode="";
				$scope.billOnCode="";
				$scope.empTempCode="";
				$scope.customerName="";
				$scope.vehicleName="";
				$scope.cnmt.relType=null;
				$scope.cnmt.relChln=null;
				$scope.cnmt.prBlCode=null;
				$scope.cnmt.whCode="";
				
				$scope.cnmtInvList = [];
				$scope.contractList = [];

				$scope.contract = {};				
				$scope.stationList = [];
				//$scope.cnmt.cnmtInvoiceNo="";
				$scope.deleteAllInv();
				$scope.successToast(data.result);				
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in CnmtCntlr");
			$scope.errorToast(data);
		});	

	}

	$scope.closeViewCnmtDetailsDB = function(){
		$('div#viewCnmtDetailsDB').dialog('close');
	}


	$scope.uploadCnmtImage = function(){
		console.log("enter into uploadCnmtImage function");
		var file = $scope.cnmtImage;
		//console.log("size of file ----->"+file.size);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadCnmtImage";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}


	$scope.uploadConfCnmtImage = function(){
		console.log("enter into uploadConfCnmtImage function");
		var file = $scope.cnmtConfirmImage;
		//console.log("size of file ----->"+file.size);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadConfCnmtImage";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}

	$scope.getConsigneeList = function(keyCode){		
		console.log("Enter into getConsigneeList()");
		
		if($scope.customerName.substring(0,12)==="RELIANCE JIO"){
			var response = $http.post($scope.projectName+'/getRelConsignee');		
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){				
					$scope.consigneeList = data.customerList;
					$scope.consigneeTmpCode = "";
					$scope.OpenCnmtConsigneeDB();
				}else{
					$scope.alertToast(msg);
				}
			});
			response.error(function(data, status, headers, config) {
				console.log("Error in hitting /getRelConsignee");
			});
		
		}
		
		
		if(keyCode === 8 || parseInt($scope.consigneeTmpCode.length) < 2)
			return;
		
		var response = $http.post($scope.projectName+'/getCustomerListByNameFa', $scope.consigneeTmpCode);		
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){				
				$scope.consigneeList = data.customerList;
				$scope.consigneeTmpCode = "";
				$scope.OpenCnmtConsigneeDB();
			}else{
				$scope.alertToast(msg);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getConsigneeList");
		});
		console.log("Exit from getConsigneeList()");
	}

	$scope.saveInvoiceNo = function(saveInvoiceNoForm,invoiceNo,invoiceDt){
		console.log("enter into saveInvoiceNo function");
		if(saveInvoiceNoForm.$invalid){
			if(saveInvoiceNoForm.invoiceNo.$invalid){
				$scope.alertToast("Invoice number should be between 3-10 digits");
			}else if(saveInvoiceNoForm.invoiceDt.$invalid){
				$scope.alertToast("Please enter invoice date..");
			}
		}else{
			$scope.saveInvoiceNoFlag=true;
			$('div#saveInvoiceNo').dialog('close')
			if($.inArray(invoiceNo , $scope.cnmtInvList) !== -1){
				$scope.alertToast("Invoice Number already present");
				$scope.invoiceNo = "";
			}else {
				$scope.addInvoiceForCnmt(invoiceNo,invoiceDt);
				$scope.invoiceDt = "";
			}
		}						
	}	


	$scope.addInvoiceForCnmt = function(invoiceNo,invoiceDt){
		console.log("enter into addInvoiceForCnmt function");
		var invList = {
				"invoice" : invoiceNo,
				"date"	  : invoiceDt
		}
		var response = $http.post($scope.projectName+'/addInvoiceForCnmt',invList);
		response.success(function(data, status, headers, config) {
			$scope.getCnmtInvoiceList();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	
	//reliance challan for O&M
	$scope.saveRelChlnNo = function(relChln){
		$('div#saveRelChlnNoId').dialog('close');
		$scope.cnmt.relChln=relChln;
		console.log("$scope.cnmt.relChlnNo= "+$scope.cnmt.relChln);
		if($scope.cnmt.relChln.substring(0,2)==="60"){
			$scope.cnmt.relType="O";
			$scope.cnmt.prBlCode="1756";
			$scope.cnmt.cnmtConsignee="1756";
			console.log("relType="+$scope.cnmt.relType);
		}else if($scope.cnmt.prBlCode !="1754"){
			$scope.cnmt.relType="S";
			$scope.cnmt.prBlCode="1755";
			$scope.cnmt.cnmtConsignee="1755";
		}
	};
	
	

	$scope.getCnmtInvoiceList = function(){
		console.log("enter into getCnmtInvoiceList function");
		var response = $http.post($scope.projectName+'/getCnmtInvoiceList');
		response.success(function(data, status, headers, config) {
			console.log("msg from server --->"+data.list.length);
			$scope.cnmtInvList = data.list;
			
			
			//for relaince o&m
			if($scope.customerName.substring(0,12)==="RELIANCE JIO"){
				var str=$scope.cnmtInvList[0].invoice;
				if(str.substring(0,2)==="60"){
					console.log("str="+str);
					$scope.cnmt.relType="O";
					$scope.cnmt.prBlCode="1756";
					$scope.cnmt.cnmtConsignee="1756";
				}else{
					$scope.relChlnShowFlag=true;
					$('div#saveRelChlnNoId').dialog({
						autoOpen: false,
						modal:true,
						title: "Enter Relaince Challan No",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#saveRelChlnNoId').dialog('open');
					
					console.log("else str="+str);
				}	
			}
			
			
			//$scope.getStationData();invoice
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getStationFrom = function(keyCode){
		console.log("Enter into getStationData()");
		if(keyCode === 8 || parseInt($scope.frmStationCode.length) < 2)
			return;	
		
		var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.frmStationCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result==="success"){
				$scope.frmStationCode = "";
				$scope.stationList = data.stationList;
				$scope.OpenCnmtFromStationDB();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getStationDataByNameCode");
		});
		console.log("Exit from getStationData()");
	}
	
	$scope.getStationTo = function(keyCode){
		console.log("Enter into getStationData()");
		if(keyCode === 8 || parseInt($scope.toStationCode.length) < 2)
			return;	
		var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.toStationCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result==="success"){
				$scope.toStationCode = "";
				$scope.stationList = data.stationList;
				$scope.OpenCnmtToStationDB();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getStationDataByNameCode");
		});
		console.log("Exit from getStationData()");
	}

	$scope.getProductName = function(){
		console.log("getProductName------>");
		var response = $http.post($scope.projectName+'/getProductNameForRegCont');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.ptList = data.list;
				//$scope.getVehicleTypeCode();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getVehicleTypeCode = function(){
		console.log("getVehicleTypeCode------>");
		var response = $http.post($scope.projectName+'/getVehicleTypeCodeForChallan');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vtList = data.list;
			}else{
				$scope.alertToast("you don't have any vehicle type");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}


	$scope.deleteCnmtInv = function(cnmtInv){
		console.log("enter inot removeCnmtInv function");
		var response = $http.post($scope.projectName+'/deleteCnmtInv',cnmtInv);
		response.success(function(data, status, headers, config) {
			$scope.getCnmtInvoiceList();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}




	$scope.deleteAllInv = function(){
		console.log("enter into deleteAllInv function");
		var response = $http.post($scope.projectName+'/deleteAllInv');
		response.success(function(data, status, headers, config) {
			$scope.getCnmtInvoiceList();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}



	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getVehicleTypeCode();
		$scope.getProductName();
		$scope.deleteAllInv();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 

}]);