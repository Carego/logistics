'use strict';

var app = angular.module('application');

app.controller('BrokerCntlrApp',['$scope','$location','$http','FileUploadService',
                                 function($scope,$location,$http,FileUploadService){
	
	$scope.mobileList = [];
	$scope.pendingOwnBrkList = [];
	$scope.ownBrk = {};
	$scope.broker = {};
	$scope.currentAddress = {};
	$scope.registerAddress = {};
	$scope.otherAddress = {};
	$scope.contPerson={};
	$scope.state={};
	$scope.station={};
	$scope.stnList = {};
	$scope.stateList ={};
	
	$scope.branchCodeFlag = true;
	$scope.pendingBrokerDBFlag = true;
	$scope.CurrentAddressFlag = true;
	$scope.ContactPersonFlag=true;
	$scope.RegisterAddressFlag = true;
	$scope.OtherAddressFlag = true;
	$scope.StationFlag = true;
	$scope.StateFlag = true;
	$scope.StationCodeFlag = true;
	$scope.StateCodeFlag = true;
	$scope.VehicleTypeFlag = true;
	$scope.CAFlag=false
	$scope.CPFlag=false;
	$scope.AddStateFlag=true;
	$scope.statename =[];
	$scope.AddStationFlag = true;
	$scope.stationname=[];
	$scope.AddVehicleTypeFlag = true;
	$scope.type=[];
	$scope.multistateFlag=false;
	$scope.multistationFlag=false;
	$scope.brokerFlag = true;
	$scope.showRAFlag=false;
	$scope.showOAFlag=false;
	$scope.panCardDBFlag = true;
	
	var panImgSize = 0;
	var decImgSize = 0;
	
	$(document).ready(function() {
	    $('#addPinC').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    $('#cpMobile').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    $('#phNoId').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    $('#cpPhone').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	   
	    $('#addPinR').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#addPinO').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtLoadLimit').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#vtGuaranteeWt').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#stateName').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		});
	    
	    $('#stateSTD').keypress(function(key) {
		       if(key.charCode < 48 || key.charCode > 57)
		           return false;
	   });

	    $('#stateSTD').keypress(function(e) {
		       if (this.value.length == 5) {
		           e.preventDefault();
		       }
		});
	    
	    $('#stnPin').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	    
	    $('#stnPin').keypress(function(e) {
            if (this.value.length == 6) {
                e.preventDefault();
            }
        });
	    
	    $('#stnName').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		});
		 
		 $('#stnDistrict').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		});
	    
	    $('#addPinC').keypress(function(e) {
            if (this.value.length == 6) {
                e.preventDefault();
            }
        });
	    
	    $('#addPinR').keypress(function(e) {
            if (this.value.length == 6) {
                e.preventDefault();
            }
        });
	    
	    $('#addPinO').keypress(function(e) {
            if (this.value.length == 6) {
                e.preventDefault();
            }
        });
	    
	    $('#addressC').keypress(function(e) {
            if (this.value.length == 255) {
                e.preventDefault();
            }
        });
	    
	    $('#addressR').keypress(function(e) {
            if (this.value.length == 255) {
                e.preventDefault();
            }
        });
	    
	    $('#addressO').keypress(function(e) {
            if (this.value.length == 255) {
                e.preventDefault();
            }
        });
	    
	    $('#addCityC').keypress(function(e) {
            if (this.value.length == 20) {
                e.preventDefault();
            }
        });
	    
	    $('#addCityR').keypress(function(e) {
            if (this.value.length == 20) {
                e.preventDefault();
            }
        });
	    
	    $('#addCityO').keypress(function(e) {
            if (this.value.length == 20) {
                e.preventDefault();
            }
        });
	    
	    $('#addStateC').keypress(function(e) {
            if (this.value.length == 40) {
                e.preventDefault();
            }
        });
	    
	    $('#addStateR').keypress(function(e) {
            if (this.value.length == 40) {
                e.preventDefault();
            }
        });
	    
	    $('#addStateO').keypress(function(e) {
            if (this.value.length ==40) {
                e.preventDefault();
            }
        });
	    
	    $('#brkPPNo').keypress(function(e) {
            if (this.value.length ==25) {
                e.preventDefault();
            }
        });
	    
	    $('#brkVoterId').keypress(function(e) {
            if (this.value.length ==10) {
                e.preventDefault();
            }
        });
	    
	    $('#brkSrvTaxNo').keypress(function(e) {
            if (this.value.length ==15) {
                e.preventDefault();
            }
        });
	    
	    $('#brkRegPlace').keypress(function(e) {
            if (this.value.length ==40) {
                e.preventDefault();
            }
        });
	    
	    $('#brkFirmRegNo').keypress(function(e) {
            if (this.value.length ==20) {
                e.preventDefault();
            }
        });
	    
	    $('#BrokerName').keypress(function(e) {
            if (this.value.length ==255) {
                e.preventDefault();
            }
        });
	    
	    $('#cpName').keypress(function(e) {
            if (this.value.length ==40) {
                e.preventDefault();
            }
        });
	    
	    $('#cpMobile').keypress(function(e) {
            if (this.value.length ==10) {
                e.preventDefault();
            }
        });
	    
	    $('#phNoId').keypress(function(e) {
            if (this.value.length ==10) {
                e.preventDefault();
            }
        });
	    
	    $('#cpPhone').keypress(function(e) {
            if (this.value.length ==15) {
                e.preventDefault();
            }
        });
	    
	    $('#cpPanNo').keypress(function(e) {
            if (this.value.length ==10) {
                e.preventDefault();
            }
        });
        
        $('#brPanNoId').keypress(function(e) {
            if (this.value.length ==10) {
                e.preventDefault();
            }
        })
	    
	    $('#pan_name').keypress(function(e) {
            if (this.value.length ==50) {
                e.preventDefault();
            }
        });
	    
	    $('#union').keypress(function(e) {
            if (this.value.length ==30) {
                e.preventDefault();
            }
        });
	    
	    $('#brkCIN').keypress(function(e) {
            if (this.value.length ==30) {
                e.preventDefault();
            }
        });
	    

        $('#vtLoadLimit').keypress(function(e) {
            if (this.value.length == 7) {
                e.preventDefault();
            } 
        });
        
        $('#vtGuaranteeWt').keypress(function(e) {
        	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
                e.preventDefault();
            } 
        });
        
        $('#vtVehicleType').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#vtServiceType').keypress(function(e) {
            if (this.value.length == 30) {
                e.preventDefault();
            } 
        });
        
        $('#vtCode').keypress(function(e) {
            if (this.value.length == 1) {
                e.preventDefault();
            } 
        });
        
        $('#brkFirmRegNo').keypress(function(key) {
	        if(key.charCode < 48 || key.charCode > 57)
	        	return false;
	    });
	 }); 

	// Owner DB
	$scope.OpenPendingBrokerDB = function(){
		$scope.pendingBrokerDBFlag = false;
    	$('div#pendingBrokerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Broker",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			}); 	
		$('div#pendingBrokerDB').dialog('open');
	}
	
	$scope.OpenAddVehicleTypeDB = function(){
		$scope.AddVehicleTypeFlag = false;
    	$('div#AddVehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Vehicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#AddVehicleTypeDB').dialog('open');	
	}	
	
	$scope.openAddStateDB = function(){
		$scope.AddStateFlag = false;
    	$('div#AddStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "State",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#AddStateDB').dialog('open');
	}
	
	$scope.openAddStationDB = function(){
		$scope.AddStationFlag = false;
    	$('div#AddStationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#AddStationDB').dialog('open');
	}

	$scope.OpenBranchCodeDB = function(){
		$scope.branchCodeFlag = false;
		$('input[name=branchCode]').attr('checked',false);
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#branchCodeDB').dialog('open');
		}
	
	$scope.OpenCurrentAddressDB = function(){
		$scope.CurrentAddressFlag = false;
		$scope.currentAddress.addType="Current Address";
		
    	$('div#CurrentAddressDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Current Address",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#CurrentAddressDB').dialog('open');
		}
	
	$scope.OpenContactPersonDB = function(){
		$scope.ContactPersonFlag = false;
    	$('div#ContactPersonDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Contact Person",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#ContactPersonDB').dialog('open');
		}
	
	$scope.OpenRegisterAddressDB = function(){
		$scope.RegisterAddressFlag = false;
		$scope.registerAddress.addType="Register Address";
		
    	$('div#RegisterAddressDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Register Address",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#RegisterAddressDB').dialog('open');
		}
	
	$scope.OpenOtherAddressDB = function(){
		$scope.OtherAddressFlag = false;
		$scope.otherAddress.addType="Other Address";
		
    	$('div#OtherAddressDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Other Address",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#OtherAddressDB').dialog('open');
		}
	
	$scope.OpenStateDB = function(){
		$scope.StateFlag = false;
		$scope.state.mulStateStateCode="";
    	$('div#StateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "State",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#StateDB').dialog('open');
		}
	
	$scope.OpenStationDB = function(){
		$scope.StationFlag = false;
		$scope.station.mulStnStnCode="";
    	$('div#StationDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#StationDB').dialog('open');
		}
	
	$scope.OpenStationList = function(){
		$scope.StationCodeFlag = false;
		$('input[name=station]').attr('checked',false);
    	$('div#StationCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Station Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#StationCodeDB').dialog('open');
		}
	
	$scope.OpenStateList = function(){
		$scope.StateCodeFlag = false;
		$('input[name=state]').attr('checked',false);
    	$('div#StateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "State Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#StateCodeDB').dialog('open');
		}
	
	$scope.OpenVehicleTypeDB = function(){
		$scope.VehicleTypeFlag = false;
		$('input[name=vehicleTypeCode]').attr('checked',false);
    	$('div#VehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Vehicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#VehicleTypeDB').dialog('open');
		}
	
	$scope.savBranchCode = function(branch){
		$scope.branchCodeTemp = branch.branchName;
		$scope.broker.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('destroy');
		$scope.branchCodeFlag = true;
	}
	
	$scope.saveStationCode = function(station){
		$scope.station.mulStnStnCode = station.stnName;
		$('div#StationCodeDB').dialog('destroy');
		$scope.StationCodeFlag = true;
	}
	
	$scope.saveStateCode = function(state){
		$scope.state.mulStateStateCode = state.stateName;
		$('div#StateCodeDB').dialog('destroy');
		$scope.StateCodeFlag = true;
	}
	
	$scope.saveVehicleType = function(vt){
		$scope.broker.brkVehicleType =vt.vtCode;
		$('div#VehicleTypeDB').dialog('destroy');
		$scope.VehicleTypeFlag = true;
	}
	
	$scope.getBranchData = function(){
		   console.log("getBranchData------>");
		   var response = $http.post($scope.projectName+'/getBranchDataForBroker');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.branchList = data.list;
			   $scope.getStationData();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	$scope.getStationData = function(){
		   console.log("getStationData------>");
		   var response = $http.post($scope.projectName+'/getStationDataForBroker');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.stationList = data.list;
			   $scope.getStateData();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	$scope.getStateData = function(){
		   console.log("getStateData------>");
		   var response = $http.post($scope.projectName+'/getStateDataForBroker');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.listState = data.list;
			   $scope.getVehicleTypeCode();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	 $scope.getVehicleTypeCode = function(){
		   console.log("getVehicleTypeCode------>");
		   var response = $http.post($scope.projectName+'/getVehicleTypeCodeForBroker');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.vtList = data.list;
			   console.log("-->"+data.list);
			   }else{
				console.log(data);   
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 		
	 $scope.saveState = function(StateForm,state){
			console.log("enter into saveState function--->");
			 $('div#StateDB').dialog('destroy');
			 $scope.StateFlag=true;
			 $scope.multistateFlag=true;
			 console.log("StateForm.$invalid----"+StateForm.$invalid);
			 if(StateForm.$invalid){
				 if(StateForm.mulStateStateCode.$invalid){
					 $scope.alertToast("please Enter State Name");
				 }
			 }else{
			var response = $http.post($scope.projectName+'/addStateForBroker',state);
			response.success(function(data, status, headers, config) {
				$scope.successToast(data.result);
				$scope.fetchStateList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	 } 
	 	
	 $scope.fetchStateList = function(){
			console.log("enter into fetchStateList function");
			 var response = $http.get($scope.projectName+'/fetchStateListForBroker');
			 response.success(function(data, status, headers, config){
				 	$scope.stateList = data.list;
				 	console.log($scope.stateList);
				 	//$scope.removeAllStation();	
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	}	
	 
	 $scope.removeState = function(state) {
			console.log("enter into removeState function");
			var response = $http.post($scope.projectName+'/removeStateForBroker',state);
			response.success(function(data, status, headers, config) {
				$scope.fetchStateList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	 
	 $scope.removeAllState = function() {
			var response = $http.post($scope.projectName+'/removeAllStateForBroker');
			response.success(function(data, status, headers, config) {
				$scope.fetchStateList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    };
	    
	    $scope.saveStation = function(StationForm,station){
			console.log("enter into saveStation function--->");
			$('div#StationDB').dialog('destroy');
			 $scope.StationFlag=true;
			 $scope.multistationFlag=true;
			if(StationForm.$invalid){
				if(StationForm.mulStnStnCode.$invalid){
					$scope.alertToast("please Enter Station Name");
				}
			}
			else{
			var response = $http.post($scope.projectName+'/addStationForBroker',station);
			response.success(function(data, status, headers, config) {
				$scope.successToast(data.result);
				$scope.fetchStationList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	    }
	    
	    $scope.fetchStationList = function(){
			console.log("enter into fetchStationList function");
			 var response = $http.get($scope.projectName+'/fetchStationListForBroker');
			 response.success(function(data, status, headers, config){
				 	$scope.stnList = data.list;
				 	$scope.getBranchData();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}	
	    
	    $scope.removeStation = function(station) {
			console.log("enter into removeStation function");
			var response = $http.post($scope.projectName+'/removeStationForBroker',station);
			response.success(function(data, status, headers, config) {
				$scope.fetchStationList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
	 
	 $scope.removeAllStation = function() {
		 console.log("enter into removeAllStation function");
			var response = $http.post($scope.projectName+'/removeAllStationForBroker');
			response.success(function(data, status, headers, config) {
				$scope.fetchStationList();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    };
	    
	    $scope.saveCurrentAddress = function(CurrentAddressForm,currentAddress){
			 $('div#CurrentAddressDB').dialog('destroy');
			 $scope.CurrentAddressFlag=true;
			 $scope.CAFlag=true;
			 
			 console.log("CurrentAddressForm.$invalid---"+CurrentAddressForm.$invalid);
			 if(CurrentAddressForm.$invalid){
				 if(CurrentAddressForm.address.$invalid){
					 $scope.alertToast("enter correct address in current address");
				 }else if(CurrentAddressForm.addCity.$invalid){
					 $scope.alertToast("city name should be between 3-15 characters in current address");
				 }else if(CurrentAddressForm.addState.$invalid){
					 $scope.alertToast("enter correct State in current address");
				 }else if(CurrentAddressForm.addPin.$invalid){
					 $scope.alertToast("Pin in Current address should be of 6 digits");
				 }
			 }
			 else{
				 $scope.alertToast("Your entry is saved");
			 }
		 }	
		 
		 $scope.saveRegisterAddress = function(RegisterAddressForm,registerAddress){
			 $('div#RegisterAddressDB').dialog('destroy');
			 $scope.RegisterAddressFlag=true;
			 $scope.showRAFlag=true;
			 console.log("enter into saveRegisterAddress function");
			 console.log("RegisterAddressForm.$invalid"+RegisterAddressForm.$invalid);
			 if(RegisterAddressForm.$invalid){
				 if(RegisterAddressForm.address.$invalid){
					 $scope.alertToast("enter correct address in Register address");
				 }else if(RegisterAddressForm.addCity.$invalid){
					 $scope.alertToast("city cannot exceed 15 characters in Register address");
				 }else if(RegisterAddressForm.addState.$invalid){
					 $scope.alertToast("enter correct State in Register address");
				 }else if(RegisterAddressForm.addPin.$invalid){
					 $scope.alertToast("Pin should be of 6 digits in Register address");
				 } 
				 $scope.registerAddress.completeAdd="";
				 $scope.registerAddress.addCity="";
				 $scope.registerAddress.addState="";
				 $scope.registerAddress.addPin="";
				 $scope.registerAddress.addType="no";
				 $scope.showRAFlag=false;
			 }else{
				 $scope.alertToast("Your entry is saved");
			 }
		 }
		 
		 $scope.saveOtherAddress = function(OtherAddressForm,otherAddress){
			 $('div#OtherAddressDB').dialog('destroy');
			 $scope.OtherAddressFlag=true;
			 $scope.showOAFlag=true;
			 console.log("enter into saveOtherAddress function");
			 console.log("OtherAddressForm.$invalid"+OtherAddressForm.$invalid);
			 if(OtherAddressForm.$invalid){
				 if(OtherAddressForm.address.$invalid){
					 $scope.alertToast("enter correct address in Other Address");
				 }else if(OtherAddressForm.addCity.$invalid){
					 $scope.alertToast("city cannot exceed 15 characters in Other Address");
				 }else if(OtherAddressForm.addState.$invalid){
					 $scope.alertToast("enter correct State in Other Address");
				 }else if(OtherAddressForm.addPin.$invalid){
					 $scope.alertToast("Pin should be of 6 digits in Other Address");
				 } 
				  $scope.otherAddress.completeAdd="";
				   $scope.otherAddress.addCity="";
				   $scope.otherAddress.addState="";
				   $scope.otherAddress.addPin="";
				   $scope.otherAddress.addType="no";
				   $scope.showOAFlag=false;
			 }
			 else{
				 $scope.alertToast("Your entry is saved");
			 }
		 }
		 
		 $scope.saveContactPerson = function(ContactPersonForm,contPerson,mobNo){
			 $('div#ContactPersonDB').dialog('destroy');
			 $scope.CPFlag=true;
			 $scope.ContactPersonFlag=true;
			 console.log("enter into saveContactPerson function");
			 console.log("ContactPersonForm.$invalid"+ContactPersonForm.$invalid);
			 if(ContactPersonForm.$invalid){
				 if(ContactPersonForm.cpName.$invalid){
					 $scope.alertToast("please Enter correct Name in Contact Person");
				 }else  if(ContactPersonForm.cpMobile.$invalid){
					 $scope.alertToast("Mobile number should be of 10 digits in Contact Person");
				 }else  if(ContactPersonForm.cpPhone.$invalid){
					 $scope.alertToast("Phone number should be between 4-15 digits in Contact Person");
				 }else  if(ContactPersonForm.cpPanNo.$invalid){
					 $scope.alertToast("Pan number should be of 10 digits in Contact Person");
				 }
			 }else{
				 $scope.mobileList.push(mobNo);
				 $scope.contPerson.cpMobile = $scope.mobileList;
				 $scope.alertToast("Your entry is saved");
			 }
		 }
	    
	    $scope.SubmitBroker = function(BrokerForm,CurrentAddressForm,ContactPersonForm,broker,contPerson,currentAddress){
			 console.log("enter into SubmitBroker function");
			 if(BrokerForm.$invalid){
				 if(BrokerForm.branchCode.$invalid){
					 $scope.alertToast("please Enter Branch Code");
				 }else if(BrokerForm.brkName.$invalid){
					 $scope.alertToast("please Enter Owner Name");
				 }else if(BrokerForm.brkVehicleType.$invalid){
					 $scope.alertToast("please Enter vehicle type");
				 }else if(BrokerForm.brkVoterId.$invalid){
					 $scope.alertToast("please Enter voterID number of 10 digits");
				 }else if(BrokerForm.brkPPNo.$invalid){
					 $scope.alertToast("please Enter PassPort No number within 8-25 digits");
				 }else if(BrokerForm.brkRegPlace.$invalid){
					 $scope.alertToast("please Enter Reg Place within 40 digits");
				 }else if(BrokerForm.brkEmailId.$invalid){
					 $scope.alertToast("please Enter correct email-Id");
				 }else if(BrokerForm.brkSrvTaxNo.$invalid){
					 $scope.alertToast("please Enter service tax number within 15 digits");
				 }else if(BrokerForm.brkFirmType.$invalid){
					 $scope.alertToast("please Enter Firm Type");
				 } else if(BrokerForm.brkBsnCard.$invalid){
					 $scope.alertToast("please Enter Business card ");
				 } 
			 }
			 
			
			 else{ console.log("$scope.CAFlag++++"+$scope.CAFlag);
				/* if($scope.CAFlag===false){
				 if(currentAddress.completeAdd===null || currentAddress.completeAdd ==="" || angular.isUndefined(currentAddress.completeAdd) || currentAddress.addCity=== null || currentAddress.addCity==="" || angular.isUndefined(currentAddress.addCity) || currentAddress.addState===null || currentAddress.addState==="" || angular.isUndefined(currentAddress.addState)
					|| currentAddress.addPin===null || currentAddress.addPin==="" || angular.isUndefined(currentAddress.addPin) || currentAddress.addType===null || currentAddress.addType==="" || angular.isUndefined(currentAddress.addType))
					 {
					 $scope.alertToast("please Enter current address");
					 }
				 }
				 
				 else if(CurrentAddressForm.$invalid){
					 if(CurrentAddressForm.address.$invalid){
						 $scope.alertToast("enter correct address in current address");
					 }else if(CurrentAddressForm.addCity.$invalid){
						 $scope.alertToast("city name should be between 3-15 characters in current address");
					 }else if(CurrentAddressForm.addState.$invalid){
						 $scope.alertToast("enter correct State in current address");
					 }else if(CurrentAddressForm.addPin.$invalid){
						 $scope.alertToast("Pin in Current address should be of 6 digits");
					 }
				 }
				 
				 else if($scope.CPFlag===false){ 
					 if(contPerson.cpName===null || contPerson.cpName==="" || angular.isUndefined(contPerson.cpName) || contPerson.cpMobile===null || contPerson.cpMobile==="" || angular.isUndefined(contPerson.cpMobile) || contPerson.cpPhone===null || contPerson.cpPhone==="" || angular.isUndefined(contPerson.cpPhone) || contPerson.cpPanNo==="" || contPerson.cpPanNo ===null || angular.isUndefined(contPerson.cpPanNo))
					 {
					 $scope.alertToast("please Enter contact person");
					 }
				 }
				 
				 else if(ContactPersonForm.$invalid){
					 if(ContactPersonForm.cpName.$invalid){
						 $scope.alertToast("please Enter correct Name in Contact Person");
					 }else  if(ContactPersonForm.mobNo.$invalid){
						 $scope.alertToast("Mobile number should be of 10 digits in Contact Person");
					 }else  if(ContactPersonForm.cpPhone.$invalid){
						 $scope.alertToast("Phone number should be between 4-15 digits in Contact Person");
					 }else  if(ContactPersonForm.cpPanNo.$invalid){
						 $scope.alertToast("Pan number should be of 10 digits in Contact Person");
					 }
				 }
				 
				 else if($scope.multistateFlag===false){
					 if(state.mulStateStateCode===null || state.mulStateStateCode==="" || angular.isUndefined(state.mulStateStateCode))
						 {
						 $scope.alertToast("please Enter atleast one state");
						 }
				 }
				 
				 else if($scope.multistationFlag===false){
					 if(station.mulStnStnCode===null || station.mulStnStnCode==="" || angular.isUndefined(station.mulStnStnCode))
						 {
						 $scope.alertToast("please Enter atleast one station");
						 }
				 } else {*/
					 $scope.brokerFlag = false;
				    	$('div#brokerDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						    }
							});
						$('div#brokerDB').dialog('open');
				 //}
			 }
	    }
	  
			 $scope.back= function(){
					$scope.brokerFlag = true;
					$('div#brokerDB').dialog('close');
				}
			 
			 
			 $scope.saveBroker = function(broker,currentAddress,contPerson){
				 console.log("Enter into save Broker Function");
				 $('div#brokerDB').dialog('close');
				 
				 $scope.broker.brkPhNoList = [];
				 $scope.broker.brkPhNoList.push($scope.phNo);
				 
				 var finalObject = {
						 "contPerson"			: $scope.contPerson,
						 "currentAddress" 		: $scope.currentAddress,
					 	 "registerAddress"      : $scope.registerAddress,
					 	 "otherAddress"         : $scope.otherAddress,
					 	 "broker"               : $scope.broker
					 };
				 
				 
				 $('#saveBtnId').attr("disabled","disabled");
					 var response = $http.post($scope.projectName+'/verifyBroker',finalObject);
					   response.success(function(data, status, headers, config){
						   if(data.result==="success"){
							   $('#saveBtnId').removeAttr("disabled");
							   $scope.successToast(data.result);
							  /* $scope.broker.branchCode="";
							   $scope.broker.brkName="";
							   $scope.broker.brkComStbDt="";
							   $scope.broker.brkPanDOB="";
							   $scope.broker.brkPanDt="";
							   $scope.broker.brkPanName="";
							   $scope.broker.brkVehicleType="";
							   $scope.broker.brkPPNo="";
							   $scope.broker.brkVoterId="";
							   $scope.broker.brkSrvTaxNo="";
							   $scope.broker.brkFirmRegNo="";
							   $scope.broker.brkRegPlace="";
							   $scope.broker.brkFirmType="";
							   $scope.broker.brkCIN="";
							   $scope.broker.brkBsnCard="";
							   $scope.broker.brkUnion="";
							   $scope.broker.brkEmailId="";
							   $scope.broker.brkActiveDt="";
							   $scope.broker.brkDeactiveDt="";*/
							   $scope.broker = {};
							   $scope.branchCodeTemp = "";
							   $scope.phNo = "";
							   $scope.currentAddress.completeAdd="";
							   $scope.currentAddress.addCity="";
							   $scope.currentAddress.addState="";
							   $scope.currentAddress.addPin="";
							  
							   $scope.registerAddress.completeAdd="";
							   $scope.registerAddress.addCity="";
							   $scope.registerAddress.addState="";
							   $scope.registerAddress.addPin="";
							   
							   $scope.contPerson.cpName="";
							  /* for(var i=0;i<$scope.contPerson.cpMobile.length;i++){
								   $scope.contPerson.cpMobile.splice(i,1);
							   }*/
							   $scope.contPerson.cpMobile = [];
							   $scope.stateList = [];
							   $scope.stnList = [];
							   //$scope.contPerson.cpMobile="";
							   $scope.contPerson.cpPhone="";
							   $scope.contPerson.cpPanNo="";
							   $('#cpMobile').val('');
							   
							   $scope.otherAddress.completeAdd="";
							   $scope.otherAddress.addCity="";
							   $scope.otherAddress.addState="";
							   $scope.otherAddress.addPin="";
							   
							   $scope.multistateFlag=false;
							   $scope.multistationFlag=false;
							   $scope.fetch();
						   }else{
							console.log(data);   
							if(!angular.isUndefined(data.msg)){
								$scope.alertToast(data.msg);
							}
						   }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});

			 }
			 
	    $scope.saveNewState = function(AddStateForm,newState){
			console.log("Entr to save new state function--");
			$('div#AddStateDB').dialog("destroy");
			$scope.AddStateFlag=true;
			newState.stateName = newState.stateName.toUpperCase();
			
			if($scope.listState){
				for(var i =0;i<$scope.listState.length;i++){
					$scope.statename.push($scope.listState[i].stateName);
					console.log("list values "+$scope.listState[i].stateName);
				}
				
				if($.inArray(newState.stateName,$scope.statename)!== -1){
					console.log("newState.stateName-"+newState.stateName);
					$scope.alertToast("Name already exists");
				}else{
					var response = $http.post($scope.projectName+'/saveNewState',newState);
					response.success(function(data, status, headers, config) {
						$scope.successToast(data.result);
						if(data.result==="success"){
							$scope.alertToast(data.result);
							$scope.getStateData();
							$scope.newState="";	
						}else{
							$scope.errorToast(data.result);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}else{
				var response = $http.post($scope.projectName+'/saveNewState',newState);
				response.success(function(data, status, headers, config) {
					$scope.successToast(data.result);
					if(data.result==="success"){
						$scope.alertToast(data.result);
						$scope.getStateData();
						$scope.newState="";
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		}
	    

	    $scope.saveNewStation = function(AddStationForm,newStation){
			console.log("Entr to save new state function--");
			$('div#AddStationDB').dialog("destroy");
			$scope.AddStationFlag=true;
			
			newStation.stnName = newStation.stnName.toUpperCase();
			if($scope.stationList){
				for(var i =0;i<$scope.stationList.length;i++){
					$scope.stationname.push($scope.stationList[i].stnName);
					console.log("list values "+$scope.stationList[i].stnName);
				}
				
				if($.inArray(newStation.stnName,$scope.stationname)!== -1){
					$scope.alertToast("Name already exists");
				}else{
					var response = $http.post($scope.projectName+'/saveNewStation',newStation);
					response.success(function(data, status, headers, config) {
						$scope.successToast(data.result);
						if(data.result==="success"){
							$scope.alertToast(data.result);
							  $scope.getStationData();
							  $scope.newStation="";	
						}else{
							$scope.errorToast(data.result);
						}	
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}else{
				var response = $http.post($scope.projectName+'/saveNewStation',newStation);
				response.success(function(data, status, headers, config) {
					$scope.successToast(data.result);
					if(data.result==="success"){
						$scope.alertToast(data.result);
						  $scope.getStationData();
						  $scope.newStation="";
					}else{
						$scope.errorToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}	
		}

		$scope.saveNewvehicletype = function(AddVehicleForm,vt){
			$scope.AddVehicleTypeFlag=true;
			console.log("enter into saveVehicleType function--->");
			$('div#AddVehicleTypeDB').dialog("destroy");
			vt.vtServiceType=vt.vtServiceType.toUpperCase();
			vt.vtVehicleType=vt.vtVehicleType.toUpperCase();
			$scope.code = vt.vtServiceType+vt.vtVehicleType;
			if($scope.vtList){
				for(var i=0;i<$scope.vtList.length;i++){
					$scope.type[i] = $scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType;
					$scope.type.push($scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType);
				}
				if(AddVehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(AddVehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(AddVehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(AddVehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleType',vt);
				response.success(function(data, status, headers, config) {
					$scope.successToast(data.result);
					$scope.getVehicleTypeCode();
					$scope.vt="";
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}else{
				if(AddVehicleForm.vtGuaranteeWt.$invalid){
					$scope.alertToast("Enter correct guarantee weight");
				}else if(AddVehicleForm.vtLoadLimit.$invalid){
					$scope.alertToast("Load limit can't be greater than 7 digits");
				}else if(AddVehicleForm.vtVehicleType.$invalid){
					$scope.alertToast("Enter correct vehicle type");
				}else if(AddVehicleForm.vtServiceType.$invalid){
					$scope.alertToast("Enter correct service type");
				}else if($.inArray($scope.code,$scope.type)!==-1){
					$scope.alertToast("Name already exists");
					$scope.vt="";
				}else {
				var response = $http.post($scope.projectName+'/saveVehicleType',vt);
				response.success(function(data, status, headers, config) {
					$scope.successToast(data.result);
					$scope.getVehicleTypeCode();
					$scope.vt="";
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			}			
}
		
		
		
		$scope.uploadPanImg = function(){
			console.log("enter into uploadPanImg function");
			var file = $scope.panImg;
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else if(file.size > $scope.maxFileSize){
				$scope.alertToast("image size must be less than or equal to 1mb");
			}else{
				panImgSize = file.size;
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldBrkPanImg";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			}
		}
		
		
		
		
		$scope.uploadDecImg = function(){
			console.log("enter into uploadDecImg function");
			var file = $scope.decImg;
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else if(file.size > $scope.maxFileSize){
				$scope.alertToast("image size must be less than or equal to 1mb");
			}else{
				decImgSize = file.size;
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldBrkDecImg";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			}	
		}
		
		
		
		$scope.openPanDetail = function(){
			console.log("enter into openPanDetail function");
			 $scope.panCardDBFlag = false;
		    	$('div#panCardDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.panCardDBFlag = true;
				    }
					});
			$('div#panCardDB').dialog('open');
		}	
		
		
		$scope.savePanCard = function(panCardForm){
			console.log("enter into savePanCard function = "+panCardForm.$invalid + "panImgSize = "+panImgSize);
			if(panCardForm.$invalid){
				 if(panCardForm.brkPanName.$invalid){
					 $scope.alertToast("please Enter Pan Name");
				 }else if(panCardForm.brkPanNoName.$invalid){
					 $scope.alertToast("please Enter Valid PanCard No ");
				 }
			}else{
				if(panImgSize > 0){
					$('div#panCardDB').dialog('close');
				}else{
					$scope.alertToast("please uplaod scan image of PanCard");
				}
			}
		}
		
		$scope.savOwnBrk = function(ownBrk){
			console.log("Enter into saveOwnBrk()...");
			var response = $http.post($scope.projectName+'/getOwnBrkDtById', ownBrk);
			response.success(function(data, status, headers, config){
				console.log("Request /getOwnBrkDtById success");
				if(data.result === "success"){
					console.log("Broker detail found !");
					$scope.ownBrk = data.pendingOwnBrk;
					$scope.broker.brkId = $scope.ownBrk.ownBrkId; 
					$scope.broker.brkName = $scope.ownBrk.ownBrkName;					
					$scope.broker.branchCode = $scope.ownBrk.branchCode;	
					$scope.branchCodeTemp = $scope.broker.branchCode;			
					$scope.addressList = $scope.ownBrk.addressList;
					// Fill current address, register address and other address					
					for(var i=0; i<$scope.addressList.length; i++){
						$scope.address = $scope.addressList[i];
						if($scope.address.addType === "currentAddress"){
							$scope.currentAddress.completeAdd = $scope.address.completeAdd;
							$scope.currentAddress.addCity = $scope.address.addCity;
							$scope.currentAddress.addState = $scope.address.addState;
							$scope.currentAddress.addPin = $scope.address.addPin;
							$scope.currentAddress.addType = $scope.address.addType;
						}else if($scope.address.addType === "registerAddress"){
							$scope.registerAddress.completeAdd = $scope.address.completeAdd;
							$scope.registerAddress.addCity = $scope.address.addCity;
							$scope.registerAddress.addState = $scope.address.addState;
							$scope.registerAddress.addPin = $scope.address.addPin;
							$scope.registerAddress.addType = $scope.address.addType;
						}else if($scope.address.addType = "otherAddress"){
							$scope.otherAddress.completeAdd = $scope.address.completeAdd;
							$scope.otherAddress.addCity = $scope.address.addCity;
							$scope.otherAddress.addState = $scope.address.addState;
							$scope.otherAddress.addPin = $scope.address.addPin;
							$scope.otherAddress.addType = $scope.address.addType;
						}
					}				
					
					$scope.broker.brkUnion = $scope.ownBrk.ownBrkUnion;
					$scope.broker.brkEmailId = $scope.ownBrk.ownBrkEmailId;				
					// Add state
					$scope.phNo = parseInt($scope.ownBrk.ownBrkPhoneNo);
					$scope.broker.brkVehicleType = $scope.ownBrk.ownBrkVehicleType;
					// pan detail
					$scope.broker.brkPanName =  $scope.ownBrk.ownBrkPanName;
					$scope.broker.brkPanDt =  $scope.ownBrk.ownBrkPanDt;				
					$scope.broker.brkPanNo =  $scope.ownBrk.ownBrkPanNo;
					$scope.broker.brkPanDOB =  $scope.ownBrk.ownBrkPanDOB;
					// add station										
					$scope.broker.brkSrvTaxNo = $scope.ownBrk.ownBrkSerTaxNo;
					$scope.broker.brkPPNo = $scope.ownBrk.ownBrkPPNo;
					$scope.broker.brkVoterId = $scope.ownBrk.ownBrkVoterId;
					$scope.broker.brkFirmRegNo = $scope.ownBrk.ownBrkFirmRegNo;
					$scope.broker.brkRegPlace = $scope.ownBrk.ownBrkRegPlace;
					$scope.broker.brkCIN = $scope.ownBrk.ownBrkComIdNo;
					$scope.broker.brkBsnCard = $scope.ownBrk.ownBrkBsnCard;
					$scope.broker.brkFirmType = $scope.ownBrk.ownBrkFirmType;
					$scope.broker.brkComStbDt = $scope.ownBrk.ownBrkComEstDt;
					$scope.broker.brkActiveDt = $scope.ownBrk.ownBrkActiveDt;
				}else{
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(){
				console.log("Error in saveOwnBrk()...");
			});
			$('div#pendingBrokerDB').dialog('close');
			$scope.pendingBrokerDBFlag = false;
			console.log("Exit from saveOwnBrk()...");
		}
		
		// Fetching Pending Broker List
		$scope.fetchPendingBrokers = function(){
			console.log("Enter into fetchPendingBrokers()...");
			var response = $http.post($scope.projectName+'/getPendingBrokers');
			response.success(function(data, status, headers, config){
				console.log("Request /getPendingBrokers success");
				if(data.result === "success"){
					console.log("Pending Broker list found !");
					$scope.pendingOwnBrkList = data.pendingOwnBrkList;				
				}else{
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(){
				console.log("Error in fetchingPendingOwners !");
			});		
			console.log("Exit from fetchPendingOwners()...");
		}
		
	    $scope.fetch = function(){
	    	console.log("Enter into fetch()....");
	    	$scope.fetchPendingBrokers();
			$scope.removeAllState();
			$scope.removeAllStation();
			console.log("Exit from fetch()....");
		}
	    
	    if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	    	 $scope.fetch();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
	   
	   
}]);