package com.mylogistics.controller;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ContPersonDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.MultipleStateDAO;
import com.mylogistics.DAO.MultipleStationDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.MultipleState;
import com.mylogistics.model.MultipleStation;
import com.mylogistics.model.Owner;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ImageService;
import com.mylogistics.services.ModelService;
import com.mylogistics.services.MultipleStateService;
import com.mylogistics.services.MultipleStateServiceImpl;
import com.mylogistics.services.MultipleStationService;
import com.mylogistics.services.MultipleStationServiceImpl;

@Controller
public class BrokerCntlr {
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired 
	private MultipleStateDAO multipleStateDAO;
	
	@Autowired
	private MultipleStationDAO multipleStationDAO;
	
	@Autowired
	private ContPersonDAO contPersonDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private MultipleStateService multipleStateService = new MultipleStateServiceImpl();
	
	private MultipleStationService multipleStationService = new MultipleStationServiceImpl();
	
	private ImageService imageService = new ImageService();
	
	
	public static Logger logger = Logger.getLogger(BrokerCntlr.class);
	
	@RequestMapping(value = "/getBranchDataForBroker", method = RequestMethod.POST)
	public @ResponseBody Object getBranchData() {

		List<Branch> branchList = new ArrayList<Branch>();
		Map<String, Object> map = new HashMap<String, Object>();
		branchList = branchDAO.getAllActiveBranches();
		if(!branchList.isEmpty()){
			map.put("list", branchList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getStateDataForBroker", method = RequestMethod.POST)
	public @ResponseBody Object getStateData() {
		
		List<State> listState = stateDAO.getStateData();
		Map<String, Object> map = new HashMap<String, Object>();
		if(!listState.isEmpty()){
			map.put("list", listState);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getStationDataForBroker", method = RequestMethod.POST)
	public @ResponseBody Object getStationData() {
		System.out.println("Enter into getStationData function ");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Station> stationList = new ArrayList<Station>();
		 stationList = stationDAO.getStationData();
		 for(int i=0;i<stationList.size();i++){
			 System.out.println("Inside list---------"+stationList.get(i).getStateCode());
			 System.out.println("Inside list---------"+stationList.get(i).getStnCode());
			 System.out.println("Inside list---------"+stationList.get(i).getStnName());
		 }
		 if(!stationList.isEmpty()){
				map.put("list", stationList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	return map;
}
	@RequestMapping(value = "/getVehicleTypeCodeForBroker", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleTypeCode() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<VehicleType> vtList = new ArrayList<VehicleType>();
		 vtList = vehicleTypeDAO.getVehicleType();
		 if (!vtList.isEmpty()) {	 
			 map.put("list", vtList);
			 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		 }else{
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }
		return map;
	}
	
	@RequestMapping(value = "/fetchStateListForBroker", method = RequestMethod.GET)
	public @ResponseBody Object fetchStateListForBroker() {

		List<MultipleState> multipleStates = multipleStateService.getAllState();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multipleStates);
		return map;
	}

	
	@RequestMapping(value = "/addStateForBroker", method = RequestMethod.POST)
	public @ResponseBody Object addStateForBroker(@RequestBody MultipleState multipleState) {

		multipleStateService.addState(multipleState);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeStateForBroker", method = RequestMethod.POST)
	public @ResponseBody Object removeStateForBroker(@RequestBody MultipleState multipleState) {
		
		multipleStateService.deleteState(multipleState);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllStateForBroker", method = RequestMethod.POST)
	public @ResponseBody Object removeAllStateForBroker() {
	
		multipleStateService.deleteAllState();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchStationListForBroker", method = RequestMethod.GET)
	public @ResponseBody Object fetchStationListForBroker() {

		List<MultipleStation> multipleStations = multipleStationService.getAllStation();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multipleStations);
		return map;
	}
	
	@RequestMapping(value = "/addStationForBroker", method = RequestMethod.POST)
	public @ResponseBody Object addStationForBroker(@RequestBody MultipleStation multipleStation) {

		multipleStationService.addStation(multipleStation);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeStationForBroker", method = RequestMethod.POST)
	public @ResponseBody Object removeStationForBroker(@RequestBody MultipleStation multipleStation) {

		multipleStationService.deleteStation(multipleStation);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllStationForBroker", method = RequestMethod.POST)
	public @ResponseBody Object removeAllStationForBroker() {

		multipleStationService.deleteAllStation();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/SubmitBroker", method = RequestMethod.POST)
	public @ResponseBody Object SubmitBroker(@RequestBody ModelService modelService){
		User user = (User)httpSession.getAttribute("currentUser");
		Map<String, String> resultMap = new HashMap<String, String>();
		
		logger.info("UserID = "+user.getUserId()+" : Enter into /SunmitBroker()");
		
		Address currentAddress = modelService.getCurrentAddress();
		Address registerAddress = modelService.getRegisterAddress();
		Address otherAddress = modelService.getOtherAddress();		
		
		Broker broker = modelService.getBroker();	
		
		String brokerCode=null;		
		String faCode=null;
		
		List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_BRK);
		
		FAParticular fAParticular = new FAParticular();
		fAParticular = faParticulars.get(0);
		int fapId = fAParticular.getFaPerId();
		
		String fapIdStr = String.valueOf(fapId);
		
		if(fapId < 10)
			fapIdStr = "0"+fapIdStr;
		
		broker.setfAParticular(fAParticular);
		broker.setView(false);
		broker.setUserCode(user.getUserCode());
		broker.setbCode(user.getUserBranchCode());
		
		BrokerImg brokerImg = new BrokerImg();
			
		//TODO Pan interest rate calculation
		if (imageService.getImage() != null && imageService.getDecImg() != null) 
			broker.setBrkPanIntRt(0);
		else if (imageService.getImage() != null ) 			
			if (broker.getBrkPanNo().charAt(3) == 'P' || broker.getBrkPanNo().charAt(3) == 'H' || broker.getBrkPanNo().charAt(3) == 'J')
				broker.setBrkPanIntRt(1);
			else 
				broker.setBrkPanIntRt(2);			
		
		Broker isAvailable = brokerDAO.findBrokerByBrh_Name(broker.getBranchCode(), broker.getBrkName());
		
		if(isAvailable == null){
			isAvailable = null;
				
			Integer brkId = brokerDAO.saveBrkAndImg(broker, brokerImg, imageService.getImage(), imageService.getDecImg());
				
			if(brkId > 0){
				
				brokerCode = "brk"+String.valueOf(brkId);				
				brkId = brkId+100000;
				String brkIdStr = String.valueOf(brkId).substring(1,6);
				faCode = fapIdStr+brkIdStr;
				
				logger.info("UserID = "+user.getUserId()+" : GeneratedFaCode = "+faCode);
				
				broker.setBrkFaCode(faCode);
				int updateBroker = brokerDAO.updateBroker(broker);
					
				FAMaster faMaster = new FAMaster();
				faMaster.setFaMfaCode(faCode);
				faMaster.setFaMfaType("broker");
				faMaster.setFaMfaName(broker.getBrkName());
				faMaster.setbCode(user.getUserBranchCode());
				faMaster.setUserCode(user.getUserCode());
				
				faMasterDAO.saveFaMaster(faMaster);
					
				if (currentAddress.getAddType() != null) {
					currentAddress.setUserCode(user.getUserCode());
					currentAddress.setbCode(user.getUserBranchCode());
					currentAddress.setAddRefCode(brokerCode);
					
					addressDAO.saveAddress(currentAddress);
				}
					
				if(registerAddress.getAddType() != null && !(registerAddress.getAddType().equalsIgnoreCase("no"))){
					registerAddress.setUserCode(user.getUserCode());
					registerAddress.setbCode(user.getUserBranchCode());
					registerAddress.setAddRefCode(brokerCode);
					
					addressDAO.saveAddress(registerAddress);
				}
					
				if(otherAddress.getAddType() != null && !(otherAddress.getAddType().equalsIgnoreCase("no"))){
					otherAddress.setUserCode(user.getUserCode());
					otherAddress.setbCode(user.getUserBranchCode());
					otherAddress.setAddRefCode(brokerCode);
					
					addressDAO.saveAddress(otherAddress);
				}
					
				List<MultipleState> mStates = multipleStateService.getAllState();
				List<MultipleStation> mStations = multipleStationService.getAllStation();
					
				for(int i =0;i<mStates.size();i++){
					mStates.get(i).setMulStateRefCode(brokerCode);
					mStates.get(i).setbCode(user.getUserBranchCode());
					mStates.get(i).setUserCode(user.getUserCode());
					
					multipleStateDAO.saveMultipleState(mStates.get(i));
				}
				for(int i = 0;i<mStations.size();i++){
					mStations.get(i).setMulStnRefCode(brokerCode);
					mStations.get(i).setbCode(user.getUserBranchCode());
					mStations.get(i).setUserCode(user.getUserCode());
					
					multipleStationDAO.saveMultipleStation(mStations.get(i));
				}
				
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("msg", "Broker is saved !");
					
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please try again !");
			}
				
		}else{
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Broker already exists !");
		}
		logger.info("Exit from /SubmitBroker()");
		return resultMap;
	}
	
	@RequestMapping(value = "/getBrokerCodes", method = RequestMethod.POST)
	public @ResponseBody Object getBrokerCodes() {
		
		List<String> bList = brokerDAO.getBrokerCode();
		Map<String, Object> map = new HashMap<String, Object>();
		if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/saveNewStateForBroker", method = RequestMethod.POST)
	public @ResponseBody Object saveNewStateForBroker(@RequestBody State state) {

		System.out.println("Controller called of saveNewStateForBroker");

		Map<String, String> map = new HashMap<String, String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		state.setUserCode(currentUser.getUserCode());
		state.setbCode(currentUser.getUserBranchCode());
		int temp = stateDAO.saveStateToDB(state);
		if (temp >= 0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/saveNewStationForBroker", method = RequestMethod.POST)
	public @ResponseBody Object saveNewStationForBroker(@RequestBody Station station) {

		System.out.println("Controller called of saveNewStationForBroker");

		Map<String, String> map = new HashMap<String, String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		station.setUserCode(currentUser.getUserCode());
		station.setbCode(currentUser.getUserBranchCode());
		int temp = stationDAO.saveStationToDB(station);
		if (temp >= 0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/upldBrkPanImg", method = RequestMethod.POST)
	public @ResponseBody Object upldBrkPanImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldBrkPanImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setImage(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setImage(blob) ;
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/upldBrkDecImg", method = RequestMethod.POST)
	public @ResponseBody Object upldBrkDecImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldBrkDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setDecImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setDecImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/upldBrkChqImg", method = RequestMethod.POST)
	public @ResponseBody Object upldBrkChqImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldBrkChqImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setChqImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setChqImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/getBrkCodeList", method=RequestMethod.POST)
	public @ResponseBody Object getBrkCodeList(){
		System.out.println("Entered into getBrkCodeList");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> brkCodeList = brokerDAO.getBrkNameCodeIdFa();
		
		if (!brkCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("brkCodeList", brkCodeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getBrokerByNameCode", method = RequestMethod.POST)
	public @ResponseBody Object getOwnerByNameCode(@RequestBody String brkNameCode){
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("UserID = "+user.getUserId()+" : Enter into /getBrokerByNameCode() : BrkNameCode = "+brkNameCode);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			List<String> proList = new ArrayList<String>();
			
			proList.add(BrokerCNTS.BRK_ID);
			proList.add(BrokerCNTS.BRK_NAME);
			proList.add(BrokerCNTS.BRK_CODE);
			proList.add(BrokerCNTS.BRK_PAN_No);			
			
			List<Broker> brkList = brokerDAO.getBrokerByNameCode(session, user, brkNameCode, proList);
			
			if(brkList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such broker found !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("brokerList", brkList);
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from /getBrokerByNameCode() : BrKNameCode = "+brkNameCode);
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/getBrokerView", method=RequestMethod.POST)
	public @ResponseBody Object getBrokerView(){
		System.out.println("Entered into getBrokerView");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> brkCodeList = brokerDAO.getBrkNamePhCodeIdFa();
		
		if (!brkCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", brkCodeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}		
		return map;
	}
	

	@RequestMapping(value = "/checkBrokerPan", method=RequestMethod.POST)
	public @ResponseBody Object checkBrokerPan(@RequestBody String brkPanNo){
		System.out.println("Entered into getBrokerPan");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> brkCodeList = brokerDAO.getBrkNameCodeByPanNo(brkPanNo);
		
		if (!brkCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", brkCodeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}		
		return map;
	}
	
	
	
	
	
	@RequestMapping(value = "/SubmitBrokerN", method = RequestMethod.POST)
	public @ResponseBody Object SubmitBrokerN(@RequestBody ModelService modelService){
		User user = (User)httpSession.getAttribute("currentUser");
		Map<String, String> resultMap = new HashMap<String, String>();
		
		logger.info("UserID = "+user.getUserId()+" : Enter into /SunmitBrokerN()");
		Session session=this.sessionFactory.openSession();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		Transaction transaction=null;
		Address currentAddress = modelService.getCurrentAddress();
		
		Broker broker = modelService.getBroker();	
		
		try {
			
			Boolean isAvailable = brokerDAO.isBrokerExist(session, broker.getBrkPanNo());
			
			if(isAvailable) {
				String brokerCode=null;		
				String faCode=null;
				transaction=session.beginTransaction();
				List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_BRK,session);
				
				FAParticular fAParticular = new FAParticular();
				fAParticular = faParticulars.get(0);
				int fapId = fAParticular.getFaPerId();
				
				String fapIdStr = String.valueOf(fapId);
				
				if(fapId < 10)
					fapIdStr = "0"+fapIdStr;
				
				broker.setfAParticular(fAParticular);
				broker.setView(false);
				broker.setUserCode(user.getUserCode());
				//broker.setbCode(user.getUserBranchCode());
				broker.setBranchCode(user.getUserBranchCode());
				
				BrokerImg brokerImg = new BrokerImg();
					
				//TODO Pan interest rate calculation
				if (imageService.getImage() != null && imageService.getDecImg() != null) 
					broker.setBrkPanIntRt(0);
				else if (imageService.getImage() != null ) 			
					if (broker.getBrkPanNo().charAt(3) == 'P' || broker.getBrkPanNo().charAt(3) == 'H' || broker.getBrkPanNo().charAt(3) == 'J')
						broker.setBrkPanIntRt(1);
					else 
						broker.setBrkPanIntRt(2);			
				
				
						
					Integer brkId = brokerDAO.saveBrkAndImgN(broker, brokerImg, imageService.getImage(), imageService.getDecImg(),imageService.getChqImg(),session);
						
					
					imageService.setImage(null);
					imageService.setDecImg(null);
					imageService.setChqImg(null);
					if(brkId > 0){
						
						brokerCode = "brk"+String.valueOf(brkId);				
						brkId = brkId+100000;
						String brkIdStr = String.valueOf(brkId).substring(1,6);
						faCode = fapIdStr+brkIdStr;
						
						logger.info("UserID = "+user.getUserId()+" : GeneratedFaCode = "+faCode);
						
						broker.setBrkFaCode(faCode);
						 brokerDAO.updateBroker(session,broker);
							
						FAMaster faMaster = new FAMaster();
						faMaster.setFaMfaCode(faCode);
						faMaster.setFaMfaType("broker");
						faMaster.setFaMfaName(broker.getBrkName());
						//faMaster.setbCode(user.getUserBranchCode());
						faMaster.setbCode(broker.getbCode());
						faMaster.setUserCode(user.getUserCode());
						
						faMasterDAO.saveFaMaster(faMaster,session);
							
						Address address=new Address();
						address.setAddCity(modelService.getCurrentAddress().getAddCity());
						address.setAddPin(modelService.getCurrentAddress().getAddPin());
						address.setAddDist(modelService.getCurrentAddress().getAddDist());
						address.setAddPost(modelService.getCurrentAddress().getAddPost());
						address.setAddRefCode(brokerCode);
						address.setAddState(modelService.getCurrentAddress().getAddState());
						address.setAddType("Current Address");
						address.setCompleteAdd(modelService.getCurrentAddress().getCompleteAdd());
						//address.setbCode(currentUser.getUserBranchCode());
						address.setbCode(broker.getbCode());
						address.setUserCode(currentUser.getUserCode());
						addressDAO.saveAddress(address,session);
							
						
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						resultMap.put("msg", "Broker is saved !");
						resultMap.put("brkCode", brokerCode);
							
					}
					session.flush();
					session.clear();
					transaction.commit();
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Broker already exists !");
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("Exception in saving broker "+e);
			transaction.rollback();
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Exception in saving Broker !");
			
		}finally {
			session.close();
		}
		
		logger.info("Exit from /SubmitBroker()");
		return resultMap;
	}
	
	
	@RequestMapping(value = "/getBrokerFrBnkVerify", method=RequestMethod.POST)
	public @ResponseBody Object getBrokerFrBnkVerify(){
		System.out.println("Entered into getBrokerFrBnkVerify");
		Map<String, Object> map = new HashMap<>();
		
		List<Broker> brkList = brokerDAO.getBrkFrBnkVerify();
		
		if (!brkList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("brkList", brkList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "No record found");
		}		
		return map;
	}
	
	
	@RequestMapping(value = "/validBnkDet", method=RequestMethod.POST)
	public @ResponseBody Object validBnkDet(@RequestBody int brkId){
		System.out.println("Entered into validBnkDet");
		Map<String, String> map = new HashMap<>();
		
			map=brokerDAO.bnkInfoValid(brkId);
				
		return map;
	}
	
	
	@RequestMapping(value = "/invalidBnkDet", method=RequestMethod.POST)
	public @ResponseBody Object invalidBnkDet(@RequestBody int brkId){
		System.out.println("Entered into validBnkDet");
		Map<String, String> map = new HashMap<>();
		
			map=brokerDAO.bnkInfoInValid(brkId);
		
		return map;
	}
	
	
	@RequestMapping(value = "/getBrokerInvalidAcc", method=RequestMethod.POST)
	public @ResponseBody Object getBrokerInvalidAcc(){
		System.out.println("Entered into getBrokerInvalidAcc");
		Map<String, Object> map = new HashMap<>();
		
		List<Broker> brkList = brokerDAO.getBrokerInvalidAcc();
		
		if (!brkList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("brkList", brkList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "No record found");
		}		
		return map;
	}
	
	
	@RequestMapping(value = "/updateBrkBnkDet", method=RequestMethod.POST)
	public @ResponseBody Object updateBrkBnkDet(@RequestBody Broker brk){
		System.out.println("Entered into updateBrkBnkDet");
		Map<String, String> map = new HashMap<>();
		
			map=brokerDAO.bnkInfoUpdate(brk);
		
		return map;
	}
	
	
	
	@RequestMapping(value = "/uploadBrkChqImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadBrkChqImage(@RequestParam("file") MultipartFile file,@RequestParam("id") Integer brkId){	
		logger.info("Enter into /uploadBrkChqImage() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		
		Map<String, String> resultMap = new HashMap<String, String>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = brokerDAO.uploadChqImg(fileInBytes, brkId);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put("result", "error");
			resultMap.put("msg", "There is some problem to upload file");
			resultMap.put("exp", "Exception="+e);
		}
		logger.info("Exit from /upldExcelFile()");
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/uploadBrkPanImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadBrkPanImage(@RequestParam("file") MultipartFile file,@RequestParam("id") Integer brkId){	
		logger.info("Enter into /uploadBrkPanImage() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		
		Map<String, String> resultMap = new HashMap<String, String>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = brokerDAO.uploadPanImg(fileInBytes, brkId);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put("result", "error");
			resultMap.put("msg", "There is some problem to upload file");
			resultMap.put("exp", "Exception="+e);
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/uploadBrkDecImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadBrkDecImage(@RequestParam("file") MultipartFile file,@RequestParam("id") Integer brkId){	
		logger.info("Enter into /uploadBrkDecImage() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		
		Map<String, String> resultMap = new HashMap<String, String>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = brokerDAO.uploadDecImg(fileInBytes, brkId);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put("result", "error");
			resultMap.put("msg", "There is some problem to upload file");
			resultMap.put("exp", "Exception="+e);
		}
		return resultMap;
	}
	
	
	
}

