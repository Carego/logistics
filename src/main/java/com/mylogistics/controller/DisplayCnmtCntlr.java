package com.mylogistics.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.pdf.codec.Base64;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayCnmtCntlr {


	@Autowired
	CnmtDAO cnmtDAO;

	@Autowired
	private HttpSession httpSession;

	@RequestMapping(value="/getBiltyIsViewNo",method = RequestMethod.POST)
	public @ResponseBody Object getBiltyIsViewNo(){

		System.out.println("Controller called of getBiltyIsViewNo");
		Map<String,Object> map = new HashMap<String,Object>();

		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();

		List<Cnmt> cList = cnmtDAO.getCnmtisViewFalse(branchCode);
		List<Map<String,Object>> finalcList = new ArrayList<Map<String,Object>>();
		System.out.println("Controller called after retrieving list---------"+cList);

		if(!cList.isEmpty()){
			for(int i=0;i<cList.size();i++){
				/*System.out.println("The values in the list is"+challans.get(i).getBranchCode());
				System.out.println(" -------------->"+challans.get(0).getChallanImage().length());*/
				Map<String,Object> cnmtMap = new HashMap<String, Object>();
				cnmtMap.put(CnmtCNTS.CNMT_ID,cList.get(i).getCnmtId());
				cnmtMap.put(CnmtCNTS.CNMT_CODE,cList.get(i).getCnmtCode() );
				cnmtMap.put(CnmtCNTS.BRANCH_CODE,cList.get(i).getBranchCode());
				cnmtMap.put(CnmtCNTS.CNMT_DT,cList.get(i).getCnmtDt());
				cnmtMap.put(CnmtCNTS.CUST_CODE,cList.get(i).getCustCode());
				cnmtMap.put(CnmtCNTS.CNMT_CONSIGNOR,cList.get(i).getCnmtConsignor());
				cnmtMap.put(CnmtCNTS.CNMT_CONSIGNEE,cList.get(i).getCnmtConsignee());
				cnmtMap.put(CnmtCNTS.CNMT_NO_OF_PKG,cList.get(i).getCnmtNoOfPkg());
				cnmtMap.put(CnmtCNTS.CNMT_ACTUAL_WT,cList.get(i).getCnmtActualWt());
				cnmtMap.put(CnmtCNTS.CNMT_PAY_AT,cList.get(i).getCnmtPayAt());
				cnmtMap.put(CnmtCNTS.CNMT_BILL_AT,cList.get(i).getCnmtBillAt());
				cnmtMap.put(CnmtCNTS.CNMT_FREIGHT,cList.get(i).getCnmtFreight());
				cnmtMap.put(CnmtCNTS.CNMT_VOG,cList.get(i).getCnmtVOG());
				cnmtMap.put(CnmtCNTS.CNMT_EXTRA_EXP,cList.get(i).getCnmtExtraExp());
				cnmtMap.put(CnmtCNTS.CNMT_DT_OF_DLY,cList.get(i).getCnmtDtOfDly());
				cnmtMap.put(CnmtCNTS.CNMT_EMP_CODE,cList.get(i).getCnmtEmpCode());
				cnmtMap.put(CnmtCNTS.CNMT_KM,cList.get(i).getCnmtKm());
				cnmtMap.put(CnmtCNTS.CNMT_STATE,cList.get(i).getCnmtState());
				cnmtMap.put(CnmtCNTS.CONTRACT_CODE,cList.get(i).getContractCode());
				cnmtMap.put(CnmtCNTS.CNMT_INVOICE_NO,cList.get(i).getCnmtInvoiceNo());
				cnmtMap.put(CnmtCNTS.CREATION_TS,cList.get(i).getCreationTS());
				cnmtMap.put(CnmtCNTS.USER_BRANCH_CODE,cList.get(i).getbCode());
				cnmtMap.put(CnmtCNTS.IS_VIEW,cList.get(i).isView());
				cnmtMap.put(CnmtCNTS.USER_CODE,cList.get(i).getUserCode());

				finalcList.add(cnmtMap);
			}
		}

		if(!finalcList.isEmpty()){
			System.out.println("enter into if -------------->"+cList.get(0).getBranchCode());
			map.put("list",finalcList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
			System.out.println("map is ready for client");
		}else {
			System.out.println("enter into else -------------->");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		System.out.println("finally send the data");
		return map;
	}

	@RequestMapping(value = "/getAllBiltyCodes" , method = RequestMethod.GET)  
	public @ResponseBody Object getAllBiltyCodes(){

		System.out.println("Entered into getAllBiltyCodes mapping---->");
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode=currentUser.getUserBranchCode();
		System.out.println("branch code of current user------>"+branchCode);

		List<Cnmt> cnmtCodeList = cnmtDAO.getAllBiltyCodes(branchCode);

		Map<String,Object> map = new HashMap<String,Object>();
		if (!(cnmtCodeList.isEmpty())) {				
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("cnmtCodeList",cnmtCodeList);
		}else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;

		/*User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<Cnmt> cList = cnmtDAO.getCnmtisViewFalse(branchCode);
		String cnmtCodes [] = new String[cList.size()];

		for(int i=0;i<cList.size();i++){
			cnmtCodes[i]=cList.get(i).getCnmtCode();	
		}

		return cnmtCodes;*/

	} 

	@RequestMapping(value="/downloadConfCnmt" , method = RequestMethod.POST)
	public void downloadConfCnmt(HttpServletRequest request , HttpServletResponse response )throws Exception {
		System.out.println("enter into downloadCnmt funntion");
		String cnmtCode = request.getParameter("submitConfImageName");
		System.out.println("*************cnmtCode = "+cnmtCode);
		//String chlnCode = challan.getChlnCode();
		String fileName = "final_"+cnmtCode+".jpg";

		Blob cnmtImage = cnmtDAO.getCnmtConfImageByCode(cnmtCode);

		//Blob chlnImage = challan.getChallanImage();

		InputStream in = cnmtImage.getBinaryStream();
		int fileLength = in.available();
		System.out.println("####################size of downloading image = " + fileLength);
		String mimeType = "application/octet-stream";
		response.setContentType(mimeType);
		response.setContentLength(fileLength);
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		OutputStream out = response.getOutputStream();
		byte[] buffer = new byte[2048];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
		}
		in.close();
		out.flush();
		out.close(); 
		//return "success";
	}



	@RequestMapping(value="/downloadCnmt" , method = RequestMethod.POST)
	public void downloadCnmt(HttpServletRequest request , HttpServletResponse response )throws Exception {
		System.out.println("enter into downloadCnmt funntion");
		String cnmtCode = request.getParameter("submitImageName");
		System.out.println("*************cnmtCode = "+cnmtCode);
		//String chlnCode = challan.getChlnCode();
		String fileName = cnmtCode+".jpg";

		Blob cnmtImage = cnmtDAO.getCnmtImageByCode(cnmtCode);

		//Blob chlnImage = challan.getChallanImage();

		InputStream in = cnmtImage.getBinaryStream();
		int fileLength = in.available();
		System.out.println("####################size of downloading image = " + fileLength);
		String mimeType = "application/octet-stream";
		response.setContentType(mimeType);
		response.setContentLength(fileLength);
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		OutputStream out = response.getOutputStream();
		byte[] buffer = new byte[2048];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
		}
		in.close();
		out.flush();
		out.close(); 
		//return "success";
	}



	/*@RequestMapping(value="/downloadCnmt" , method = RequestMethod.POST)*/
	/*public void cnmtDownload(InputStream in,String fileName)throws Exception {
		System.out.println("enter into cnmtDownload funntion");
		//byte[] image = IOUtils.toByteArray(in);
		int fileLength = in.available();
		System.out.println("####################size of downloading image = " + fileLength);
		String mimeType = "application/octet-stream";
		HttpServletResponse responsenew = new HttpServletResponseWrapper(responsenew);
		response.setContentType(mimeType);
		response.setContentLength(fileLength);
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		OutputStream out = response.getOutputStream();
		byte[] buffer = new byte[2048];
		 int bytesRead = -1;
		 while ((bytesRead = in.read(buffer)) != -1) {
            out.write(buffer, 0, bytesRead);
        }
		in.close();
		out.flush();
        out.close(); 	

	}*/

}