'use strict';

var app = angular.module('application');

app.controller('ViewBranchCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.branch = {};
	$scope.showBranchDetails = true;
	$scope.branchCodeDBFlag = true;
	$scope.br = {};
	
	$scope.showTableFlag = false;
	
	$scope.bankMstrList = [];
	
	 $scope.Submit = function(BranchForm,br){
			console.log("enter into submit function for branchDetails--->"+$scope.br.branchCode);
			$scope.showTableFlag = false;
			$scope.bankMstrList = [];
			if(BranchForm.$invalid){
				console.log("BranchForm.$invalid--->"+BranchForm.$invalid);
				if(BranchForm.branchName.$invalid){
					$scope.alertToast("Please enter branch code....");
				}	
			}else{
				var response = $http.post($scope.projectName+'/branchDetails', $scope.br.branchCode);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.branch = data.branch;
						$scope.showBranchDetails = false;
						$scope.branch.branchOpenDt =  $filter('date')($scope.branch.branchOpenDt, 'MM/dd/yyyy hh:mm:ss');
						$scope.branch.branchCloseDt =  $filter('date')($scope.branch.branchCloseDt, 'MM/dd/yyyy hh:mm:ss');
						$scope.branch.creationTS = $filter('date')($scope.branch.creationTS, 'MM/dd/yyyy hh:mm:ss');
						$scope.successToast(data.result);
						
					}else{
						console.log(data);
					}				
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
	 }
	 
	 $scope.openBranchCodeDB = function(){
			$scope.branchCodeDBFlag = false;
			$('div#branchCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Branch Code",
				draggable: true,
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
			});
		
			$('div#branchCodeDB').dialog('open');
	 }   
	 
	 $scope.getAllBranchesList = function(){
		 console.log("entered into getAllBranchesList function------>");
		 var response = $http.post($scope.projectName+'/getAllBranchesList');
		 response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.branchCodeList = data.branchCodeList;
				 $scope.successToast(data.result);
			 }else{
				 console.log(data);
			 }	
		 });
		 response.error(function(data, status, headers, config) {
			 $scope.errorToast(data);
		 });
	 }
	   
	 $scope.saveBranchCode =  function(branch){
			console.log("enter into saveBranchCode----->"+branch.branchCode);
			$scope.br.branchName = branch.branchName;
			$scope.br.branchCode = branch.branchCode;
			$('div#branchCodeDB').dialog('close');
			$scope.branchCodeDBFlag = true;
	}
	 
	 $scope.getChildBankMstrList = function(){
		 console.log("entered into getChildBankMstrList function------>");
		 
		 var response = $http.post($scope.projectName+'/getChildBankMstrList', $scope.branch.branchId);
			response.success(function(data, status, headers, config){
				if (data.result === "success") {
					$scope.bankMstrList = data.bankMstrList;
					console.log("Success");
					$scope.showTableFlag = true;
				}else{
					console.log("Eror");
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getBankMstr Error: "+data);
			});
	 }
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getAllBranchesList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);