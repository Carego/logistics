package com.mylogistics.controller.webservices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylogistics.DAO.werservice.LoadingDao;
import com.mylogistics.model.webservice.ChallanApp;
import com.mylogistics.model.webservice.LoadingModel;
import com.mylogistics.model.webservice.VendorDetail;
import com.mylogistics.services.ConstantsValues;

@RestController
public class LoadingCntlr {

	public static Logger logger = Logger.getLogger(LoadingCntlr.class);
	
	@Autowired
	private LoadingDao loadingDao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@RequestMapping(value = "/getOwnBrkWs", method = RequestMethod.POST)
	public @ResponseBody Object getOwnBrkWs(@RequestParam("branchCode") String branchCode){
		logger.info("Enter into /getOwnBrkWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{			
			resultMap = loadingDao.getOwnBrkWs(session, branchCode);
		}catch(Exception e){
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getOwnBrkWs()");
		return resultMap;
	}
	
	@RequestMapping(value = "/getTrafficOfficers", method = RequestMethod.POST)
	public @ResponseBody Object getTrafficOfficers(){
		logger.info("Enter into /getTrafficOfficers()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{			
			List<String> nameList = loadingDao.getTrafficOfficers(session);
			if(nameList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Officer not found !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("list", nameList);
			}
		}catch(Exception e){
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getTrafficOfficers()");
		return resultMap;
	}
	
	@RequestMapping(value = "/getLoadingDt", method = RequestMethod.POST)
	public @ResponseBody Object getLoadingDt(@RequestBody Map<String, String> initParam){
		logger.info("Enter into /getLoadingDt() : InitParam = "+initParam);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			String branchCode = initParam.get("branchCode");
			String fromDate = initParam.get("fromDate");
			String toDate = initParam.get("toDate");
			String officerName = initParam.get("offerName");
			
			if(branchCode == null)
				branchCode = "";			
			if(fromDate == null)
				fromDate = "";
			if(toDate == null)
				toDate = "";
			if(officerName == null)
				officerName = "";
			
			List<Map<String, Object>> list = loadingDao.getLoadingDt(session, branchCode, fromDate, toDate, officerName);
			if(list.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such record !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("list", list);
			}			
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getLoadingDt() : "+resultMap);
		return resultMap;
	}
	
	@RequestMapping(value = "/saveLoadingWs", method = RequestMethod.POST)	
	public @ResponseBody Object saveLoadingWs(@RequestParam("loading") String loading){
		logger.info("Enter into /save LoadingWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			if(loading == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Details not found !");
			}else{
				try{
					logger.info("JSON = "+loading);
					ObjectMapper mapper = new ObjectMapper();
					LoadingModel loadingModel = mapper.readValue(loading, LoadingModel.class);				
					resultMap = loadingDao.saveLoadingDetail(session, loadingModel);
					Object result = resultMap.get(ConstantsValues.RESULT);
					if(result != null){
						if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS))
							transaction.commit();
						else
						transaction.rollback();
					}else{
						transaction.rollback();
					}
				}catch(Exception e){
					transaction.rollback();
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					logger.error("Error in converting JSON = Exception : "+e);
					resultMap.put("msg", "Please try again !");
				}
			}
		}catch(Exception e){
			transaction.rollback();
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /saveLoadingWs()");
		return resultMap;
	}
	
	@RequestMapping(value = "/getPendingOrderWs", method = RequestMethod.POST)	
	public @ResponseBody Object getPendingOrderWs(@RequestParam("branchCode") String branchCode){
		logger.info("Enter into /getPendingOrderWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			if(branchCode == null || branchCode.equalsIgnoreCase("")){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Enter branch code !");
			}else
				resultMap = loadingDao.getPendingOrder(session, branchCode, resultMap);
		}catch(Exception e){
			logger.error("Exception = "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getPendingOrderWs()");
		return resultMap;
	}
	
	@RequestMapping(value = "/saveLoadingModelWs", method = RequestMethod.POST)	
	public @ResponseBody Object saveLoadingModelWs(@RequestParam("loading") String loading){
		logger.info("Enter into /saveLoadingModelWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			if(loading == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Details not found !");
			}else{
				try{
					logger.info("JSON = "+loading);
					ObjectMapper mapper = new ObjectMapper();
					LoadingModel loadingModel = mapper.readValue(loading, LoadingModel.class);				
					resultMap = loadingDao.saveLoadingDetail1(session, loadingModel);
					Object result = resultMap.get(ConstantsValues.RESULT);
					if(result != null){
						if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS))
							transaction.commit();
						else
						transaction.rollback();
					}else{
						transaction.rollback();
					}
				}catch(Exception e){
					transaction.rollback();
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					logger.error("Error in converting JSON = Exception : "+e);
					resultMap.put("msg", "Please try again !");
				}
			}
		}catch(Exception e){
			transaction.rollback();
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /saveLoadingModelWs()");
		return resultMap;
	}
	
	@RequestMapping(value = "/saveVendorDetailWs", method = RequestMethod.POST)	
	public @ResponseBody Object saveVendorDetailWs(@RequestParam("loading") String loading){
		logger.info("Enter into /saveVendorDetailWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			if(loading == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Details not found !");
			}else{
				try{
					logger.info("JSON = "+loading);
					ObjectMapper mapper = new ObjectMapper();
					LoadingModel loadingModel = mapper.readValue(loading, LoadingModel.class);				
					resultMap = loadingDao.saveVendorDetail(session, loadingModel);
					Object result = resultMap.get(ConstantsValues.RESULT);
					if(result != null){
						if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS))
							transaction.commit();
						else
						transaction.rollback();
					}else{
						transaction.rollback();
					}
				}catch(Exception e){
					transaction.rollback();
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					logger.error("Error in converting JSON = Exception : "+e);
					resultMap.put("msg", "Please try again !");
				}
			}
		}catch(Exception e){
			transaction.rollback();
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /saveVendorDetailWs()");
		return resultMap;
	}
	
	
	@RequestMapping(value = "/getVendorDtWs", method = RequestMethod.POST)	
	public @ResponseBody Object getVendorDT(@RequestParam("ldId") Integer ldId){
		logger.info("Enter into /getVendorDtWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			if(ldId == null || ldId < 0){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Details not found !");
			}else						
				resultMap = loadingDao.getVendorDt(session, ldId, resultMap);
			
		}catch(Exception e){
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getVendorDtWs() : Result = "+resultMap);
		return resultMap;
	}
	
	@RequestMapping(value = "/saveFinalOrderWs", method = RequestMethod.POST)	
	public @ResponseBody Object saveFinalOrderWs(@RequestParam("vendorDt") String vendorDt){
		logger.info("Enter into /saveFinalOrderWs()");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			if(vendorDt == null || vendorDt.equalsIgnoreCase("")){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Vendor detail not found !");
				transaction.rollback();
			}else{
				logger.info("JSON = "+vendorDt);
				ObjectMapper mapper = new ObjectMapper();
				VendorDetail vDt = mapper.readValue(vendorDt, VendorDetail.class);				
				resultMap = loadingDao.saveFinalOrder(session, vDt, resultMap);
				Object status = resultMap.get(ConstantsValues.RESULT);
				if(status == null){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Vendor detail not found !");
					transaction.rollback();
				}else if(String.valueOf(status).equalsIgnoreCase(ConstantsValues.SUCCESS))
					transaction.commit();
			}
		}catch(Exception e){
			logger.error("Excetpion : "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /saveFinalOrderWs() : Result = "+resultMap);
		return resultMap;
	}
	
}