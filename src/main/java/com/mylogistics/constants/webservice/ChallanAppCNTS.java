package com.mylogistics.constants.webservice;

public class ChallanAppCNTS {

	public static final String CHLN_ID = "chlnId";
	public static final String CHLN_CODE = "chlnCode";
	public static final String BRANCH_CODE = "branchCode";
	public static final String CHLN_CHD_CODE = "chlnChdCode";
	public static final String CHLN_FROM_STN_CODE = "chlnFromStnCode";
	public static final String CHLN_FROM_STN_NAME = "chlnFromStnName";
	public static final String CHLN_TO_STN_CODE = "chlnToStnCode";
	public static final String CHLN_TO_STN_NAME = "chlnToStnName";
	public static final String CHLN_EMP_CODE = "chlnEmpCode";
	public static final String CHLN_NO_OF_PKG = "chlnNoOfPkg";
	public static final String CHLN_LRY_RATE = "chlnLryRate";
	public static final String CHLN_LRY_TYPE = "chlnLryType";
	public static final String CHLN_CHG_WT = "chlnChgWt";
	public static final String CHLN_CHG_WT_TYPE = "chlnChgWtType";
	public static final String CHLN_FREIGHT = "chlnFreight";
	public static final String CHLN_LOADING_AMOUNT = "chlnLoadingAmount";
	public static final String CHLN_EXTRA = "chlnExtra";
	public static final String CHLN_STATISTICAL_CHG = "chlnStatisticalChg";
	public static final String CHLN_TDS_AMT = "chlnTdsAmt";
	public static final String CHLN_TOTAL_FREIGHT = "chlnTotalFreight";
	public static final String CHLN_ADVANCE = "chlnAdvance";
	public static final String CHLN_BALANCE = "chlnBalance";
	public static final String CHLN_PAY_AT = "chlnPayAt";
	public static final String CHLN_PAY_AT_CODE = "chlnPayAtCode";
	public static final String CHLN_TIME_ALLOW = "chlnTimeAllow";
	public static final String CHLN_DT = "chlnDt";
	public static final String CHLN_BR_RATE = "chlnBrRate";
	public static final String CHLN_TOTAL_WT = "chlnTotalWt";
	public static final String CHLN_WT_SLIP = "chlnWtSlip";
	public static final String CHLN_LRY_REP_DT = "chlnLryRepDT";
	public static final String CHLN_VEHICLE_TYPE = "chlnVehicleType";
	public static final String CHLN_VEHICLE_CODE = "chlnVehicleCode";
	public static final String CHLN_RR_NO = "chlnRrNo";
	public static final String CHLN_TRAIN_NO = "chlnTrainNo";
	public static final String CHLN_LRY_LOADTIME = "chlnLryLoadTime";
	public static final String CHLN_LRY_RPT_TIME = "chlnLryRptTime";
	public static final String CHLN_LRY_NO = "chlnLryNo";
	public static final String USER_CODE = "userCode";
	public static final String IS_PENDING = "isPending";
	public static final String IS_CANCEL = "isCancel";
	public static final String CHLN_CREATION_TIME = "creationTS";
	
}