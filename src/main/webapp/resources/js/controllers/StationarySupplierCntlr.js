'use strict';

var app = angular.module('application');

app.controller('StationarySupplierCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.stateCodeDBFlag = true;
	$scope.stsupplier = {};
	$scope.viewStSupplierFlag = true;
	$scope.state = {};
	
	$scope.submitSupplier = function(StationarySupplierForm,stsupplier){
		console.log("Entered into submitSupplier function");
		
		if(StationarySupplierForm.$invalid){
			if(StationarySupplierForm.stSupName.$invalid){
				$scope.alertToast("Please enter supplier name between 3-40 characters.... ");
			}else if(StationarySupplierForm.stSupAdd.$invalid){
				$scope.alertToast("Please enter supplier address between 3-255 characters... ");
			}else if(StationarySupplierForm.stSupCity.$invalid){
				$scope.alertToast("Please enter supplier city between 3-40 characters... ");
			}else if(StationarySupplierForm.stSupStateName.$invalid){
				$scope.alertToast("Please enter supplier state... ");
			}else if(StationarySupplierForm.stSupPin.$invalid){
				$scope.alertToast("Please enter supplier Pin of 6 digits...");
			}else if(StationarySupplierForm.stSupPanNo.$invalid){
				$scope.alertToast("Please enter supplier PAN No. of 10 characters...");
			}else if(StationarySupplierForm.stSupTdsCode.$invalid){
				$scope.alertToast("Please enter supplier Tds Code between 3-40 characters... ");
			}else if(StationarySupplierForm.stSupGroup.$invalid){
				$scope.alertToast("Please enter supplier group between 3-40 characters... ");
			}else if(StationarySupplierForm.stSupSubGroup.$invalid){
				$scope.alertToast("Please entersupplier sub-group between 3-40 characters... ");
			}
		}else{
			
			$scope.viewStSupplierFlag = false;
			$('div#viewStSupplierDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "View Stationary Supplier Details",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
			});
			$('div#viewStSupplierDB').dialog('open');
		}
	}
	
	$scope.saveStSupplier = function(stsupplier){
		console.log("Entered into saveStSupplier function--->");
		
		var response = $http.post($scope.projectName+'/submitSupplier',stsupplier);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewStSupplierDB').dialog('close');
				$scope.successToast(data.result);
				$scope.stsupplier.stSupName="";
				$scope.stsupplier.stSupAdd="";
				$scope.stsupplier.stSupCity="";
				$scope.stsupplier.stSupState="";
				$scope.stsupplier.stSupPin="";
				$scope.stsupplier.stSupPanNo="";
				$scope.stsupplier.stSupTdsCode="";
				$scope.stsupplier.stSupGroup="";
				$scope.stsupplier.stSupSubGroup="";
				$scope.stSupStateName = "";
				$('input[name=stateName]').attr('checked',false);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.closeStSupplierDB = function(){
		$('div#viewStSupplierDB').dialog('close');
	}
	
	$scope.openStateCodeDB = function(){
		$scope.stateCodeDBFlag = false;
		$('div#stateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State Code",
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
		
		$('div#stateCodeDB').dialog('open');
	}
	
	$scope.getStateForSupplier = function(){
		console.log("Entered into getStateForSupplier function------>");
		var response = $http.post($scope.projectName+'/getStateForSupplier');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stateList = data.list;
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.saveStateCode =  function(state){
		console.log("enter into saveStateCode----->"+state.stateCode);
		$scope.stsupplier.stSupState = state.stateCode;
		$scope.stSupStateName = state.stateName;
		$('div#stateCodeDB').dialog('close');
		$scope.stateCodeDBFlag = true;
	 }
	
	 $('#supName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	 });
	 
	 $('#supAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	 });
	 
	 $('#supCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	 });
	 
	 $('#supPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	 });

     $('#supPin').keypress(function(e) {
		   if (this.value.length == 6) {
	           e.preventDefault();
	       }
	 });
	   
	 $('#supPanNo').keypress(function(e) {
		   if (this.value.length == 10) {
	           e.preventDefault();
	       }
	 });
	 
	 $('#supTdsCode').keypress(function(e) {
		   if (this.value.length == 40) {
	           e.preventDefault();
	       }
	 });
	 
	 $('#supGroup').keypress(function(e) {
		   if (this.value.length == 40) {
	           e.preventDefault();
	       }
	 });
	 
	 $('#supSubGroup').keypress(function(e) {
		   if (this.value.length == 40) {
	           e.preventDefault();
	       }
	 });
	 	 	  	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getStateForSupplier();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
}]);