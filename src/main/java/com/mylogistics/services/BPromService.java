package com.mylogistics.services;

import org.springframework.stereotype.Service;

import com.mylogistics.model.Customer;
import com.mylogistics.model.FaBProm;

@Service
public class BPromService {

	private Customer customer;
	
	private FaBProm faBProm;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public FaBProm getFaBProm() {
		return faBProm;
	}

	public void setFaBProm(FaBProm faBProm) {
		this.faBProm = faBProm;
	}
	
}
