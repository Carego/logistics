package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.Acknowledge_ValidationDao;
import com.mylogistics.constants.CnmtImageCNTS;
import com.mylogistics.model.Acknowledge_Validation;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.CnmtImage;




public class Acknowledge_ValidationDaoImpl implements Acknowledge_ValidationDao {
	
	SessionFactory sessionFactory;
	
	@Autowired
	public Acknowledge_ValidationDaoImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	@Transactional
	@Override
	public int saveAckValidation(Acknowledge_Validation acknowledge_Validation) {
		// TODO Auto-generated method stub
		int i=0;
	Session session=this.sessionFactory.openSession();
	if(session!=null)
	{
	Transaction transaction=  session.beginTransaction();
	i=(Integer)session.save(acknowledge_Validation);
		transaction.commit();
		session.close();
	}
		return i;
	}
	
     @Transactional
	@Override
	public List<CnmtImage> getCnmtImage(int cmId) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.openSession();
		Criteria criteria=session.createCriteria(CnmtImage.class);
		criteria.add(Restrictions.eq(CnmtImageCNTS.CM_ID, cmId));

	@SuppressWarnings("unchecked")
	List<CnmtImage> cnmtImageList=criteria.list();
	session.close();
		return cnmtImageList;
	}
     
     @SuppressWarnings("unchecked")
	 @Transactional
     @Override
    public List<Acknowledge_Validation> getAckValidatorFromTODate(Date fromDate, Date toDate,String remarkType) {
    	// TODO Auto-generated method stub
    Session session=	this.sessionFactory.openSession();
    List<Acknowledge_Validation> acknowledge_Validations=null;
    if(session!=null)
    {
    	
     if(remarkType.equalsIgnoreCase("ALL"))
    {
    	 Criteria criteria=session.createCriteria(Acknowledge_Validation.class);
    	 criteria.add(Restrictions.ge("scandate", fromDate));
    	 criteria.add(Restrictions.le("scandate", toDate));
    	 criteria.add(Restrictions.or(Restrictions.isNull("ischeck"),Restrictions.eq("ischeck", "N")));
    acknowledge_Validations=	 criteria.list();
    }
    else if(remarkType.equalsIgnoreCase("QUALIFIED"))
    {
    	Criteria criteria=session.createCriteria(Acknowledge_Validation.class);
   	 criteria.add(Restrictions.ge("scandate", fromDate));
   	 criteria.add(Restrictions.le("scandate", toDate));
     criteria.add(Restrictions.eq("remarks", remarkType));
   acknowledge_Validations=	 criteria.list();	
    }
    else
    {
    	Criteria criteria=session.createCriteria(Acknowledge_Validation.class);
   	 criteria.add(Restrictions.ge("scandate", fromDate));
   	 criteria.add(Restrictions.le("scandate", toDate));
     criteria.add(Restrictions.isNull("remarks"));
   acknowledge_Validations=	 criteria.list();	
    }

   
    	System.out.println("Size of List is "+acknowledge_Validations.size());
   session.close();
    }
    	return acknowledge_Validations;
    }
    
    @SuppressWarnings("unchecked")
	@Transactional
    @Override
    public List<Acknowledge_Validation> getAckByCnmtCode(String cnmtCode) {
    	// TODO Auto-generated method stub
    	List<Acknowledge_Validation> acknowledge_Validations=null;
    Session session=	this.sessionFactory.openSession();
    if(session!=null)
    {
    	Criteria criteria=session.createCriteria(Acknowledge_Validation.class);
    	criteria.add(Restrictions.eq("cnmtCode", cnmtCode));
    acknowledge_Validations=	criteria.list();
    session.close();
    }
    	
    	return acknowledge_Validations;
    }
    
    @Transactional
    @Override
    public void updateAckValidation(int ackId, Acknowledge_Validation acknowledge_Validation,ArrivalReport arrivalReport) {
    	// TODO Auto-generated method stub
    Session session=this.sessionFactory.openSession();
   
    if(session!=null)
    {
    	 Transaction transaction=session.beginTransaction();
   //  acknowledge_Validation=(Acknowledge_Validation)session.load(Acknowledge_Validation.class, ackId);
    	 
     session.update(acknowledge_Validation);
     session.update(arrivalReport);
    	transaction.commit();
    	System.out.println("Data is updated of "+ackId);
    	session.close();
    }
    	
    	
    }

}
