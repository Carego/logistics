<div ng-show="operatorLogin || superAdminLogin">
	<title>Challan</title>


	<!-- <form>
			
			<input type="date" ng-model="actDate" />
			<input type="button" value="save date" ng-click="saveActDt()"/>
	</form> -->

	<div class="row" ng-show="multiCnmtCodeList.length > 0">
		<table class="tblrow">
       		<caption><a href="" id="ankr" class="btn btn-danger1" >Enter CNMT's weight and No of Pkg</a></caption> 
            <tr class="rowclr">
	            <th class="colclr">CNMT Code</th>
	            <th class="colclr">No. Of Pkg</th>
	            <th class="colclr">Weight(Ton)</th>
				<th class="colclr">Action</th>
	        </tr>	
	        <tr ng-repeat="multiCnmt in multiCnmtCodeList">
				<td class="rowcel">{{ multiCnmt.cnmt }}</td>
				<td class="rowcel">{{ multiCnmt.pkg }}</td>
				<td class="rowcel">{{ multiCnmt.wt }}</td>
				<td class="rowcel"><button class="btn" ng-click="removeMultiCnmt(multiCnmt)">Remove</button></td>
			</tr>
    	</table>
          
	</div>

	<!-- <div class="row" ng-show="multiCnmtCodeList.length > 0">
		<div class="col s3 hide-on-med-and-down">&nbsp;</div>
		<form class="col s6 card"
			style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<table class="table table-condensed center">
				<thead>
					<tr>
						<th class="center">CNMT Code</th>
						<th class="center">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="multiCnmt in multiCnmtCodeList">
						<td>{{ multiCnmt }}</td>
						<td><button class="btn" ng-click="removeMultiCnmt(multiCnmt)">Remove</button></td>
					</tr>
				</tbody>
			</table>
			<div class="col s12 center">
				<button class="btn" ng-show="multiCnmtCodeList.length > 1"
					ng-click="removeAllMultiCnmt()">Remove All CNMT</button>
			</div>

		</form>

		<div class="col s3">&nbsp;</div>
	</div> -->
	<div class="row">
		<form name=ChallanForm class="col s12 card"
			style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
			name="ChallanForm"
			ng-submit="submitChallan(chln,cnmtCode,ChallanForm,chd,chlnTotalWtPer)">

			<div class="row">

				<div class="input-field col s5">
					<input type="text" name="cnmtCodeName" id="cnmtCodeId"
						ng-model="cnmtCode" ng-model="multiCnmtCode" ng-click="OpenCnmtDB()" readonly="readonly" disabled="disabled">
				</div>
				<div class="input-field col s5">
					<div style="float: left;">
					<label> Single CNMT</label>
					<input type="radio" name="btn" ng-click="singleCnmt()" />
					</div>
					<div style="margin-left: 25%; float: left;">
						<label style="margin-left: 35%;"> Multiple CNMT</label>
						<input type="radio" name="btn" ng-click="multipleCnmt()" />
					</div>				
				</div>				
				<!--  <div id ="cnmtCode">
       	<div class="input-field col s3">
        	<input class="validate" type ="text" name ="chlnCnmtCode" id="chlnCnmtCode" ng-model="chln.chlnCnmtCode" ng-click="OpenCnmtDB()" readonly>
        	<label for="chlnCnmtCode">Cnmt Code</label>
      	</div>
      </div> -->
			</div>
			<div class="row">
				<div class="input-field col s4">
					<input type="text" id="chlnCode" name="chlnCode" ng-model="chln.chlnCode" required ng-change="openChallanCodeDB()">
					<label>Challan Code</label>
				</div>
				<div class="input-field col s4">
					<!-- <input class="validate" type ="text" name ="branchCodeTemp" ng-model="branchCodeTemp" readonly ng-click="OpenBranchCodeDB()" required> -->
					<input class="validate" type="text" name="branchCodeTemp" ng-model="branchCodeTemp" readonly required> 
					<input class="validate" type="hidden" name="branchCode" ng-model="chln.branchCode">
					<!-- <input type="text" name="branchCode" id="branchCode"
					ng-model="chln.branchCode" ng-click="OpenBranchCodeDB()" required
					readonly> -->
					<label>Branch Code</label>
				</div>
				<!-- <div class="col s4">
					<div class="input-field col s6">
						<select name="chlnLryNo1" ng-model="chlnLryNoPer"
							ng-options="state as state for state in stateList" required></select>
						<label>Lorry No</label>
					</div>
					<div class="input-field col s6">
						<input type="text" name="chlnLryNo" id="chlnLryNo"
							ng-model="chln.chlnLryNo" ng-minlength="1" ng-maxlength="8"
							required>
					</div>
					
				</div> -->
				
				<div class="input-field col s4">
						<input class="validate" type="button" name="acd" id="acd"
							value="Add Challan Detail" ng-model="chln.chlnChdCode"
							ng-click="OpenChdCodeCbDB()" disabled="disabled"> <label>Challan
							Details</label>
					</div>
			</div>

			<div class="row">
				<div class="input-field col s4">
					<input class="validate" id="fromStationId" type="text" name="fromStationName" ng-model="frmStationCode" ng-change="OpenChallanFromStationDB()" required> 
					<input class="validate" type="hidden" name="chlnFromStn" ng-model="chln.chlnFromStn">
					<!-- <input type="text" name="chlnFromStn" id="chlnFromStn"
					ng-model="chln.chlnFromStn" ng-click="OpenChallanFromStationDB()"
					required readonly>  -->
					<label>From Station</label>
				</div>
				<div class="input-field col s4">
					<input class="validate" type="text" name="toStationName" ng-model="toStationCode" ng-change="OpenChallanToStationDB()" required> 
					<input class="validate" type="hidden" name="chlnToStn" ng-model="chln.chlnToStn">
					<!-- <input type="text" name="chlnToStn" id="chlnToStn"
					ng-model="chln.chlnToStn" ng-click="OpenChallanToStationDB()"
					required readonly> -->
					<label>To Station</label>
				</div>
				<div class="input-field col s4">
					<input class="validate" type="text" name="empTempCode" ng-model="empTempCode" ng-change="OpenEmployeeCodeDB()" required>
					<input class="validate" type="hidden" name="chlnEmpCode" ng-model="chln.chlnEmpCode">
					<!-- <input type="text" name="chlnEmpCode" id="chlnEmpCode"
					ng-model="chln.chlnEmpCode" ng-click="OpenEmployeeCodeDB()"
					required readonly> -->
					<label>Employee Code</label>
				</div>
			</div>

			<div class="row">
				<!-- <div class="col s6">
					<div class="input-field col s6">
						<input class="validate" type="button" name="acd" id="acd"
							value="Add CD" ng-model="chln.chlnChdCode"
							ng-click="OpenChdCodeCbDB()" disabled="disabled"> <label>Challan
							Details</label>
					</div>
				</div>	 -->
				<div class="input-field col s4">
						<input class="validate" type="number" name="chlnNoOfPkg"
							id="chlnNoOfPkg" ng-model="chln.chlnNoOfPkg" ng-minlength="1"
							ng-maxlength="7" ng-required="true"> <label>No.
							Of Package</label>
					</div>
				
				<div class="col s4">
					<div class="input-field col s6">
						<input type="number" name="chlnLryRate" id="chlnLryRate"
							ng-model="chln.chlnLryRate" step="0.00001" min="0.00000"
							ng-minlength="1" ng-maxlength="7" ng-required="true"> <label>Lorry
							Rate</label>
					</div>
					<div class="input-field col s6">
						<select name="chlnLryRate1" id="chlnLryRate1"
							ng-model="chlnLryRatePer" required>
							<option value="Ton">Ton</option>
							<option value="Kg">Kg</option>
						</select>
					</div>
				</div>	
				
				<div class="col s4">
					<div class="input-field col s6">
						<input type="number" name="chlnChgWt" id="chlnChgWt"
							ng-model="chln.chlnChgWt" step="0.00001" min="0.00000"
							ng-minlength="1" ng-maxlength="7" ng-required="true"> <label>Charge
							Weight</label>
					</div>
					<div class="input-field col s6">
						<select name="chlnChgWt1" id="chlnChgWt1" ng-model="chlnChgWtPer1" ng-init="chlnChgWtPer = 'Ton'" readonly required>
							<option value="Ton">Ton</option>
							<!-- <option value="Kg">Kg</option> -->
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="number" name="chlnFreight" id="chlnFreight"
						ng-model="chln.chlnFreight" step="0.00001" min="0.00000"
						ng-blur="calTotalFreight(chln)" ng-keyup="calTotalFreight(chln)"
						ng-click="calculateFreight()" ng-required="true"> <label>Freight</label>
				</div>
				<div class="input-field col s4">
					<input type="number" name="chlnLoadingAmt" id="chlnLoadingAmt"
						ng-model="chln.chlnLoadingAmt" step="0.00001" min="0.00000"
						ng-blur="calTotalFreight(chln)" ng-keyup="calTotalFreight(chln)"
						ng-minlength="1" ng-maxlength="9" ng-required="true"> <label>Loading
						Amount</label>
				</div>
				<!-- 
				<div class="row">
					<div class="input-field col s4">
						<input class="validate" type="number" name="chlnExtra"
							id="chlnExtra" ng-model="chln.chlnExtra" step="0.00001"
							min="0.00000" ng-blur="calTotalFreight(chln)" ng-click="openExtraLoadingDB()" 
							ng-minlength="1"
							ng-maxlength="7" ng-required="true" readonly="readonly"> <label>Extra</label>
					</div>
				</div>
				-->
				<div class="row">
					<div class="input-field col s4">
						<input class="validate" type="number" name="chlnExtra"
							id="chlnExtra" ng-model="chln.chlnExtra" step="0.00001"
							min="0.00000" ng-blur="calTotalFreight(chln)"
							ng-keyup="calTotalFreight(chln)" ng-minlength="1"
							ng-maxlength="7" ng-required="true" readonly ng-click="openExtraLoadingDB()"> <label>Extra</label>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="number" name="chlnStatisticalChg"
						id="chlnStatisticalChg" ng-model="chln.chlnStatisticalChg"
						step="0.00001" min="0.00000" ng-blur="calTotalFreight(chln)"
						ng-keyup="calTotalFreight(chln)" ng-minlength="1" ng-maxlength="7"
						ng-required="true"> <label>Statistical Charge</label>
				</div>
				<div class="input-field col s4">
					<input type="number" name="chlnTdsAmt" id="chlnTdsAmt"
						ng-model="chln.chlnTdsAmt" step="0.00001" min="0.00000"
						ng-blur="calTotalFreight(chln)" ng-keyup="calTotalFreight(chln)"
						ng-minlength="1" ng-maxlength="7" ng-required="true"> <label>TDS
						Amount</label>
				</div>
				<div class="input-field col s4">
					<input type="number" name="chlnTotalFreight" id="chlnTotalFreight"
						ng-model="chln.chlnTotalFreight" step="0.00001" min="0.00000"
						readonly> <label>Total Freight</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="number" name="chlnAdvance" id="chlnAdvance"
						ng-model="chln.chlnAdvance" step="0.00001" min="0.00000"
						ng-blur="checkAdvance()" disabled="disabled" ng-minlength="1"
						ng-maxlength="7" ng-required="true"> <label>Advance</label>
				</div>
				<div class="input-field col s4">
					<input type="number" name="chlnBalance" id="chlnBalance"
						ng-model="chln.chlnBalance" step="0.00001" min="0.00000"
						ng-click="balance(chln)" readonly> <label>Balance</label>
				</div>
				<div class="input-field col s4">
					<input class="validate" type="text" name="payOnCode" ng-model="payOnCode" ng-change="OpenPayAtDB()" required>
					<input class="validate" type="hidden" name="chlnPayAt" ng-model="chln.chlnPayAt">
					<!-- <input type="text" name="chlnPayAt" id="chlnPayAt"
					ng-model="chln.chlnPayAt" ng-click="OpenPayAtDB()" required
					readonly> -->
					<label>Pay At</label>
				</div>

			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="number" name="chlnTimeAllow" id="chlnTimeAllow"
						ng-model="chln.chlnTimeAllow" ng-minlength="1" ng-maxlength="2">
					<label>Time Allow</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chlnDt" id="chlnDt" ng-model="chln.chlnDt"
						ng-blur="checkdate()" required> <label>Challan
						Date</label>
				</div>
				<div class="input-field col s4">
					<input type="number" name="chlnBrRate" id="chlnBrRate"
						ng-model="chln.chlnBrRate" ng-model="chln.chlnFreight"
						step="0.00001" min="0.00000" ng-minlength="1" ng-maxlength="7"
						ng-required="true"> <label>BR Rate</label>
				</div>
			</div>

			<div class="row">
				<div class="col s4">
					<div class="input-field col s6">
						<input type="number" name="chlnTotalWt" id="chlnTotalWt"
							ng-model="chln.chlnTotalWt" step="0.00001" min="0.00000"
							ng-minlength="1" ng-maxlength="7" ng-required="true"> <label>Total
							Weight</label>
					</div>
					<div class="input-field col s6">
						<select name="chlnTotalWt1" id="chlnTotalWt1" ng-model="chlnTotalWtPer" ng-init="chlnTotalWtPer='Ton'" readonly required>
							<option value="Ton">Ton</option>
							<!-- <option value="Kg">Kg</option> -->
						</select>
					</div>
				</div>
				<div class="input-field col s4">
					<select name="chlnWtSlip" id="chlnWtSlip"
						ng-model="chln.chlnWtSlip" required>
						<option value="Yes">Yes</option>
						<option value="No">No</option>
					</select> <label>Weight Slip</label>
				</div>
				<div class="col s4">
					<div class="input-field col s6">
						<input type="date" name="chlnLryRepDT" required="required"
							ng-model="chln.chlnLryRepDT" ng-blur="isInFY()"> <label>Lorry
							Reporing Date</label>
					</div>
					<div class="input-field col s6">
						<input type="text" name="chlnVehicleType" id="chlnVehicleType" ng-model="chln.chlnVehicleType" ng-click="OpenVehicleTypeDB()" ng-blur="chlnRRNo(chln)" readonly="readonly" required>
						<label>Vehicle Type</label>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="text" name="chlnRrNo" ng-model="chln.chlnRrNo"	>
					 <label>Railway Reciept No</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chlnTrainNo" ng-model="chln.chlnTrainNo">
					 <label>Train No</label>
				</div>
				<div class="col s4">
					<div class="input-field col s6">
						<input class="col s12 grey-text text-darken-4" type="time"
							name="chlnLryLoadTime" ng-model="chln.chlnLryLoadTime" required>
						<label>Lorry Load Time</label>
					</div>
					<div class="input-field col s6">
						<input class="col s12 grey-text text-darken-4" type="time"
							name="chlnLryRptTime" ng-model="chln.chlnLryRptTime" required="required"> <label>Lorry
							Reporting Time</label>
					</div>
				</div>
			</div>

			<!-- <div class="row">
			<div class="input-field col s4">
				<input class="btn teal white-text" type="file"
					file-model="challanImage" /> <label>Choose File</label>
			</div>

			<div class="input-field col s4">
				<input class="btn col s12" type="button"
					ng-click="uploadChallanImage()" value="upload Image" />
			</div>
		</div>
 -->
			<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
			</div>

		</form>
	</div>


	<!-- <form name="ChallanForm" ng-submit="submitChallan(chln,cnmtCode,ChallanForm)">
			<div id ="cnmtCode">
				
			<table>
			<tr>
				<td><input type="radio" name="btn" ng-click="singleCnmt()" />Single CNMT:</td>
				<td><input type ="text" name ="cnmtCodeName"  id="cnmtCodeId" ng-model="cnmtCode" ng-click="OpenCnmtDB()" disabled="disabled" readonly></td>
				<td><input type="radio" name="btn" ng-click="multipleCnmt()" />Multiple CNMT:</td>
				<td><input type ="text" name ="multiCnmtCodeName" id="multiCnmtCodeId" ng-model="multiCnmtCode" ng-click="OpenMultiCnmtDB()" disabled="disabled" readonly></td>
			</tr>
			</table>
			</div>
			
			
			<div ng-hide = "show" >	
			<table border="0">
			<tr>
				<td>Challan Code: *</td>
				<td><input type ="text" id="chlnCode" name ="chlnCode" ng-model="chln.chlnCode" ng-required="true" ng-minlength="7" ng-maxlength="7"></td>
			</tr>	
		
			<tr>
				<td> Challan Branch Code: *</td>
				<td><input type ="text" name ="branchCode" id="branchCode" ng-model="chln.branchCode" ng-click="OpenBranchCodeDB()" required readonly></td>
			</tr>
				
			<tr>
				<td>Lorry No: *</td>
				<td><select style="width:150px;" name="chlnLryNo1" ng-model="chlnLryNoPer" ng-options="state as state for state in stateList" required></select>
				<td><input type ="text" name ="chlnLryNo" id="chlnLryNo" ng-model="chln.chlnLryNo" required></td>
			</tr>
 						
			<tr>
				<td> Challan From Station: *</td>
				<td><input type ="text" name ="chlnFromStn" id="chlnFromStn" ng-model="chln.chlnFromStn" ng-click="OpenChallanFromStationDB()" required readonly></td>
			</tr>
				
			<tr>
				<td> Challan To Station: *</td>
				<td><input type ="text" name ="chlnToStn" id="chlnToStn" ng-model="chln.chlnToStn" ng-click="OpenChallanToStationDB()" required readonly></td>
			</tr>
		
			<tr>
				<td>Employee Code: *</td>
				<td><input type ="text" name ="chlnEmpCode" id="chlnEmpCode" ng-model="chln.chlnEmpCode" ng-click="OpenEmployeeCodeDB()" required readonly></td>
			</tr>

			<tr>
				<td>Lorry Rate: *</td>
				<td><input type ="number" name ="chlnLryRate" ng-model="chln.chlnLryRate" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true"></td>
			</tr>
				
			<tr>
				<td>Charge Weight: *</td>
				<td><input type ="text" name ="chlnChgWt" id="chlnChgWt" ng-model="chln.chlnChgWt" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true">
				<select name="chlnChgWt1" id="chlnChgWt1" ng-model="chlnChgWtPer" required>
				<option value="Ton">Ton</option>
				<option value="Kg">Kg</option>
				</select></td>
			</tr>
			
			<tr>
				<td>No. Of Package: *</td>
				<td><input type ="number" name ="chlnNoOfPkg" ng-model="chln.chlnNoOfPkg" ng-minlength="7" ng-maxlength="7"></td>
			</tr>
				
			<tr>
				<td> Challan Total Weight: *</td>
				<td><input type ="text" name ="chlnTotalWt" id="chlnTotalWt" ng-model="chln.chlnTotalWt" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true">
				<select name="chlnTotalWt1" id="chlnTotalWt1" ng-model="chlnTotalWtPer" required>
				<option value="Ton">Ton</option>
				<option value="Kg">Kg</option>
				</select>
				</td>
			</tr>
				
			<tr>
				<td>Challan Freight: *</td>
				<td>Rs.<input type ="number" name ="chlnFreight" id="chlnFreight" ng-model="chln.chlnFreight" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true"></td>
			</tr>
				
			<tr>
				<td>Loading Amount: *</td>
				<td>Rs.<input type ="number" name ="chlnLoadingAmt" id="chlnLoadingAmt" ng-model="chln.chlnLoadingAmt" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true"></td>
			</tr>
				
			<tr>
				<td> Challan Extra: *</td>
				<td>Rs.<input type ="number" name ="chlnExtra" id="chlnExtra" ng-model="chln.chlnExtra" ng-blur="calTotalFreight(chln)" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true"></td>
			</tr>
				
			<tr>
				<td>Total Freight:</td>
				<td>Rs.<input type ="text" name ="chlnTotalFreight" id="chlnTotalFreight" ng-model="chln.chlnTotalFreight" readonly></td>
			</tr>
				
			<tr>
				<td>Challan Advance: *</td>
				<td><input type ="number" name ="chlnAdvance" id="chlnAdvance" ng-model="chln.chlnAdvance" ng-blur="checkAdvance()" ng-required="true"></td>
			</tr>
				
			<tr>
				<td>Challan Balance:</td>
				<td><input type ="text" name ="chlnBalance" id="chlnBalance" ng-model="chln.chlnBalance" ng-click="balance(chln)" readonly></td>
			</tr>
				
			<tr>
				<td>Challan Pay At: *</td>
				<td><input type ="text" name ="chlnPayAt" id="chlnPayAt" ng-model="chln.chlnPayAt" ng-click="OpenPayAtDB()" required readonly></td>
			</tr>
		
			<tr>
				<td>Challan Sedr: *</td>
				<td><input type ="text" name ="chlnSedr" id="chlnSedr" ng-model="chln.chlnSedr" ng-click="OpenSedrDB()" required readonly></td>
			</tr>
							
			<tr>
				<td>Time Allow: *</td>
				<td><input type ="number" name ="chlnTimeAllow" id="chlnTimeAllow" ng-model="chln.chlnTimeAllow" ng-minlength="1" ng-maxlength="2"></td>
			</tr>
				
			<tr>
				<td>Challan Date: *</td>
				<td><input type ="date" name ="chlnDt" ng-model="chln.chlnDt"></td>
			</tr>
					
			<tr>
				<td>BR Rate: *</td>
				<td><input type ="number" name ="chlnBrRate" ng-model="chln.chlnBrRate"  ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true"></td>
			</tr>
			
			<tr>
					<td>Weight Slip: *</td>
					<td>
						<select name ="chlnWtSlip" id="chlnWtSlip" ng-model="chln.chlnWtSlip" required>
							<option value = "Yes">Yes</option>
							<option value = "No">No</option>
						</select>
					</td>
				</tr>
			
			<tr>
				<td>Vehicle type: *</td>
				<td><input type ="text" name ="chlnVehicleType" id="chlnVehicleType" ng-model="chln.chlnVehicleType" ng-mousedown="OpenVehicleTypeDB()" ng-blur="chlnRRNo(chln)"  required readonly></td>
			</tr>
			
			<tr>
				<td>Statistical Charge: *</td>
				<td><input type ="number" name ="chlnStatisticalChg"  ng-model="chln.chlnStatisticalChg" ng-blur="calStatChnge(chln)" min="1"></td>
			</tr>
			
			<tr>
				<td>Challan Chd Code: *</td>
				<td><input type ="text" name ="chlnChdCode" id="chlnChdCode" ng-model="chln.chlnChdCode" ng-click="OpenChdCodeCbDB()" required></td>
			</tr>
			
			<tr>
				<td>Tds Amount: *</td>
				<td><input type ="number" name ="chlnTdsAmt" id="chlnTdsAmt" ng-model="chln.chlnTdsAmt" ng-blur="calTdsAmt(chln)" ng-pattern="/(^[1-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.01"  ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Challan RR No:</td>
				<td><input type ="text" name ="chlnRrNo"  ng-model="chln.chlnRrNo" disabled="disabled"></td>
			</tr>
			
			<tr>
				<td>Challan Train No:</td>
				<td><input type ="text" name ="chlnTrainNo" ng-model="chln.chlnTrainNo" disabled="disabled"></td>
			</tr>
			
			<tr>
				<td>Lorry Load Time: *</td>
				<td><input type ="time" name ="chlnLryLoadTime" ng-model="chln.chlnLryLoadTime" required></td>
			<td></td>
			</tr>
			
			<tr>
				<td>Lorry Reporting Date: *</td>
				<td><input type ="date" name ="chlnLryRepDT" ng-model="chln.chlnLryRepDT"></td>
			</tr>
			
			<tr>
				<td>Lorry Reporting Time: *</td>
				<td><input type ="time"  name ="chlnLryRptTime" ng-model="chln.chlnLryRptTime"></td>
			</tr>
			
			<tr>
				<td>
					 <input type="file" file-model="challanImage"/>
				</td>
				<td>	  
	   				 <input type="button" ng-click="uploadChallanImage()" value="upload Image" />
   				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr> 
			
		</table>
	 </form> -->

	<!-- <form method="post" action="uploadChallanFile.html" enctype="multipart/form-data">
		<tr>
			<td>Upload Image : </td>
			<td><input type="file" name="file" size="50" /></td>
		</tr>
		<tr>
			<td><input type="submit" value="Upload" /></td>
		</tr>
	</form> -->



	<div class="row" id="chdCodeCb" ng-hide="ChdCodeCbFlag">
		<form name=ChallanDetailForm
			ng-submit="submitCD(ChallanDetailForm,chd)" class="col s12 card"
			style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

			<div class="row">

				<!-- <div class="input-field col s4">
				<input type ="text" id="chdChlnCode" name ="chdChlnCode" ng-model="chd.chdChlnCode"readonly>
				<label>Chd Challan Code</label>
				</div> -->
				
				<div class="input-field col s4">
					<input type ="text" id="vehId" name ="vehName" ng-model="chln.chlnLryNo" ng-change="openVehicleDB()" required>
					<label>Select Vehicle</label>
				</div> 
				
				<div class="input-field col s4">
					<input type="text" name="chdBrCode" id="chdBrCode"
						ng-model="chdBrCodeTemp" ng-change="OpenBrokerCodeDB()" required> <label>Broker Code</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdOwnCode" ng-model="chdOwnCodeTemp"
						ng-change="OpenOwnerCodeDB()" required> <label>Owner
						Code</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="text" name="chdBrMobNo" id="chdBrMobNo" ng-model="chd.chdBrMobNo" ng-click="OpenBrMobNoDB()" ng-maxlength="10" ng-minlength="10">
					<label>Broker Mobile No</label> <a
						style="width: 40px; position: absolute; right: -10px; padding: 0 1rem"
						class="btn waves-effect teal" id="openMobbrk"
						ng-model="savPerStateCode" ng-click="openBrkMobileNo()"
						disabled="disabled"><i class="mdi-content-add white-text"
						style="font-size: 24px; margin-left: -6px;"></i></a>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdOwnMobNo" id="chdOwnMobNo" ng-model="chd.chdOwnMobNo" ng-click="OpenOwnMobNoDB()" ng-maxlength="10" ng-minlength="10"> 
					<label>Owner Mobile No</label> <a
						style="width: 40px; position: absolute; right: -10px; padding: 0 1rem"
						class="btn waves-effect teal" id="openMobown"
						ng-model="savPerStateCode" ng-click="openOwnMobileNo()"
						disabled="disabled"><i class="mdi-content-add white-text"
						style="font-size: 24px; margin-left: -6px;"></i></a>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdDvrName" ng-model="chd.chdDvrName" required="required">
					<label>Driver Name</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="text" name="chdDvrMobNo" ng-model="chd.chdDvrMobNo" ng-maxlength="10" ng-minlength="10">
					<label>Driver Mobile No</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdRcNo" ng-model="chd.chdRcNo">
					<label>RC No</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chdRcIssueDt" ng-model="chd.chdRcIssueDt">
					<label>RC Issue Date</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="date" name="chdRcValidDt" ng-model="chd.chdRcValidDt">
					<label>RC Valid Date</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdDlNo" ng-model="chd.chdDlNo" ng-minlength="3" ng-maxlength="50">
					<label>DL No</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chdDlIssueDt" ng-model="chd.chdDlIssueDt">
					<label>DL Issue Date</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="date" name="chdDlValidDt" ng-model="chd.chdDlValidDt">
					<label>DL Valid Date</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdPerNo" ng-model="chd.chdPerNo"> <label>Per NO</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chdPerIssueDt" ng-model="chd.chdPerIssueDt"> 
					<label>Per Issue Date</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="date" name="chdPerValidDt" ng-model="chd.chdPerValidDt">
					 <label>Per Valid Date</label>
				</div>
				<div class="input-field col s4">
					<input
						style="width: calc(100% -     65px); position: absolute; left: 10px"
						type="text" name="chdPerState" id="chdPerState"
						ng-model="chdPerStateTemp" ng-click="OpenchdPerStateCodeDB()"
						readonly> <label>Per State</label> <a
						style="width: 40px; position: absolute; right: -10px; padding: 0 1rem"
						class="btn waves-effect teal" id="openPerStateCodeDB"
						ng-model="savPerStateCode" ng-click="OpenperStateCodeDB1()"
						disabled="disabled"><i class="mdi-content-add white-text"
						style="font-size: 24px; margin-left: -6px;"></i></a>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdFitDocNo" ng-model="chd.chdFitDocNo">
					 <label>Fit Doc	No</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="date" name="chdFitDocIssueDt"
						ng-model="chd.chdFitDocIssueDt"> <label>Fit Doc
						Issue Date</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chdFitDocValidDt"
						ng-model="chd.chdFitDocValidDt"> <label>Fit Doc
						Valid Date</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdFitDocPlace"
						ng-model="chd.chdFitDocPlace"> <label>Fit Doc
						Place</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="text" name="chdBankFinance"
						ng-model="chd.chdBankFinance"> <label>Bank Finance</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdPolicyNo" ng-model="chd.chdPolicyNo">
					<label>Policy No</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdPolicyCom" ng-model="chd.chdPolicyCom">
					<label>Policy Com</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="text" name="chdTransitPassNo"
						ng-model="chd.chdTransitPassNo"> <label>Transit
						Pass No</label>
				</div>
				<div class="input-field col s4">
					<select name="chdPanHdrType" id="chdPanHdrType"
						ng-model="chd.chdPanHdrType"
						ng-init=" chd.chdPanHdrType = 'Owner' " ng-required="true">
						<option value="Broker">Broker</option>
						<option value="Owner">Owner</option>
					</select> <label>Pass HDR Type</label>
				</div>
				<!-- <div class="input-field col s4">
					<input type="text" name="chdPanNo" id="chdPanNo"
						ng-model="chd.chdPanNo" ng-minlength="10" ng-maxlength="10"
						ng-pattern="/[a-zA-Z0-9]/"> <label>Pan No</label>
				</div> -->
			</div>
			
			<div class="row">

				<div class="input-field col s4">
					<input type="text" name="chdEngineNo"
						ng-model="chd.chdEngineNo"> <label>Engine No.</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdChassisNo" ng-model="chd.chdChassisNo"> <label>Chassis No</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="chdModel" ng-model="chd.chdModel">
					<label>Model Year</label>
				</div>
			</div>

			<div class="row">

				<!-- <div class="input-field col s4">
					<input type="text" name="chdPanHdrName"
						ng-model="chd.chdPanHdrName"> <label>Pan Hdr
						Name</label>
				</div> -->
				<div class="input-field col s4">
					<input
						style="width: calc(100% -     65px); position: absolute; left: 10px"
						type="text" name="chdPanIssueSt" id="chdPanIssueSt"
						ng-model="chd.chdPanIssueSt" ng-click="OpenchdPanIssueStDB()"
						readonly> <label>Pan Issue St</label> <a
						style="width: 40px; position: absolute; right: -10px; padding: 0 1rem"
						class="btn waves-effect teal" id="openPanIssueStDB"
						ng-model="savPanIssueSt" ng-click="OpenchdPanIssueStDB1()"
						disabled="disabled"><i class="mdi-content-add white-text"
						style="font-size: 24px; margin-left: -6px;"></i></a>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
			</div>

		</form>
	</div>






	<!-- <div id ="chdCodeCb" ng-hide="ChdCodeCbFlag">
	 
	 <form name=ChallanDetailForm ng-submit="submitCD(ChallanDetailForm,chd)"> 

		<table  border="0"  class="table table-condensed noborder">
			<tr>
				<td>Chd l Challan Code: *</td>
				<td><input type ="text" id="chdChlnCode" name ="chdChlnCode" ng-model="chd.chdChlnCode"  ng-required="true" ng-minlength="7" ng-maxlength="7"></td>
			</tr>	
		
			<tr>
				<td>Broker Code: *</td>
				<td><input type ="text" name ="chdBrCode" id="chdBrCode" ng-model="chd.chdBrCode" ng-click="OpenBrokerCodeDB()" readonly required></td>
			</tr>
				
			<tr>
				<td>Owner Code: *</td>
				<td><input type ="text" name ="chdOwnCode" ng-model="chd.chdOwnCode" ng-click="OpenOwnerCodeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td>Broker Mobile No: *</td>
				<td><input type ="text" name ="chdBrMobNo" id="chdBrMobNo" ng-model="chd.chdBrMobNo" ng-click="OpenBrMobNoDB()" readonly required>
				<input type="button" id="openBrMobNoDB" ng-model="savBrMobNo" ng-click="OpenBrMobNoDB1()"></td>	
			</tr>
				
			<tr>
				<td>Owner Mobile No: *</td>
				<td><input type ="text" name ="chdOwnMobNo" id="chdOwnMobNo" ng-model="chd.chdOwnMobNo" ng-click="OpenOwnMobNoDB()" readonly required>
				<input type="button" id="openOwnMobNoDB" ng-model="savOwnMobNo" ng-click="OpenOwnMobNoDB1()"></td>	
			</tr>
		
			<tr>
				<td>Driver Name: *</td>
				<td><input type ="text" name ="chdDvrName" ng-model="chd.chdDvrName" ng-required="true"></td>
			</tr>

			<tr>
				<td>Driver Mobile No.: *</td>
				<td><input type ="number" name ="chdDvrMobNo" ng-model="chd.chdDvrMobNo" ng-maxlength="10" ng-minlength="10" ng-required="true"></td>
			</tr>
				
			<tr>
				<td>RC No: *</td>
				<td><input type ="text" name ="chdRcNo" ng-model="chd.chdRcNo" ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>RC Issue Date: *</td>
				<td><input type ="date" name ="chdRcIssueDt" ng-model="chd.chdRcIssueDt" required></td>
			</tr>
				
			<tr>
				<td>RC Valid Date: *</td>
				<td><input type ="date" name ="chdRcValidDt" ng-model="chd.chdRcValidDt" required></td>
			</tr>
				
			<tr>
				<td>DL No: *</td>
				<td><input type ="text" name ="chdDlNo" ng-model="chd.chdDlNo"  ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
			</tr>
				
			<tr>
				<td>DL Issue Date: *</td>
				<td><input type ="date" name ="chdDlIssueDt" ng-model="chd.chdDlIssueDt" required></td>
			</tr>
				
			<tr>
				<td>DL Valid Date: *</td>
				<td><input type ="date" name ="chdDlValidDt" ng-model="chd.chdDlValidDt" required></td>
			</tr>
				
			<tr>
				<td>Per NO: *</td>
				<td><input type ="text" name ="chdPerNo" ng-model="chd.chdPerNo" ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
			</tr>
				
			<tr>
				<td>Per Issue Date: *</td>
				<td><input type ="date" name ="chdPerIssueDt" ng-model="chd.chdPerIssueDt" required></td>
			</tr>
				
			<tr>
				<td>Per Valid Date: *</td>
				<td><input type ="date" name ="chdPerValidDt" ng-model="chd.chdPerValidDt" required></td>
			</tr>
						
			<tr>
				<td>Per State Code: *</td>
				<td><input type ="text" name ="chdPerStateCode" id="chdPerStateCode" ng-model="chd.chdPerStateCode" ng-click="OpenchdPerStateCodeDB()" readonly required>
			    <input type="button" id="openPerStateCodeDB" ng-model="savPerStateCode" ng-click="OpenperStateCodeDB1()"></td>
			</tr>
		
			<tr>
				<td>Fit Doc No: *</td>
				<td><input type ="text" name ="chdFitDocNo" ng-model="chd.chdFitDocNo" ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
			</tr>
				
			<tr>
				<td>Fit Doc Issue Date: *</td>
				<td><input type ="date" name ="chdFitDocIssueDt" ng-model="chd.chdFitDocIssueDt" required></td>
			</tr>
				
			<tr>
				<td>Fit Doc Valid Date: *</td>
				<td><input type ="date" name ="chdFitDocValidDt" ng-model="chd.chdFitDocValidDt" required></td>
			</tr>
				
			<tr>
				<td>Fit Doc Place: *</td>
				<td><input type ="text" name ="chdFitDocPlace" ng-model="chd.chdFitDocPlace" ng-required="true"></td>
			</tr>
					
			<tr>
				<td>Bank Finance: *</td>
				<td><input type ="text" name ="chdBankFinance" ng-model="chd.chdBankFinance" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Policy No: *</td>
				<td><input type ="text" name ="chdPolicyNo" ng-model="chd.chdPolicyNo" ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Policy Com: *</td>
				<td><input type ="text" name ="chdPolicyCom" ng-model="chd.chdPolicyCom" ng-required="true"></td>
			</tr>
			
			
			<tr>
				<td>Transit Pass No: *</td>
				<td><input type ="text" name ="chdTransitPassNo" ng-model="chd.chdTransitPassNo" ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Pass Hdr Type: *</td>
				<td>
				    <select name ="chdPanHdrType" id="chdPanHdrType" ng-model="chd.chdPanHdrType" required>
					    <option value = "Broker">Broker</option>
					    <option value = "Owner">Owner</option>
					</select>
				</td>
				
			</tr>
			
			<tr>
				<td>Pan No: *</td>
				<td><input type ="number" name ="chdPanNo" ng-model="chd.chdPanNo" ng-minlength="10" ng-maxlength="10" ng-pattern="/[a-zA-Z0-9]/" ng-required="true"></td>
				
			</tr>
			
			<tr>
				<td>Pan Hdr Name: *</td>
				<td><input type ="text" name ="chdPanHdrName" ng-model="chd.chdPanHdrName" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Pan Issue St: *</td>
				<td><input type ="text" name ="chdPanIssueSt" id="chdPanIssueSt" ng-model="chd.chdPanIssueSt" ng-click="OpenchdPanIssueStDB()" required readonly>
				<input type="button" id="openPanIssueStDB" ng-model="savPanIssueSt" ng-click="OpenchdPanIssueStDB1()"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		
		</table>
	 </form>  
	 
 </div> -->
 
 	<div id="vehDB" ng-hide="vehDBFlag">

		<input type="text" name="filterVehCode" ng-model="filterVehCode"
			placeholder="Search....">

		<table>
			<tr>
				<th></th>

				<th>Vehilce No</th>
			</tr>
			<tr ng-repeat="vehicle in vehList | filter:filterVehCode">
				<td><input type="radio" name="vehicleName" id="vehicleId"
					value="{{ vehicle }}" ng-model="vehicleCode"
					ng-click="selectVeh(vehicle)"></td>
				<td>{{ vehicle }}</td>
			</tr>
		</table>
	</div>
 
	<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

		<input type="text" name="filterBranchCode" ng-model="filterBranchCode"
			placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Branch FaCode</th>

				<th>Branch Name</th>

				<th>Branch Pin</th>
			</tr>
			<tr ng-repeat="branch in branchList | filter:filterBranchCode">
				<td><input type="radio" name="branchCode" id="branchId"
					value="{{ branch.branchCode }}" ng-model="branchCodeTemp"
					ng-click="saveBranchCode(branch)"></td>
				<td>{{ branch.branchFaCode }}</td>
				<td>{{ branch.branchName }}</td>
				<td>{{ branch.branchPin }}</td>
			</tr>
		</table>
	</div>
	
	
	<div id="openExtraLoadingDB" ng-hide="openExtraLoadingDBFlag">
		<form id="extraAmtForm" name="extraAmtForm" ng-submit="setExtraAmt()">
		<table>
			<tr>
				<th>S.No.</th>
				<th>Type</th>
				<th>Amount</th>
				<th></th>
			</tr>
			<tr>
				<td>2</td>
				<td>Detection</td>
				<td><input type="text" name="detection" ng-model="chln.chlnDetection" ng-init="chln.chlnDetection = '0'"/></td>
				<td></td>
			</tr>
			<tr>
				<td>3</td>
				<td>Tool Tax</td>
				<td><input type="text" name="toolTax" ng-model="chln.chlnToolTax" ng-init="chln.chlnToolTax = '0'"/></td>
				<td></td>
			</tr>
			<tr>
				<td>4</td>
				<td>Height</td>
				<td><input type="text" name="height" ng-model="chln.chlnHeight" ng-init="chln.chlnHeight = '0'"/></td>
				<td></td>
			</tr>
			<tr>
				<td>5</td>
				<td>Union</td>
				<td><input type="text" name="union" ng-model="chln.chlnUnion" ng-init="chln.chlnUnion = '0'"/></td>
				<td></td>
			</tr>
			<tr>
				<td>6</td>
				<td>Two Point</td>
				<td><input type="text" name="twoPoint" ng-model="chln.chlnTwoPoint" ng-init="chln.chlnTwoPoint = '0'"/></td>
				<td></td>
			</tr>
			<tr>
				<td>7</td>
				<td>Weightment Charges</td>
				<td><input type="text" name="weightmentChg" ng-model="chln.chlnWeightmentChg" ng-init="chln.chlnWeightmentChg = '0'"/></td>
				<td></td>
			</tr>
			<tr>
				<td>8</td>
				<td>Crane Charges</td>
				<td><input type="text"/ name="craneChg" ng-model="chln.chlnCraneChg" ng-init="chln.chlnCraneChg = '0'"></td>
				<td></td>
			</tr>
			<tr>
				<td>9</td>
				<td>Others</td>
				<td><input type="text" name="others" ng-model="chln.chlnOthers" ng-init="chln.chlnOthers = '0'"/></td>
				<td></td>
			</tr>
			
			<tr>
				<td colspan="4"><input type="submit" value="SUBMIT"> </td>
			</tr>				
		</table>
	</div>

	<div id="challanFromStationDB" ng-hide="ChallanFromStationDBFlag">

		<input type="text" name="filterChallanFromStation"
			ng-model="filterChallanFromStation" placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Station Code</th>

				<th>Station Name</th>

				<th>Station District</th>
			</tr>


			<tr
				ng-repeat="station in stationList | filter:filterChallanFromStation">
				<td><input type="radio" name="stnName" id="stnName"
					class="stnName" value="{{ station }}" ng-model="frmStationCode"
					ng-click="saveFrmStnCode(station)"></td>
				<td>{{ station.stnCode }}</td>
				<td>{{ station.stnName }}</td>
				<td>{{ station.stnDistrict }}</td>
			</tr>
		</table>
	</div>

	<div id="challanToStationDB" ng-hide="ChallanToStationDBFlag">

		<input type="text" name="filterChallanToStation"
			ng-model="filterChallanToStation" placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Station Code</th>

				<th>Station Name</th>

				<th>Station District</th>
			</tr>


			<tr
				ng-repeat="station in stationList | filter:filterChallanToStation">
				<td><input type="radio" name="stnName" id="stnName"
					class="stnName" value="{{ station}}" ng-model="toStationCode"
					ng-click="saveToStnCode(station)"></td>
				<td>{{ station.stnCode }}</td>
				<td>{{ station.stnName }}</td>
				<td>{{ station.stnDistrict }}</td>
			</tr>
		</table>
	</div>

	<div id="employeeCodeDB" ng-hide="EmployeeCodeDBFlag">

		<input type="text" name="filterEmployeeCode"
			ng-model="filterEmployeeCode" placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>
			</tr>

			<tr ng-repeat="employee in employeeList | filter:filterEmployeeCode">
				<td><input type="radio" name="empCode" id="empId"
					value="{{ employee.empCode }}" ng-model="empTempCode"
					ng-click="saveEmployeeCode(employee)"></td>
				<td>{{ employee.empFaCode }}</td>
				<td>{{ employee.empName }}</td>
			</tr>
		</table>
	</div>

	<div id="payAtDB" ng-hide="PayAtDBFlag">

		<input type="text" name="filterPay" ng-model="filterPay"
			placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Branch FaCode</th>

				<th>Branch Name</th>

				<th>Branch Pin</th>
			</tr>
			<tr ng-repeat="branch in branchList | filter:filterPay">
				<td><input type="radio" name="branchCode" id="branchId"
					value="{{ branch.branchCode }}" ng-model="payOnCode"
					ng-click="savePayAt(branch)"></td>
				<td>{{ branch.branchFaCode }}</td>
				<td>{{ branch.branchName }}</td>
				<td>{{ branch.branchPin }}</td>
			</tr>
		</table>
	</div>

	<div id="cnmtDB" ng-hide="CnmtDBFlag">

		<input type="text" name="filterCnmt" ng-model="filterCnmt"
			placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>CNMT Code</th>
				
				<th>No. Of Pkg</th>

				<th>Weight</th>

				<!-- <th>CNMT Contract Code</th>

			<th>CNMT Customer Code</th> -->
			</tr>
			<tr ng-repeat="cnmt in cnmtList | filter:filterCnmt">
				<td><input type="radio" name="cnmtName" id="cnmtId"
					value="{{ cnmt.cnmtCode }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt,pkg[$index],wt[$index])"></td>
				<td>{{ cnmt.cnmtCode }}</td>
					<td>
					<input type="text" name="pkg" id="pkgId[$index]" ng-model="pkg[$index]" ng-init="pkg[$index]=cnmt.cnmtNoOfPkg" value="cnmt.cnmtNoOfPkg">
				</td>
				<td>
					<input type="number" name="wtName" id="wtId[$index]" ng-model="wt[$index]" ng-init="wt[$index]=(cnmt.cnmtActualWt/1000)" value="(cnmt.cnmtActualWt/1000)">
				</td>
				<!-- <td>{{ cnmt.contractCode }}</td>
			<td>{{ cnmt.custCode }}</td> -->
			</tr>
		</table>
	</div>
	
	
	<div id="MyCnmtListDB" ng-show="myCnmtListFlag">
		<input type="text" name="cnmtNo" ng-model="cnmtNo" placeholder="Total CNMT" ng-change="make()">
		<table>			
			<tr ng-repeat="no in noList">				
				<td>
					<input type="text" name="myCnmtList[$index]" id="myCnmtList[$index]" ng-model="myCnmtList[$index]" placeholder="Cnmt No">
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="SUBMIT" ng-click="getCnmtList()">
				</td>
			</tr>
		</table>
	</div>

	<div id="multiCnmtDB" ng-hide="MultiCnmtDBFlag">
		<input type="text" name="filterMultiCnmt" ng-model="filterMultiCnmt"
			placeholder="Search CNMT Code">
		<table>
			<tr>
				<th></th>
				<th>CNMT Code</th>
				<th>No. Of Pkg</th>
				<th>Weight</th>				
			</tr>
			<tr ng-repeat="multiCnmt in cnmtList | filter:filterMultiCnmt">
				<td><input type="radio" name="multiCnmt"
					value="{{ multiCnmt.cnmtCode }}" ng-model="multiCnmtCode"
					ng-click="saveMultiCnmt(multiCnmt,pkg[$index],wt[$index])">
				</td>
				<td>{{ multiCnmt.cnmtCode}}</td>
				<td>
					<input type="number" name="pkg" id="pkgId[$index]" ng-model="pkg[$index]" ng-init="pkg[$index]=multiCnmt.cnmtNoOfPkg" value="multiCnmt.cnmtNoOfPkg">
				</td>
				<td>
					<input type="number" name="wtName" id="wtId[$index]" ng-model="wt[$index]" ng-init="wt[$index]=(multiCnmt.cnmtActualWt/1000)" value="(multiCnmt.cnmtActualWt/1000)">
				</td>
				<!-- <td>{{ multiCnmt.custCode }}</td> -->
			</tr>
		</table>
	</div>

	<div id="vehicleTypeDB" ng-hide="VehicleTypeDBFlag">

		<input type="text" name="filterVehicleType"
			ng-model="filterVehicleType" placeholder="Search by Code">

		<table>
			<tr>
				<th></th>
				<th>Vehicle Type</th>
				<th>Vehicle Type Code</th>
			</tr>
			<tr ng-repeat="vt in vtList | filter:filterVehicleType">
				<td><input type="radio" name="vtCode" id="vtId"
					value="{{ vt }}" ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
				<td>{{ vt.vtVehicleType }}</td>
				<td>{{ vt.vtCode }}</td>
			</tr>
		</table>
	</div>

	<div id="chdBrCodeDB" ng-hide="ChdBrCodeDBFlag">

		<input type="text" name="filterChdBrCode" ng-model="filterChdBrCode"
			placeholder="Search by Code">

		<table>
			<tr>
				<th></th>
				<th>Broker FaCode</th>
				<th>Broker Name</th>
				<!-- <th>Broker Pan Name</th> -->
			</tr>
			<tr ng-repeat="brk in brkList | filter:filterChdBrCode">
				<td><input type="radio" name="brkCode" id="brkId"
					value="{{ brk }}" ng-model="brkCode" ng-click="saveBrokerCode(brk)"></td>
				<td>{{ brk.faCode }}</td>
				<td>{{ brk.name }}</td>
				<!-- <td>{{ brk.brkPanName }}</td> -->
			</tr>
		</table>
	</div>

	<div id="chdOwnCodeDB" ng-hide="ChdOwnCodeDBFlag">

		<input type="text" name="filterChdOwnCode" ng-model="filterChdOwnCode"
			placeholder="Search by Code">
		<table>
			<tr>
				<th></th>
				<th>Owner FaCode</th>
				<th>Owner Name</th>
				<!-- <th>Owner Pan Name</th> -->
			</tr>
			<tr ng-repeat="own in ownList | filter:filterChdOwnCode">
				<td><input type="radio" name="ownCode" id="ownId"
					value="{{ own }}" ng-model="ownCode" ng-click="saveOwnerCode(own)"></td>
				<td>{{ own.faCode }}</td>
				<td>{{ own.name }}</td>
				<!-- <td>{{ own.ownPanName }}</td> -->
			</tr>
		</table>
	</div>

	<div id="chdPanIssueStDB" ng-hide="ChdPanIssueStDBFlag">

		<input type="text" name="filterChdPanIssueSt" ng-model="ChdPanIssueSt"
			placeholder="Search">

		<table>
			<tr>
				<th></th>
				<th>State Code</th>
				<th>State Name</th>
			</tr>
			<tr ng-repeat="state in stList | filter:ChdPanIssueSt">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="savePanIssueStateCode(state)"></td>
				<td>{{ state.stateCode }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>
	</div>

	<div id="chdPanIssueStDB1" ng-hide="PanIssueStFlag1">
		<table>
			<tr>
				<td>State Name:</td>
				<td><input type="text" id="stateName" name="stateName"
					ng-model="state.stateName"></td>
			</tr>

			<tr>
				<td>State Code:</td>
				<td><input type="text" id="stateCode" name="stateCode"
					ng-model="state.stateCode"></td>
			</tr>

			<tr>
				<td>State Lorry Prefix:</td>
				<td><input type="text" id="stateLryPrefix"
					name="stateLryPrefix" ng-model="state.stateLryPrefix"></td>
			</tr>

			<tr>
				<td><input type="submit" value="Submit" ng-model="savstate"
					ng-click="savepistate(state)"></td>
			</tr>
		</table>
	</div>

	<div id="chdPerStateCodeDB" ng-hide="ChdPerStateCodeDBFlag">

		<input type="text" name="filterChdPerStateCode"
			ng-model="filterChdPerStateCode" placeholder="Search">
		<table>
			<tr>
				<th></th>
				<th>State Name</th>
				<th>State Prefix</th>
			</tr>
			<tr ng-repeat="state in stList | filter:filterChdPerStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="savePerStateCode(state)"></td>
				<td>{{ state.stateName }}</td>
				<td>{{ state.stateLryPrefix }}</td>
			</tr>
		</table>
	</div>

	<div id="chdPerStateCodeDB1" ng-hide="PerStateCodeFlag1">
		<table>
			<tr>
				<td>State Name:</td>
				<td><input type="text" id="stateName" name="stateName"
					ng-model="state.stateName"></td>
			</tr>

			<tr>
				<td>State Code:</td>
				<td><input type="text" id="stateCode" name="stateCode"
					ng-model="state.stateCode"></td>
			</tr>

			<tr>
				<td>State Lorry Prefix:</td>
				<td><input type="text" id="stateLryPrefix"
					name="stateLryPrefix" ng-model="state.stateLryPrefix"></td>
			</tr>

			<tr>
				<td><input type="submit" value="Submit" ng-model="savstate"
					ng-click="savepscode(state)"></td>
			</tr>
		</table>
	</div>

	<div id="chdBrMobNoDB" ng-hide="ChdBrMobNoDBFlag">

		<input type="text" name="filterChdBrMobNo" ng-model="filterChdBrMobNo"
			placeholder="Search">
		<table>
			<tr>
				<th></th>
				<th>Mobile No.</th>
			</tr>
			<tr ng-repeat="mobile in mobListBk | filter:filterChdBrMobNo">
				<td><input type="radio" name="Mobile" id="Mobile"
					value="{{ mobile }}" ng-model="cpMobile"
					ng-click="saveBrMobNo(mobile)"></td>
				<td>{{ mobile }}</td>
			</tr>
		</table>
	</div>
	
	

	<div id="chdBrMobNoDB1" ng-hide="BrMobNoFlag1">
		<table>
			<tr>
				<td>Mobile No.:</td>
				<td><input type="text" id="cpMobile" name="cpMobile"
					ng-model="contPerson.cpMobile"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit"
					ng-model="savcontPerson" ng-click="savebrmob(contPerson.cpMobile)"></td>
			</tr>
		</table>
	</div>


	<div id="chdOwnMobNoDB" ng-hide="ChdOwnMobNoDBFlag">

		<input type="text" name="filterChdOwnMobNo"
			ng-model="filterChdOwnMobNo" placeholder="Search">
		<table>
			<tr>
				<th></th>
				<th>Mobile No.</th>
			</tr>
			<tr ng-repeat="mobile in mobListOw | filter:filterChdOwnMobNo">
				<td><input type="radio" name="cpMobile" id="cpMobile"
					value="{{ mobile }}" ng-model="cpMobile"
					ng-click="saveOwnMobNo(mobile)"></td>
				<td>{{ mobile }}</td>
			</tr>
		</table>
	</div>

	<div id="chdOwnMobNoDB1" ng-hide="OwnMobNoFlag1">
		<table>
			<tr>
				<td>Mobile No.:</td>
				<td><input type="text" id="cpMobile" name="cpMobile"
					ng-model="contPerson.cpMobile"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit"
					ng-model="savcontPerson" ng-click="saveownmob(contPerson)"></td>
			</tr>
		</table>
	</div>

	<div id="ChallanCodeDB" ng-hide="ChallanCodeDBFlag">

		<input type="text" name="filterChallanCode"
			ng-model="filterChallanCode" placeholder="Search">
		<table>
			<tr>
				<th></th>
				<th>Challan Code</th>
			</tr>
			<tr
				ng-repeat="challan in challanCodesList | filter:filterChallanCode">
				<td><input type="radio" name="challanCode"
					value="{{ challan }}" ng-model="challanCode"
					ng-click="saveChallanCode(challan)"></td>
				<td>{{ challan}}</td>
			</tr>
		</table>
	</div>



	<div id="saveMobileID" ng-hide="saveMobileFlag">
		<form name="saveMobileForm"
			ng-submit="saveBrkMobNo(saveMobileForm,mobileNo)">
			<table>
				<tr>
					<td>Mobile No:</td>
					<td><input type="text" id="mobileNo" name="mobileNo"
						ng-model="mobileNo" ng-minlength="10" ng-maxlength="10" required></td>
				</tr>
				<tr>
					<td><input type="submit" value="Submit" ng-model="saveMob"></td>
				</tr>

			</table>
		</form>
	</div>

	<div id="saveMobileOwnID" ng-hide="saveMobileOwnFlag">
		<form name="saveMobileOwnForm"
			ng-submit="saveOwnMobNolist(saveMobileOwnForm,mobileOwnNo)">
			<table>
				<tr>
					<td>Mobile No:</td>
					<td><input type="text" id="mobileOwnNo" name="mobileOwnNo"
						ng-model="mobileOwnNo" ng-minlength="10" ng-maxlength="10"
						required></td>
				</tr>
				<tr>
					<td><input type="submit" value="Submit" ng-model="saveOwnMob"></td>
				</tr>

			</table>
		</form>
	</div>
	
	
	

	<div id="viewChallanDetailsDB" ng-hide="viewChallanDetailsFlag">
		<div class="row">
			<div class="col s12 card"
				style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
				<h4>
					Here's the review and details of <span
						class="teal-text text-lighten-2">{{chln.chlnId}}</span>:
				</h4>
				<table class="table-hover table-bordered table-condensed">
					<tr>
						<td>Challan Branch Code:</td>
						<td>{{chln.branchCode}}</td>
					</tr>
					<tr>
						<td>Lorry No:</td>
						<td>{{chln.chlnLryNo}}</td>
					</tr>
					<tr>
						<td>Challan From Station:</td>
						<td>{{chln.chlnFromStn}}</td>
					</tr>
					<tr>
						<td>Challan To Station:</td>
						<td>{{chln.chlnToStn}}</td>
					</tr>
					<tr>
						<td>Employee Code:</td>
						<td>{{chln.chlnEmpCode}}</td>
					</tr>
					<tr>
						<td>Lorry Rate:</td>
						<td>{{chln.chlnLryRate}}</td>
					</tr>
					<tr>
						<td>Change Weight:</td>
						<td>{{chln.chlnChgWt}}</td>
					</tr>
					<tr>
						<td>No. Of Package:</td>
						<td>{{chln.chlnNoOfPkg}}</td>
					</tr>
					<tr>
						<td>Challan Total Weight:</td>
						<td>{{chln.chlnTotalWt}}</td>
					</tr>
					<tr>
						<td>Challan Freight:</td>
						<td>{{chln.chlnFreight}}</td>
					</tr>
					<tr>
						<td>Loading Amount:</td>
						<td>{{chln.chlnLoadingAmt}}</td>
					</tr>
					<tr>
						<td>Challan Extra:</td>
						<td>{{chln.chlnExtra}}</td>
					</tr>
					<tr>
						<td>Total Freight:</td>
						<td>{{chln.chlnTotalFreight}}</td>
					</tr>
					<tr>
						<td>Challan Advance:</td>
						<td>{{chln.chlnAdvance}}</td>
					</tr>
					<tr>
						<td>Challan Balance:</td>
						<td>{{chln.chlnBalance}}</td>
					</tr>
					<tr>
						<td>Challan Pay At:</td>
						<td>{{chln.chlnPayAt}}</td>
					</tr>
					<tr>
						<td>Time Allow:</td>
						<td>{{chln.chlnTimeAllow}}</td>
					</tr>

					<tr>
						<td>Challan Date:</td>
						<td>{{chln.chlnDt}}</td>
					</tr>

					<tr>
						<td>BR Rate:</td>
						<td>{{chln.chlnBrRate}}</td>
					</tr>

					<tr>
						<td>Weight Slip:</td>
						<td>{{chln.chlnWtSlip}}</td>
					</tr>

					<tr>
						<td>Vehicle type:</td>
						<td>{{chln.chlnVehicleType}}</td>
					</tr>

					<tr>
						<td>Statistical Change:</td>
						<td>{{chln.chlnStatisticalChg}}</td>
					</tr>

					<tr>
						<td>Tds Amount:</td>
						<td>{{chln.chlnTdsAmt}}</td>
					</tr>

					<tr>
						<td>Challan RR No:</td>
						<td>{{chln.chlnRrNo}}</td>
					</tr>

					<tr>
						<td>Challan Train No:</td>
						<td>{{chln.chlnTrainNo}}</td>
					</tr>

					<tr>
						<td>Lorry Load Time:</td>
						<td>{{chln.chlnLryLoadTime}}</td>
					</tr>

					<tr>
						<td>Lorry Reporting Date:</td>
						<td>{{chln.chlnLryRepDT}}</td>
					</tr>

					<tr>
						<td>Lorry Reporting Time:</td>
						<td>{{chln.chlnLryRptTime}}</td>
					</tr>
					<tr>
						<td>Broker Code:</td>
						<td>{{chd.chdBrCode}}</td>
					</tr>

					<tr>
						<td>Owner Code:</td>
						<td>{{chd.chdOwnCode}}</td>
					</tr>

					<tr>
						<td>Broker Mobile No:</td>
						<td>{{chd.chdBrMobNo}}</td>
					</tr>

					<tr>
						<td>Owner Mobile No:</td>
						<td>{{chd.chdOwnMobNo}}</td>
					</tr>

					<tr>
						<td>Driver Name:</td>
						<td>{{chd.chdDvrName}}</td>
					</tr>

					<tr>
						<td>Driver Mobile No.:</td>
						<td>{{chd.chdDvrMobNo}}</td>
					</tr>

					<tr>
						<td>RC No</td>
						<td>{{chd.chdRcNo}}</td>
					</tr>

					<tr>
						<td>RC Issue Date:</td>
						<td>{{chd.chdRcIssueDt}}</td>
					</tr>

					<tr>
						<td>RC Valid Date:</td>
						<td>{{chd.chdRcValidDt}}</td>
					</tr>

					<tr>
						<td>DL No:</td>
						<td>{{chd.chdDlNo}}</td>
					</tr>

					<tr>
						<td>DL Issue Date:</td>
						<td>{{chd.chdDlIssueDt}}</td>
					</tr>

					<tr>
						<td>DL Valid Date:</td>
						<td>{{chd.chdDlValidDt}}</td>
					</tr>

					<tr>
						<td>Per NO:</td>
						<td>{{chd.chdPerNo}}</td>
					</tr>

					<tr>
						<td>Per Issue Date:</td>
						<td>{{chd.chdPerIssueDt}}</td>
					</tr>

					<tr>
						<td>Per Valid Date:</td>
						<td>{{chd.chdPerValidDt}}</td>
					</tr>

					<tr>
						<td>Per State:</td>
						<td>{{chd.chdPerState}}</td>
					</tr>

					<tr>
						<td>Fit Doc No:</td>
						<td>{{chd.chdFitDocNo}}</td>
					</tr>

					<tr>
						<td>Fit Doc Issue Date:</td>
						<td>{{chd.chdFitDocIssueDt}}</td>
					</tr>

					<tr>
						<td>Fit Doc Valid Date:</td>
						<td>{{chd.chdFitDocValidDt}}</td>
					</tr>

					<tr>
						<td>Fit Doc Place:</td>
						<td>{{chd.chdFitDocPlace}}</td>
					</tr>

					<tr>
						<td>Bank Finance:</td>
						<td>{{chd.chdBankFinance}}</td>
					</tr>

					<tr>
						<td>Policy No:</td>
						<td>{{chd.chdPolicyNo}}</td>
					</tr>

					<tr>
						<td>Policy Com:</td>
						<td>{{chd.chdPolicyCom}}</td>
					</tr>

					<tr>
						<td>Transit Pass No:</td>
						<td>{{chd.chdTransitPassNo}}</td>
					</tr>

					<tr>
						<td>Pass Hdr Type:</td>
						<td>{{chd.chdPanHdrType}}</td>
					</tr>

					<tr>
						<td>Pan No:</td>
						<td>{{chd.chdPanNo}}</td>
					</tr>

					<!-- <tr>
						<td>Pan Hdr Name:</td>
						<td>{{chd.chdPanHdrName}}</td>
					</tr> -->

					<tr>
						<td>Pan Issue St:</td>
						<td>{{chd.chdPanIssueSt}}</td>
					</tr>
					
					<tr>
						<td>Engine No:</td>
						<td>{{chd.chdEngineNo}}</td>
					</tr>
					
					<tr>
						<td>Chassis No:</td>
						<td>{{chd.chdChassisNo}}</td>
					</tr>
					
					<tr>
						<td>Model:</td>
						<td>{{chd.chdModel}}</td>
					</tr>
					
				</table>
				<input type="button" value="Save" id="saveBtnId"
					ng-click="saveChallan(challan,chlnChgWtPer,chlnTotalWtPer,chlnLryRatePer,chln)">
				<input type="button" value="Cancel"
					ng-click="closeViewChallanDetailsDB()">
			</div>
		</div>
	</div>
</div>
<!-- </div> -->
