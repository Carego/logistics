package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.CustomerRepresentativeCNTS;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;


public class CustomerRepresentativeDAOImpl implements CustomerRepresentativeDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private org.hibernate.Transaction transaction;

	@Autowired
	public CustomerRepresentativeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@SuppressWarnings("unchecked")
	@Transactional//done
	public List<CustomerRepresentative> getCustomerRep(int custRepId){
		List<CustomerRepresentative> list = new ArrayList<CustomerRepresentative>();
		try{
			session = this.sessionFactory.openSession();//fixed
			Criteria cr=session.createCriteria(CustomerRepresentative.class);//fixed
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CR_ID, custRepId));
			list =cr.list();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  list; 
	}

	@Transactional//done
	@SuppressWarnings("unchecked")
	public int getLastCustomerRep() {
		List<CustomerRepresentative> list = new ArrayList<CustomerRepresentative>();
		int id=0;
		try{
			session = this.sessionFactory.openSession();
			list = session.createQuery("from CustomerRepresentative order by crId DESC ").setMaxResults(1).list();
			id = list.get(0).getCrId();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return id;
	}


	@Transactional
	public int saveCustomerRepToDB(CustomerRepresentative custrep){
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			temp = (Integer)session.save(custrep);
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Transactional
	public long totalCount(){
		long tempCount = -1;
		try{
			session = this.sessionFactory.openSession();
			tempCount= (Long) session.createCriteria(CustomerRepresentative.class).setProjection(Projections.rowCount()).uniqueResult();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return tempCount;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Object[]> getCustomerRepresentativeCodeNameDesig(){
		List<Object[]>	list = new ArrayList<Object[]>();
		try{
			System.out.println("Inside getCustomerRepresentativeCodeNameDesig");
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			list =(List<Object[]>)session.createQuery("select cr.crName,cr.crCode,cr.crDesignation from CustomerRepresentative cr").list();
			for(Object[] e1: list){

				String crName = (String)e1[0];
				String crCode = (String)e1[1];
				String crDesignation = (String)e1[2];


				System.out.println("The crName is "+crName);
				System.out.println("The crCode is "+crCode);
				System.out.println("The crDesignation is "+crDesignation);
			}
			transaction.commit();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.close();

		return list;
	}	



	@Transactional
	@SuppressWarnings("unchecked")
	public List<Object[]> getCRCodeNameDesigCustName(String custCode){
		List<Object[]>	list = new ArrayList<Object[]>();

		try{
			System.out.println("Inside getCustomerRepresentativeCodeNameDesig");
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			org.hibernate.Query query=session.createQuery("select cr.crName,cr.crCode,cr.crDesignation,cust.custName from CustomerRepresentative cr,Customer cust where cr.custCode=cust.custCode and cust.custCode= :custCode");
			query.setParameter("custCode",custCode);
			list=(List<Object[]>)query.list();

			for(Object[] e1: list){	 
				String crName = (String)e1[0];
				String crCode = (String)e1[1];
				String crDesignation = (String)e1[2];
				String custName =(String)e1[3];

				System.out.println("The crName is "+crName);
				System.out.println("The crCode is "+crCode);
				System.out.println("The crDesignation is "+crDesignation);
				System.out.println("The custName is "+custName);
			}
			transaction.commit();
			session.flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return list;
	}	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getCustomerRepCode(){

		List<String> custRepCodeList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(CustomerRepresentative.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(CustomerRepresentativeCNTS.CR_CODE));
			cr.setProjection(projList);
			custRepCodeList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		
		session.clear();
		session.close();
		return custRepCodeList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<CustomerRepresentative> getCustomerRepByCustCode(String custCode){
		System.out.println("enter into getCustomerRepByCustCode function");
		List<CustomerRepresentative> custRepList = new ArrayList<CustomerRepresentative>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(CustomerRepresentative.class);
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CUST_CODE, custCode));
			custRepList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return custRepList;
	}



	@Transactional
	@SuppressWarnings("unchecked")
	public List<CustomerRepresentative> getCustRepIsViewNo(String custCode){
		System.out.println("enter into getCustRepIsViewNo function ==> "+custCode);
		List<CustomerRepresentative> result = new ArrayList<CustomerRepresentative>();	 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(CustomerRepresentative.class);
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.IS_VIEW,false));
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CUST_CODE,custCode));
			result =cr.list();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public List<String>getAllCustRepCode(String custCode){
		System.out.println("enter into getAllCustRepCode function ==> "+custCode);
		List<String> result = new ArrayList<String>();	 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			ProjectionList projList = Projections.projectionList();
			Criteria cr=session.createCriteria(CustomerRepresentative.class);
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CUST_CODE,custCode));
			projList.add(Projections.property(CustomerRepresentativeCNTS.CR_CODE));
			cr.setProjection(projList);
			result =cr.list();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}



	@Transactional
	@SuppressWarnings("unchecked")
	public int updateCustRepIsView(String[] code) {
		System.out.println("enter into updateCustRepIsView function");
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			for(int i=0;i<code.length;i++){
				CustomerRepresentative custRep = new CustomerRepresentative();
				List<CustomerRepresentative> list = new ArrayList<CustomerRepresentative>();
				Criteria cr=session.createCriteria(CustomerRepresentative.class);
				cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CR_CODE,code[i]));
				list =cr.list();
				custRep = list.get(0);
				custRep.setView(true);
				session.update(custRep);

			}
			transaction.commit();
			temp= 1;
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public  CustomerRepresentative getCustRepByCode(String custRepCode){
		System.out.println("enter into getCustRepByCode function ==> "+custRepCode);
		CustomerRepresentative customerRepresentative = new CustomerRepresentative();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(CustomerRepresentative.class);
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CR_CODE,custRepCode));
			customerRepresentative = (CustomerRepresentative)cr.list().get(0);
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  customerRepresentative; 
	}

	@Transactional
	public int updateCustomerRepByCrCode(CustomerRepresentative custrep) {
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("****enter into retrieveUser---->");
		String crCode = custrep.getCrCode();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(CustomerRepresentative.class);
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CR_CODE,crCode));
			transaction = session.beginTransaction();
			custrep.setCreationTS(calendar);
			session.saveOrUpdate(custrep);
			transaction.commit();
			temp= 1;
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}
	
	 @Transactional
	 public int updateCustomerRepById(CustomerRepresentative custrep){
		 System.out.println("Inside update functon with id-----"+custrep.getCrId());
 		int temp;
 		Date date = new Date();
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(CustomerRepresentative.class);
			 cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CR_ID, custrep.getCrId()));
			 transaction = session.beginTransaction();
			 custrep.setCreationTS(calendar);
			 session.saveOrUpdate(custrep);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }		
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	@Transactional
	@SuppressWarnings("unchecked")
	public List<CustomerRepresentative> getAllCustRepByBrCode(String brCode) {
	System.out.println("enter into getAllCustRepByBrCode function");
		List<CustomerRepresentative> custRepList = new ArrayList<CustomerRepresentative>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerRepresentative.class);
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.USER_BRANCH_CODE,
					brCode));
			custRepList = cr.list();
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return custRepList;
	}
}
