package com.mylogistics.controller;

//import java.util.Date;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaCarDivisionDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.VehicleMstrDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FaCarDivision;
import com.mylogistics.model.SubVehicleMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VM_SubVMService;
import com.mylogistics.services.VehVoucherService;
import com.mylogistics.services.VehVoucherServiceImpl;
import com.mylogistics.services.VoucherService;

@Controller
public class VehVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private VehicleMstrDAO vehicleMstrDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private FaCarDivisionDAO faCarDivisionDAO;
	
	@Autowired
	private ChequeLeavesDAO chequeLeavesDAO;
	
	private VehVoucherService vehVoucherService = new VehVoucherServiceImpl();
	
	
	@RequestMapping(value = "/getVDetFrVehV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrVehV() {  
		System.out.println("Enter into getVDetFrVehV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getVehMstrFrVehV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVehMstrFrVehV() {  
		System.out.println("Enter into getVehMstrFrVehV---->");
		Map<String, Object> map = new HashMap<>();	
		List<VehicleMstr> vehList = vehicleMstrDAO.getAllVehicleMstr();
		if(!vehList.isEmpty()){
			map.put("list",vehList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getChequeNoFrVehV" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrVehV(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNoFrVehV---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		ChequeLeaves chequeLeaves = bankMstrDAO.getAccPayChqByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getAPChqLByBnkC(bankCode,CType);
		if(chequeLeaves != null){
			System.out.println("***************** 1");
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put("list",chqList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/addVMEDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object addVMDetails(@RequestBody VM_SubVMService vM_SubVMService) {  
		System.out.println("Enter into addVMDetails---->");
		Map<String, Object> map = new HashMap<>();	
		vehVoucherService.addVmAndSVm(vM_SubVMService);
		return map;
	}	
	
	
	@RequestMapping(value = "/fetchVMEDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object fetchVMEDetails() {  
		System.out.println("Enter into fetchVMEDetails---->");
		Map<String, Object> map = new HashMap<>();	
		List<VM_SubVMService> vmsvmList = vehVoucherService.getAllVmAndSVm();
		map.put("list",vmsvmList);
		return map;
	}	
	
	
	@RequestMapping(value = "/removeVMEDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object removeVMEDetails(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into removeVMEDetails---->");
		Map<String, Object> map = new HashMap<>();	
		int index = (int) clientMap.get("index");
		vehVoucherService.remove(index);
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitVehVoucher" , method = RequestMethod.POST)
	public @ResponseBody Object submitVehVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("enter into submitVehVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(voucherService.getVoucherType().equalsIgnoreCase("Vehicle")){
			 map = saveVehVoucher(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	
	public Map<String,Object> saveVehVoucher(VoucherService voucherService){
		System.out.println("Enter into saveVehVoucher----->>>");
		Map<String,Object> map = new HashMap<String,Object>();
		List<FAMaster> famList = faMasterDAO.getFAMByFaName(ConstantsValues.CAR_EXP_FANAME);
		String carFaCode = "";
		if(!famList.isEmpty()){
			carFaCode = famList.get(0).getFaMfaCode();
		}
		
		List<FAMaster> famList1 = faMasterDAO.getFAMByFaName(ConstantsValues.SANDM_EXP_FANAME);
		String smFaCode = "";
		if(!famList1.isEmpty()){
			smFaCode = famList1.get(0).getFaMfaCode();
		}
		
		char payBy = voucherService.getPayBy();	
		
		System.out.println("value of payBy = "+payBy);
		
		if(payBy == 'C'){			
			map = saveVehByCash(voucherService , carFaCode , smFaCode);			
		}else if(payBy == 'Q'){
			map = saveVehByCheque(voucherService , carFaCode ,smFaCode);
		}else if(payBy == 'O'){
			map = saveVehByOnline(voucherService , carFaCode ,smFaCode);
		}else{
			map.put("vhNo",0);
			map.put("tvNo","0");
			System.out.println("invalid payment method");
		}
		
		return map;
	}
	
	
	
	public Map<String,Object> saveVehByCash(VoucherService voucherService , String carFaCode , String smFaCode){
		System.out.println("Enter into saveVehByCash----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<VM_SubVMService> vm_svmList = vehVoucherService.getAllVmAndSVm();
		String payMode = "C";	
		int voucherNo = 0;
		if(!vm_svmList.isEmpty()){
			String tdsCode = voucherService.getTdsCode();
			double tdsAmt = voucherService.getTdsAmt();
			/*String tdsCode = voucherService.getTdsCode();
			double tdsAmt = voucherService.getTdsAmt();
			if(tdsCode != null && tdsAmt > 0){
				Map<String,Object> newMap = new HashMap<String,Object>();
				
				CashStmt csWTds = new CashStmt();
				csWTds.setbCode(currentUser.getUserBranchCode());
				csWTds.setUserCode(currentUser.getUserCode());
				csWTds.setCsDescription(voucherService.getDesc());
				csWTds.setCsDrCr('C');
				csWTds.setCsAmt(tdsAmt);
				csWTds.setCsType(voucherService.getVoucherType());
				csWTds.setCsVouchType("cash");
				csWTds.setCsPayTo(voucherService.getPayTo());
				csWTds.setCsTvNo(ConstantsValues.CONST_TVNO);
				csWTds.setCsFaCode(tdsCode);
				csWTds.setCsVouchNo(voucherNo);
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				csWTds.setCsDt(sqlDate);
				
				newMap = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
				voucherNo = (int) newMap.get("vhNo");
			}*/
			
			for(int i=0;i<vm_svmList.size();i++){
				Map<String,Object> newMap = new HashMap<String,Object>();
				VM_SubVMService vmsvm = vm_svmList.get(i);
				VehicleMstr vehicleMstr = vmsvm.getVehMstr();
				SubVehicleMstr subVehicleMstr = vmsvm.getsVehMstr();
				List<FaCarDivision> faCarList = vmsvm.getFaCarList();
				
				String tvNo = vehicleMstr.getVehNo();			
				
				String vehBrFaCode =  vehicleMstr.getBranch().getBranchFaCode();
				
				if(brFaCode.equalsIgnoreCase(vehBrFaCode)){
					
					CashStmt cashStmt = new CashStmt();
					cashStmt.setbCode(currentUser.getUserBranchCode());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setCsDescription(voucherService.getDesc());
					cashStmt.setCsDrCr('D');
					cashStmt.setCsAmt(subVehicleMstr.getSvmTotAmt());
					cashStmt.setCsType(voucherService.getVoucherType());
					cashStmt.setCsVouchType("cash");
					cashStmt.setCsPayTo(voucherService.getPayTo());
					cashStmt.setCsTvNo(tvNo);
					if(vehicleMstr.getVehType() == 'C'){
						cashStmt.setCsFaCode(carFaCode);
					}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
						cashStmt.setCsFaCode(smFaCode);
					}else{
						
					}
					
					cashStmt.setCsVouchNo(voucherNo);					
					java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					cashStmt.setCsDt(sqlDate);
					cashStmt.setPayMode(payMode);
					
					newMap = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt);
					voucherNo = (int) newMap.get("vhNo");
					System.out.println("cashStmt id = "+cashStmt.getCsId());
					
					subVehicleMstr.setbCode(currentUser.getUserBranchCode());
					subVehicleMstr.setUserCode(currentUser.getUserCode());
					subVehicleMstr.setSvmCsId(cashStmt.getCsId());
					subVehicleMstr.setSvmTvNo(tvNo);
					subVehicleMstr.setSvmFaCode(carFaCode);
					
					vehicleMstrDAO.updateVMBySVM(vehicleMstr.getVehId() , subVehicleMstr);
					
					cashStmtStatusDAO.updateCSSBySFId(cashStmt.getCsId() , subVehicleMstr.getSvmId());
					
					for(int k=0;k<faCarList.size();k++){
						FaCarDivision faCarDivision = faCarList.get(k);
						faCarDivision.setbCode(currentUser.getUserBranchCode());
						faCarDivision.setUserCode(currentUser.getUserCode());
						faCarDivision.setFcdSvmId(subVehicleMstr.getSvmId());
						
						faCarDivisionDAO.saveFaCarD(faCarDivision);
					}
					
					
					if(tdsCode != null && tdsAmt > 0){
						Map<String,Object> newMap1 = new HashMap<String,Object>();
						
						CashStmt csWTds = new CashStmt();
						csWTds.setbCode(currentUser.getUserBranchCode());
						csWTds.setUserCode(currentUser.getUserCode());
						csWTds.setCsDescription(voucherService.getDesc());
						csWTds.setCsDrCr('C');
						csWTds.setCsAmt(tdsAmt);
						csWTds.setCsType(voucherService.getVoucherType());
						csWTds.setCsVouchType("cash");
						csWTds.setCsPayTo(voucherService.getPayTo());
						csWTds.setCsTvNo(tvNo);
						csWTds.setCsFaCode(tdsCode);
						csWTds.setCsVouchNo(voucherNo);					
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						csWTds.setCsDt(sqlDate);
						csWTds.setPayMode(payMode);
						
						newMap1 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
						voucherNo = (int) newMap1.get("vhNo");
					}
					
				}else{
					
					CashStmt cashStmt1 = new CashStmt();
					cashStmt1.setbCode(currentUser.getUserBranchCode());
					cashStmt1.setUserCode(currentUser.getUserCode());
					cashStmt1.setCsDescription(voucherService.getDesc());
					cashStmt1.setCsDrCr('D');
					cashStmt1.setCsAmt(subVehicleMstr.getSvmTotAmt() - tdsAmt);
					cashStmt1.setCsType(voucherService.getVoucherType());
					cashStmt1.setCsVouchType("cash");
					cashStmt1.setCsTvNo(tvNo);
					cashStmt1.setCsPayTo(voucherService.getPayTo());
					cashStmt1.setCsFaCode(vehicleMstr.getBranch().getBranchFaCode());					
					cashStmt1.setCsVouchNo(voucherNo);					
					java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					cashStmt1.setCsDt(sqlDate);
					cashStmt1.setPayMode(payMode);
					
					newMap =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt1);
					voucherNo = (int) newMap.get("vhNo");
					System.out.println("cashStmt id = "+cashStmt1.getCsId());
					
					subVehicleMstr.setbCode(currentUser.getUserBranchCode());
					subVehicleMstr.setUserCode(currentUser.getUserCode());
					subVehicleMstr.setSvmCsId(cashStmt1.getCsId());
					subVehicleMstr.setSvmTvNo(tvNo);
					subVehicleMstr.setSvmFaCode(carFaCode);
					
					vehicleMstrDAO.updateVMBySVM(vehicleMstr.getVehId() , subVehicleMstr);
					
					cashStmtStatusDAO.updateCSSBySFId(cashStmt1.getCsId() , subVehicleMstr.getSvmId());
					
					for(int k=0;k<faCarList.size();k++){
						FaCarDivision faCarDivision = faCarList.get(k);
						faCarDivision.setbCode(currentUser.getUserBranchCode());
						faCarDivision.setUserCode(currentUser.getUserCode());
						faCarDivision.setFcdSvmId(subVehicleMstr.getSvmId());
						
						faCarDivisionDAO.saveFaCarD(faCarDivision);
					}
					
					
					CashStmtTemp cashStmt2 = new CashStmtTemp();
					cashStmt2.setbCode(currentUser.getUserBranchCode());
					cashStmt2.setUserCode(currentUser.getUserCode());
					cashStmt2.setCsDescription(voucherService.getDesc());
					cashStmt2.setCsDrCr('C');
					cashStmt2.setCsAmt(subVehicleMstr.getSvmTotAmt() - tdsAmt);
					cashStmt2.setCsType(voucherService.getVoucherType());
					cashStmt2.setCsVouchType("contra");
					cashStmt2.setCsPayTo(voucherService.getPayTo());
					cashStmt2.setCsTvNo(tvNo);
					cashStmt2.setCsFaCode(brFaCode);
					cashStmt2.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());
					/*if(vehicleMstr.getVehType() == 'C'){
						cashStmt2.setCsFaCode(carFaCode);
					}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
						cashStmt2.setCsFaCode(smFaCode);
					}else{
						
					}*/					
					//cashStmt2.setCsVouchNo(voucherNo);
					cashStmt2.setCsDt(sqlDate);
					cashStmt2.setPayMode(payMode);
					
					cashStmtStatusDAO.saveCsTemp(cashStmt2);
					/*newMap =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt2);
					voucherNo = (int) newMap.get("vhNo");
					System.out.println("cashStmt id = "+cashStmt2.getCsId());*/
					
					CashStmtTemp cashStmt3 = new CashStmtTemp();
					cashStmt3.setbCode(currentUser.getUserBranchCode());
					cashStmt3.setUserCode(currentUser.getUserCode());
					cashStmt3.setCsDescription(voucherService.getDesc());
					cashStmt3.setCsDrCr('D');
					cashStmt3.setCsAmt(subVehicleMstr.getSvmTotAmt());
					cashStmt3.setCsType(voucherService.getVoucherType());
					cashStmt3.setCsVouchType("contra");
					cashStmt3.setCsPayTo(voucherService.getPayTo());
					cashStmt3.setCsTvNo(tvNo);
					if(vehicleMstr.getVehType() == 'C'){
						cashStmt3.setCsFaCode(carFaCode);
					}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
						cashStmt3.setCsFaCode(smFaCode);
					}else{
						
					}
					cashStmt3.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());					
					//cashStmt3.setCsVouchNo(voucherNo);
					cashStmt3.setCsDt(sqlDate);
					cashStmt3.setPayMode(payMode);
					
					cashStmtStatusDAO.saveCsTemp(cashStmt3);
					/*newMap =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt3);
					voucherNo = (int) newMap.get("vhNo");
					System.out.println("cashStmt id = "+cashStmt3.getCsId());*/
					
					if(tdsCode != null && tdsAmt > 0){
						
						CashStmtTemp csWTds = new CashStmtTemp();
						csWTds.setbCode(currentUser.getUserBranchCode());
						csWTds.setUserCode(currentUser.getUserCode());
						csWTds.setCsDescription(voucherService.getDesc());
						csWTds.setCsDrCr('C');
						csWTds.setCsAmt(tdsAmt);
						csWTds.setCsType(voucherService.getVoucherType());
						csWTds.setCsVouchType("contra");
						csWTds.setCsPayTo(voucherService.getPayTo());
						csWTds.setCsTvNo(tvNo);
						csWTds.setCsFaCode(tdsCode);
						//csWTds.setCsVouchNo(voucherNo);
						csWTds.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						csWTds.setCsDt(sqlDate);
						csWTds.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(csWTds);
						/*newMap1 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
						voucherNo = (int) newMap1.get("vhNo");*/
					}
				}
			}
			vehVoucherService.deleteAllVmAndSVM();
			map.put("vhNo",voucherNo);
		}else{
			map.put("tvNo","0");
			System.out.println("vehicle details are not avaliable");
		}
		return map;
	}
	
	
	
	public Map<String,Object> saveVehByCheque(VoucherService voucherService , String carFaCode , String smFaCode){
		System.out.println("Enter into saveVehByCheque----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		char chequeType = voucherService.getChequeType();
		System.out.println("chequeType = "+chequeType);
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		String bankCode = voucherService.getBankCode();
		List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(bankCode);
		String payMode = "Q";
		int voucherNo = 0;
		
		
		if(!bankMList.isEmpty()){
			
			String tdsCode = voucherService.getTdsCode();
			double tdsAmt = voucherService.getTdsAmt();
			/*if(tdsCode != null && tdsAmt > 0){
				Map<String,Object> newMap = new HashMap<String,Object>();
				
				CashStmt csWTds = new CashStmt();
				csWTds.setbCode(currentUser.getUserBranchCode());
				csWTds.setUserCode(currentUser.getUserCode());
				csWTds.setCsDescription(voucherService.getDesc());
				csWTds.setCsDrCr('C');
				csWTds.setCsAmt(tdsAmt);
				csWTds.setCsType(voucherService.getVoucherType());
				csWTds.setCsVouchType("bank");
				csWTds.setCsPayTo(voucherService.getPayTo());
				csWTds.setCsTvNo(ConstantsValues.CONST_TVNO);
				csWTds.setCsFaCode(tdsCode);
				csWTds.setCsVouchNo(voucherNo);
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				csWTds.setCsDt(sqlDate);
				
				newMap = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
				voucherNo = (int) newMap.get("vhNo");
			}*/
			
			
			
			
			BankMstr bankMstr = bankMList.get(0);
			List<VM_SubVMService> vm_svmList = vehVoucherService.getAllVmAndSVm();
			double totAmt = 0.0;
			if(!vm_svmList.isEmpty()){
				for(int i=0;i<vm_svmList.size();i++){
					totAmt = totAmt + vm_svmList.get(i).getsVehMstr().getSvmTotAmt();
				}
			}
			
			ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
			chequeLeaves.setBankMstr(bankMstr);
			chequeLeaves.setChqLChqAmt(totAmt - tdsAmt);
			chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
			chequeLeaves.setChqLUsed(true);
			
			String tvNo = chequeLeaves.getChqLChqNo();
			
			for(int i=0;i<vm_svmList.size();i++){
				VM_SubVMService vmsvm = vm_svmList.get(i);
				VehicleMstr vehicleMstr = vmsvm.getVehMstr();				
				tvNo = vehicleMstr.getVehNo()+" ("+tvNo+")";
			}
			
			int temp = chequeLeavesDAO.updateChqLeaves(chequeLeaves);
			
			
			// InterBranch Case Main Cash Stmt Start
			double amount = 0.0;
			int count = 0;
			for(int i=0;i<vm_svmList.size();i++){

				VM_SubVMService vmsvm = vm_svmList.get(i);
				VehicleMstr vehicleMstr = vmsvm.getVehMstr();				
				
				SubVehicleMstr subVehicleMstr = vmsvm.getsVehMstr();
				List<FaCarDivision> faCarList = vmsvm.getFaCarList();
					
				String vehBrFaCode =  vehicleMstr.getBranch().getBranchFaCode();
				amount = amount + subVehicleMstr.getSvmTotAmt();
				/*if(brFaCode.equalsIgnoreCase(vehBrFaCode)){
					System.out.println("************** "+i);
				}else{
					amount = amount + subVehicleMstr.getSvmTotAmt();
					count = count + 1;
				}*/	
			}
			
			System.out.println(count+" vehicle no are from other branches--");
			CashStmt mainCashStmt = new CashStmt();
			
			//if(count > 0){
				Map<String,Object> newMap = new HashMap<String,Object>();
				
				mainCashStmt.setbCode(currentUser.getUserBranchCode());
				mainCashStmt.setUserCode(currentUser.getUserCode());
				mainCashStmt.setCsDescription(voucherService.getDesc());
				mainCashStmt.setCsDrCr('C');
				mainCashStmt.setCsAmt(amount - tdsAmt);
				mainCashStmt.setCsType(voucherService.getVoucherType());
				mainCashStmt.setCsVouchType("bank");
				mainCashStmt.setCsChequeType(chequeType);
				mainCashStmt.setCsPayTo(voucherService.getPayTo());
				mainCashStmt.setCsFaCode(voucherService.getBankCode());
				mainCashStmt.setCsTvNo(tvNo);				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				mainCashStmt.setCsDt(sqlDate);
				mainCashStmt.setPayMode(payMode);
				
				newMap = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), mainCashStmt);
				voucherNo = (int) newMap.get("vhNo");
			//}
			
			// InterBranch Case Main Cash Stmt End
			
			if(!vm_svmList.isEmpty()){
				
				for(int i=0;i<vm_svmList.size();i++){
					Map<String,Object> newMap11 = new HashMap<String,Object>();
					VM_SubVMService vmsvm = vm_svmList.get(i);
					VehicleMstr vehicleMstr = vmsvm.getVehMstr();
					SubVehicleMstr subVehicleMstr = vmsvm.getsVehMstr();
					List<FaCarDivision> faCarList = vmsvm.getFaCarList();
					
					String vehBrFaCode =  vehicleMstr.getBranch().getBranchFaCode();
					
					if(brFaCode.equalsIgnoreCase(vehBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(subVehicleMstr.getSvmTotAmt());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsChequeType(chequeType);
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);						
						if(vehicleMstr.getVehType() == 'C'){
							cashStmt.setCsFaCode(carFaCode);
						}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
							cashStmt.setCsFaCode(smFaCode);
						}else{
							
						}						
						cashStmt.setCsVouchNo(voucherNo);						
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);						
						
						newMap11 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt);
						voucherNo = (int) newMap11.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt.getCsId());
						
						subVehicleMstr.setbCode(currentUser.getUserBranchCode());
						subVehicleMstr.setUserCode(currentUser.getUserCode());
						subVehicleMstr.setSvmCsId(cashStmt.getCsId());
						subVehicleMstr.setSvmTvNo(tvNo);
						subVehicleMstr.setSvmFaCode(carFaCode);
						
						vehicleMstrDAO.updateVMBySVM(vehicleMstr.getVehId() , subVehicleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt.getCsId() , subVehicleMstr.getSvmId());
						
						for(int k=0;k<faCarList.size();k++){
							FaCarDivision faCarDivision = faCarList.get(k);
							faCarDivision.setbCode(currentUser.getUserBranchCode());
							faCarDivision.setUserCode(currentUser.getUserCode());
							faCarDivision.setFcdSvmId(subVehicleMstr.getSvmId());
							
							faCarDivisionDAO.saveFaCarD(faCarDivision);
						}
						
						if(tdsCode != null && tdsAmt > 0){
							//Map<String,Object> newMap = new HashMap<String,Object>();
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("bank");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);							
							sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							newMap11 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
							voucherNo = (int) newMap11.get("vhNo");
						}
						/*CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('C');
						cashStmt1.setCsAmt(subVehicleMstr.getSvmTotAmt());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsChequeType(chequeType);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsFaCode(voucherService.getBankCode());
						
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
		
						newMap = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt1);
						voucherNo = (int) newMap.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt1.getCsId());*/
						
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(subVehicleMstr.getSvmTotAmt() - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsChequeType(chequeType);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsFaCode(vehicleMstr.getBranch().getBranchFaCode());						
						cashStmt1.setCsVouchNo(voucherNo);						
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						newMap11 =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt1);
						voucherNo = (int) newMap11.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt1.getCsId());
						
						subVehicleMstr.setbCode(currentUser.getUserBranchCode());
						subVehicleMstr.setUserCode(currentUser.getUserCode());
						subVehicleMstr.setSvmCsId(cashStmt1.getCsId());
						subVehicleMstr.setSvmTvNo(tvNo);
						subVehicleMstr.setSvmFaCode(carFaCode);
						
						vehicleMstrDAO.updateVMBySVM(vehicleMstr.getVehId() , subVehicleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt1.getCsId() , subVehicleMstr.getSvmId());
						
						for(int k=0;k<faCarList.size();k++){
							FaCarDivision faCarDivision = faCarList.get(k);
							faCarDivision.setbCode(currentUser.getUserBranchCode());
							faCarDivision.setUserCode(currentUser.getUserCode());
							faCarDivision.setFcdSvmId(subVehicleMstr.getSvmId());
							
							faCarDivisionDAO.saveFaCarD(faCarDivision);
						}
						
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(subVehicleMstr.getSvmTotAmt() - tdsAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsChequeType(chequeType);
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
						cashStmt2.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());						
						//cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt2);
						/*newMap =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt2);
						voucherNo = (int) newMap.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt2.getCsId());*/
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(subVehicleMstr.getSvmTotAmt());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsChequeType(chequeType);
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());
						if(vehicleMstr.getVehType() == 'C'){
							cashStmt3.setCsFaCode(carFaCode);
						}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
							cashStmt3.setCsFaCode(smFaCode);
						}else{
							
						}						
						//cashStmt3.setCsVouchNo(voucherNo);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt3);
						/*newMap =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt3);
						voucherNo = (int) newMap.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt3.getCsId());*/
						
						if(tdsCode != null && tdsAmt > 0){
							//Map<String,Object> newMap = new HashMap<String,Object>();
							
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());
							//csWTds.setCsVouchNo(voucherNo);							
							sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							cashStmtStatusDAO.saveCsTemp(csWTds);
							/*newMap11 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
							voucherNo = (int) newMap11.get("vhNo");*/
						}
						
					}
				}
				vehVoucherService.deleteAllVmAndSVM();
				map.put("vhNo",voucherNo);
			}else{
				map.put("tvNo","0");
				System.out.println("vehicle details are not avaliable");
			}
			
		}else{
			System.out.println("invalid bank code");
			map.put("vhNo",0);
		}
		
		
		

		return map;
	}	
	
	
	public Map<String,Object> saveVehByOnline(VoucherService voucherService , String carFaCode , String smFaCode){
		System.out.println("Enter into saveVehByCheque----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		char chequeType = voucherService.getChequeType();
		System.out.println("chequeType = "+chequeType);
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		String bankCode = voucherService.getBankCode();
		List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(bankCode);		
		
		String tvNo="";
		String payMode = "O";
		
		int voucherNo = 0;
		
		if(!bankMList.isEmpty()){
			
			String tdsCode = voucherService.getTdsCode();
			double tdsAmt = voucherService.getTdsAmt();
					
			BankMstr bankMstr = bankMList.get(0);
			List<VM_SubVMService> vm_svmList = vehVoucherService.getAllVmAndSVm();
			double totAmt = 0.0;
			if(!vm_svmList.isEmpty()){
				for(int i=0;i<vm_svmList.size();i++){
					totAmt = totAmt + vm_svmList.get(i).getsVehMstr().getSvmTotAmt();
				}
			}
			
			bankMstrDAO.decBankBal(bankMstr.getBnkId(), totAmt - tdsAmt);
			
			
			// InterBranch Case Main Cash Stmt Start
			double amount = 0.0;
			int count = 0;
			for(int i=0;i<vm_svmList.size();i++){
				VM_SubVMService vmsvm = vm_svmList.get(i);
				VehicleMstr vehicleMstr = vmsvm.getVehMstr();
				SubVehicleMstr subVehicleMstr = vmsvm.getsVehMstr();
				List<FaCarDivision> faCarList = vmsvm.getFaCarList();
				tvNo = vehicleMstr.getVehNo();	
				String vehBrFaCode =  vehicleMstr.getBranch().getBranchFaCode();
				amount = amount + subVehicleMstr.getSvmTotAmt();
			}
			
			System.out.println(count+" vehicle no are from other branches--");
			CashStmt mainCashStmt = new CashStmt();
			
			
				Map<String,Object> newMap = new HashMap<String,Object>();
				
				mainCashStmt.setbCode(currentUser.getUserBranchCode());
				mainCashStmt.setUserCode(currentUser.getUserCode());
				mainCashStmt.setCsDescription(voucherService.getDesc());
				mainCashStmt.setCsDrCr('C');
				mainCashStmt.setCsAmt(amount - tdsAmt);
				mainCashStmt.setCsType(voucherService.getVoucherType());
				mainCashStmt.setCsVouchType("bank");
				mainCashStmt.setCsChequeType(chequeType);
				mainCashStmt.setCsPayTo(voucherService.getPayTo());
				mainCashStmt.setCsFaCode(voucherService.getBankCode());
				mainCashStmt.setCsTvNo(tvNo);				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				mainCashStmt.setCsDt(sqlDate);
				mainCashStmt.setPayMode(payMode);
				
				newMap = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), mainCashStmt);
				voucherNo = (int) newMap.get("vhNo");
			
			
			// InterBranch Case Main Cash Stmt End
			
			if(!vm_svmList.isEmpty()){
				
				for(int i=0;i<vm_svmList.size();i++){
					Map<String,Object> newMap11 = new HashMap<String,Object>();
					VM_SubVMService vmsvm = vm_svmList.get(i);
					VehicleMstr vehicleMstr = vmsvm.getVehMstr();
					SubVehicleMstr subVehicleMstr = vmsvm.getsVehMstr();
					List<FaCarDivision> faCarList = vmsvm.getFaCarList();
					
					String vehBrFaCode =  vehicleMstr.getBranch().getBranchFaCode();
					
					if(brFaCode.equalsIgnoreCase(vehBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(subVehicleMstr.getSvmTotAmt());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsChequeType(chequeType);
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);						
						if(vehicleMstr.getVehType() == 'C'){
							cashStmt.setCsFaCode(carFaCode);
						}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
							cashStmt.setCsFaCode(smFaCode);
						}else{
							
						}						
						cashStmt.setCsVouchNo(voucherNo);						
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						newMap11 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt);
						voucherNo = (int) newMap11.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt.getCsId());
						
						subVehicleMstr.setbCode(currentUser.getUserBranchCode());
						subVehicleMstr.setUserCode(currentUser.getUserCode());
						subVehicleMstr.setSvmCsId(cashStmt.getCsId());
						subVehicleMstr.setSvmTvNo(tvNo);
						subVehicleMstr.setSvmFaCode(carFaCode);
						
						vehicleMstrDAO.updateVMBySVM(vehicleMstr.getVehId() , subVehicleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt.getCsId() , subVehicleMstr.getSvmId());
						
						for(int k=0;k<faCarList.size();k++){
							FaCarDivision faCarDivision = faCarList.get(k);
							faCarDivision.setbCode(currentUser.getUserBranchCode());
							faCarDivision.setUserCode(currentUser.getUserCode());
							faCarDivision.setFcdSvmId(subVehicleMstr.getSvmId());
							
							faCarDivisionDAO.saveFaCarD(faCarDivision);
						}
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("bank");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);							
							sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							newMap11 = cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), csWTds);
							voucherNo = (int) newMap11.get("vhNo");
						}
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(subVehicleMstr.getSvmTotAmt() - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsChequeType(chequeType);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsFaCode(vehicleMstr.getBranch().getBranchFaCode());						
						cashStmt1.setCsVouchNo(voucherNo);						
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						newMap11 =  cashStmtStatusDAO.updateCSSForV(cashStmtStatus.getCssId(), cashStmt1);
						voucherNo = (int) newMap11.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt1.getCsId());
						
						subVehicleMstr.setbCode(currentUser.getUserBranchCode());
						subVehicleMstr.setUserCode(currentUser.getUserCode());
						subVehicleMstr.setSvmCsId(cashStmt1.getCsId());
						subVehicleMstr.setSvmTvNo(tvNo);
						subVehicleMstr.setSvmFaCode(carFaCode);
						
						vehicleMstrDAO.updateVMBySVM(vehicleMstr.getVehId() , subVehicleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt1.getCsId() , subVehicleMstr.getSvmId());
						
						for(int k=0;k<faCarList.size();k++){
							FaCarDivision faCarDivision = faCarList.get(k);
							faCarDivision.setbCode(currentUser.getUserBranchCode());
							faCarDivision.setUserCode(currentUser.getUserCode());
							faCarDivision.setFcdSvmId(subVehicleMstr.getSvmId());
							
							faCarDivisionDAO.saveFaCarD(faCarDivision);
						}
						
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(subVehicleMstr.getSvmTotAmt() - tdsAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsChequeType(chequeType);
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
						cashStmt2.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());						
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt2);						
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(subVehicleMstr.getSvmTotAmt());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsChequeType(chequeType);
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());
						if(vehicleMstr.getVehType() == 'C'){
							cashStmt3.setCsFaCode(carFaCode);
						}else if(vehicleMstr.getVehType() == 'M' || vehicleMstr.getVehType() == 'S'){
							cashStmt3.setCsFaCode(smFaCode);
						}else{
							
						}						
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt3);
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsBrhFaCode(vehicleMstr.getBranch().getBranchFaCode());							
							sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							cashStmtStatusDAO.saveCsTemp(csWTds);
						}
						
					}
				}
				vehVoucherService.deleteAllVmAndSVM();
				map.put("vhNo",voucherNo);
			}else{
				map.put("tvNo","0");
				System.out.println("vehicle details are not avaliable");
			}
			
		}else{
			System.out.println("invalid bank code");
			map.put("vhNo",0);
		}
		
		
		

		return map;
	}	
	
	
	//TODO Back Data
	@RequestMapping(value = "/getVDetFrVehVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrVehVBack() {  
		System.out.println("Enter into getVDetFrVehVBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
}
