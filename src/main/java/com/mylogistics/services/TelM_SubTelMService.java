package com.mylogistics.services;

import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.TelephoneMstr;

public class TelM_SubTelMService {

	private TelephoneMstr tMstr;
	
	private SubTelephoneMstr stMstr;

	public TelephoneMstr gettMstr() {
		return tMstr;
	}

	public void settMstr(TelephoneMstr tMstr) {
		this.tMstr = tMstr;
	}

	public SubTelephoneMstr getStMstr() {
		return stMstr;
	}

	public void setStMstr(SubTelephoneMstr stMstr) {
		this.stMstr = stMstr;
	}

	
}
