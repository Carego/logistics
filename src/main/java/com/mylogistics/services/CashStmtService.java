package com.mylogistics.services;

import java.sql.Date;
import java.util.List;

import com.mylogistics.model.CashStmt;

public class CashStmtService {

	private List<CashStmt> cashStmtList;;
	
	private CashStmt cashStmt;
	
	private Date cssDt;
	
	private int csVouchNo;
	
	private String bCode;

	public List<CashStmt> getCashStmtList() {
		return cashStmtList;
	}

	public void setCashStmtList(List<CashStmt> cashStmtList) {
		this.cashStmtList = cashStmtList;
	}

	public CashStmt getCashStmt() {
		return cashStmt;
	}

	public void setCashStmt(CashStmt cashStmt) {
		this.cashStmt = cashStmt;
	}

	public Date getCssDt() {
		return cssDt;
	}

	public void setCssDt(Date cssDt) {
		this.cssDt = cssDt;
	}

	public int getCsVouchNo() {
		return csVouchNo;
	}

	public void setCsVouchNo(int csVouchNo) {
		this.csVouchNo = csVouchNo;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
}
