package com.mylogistics.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class CnDetailService {

	private String cnmtNo;
	private String billNo;
	private double blAmt;
	private Date cnmtDt;
	private String custCode;
	private String brhCode;
	private String consignor;
	private String consignee;
	private double actWt;
	private double wtChrg;
	private int noOfPckg;
	private List<Map<String,Object>> invList = new ArrayList<>();
	private double vog;
	private String frStn;
	private String toStn;
	private int tnsTime;
	private Date expDtOfDly;
	
	private String chlnNo;
	private Date chlnDt;
	private double lryHire;
	private float anyOthChrg;
	private double totHireChrg;
	private double adv;
	private double bal;
	private String brkName;
	private List<String> brkCntList = new ArrayList<>();
	private String ownName;
	private String ownAdd;
	private List<String> ownCntList = new ArrayList<>();
	private String ownPan;
	private String driverName;
	private String drvCntNo;
	private String blngParty;
	private double prMtRt;
	private double prMtCost;
	private double cnmtFreight;
	
	public String getCnmtNo() {
		return cnmtNo;
	}
	public void setCnmtNo(String cnmtNo) {
		this.cnmtNo = cnmtNo;
	}
	public Date getCnmtDt() {
		return cnmtDt;
	}
	public void setCnmtDt(Date cnmtDt) {
		this.cnmtDt = cnmtDt;
	}
	public String getConsignor() {
		return consignor;
	}
	public void setConsignor(String consignor) {
		this.consignor = consignor;
	}
	public String getConsignee() {
		return consignee;
	}
	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}
	public double getActWt() {
		return actWt;
	}
	public void setActWt(double actWt) {
		this.actWt = actWt;
	}
	public double getWtChrg() {
		return wtChrg;
	}
	public void setWtChrg(double wtChrg) {
		this.wtChrg = wtChrg;
	}
	public int getNoOfPckg() {
		return noOfPckg;
	}
	public void setNoOfPckg(int noOfPckg) {
		this.noOfPckg = noOfPckg;
	}
	public List<Map<String, Object>> getInvList() {
		return invList;
	}
	public void setInvList(List<Map<String, Object>> invList) {
		this.invList = invList;
	}
	public double getVog() {
		return vog;
	}
	public void setVog(double vog) {
		this.vog = vog;
	}
	public String getFrStn() {
		return frStn;
	}
	public void setFrStn(String frStn) {
		this.frStn = frStn;
	}
	public String getToStn() {
		return toStn;
	}
	public void setToStn(String toStn) {
		this.toStn = toStn;
	}
	public int getTnsTime() {
		return tnsTime;
	}
	public void setTnsTime(int tnsTime) {
		this.tnsTime = tnsTime;
	}
	public Date getExpDtOfDly() {
		return expDtOfDly;
	}
	public void setExpDtOfDly(Date expDtOfDly) {
		this.expDtOfDly = expDtOfDly;
	}
	public String getChlnNo() {
		return chlnNo;
	}
	public void setChlnNo(String chlnNo) {
		this.chlnNo = chlnNo;
	}
	public Date getChlnDt() {
		return chlnDt;
	}
	public void setChlnDt(Date chlnDt) {
		this.chlnDt = chlnDt;
	}
	public double getLryHire() {
		return lryHire;
	}
	public void setLryHire(double lryHire) {
		this.lryHire = lryHire;
	}

	public double getTotHireChrg() {
		return totHireChrg;
	}
	public void setTotHireChrg(double totHireChrg) {
		this.totHireChrg = totHireChrg;
	}
	public double getAdv() {
		return adv;
	}
	public void setAdv(double adv) {
		this.adv = adv;
	}
	public double getBal() {
		return bal;
	}
	public void setBal(double bal) {
		this.bal = bal;
	}
	public String getBrkName() {
		return brkName;
	}
	public void setBrkName(String brkName) {
		this.brkName = brkName;
	}
	
	public String getOwnName() {
		return ownName;
	}
	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}
	public String getOwnAdd() {
		return ownAdd;
	}
	public void setOwnAdd(String ownAdd) {
		this.ownAdd = ownAdd;
	}
	
	public String getOwnPan() {
		return ownPan;
	}
	public void setOwnPan(String ownPan) {
		this.ownPan = ownPan;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDrvCntNo() {
		return drvCntNo;
	}
	public void setDrvCntNo(String drvCntNo) {
		this.drvCntNo = drvCntNo;
	}
	public String getBlngParty() {
		return blngParty;
	}
	public void setBlngParty(String blngParty) {
		this.blngParty = blngParty;
	}
	
	
	public List<String> getBrkCntList() {
		return brkCntList;
	}
	public void setBrkCntList(List<String> brkCntList) {
		this.brkCntList = brkCntList;
	}
	public List<String> getOwnCntList() {
		return ownCntList;
	}
	public void setOwnCntList(List<String> ownCntList) {
		this.ownCntList = ownCntList;
	}
	public float getAnyOthChrg() {
		return anyOthChrg;
	}
	public void setAnyOthChrg(float anyOthChrg) {
		this.anyOthChrg = anyOthChrg;
	}
	public double getPrMtCost() {
		return prMtCost;
	}
	public void setPrMtCost(double prMtCost) {
		this.prMtCost = prMtCost;
	}
	public double getPrMtRt() {
		return prMtRt;
	}
	public void setPrMtRt(double prMtRt) {
		this.prMtRt = prMtRt;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getBrhCode() {
		return brhCode;
	}
	public void setBrhCode(String brhCode) {
		this.brhCode = brhCode;
	}
	public double getCnmtFreight() {
		return cnmtFreight;
	}
	public void setCnmtFreight(double cnmtFreight) {
		this.cnmtFreight = cnmtFreight;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public double getBlAmt() {
		return blAmt;
	}
	public void setBlAmt(double blAmt) {
		this.blAmt = blAmt;
	}
	
	
	
}
