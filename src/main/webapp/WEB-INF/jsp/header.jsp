
<nav class="navbar navbar-inverse navbar-fixed-top" style="background-image: url('resources/img/header.jpg');  background-size:cover;">
	<div style="background: rgba(0, 0, 0, 0.2)">
      <div class="container-fluid" >
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand"  href=""><b>{{currentBranch}}</b></a>
          <a class="navbar-brand"  href=""><b>{{currentEmpName}}</b></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" >
          <div ng-if="operatorLogin">

          <ul class="nav navbar-nav navbar-right">
            <li><a href="#/operator">Dashboard</a></li>
            <li><a href="#/settings">Settings</a></li>
            <li><a href="#/profile">Profile</a></li>
            <li><a href="#help">Help</a></li>
            <li><a href="" ng-click="logout()">Logout</a></li>
          </ul>
          </div>
          <div ng-if="adminLogin">
          <ul class="nav navbar-nav navbar-right">
			<li><a href="#/admin">Dashboard</a></li>
            <li><a href="#/settings">Settings</a></li>
            <li><a href="#/profile">Profile</a></li>
            <li><a href="#help">Help</a></li>
            <li><a href="" ng-click="logout()">Logout</a></li>
          </ul>
          </div>
          <div ng-if="superAdminLogin">
     	       <ul class="nav navbar-nav navbar-right">
				<li><a href="#/superAdmin">Dashboard</a></li>
            	<li><a href="#/settings">Settings</a></li>
            	<li><a href="#/profile">Profile</a></li>
            	<li><a href="#help">Help</a></li>
            	<li><a href="" ng-click="logout()">Logout</a></li>
          </ul>
          </div>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
     </div>
</nav>


<!-- content visible if login true -->
<!-- <div ng-if="operatorLogin || adminLogin || superAdminLogin">
<header>
 --><!-- <div class="navbar-fixed">
<nav>
    <div class="nav-wrapper">
    </div> 
      <a href="#" class="brand-logo" style="text-decoration:none; padding-left:10px;">My Logistics</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a >3</a></li>
        <li><a >2</a></li>
        <li><a >1</a></li>
      </ul>

</nav>

</div>
 --> 
 <!-- <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a class="brand-logo" style="text-decoration:none; padding-left:10px;">My Logistics</a>
      </div>
    </nav>
  </div>
</header>
</div>
 -->