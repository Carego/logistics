<div ng-show="operatorLogin || superAdminLogin">

	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:12px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #26A69A;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:12px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>

	<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>

	<div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<form name="osRptForm" ng-submit="submitOsRpt(osRptForm)">
			
			<div class="row">
							
			<!-- 	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly>
		       	<label for="code">Branch</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="cust.custName" ng-click="openCustDB()" readonly >
		       	<label for="code">Customer</label>	
		       	</div> -->
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="date" id="upToDtId" name ="upToDtName" ng-model="upToDt" required="required">
		       	<label for="code">Date</label>	
		       	</div>
		       	
		    </div>
		<!--  <div class="row">
					 <div class="col s4 input-field">
		       		<td><input type="radio" ng-model="orderWise" value="blBillDt"  ng-change="clickOrderWise(orderWise)"/></td>
		       	<label for="code">Date wise</label>	
		       	</div>	
				
				<div class="col s4 input-field">
		       		<td><input type="radio" ng-model="orderWise" value="blBrhId"  ng-change="clickOrderWise(orderWise)"/></td>
		       	<label for="code">Branch wise</label>	
		       	</div>		
		       	
		       </div>-->
			<div class="row">
   			 	<div class="col s12 center">
   			 		<input type="submit" id="submitId" value="Submit">
   			 		<input type="button" id="clearBtnId" value="CLEAR" ng-click="clearAll()">
   			 		<input type ="button" id="moreCustNameId" value="customerGroup" ng-click="moreOpenCustDB()" >
   			 	</div>
      		</div>
		</form>
		<div class="col s12 center" ng-show="printXlsFlag">
			<form method="post" action="getRelStockRptXLSX" enctype="multipart/form-data">
				<input type="submit" id="printXlsId" value="Print XLS" disabled="disabled">
		  	</form>
		</div>
		
	</div>
	
<!-- 	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div> -->
	
<!-- 	<div id ="custDB" ng-hide="custDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Customer Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="cust in custList | filter:filterTextbox2">
		 	  <td><input type="radio" name="cust" value="{{ cust }}" ng-click="saveCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div> -->

<div id ="custGroupDB" ng-hide="custGroupDBFlag">
		<input type="text" name="moreFilterTextbox" ng-model="custGroupFilterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> groupName </th>
 	  	  	  <th> GroupId </th>
 	  	  </tr>
		  <tr ng-repeat="custGroup in allCustGroupList | filter:custGroupFilterTextbox">
		 	  <td><input type="radio" name="custGroup" value={{custGroup}}    ng-click="saveCustGroup(custGroup)"></td>
              <td>{{custGroup.groupName}}</td>
              <td>{{custGroup.groupId}}</td>
          </tr>
      </table> 
	</div>


</div>
	