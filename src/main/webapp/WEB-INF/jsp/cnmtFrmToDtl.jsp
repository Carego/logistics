<div ng-if="operatorLogin || superAdminLogin">
	<title>Cnmt List</title>
	<div>
	<div>
	
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="CnmtForm" ng-submit="getCnmtList()">
		<div>
		
		<div class="row">
		<div class="input-field col s4">
			<input class="validate" type ="text" name ="customerName" ng-model="customerName" readonly ng-click="OpenCustomerCodeDB()" required>
       		 <input class="validate" type ="hidden" name ="custCode" ng-model="custCode" >
				 <label>Customer Name</label>
			</div>
			
			<div class="input-field col s4">
			 	<input class="validate" type ="text" name ="fromStationName" ng-model="frmStationName" readonly ng-click="OpenCnmtFromStationDB()" ng-required="true">
       			 	<input class="validate" type ="hidden" name ="cnmtFromSt" ng-model="frmStnCode" >
				 <label>From Station</label>
			</div>
			
			<div class="input-field col s4">
				<input class="validate" type ="text" name ="toStationName" ng-model="toStationName" readonly ng-click="OpenCnmtToStationDB()" ng-required="true">
       			 <input class="validate" type ="hidden" name ="cnmtToSt" ng-model="toStnCode" >
				 <label>To Station</label>
			</div>
		
		</div>
		</div>
		<div class="row">
				
		<div class="input-field col s4">
					<input type="date" name="frmDtName" ng-model="obj.frmDt"  ng-required=true>
					<label>From Date</label>
		</div>
		<div class="input-field col s4">
					<input type="date" name="toDtName" ng-model="obj.toDt" ng-required=true>
					<label>To Date</label>
		</div>
	</div>
		
		<div class="row">
			<div class="input-field col s12 center">
				<input  type="submit" value="Submit" id="submitId">
			</div>

		</div>
		<div class="row">
			<div class="input-field col s12 center">
				<input  type="button" value="Clear" id="clearId" ng-click="clear()">
			</div>

		</div>
	</form>
	</div>
	
	<div ng-show="printXLSFlg">
			  	
			  	
			  	<form method="post" action="getCnmtReportXLSX" enctype="multipart/form-data">
					<input type="submit" id="printXlsId" value="Print XLS" >
			  	</form>
	</div>
	
	<div id ="customerCodeDB" ng-hide="CustomerCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Code</th>
 	  	  	  <th>Customer Name</th>
 	  	  </tr>
		  <tr ng-repeat="cust in customerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="custName"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustCode(cust)"></td>
              <td>{{cust.custCode}}</td>
              <td>{{cust.custName}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="frmStnCodeDB" ng-hide="frmStnCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Station Code</th>
 	  	  	  <th>Station Name</th>
 	  	  </tr>
		  <tr ng-repeat="stn in stationList | filter:filterTextbox">
		 	  <td><input type="radio"  name="frmStnName"   value="{{ stn }}" ng-model="frmStnCode" ng-click="saveFrmStnCode(stn,$index)"></td>
              <td>{{stn[0]}}</td>
              <td>{{stn[1]}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="toStnCodeDB" ng-hide="toStnCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Station Code</th>
 	  	  	  <th>Station Name</th>
 	  	  </tr>
		  <tr ng-repeat="stn in stationList | filter:filterTextbox">
		 	  <td><input type="radio"  name="toStnName"   value="{{ stn }}" ng-model="toStnCode" ng-click="saveToStnCode(stn,$index)"></td>
              <td>{{stn[0]}}</td>
              <td>{{stn[1]}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="cnListDB" ng-hide="cnListDBFlag">
		 <!--  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search"> -->
 	  <table>
 	  <caption class="coltag tblrow">CNMT LIST</caption>
 	  	  <tr>
 	  	      <th class="colclr">SrNo</th>
 	  	  	  <th class="colclr">Bcode</th>
 	  	  	  <th class="colclr">Cnmt No</th>
 	  	  	  <th class="colclr">Date</th>
 	  	  	  <th class="colclr">Frm Stn</th>
 	  	  	  <th class="colclr">To Stn</th>
 	  	  	  <th class="colclr">Pkg</th>
 	  	  	  <th class="colclr">Weight</th>
 	  	  	  <th class="colclr">Freight</th>
 	  	  </tr>
		  <tr ng-repeat="cn in cnList ">
              <th class="rowcel">{{$index+1}}</th>
              <td class="rowcel">{{cn[0]}}</td>
              <td class="rowcel">{{cn[1]}}</td>
              <td class="rowcel">{{cn[2]}}</td>
              <td class="rowcel">{{cn[3]}}</td>
              <td class="rowcel">{{cn[4]}}</td>
              <td class="rowcel">{{cn[5]}}</td>
              <td class="rowcel">{{cn[6]}}</td>
              <td class="rowcel">{{cn[7]}}</td>
          </tr>
      </table> 
	</div>
	
	
	
</div>