package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.CustomerRights;

public interface CustomerRightsDAO {
	
	public int saveCustRights(CustomerRights customerRgt);
	
	//Satarupa: get the list of all customerrights where isAllow=yes.
	public List<CustomerRights> getCustRightsIsAllow();
	
	//Satarupa: updating the isAllow  of customerrights table field to "no".
	public int updateCustRightsIsAllowNo(int contIdsInt[]);
}
