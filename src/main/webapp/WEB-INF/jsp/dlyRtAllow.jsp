<div ng-show="adminLogin || superAdminLogin">
	<div class="row">
		<form name="dlyRtForm" ng-submit="submitDlyRt(dlyRtForm)" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBrhDB()" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>

		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="cnmtId" name ="cnmtName" ng-model="cnmt" ng-click="openCnmtDB()" ng-required="true" readonly>
		       			<label for="code">Cnmt Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
			    		<select name="isActiveName" id="isActiveId" ng-model="dlyRt" ng-init="dlyRt=false" ng-required="true">
							<option value = true>YES</option>
							<option value = false>NO</option>
						</select>
						<label>Daily Rate Allow</label>
					</div>
			
		    </div> 
		    
		    <div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
		    </div>
		  </form>
    </div>
    
    
    <div id ="brhId" ng-hide="brhFlag">
		 <input type="text" name="filterBrhbox" ng-model="filterBrhbox" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	 <!--  <th> Branch Code </th> -->
 	  	  	  
 	  	  	  <th> Branch Name </th>
 	  	  	  
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in brhList | filter:filterBrhbox">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranch(branch)"></td>
              <!-- <td>{{branch.branchCode}}</td> -->
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>
	
	
	<div id ="cnmtDBId" ng-hide="cnmtDBFlag">
		 <input type="text" name="filterCnmtbox" ng-model="filterCnmtbox" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	 <!--  <th> Branch Code </th> -->
 	  	  	  
 	  	  	  <th> CNMT Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cnmt in cnmtList | filter:filterCnmtbox">
		 	  <td><input type="radio"  name="cnmtName"  class="cnmtCls"  value="{{ cnmt }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt)"></td>
              <td>{{ cnmt }}</td>
          </tr>
      </table>
	</div>
    
    
</div>    
