package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.TelephoneMstrDAO;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.TelephoneMstr;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.TPMstrService;

@Controller
public class PhoneAllotCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private TelephoneMstrDAO telephoneMstrDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/getBrListFrPA" , method = RequestMethod.POST)  
	public @ResponseBody Object getBrListFrPA() {  
		System.out.println("Enter into getBrListFrPA---->");
		Map<String, Object> map = new HashMap<>();	
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		if(!branchList.isEmpty()){
			map.put("brList",branchList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getEmpListFrPA" , method = RequestMethod.POST)  
	public @ResponseBody Object getEmpListFrPA() {  
		System.out.println("Enter into getEmpListFrPA---->");
		Map<String, Object> map = new HashMap<>();	
		List<Employee> empList = employeeDAO.getAllActiveEmployees();
		if(!empList.isEmpty()){
			map.put("empList",empList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/saveTpmsm" , method = RequestMethod.POST)  
	public @ResponseBody Object saveTpmsm(@RequestBody TPMstrService tpMstrService) {  
		System.out.println("Enter into saveTpmsm---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		TelephoneMstr telephoneMstr = tpMstrService.getTelephoneMstr();
		Branch branch = tpMstrService.getBranch();
		Employee employee = tpMstrService.getEmployee();
		Address address = tpMstrService.getAddress();
		if(address != null){
			address.setAddType("Other Address");
			int id = addressDAO.saveAddress(address);
			if(id > 0){
				telephoneMstr.setAddress(address);
			}
		}
		telephoneMstr.setbCode(currentUser.getUserBranchCode());
		telephoneMstr.setUserCode(currentUser.getUserCode());
		int temp = telephoneMstrDAO.saveTelephoneMstr(branch, employee, telephoneMstr);
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
}
