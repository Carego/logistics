package com.mylogistics.DAO;

import java.sql.Blob;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.State;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.services.Cnmt_ChallanService;

public interface ChallanDAO {
	
	//TODO
	//public Map<String,Object> getTransitDay(String contractCode);
	
	  public int savePanIssueState(State state);
	  
	  //save new challan
	 public int saveChallan(Challan challan);
	 
	 public int saveChallan(Challan challan,Session session);
	 
	 public int saveChlnAndChd(Challan challan , ChallanDetail challanDetail);
	 
	 public int saveChlnAndChd(Challan challan , ChallanDetail challanDetail,Session session);
	 
	 //get challan by chlnId
	 public List<Challan> getChallanById(int chlnId);
	 
	//Sanjana: retrieves the list of challan whose isView is false
		public List<Challan> getChallanisViewFalse(String branchCode);
			
	//Sanjana: updates the challan isView to true
		public int updateChallanisViewTrue(String code[]);
			
	//Sanjana: retrieves challan from database on the basis of entered challan code
	   public List<Challan> getChallanList(String chlnCode);
	
	//Sanjana: updates the challan
		public int updateChallan(Challan challan);
		
	//Rohit: get chaalan image by using challan code
		public Blob getChallanImageByCode(String chlnCode);
		
	/*	public List<Challan> getChallan(String chlnCode);*/
		
		public Map<String,Object> getChallanNChallanDet(String chlnCode);
		
		public List<String> getChallanCode();
		
		public List<String> getChlnCodeFrLHA();
		
		public List<String> getChlnCodeFrLHB();
		
		public List<String> getChlnCodeFrLHBTemp();
		
		public List<String> getChlnCodeFrLHSP();
		
		public List<Challan> getAllChallan();
		
		public List<Challan> getAllChallanCodes(String branchCode);
		
		public List<Challan> getAllChlnByBrh(String brhCode);
		
		public List<String> getChCodeByBrh(String brhCode);
		
		public List<Challan> getChlnFrSedrByBrh(String brhCode);
		
		public int checkLA(int chlnId);
		
		public int checkLB(int chlnId);
		
		public int checkLSP(int chlnId);
		
		public List<LhpvBal> getLBOfChln(int chlnId);
		
		public Map<String,Object> getChlnInfoById(int chlnId);
		
		public int saveChallanDetail(Map<String, String> chdService);
		
		public List<String> getChlnCodeNotInChd();
		
		public int updateChlnNChd(Challan challan, ChallanDetail challanDetail);
		
		public Map<String, Object> getCnmtByChln(String chlnCode);
		
		public int submitChlnNCnmt(Cnmt_ChallanService chlnNCnmtService);
		
		public Map<String, Object> verifyChlnForLhpvAdv(Map<String,String> chlnCode);
		
		//public Map<String, Object> verifyChlnForLhpvBal(String chlnCode);
		
		public Map<String, Object> verifyChlnForLhpvBal(Map map);
		
		public Map<String, Object> verifyChlnForLhpvSup(String chlnCode);

		Map<String, Object> verifyChlnForLhpvAdvRvrs(Map<String , String> chlnCheq);

		public int chlnDtlFrCncl(Map<String, String> chlnDtl);
		
		public Map<String, Object> verifyChlnForLhpvBalRvrs(Map<String,String> chlnData);
		
/*		public String getChlnCodeById(int chlnId);*/
		
		public List<Challan> getChallanByCode(String chlnCode);
		
		public Map<String,Object> getChallanForHoLbPay(String branchCode);

		public Map<String, Object> verifyChlnForLhpvBalHO(String chlnCode);
		
		public Map<String, Object> verifyChlnForLhpvBalOld(String chlnCode);
		
		public Map<String, Object> verifyChlnForLhpvBalCash(String chlnCode);
		
		public int getChlnId(String chlnCode);
		
		public Map<String, Object> getChlnDetFrAdvPay(String chlnCode);

		public void updateChallan(Challan challan, Session sassion);
		
		public Map<String, String> alwOldBalance(String chlnCode);
		
		public List<Challan> getChallanByCodeFrAr(String chlnCode);
}
