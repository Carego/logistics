package com.mylogistics.controller.bank;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.bank.AtmCardDAO;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.bank.AtmCardService;

@Controller
public class AtmCardMstrCntlr {
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private AtmCardDAO atmCardDAO;

	@RequestMapping(value = "/saveAtmCard", method = RequestMethod.POST)
	public @ResponseBody Object saveAtmCard(@RequestBody AtmCardService atmCardService) {
		System.out.println("Enter into saveAtmCard---->");
		Map<String, Object> map = new HashMap<>();

		System.out.println("card no: "+ atmCardService.getAtmCardMstr().getAtmCardNo());
		System.out.println("employee id: "+atmCardService.getEmpId());
		System.out.println("branch id: "+atmCardService.getBranchId());
		System.out.println("bank id: "+ atmCardService.getBankId());
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		int temp = atmCardDAO.saveAtmCard(atmCardService, currentUser);
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("msg", ConstantsValues.SUCCESS);
		} else if (temp == -2) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "ATM already exist");
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getAtmCardByNo", method = RequestMethod.POST)
	public @ResponseBody Object getAtmCardByNo(@RequestBody String atmCardNo){
		System.out.println("AtmCardMstrCntlr.getAtmCardByNo()");;
		Map<String, Object> map = new HashMap<>();
		
		AtmCardMstr atmCardMstr = atmCardDAO.getAtmCardByNo(atmCardNo);
		
		if (atmCardMstr != null) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("atmCardMstr", atmCardMstr);
		} else {
			map.put(ConstantsValues.RESULT, "Please enter valid ATM Card No");
		}
		
		return map;
	}
	
	@RequestMapping(value = "/transAtmToBr", method = RequestMethod.POST)
	public @ResponseBody Object transAtmToBr(@RequestBody Map<String, Object> transAtmToBrService){
		System.out.println("AtmCardMstrCntlr.transAtmToBr()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = atmCardDAO.transAtmToBr(transAtmToBrService);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/deactiveAtmCard", method=RequestMethod.POST)
	public @ResponseBody Object deactiveAtmCard(@RequestBody Integer atmCardId){
		System.out.println("AtmCardMstrCntlr.deactiveAtmCard()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = atmCardDAO.deactiveAtmCard(atmCardId);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/activeAtmCard", method=RequestMethod.POST)
	public @ResponseBody Object activeAtmCard(@RequestBody Integer atmCardId){
		System.out.println("AtmCardMstrCntlr.activeAtmCard()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = atmCardDAO.activeAtmCard(atmCardId);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
}
