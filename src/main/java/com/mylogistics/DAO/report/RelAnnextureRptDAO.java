package com.mylogistics.DAO.report;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public interface RelAnnextureRptDAO {
  public List<Map<String,Object>> submitRelAnnextureRpt(Date fromDt,Date toDt,String custGroupId);
}
