<div ng-if="operatorLogin || superAdminLogin">
	<title>Reissue Challan, Cnmt Or Arrival</title>
	<div>
	<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="reIssue()">
	
			<div class="input-field col s3">
   				<input type="text" id="bCodeId" name="bCodeName" ng-model="bCode" />
   				<label>Enter Branch Code </label>
 			</div>
 			<div class="input-field col s3">
   				<input type="text" id="codeId" name="codeName" ng-model="code" />
   				<label>Enter Code </label>
 			</div>
 			<div class="input-field col s3">
   				<input type="text" id="userCodeId" name="userCodeName" ng-model="userCode" />
   				<label>Enter User Code </label>
 			</div>
 			<div class="col s3 input-field">
				    		<select name="typeName" id="typeId" ng-model="type" ng-change="selectType()" ng-init="type = 'cash'" ng-required="true">
								<option value='chln'>Challan</option>
								<option value='cnmt'>Cnmt</option>
								<option value='sedr'>Sedr</option>
							</select>
							<label>Type</label>
					</div>
			
		 	<div class="input-field col s6 center">
		 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
		 	</div>
			
		</form>
	<div class="col s3"> &nbsp; </div>
</div>
	
	</div>
</div>