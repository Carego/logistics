package com.mylogistics.constants;

public class LhpvStatusCNTS {

	public static final String LS_ID = "lsId";
	
	public static final String LS_DT = "lsDt";
	
	public static final String LS_NO = "lsNo";
	
	public static final String LS_BACK = "lsBackDt";
	
	public static final String B_CODE = "bCode";
	
}
