package com.mylogistics.model.lhpv;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lhpvcashsmry")
public class LhpvCashSmry {

	@Id
	@Column(name="lcsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int lcsId;
	
	@Column(name="lcsBrhCode")
	private String lcsBrhCode;
	
	@Column(name="lcsBrkOwn")
	private String lcsBrkOwn;
	
	@Column(name="lcsChlnCode")
	private String lcsChlnCode;
	
	@Column(name="lcsPayAmt" , columnDefinition="double default 0")
	private double lcsPayAmt;
	
	@Column(name="lcsLhpvType")
	private String lcsLhpvType;
	
	@Column(name="lcsLhpvDt")
	private Date lcsLhpvDt;
	
	@Column(name="lcsCsEntr" , columnDefinition="boolean default false") 
	private boolean lcsCsEntr;
	
	@Column(name="lcsToday" , columnDefinition="boolean default false") 
	private boolean lcsToday;
	
	@Column(name="lcsAdvEntr" , columnDefinition="boolean default false") 
	private boolean lcsAdvEntr;
	
	@Column(name="lcsCashDiscR" , columnDefinition="double default 0")
	private double lcsCashDiscR;
	
	@Column(name="lcsMunsR" , columnDefinition="double default 0")
	private double lcsMunsR;
	
	@Column(name="lcsTdsR" , columnDefinition="double default 0")
	private double lcsTdsR;
	
	@Column(name="lcsWtShrtgCR" , columnDefinition="double default 0.0")
	private double lcsWtShrtgCR;
	
	@Column(name="lcsDrRcvrWtCR" , columnDefinition="double default 0.0")
	private double lcsDrRcvrWtCR;
	
	@Column(name="lcsLateDelCR" , columnDefinition="double default 0.0")
	private double lcsLateDelCR;
	
	@Column(name="lcsLateAckCR" , columnDefinition="double default 0.0")
	private double lcsLateAckCR;
	
	@Column(name="lcsOthExtKmP" , columnDefinition="double default 0.0")
	private double lcsOthExtKmP;
	
	@Column(name="lcsOthOvrHgtP" , columnDefinition="double default 0.0")
	private double lcsOthOvrHgtP;
	
	@Column(name="lcsOthPnltyP" , columnDefinition="double default 0.0") 
	private double lcsOthPnltyP;
	
	@Column(name="lcsOthMisctP" , columnDefinition="double default 0.0")
	private double lcsOthMiscP;
	
	@Column(name="lcsUnLoadingP" , columnDefinition="double default 0.0")
	private double lcsUnLoadingP;
	
	@Column(name="lcsUnpDetP" , columnDefinition="double default 0.0")
	private double lcsUnpDetP;
	
	@Column(name="laId" , columnDefinition="int default -1")
	private int laId;
	
	@Column(name="lbId" , columnDefinition="int default -1")
	private int lbId;
	
	@Column(name="lspId" , columnDefinition="int default -1")
	private int lspId;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getLcsId() {
		return lcsId;
	}

	public void setLcsId(int lcsId) {
		this.lcsId = lcsId;
	}

	public String getLcsBrkOwn() {
		return lcsBrkOwn;
	}

	public void setLcsBrkOwn(String lcsBrkOwn) {
		this.lcsBrkOwn = lcsBrkOwn;
	}

	public String getLcsChlnCode() {
		return lcsChlnCode;
	}

	public void setLcsChlnCode(String lcsChlnCode) {
		this.lcsChlnCode = lcsChlnCode;
	}

	public double getLcsPayAmt() {
		return lcsPayAmt;
	}

	public void setLcsPayAmt(double lcsPayAmt) {
		this.lcsPayAmt = lcsPayAmt;
	}

	public String getLcsLhpvType() {
		return lcsLhpvType;
	}

	public void setLcsLhpvType(String lcsLhpvType) {
		this.lcsLhpvType = lcsLhpvType;
	}

	public Date getLcsLhpvDt() {
		return lcsLhpvDt;
	}

	public void setLcsLhpvDt(Date lcsLhpvDt) {
		this.lcsLhpvDt = lcsLhpvDt;
	}

	public boolean isLcsCsEntr() {
		return lcsCsEntr;
	}

	public void setLcsCsEntr(boolean lcsCsEntr) {
		this.lcsCsEntr = lcsCsEntr;
	}

	public boolean isLcsToday() {
		return lcsToday;
	}

	public void setLcsToday(boolean lcsToday) {
		this.lcsToday = lcsToday;
	}

	public boolean isLcsAdvEntr() {
		return lcsAdvEntr;
	}

	public void setLcsAdvEntr(boolean lcsAdvEntr) {
		this.lcsAdvEntr = lcsAdvEntr;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getLcsBrhCode() {
		return lcsBrhCode;
	}

	public void setLcsBrhCode(String lcsBrhCode) {
		this.lcsBrhCode = lcsBrhCode;
	}

	public double getLcsCashDiscR() {
		return lcsCashDiscR;
	}

	public void setLcsCashDiscR(double lcsCashDiscR) {
		this.lcsCashDiscR = lcsCashDiscR;
	}

	public double getLcsMunsR() {
		return lcsMunsR;
	}

	public void setLcsMunsR(double lcsMunsR) {
		this.lcsMunsR = lcsMunsR;
	}

	public double getLcsTdsR() {
		return lcsTdsR;
	}

	public void setLcsTdsR(double lcsTdsR) {
		this.lcsTdsR = lcsTdsR;
	}

	public double getLcsWtShrtgCR() {
		return lcsWtShrtgCR;
	}

	public void setLcsWtShrtgCR(double lcsWtShrtgCR) {
		this.lcsWtShrtgCR = lcsWtShrtgCR;
	}

	public double getLcsDrRcvrWtCR() {
		return lcsDrRcvrWtCR;
	}

	public void setLcsDrRcvrWtCR(double lcsDrRcvrWtCR) {
		this.lcsDrRcvrWtCR = lcsDrRcvrWtCR;
	}

	public double getLcsLateDelCR() {
		return lcsLateDelCR;
	}

	public void setLcsLateDelCR(double lcsLateDelCR) {
		this.lcsLateDelCR = lcsLateDelCR;
	}

	public double getLcsLateAckCR() {
		return lcsLateAckCR;
	}

	public void setLcsLateAckCR(double lcsLateAckCR) {
		this.lcsLateAckCR = lcsLateAckCR;
	}

	public double getLcsOthExtKmP() {
		return lcsOthExtKmP;
	}

	public void setLcsOthExtKmP(double lcsOthExtKmP) {
		this.lcsOthExtKmP = lcsOthExtKmP;
	}

	public double getLcsOthOvrHgtP() {
		return lcsOthOvrHgtP;
	}

	public void setLcsOthOvrHgtP(double lcsOthOvrHgtP) {
		this.lcsOthOvrHgtP = lcsOthOvrHgtP;
	}

	public double getLcsOthPnltyP() {
		return lcsOthPnltyP;
	}

	public void setLcsOthPnltyP(double lcsOthPnltyP) {
		this.lcsOthPnltyP = lcsOthPnltyP;
	}

	public double getLcsOthMiscP() {
		return lcsOthMiscP;
	}

	public void setLcsOthMiscP(double lcsOthMiscP) {
		this.lcsOthMiscP = lcsOthMiscP;
	}

	public double getLcsUnLoadingP() {
		return lcsUnLoadingP;
	}

	public void setLcsUnLoadingP(double lcsUnLoadingP) {
		this.lcsUnLoadingP = lcsUnLoadingP;
	}

	public double getLcsUnpDetP() {
		return lcsUnpDetP;
	}

	public void setLcsUnpDetP(double lcsUnpDetP) {
		this.lcsUnpDetP = lcsUnpDetP;
	}

	public int getLaId() {
		return laId;
	}

	public void setLaId(int laId) {
		this.laId = laId;
	}

	public int getLbId() {
		return lbId;
	}

	public void setLbId(int lbId) {
		this.lbId = lbId;
	}

	public int getLspId() {
		return lspId;
	}

	public void setLspId(int lspId) {
		this.lspId = lspId;
	}

	
}
