'use strict';

var app = angular.module('application');

app.controller('ViewOwnerCntlr',['$scope','$location','$http','FileUploadService',
                                 function($scope,$location,$http,FileUploadService){
	
	$scope.ownerDetails=true;
	$scope.ownerCodesDBFlag=true;
	$scope.add = {};
	$scope.owner={};
	
	
	$(document).ready(function() {
	 $('#addPinC').keypress(function(e) {
         if (this.value.length == 6) {
             e.preventDefault();
         }
     });
	    
	    
	    $('#addressC').keypress(function(e) {
            if (this.value.length == 255) {
                e.preventDefault();
            }
        });
	    
	    
	    $('#addCityC').keypress(function(e) {
            if (this.value.length == 20) {
                e.preventDefault();
            }
        });
	    
	    
	    $('#addStateC').keypress(function(e) {
            if (this.value.length == 40) {
                e.preventDefault();
            }
        });
	    
	   
	    $('#cpMobile').keypress(function(e) {
            if (this.value.length ==10) {
                e.preventDefault();
            }
        });
	    
	    $('#cpPhone').keypress(function(e) {
            if (this.value.length ==15) {
                e.preventDefault();
            }
        });
	    
	    
	    $('#ownPanName').keypress(function(e) {
            if (this.value.length ==50) {
                e.preventDefault();
            }
        });
	    
	    
	 }); 
	
	 
	 $scope.ownerView = function(ownCode){
			console.log("After submit button");
				$scope.code= $( "#ownercode" ).val();
				$scope.add = {};
				$scope.owner={};
				if($scope.code===""){
					$scope.alertToast("Enter code");
				}else{
					$scope.ownerDetails  = false;
					var response = $http.post($scope.projectName+'/ownerDetFrVO',ownCode);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.owner = data.owner;
							$scope.getCurrentAddress();
							if($scope.owner.ownPhNoList==null){
								$scope.owner.ownPhNoList=[];
							}
							console.log("ownPhNo="+$scope.owner.ownPhNoList);
						}else{
							console.log(data.result);
						}
						
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}		
		} 
	
	
	 
	 $scope.getCurrentAddress = function(){
		   console.log("getCurrentAddress------>"+$scope.code);
		   var response = $http.post($scope.projectName+'/getCurrentAddress',$scope.code);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.add = data.list;
			  // $scope.getRegisterAddress();
			   }else{
				console.log(data);   
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	
	 
	 
	 $scope.EditOwnerSubmit = function(ViewOwnerForm){
			console.log("Enter into EditOwnerSubmit function");
			 console.log($scope.add.completeAdd);
			 console.log($scope.owner.ownName);
			 if(ViewOwnerForm.$invalid){
				 if(ViewOwnerForm.ownPanNo.$invalid){
					 $scope.alertToast("please Enter Valid PAN No.");
				 }else if(ViewOwnerForm.ownPhNo.$invalid){
					 $scope.alertToast("please Enter Valid Phone No.");
				 }else if(ViewOwnerForm.ownName.$invalid){
					 $scope.alertToast("please Enter Owner Name");
				 }else if(ViewOwnerForm.ownEmailId.$invalid){
					 $scope.alertToast("please Enter correct email-Id");
				 }else if(ViewOwnerForm.addressC.$invalid){
					 $scope.alertToast("please Enter correct address in current address");
				 }else if(ViewOwnerForm.addCityC.$invalid){
					 $scope.alertToast("please Enter correct city in current address");
				 }else if(ViewOwnerForm.addStateC.$invalid){
					 $scope.alertToast("please Enter correct state in current address");
				 }else if(ViewOwnerForm.addPinC.$invalid){
					 $scope.alertToast("please Enter correct pin in current address");
				 }
			 }
				 else{
					
					 $scope.viewOwnerFlag = true;
				    	$('div#viewOwnerDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						    }
							});
						$('div#viewOwnerDB').dialog('open');
				  } 
			 }
	 
	 $scope.back=function(){
		 console.log("Inside back");
		 $('div#viewOwnerDB').dialog('close');
		 $scope.viewOwnerFlag =false;
	 }
	 
	 $scope.saveOwner=function(){
	console.log("Inside saveOwner");	
	 var finalObject = {
			 "currentAddress" 		: $scope.add,
		 	 "owner"                : $scope.owner
		 };
	 
	 var response = $http.post($scope.projectName+'/submitEditOwner',finalObject);
	   response.success(function(data, status, headers, config){
		   if(data.result==="success"){
			   $scope.alertToast(data.result);
			   $scope.ownerDetails=true;
			   $scope.add = {};
				$scope.owner={};
			   $('input[name=ownerCode]').attr('checked',false);
			   $('div#viewOwnerDB').dialog('close');
			   
		   }else{
			   console.log(data.result);
			   }
	   });
	   response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	 }
	 
	 
	 $scope.showPanImage=function(own){
			
			var img={
					"imageType":"OWN",
					"id" : own.ownId,
					"ownBrkImgType": "PAN"
			};
			
			
			var response = $http.post($scope.projectName+'/isDownloadImg', img);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					var form = document.createElement("form");
			        form.method = "POST";
			        form.action = $scope.projectName + '/downloadImg';
			        form.target = "_blank";
			        document.body.appendChild(form);
			        form.submit();
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
			
			
		}
	 
	 $scope.showDecImage=function(own){
			
			var img={
					"imageType":"OWN",
					"id" : own.ownId,
					"ownBrkImgType": "DEC"
			};
			
			
			var response = $http.post($scope.projectName+'/isDownloadImg', img);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					var form = document.createElement("form");
			        form.method = "POST";
			        form.action = $scope.projectName + '/downloadImg';
			        form.target = "_blank";
			        document.body.appendChild(form);
			        form.submit();
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
			
			
		}
	 
	 
	 $scope.showBankImage=function(own){
			
			var img={
					"imageType":"OWN",
					"id" : own.ownId,
					"ownBrkImgType": "CHQ"
			};
			
			
			var response = $http.post($scope.projectName+'/isDownloadImg', img);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					var form = document.createElement("form");
			        form.method = "POST";
			        form.action = $scope.projectName + '/downloadImg';
			        form.target = "_blank";
			        document.body.appendChild(form);
			        form.submit();
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
			
			
		}
	 
	 
	 
	 $scope.uploadOwnPanImage = function(ownPanImage){
			console.log("enter into uploadOwnChqImage function");
			var file = ownPanImage;
			console.log("file name="+file.name);
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				
				
				  console.log('file is ' + JSON.stringify(file) + "ownId"+$scope.owner.ownId);
					var uploadUrl = $scope.projectName+"/uploadOwnPanImage";
					FileUploadService.uploadIdFileToUrl(file, uploadUrl,$scope.owner.ownId);
//					$scope.ownChqUpldFlag=false;
					
					console.log("file save on server");
//					$('div#ownChqUpldDBId').dialog('close');
				
			}
			
		}
	 
	 $scope.uploadOwnDecImage = function(ownDecImage){
			console.log("enter into uploadOwnDecImage function");
			var file = ownDecImage;
			console.log("file name="+file.name);
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				
				
				  console.log('file is ' + JSON.stringify(file) + "ownId"+$scope.owner.ownId);
					var uploadUrl = $scope.projectName+"/uploadOwnDecImage";
					FileUploadService.uploadIdFileToUrl(file, uploadUrl,$scope.owner.ownId);
//					$scope.ownChqUpldFlag=false;
					
					console.log("file save on server");
//					$('div#ownChqUpldDBId').dialog('close');
				
			}
			
		}
	 
	 
	 
		$scope.stateDBFlag=true;
		$scope.distDBFlag=true;
		$scope.cityDBFlag=true;
		$scope.stnDBFlag=true;
		
		
		$('#addPinId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		/*$('#phNoId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});*/
		
		$scope.getState=function(){
			console.log("getState()");
			 var response= $http.post($scope.projectName+'/getStateDetails');
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stateList=data.list;
						$scope.OpenStateDB();
					}else{
						$scope.alertToast("state not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching state");
				});
			
		}
		
		
		
		$scope.OpenStateDB = function(){
			$scope.stateDBFlag=false;
			$('div#stateDB').dialog({
				autoOpen: false,
				modal:true,
				title: "State",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stateDB').dialog('open');
		}

		$scope.saveStateCode = function(state){
			$scope.stateCode = state.stateCode;
			$scope.stateName = state.stateName;
			$scope.stateGST=state.stateGST;
			$('div#stateDB').dialog('close');
			$scope.stateDBFlag=true;
			
			
			 var response= $http.post($scope.projectName+'/getADistByStateCode',state.stateCode);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.distList=data.list;
						$scope.OpenDistDB();
					}else{
						$scope.alertToast("District not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching District");
				});
			
			
		}
		
		
		$scope.OpenDistDB = function(){
			$scope.distDBFlag=false;
			$('div#distDB').dialog({
				autoOpen: false,
				modal:true,
				title: "District",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#distDB').dialog('open');
		}
		
		
		
		
		$scope.saveDist = function(dist){
			$scope.distName = dist;
			$('div#distDB').dialog('close');
			$scope.distDBFlag=true;
			var map={
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getACityByDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.cityList=data.list;
						$scope.OpenCityDB();
					}else{
						$scope.alertToast("city not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching city");
				});
			
			
		}
		
		
		
		$scope.OpenCityDB = function(){
			$scope.cityDBFlag=false;
			$('div#cityDB').dialog({
				autoOpen: false,
				modal:true,
				title: "City",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#cityDB').dialog('open');
		}
		
		
		$scope.saveCity = function(city){
			$scope.cityName = city;
			$('div#cityDB').dialog('close');
			$scope.cityDBFlag=true;
			var map={
					"city":$scope.cityName,
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getAStnByCityDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stnList=data.list;
						$scope.OpenStnDB();
					}else{
						$scope.alertToast("station not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching station");
				});
			
			
		}
		
		
		$scope.OpenStnDB = function(){
			$scope.stnDBFlag=false;
			$('div#stnDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Station",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stnDB').dialog('open');
		}
		
		
		
		
		$scope.saveStn = function(stn){
						
			$scope.add.addPost=stn.stationName;
			$scope.add.addDist=stn.district;
			$scope.add.addState=$scope.stateName;
			$scope.add.addPin=stn.pinCode;
			$scope.add.addCity=stn.city;
			
			$('div#stnDB').dialog('close');
			console.log("Station"+stn.stationName);
			$scope.stnDBFlag=true;
		}
		
		
		
		
		$scope.getStnByPin=function(){
			
			if($scope.add.addPin.length < 6)
				return;
			
			var response= $http.post($scope.projectName+'/getAStnByPin',$scope.add.addPin);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					//console.log(data.list +""+data.stateName);
					$scope.stnList=data.list;
					$scope.stateName=data.stateName;
					//$scope.stateGST=data.stateGST;
					//console.log("$scope.stateGST"+$scope.stateGST);
					$scope.OpenStnDB();
				}else{
					$scope.alertToast("station not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching station");
			});
			
			
		}
		
		

	 
	 
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	 
	 
}]);