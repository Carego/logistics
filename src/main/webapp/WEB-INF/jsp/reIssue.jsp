<div ng-show="operatorLogin || superAdminLogin" >
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		
		<!-- Find By Branch Form -->
		<form name="stnryForm" id="stnryFormId" method="POST" ng-submit="stnryReIssue(stnryForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
				<div class="col s3 input-field">
		    		<select name="stnryType" id="stnryTypeId" ng-change="changeFlag()" ng-model="stnry.stationryType" ng-init="stnry.stationryTupe='cnmt'" required>
						<option value='cnmt'>CNMT</option>
						<option value='sedr'>SEDR</option>
						<option value='chln'>Challan</option>
						<option value='chq'>Cheque</option>
					</select>
					<label>Select stationary type</label>
				</div>
			
			
			
				<div class="col s3 input-field" ng-hide="chqFlag">		       		
		       		<input class="validate" type ="text" id="bnkId" name="bnkName" ng-model="stnry.bnkNo" >
		       		<label>Bank Code</label>
		       	</div>	
				
				<div class="col s3 input-field" ng-hide="chqFlag">		       		
		       		<input class="validate" type ="text" id="chqId" name="chqName" ng-model="stnry.chqNo" >
		       		<label>CHEQUE NO.</label>
		       	</div>	
				
				<div class="col s3 input-field" ng-hide="chlnFlag">		       		
		       		<input class="validate" type ="text" id="chlnId" name="chlnName" ng-model="stnry.chlnCode" >
		       		<label>Challan</label>
		       	</div>	
				
				<div class="col s3 input-field" ng-hide="cnmtFlag">		       		
		       		<input class="validate" type ="text" id="cnmtNo" name="cnmtNo" ng-model="stnry.cnmtNo">
		       		<label>CNMT No.</label>
		       	</div>	
		       	
		       	<div class="col s3 input-field" ng-hide="sedrFlag">		       		
		       		<input class="validate" type ="text" id="sedrId" name="sedrName" ng-model="stnry.sedrNo">
		       		<label>SEDR No.</label>
		       	</div>	
		       					       	
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" value="ReIssue" >
	       		</div>
			</div>
			
			
		</form>
		
	</div>	
	

</div>