package com.mylogistics.model;

import java.sql.Blob;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "brokerimg")
public class BrokerImg {

	
	@Id
	@Column(name="brkImgId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brkImgId;
	
	@Column(name="brkId" , columnDefinition = "int default -1")
	private int brkId;
	
	@Column(name="bCode")
	private String bCode;	
	
	@Column(name="userCode")
	private String userCode;	
	
	@Column(name="brkPanImgPath")
	private String brkPanImgPath;
	
	@Column(name="brkDecImgPath")
	private String brkDecImgPath;
	
	@Column(name="brkBnkDetImgPath")
	private String brkBnkDetImgPath;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getBrkImgId() {
		return brkImgId;
	}

	public void setBrkImgId(int brkImgId) {
		this.brkImgId = brkImgId;
	}

	public int getBrkId() {
		return brkId;
	}

	public void setBrkId(int brkId) {
		this.brkId = brkId;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getBrkPanImgPath() {
		return brkPanImgPath;
	}

	public void setBrkPanImgPath(String brkPanImgPath) {
		this.brkPanImgPath = brkPanImgPath;
	}

	public String getBrkDecImgPath() {
		return brkDecImgPath;
	}

	public void setBrkDecImgPath(String brkDecImgPath) {
		this.brkDecImgPath = brkDecImgPath;
	}

	public String getBrkBnkDetImgPath() {
		return brkBnkDetImgPath;
	}

	public void setBrkBnkDetImgPath(String brkBnkDetImgPath) {
		this.brkBnkDetImgPath = brkBnkDetImgPath;
	}
	
	
}
