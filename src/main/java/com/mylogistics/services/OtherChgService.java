package com.mylogistics.services;

import java.util.List;

import com.mylogistics.model.OtherCharge;

public interface OtherChgService {
	
	public List<List<OtherCharge>> getAllOtherCharge();

	public void addOtherCharge(List<OtherCharge> otChgList);
	
	/*public void deleteOtherCharge(OtherCharge otherCharge);*/
	 
	public void deleteAll();
	
	
}
