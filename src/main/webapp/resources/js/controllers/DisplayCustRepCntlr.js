'use strict';

var app = angular.module('application');

app.controller('DisplayCustRepCntlr',['$scope','$location','$http','$filter',
                                      function($scope,$location,$http,$filter){


	$scope.custRepCodeDBFlag = true;
	$scope.availableTags = [];
	$scope.isViewNo=false;
	$scope.showCustomerRep = true;
	$scope.custRepByModal=false;
	$scope.custRepByAuto=false;
	$scope.selection=[];
	$scope.custRepCodes =[];

	$scope.OpenCustRepCodeDB = function(){
		$scope.custRepCodeDBFlag = false;
		$('div#custRepCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custRepCodeDB').dialog('open');
	}

	$scope.enableModalTextBox = function(){
		$('#custRepByMId').removeAttr("disabled");
		$('#custRepByAId').attr("disabled","disabled");
		$('#ok').attr("disabled","disabled");
		$scope.custRepByA="";
	}

	$scope.enableAutoTextBox = function(){
		$('#custRepByAId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		$('#custRepByMId').attr("disabled","disabled");
		$scope.custRepByM="";
	}

	$scope.getCustRepIsViewNo = function(){
		console.log("enter into getCustRepIsViewNo function");
		var response = $http.post($scope.projectName+'/getCustRepIsViewNo');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.custRepList = data.list;
				$scope.isViewNo=false;
			}else{
				$scope.isViewNo=true;
				$('#bkToList').attr("disabled","disabled");
				$scope.alertToast(data.result);
			}

			$scope.getAllCustRepCodes();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}


	$scope.toggleSelection = function toggleSelection(crCode) {
		console.log("inside toggleSelection check = ")
		var idx = $scope.selection.indexOf(crCode);

		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		// is newly selected
		else {
			$scope.selection.push(crCode);
		}
	}	

	$scope.verifyCustomerRep = function(){
		if(!$scope.selection.length){
			$scope.alertToast("You have not selected anything");
		}else{
			var response = $http.post($scope.projectName+'/updateIsViewCustRep',$scope.selection.toString());
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.successToast(data.result);
					$scope.getCustRepIsViewNo();
				}else{
					console.log("---------->message==>"+data.result);
				}	
			});	 
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	$scope.backToList = function(){
		$scope.saveCustomerRepCode = true;
		$scope.isViewNo=false;
		$scope.custRepByM="";
		$scope.custRepByA="";
		$('#custRepByAId').attr("disabled","disabled");
		$('#custRepByMId').attr("disabled","disabled");
		$scope.showCustomerRep=true;
	}

	$scope.getAllCustRepCodes = function(){
		console.log("getAllCustRepCode------>");
		var response = $http.post($scope.projectName+'/getAllCustRepCodes');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.custRepCodeList = data.list;
				$scope.getDesignation();
				$scope.custRepCodes = data.list;
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.saveCustomerRepCode = function(custRepCodes){
		console.log("enter into saveCustomerRepCode function");
		$scope.custRepByM=custRepCodes;
		console.log("custRepByM--------"+$scope.custRepByM);
		$('div#custRepCodeDB').dialog('close');
		$scope.showCustomerRep = false;
		//$scope.divForAutoCode = true;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/getCustRepByCode',$scope.custRepByM);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.custrep = data.custrep;
				$scope.alertToast(data.result);
			}else{
				console.log("msg from server ---> "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}


	$scope.getCustDesigList = function(custrep){
		$( "#crDesignation" ).autocomplete({
			source: $scope.availableTags
		});
	} 

	$scope.fillCustDesigVal = function(){
		console.log("enter into myfun----------->>"+$( "#crDesignation" ).val());
		$scope.custrep.crDesignation=$( "#crDesignation" ).val();
	}

	$scope.getDesignation = function(){
		var response = $http.get($scope.projectName+'/getDesignationForECr');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			console.log("******************");
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.editCustomerRepSubmit = function(custrep){
		console.log("enter into editCustomerRepSubmit function--->"+custrep.crName);
		var response = $http.post($scope.projectName+'/editCustomerRepSubmit', custrep);
		response.success(function(data, status, headers, config) {
			console.log(data.result);
			$scope.custrep="";
			$scope.showCustomerRep = true;
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getCustRepCodeList=function(){
		console.log("Enter into getCustRepCodeList function");
		$( "#custRepByAId" ).autocomplete({
			source: $scope.custRepCodes
		});
	}

	$scope.getCustomerRepList = function(){
		$scope.code = $( "#custRepByAId" ).val();

		if($scope.code===""){
			$scope.alertToast("Please enter code");	
		}else{
			$scope.saveCustomerRepCode = false;
			$scope.isViewNo=true;	
			var response = $http.post($scope.projectName+'/getCustRepByCode',$scope.code);
			response.success(function(data, status, headers, config) {
				$scope.custrep = data.custrep;
				$scope.showCustomerRep = false;
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}

	}

	$scope.getCustomerList = function(){
		$scope.code = $( "#custByAId" ).val();

		if($scope.code===""){
			$scope.alertToast("Please enter code");	
		}else{
			$scope.saveCustomerCode = false;
			$scope.isViewNo=true;	
			var response = $http.post($scope.projectName+'/submitcustomer',$scope.code);
			response.success(function(data, status, headers, config) {
				$scope.customer = data.customer;
				$scope.showCustomer = false;

			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}

	}
	
	 if($scope.adminLogin === true || $scope.superAdminLogin === true){
		 $scope.getCustRepIsViewNo();
	 }else if($scope.logoutStatus === true){
			 $location.path("/");
	 }else{
			 console.log("****************");
	 }
	
}]);