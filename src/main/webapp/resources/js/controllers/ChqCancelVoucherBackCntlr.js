'use strict';

var app = angular.module('application');

app.controller('ChqCancelVoucherBackCntlr',['$scope','$location','$http','$filter',
                                        function($scope,$location,$http,$filter){
	
	console.log("ChqCancelVoucherBackCntlr Started");
	
	$scope.vs = {};
	
	$scope.chqList = [];
	$scope.bankACNList = [];
	$scope.bankACN = {};
	
	$scope.usedChqList = [];
	$scope.usedChq = {};
	
	$scope.unUsedChqList = [];
	$scope.unUsedChq = {};
	
	$scope.chqAmt = "";
	
	$scope.bankDBFlag = true;
	$scope.chqVoucherDBFlag = true;
	$scope.saveVsFlag = true;
	
	$scope.usedChqDBFlag = true;
	$scope.unUsedChqDBFlag = true;
	
	$('#amtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	 $('#amtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	
	
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		var response = $http.post($scope.projectName+'/getVDetFrCCVBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankACNList = data.bankACNList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = "Cheque Cancel";
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				//$scope.getAllFaCode();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.getAllFaCode = function(){
		console.log("enter into getAllFaCode function");
		var response = $http.post($scope.projectName+'/getAllFaCodeFrCPV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.faMList = data.faMList;
				$scope.faTList = data.faTList;
			}else{
				console.log("Error in bringing FaCode");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankDB = function(){
		console.log("enter into openBankDB function");
		$scope.bankDBFlag = false;
		$('div#bankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankDB').dialog('open');	
	}
	
	
	$scope.saveBank =  function(bank){
		$scope.bankACN = bank;
		console.log("enter into saveBankCode----->"+bank.bnkName);
		$scope.vs.bankCode = bank.bnkFaCode;
		$('div#bankDB').dialog('close');
		$scope.bankDBFlag = true;
		console.log("$scope.vs.chequeType = "+$scope.vs.chequeType);
		
		var response = $http.post($scope.projectName+'/getChqFrCCV',$scope.bankACN);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				
				$('#usedChqNoId').removeAttr("disabled");
				$('#unUsedChqNoId').removeAttr("disabled");
				
				$scope.unUsedChq = {};
				$scope.usedChq = {};
				$scope.chqAmt = "";
				
				$scope.usedChqList = data.usedChqList;
				$scope.unUsedChqList = data.unUsedChqList;
				
			}else{
				console.log("Error in bringing data from getChqFrCCV");
				$scope.alertToast(data.result);
				$('#usedChqNoId').attr("disabled","disabled");
				$('#unUsedChqNoId').attr("disabled","disabled");
				
				$scope.unUsedChq = {};
				$scope.usedChq = {};
				$scope.chqAmt = "";
			}

		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.openUsedChqDB = function() {
		console.log("enter into openUsedChqDB function");
		$scope.usedChqDBFlag = false;
		$('div#usedChqDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Used Cheques",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.usedChqDBFlag = true;
		    }
			});
		
		$('div#usedChqDB').dialog('open');
	}
	
	$scope.saveUsedChq = function(usedChq){
		console.log("enter into saveUsedChq function");
		$('div#usedChqDB').dialog('close');
		$scope.usedChq = usedChq;
		$scope.unUsedChq = {};
		$('#unUsedChqAmtId').attr("disabled","disabled");
	}
	
	$scope.openUnUsedChqDB = function() {
		console.log("enter into openUnUsedChqDB function");
		$scope.unUsedChqDBFlag = false;
		$('div#unUsedChqDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Unused Cheques",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.unUsedChqDBFlag = true;
		    }
			});
		
		$('div#unUsedChqDB').dialog('open');
	}
	
	$scope.saveUnUsedChq = function(unUsedChq){
		console.log("enter into saveUnUsedChq function");
		$('div#UnUsedChqDB').dialog('close');
		$scope.unUsedChq = unUsedChq;
		$scope.unUsedChq.chqLChqAmt = $scope.usedChq.chqLChqAmt;
		$('#unUsedChqAmtId').removeAttr("disabled");
	}
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("entre into voucherSubmit function = "+newVoucherForm.$invalid);
		
		if(newVoucherForm.$invalid){
			$scope.alertToast("please enter correct vouher details") 
		}else{
			console.log("final submittion");
		    $scope.saveVsFlag = false;
	    	$('div#saveVsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveVsDB').dialog('open');
		}
	}
	
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	
	$scope.saveVS = function(){
		 console.log("enter into saveVS function");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
		 
		 $scope.vs.usedChq = $scope.usedChq;
		 $scope.vs.unUsedChq = $scope.unUsedChq;
		 
		 $('#saveId').attr("disabled","disabled");
		 
		 var response = $http.post($scope.projectName+'/submitCCVoucher',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("success");
					$('#saveId').removeAttr("disabled");
					$scope.vs = {};
					$scope.usedChqList = [];
					$scope.usedChq = {};
					$scope.unUsedChqList = [];
					$scope.unUsedChq = {};
					$scope.chqAmt = "";
					$('#unUsedChqAmtId').attr("disabled","disabled");
					$('#usedChqNoId').attr("disabled","disabled");
					$('#unUsedChqNoId').attr("disabled","disabled");
					$scope.getVoucherDetails();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	}else if($scope.logoutStatus === true){
		 $location.path("/");
	}else{
		 console.log("****************");
	}
	
	console.log("ChqCancelVoucherCntlr Ended");
	
}]);