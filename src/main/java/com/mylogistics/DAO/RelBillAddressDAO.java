package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.AdaniGST;
import com.mylogistics.model.HilAddress;
import com.mylogistics.model.RelBillAddress;

@Repository
public interface RelBillAddressDAO {
	
	public List<RelBillAddress> getAllRelBlAdd();

	public Object getRelBlAddByGst(String gstNo, Session session);
	
	public List<AdaniGST> getAllAdaniGst();
	
	public List<HilAddress> getAllHilAddress();
}
