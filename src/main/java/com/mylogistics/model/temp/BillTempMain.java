package com.mylogistics.model.temp;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mylogistics.model.bank.ChequeBook;

@Entity
@Table(name="billtempmain")
public class BillTempMain {
	
	@Id
	@Column(name="btmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int btmId;
	
	@Column(name="btmCustCode")
	private String btmCustCode;
	
	@Column(name="btmPanNo")
	private String btmPanNo;
	
	@Column(name="btmBillNo")
	private String btmBillNo;
	
	@Column(name="btmBillDt")
	private Date btmBillDt;
	
	@Column(name="btmSubTotal")
	private double btmSubTotal;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="btmain_btdetail",
	joinColumns=@JoinColumn(name="btmId"),
	inverseJoinColumns=@JoinColumn(name="btdId"))
	private List<BillTempDetail> billTempDetailList = new ArrayList<>();

	public int getBtmId() {
		return btmId;
	}

	public void setBtmId(int btmId) {
		this.btmId = btmId;
	}

	public String getBtmCustCode() {
		return btmCustCode;
	}

	public void setBtmCustCode(String btmCustCode) {
		this.btmCustCode = btmCustCode;
	}

	public String getBtmPanNo() {
		return btmPanNo;
	}

	public void setBtmPanNo(String btmPanNo) {
		this.btmPanNo = btmPanNo;
	}

	public String getBtmBillNo() {
		return btmBillNo;
	}

	public void setBtmBillNo(String btmBillNo) {
		this.btmBillNo = btmBillNo;
	}

	public Date getBtmBillDt() {
		return btmBillDt;
	}

	public void setBtmBillDt(Date btmBillDt) {
		this.btmBillDt = btmBillDt;
	}

	public double getBtmSubTotal() {
		return btmSubTotal;
	}

	public void setBtmSubTotal(double btmSubTotal) {
		this.btmSubTotal = btmSubTotal;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public List<BillTempDetail> getBillTempDetailList() {
		return billTempDetailList;
	}

	public void setBillTempDetailList(List<BillTempDetail> billTempDetailList) {
		this.billTempDetailList = billTempDetailList;
	}

	
}
