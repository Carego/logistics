package com.mylogistics.constants;

public class BranchSStatsCNTS {

	public static final String BRS_ID = "brsStatsId";
	
	public static final String BRS_BRCODE = "brsStatsBrCode";
	
	public static final String BRS_DT = "brsStatsDt";
	
	public static final String BRS_STATUS_TYPE = "brsStatsType";
	
	public static final String BRS_SERIAL_NO = "brsStatsSerialNo";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
