package com.mylogistics.constants;

public class BranchStockLeafDetCNTS {
	
	public static final String BRSLD_ID = "brsLeafDetId";
	
	public static final String BRSLD_BRCODE = "brsLeafDetBrCode";
	
	public static final String BRSLD_STATUS = "brsLeafDetStatus";
	
	public static final String BRSLD_SNO = "brsLeafDetSNo";
	
	public static final String BRSLD_DLYRT = "brsLeafDetDlyRt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";	

}
