<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<div class="col s4 input-field">
			<input class="validate" type="radio" id="asgnBankID" name="assignRBG" ng-click="assignBankRB()"> 
			<label for="code">Assign Bank</label>
		</div>

		<div class="col s4 input-field">
			<input class="validate" type="radio" id="bankDissID"	name="assignRBG" ng-click="bankDissRB()" > 
			<label for="code">Dissociate by Bank</label>
		</div>
		
		<div class="col s4 input-field">
			<input class="validate" type="radio" id="dissociateBankID"	name="assignRBG" ng-click="dissociateBankRB()" > 
			<label for="code">Dissociate by Branch</label>
		</div>
	</div>
	
	<!--Assign bank  -->
	<div class="row" ng-show="assignBankRBFlag">
	<!--  <div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form name="assignBankForm" ng-submit=assignBank(assignBankForm) class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);"> -->
		<form name="assignBankForm" ng-submit=assignBank(assignBankForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectBnkId" name="selectBnk" ng-model="bankMstr.bnkName" ng-click="openBankDB()" readonly ng-required="true"> 
					<label for="code">Select Bank</label>
				</div>

				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectBranchId"	name="selectBranch" ng-model="branch.branchName" ng-click="openBranchDB()" disabled="disabled" readonly ng-required="true"> 
					<label for="code">Select Branch</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type="submit" value="Assign">
				</div>
			</div>
		</form>
		
	</div>
	
	<div class="row" ng-if="bankDissRBFlag">
		<table class="tblrow">
       		
       		<caption class="coltag tblrow">Assigned Bank</caption> 
       		
            <tr class="rowclr">
	            <th class="colclr">SNo</th>
	            <th class="colclr">Name</th>
	            <th class="colclr">FaCode</th>
	            <th class="colclr">A/C No.</th>
	            <th class="colclr">Action</th>
	        </tr>	
	         
	        <tr ng-repeat="bankMstr in assignedBankList">
	            <td class="rowcel">{{$index+1}}</td>
	            <td class="rowcel">{{bankMstr.bnkName}}</td>
	            <td class="rowcel">{{bankMstr.bnkFaCode}}</td>
	            <td class="rowcel">{{bankMstr.bnkAcNo}}</td>
	            <td class="rowcel"><input type="button" value="Dissociate" ng-click="dissAssignedBank(bankMstr, $index)"/></td>
	        </tr>
         
	
    	</table>
          
	</div>
	
	
	<!--dissociate bank by branch-->
	<div class="row" ng-show="dissociateBankRBFlag">
		<form name="dissBankForm" ng-submit=dissBankSubmit(dissBankForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="input-field col s6">
				<input type="text" name="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" required readonly ng-click="openBranchDB()" />
   				<label>Enter Branch Code</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
		</form>
		<div class="col s3"> &nbsp; </div>
	</div>
	
	<div id ="bankDB" ng-hide="bankDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.bankName" placeholder="Search by Bank Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Name </th>
 	  	  	  <th> Bank FaCode </th>
 	  	  	  <th> Bank A/c No. </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox.bankName ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr, $index)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.branchName" placeholder="Search by branch Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox.branchName ">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="assignBankDB" ng-hide="assignBankFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<h6>Assign {{bankMstr.bnkName}} bank to the branch {{branch.branchName}}</h6>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Assign" ng-click="assignBankToBranch()"/>
		</div>
	</div>
	
	
	
	<!-- <div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		
	</div> -->
	
	<div class="row" ng-if="showTableFlag">
			<table class="tblrow">
	       		
	       		<caption class="coltag tblrow">Bank Assigned to this Branch</caption> 
	       		
	            <tr class="rowclr">
		            <th class="colclr">SNo</th>
		            <th class="colclr">Name</th>
		            <th class="colclr">FaCode</th>
		            <th class="colclr">A/C No.</th>
		            <th class="colclr">Action</th>
		        </tr>	
		         
		        <tr ng-repeat="bankMstr in branchBMList">
		            <td class="rowcel">{{$index+1}}</td>
		            <td class="rowcel">{{bankMstr.bnkName}}</td>
		            <td class="rowcel">{{bankMstr.bnkFaCode}}</td>
		            <td class="rowcel">{{bankMstr.bnkAcNo}}</td>
		            <td class="rowcel"><input type="button" value="Dissociate" ng-click="dissBank(bankMstr, $index)"/></td>
		        </tr>
	         
		
	    	</table>
	          
		</div>
		
		<div id="dissBankDB" ng-hide="dissBankFlag"> 
			<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
				<table class="table-hover table-bordered table-bordered">
					<h6>Dissociate {{branchBankMstr.bnkName}} bank from the branch {{branch.branchName}}</h6>
				</table>
				<input type="button" value="Cancel" ng-click="dissBack()"/>
				<input type="button" value="Dissociate" ng-click="dissBankFromBranch()"/>
			</div>
		</div>
		
		<div id="dissAssBankDB" ng-hide="dissAssBankFlag"> 
			<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
				<table class="table-hover table-bordered table-bordered">
					<h6>Dissociate {{dissAssBankMstr.bnkName}} bank </h6>
				</table>
				<input type="button" value="Cancel" ng-click="dissAssBack()"/>
				<input type="button" value="Dissociate" ng-click="dissAssBank()"/>
			</div>
		</div>
	
</div>
