'use strict';

var app = angular.module('application');

app.controller('OperatorCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.operatorCodeFlag = true;
	$scope.createDispatchFlag = true;
	$scope.dispatchDetailsDBFlag = true;
	$scope.createStnOrderFlag = true;
	$scope.StationarySupplierFlag = true;
	$scope.RecStationaryFlag = true;
	$scope.cnmtFlag=false;
	$scope.chlnFlag=false;
	$scope.sedrFlag=false;
	
	$scope.id=0;
	var responseCnmt;
	var responseChln;
	var responseSedr;
	
	$scope.operatorBCode = "";
	$scope.dispatchCode = "";
	$scope.DispatchCodes="";
	$scope.cnmtSeq = "";
	$scope.chlnSeq = "";
	$scope.sedrSeq = "";
	
	$scope.totalHBONList = [];
	$scope.cnmtStartNoList = [];
	$scope.chlnStartNoList = [];
	$scope.sedrStartNoList = [];
	$scope.oprtrNotList=[];
	$scope.hboList = [];
	$scope.cnmtTextboxList = [];
	$scope.chlnTextboxList =[];
	$scope.sedrTextboxList = [];
	$scope.modelCnmt = [];
	$scope.modelChln = [];
	$scope.modelSedr = [];
	$scope.brSDisDetCnmtList = [];
	$scope.brSDisDetChlnList = [];
	$scope.brSDisDetSedrList = [];
	
	$scope.brsDisData = {};
	$scope.st = {};
	$scope.supplierCodeList={};
	$scope.Stationary = {};
	$scope.currentHBOSN = {};	
	$scope.stnOdrInv={};
	$scope.brsd = {};
	$scope.HBON={};
	
	$scope.getBrStDisDetData = function(dispatchCode){
		console.log("Enter into getBrStDisDetData() : "+ dispatchCode);
		var response = $http.post($scope.projectName+'/getBrStDisDetData', dispatchCode);
		response.success(function(data, status, headers, config){
			if(data.result==="success"){				  
				$scope.brsDisData      = data.brsdData;
				
				$scope.DispatchCodes = $scope.brsDisData.brsDisCode;
				$scope.dispatchCode = $scope.brsDisData.brsDisCode;
				
				$scope.cnmtStartNoList = data.cnmtStNoList;
				$scope.chlnStartNoList = data.chlnStNoList;
				$scope.sedrStartNoList = data.sedrStNoList;
				//Open Dialog
				$scope.openDispatchDetailsDB();				
			}else{
				console.log(data);				   
			}
		});
		response.error(function(data, status, headers, config) {
			scope.errorToast(data.result);
		});
	}
	
	
	
	
	$scope.saveBranchStockDispatch = function(createDispatchOrderForm,brsd,modelCnmt,modelChln,modelSedr){
		console.log("Enter into saveBranchStockDispatch");
		$scope.createDispatchFlag = true;
		$('div#createDispatchID').dialog('close');
		console.log("$scope.cnmtFlag = "+$scope.cnmtFlag);
		console.log("$scope.chlnFlag = "+$scope.chlnFlag);
		console.log("$scope.sedrFlag = "+$scope.sedrFlag);
		
		if(brsd.brsDisCnmt <= 0){
			$scope.cnmtFlag = true;
		}
		
		if(brsd.brsDisChln <= 0){
			$scope.chlnFlag = true;
		}
		
		if(brsd.brsDisSedr <= 0){
			$scope.sedrFlag = true;
		}
			
		if($scope.cnmtFlag===true && $scope.chlnFlag===true && $scope.sedrFlag===true){
			 var brSDis = {
					"cnmtList"         :    modelCnmt,
					"chlnList"         :    modelChln,
					"sedrList"         :    modelSedr,
					"brStkDis"         :	brsd,
					"operatorCode"		:	brsd.operatorCode,
					"id"               :    $scope.id
			 };
			
			   var response = $http.post($scope.projectName+'/saveBranchStockDispatch',brSDis);
			   response.success(function(data, status, headers, config){
				   if (data.result==="success") {
					   $scope.alertToast(data.result);
					   $scope.hboList = "";
					   $scope.getHBOprtrData();
				   } else {	
					   $scope.alertToast("Either Stationary already Dispatched or DB error");
				   }
		       });
			   response.error(function(data, status, headers, config) {
				   $scope.alertToast("error in response: "+data);
			   });
			
	    }else{
			$scope.alertToast("Please fill the correct number")
		}
	}
			
			$scope.getHBOprtrData = function(){
				console.log("Enter into getHBOprtrData");
				   var response = $http.post($scope.projectName+'/getHBOprtrData');
				   response.success(function(data, status, headers, config){
					   if(data.result==="success"){
						   $scope.hboList = data.list;
						   console.log("---"+data.list);
						   $scope.getTotalHBONData();
					   }else{
						   console.log(data);
						   $scope.getHBOSNData();
					   }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
			   }
			
			$scope.createOrder = function(hboN){
				console.log("Entr to create order");
				
				$scope.HBON = hboN;
				$scope.id	= hboN.hbonId;
				
				$scope.brsd.brsDisBranchCode = hboN.disBranchCode;				
				
				$scope.operatorBCode = $scope.brsd.brsDisBranchCode;
				
				$scope.brsd.brsDisCnmt = hboN.noCnmt;
				$scope.brsd.brsDisChln = hboN.noChln;
				$scope.brsd.brsDisSedr = hboN.noSedr;  
				$scope.saveCnmt(hboN.noCnmt);
				$scope.saveChln(hboN.noChln);
				$scope.saveSedr(hboN.noSedr);
				
				console.log("ID = "+$scope.id);
				console.log("BRS_DIS_Branch_Code = "+$scope.brsd.brsDisBranchCode);
				console.log("CNMT = "+$scope.brsd.brsDisCnmt);
				console.log("CHALLAN = "+$scope.brsd.brsDisChln);
				console.log("SEDR = "+$scope.brsd.brsDisSedr);
								
				$scope.getOperatorCode($scope.operatorBCode);
				
				$scope.createDispatchFlag = false;
				$('div#createDispatchID').dialog({
					autoOpen: false,
					modal:true,
					resizable: true,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					/*title: "To Station",*/
					draggable: true,
					close: function(event, ui) { 
						 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
					
					$('div#createDispatchID').dialog('open');
				
				
				/*var response = $http.post($scope.projectName+'/createOrder',hbo);
					console.log("After getting response--");
				   response.success(function(data, status, headers, config){
					   if(data.result==="success"){
						   $location.path('/operator/creatDispatchOrder');
					   }else{
						   console.log(data);
					   }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});*/
			}
			
			
			
			
			$scope.saveCnmt = function(cnmt){
				console.log("Enter into saveCNMT---"+cnmt);
				
				/*for(var i=0;i<$scope.cnmtTextboxList.length;i++){
					$scope.cnmtTextboxList.splice(i,1);
				}*/
				$scope.cnmtTextboxList = [];
				for(var i=0;i<cnmt;i++){
					$scope.cnmtTextboxList.push(i);
				}
			}
			
			$scope.saveChln = function(chln){
				console.log("Enter into saveChln---"+chln);
				
				/*for(var i=0;i<$scope.chlnTextboxList.length;i++){
					$scope.chlnTextboxList.splice(i,1);
				}*/
				$scope.chlnTextboxList = [];
				for(var i=0;i<chln;i++){
					$scope.chlnTextboxList.push(i);
				}
			}
			
			$scope.saveSedr = function(sedr){
				console.log("Enter into saveSedr---"+sedr);
				
				/*for(var i=0;i<$scope.sedrTextboxList.length;i++){
					$scope.sedrTextboxList.splice(i,1);
				}*/
				$scope.sedrTextboxList = [];
				for(var i=0;i<sedr;i++){
					$scope.sedrTextboxList.push(i);
				}
			}
			
			
			
			
			
		$scope.getTotalHBONData = function(){
			console.log("enter into getTotalHBONData function");
			var response = $http.post($scope.projectName+'/getTotalHBONData');
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   $scope.totalHBONList = data.list;
					   console.log("total notification---"+data.list);
					   $scope.getHBOSNData();
				   }else{
					   $scope.getHBOSNData();
					   console.log(data);
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
			
		$scope.getListWithDate = function(disDate){
			//$scope.date= dateList;
			$scope.notificationDate = {
					date : disDate
			};
			$scope.date = new Date();
			$scope.date = disDate;
			console.log("$scope.date = "+$scope.date);
			console.log("disDate = "+disDate);
			var response = $http.post($scope.projectName+'/getNotificationWithDate',$scope.notificationDate);
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
				   $scope.totalHBONList = data.list;
					   console.log("total notification---"+data.list);
				   }else{
					   console.log(data);
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		
		$scope.getOprtrNotfctnData = function(){
			console.log("Enter into getOprtrNotfctnData");
			   var response = $http.post($scope.projectName+'/getOprtrNotfctnData');
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   $scope.oprtrNotList = data.list;				    
				   }else{
					   console.log(data);					   
				   }
				   $scope.getHBOprtrData();
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		   }
		
		$scope.openDispatchDetailsDB = function(){
			
			$scope.dispatchDetailsDBFlag = false;
			$('div#dispatchDetailsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				/*title: "To Station",*/
				draggable: true,
				close: function(event, ui) { 
					 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
				
				$('div#dispatchDetailsDB').dialog('open');
		}
		
		$scope.submitDisDetForm = function(dispatchDetailForm,disDate,receiveDate,invoiceNo){
			console.log("enter into submitDisDetForm function--->"+$scope.brsDisData.brsDisBranchCode);
			
			if($('.abc:checked').length == $('.abc').length){
			var disDetService = {
					"dispatchCode"    : $scope.dispatchCode,
					"cnmtStartNoList" : $scope.cnmtStartNoList,
					"chlnStartNoList" : $scope.chlnStartNoList,
					"sedrStartNoList" : $scope.sedrStartNoList,
					"dispatchDate"    : disDate,
					"receiveDate"     : receiveDate,
					"invoiceNo"       : invoiceNo,
					"branchCode"      : $scope.brsDisData.brsDisBranchCode
			};
			
			console.log("enter into submitDisDetForm function");
			if($scope.receiveDate>=$scope.brsDisData.brsDisDt){
			 var response = $http.post($scope.projectName+'/setIsReceivedYesForOprtrNot',disDetService);
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   $scope.alertToast(data.result);
					   $('div#dispatchDetailsDB').dialog('close');
					   $scope.getOprtrNotfctnData();
				   }else{
					console.log(data);   
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});			   
		     }else{
				$scope.alertToast("Receiving date cannot be less than Dispatch Date")
				console.log("Invalid");					
			}
			}else{
					$scope.alertToast("check all checkboxes");
			}
		}
		
		
		$scope.compareDate=function (){
			
			if($scope.receiveDate<$scope.brsDisData.brsDisDt)
				{
				$scope.alertToast("Receiving date cannot be less than Dispatch Date")
				console.log("Invalid");
				
				}
		}
		
		
		$scope.getHBOSNData = function(){
			console.log("enter into getHBOSNData function");
			var response = $http.post($scope.projectName+'/getHBOSNData');
			response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					  $scope.HBOSNList = [];
					  $scope.HBOSNList = data.HBOSNList;
					  console.log("size of HBOSNList = "+$scope.HBOSNList.length);
					  $scope.getStationarySupplierCode();
				   }else{
					   $scope.HBOSNList = [];
					   console.log(data);   
				   }
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			});
		}
		
		
		$scope.createStnOrder = function(HBOSN){
			console.log("enter into createStnOrder function --->HBOSN_id="+HBOSN.hBOSN_isCreate);
			$scope.currentHBOSN = HBOSN;
			
			/*if($scope.currentHBOSN === "received"){
				$scope.alertToast("already created");
			}*/if(HBOSN.hBOSN_isCreate === "yes"){
				$scope.alertToast("already created");
			}else{
				console.log("$scope.currentHBOSN id = "+$scope.currentHBOSN.hBOSN_Id);
				$scope.createStnOrderFlag = false;
				$('div#createStnOrderDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					title : "Create Stationary Order",
					show: UDShow,
					hide: UDHide,
					draggable: true,
					close: function(event, ui) {  
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
					
			   $('div#createStnOrderDB').dialog('open');
			   $scope.st.stOdrCnmt = HBOSN.hBOSN_cnmtNo;
			   $scope.st.stOdrChln = HBOSN.hBOSN_chlnNo;
			   $scope.st.stOdrSedr = HBOSN.hBOSN_sedrNo;
			   console.log("HBOSN.hBOSN_sedrNo = "+HBOSN.hBOSN_sedrNo)
			}
		}
		
		
		$scope.openStationarySupplierDB = function(){
			console.log("enter into openStationarySupplierDB function");
			$scope.StationarySupplierFlag = false;
	    	$('div#StationarySupplierDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
				});
			
			$('div#StationarySupplierDB').dialog('open');
		}
		
		
		$scope.getStationarySupplierCode = function(){
			console.log("Enter into getStationarySupplierCode function");
			
			var response = $http.post($scope.projectName+'/getStationarySupplierCode');
			response.success(function(data, status, headers, config) {
				console.log("from server ---->"+data.list);
				$scope.supplierCodeList = data.list;
				//$scope.getHBOprtrData();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
		
		
		$scope.saveSupplierCode = function(supplier){
			console.log("Enter into saveSupplierCode");
			$('div#StationarySupplierDB').dialog('close');
			console.log("supplier.stSupCode"+supplier.stSupCode);
			$scope.st.stSupCode = supplier.stSupCode;
		}
		
		
		$scope.submitStationaryOrder = function(StationaryOrderForm,st){
			console.log("enter into submitStationaryOrder function--->");
			if(StationaryOrderForm.$invalid){
				if(StationaryOrderForm.stOdrCnmt.$invalid){
					$scope.alertToast("Cnmt cannot be greater than 7 digits");	
				}else if(StationaryOrderForm.stOdrChln.$invalid){
					$scope.alertToast("Chln cannot be greater than 7 digits");	
				}else if(StationaryOrderForm.stOdrSedr.$invalid){
					$scope.alertToast("Sedr cannot be greater than 7 digits");	
				}else if(StationaryOrderForm.stSupCode.$invalid){
					$scope.alertToast("Please Enter Supplier Code");	
				}
			}else{
				/*if((st.stOdrCnmt===null || angular.isUndefined(st.stOdrCnmt) || st.stOdrCnmt==="")&&
						(st.stOdrChln===null || angular.isUndefined(st.stOdrChln) || st.stOdrChln==="")&&	
						(st.stOdrSedr===null || angular.isUndefined(st.stOdrSedr) || st.stOdrSedr==="")){
						$scope.alertToast("please Enter any one field ");
				}else{*/
					
					var StationaryService = {
							"hBO_StnNotification" : $scope.currentHBOSN,
							"stationary"          : st
					};
					
					var response = $http.post($scope.projectName+'/saveStationaryOrder',StationaryService);
					
					response.success(function(data, status, headers, config) {
						console.log("from server ---->"+data.result);
						$scope.successToast(data.result);
						/*alert(data.resultCnmt);
						alert(data.resultChln);
						alert(data.resultSedr);*/
						//$scope.st="";
						$('div#createStnOrderDB').dialog('close');
						$scope.createStnOrderFlag = true;
						/*$scope.Stationary = data.stationary;
						$scope.cnmtSeq = data.cnmtSeq;
						$scope.chlnSeq = data.chlnSeq;
						$scope.sedrSeq = data.sedrSeq;*/
						$scope.getHBOSNData();
						
						/*$scope.stnOdrInv.stOdrDt = st.stOdrDt;
						$scope.stnOdrInv.stCode=$scope.Stationary.stCode;
						$scope.stnOdrInv.stSupCode=$scope.Stationary.stSupCode;
						$scope.stnOdrInv.stSupInvcNo=$scope.Stationary.stSupInvcNo;
						$scope.stnOdrInv.stOdrSedr=$scope.Stationary.stOdrSedr;
						$scope.stnOdrInv.stOdrChln=$scope.Stationary.stOdrChln;
						$scope.stnOdrInv.stOdrCnmt=$scope.Stationary.stOdrCnmt;*/
						//$scope.saveStnOdrInv();
					});
					response.error(function(data, status, headers, config) {
						console.log("from server ---->"+data.result);
						$scope.errorToast(data.result);
					});
				//}
			}	
		}
		
		
		
		
		$scope.recStnOrder = function(HBOSN){
			console.log("enter into recStnOrder function = ");
			
			if(HBOSN.hBOSN_isCreate === "no"){
				$scope.alertToast("please create stationary order");
			}else{
				var recData = {
						"stnId" : HBOSN.hBOSN_stnId
				};
				$scope.currentHBOSN = HBOSN;
				var response = $http.post($scope.projectName+'/getStnRecService',recData);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
						$scope.Stationary = data.stn;
						$scope.RecStationaryFlag = false;
				    	$('div#RecStationaryDB').dialog({
				    		autoOpen: false,
							modal:true,
							resizable: false,
							position: UDPos,
							title : "Receive Stationary Order",
							show: UDShow,
							hide: UDHide,
							draggable: true,
							close: function(event, ui) {  
						        $(this).dialog('destroy');
						        $(this).hide();
						    }
						});
				    	$('div#RecStationaryDB').dialog('open');
					}else{
						console.log("error in bringing data from server = "+data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
							
						//}
					//}
				/*});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});*/
		}
		
		
		$scope.recStationary = function(date,nowCnmtSeq,nowChlnSeq,nowSedrSeq){
			console.log("enter into recStationary function");
			console.log("before updation time $scope.currentHBOSN.hBOSN_Id="+$scope.currentHBOSN.hBOSN_Id);
				$scope.Stationary.stRecDt = date;
				$scope.Stationary.stIsRec = "yes";

				console.log("before updation time $scope.currentHBOSN.hBOSN_Id="+$scope.currentHBOSN.hBOSN_Id);
				var response = $http.post($scope.projectName+'/recStationary',$scope.Stationary);
				response.success(function(data, status, headers, config) {
					console.log("from server ---->"+data.result);
					if(data.result === "success"){
						console.log("at the updation time $scope.currentHBOSN.hBOSN_Id="+$scope.currentHBOSN.hBOSN_Id);
						
						var chcnseInfo = {
								"nowCnmtSeq" : nowCnmtSeq,
								"nowChlnSeq" : nowChlnSeq,
								"nowSedrSeq" : nowSedrSeq,
								"hbosnId"    : $scope.currentHBOSN.hBOSN_Id
						};
						
						var response = $http.post($scope.projectName+'/updateHBOSN',chcnseInfo);
						response.success(function(data, status, headers, config) {
							if(data.result === "success"){
								$scope.RecStationaryFlag = true;
								$('div#RecStationaryDB').dialog('close');
							}	
							$scope.getHBOSNData();
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data);
						});
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			//}
		}
		
		
		$scope.checkCnmtNo=function(modelCnmt){
			 console.log("enter into checkCnmtNo function");	
			 
			 for(var i =0;i<modelCnmt.length;i++){
				 	var t = ((modelCnmt[i]-1)%50);
				 	//if(t===0 && modelCnmt[i].length===7){
				 	if(t===0){
				 		$scope.cnmtFlag=true;
				 		console.log(modelCnmt[i]);
				 	}else{
				 		$scope.cnmtFlag=false;
				 		$scope.alertToast("Enter correct Cnmt Number");
			 			console.log("Failed from checkCnmt");
				 	}
				
				} 
			}
			
			
			$scope.checkChlnNo=function(modelChln){
				console.log("enter into checkChlnNo function");	
				
				for(var i =0;i<modelChln.length;i++){
				 	var t = ((modelChln[i]-1)%50);
				 	//if(t===0 && modelChln[i].length===7){
				 	if(t===0){
				 		$scope.chlnFlag=true;
				 		console.log(modelChln[i]);
				    }else
				 		{
				 		$scope.chlnFlag=false;
				 		$scope.alertToast("Enter correct Chln Number");
				 		console.log("Failed from checkChln");
			 			
				 	}
				    
				 }	 
			}
			
			
			$scope.checkSedrNo=function(modelSedr)
			{
				console.log("enter into checkSedrNo function");	
				
				 for(var i =0;i<modelSedr.length;i++){
					 	var t = ((modelSedr[i]-1)%50);
					 	//if(t===0 && modelSedr[i].length===7)
					 	if(t===0){
					 		$scope.sedrFlag=true;
					 		console.log(modelSedr[i]);
					 	}else{
					 		$scope.sedrFlag=false;
					 		$scope.alertToast("Enter correct Sedr Number");
				 			console.log("Failed from checkSedr");
					 	} 
				 }
			}
			
			$scope.saveStnOdrInv = function(){
				console.log("enter into saveStnOdrInv function");	
				var finalObject = {
						"stationary" : $scope.stnOdrInv,
						"cnmtSeq"    : $scope.cnmtSeq,
						"chlnSeq"    : $scope.chlnSeq,
						"sedrSeq"   :  $scope.sedrSeq
				};
				
				var response = $http.post($scope.projectName+'/saveStnOdrInv',finalObject);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
						$location.path('/operator/stnOdrInv');
					}
				});
			}
			
			
		/*$scope.getStnRecService = function(){
			console.log("enter into getStnRecService function");
			var response = $http.post($scope.projectName+'/getStnRecService');
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.Stationary = data.stnservice.stationary;
					$scope.cnmtSeq = data.stnservice.cnmtSeq;
					$scope.chlnSeq = data.stnservice.chlnSeq;
					$scope.sedrSeq = data.stnservice.sedrSeq;
				}
			});
			response.error(function(data, status, headers, config) {
				alert(data);
			});
		}*/
		
		
	//$scope.getHBOSNData();	
	
	//$scope.getHBOprtrData();
	//$scope.getDispatchCode();
			$scope.getOperatorCode = function(branchCode){
				console.log("Enter into getOperatorCode");
				   var response = $http.post($scope.projectName+'/getOperatorCodeForStock',branchCode);
				   response.success(function(data, status, headers, config){
					   if(data.result==="success"){
						   $scope.opCodeList = data.list;
						   console.log("---"+data.list);
					   }else{
						   console.log(data);
					   }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});	
			}
			
			$scope.openOperatorCodeDB = function(){
				$scope.operatorCodeFlag = false;
				$('div#operatorCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: [350,200],
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#operatorCodeDB').dialog('open');
				}
			
			$scope.savOperatorCode = function(opcode){
				console.log("Enter into saveOprtrCode"+opcode);							
				$scope.brsd.operatorCode = opcode;
				$('div#operatorCodeDB').dialog('destroy');
				$scope.operatorCodeFlag = true;
			}
			
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getOprtrNotfctnData();		
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);