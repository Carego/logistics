package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.RegularContractOldDAO;
import com.mylogistics.constants.RegularContractCNTS;
import com.mylogistics.model.RegularContractOld;

public class RegularContractOldDAOImpl implements RegularContractOldDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public RegularContractOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveRegContOld(RegularContractOld regularContractOld){
		 System.out.println("enter into saveRegContOld function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(regularContractOld);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<RegularContractOld> getOldRegularContract(String contractCode){
		 System.out.println("Enter into getOldDailyContract function");
		 List<RegularContractOld> result = new ArrayList<RegularContractOld>();
				 
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(RegularContractOld.class);
			 cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,contractCode));
			 result =cr.list();
			 transaction.commit();
			 session.flush();
			 System.out.println("After getting the list from old regular contract"+result.size());
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return  result; 
	 }
}
