'use strict';

var app = angular.module('application');

app.controller('DlyRtAllowCntlr',['$scope','$location','$http','$filter','$window',
                                   function($scope,$location,$http,$filter,$window){
	
	$scope.brhList = [];
	$scope.cnmtList = [];
	
	$scope.branch = {};
	$scope.cnmt = "";
	$scope.dlyRt = false
	$scope.cnmtDBFlag = true;
	$scope.brhFlag = true;
	
	$scope.getBranch = function(){
		var response = $http.post($scope.projectName+'/getBrhFrDlyRt');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.brhList = data.list;
			}else{
				console.log("error in fetching branch list");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.brhFlag = false;
    	$('div#brhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: 'Select Branch',
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brhFlag = true;
		    }
			});
		$('div#brhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(branch){
		console.log("enter into saveBranch function");
		$('div#brhId').dialog('close');	
		$scope.branch = branch;
		
		var map = {
				"brhId" : $scope.branch.branchId
		};
		
		var response = $http.post($scope.projectName+'/getCnmtFrDlyRt',map);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.cnmtList = data.list;
			}else{
				console.log("error in fetching cnmt list");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	$scope.openCnmtDB = function(){
		console.log("enter into openCnmtDB function");
		if(angular.isUndefined($scope.branch.branchName) || $scope.branch.branchName === null || $scope.branch.branchName === ""){
			$scope.alertToast("please select any branch");
		}else{
			$scope.cnmtDBFlag = false;
	    	$('div#cnmtDBId').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: 'Select Cnmt',
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.cnmtDBFlag = true;
			    }
				});
			$('div#cnmtDBId').dialog('open');	
		}
	}
	
	$scope.saveCnmt = function(cnmt){
		console.log("enter into saveCnmt funciton")
		$('div#cnmtDBId').dialog('close');	
		$scope.cnmt = cnmt;	
	}
	
	
	$scope.submitDlyRt = function(dlyRtForm){
		console.log("enter into submitDlyRt funciton = "+dlyRtForm.$invalid);
		if(dlyRtForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			
			var map = {
					"brhId"    : $scope.branch.branchId,
					"cnmtCode" : $scope.cnmt,
					"allow"    : $scope.dlyRt
			};
			
			var response = $http.post($scope.projectName+'/submitDlyRt',map);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.brhList = [];
					$scope.cnmtList = [];
					
					$scope.branch = {};
					$scope.cnmt = "";
					$scope.dlyRt = false
					
					$scope.getBranch();
				}else{
					console.log("error in submit Daily Rate");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	}else if($scope.logoutStatus === true){
			 $location.path("/");
	}else{
			 console.log("****************");
	}
}]);