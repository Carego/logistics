'use strict';

var app = angular.module('application');

app.controller('UserCntlr',['$scope','$rootScope','$location','$http','$window','AuthorisationService',
                                 function($scope,$rootScope,$location,$http,$window,AuthorisationService){
	
	$scope.message = "";
	
	
	$scope.login = function(user) {
		console.log("enter into login function");
		/*var user = {
				"userCode" : "",
				"userName" : $scope.userName,
				"password" : $scope.password,
				"isAdmin"  : ""	
		};*/
		console.log("------------------->");
			var response = $http.post($scope.projectName+'/user', user);
			response.success(function(data, status, headers, config) {
				console.log("data-----"+data.targetPage)
				console.log("message----"+data.message);
				if(data.message === "incorrect password" || data.message === "User not exist"){
					$scope.successToast(data.message);
					user.userName = "";
					user.password = "";
				}else if(data.message === "this user has been inactivate by Super-Admin" || data.message === "User Authentication fail"){
					$scope.successToast(data.message);
					user.userName = "";
					user.password = "";
				}else if(data.message === "already logged in"){
					console.log(data.targetPage);
					$scope.alertToast("already logged in")
					if(data.targetPage === "superAdmin"){
						$location.path("/superAdmin");
						$scope.userRole();
					}else if(data.targetPage === "admin"){
						$location.path("/admin");
						$scope.userRole();
					}else if(data.targetPage === "operator"){
						$location.path("/operator");
						$scope.userRole();
					}else{
						$scope.alertToast("Internal Server Error");
					}
				}else if(data.message === "ERROR"){
					$scope.alertToast("Internal Server Error");
				}else{
					//$window.location.href = "/MyLogistics/"+data.targetPage;
					if(data.targetPage === "operator"){
						$scope.userRole();
						//$scope.setUserRights(data.list);
						AuthorisationService.setUser(data.currentUser);
						localStorage.setItem('userRole', AuthorisationService.getUserRole());
						//$localStorage.currentUserRole = AuthorisationService.getUserRole();
						$rootScope.authenticated = true;
						$location.path("/operator");
					}	
					else if(data.targetPage === "admin"){
						$scope.userRole();
						AuthorisationService.setUser(data.currentUser);
						localStorage.setItem('userRole', AuthorisationService.getUserRole());
						//$localStorage.currentUserRole = AuthorisationService.getUserRole();
						$rootScope.authenticated = true;
						$location.path("/admin");			
					}	
					else if(data.targetPage === "superAdmin"){
						$scope.userRole();
						AuthorisationService.setUser(data.currentUser);
						localStorage.setItem('userRole', AuthorisationService.getUserRole());
						//$localStorage.currentUserRole = AuthorisationService.getUserRole();
						$rootScope.authenticated = true;
						$location.path("/superAdmin");
					}	
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}	
	
	
	$scope.checkAuthorisation = function(){
		console.log("enter into checkAuthorisation function");
		var currentUser = AuthorisationService.getUser();
		console.log("currentUser ------>"+currentUser);
		console.log("currentUser.userRole ------>"+currentUser.userRole);
		if(angular.isUndefined(currentUser.userRole) || angular.isUndefined(currentUser)){
			$scope.alertToast("Please login");
		}else{
			if(currentUser.userRole === 11){
				console.log("currentUser is superadmin --------------");
				$scope.userRole();
				$location.path("/superAdmin");
			}else if(currentUser.userRole === 22){
				$scope.userRole();
				$location.path("/admin");		
			}else if(currentUser.userRole === 33){
				$scope.userRole();
				$location.path("/operator");
			}else{
				$scope.alertToast("unauthorised person");
				console.log("unauthorised person");
			}
		}
	}
	
	$scope.checkAuthorisation();
	
}]);