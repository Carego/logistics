'use strict';

var app = angular.module('application');

app.controller('SuperAdminCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.message = "welcome SuperAdmin";
	$scope.userList = {};
	$scope.branchList = {};
	$scope.hbosn = {};
	$scope.HBOSNList = [];
	$scope.employeeList = [];
	$scope.finalEmpList = [];
	$scope.statusOptions =["active","inactive"];
	$scope.roleOptions = ["Admin","Super Admin","Operator"];
	$scope.user = {};
	$scope.userBranchName = "";
	$scope.updatedUserList = [];
	$scope.displayBranchFlag = true;
	$scope.displayUserCodeFlag = true;
	$scope.createStnOrderFlag = true;
	$scope.enableUserCode = false;
	$scope.operatorCodeFlag = true;
	/*$scope.clrBranchCode = false;
	$scope.clrEmpCode = false;*/
	
	$scope.superAdmin = function(){
		console.log("enter into superAdmin");
		$scope.NowSuperAdmin();
	}
	
	$scope.admin = function(){
		console.log("enter into admin function");
		$scope.NowAdmin();
	}
	
	$scope.operator = function(){
		console.log("enter into operator function");
		$scope.NowOperator();
	}
	
	$scope.addNewUser = function(UserForm,user){
		console.log("enter into addNewUser1 function---->UserForm.$invalid  = "+UserForm.$invalid);
		if(UserForm.$invalid){
			if(UserForm.userRole.$invalid){
				$scope.alertToast("please select Role Of User");
			}
			if(UserForm.userStatus.$invalid){
				$scope.alertToast("please select User Status");
			}
		}else{
			if(user.userRole === "Super Admin")
				user.userRole = 11;
			else if(user.userRole === "Admin")
				user.userRole = 22;
			else if(user.userRole === "Operator")
				user.userRole = 33;
			
			if(user.userRole === 22 || user.userRole === 33){
				/*if(user.userBranchCode === null || angular.isUndefined(user.userBranchCode) || user.userBranchCode === "" || user.userCode === null || angular.isUndefined(user.userCode) || user.userCode === ""){*/
				if(user.userBranchCode === null || angular.isUndefined(user.userBranchCode) || user.userBranchCode === ""){
					$scope.alertToast("please complete the form");
				}else{
					//user.userRights = ["cnmt","challan","sedr"];
					var response = $http.post($scope.projectName+'/addNewUser', user);
					response.success(function(data, status, headers, config) {
						//$scope.alertToast(data.result);
						$scope.successToast(data.result);
						$scope.fetchUserList();
						//user.userName = "";
						/*user.password = "";
						user.userCodeTemp = "";
						user.userStatus = "";
						user.userRole = "";
						user.userBranchCode = "";*/
						$scope.user = {};
						$scope.userBranchName = "";
						//user.userCode = "";
						$location.path("/superAdmin/addUser");
						/*$scope.clrbranchCode = false;
						$scope.clrempCode = false;*/
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});
				}
			}else{
				console.log("Add a New Super Admin");
				var response = $http.post($scope.projectName+'/addNewUser', user);
				response.success(function(data, status, headers, config) {
					//$scope.alertToast(data.result);
					$scope.successToast(data.result);
					$scope.fetchUserList();
					//user.userName = "";
					user.password = "";
					user.userCode = "";
					user.userStatus = "";
					user.userRole = "";
					user.userBranchCode = "";
					$scope.userBranchName = "";
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
					
				});
			}
		}
	}
	
	
	/*$scope.addNewUser = function(user){
		console.log("enter into addNewUser function-->"+user.userStatus);
		if(user.userRole === "Super Admin")
			user.userRole = 11;
		else if(user.userRole === "Admin")
			user.userRole = 22;
		else if(user.userRole === "Operator")
			user.userRole = 33;
		
		if(user.userRole === null || angular.isUndefined(user.userRole) || user.userRole === ""){
			$scope.alertToast("please select Role Of User");
		}else if(user.userStatus === null || angular.isUndefined(user.userStatus) || user.userStatus === ""){
			$scope.alertToast("please select User Status");
		}else if(user.userRole === 22 || user.userRole === 33){
			if(user.userBranchCode === null || angular.isUndefined(user.userBranchCode) || user.userBranchCode === "" || user.userCode === null || angular.isUndefined(user.userCode) || user.userCode === ""){
				$scope.alertToast("please complete the form");
			}else{
				var response = $http.post($scope.projectName+'/addNewUser', user);
				response.success(function(data, status, headers, config) {
					//$scope.alertToast(data.result);
					$scope.successToast(data.result);
					$scope.fetchUserList();
					user.userName = "";
					user.password = "";
					user.userCode = "";
					user.userStatus = "";
					user.userRole = "";
					user.userBranchCode = "";
					$scope.userBranchName = "";
					$location.path("/superAdmin/addUser");
					$scope.clrbranchCode = false;
					$scope.clrempCode = false;
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast(data);
				});
			}
		}else{
			console.log("Add a New Super Admin");
			var response = $http.post($scope.projectName+'/addNewUser', user);
			response.success(function(data, status, headers, config) {
				//$scope.alertToast(data.result);
				$scope.successToast(data.result);
				$scope.fetchUserList();
				user.userName = "";
				user.password = "";
				user.userCode = "";
				user.userStatus = "";
				user.userRole = "";
				user.userBranchCode = "";
				$scope.userBranchName = "";
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast(data);
			});
		}
	}*/
	
		$scope.fetchUserList = function(){
		console.log("enter into fetchUserList function");
		var response = $http.get($scope.projectName+'/fetchUserList');
		response.success(function(data, status, headers, config) {
			$scope.userList = data.result;
			$scope.getHOSCnmtMonLast();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.changeStatus = function(singleUser){
		console.log("inside changeStatus singleUser.userStatus = "+singleUser.userStatus);
		console.log("$scope.updatedUserList = "+$scope.updatedUserList);
		for(var i=0;i < $scope.updatedUserList.length;i++){
			if($scope.updatedUserList[i].userId === singleUser.userId){
				$scope.updatedUserList.splice(i,1);
			}
		}
		$scope.updatedUserList.push(singleUser);
		console.log("$scope.updatedUserList = "+$scope.updatedUserList);
	}
	
	$scope.selectUserRole = function(role){
		console.log("enter into selectUserRole function --->"+role);
		if(role === "Admin" || role === "Operator"){
			console.log("**************");
			$scope.fetchAllBranchAndEmployee();
			//$scope.fetchAllBranch();
		}else if(role === "Super Admin"){
			$scope.enableUserCode = false;
			$scope.user.userCode = "";
			$scope.user.userBranchCode = "";
			$scope.userBranchName = "";
			
		}	
	}
	
	$scope.submit = function(){
		console.log("enter into submit function");
		var response = $http.post($scope.projectName+'/updateUserStatus',$scope.updatedUserList);
		response.success(function(data, status, headers, config) {
			console.log(data.result);
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.fetchAllBranchAndEmployee = function(){
		console.log("enter into fetchAllBranch function");
		var response = $http.get($scope.projectName+'/fetchAllBranchAndEmployee');
		response.success(function(data, status, headers, config){
			if(data.message === "success"){
				$scope.branchList = data.bList;
				$scope.employeeList = data.eList;
			}	
			console.log("branch list from server---->"+$scope.branchList);
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	/*$scope.fetchAllBranch = function(){
		console.log("enter into fetchAllBranch function");
		var response = $http.get('/MyLogistics/fetchAllBranch');
		response.success(function(data, status, headers, config){
			if(data.message === "success")
				$scope.branchList = data.list;
			console.log("branch list from server---->"+$scope.branchList);
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	$scope.fetchAllEmployee = function(){
		console.log("enter into fetchAllEmployee function");
		var response = $http.get('/MyLogistics/fetchAllEmployee');
		response.success(function(data, status, headers, config){
			if(data.message === "success")
				$scope.employeeList = data.list;
			console.log("branch list from server---->"+$scope.branchList);
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}*/
	
	$scope.displayBranch = function(){
		console.log("enter into displayBranch function");
		$scope.displayBranchFlag = false;
		$('div#displayBranchList').dialog({
            autoOpen: false,
            modal:true,
            resizable: false,
            draggable: true
        });
            
        $('div#displayBranchList').dialog('open');
	}
	
	$scope.selectBranch = function(branch){
		console.log("enter into selectBranch function-->"+branch.branchName);
		$scope.userBranchName = branch.branchName;
		$scope.user.userBranchCode = branch.branchCode;
		
		/*for(var i=0;i<$scope.finalEmpList.length;i++){
			console.log("remove all the elements");
			$scope.finalEmpList.splice(i,1);
		}*/
		var List = [];
		
		for(var i=0;i<$scope.employeeList.length;i++){
			//if($scope.employeeList[i].branchCode === $scope.user.userBranchCode){
				List.push($scope.employeeList[i]);
			//}
		}
		$scope.finalEmpList = List;
		$scope.enableUserCode = true;
		 $('div#displayBranchList').dialog('close');
         $scope.displayBranchFlag = true;   
	}
	
	$scope.selectUserCode = function(employee){
		console.log("enter into selectUserCode function-->"+employee.empCode);
		$scope.user.userCode = employee.empCode;
		$scope.user.userCodeTemp = employee.empCodeTemp;
		$scope.displayUserCodeFlag = true;
		$('div#displayUserCodeList').dialog('close'); 
	}
	
	$scope.displayUserCode = function(){
		console.log("enter into displayUserCode function");
		$scope.displayUserCodeFlag = false;
		$('div#displayUserCodeList').dialog({
            autoOpen: false,
            modal:true,
            resizable: false,
            draggable: true
            });
            
        $('div#displayUserCodeList').dialog('open');
	}
	
	
	$scope.getHOSCnmtMonLast = function(){
		console.log("enter into getHOSCnmtMonLast function");
		
		var response = $http.post($scope.projectName+'/getHOSCnmtMonLast');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("from server ------>data.daysLast"+data.daysLast);
				$scope.cnmtDaysLast = data.daysLast;
				console.log("from server ------>data.monthsLast"+data.monthsLast);
				$scope.cnmtMonthsLast = data.monthsLast;
				$scope.getHOSAvgMonUsgCNMT();
			}else{
				$scope.getHOSAvgMonUsgCNMT();
			}	
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	$scope.getHOSAvgMonUsgCNMT = function(){
		console.log("enter into getHOSAvgMonUsgCNMT function");
		var response = $http.post($scope.projectName+'/getHOSAvgMonUsgCNMT');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("from server ------>data.average"+data.average);
				$scope.cnmtAvgMonUsg = data.average;
				$scope.getHOSChlnMonLast();
			}else{
				$scope.getHOSChlnMonLast();
			}
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	$scope.getHOSChlnMonLast = function(){
		console.log("enter into getHOSChlnMonLast function");
		var response = $http.post($scope.projectName+'/getHOSChlnMonLast');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("from server ------>data.daysLast"+data.daysLast);
				$scope.chlnDaysLast = data.daysLast;
				console.log("from server ------>data.monthsLast"+data.monthsLast);
				$scope.chlnMonthsLast = data.monthsLast;
				$scope.getHOSAvgMonUsgCHLN();
			}else{
				$scope.getHOSAvgMonUsgCHLN();
			}	
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	$scope.getHOSAvgMonUsgCHLN = function(){
		console.log("enter into getHOSAvgMonUsgCHLN function");
		var response = $http.post($scope.projectName+'/getHOSAvgMonUsgCHLN');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("from server ------>data.average"+data.average);
				$scope.chlnAvgMonUsg = data.average;
				$scope.getHOSSedrMonLast();
			}else{
				$scope.getHOSSedrMonLast();
			}	
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	$scope.getHOSSedrMonLast = function(){
		console.log("enter into getHOSSedrMonLast function");
		var response = $http.post($scope.projectName+'/getHOSSedrMonLast');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("from server ------>data.daysLast"+data.daysLast);
				$scope.sedrDaysLast = data.daysLast;
				console.log("from server ------>data.monthsLast"+data.monthsLast);
				$scope.sedrMonthsLast = data.monthsLast;
				$scope.getHOSAvgMonUsgSEDR();
			}else{
				$scope.getHOSAvgMonUsgSEDR();
			}	
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	$scope.getHOSAvgMonUsgSEDR = function(){
		console.log("enter into getHOSAvgMonUsgSEDR function");
		var response = $http.post($scope.projectName+'/getHOSAvgMonUsgSEDR');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("from server ------>data.average"+data.average);
				$scope.sedrAvgMonUsg = data.average;
				$scope.getHBOSNForSA();
			}else{
				$scope.getHBOSNForSA();
			}	
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
	}
	
	
	$scope.createStnOrder = function(){
		console.log("enter into createStnOrder function");
		$scope.createStnOrderFlag = false;
		$('div#openStnOrderDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: [350,200],
			draggable: true,
		    close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
		$('div#openStnOrderDB').dialog('open');
	}
	
	
	$scope.saveStnOrder = function(hbosn,stnOrderForm){
		console.log("enter into saveStnOrder function---->hbosn.hBOSN_opCode " +hbosn.hBOSN_opCode);
		$scope.createStnOrderFlag = true;
		$('div#openStnOrderDB').dialog('close');
		if(stnOrderForm.$invalid){
			$scope.alertToast("please fill the correct entry");
		}else{
			var response = $http.post($scope.projectName+'/saveStnOrder',hbosn);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast(data.result);
					$scope.getHBOSNForSA();
				}	
			});
			response.error(function(data, status, headers, config){
				$scope.errorToast(data);
			});
		}
	}
	
	$scope.selectOperatorCode = function(){
		console.log("enter into selectOperatorCode function");	
		var response = $http.post($scope.projectName+'/getOperatorCode');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.operatorList = data.empList;
				console.log("employee list from server === > "+data.empList);
				$scope.operatorCodeFlag = false;
				$('div#selectOperatorCodeModel').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
			            $(this).dialog('destroy');
			            $(this).hide();
			        }
				});
				$('div#selectOperatorCodeModel').dialog('open');
				
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
	    });
	}
	
	
	$scope.selectOperator = function(empCode){
		console.log("enter into selectOperator function");	
		$scope.hbosn.hBOSN_opCode = empCode;
		$scope.operatorCodeFlag = true;
		$('div#selectOperatorCodeModel').dialog('close');
	}
	
	
	$scope.getHBOSNForSA = function(){
		console.log("enter into getHBOSNData function");
		var response = $http.post($scope.projectName+'/getHBOSNForSA');
		response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				  $scope.HBOSNList = data.HBOSNList;
				  console.log("size of HBOSNList = "+$scope.HBOSNList.length);
			   }else{
				   console.log(data);   
			   }
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		});
	}
	
	
	$scope.confirmOrder = function(hbosn){
		console.log("enter into confirmOrder function");
		if(hbosn.hBOSN_hasRec === "not received"){
			$scope.alertToast("Stationary order is not received by "+hbosn.hBOSN_opCode);
		}else{
			var response = $http.post($scope.projectName+'/confirmOrder',hbosn);
			response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   $scope.alertToast(data.result);
					   $scope.getHBOSNForSA();
				   }else{
					   console.log(data);   
				   }
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			});
		}
	}
	
	
	$scope.downloadExl = function(){
		console.log("enter into downloadExl function");
	/*	var response = $http.get($scope.projectName+'/downloadExl');
			response.success(function(data, status, headers, config){
				  console.log("success ===> "+data);
				var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
				    var objectUrl = URL.createObjectURL(blob);
				    $window.open(objectUrl);
				  
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast("error");
			});*/
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "UserReport.xls");
	} 
	
	
	
	  
	console.log("Status--->>>>>> "+$scope.superAdminLogin);
	 if($scope.superAdminLogin === true){
		 $scope.fetchUserList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	 	
	
	
	
	//$scope.fetchAllBranch();
}]);