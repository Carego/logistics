'use strict';

var app = angular.module('application');

app.controller('TrafficOffCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("TrafficOffCntlr Started");
	
	$scope.branchDBFlag = true;
	$scope.enquiryTable = true;
	
	$scope.trafficOff= {};
	$scope.trafficOffList = [];
	$scope.trafficOfficerNames = [];
	
	$scope.submitTrafficOffForm = function(Form){		
		console.log("Enter into submitTrafficOffForm()");
		var response = $http.post($scope.projectName + '/getLoadingDt', $scope.trafficOff);
		response.success(function(data, status, config, headers){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.enquiryTable = false;
				$scope.alertToast(data.result);
				$scope.trafficOffList = data.list;					
			}else{
				$scope.enquiryTable = true;
				$scope.trafficOffList = [];
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, config, headers){
			console.log("Error in hitting /getLoadingDt....");
		});
		console.log("Exit from submitTrafficOfForm()");
	}
	
	$scope.getBranchData = function(){
		console.log("Enter into getBranchData() ");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){				
				$scope.branchList = data.list;
				$scope.getTrafficOfficers();
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers,config) {
			console.log("Error in hitting /getBranchDataForCustomer");
		});
		console.log("Exit from getBranchData() ");
	}
	
	$scope.getTrafficOfficers = function(){
		console.log("Enter into getTrafficOfficers()");
		var response = $http.post($scope.projectName + '/getTrafficOfficers');
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.trafficOfficerNames = data.list;
			}else{
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getTrafficOfficers");
		});
		console.log("Exit from getTrafficOfficers()");
	}
	
	$scope.saveBranch = function(branch){
		console.log("Enter into saveBranch()");
		$scope.branchFaCode = branch.branchFaCode;
		$scope.trafficOff.branchCode = branch.branchCode;
		$('div#branchDB').dialog('close');
		console.log("Exit from saveBranch()");
	}
	
	$scope.OpenBranchDB = function(){
		console.log("Enter into OpenBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#branchDB').dialog('open');
		console.log("Exit from OpenBranchDB()");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchData();
	}else if($scope.logoutStatus === true){	
		$location.path("/");
	}else{	
		console.log("****************");
	}	
	console.log("TrafficOffCntlr Ended");
}]);