'use strict';

var app = angular.module('application');

app.controller('PayLhpvBalanceCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){


	$scope.excelBtn=true;
	$scope.excelPetroBtn=true;
	$scope.submitDisableFlag=false;


	 $scope.generateExcel=function(excelType){
		 console.log("Hello **************************");
		 var excelData={
				 "date":$scope.fromDt,
				 "excelData":excelType,
		 }
	var response=$http.post($scope.projectName+'/fetchLhpvBalData',excelData);
		 response.success(function(data,status,headers,config){
				if (data.result === "success") {
					console.log(data);
					$scope.alertToast(data.result);
					if(data.excelType === "B"){
						$scope.excelBtn=false;
						$scope.excelPetroBtn=true;
					}else if(data.excelType === "P"){
						$scope.excelPetroBtn=false;
						$scope.excelBtn=true;
					}
						
					$scope.submitDisableFlag=true;
					
				} else {
					$scope.alertToast(data.result);
				}
			 
		 });
			response.error(function(data, status, headers, config) {
				console.log("error in response getLhpvBal: "+data);
				
			});
		 
	 
	 }

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);