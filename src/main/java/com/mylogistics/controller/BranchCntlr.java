package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonFormat.Value;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ModelService;


@Controller
public class BranchCntlr {
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private UserDAO userDAO; 
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	private ModelService modelService = new ModelService();
	
	//String brName = "";
	
	@RequestMapping(value="/getStationCodeData",method = RequestMethod.POST)
	public @ResponseBody Object getStationCodeData(){
		
		List<Station> stationList= stationDAO.getStationData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(stationList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
  			map.put("list",stationList);
  		}else {
  		  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getListOfEmp",method = RequestMethod.POST)
	public @ResponseBody Object getListOfEmployee(){
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		List<Employee> employeeList = employeeDAO.getAllActiveEmployees();
		
		List<Employee> empList = modelService.getEmployeeList();
		if (!(empList.isEmpty())) {
			employeeList.addAll(empList);
		}
		
		if (!(employeeList.isEmpty())) {
			   map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			   map.put("list",employeeList);
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/submitBranch", method = RequestMethod.POST)
    public @ResponseBody Object submitBranch(@RequestBody Branch branch){
        System.out.println("Entered into submitBranch of Branch Controller---->");
        
        Map<String,String> map = new HashMap<String,String>();
        
        branch.setView(false);
        String faCode=null;
        
        User currentUser = (User)httpSession.getAttribute("currentUser");
        String currentUserCode = currentUser.getUserCode();
        branch.setUserCode(currentUserCode);
        String currentUserBranchCode = currentUser.getUserBranchCode();
        branch.setbCode(currentUserBranchCode);
        		
/*		String oldBrDirector = branch.getBranchDirector();
		String oldBrMarketingHD = branch.getBranchMarketingHD();
		String oldBrOutStandingHD = branch.getBranchOutStandingHD();
		String oldBrMarketing = branch.getBranchMarketing();
		String oldBrMngr = branch.getBranchMngr();
		String oldBrCashier = branch.getBranchCashier();
		String oldBrTraffic = branch.getBranchTraffic();
		String oldBrAreaMngr = branch.getBranchAreaMngr();
		String oldBrRegionalMngr = branch.getBranchRegionalMngr();
		
		map = updateEmployeeBranch(branch); 
		
		if(currentUserCode.equals(oldBrDirector)){
			System.out.println("Entered to change the empCode of oldBrDirector"+oldBrDirector);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrMarketingHD)){
			System.out.println("Entered to change the empCode of oldBrMarketingHD---"+oldBrMarketingHD);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrOutStandingHD)){
			System.out.println("Entered to change the empCode of oldBrOutStandingHD---"+oldBrOutStandingHD);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrMarketing)){
			System.out.println("Entered to change the empCode of oldBrMarketing---"+oldBrMarketing);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrMngr)){
			System.out.println("Entered to change the empCode of oldBrMngr---"+oldBrMngr);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrCashier)){
			System.out.println("Entered to change the empCode of oldBrCashier---"+oldBrCashier);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrTraffic)){
			System.out.println("Entered to change the empCode of oldBrTraffic---"+oldBrTraffic);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrAreaMngr)){
			System.out.println("Entered to change the empCode of oldBrAreaMngr---"+oldBrAreaMngr);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
		if(currentUserCode.equals(oldBrRegionalMngr)){
			System.out.println("Entered to change the empCode of oldBrRegionalMngr---"+oldBrRegionalMngr);
			branch.setbCode(branch.getBranchCode());
			currentUser.setUserBranchCode(branch.getBranchCode());
			httpSession.setAttribute("currentUser",currentUser);
		}
		
               
		List<Employee> empList = modelService.getEmployeeList();
		System.out.println("Size of empList.size()------>"+empList.size());
		for (int i = 0; i < empList.size(); i++) {
			 Employee emp = new Employee();
			 emp = empList.get(i);
			 emp.setbCode(currentUser.getUserBranchCode());
			 emp.setUserCode(currentUser.getUserCode());
			 
			 List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_EMP);
				FAParticular fAParticular = new FAParticular();
				fAParticular=faParticulars.get(0);
				int fapId=fAParticular.getFaPerId();
				String fapIdStr = String.valueOf(fapId); 
				System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
				if(fapId<10){
					fapIdStr="0"+fapIdStr;
					System.out.println("After adding zeroes--"+fapIdStr);
				}
				emp.setfAParticular(fAParticular);
			 
			
			int empId = employeeDAO.saveEmployeeToDB(emp);  
			System.out.println("employee saved with id------------------"+empId);
			empId=empId+100000;
			String subEmpId = String.valueOf(empId).substring(1,6);
			faCode=fapIdStr+subEmpId;
			System.out.println("faCode code is------------------"+faCode);
			emp.setEmpFaCode(faCode);
			int temp= employeeDAO.updateEmployeeToDB(emp);
			if(temp >= 0){
				System.out.println("employee data updated successfully");
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}
		empList.removeAll(empList);
		

		List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_BRANCH);
		FAParticular fAParticular = new FAParticular();
		fAParticular=faParticulars.get(0);
		int fapId=fAParticular.getFaPerId();
		String fapIdStr = String.valueOf(fapId); 
		System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
		if(fapId<10){
			fapIdStr="0"+fapIdStr;
			System.out.println("After adding zeroes--"+fapIdStr);
		}
		branch.setfAParticular(fAParticular);*/
		
        int branchId=branchDAO.saveBranchToDB(branch);
        
        if(branchId > 0){
        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }else{
        	map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
        }
        
   /*     if (branchId>=0) {
        	System.out.println("branch saved with id------------------"+branch.getBranchId());
        	branchId = branchId+100000;
			String subEmpId = String.valueOf(branchId).substring(1,6);
			faCode=fapIdStr+subEmpId;
			System.out.println("faCode code is------------------"+faCode);
			branch.setBranchFaCode(faCode);
			int temp= branchDAO.updateBranchToDB(branch);
			if(temp >= 0){
				System.out.println("branch data updated successfully");
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			FAMaster fAMaster = new FAMaster();
			fAMaster.setFaMfaCode(faCode);
			fAMaster.setFaMfaType(ConstantsValues.FAP_BRANCH);
			fAMaster.setFaMfaName(branch.getBranchName());
			fAMaster.setUserCode(currentUser.getUserCode());
			fAMaster.setbCode(currentUser.getUserBranchCode());
			
			int tmp = faMasterDAO.saveFaMaster(fAMaster);
			if(tmp >= 0){
				System.out.println("FAMaster is updated successfully for branch");
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
					
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
        return map;
    }
	
	
	
	public Map<String, String> updateEmployeeBranch(Branch branch){
		
		System.out.println("enter into updateEmployeeBranch function of BranchCntlr*****");
		
		String branchCode = branch.getBranchCode();
		String branchDirectorEmpCode = branch.getBranchDirector();
		String branchMarketingHDEmpCode = branch.getBranchMarketingHD();
		String branchOutStandingHDEmpCode = branch.getBranchOutStandingHD();
		String branchMarketingEmpCode = branch.getBranchMarketing();
		String branchMngrEmpCode = branch.getBranchMngr();
		String branchCashierEmpCode = branch.getBranchCashier();
		String branchTrafficEmpCode = branch.getBranchTraffic();
		String branchAreaMngrEmpCode = branch.getBranchAreaMngr();
		String branchRegionalMngrEmpCode = branch.getBranchRegionalMngr();
		
		Map<String, String> map = new HashMap<String, String>();
		if(branchDirectorEmpCode != null){
		   int temp = employeeDAO.updateEmployee(branchDirectorEmpCode,branchCode);
		    
		    if(temp >= 0) {
		    	map.put(ConstantsValues.RESULT+"bDirector",ConstantsValues.SUCCESS+"bDirector");
		    	int tmp=userDAO.updateUser(branchDirectorEmpCode,branchCode);
		    	if(tmp>=0){
		    		System.out.println("User table is updated for brDirector-----");
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
		    }else {
				map.put(ConstantsValues.RESULT+"bDirector", ConstantsValues.ERROR+"bDirector");
			}
		}   
		   
		if(branchMarketingHDEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchMarketingHDEmpCode,branchCode);
			 if(temp >= 0){
		    	map.put(ConstantsValues.RESULT+"bMarketingHD",ConstantsValues.SUCCESS+"bMarketingHD");
		    	int tmp=userDAO.updateUser(branchMarketingHDEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
			} else {
				map.put(ConstantsValues.RESULT+"bMarketingHD", ConstantsValues.ERROR+"bMarketingHD");
			}
		} 
		   
		if(branchOutStandingHDEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchOutStandingHDEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchOutStandingHDEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bOutHD",ConstantsValues.SUCCESS+"bOutHD");
			} else {
				map.put(ConstantsValues.RESULT+"bOutHD", ConstantsValues.ERROR+"bOutHD");
			}
		}
		
		if(branchMarketingEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchMarketingEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchMarketingEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bMarketing",ConstantsValues.SUCCESS+"bMarketing");
			} else {
				map.put(ConstantsValues.RESULT+"bMarketing", ConstantsValues.ERROR+"bMarketing");
			}
	}
		
		if(branchMngrEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchMngrEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchMngrEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bMngr",ConstantsValues.SUCCESS+"bMngr");
			} else {
				map.put(ConstantsValues.RESULT+"bMngr", ConstantsValues.ERROR+"bMngr");
			}
		}
		
		if(branchCashierEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchCashierEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchCashierEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bCashier",ConstantsValues.SUCCESS+"bCashier");
			} else {
				map.put(ConstantsValues.RESULT+"bCashier", ConstantsValues.ERROR+"bCashier");
			}
		}
		
		if(branchTrafficEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchTrafficEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchTrafficEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bTraffic",ConstantsValues.SUCCESS+"bTraffic");
			} else {
				map.put(ConstantsValues.RESULT+"bTraffic", ConstantsValues.ERROR+"bTraffic");
			}
		}
		
		if(branchAreaMngrEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchAreaMngrEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchAreaMngrEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bAreaMngr",ConstantsValues.SUCCESS+"bAreaMngr");
			} else {
				map.put(ConstantsValues.RESULT+"bAreaMngr", ConstantsValues.ERROR+"bAreaMngr");
			}
		}
		
		if(branchRegionalMngrEmpCode != null){
			int temp = employeeDAO.updateEmployee(branchRegionalMngrEmpCode,branchCode);
			 if(temp >= 0){
				int tmp=userDAO.updateUser(branchRegionalMngrEmpCode,branchCode);
		    	if(tmp>=0){
		    		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    	}else{
		    		map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		    	}
				map.put(ConstantsValues.RESULT+"bRegionalMngr",ConstantsValues.SUCCESS+"bRegionalMngr");
			} else {
				map.put(ConstantsValues.RESULT+"bRegionalMngr", ConstantsValues.ERROR+"bRegionalMngr");
			}
		}
		return map;
	}
	
	
	@RequestMapping(value="/getBranchCode",method = RequestMethod.POST)
	public @ResponseBody Object getBranchCode(@RequestBody String branchName){
	    
		//String brName = branchName;
		String branchCode = "";
		String lastFiveChar = "";
        long rowCount = branchDAO.totalCount();
        System.out.println("Value of rowCount of branch------>"+rowCount);
        Map<String,String> map = new HashMap<String,String>();
	    	
        if(!(rowCount==-1)){
		 
        	if(rowCount==0){
        		lastFiveChar="00001";
        		branchCode = "1";
        	} else{
        		List<Branch> listBranch = branchDAO.getLastBranch();
        		System.out.println("BranchID"+ listBranch.get(0).getBranchId());
		    		
		    	int lastBranchid = listBranch.get(0).getBranchId();
		    	int branchid = lastBranchid+1;
		    	branchCode = String.valueOf(branchid);
		    	int end = 100000+branchid;
		    	lastFiveChar = String.valueOf(end).substring(1,6);		
        	}
        	
        	String branchCodeTemp = CodePatternService.branchCodeGen(branchName,lastFiveChar);
        	map.put("branchCode", branchCode);
        	map.put("branchCodeTemp", branchCodeTemp);
        	map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
        }else{
        	map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
        }
        return map;
	}
	
	@RequestMapping(value = "/branchDetails", method = RequestMethod.POST)
	  public @ResponseBody Object branchDetails(@RequestBody String branchCode){
		    	   	   
	    Branch branch = new Branch();
		List<Branch> list = branchDAO.retrieveBranch(branchCode);
				  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			branch = list.get(0);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("branch",branch);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	 }
	
	@RequestMapping(value = "/getAllBranchesList", method = RequestMethod.POST)
	  public @ResponseBody Object getAllBranchesList(){
		System.out.println("Mapping of getAllBranchCodes in controller....");
		
		List<Branch> branchCodeList = branchDAO.getAllActiveBranches();
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(branchCodeList.isEmpty())) {
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("branchCodeList",branchCodeList);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	 }
	
	@RequestMapping(value = "/submitEmployeeInBranch", method = RequestMethod.POST)
    public @ResponseBody Object submitEmployeeInBranch(@RequestBody Employee employee){
		System.out.println("Mapping of submitEmployeeInBranch in controller....");
				
		Map<String,Object> map = new HashMap<String , Object>();
		
		employee.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		employee.setUserCode(currentUser.getUserCode());
		employee.setbCode(currentUser.getUserBranchCode());
		
		String empCode = "";
		String empName = employee.getEmpName();
		String lastFiveChar = "";
		long totalRows = employeeDAO.totalCount();
		
		List<Employee> list = modelService.getEmployeeList();
		
		if(list.isEmpty()){
			if(!(totalRows == -1)){
		    	 if(totalRows==0){
		    		 lastFiveChar="00001";
		    		 empCode = "1";
				 }else{
					List<Employee> empList =employeeDAO.getLastEmployeeRow();
					System.out.println("EmployeeID is"+ empList.get(0).getEmpId());
				    int lastEmpId = empList.get(0).getEmpId();
					int empId = lastEmpId+1;
					empCode = String.valueOf(empId);
					int end = 100000+empId;
					lastFiveChar = String.valueOf(end).substring(1,6);	
				}
		    	employee.setEmpCode(empCode); 
		    	
		    	/*System.out.println("Entered into branchName by  branchCode---"+brName);
		    	if(!(brName.equals(""))){ 
		    		String empCodeTemp = CodePatternService.empCodeGen(employee,lastFiveChar,brName);
		    		System.out.println("Retutning from CodePatternService--->"+empCodeTemp);
		    		employee.setEmpCodeTemp(empCodeTemp);
		    	}else{
		    		System.out.println("No branch exists");
		    	}*/
		    	
		    	String empCodeTemp = CodePatternService.empCodeGen(employee,lastFiveChar);
	    		System.out.println("Retutning from CodePatternService--->"+empCodeTemp);
	    		employee.setEmpCodeTemp(empCodeTemp);
		    	
		 		modelService.setEmployee(employee);
		 		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("empName", empName);
				map.put("empCode", empCode);
			}	 
		}else if(!(list.isEmpty())){	
				int lenList = list.size()-1;
				Employee emp = list.get(lenList);
				String subEmpCodeTemp = emp.getEmpCodeTemp().substring(7,12);
				int lastEmpId = Integer.parseInt(subEmpCodeTemp);
			 	int empId = lastEmpId+1;
			 	empCode = String.valueOf(empId);
			 	int end = 100000+empId;
			 	lastFiveChar = String.valueOf(end).substring(1,6);
			 	employee.setEmpCode(empCode);
			 	
			 	/*System.out.println("Entered into branchName by  branchCode---"+brName);
				if(!(brName.equals(""))){ 
					String empCodeTemp = CodePatternService.empCodeGen(employee,lastFiveChar,brName);
					System.out.println("Retutning from CodePatternService--->"+empCode);
					employee.setEmpCodeTemp(empCodeTemp);
				}	*/
			 	
			 	String empCodeTemp = CodePatternService.empCodeGen(employee,lastFiveChar);
	    		System.out.println("Retutning from CodePatternService--->"+empCodeTemp);
	    		employee.setEmpCodeTemp(empCodeTemp);
	    		
		 		modelService.setEmployee(employee);
		 		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("empName", empName);
				map.put("empCode", empCode);
		}else{
			    map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
		
	}
	
	@RequestMapping(value="/refreshNewEmpList",method = RequestMethod.POST)
	public @ResponseBody Object refreshNewEmpList(){
		System.out.println("entered into refreshNewEmpList function");
		modelService.removeAllEmployeeList();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;	
	}
	
	@RequestMapping(value = "/getChildBankMstrList", method = RequestMethod.POST)
	public @ResponseBody Object getChildBankMstrList(@RequestBody String branchId){
		System.out.println("Entered into getChildBankMstrList controller...."+branchId);
		Map<String,Object> map = new HashMap<String , Object>();
		
		Branch branch = branchDAO.getBranchBMstrLt(Integer.parseInt(branchId));
		List<BankMstr> bankMstrList = branch.getBankMstrList();
		
		if (!(bankMstrList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("bankMstrList",bankMstrList);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/getBranchNameIdFa", method = RequestMethod.POST)
	public @ResponseBody Object getBranchNameIdFa(){
		
		Map<String,Object> map = new HashMap<String , Object>();
		
		List<Map<String, Object>> branchMapList = new ArrayList<>();
		
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		
		if (!branchList.isEmpty()) {
			for (Branch branch : branchList) {
				Map<String, Object> branchMap = new HashMap<>();
				branchMap.put(BranchCNTS.BRANCH_ID, branch.getBranchId());
				branchMap.put(BranchCNTS.BRANCH_NAME, branch.getBranchName());
				branchMap.put(BranchCNTS.BRANCH_FA_CODE, branch.getBranchFaCode());
				branchMapList.add(branchMap);
			}
		}
		
		if (!branchMapList.isEmpty()) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("branchList",branchMapList);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		branchList.clear();
		return map;	
	}
	
	@RequestMapping(value="/getActiveBrNCI", method=RequestMethod.POST)
	public @ResponseBody Object getActiveBrNCI(){
		
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> branchNCIList = branchDAO.getActiveBrNCI();
		System.out.println("branchList IsEmpty: "+branchNCIList.isEmpty());
		System.out.println("branchList size: "+branchNCIList.size());
		if (!branchNCIList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("branchNCIList", branchNCIList);
		} else {
			map.put(ConstantsValues.RESULT,  ConstantsValues.ERROR);
		}
		
		return map;
	}

}
