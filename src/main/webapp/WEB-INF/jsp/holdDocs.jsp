<div ng-show="superAdminLogin">
	<div>
		<form name="holdDocsForm" ng-submit="holdDocs(holdDocsForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
<div class="row">

			<div class="col s3 input-field">
				<select name="docSer" ng-model="docSer" ng-change="changeDocs(docSer)" required>
					<option value='CN'>CNMT</option>
					<option value='CH'>CHALLAN</option>
					<option value='AR'>SEDR</option>
					<option value='LH'>LHPV</option>
					<option value='BL'>BILL</option>
					<option value='MR'>MR</option>
				</select> <label for="code">SELECT DOCUMENT TYPE:</label>
			</div>
			


			<div class="col s3 input-field">
				<input type="text" id="code" name="code" ng-model="code" ng-blur="checkCode(code)"  required>
				<label for="code">SERIAL NO:</label>
			</div>

			<!-- <div class="col s3 input-field">
				<select name="onHold" ng-model="onHold" ng-change="onHoldF(onHold)" required readonly>
					<option value='true'>HOLD</option>
					<option value='false'>UNHOLD</option>
				</select> <label for="code">HOLD/UNHOLD:</label>
			</div> -->
			
			 <!-- <div class="col s3 input-field" ng-show="holdFlag"> -->
			 <div class="col s3 input-field" >
				<input type="text" name="onHold" ng-model="onHold" required readonly>
				<label for="code">HOLD/UNHOLD:</label>
			</div> 
			
			</div>

			<div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
					<textarea id="textarea" class="materialize-textarea" rows="3"
						cols="92" name="remark" ng-model="remark"  required></textarea>
					<label>Description</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 center">
					<input class="validate" type="submit" value="HOLD" >
				</div>
			</div>

		</form>
	</div>

</div>