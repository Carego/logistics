package com.mylogistics.constants;

public class CustomerRightsCNTS {
	
	public static final String CRH_ID = "crhId";
	
	public static final String CRH_CUST_CODE = "crhCustCode";
	
	public static final String CRH_ADV_ALLOW = "crhAdvAllow";
	
	public static final String CRH_IS_CUSTOMER = "crhIsCustomer";
	
	public static final String CRH_IS_CONSIGNOR = "crhIsConsignor";
	
	public static final String CRH_IS_CONSIGNEE = "crhIsConsignee";
	
	public static final String CRH_ADV_VALID_DT = "crhAdvValidDt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";

}
