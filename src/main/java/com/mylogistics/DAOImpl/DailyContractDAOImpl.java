package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.ContToStnCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.DailyContractCNTS;
import com.mylogistics.constants.RegularContractCNTS;
import com.mylogistics.constants.StationCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;

public class DailyContractDAOImpl implements DailyContractDAO{
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public DailyContractDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveDailyContract(DailyContract dailyContract){
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 int id = (Integer) session.save(dailyContract);
			 transaction.commit();
			 temp =  id;
			 System.out.println("Daily Contract Data is inserted successfully");
			 session.flush();
		 }
		   
		 catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<DailyContract> getDailyContract(String dlyContCode){
				
		 List<DailyContract> result = new ArrayList<DailyContract>();
				 
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(DailyContract.class);
			 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,dlyContCode));
			 result =cr.setMaxResults(1).list();
			 session.flush();
		 }
				 
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return  result; 
	 }
	 
	 	@Transactional
		 public int updateDailyContract(DailyContract dailyContract){
	 		int temp;
	 		Date date = new java.sql.Date(new java.util.Date().getTime());
	 		Calendar calendar = Calendar.getInstance();
	 		calendar.setTime(date);
	 		System.out.println("Updating contract in dao-------"+dailyContract.getDlyContCode()+"  "+dailyContract.getDlyContId());
			 try{
				 session = this.sessionFactory.openSession();
				 Criteria cr=session.createCriteria(DailyContract.class);
				 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE, dailyContract.getDlyContCode()));
				 transaction = session.beginTransaction();
				 dailyContract.setCreationTS(calendar);
				 session.saveOrUpdate(dailyContract);
				 transaction.commit();
				 session.flush();
				 temp= 1;
			 }
			 catch(Exception e){
				 e.printStackTrace();
				 temp= -1;
			 }		
			 session.clear();
			 session.close();
			 return temp;
		 }
		 
		 @Transactional
		 @SuppressWarnings("unchecked")
		 public List<DailyContract> getDlyContractisVerifyNo(){
					
			 List<DailyContract> result = new ArrayList<DailyContract>();
					 
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(DailyContract.class);
				 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_IS_VERIFY,"no"));
				 result =cr.list();
				 transaction.commit();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return  result; 
		 }
		 
		 @SuppressWarnings("unchecked")
		 @Transactional
		 public int updateContractisVerify(String code[]){
			 int temp;
			 Date date = new java.sql.Date(new java.util.Date().getTime());
		 	 Calendar calendar = Calendar.getInstance();
		 	 calendar.setTime(date);
			 try{
				 session = this.sessionFactory.openSession();
				 for(int i=0;i<code.length;i++){
					 String contCodesub = code[i].substring(0, 3);
					 if(contCodesub.equals("dly")){ 
						transaction = session.beginTransaction();
				 		DailyContract dc = new DailyContract();
				 		List<DailyContract> list = new ArrayList<DailyContract>();
				 		Criteria cr=session.createCriteria(DailyContract.class);
						cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,code[i]));
						list =cr.list();
						dc = list.get(0);
						dc.setDlyContIsVerify("yes");
						dc.setCreationTS(calendar);
					 	session.update(dc);
					 	transaction.commit();
					 }
					 else if(contCodesub.equals("reg")){
						 transaction = session.beginTransaction();
						 RegularContract rc = new RegularContract();
						 List<RegularContract> list = new ArrayList<RegularContract>();
						 Criteria cr=session.createCriteria(RegularContract.class);
						 cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,code[i]));
						 list =cr.list();
						 rc = list.get(0);
						 rc.setRegContIsVerify("yes");
						 rc.setCreationTS(calendar);
						 session.update(rc);
						 transaction.commit();
						
					 	}
				 	}
				 
				 session.flush();
				 temp = 1;
			}catch(Exception e){
				 e.printStackTrace();
				 temp = -1;
			 }
			 session.clear();
			 session.close();
			 return temp;
		 }
		 
		 	@SuppressWarnings("unchecked")
			@Transactional
			 public int getLastDlycontractId(){
				 List<DailyContract> list = new ArrayList<DailyContract>();
				 int id=0;
				 try{
					 session = this.sessionFactory.openSession();
					 list = session.createQuery("from DailyContract order by dlyContId DESC").setMaxResults(1).list();
					 id = list.get(0).getDlyContId();
					 session.flush();
				 }
				 catch(Exception e){
					 e.printStackTrace();
				 }
				 session.clear();
				 session.close();
				 return id;
			}
		 	
		 	 @Transactional
			 public long totalCount(){
		 		 long tempCount = -1;
		 		 try {
		 			session = this.sessionFactory.openSession();
		 			tempCount = (Long) session.createCriteria(DailyContract.class).setProjection(Projections.rowCount()).uniqueResult();
		 			session.flush();
		 		 	} 
		 		 catch (Exception e) {
		 		 	e.printStackTrace();
		 		 }
		 		session.clear();
		 		session.close();
	 			return tempCount;
			 }
		 	 
		 	 @SuppressWarnings("unchecked")
		 	 @Transactional
		 	 public List<DailyContract> getDailyContractCode(String custCode){
		 		 List<DailyContract> dlyContList = new ArrayList<DailyContract>();
				 try{
					 session = this.sessionFactory.openSession();
					 transaction = session.beginTransaction();
					 Criteria cr=session.createCriteria(DailyContract.class);
					 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_BLPM_CODE,custCode));
					 dlyContList =cr.list();
					 transaction.commit();
					 session.flush();
				 }
				 catch(Exception e){
					 e.printStackTrace();
				 }
				 session.clear();
				 session.close();
				 return dlyContList;
		 	 }
		 	 
		 	 
		 	 @SuppressWarnings("unchecked")
		 	 @Transactional
		 	 public List<DailyContract> getDailyContForCnmt(String custCode,String cnmtFromSt){
		 		 List<DailyContract> dailyContractList = new ArrayList<DailyContract>();
				 try{
					 session = this.sessionFactory.openSession();
					 transaction = session.beginTransaction();
					 Criteria cr=session.createCriteria(DailyContract.class);
					 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_BLPM_CODE,custCode));
					/* cr.add(Restrictions.ge(DailyContractCNTS.DLY_CONT_START_DT,cnmtDt)); 
		 			 cr.add(Restrictions.lt(DailyContractCNTS.DLY_CONT_END_DT,cnmtDt));*/
					 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_FROM_STATION,cnmtFromSt));
					 dailyContractList =cr.list();
					 transaction.commit();
					 session.flush();
				 }
				 catch(Exception e){
					 e.printStackTrace();
				 }
				 session.clear();
				 session.close();
				 return dailyContractList;
		 	 }
		 	 
		 	 
		 	 
		 	@Transactional
			 @SuppressWarnings("unchecked")
			 public List<DailyContract> getDlyContractisViewFalse(String branchCode){
		 		
				 List<DailyContract> result = new ArrayList<DailyContract>();
						 
				 try{
					 session = this.sessionFactory.openSession();
					 transaction = session.beginTransaction();
					 Criteria cr=session.createCriteria(DailyContract.class);
					 cr.add(Restrictions.eq(DailyContractCNTS.IS_VIEW,false));
					 cr.add(Restrictions.eq(DailyContractCNTS.BRANCH_CODE,branchCode));
					 result =cr.list();
					 transaction.commit();
					 session.flush();
					 System.out.println("After getting the list");
				 }
				 catch(Exception e){
					 e.printStackTrace();
				 }
				 session.clear();
				 session.close();
				 return  result; 
			 }
		 	
			 @SuppressWarnings("unchecked")
			 @Transactional
			 public int updateContractisViewTrue(String code[]){
				 Date date = new java.sql.Date(new java.util.Date().getTime());
			 	Calendar calendar = Calendar.getInstance();
			 	calendar.setTime(date);
				 try{
					 session = this.sessionFactory.openSession();
					 for(int i=0;i<code.length;i++){
						transaction = session.beginTransaction();
					 	DailyContract dc = new DailyContract();
				 		List<DailyContract> list = new ArrayList<DailyContract>();
				 		Criteria cr=session.createCriteria(DailyContract.class);
						cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,code[i]));
						list =cr.list();
						dc = list.get(0);
						dc.setView(true);
						dc.setCreationTS(calendar);
						 session.update(dc);
						 transaction.commit();
					 	}
					 session.flush();
					 session.clear();
					 session.close();
					 return 1;
					 }
				 
				 catch(Exception e){
					 e.printStackTrace();
					 session.clear();
					 session.close();
					 return -1;
				 }
				 
				 
			 }
			 
			 @Transactional
			@SuppressWarnings("unchecked")
			 public List<String> getDailyContractCode(){
					List<String> dlyContList = new ArrayList<String>();
					try{
						session = this.sessionFactory.openSession();
						transaction = session.beginTransaction();
						Criteria cr=session.createCriteria(DailyContract.class);
						ProjectionList projList = Projections.projectionList();
						projList.add(Projections.property("dlyContCode"));
						cr.setProjection(projList);
						dlyContList = cr.list();
						transaction.commit();
						session.flush();
					}
					catch(Exception e){
						e.printStackTrace();
					}
					session.clear();
					session.close();
					return dlyContList;
				}
			 
			 @Transactional
			 @SuppressWarnings("unchecked")
			 public List<String> getDailyContractCodes(String branchCode){
				 List<String> result = new ArrayList<String>();
						 
				 try{
					 session = this.sessionFactory.openSession();
					 transaction = session.beginTransaction();
					 Criteria cr=session.createCriteria(DailyContract.class);
					 cr.add(Restrictions.eq(DailyContractCNTS.BRANCH_CODE,branchCode));
					 ProjectionList projList = Projections.projectionList();
						projList.add(Projections.property("dlyContCode"));
						cr.setProjection(projList);
					 result =cr.list();
					 transaction.commit();
					 session.flush();
				 }
				 catch(Exception e){
					 e.printStackTrace();
				 }
				 session.clear();
				 session.close();
				 return  result; 
			 }
			 
			 @SuppressWarnings("unchecked")
			 @Transactional
			 public List<DailyContract> getDailyContractData(int id){
					 List<DailyContract> list = new ArrayList<DailyContract>();
					try{
						 session = this.sessionFactory.openSession();
						 transaction = session.beginTransaction();
						 Criteria cr=session.createCriteria(DailyContract.class);
						 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_ID,id));
						 list =cr.list();
						 transaction.commit();
						 session.flush();
					}
					catch (Exception e) {
						   e.printStackTrace();
					}
					session.clear();
					session.close();
					return list;
				}
			 
		 @SuppressWarnings("unchecked")
	 	 @Transactional
	 	 public List<DailyContract> getDailyContractData(String contractCode){
			 System.out.println("enter into getDailyContractData function = "+contractCode);
	 		 List<DailyContract> dailyList = new ArrayList<DailyContract>();
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(DailyContract.class);
				 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,contractCode));
				 dailyList =cr.list();
				 transaction.commit();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return dailyList;
	 	 }
		 
		 
		 @SuppressWarnings("unchecked")
	 	 @Transactional
		 public List<String> getDlyFaCode(){
			 System.out.println("enter into getDlyFaCode function");
			 List<String> faList = new ArrayList<String>();
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(DailyContract.class);
				 ProjectionList projList = Projections.projectionList();
				 projList.add(Projections.property(DailyContractCNTS.DLY_CONT_FA_CODE));
				 cr.setProjection(projList);
				 faList = cr.list();
				 
				 session.flush();
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return faList;
		 }
		 
		 

		 @SuppressWarnings("unchecked")
	 	 @Transactional 
		 public List<DailyContract> getDlyContByFaCode(String faCode){
			 System.out.println("enter into getDlyContByFaCode function");
			 List<DailyContract> dlyList = new ArrayList<>();
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(DailyContract.class);
				 cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_FA_CODE,faCode));
				 dlyList = cr.list();
				 
				 session.flush();
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return dlyList;
		 }
		 
		 
		 @SuppressWarnings("unchecked")
	 	 @Transactional 
		 public List<Map<String,String>> getDlyCont(){
			 System.out.println("enter into getDlyCont function");
			 List<Map<String,String>> list = new ArrayList<>();
			 List<RegularContract> regList = new ArrayList<>();
			 List<DailyContract> dlyList = new ArrayList<>();
			 List<Customer> custList = new ArrayList<>();
			 List<Branch> brList = new ArrayList<>();
			 List<Station> stnList =new ArrayList<>();
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(DailyContract.class);
				 dlyList = cr.list();
				 if(!dlyList.isEmpty()){
					 for(int i=0;i<dlyList.size();i++){
						 Map<String,String> dlyMap = new HashMap<String, String>();
						 dlyMap.put("contFaCode",dlyList.get(i).getDlyContFaCode());
						 dlyMap.put("contCode",dlyList.get(i).getDlyContCode());
						 cr=session.createCriteria(Customer.class);
						 cr.add(Restrictions.eq(CustomerCNTS.CUST_CODE,dlyList.get(i).getDlyContBLPMCode()));
						 custList = cr.list();
						 if(!custList.isEmpty()){
							 dlyMap.put("custName",custList.get(0).getCustName());
						 }
						 
						 cr=session.createCriteria(Branch.class);
						 cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,dlyList.get(i).getBranchCode()));
						 brList = cr.list();
						 
						 if(!brList.isEmpty()){
							 dlyMap.put("brhName",brList.get(0).getBranchName());
						 }
						 
						 cr=session.createCriteria(Station.class);
						 cr.add(Restrictions.eq(StationCNTS.STN_CODE, dlyList.get(i).getDlyContFromStation()));
						 stnList = cr.list();
						 
						 if(!stnList.isEmpty()){
							 dlyMap.put("frmStnName" , stnList.get(0).getStnName());
						 }
						 
						 
						 list.add(dlyMap);
					 }
				 }
				 
				 cr=session.createCriteria(RegularContract.class);
				 regList = cr.list();
				 if(!regList.isEmpty()){
					for(int i=0;i<regList.size();i++){
						Map<String,String> regMap = new HashMap<>();
						regMap.put("contFaCode",regList.get(i).getRegContFaCode());
						regMap.put("contCode",regList.get(i).getRegContCode());
						cr=session.createCriteria(Customer.class);
						cr.add(Restrictions.eq(CustomerCNTS.CUST_CODE,regList.get(i).getRegContBLPMCode()));
						custList = cr.list();
						
						 if(!custList.isEmpty()){
							 regMap.put("custName",custList.get(0).getCustName());
						 }
						 
						 cr=session.createCriteria(Branch.class);
						 cr.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,regList.get(i).getBranchCode()));
						 brList = cr.list();
						 
						 if(!brList.isEmpty()){
							 regMap.put("brhName",brList.get(0).getBranchName());
						 }
						 
						 
						 cr=session.createCriteria(Station.class);
						 cr.add(Restrictions.eq(StationCNTS.STN_CODE, regList.get(i).getRegContFromStation()));
						 stnList = cr.list();
						 
						 if(!stnList.isEmpty()){
							 regMap.put("frmStnName" , stnList.get(0).getStnName());
						 }
						 
						 list.add(regMap);
					}
				 }
				 session.flush();
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return list;
		 }

		 
		@SuppressWarnings("unchecked")
	 	@Transactional	
		 public int extDtOfCont(String contFaCode , Date date){
			System.out.println("enter into extDtOfCont function");
	 		int res = 0;
	 		List<DailyContract> dlyList = new ArrayList<>();
	 		try{
	 			session = this.sessionFactory.openSession();
	 			transaction = session.beginTransaction();
 				Criteria cr=session.createCriteria(DailyContract.class);
 				cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_FA_CODE,contFaCode));
 				dlyList = cr.list();
 				
 				
 				if(!dlyList.isEmpty()){
 					DailyContract dlyCont = dlyList.get(0);
 					Date prevDt = dlyCont.getDlyContEndDt();
 					dlyCont.setDlyContEndDt(date);	
 					session.update(dlyCont);
 					
 					List<ContToStn> ctsList = new ArrayList<>();
 					cr=session.createCriteria(ContToStn.class);
 					cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,dlyCont.getDlyContCode()));
 	 				cr.add(Restrictions.eq(ContToStnCNTS.CTS_TO_DT,prevDt));
 	 				ctsList = cr.list();
 					
 					if(!ctsList.isEmpty()){
 						for(int i=0;i<ctsList.size();i++){
 							ContToStn cts = ctsList.get(i);
 							cts.setCtsToDt(date);
 							session.update(cts);
 						}
 					}
 					
 					transaction.commit();
 					res = 1;
 				}
 		
 				session.flush();
	 		}catch(Exception e){
	 			e.printStackTrace();
	 		}
	 		session.clear();
	 		session.close();
	 		return res;
		 }
		
		
		
		@SuppressWarnings("unchecked")
	 	@Transactional	
		public int updateBillBase(String contCode , String billBase){
			System.out.println("enter into updateBillBase function");
			int res = 0;
			List<DailyContract> dlyList = new ArrayList<>();  
			try{
				session = this.sessionFactory.openSession();
	 			transaction = session.beginTransaction();
 				Criteria cr=session.createCriteria(DailyContract.class);
 				cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,contCode));
 				dlyList = cr.list();
 				
 				if(!dlyList.isEmpty()){
 					DailyContract dailyContract = dlyList.get(0);
 					dailyContract.setDlyContBillBasis(billBase);
 					session.update(dailyContract);
 					transaction.commit();
 					res = 1;
 				}
 				session.flush();
 				
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return res;
		}
}
