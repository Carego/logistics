<style type="text/css">
            .printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 12in !important;
                		position: fixed;
                		top: 0in;
                		left: 0in;
                		z-index: 999999;
                		border: 1px solid black;
                		}
                		
                		body {
						    width: 12in;
						    height: 60in;
						    margin-left: 0 !important;
						    padding: 0 !important;
						     
						  }
						  th {
							    text-align:left;
							}
		    }
            @PAGE {
				size: A4 portrait;
				 margin-bottom: 2in;
				 margin-top: 2in;
				 padding-bottom: 2in;
				 padding-top: 2in;
			}
			
			/* .printable{
				border: 1px dotted #CCCCCC;
				padding: 10px;
			} */
			
}
</style>
<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
		<form name="shortExcessForm" ng-submit=shortExcessSubmit(shortExcessForm)>
			<div class="row">
	     		<div class="col s3 input-field">
	       				<input class="validate" type ="date" id="fromDtId" name ="fromDtName" ng-model="fromDt" ng-required="true" >
	       		<label for="code">From Date</label>	
	       		</div>
	       		
	       		<div class="col s3 input-field">
	       				<input class="validate" type ="date" id="toDtId" name ="toDtName" ng-model="toDt"  ng-required="true">
	       			<label for="code">To Date</label>	
	       		</div>
	       		
	       	 <div class="col s4 input-field">
	       				<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="cust.custName" ng-click="openCustDB()" disabled="disabled" ng-required="isCustStar" readonly>
	       			<label for="code">Customer</label>	
	       		</div>
	       		
	       	<!-- 	<div class="col s3 input-field">
	       				<input class="validate" type ="text" id="toFaCodeId" name ="toFaCodeName" ng-model="toFaCode" ng-keyup="openToFaCodeDB()" ng-minlength="0" ng-maxlength="7" ng-required="true">
	       			<label for="code">To Fa Code</label>	
	       		</div>-->
	       		
	       
		    </div>
		    
		    <div class="row">
		    
		    	<div class="col s3 input-field">
					<input class="validate" type="radio" id="consolidateRBId" name="isRBName" ng-click="consolidateRB()" > 
					<label for="code">Consolidate</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="validate" type="radio" id="branchRBId" name="isRBName" ng-click="branchRB()"> 
					<label for="code">Branch</label>
				</div>
				        
				<div class="col s3 input-field">
	       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" disabled="disabled" readonly="readonly">
	       			<label for="code">Branch</label>	
	       		</div>
		    
		    </div>		
		    
			 <div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" id="shortExcessSubmmitId" value="Submit">
      			 		<!-- <input type="button" id="printShortXlsId" value="Print Xls" ng-click="printXls()" ng-show="showTableFlag" disabled="disabled"> -->
      			 	</div>
      		</div>
      		
      		<!-- <div class="row">
      			 	<div class="col s12 center">
      			 		<input type="button" id="printShortExcesId" value="Print Pdf" ng-click="printPdf()" ng-show="showTableFlag" disabled="disabled">
      			 	</div>
      		</div> -->
	       	 
		  </form>
		  <div class="row" ng-show="showPrintFlag">
		  	<div class="col s12 center">
				<form method="post" action="getShortReportXLSX" enctype="multipart/form-data">
					<input type="submit" id="printXlsId" value="Print XLS" disabled="disabled">
			  	</form>
			</div>
		  </div>
    </div>
    <!-- 
    <div id ="frmFACodeDB" ng-hide="frmFACodeDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>From FACode</th>
 	  	  </tr>
		  <tr ng-repeat="fromFaCode in frmFaCodeList | filter:filterTextbox1">
		 	  <td><input type="radio" name="fromFaCode" ng-click="saveFrmFACode(fromFaCode)"></td>
              <td>{{fromFaCode}}</td>
          </tr>
      </table> 
	</div>
	<div id ="toFACodeDB" ng-hide="toFACodeDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>To FACode</th>
 	  	  </tr>
		  <tr ng-repeat="toFaCode in toFaCodeList | filter:filterTextbox2">
		 	  <td><input type="radio" name="toFaCode" ng-click="saveToFACode(toFaCode)"></td>
              <td>{{toFaCode}}</td>
          </tr>
      </table> 
	</div>
    -->
    <div id ="custDB" ng-hide="custDBFlag" class="noprint">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Customer Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="cust in custList | filter:filterTextbox2">
		 	  <td><input type="radio" name="cust" value="{{ cust }}" ng-click="saveCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
    
    
    <div id ="branchDB" ng-hide="branchDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<!-- <div class="row" ng-if="showTableFlag" class="printable" id="exportable"> -->
	<!--  <div class="row" ng-if="false" class="printable" id="exportable">
		<div class="container" style="width: 100% !important;">
			<table class="tblrow" style="width: 100% !important;">
	       		
	       		<tr>
	       			<th class="coltag tblrow" colspan="2">From Dt: {{fromDt | date:'dd-MM-yyyy'}}</th>
	       			<th class="coltag tblrow" colspan="2">To Dt: {{toDt | date:'dd-MM-yyyy'}}</th>
	       			<th class="coltag tblrow" colspan="2">From Fa: {{fromFaCode}}</th>
	       			<th class="coltag tblrow" colspan="2">To Fa: {{toFaCode}}</th>
	       		</tr>
	       		
	       		
	       		<tr>
	  		/****** style="width: 100% !important; margin-left: -5in;" align="left" ****************/
	       	 		<table ng-repeat="faCode in faCodeNameList" >
	       				<tr>
			       			<th class="tblrow" colspan="3">FaCode: {{faCode.csFaCode}}</th>
			       			<th class="tblrow" colspan="5">FaName: {{faCode.csFaName}}</th>
		       			</tr>
		       			
		       				
	       				<tr class="rowclr">
				            <th class="colclr">Bcd</th>
				            <th class="colclr" style="width: 100px">Date</th>
				            <th class="colclr" style="width: 60px">V No.</th>
				            <th class="colclr">Perticular</th>
				            <th class="colclr">TV No.</th>
				            <th class="colclr" style="width: 70px">V Type</th>
				            <th class="colclr">Debit</th>
				            <th class="colclr">Credit</th>
				        </tr>	
				         
				        <tr ng-repeat="cashStmt in cashStmtList" ng-if="cashStmt.csFaCode == faCode.csFaCode">
				            <td class="rowcel">{{cashStmt.branchId}}</td>
				            <td class="rowcel">{{cashStmt.csDt | date:'dd-MM-yy'}}</td>
				            <td class="rowcel">{{cashStmt.csVouchNo}}</td>
				            <td class="rowcel">{{cashStmt.csDescription}}</td>
				            <td class="rowcel">{{cashStmt.csTvNo}}</td>
				            <td class="rowcel">{{cashStmt.csVouchType}}</td>
				            <td class="rowcel" ng-if="cashStmt.csDrCr === 'D'">{{cashStmt.csAmt | number : 2}}</td>
				            <td class="rowcel" ng-if="cashStmt.csDrCr === 'D'"></td>
				            <td class="rowcel" ng-if="cashStmt.csDrCr === 'C'"></td>
				            <td class="rowcel" ng-if="cashStmt.csDrCr === 'C'">{{cashStmt.csAmt | number : 2}}</td>
				        </tr>
		       				
		       			<tr>
		       				<td class="rowcel" colspan="6">BALANCE</td>
		       				<td class="rowcel" ng-if="faCode.csDrCrBalance === 'D'">{{faCode.csAmtBalance | number : 2}}</td>
				            <td class="rowcel" ng-if="faCode.csDrCrBalance === 'D'"></td>
				            <td class="rowcel" ng-if="faCode.csDrCrBalance === 'C'"></td>
				            <td class="rowcel" ng-if="faCode.csDrCrBalance === 'C'">{{faCode.csAmtBalance | number : 2}}</td>
		       			</tr>
		       			
		       			<tr>
		       				<td class="colclr" colspan="6">TOTAL</td>
		       				<td class="colclr" >{{faCode.csAmtTotal | number : 2}}</td>
		       				<td class="colclr" >{{faCode.csAmtTotal | number : 2}}</td>
		       			</tr>
		       			
		       			
	       			</table>
	       		</tr>
	       		
	            
	       	</table>
	    </div>
	          
	</div>-->
	

</div>
