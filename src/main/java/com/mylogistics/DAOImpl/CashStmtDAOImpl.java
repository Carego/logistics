package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.BTBFundTrans;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.BTBFundTransService;
import com.mylogistics.services.CashStmtService;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;

public class CashStmtDAOImpl implements CashStmtDAO {
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;

	public static Logger logger = Logger.getLogger(CashStmtDAOImpl.class);
	
	@Autowired
	public CashStmtDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveCashStmt(CashStmt cashStmt){
		System.out.println("enter into saveCashStmt function->>>>>");
		int temp = -1;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(cashStmt);
			transaction.commit();
			temp= 1;
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int updateCashStmt(int csId){
		System.out.println("enter into updateCashStmt function->"+csId);
		List<CashStmt> csList = new ArrayList<>();
		int temp = -1;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,csId));
			csList = cr.list();
			if(!csList.isEmpty()){
				CashStmt cashStmt = csList.get(0);
				cashStmt.setCsIsClose(true);
				session.merge(cashStmt);
			}
			transaction.commit();
			session.flush();
			temp= 1;
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}
	
	@Override
	public Integer checkCsIsClose(Date date) {
		// TODO Auto-generated method stub
		List<CashStmtStatus> checkCssList = new ArrayList<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		System.out.println("check branchCode="+branchCode);
		System.out.println("checkDate="+date);
		try{
		session = this.sessionFactory.openSession();
		//transaction = session.beginTransaction();
		Criteria cr = session.createCriteria(CashStmtStatus.class);
		cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,date));
		cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,branchCode));
		checkCssList = cr.list();
		System.out.println("size of checkCssList = "+checkCssList.size());
		//transaction.commit();
		session.close();
			}catch(Exception e){
			e.printStackTrace();
			//transaction.commit();
			session.close();
		}
		
		return checkCssList.size();
			
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public int saveFundTrans(BTBFundTransService btbFundTransService , List<Map<String,Object>> toBrhList){
		System.out.println("enter into saveFundTrans function = "+btbFundTransService.getFundDt());
		System.out.println("toBrhList"+toBrhList);
		User currentUser = (User) httpSession.getAttribute("currentUser");
		//String branchCode = currentUser.getUserBranchCode();
		//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		Session session=null;
		Transaction transaction=null;

		session = this.sessionFactory.openSession();
		transaction = session.beginTransaction();
			
		
		//int result = 0;
		int voucherNo = 0;
		try{
			
			List<CashStmtStatus> actCssList = new ArrayList<>();
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,new java.sql.Date(btbFundTransService.getFundDt().getTime())));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
			actCssList = cr.list();
			
			System.out.println("size of actCssList = "+actCssList.size());
			if(!actCssList.isEmpty()){
				
				System.out.println("************** 1");
				CashStmtStatus cashStmtStatus = actCssList.get(0);
						
						
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				
				/*cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);*/
				List<CashStmt> csList = cashStmtStatus.getCashStmtList();
				if(!csList.isEmpty()){
					/*int vNo  = csList.get(csList.size() - 1).getCsVouchNo();
					voucherNo = vNo + 1;*/
					for(int i=0;i<csList.size();i++){
						if(voucherNo < csList.get(i).getCsVouchNo()){
							voucherNo = csList.get(i).getCsVouchNo();
						}
					}
					voucherNo = voucherNo + 1;
				}else{
					voucherNo = 1;
				}
				
				/*List<Integer>  voucherNoList = cr.list();
				
				if(!voucherNoList.isEmpty()){
					int vNo = voucherNoList.get(voucherNoList.size() - 1);
					voucherNo = vNo + 1;
				}else{
					voucherNo = 1;
				}*/
				
				if(!toBrhList.isEmpty()){
					System.out.println("************** 2");
					double totAmt = 0.0;
					for(int i=0;i<toBrhList.size();i++){
						
						System.out.println("************** 3");
						BTBFundTrans btbFundTrans = new BTBFundTrans();
						btbFundTrans.setBtbFrBrCode(btbFundTransService.getFrBrCode());
						btbFundTrans.setBtbFrBnkCode(btbFundTransService.getFrBkCode());
						btbFundTrans.setBtbToBrCode((String) toBrhList.get(i).get("toBrCode"));
						btbFundTrans.setBtbToBnkCode((String) toBrhList.get(i).get("toBkCode"));
						btbFundTrans.setBtbAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
						btbFundTrans.setbCode(currentUser.getUserBranchCode());
						btbFundTrans.setUserCode(currentUser.getUserCode());
						btbFundTrans.setBtbVouchNo(voucherNo);
						btbFundTrans.setBtbCssDt(sqlDate);
						
						session.save(btbFundTrans);
						totAmt = totAmt + Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt")));
					}
					
					//HO Entry
			
					
					/*CashStmt cs = new CashStmt();
					cs.setbCode(currentUser.getUserBranchCode());
					cs.setUserCode(currentUser.getUserCode());
					cs.setCsDescription(btbFundTransService.getDesc());
					cs.setCsDrCr('C');
					cs.setCsAmt(totAmt);
					cs.setCsType("Fund Transfer");
					cs.setCsVouchType("bank");
					//csWTds.setCsPayTo(voucherService.getPayTo());
					cs.setCsTvNo(ConstantsValues.CONST_TVNO);
					cs.setCsFaCode(btbFundTransService.getFrBrCode());
					cs.setCsVouchNo(voucherNo);
					cs.setCsDt(sqlDate);
					
					cr = session.createCriteria(CashStmtStatus.class);
					cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = cr.list();
					
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						cs.setCsNo(csStatus);
						csStatus.getCashStmtList().add(cs);
						session.update(csStatus);
					}*/
					
					
					List<BankMstr> bnkList = new ArrayList<>();
					cr = session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,btbFundTransService.getFrBkCode()));
					bnkList = cr.list();
					
					//boolean chqFlag = false;
					char chqT = 'C';
					
					if(!bnkList.isEmpty()){
						BankMstr bank = bnkList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						bank.setBnkBalanceAmt(balAmt - totAmt);
						session.merge(bank);
						System.out.println("************** 4");
						
						if(!toBrhList.isEmpty()){
							for(int i=0;i<toBrhList.size();i++){
								String chqNo = "";
								chqNo = (String) toBrhList.get(i).get("frChqNo");
								System.out.println("used chq no = "+chqNo);
								if(chqNo != null && chqNo != ""){
									List<ChequeLeaves> chqList = new ArrayList<>();
									/*cr = session.createCriteria(ChequeLeaves.class);
									cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_CHQ_NO,chqNo));
									chqList = cr.list();*/
									
									chqList = bank.getChequeLeavesList();
									System.out.println("size of chqList = "+chqList.size());
									if(!chqList.isEmpty()){
										
										ChequeLeaves chq = null;
										for(int j=0;j<chqList.size();j++){
										
											if(chqList.get(j).getChqLChqNo().equalsIgnoreCase(chqNo) && chqList.get(j).isChqLUsed() == false && chqList.get(j).isChqLCancel() == false){
												chq = chqList.get(j);
												System.out.println("%%%%%%%%%%%%%%%%%%");
												break;
											}
											System.out.println("&&&&&&&& j "+j);
											/*if(chqList.get(j).getBankMstr().getBnkId() == bank.getBnkId()){
												chq = chqList.get(j);
												System.out.println("%%%%%%%%%%%%%%%%%%");
												break;
											}*/
										}
										
										if(chq != null){
											chq.setChqLUsed(true);
											chq.setChqLUsedDt(sqlDate);
											chq.setChqLChqAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
											tvNo = chq.getChqLChqNo()+"000000";
											chqT = chq.getChqLCType();
											session.merge(chq);
										}else{
											System.out.println("************chq = "+chq);
										}
										
										
										/*ChequeLeaves chq = chqList.get(0);
										chq.setChqLUsed(true);
										chq.setChqLUsedDt(sqlDate);
										chq.setChqLChqAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
										
										session.update(chq);*/
										/*chqFlag = true;
										chqT = chq.getChqLCType();
										
										tvNo = "000"+chq.getChqLChqNo();
										
										cs1.setCsChequeType(chqT);
										cs1.setCsTvNo(tvNo);
										session.merge(cs1);*/
									}
								}
							}
						}
					}
					
					
					CashStmt cs1 = new CashStmt();
					cs1.setbCode(currentUser.getUserBranchCode());
					cs1.setUserCode(currentUser.getUserCode());
					cs1.setCsDescription(btbFundTransService.getDesc());
					cs1.setCsDrCr('C');
					cs1.setCsAmt(totAmt);
					cs1.setCsType("Fund Transfer");
					cs1.setCsVouchType("bank");
					//csWTds.setCsPayTo(voucherService.getPayTo());
					cs1.setCsTvNo(ConstantsValues.CONST_TVNO);
					cs1.setCsFaCode(btbFundTransService.getFrBkCode());
					cs1.setCsVouchNo(voucherNo);
					cs1.setCsTvNo(tvNo);
					cs1.setCsDt(sqlDate);
					
					cr = session.createCriteria(CashStmtStatus.class);
					cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = cr.list();
					
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						cs1.setCsNo(csStatus);
						csStatus.getCashStmtList().add(cs1);
						session.update(csStatus);
					}	
					
					
					
					if(!toBrhList.isEmpty()){
						for(int i=0;i<toBrhList.size();i++){
							System.out.println("************** 5");
							CashStmt cs3 = new CashStmt();
							cs3.setbCode(currentUser.getUserBranchCode());
							cs3.setUserCode(currentUser.getUserCode());
							cs3.setCsDescription((String)toBrhList.get(i).get("toDesc"));
							cs3.setCsDrCr('D');
							cs3.setCsAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
							cs3.setCsType("Fund Transfer");
							cs3.setCsVouchType("bank");
							//csWTds.setCsPayTo(voucherService.getPayTo());
							cs3.setCsTvNo(ConstantsValues.CONST_TVNO);
							cs3.setCsFaCode((String) toBrhList.get(i).get("toBrCode"));
							cs3.setCsVouchNo(voucherNo);
							cs3.setCsDt(sqlDate);
							String cheqNo = (String) toBrhList.get(i).get("frChqNo");
							if(cheqNo != null){
								cs3.setCsTvNo(tvNo);
								cs3.setCsChequeType(chqT);
							}else{
								cs3.setCsTvNo("0000000000000");
							}
							/*cs3.setCsTvNo(tvNo);
							if(chqFlag){
								cs3.setCsChequeType(chqT);
							}*/
							
							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cs3.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cs3);
								session.update(csStatus);
							}	
							
							
							/*Date toDate = CodePatternService.getSqlDate(String.valueOf(toBrhList.get(i).get("toDate")));
							System.out.println("toDate ====> "+toDate);*/
							
							List<Branch> toBrList = new ArrayList<>();
							String toBrCode = (String) toBrhList.get(i).get("toBrCode");
							cr = session.createCriteria(Branch.class);
							cr.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE,toBrCode));
							toBrList = cr.list();
							
							if(!toBrList.isEmpty()){
								Branch toBrh = toBrList.get(0);
								List<CashStmtStatus> toBrCssList = new ArrayList<>();
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,sqlDate));
								cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,String.valueOf(toBrh.getBranchId())));
								toBrCssList = cr.list();
								
								if(!toBrCssList.isEmpty()){
									System.out.println("************** 6");
									    CashStmtStatus toBrCss = toBrCssList.get(0);
									
									    int vhNo = 0;
									
										java.sql.Date toSqlDate = new java.sql.Date(toBrCss.getCssDt().getTime());
										
										cr = session.createCriteria(CashStmt.class);
										cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,toSqlDate));
										cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,toBrCss.getbCode()));
										ProjectionList proList1 = Projections.projectionList();
										proList1.add(Projections.property("csVouchNo"));
										cr.setProjection(proList1);
										
										List<Integer>  voucherNoList1 = cr.list();
										
										if(!voucherNoList1.isEmpty()){
											int vNo = voucherNoList1.get(voucherNoList1.size() - 1);
											vhNo = vNo + 1;
										}else{
											vhNo = 1;
										}
										
										
										CashStmt cs4 = new CashStmt();
										if(toBrhList.get(i).get("toBcode")!=null){
											cs4.setbCode((String)toBrhList.get(i).get("toBcode"));
										}else{
										cs4.setbCode(currentUser.getUserBranchCode());
										}
										cs4.setUserCode(currentUser.getUserCode());
										cs4.setCsDescription((String)toBrhList.get(i).get("toDesc"));
										cs4.setCsDrCr('D');
										cs4.setCsAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
										cs4.setCsType("Fund Transfer");
										if(toBrhList.get(i).get("toBcode")!=null){
											cs4.setCsVouchType((String)toBrhList.get(i).get("toBkVouchType"));
										}else{
											cs4.setCsVouchType("bank");
										}
										
										//csWTds.setCsPayTo(voucherService.getPayTo());
										//cs4.setCsBrhFaCode((String) toBrhList.get(i).get("toBrCode"));
										cs4.setCsTvNo(ConstantsValues.CONST_TVNO);
										cs4.setCsFaCode((String) toBrhList.get(i).get("toBkCode"));
										cs4.setCsVouchNo(vhNo);
										cs4.setCsDt(sqlDate);
										//cs4.setCsTvNo(tvNo);
										if(cheqNo != null){
											cs4.setCsTvNo(tvNo);
											cs4.setCsChequeType(chqT);
										}else{
											cs4.setCsTvNo("0000000000000");
										}
										/*cs4.setCsTvNo(tvNo);
										if(chqFlag){
											cs4.setCsChequeType(chqT);
										}*/
										
										//session.save(cs4);
										cr = session.createCriteria(CashStmtStatus.class);
										cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,toBrCss.getCssId()));
										cssList = cr.list();
										
										if(!cssList.isEmpty()){
											CashStmtStatus csStatus = cssList.get(0);
											cs4.setCsNo(csStatus);
											csStatus.getCashStmtList().add(cs4);
											session.update(csStatus);
										}
										
										CashStmt cs5 = new CashStmt();
										if(toBrhList.get(i).get("toBcode")!=null){
											cs5.setbCode((String)toBrhList.get(i).get("toBcode"));
										}else{
										cs5.setbCode(currentUser.getUserBranchCode());
										}
										cs5.setUserCode(currentUser.getUserCode());
										cs5.setCsDescription((String)toBrhList.get(i).get("toDesc"));
										cs5.setCsDrCr('C');
										cs5.setCsAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
										cs5.setCsType("Fund Transfer");
										if(toBrhList.get(i).get("toBcode")!=null){
											cs5.setCsVouchType((String)toBrhList.get(i).get("toBkVouchType"));
										}else{
											cs5.setCsVouchType("bank");
										}
										
										//csWTds.setCsPayTo(voucherService.getPayTo());
										//cs5.setCsBrhFaCode((String) toBrhList.get(i).get("toBrCode"));
										cs5.setCsTvNo(ConstantsValues.CONST_TVNO);
										cs5.setCsFaCode(btbFundTransService.getFrBrCode());
										cs5.setCsVouchNo(vhNo);
										cs5.setCsDt(sqlDate);
										//cs5.setCsTvNo(tvNo);
										if(cheqNo != null){
											cs5.setCsTvNo(tvNo);
											cs5.setCsChequeType(chqT);
										}else{
											cs5.setCsTvNo("0000000000000");
										}
										/*cs5.setCsTvNo(tvNo);
										if(chqFlag){
											cs5.setCsChequeType(chqT);
										}*/
										
										//session.save(cs5);
										cr = session.createCriteria(CashStmtStatus.class);
										cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,toBrCss.getCssId()));
										cssList = cr.list();
										
										if(!cssList.isEmpty()){
											CashStmtStatus csStatus = cssList.get(0);
											cs5.setCsNo(csStatus);
											csStatus.getCashStmtList().add(cs5);
											session.update(csStatus);
										}
										
										List<BankMstr> toBnkList = new ArrayList<>();
										cr = session.createCriteria(BankMstr.class);
										cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,toBrhList.get(i).get("toBkCode")));
										toBnkList = cr.list();
										
										if(!toBnkList.isEmpty()){
											BankMstr bank = toBnkList.get(0);
											double balAmt = bank.getBnkBalanceAmt();
											double addAmt = Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt")));
											bank.setBnkBalanceAmt(balAmt + addAmt);
											session.merge(bank);
										}
									
								}else{
									System.out.println("************** 7");
									
									CashStmtTemp cs4 = new CashStmtTemp();
									if(toBrhList.get(i).get("toBcode")!=null){
										cs4.setbCode((String)toBrhList.get(i).get("toBcode"));
									}else{
									cs4.setbCode(currentUser.getUserBranchCode());
									}
									cs4.setUserCode(currentUser.getUserCode());
									cs4.setCsDescription((String)toBrhList.get(i).get("toDesc"));
									cs4.setCsDrCr('D');
									cs4.setCsAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
									cs4.setCsType("Fund Transfer");
									if(toBrhList.get(i).get("toBcode")!=null){
										cs4.setCsVouchType((String)toBrhList.get(i).get("toBkVouchType"));
									}else{
									cs4.setCsVouchType("bank");
										}
									//csWTds.setCsPayTo(voucherService.getPayTo());
									cs4.setCsBrhFaCode((String) toBrhList.get(i).get("toBrCode"));
									cs4.setCsTvNo(ConstantsValues.CONST_TVNO);
									cs4.setCsFaCode((String) toBrhList.get(i).get("toBkCode"));
									//cs4.setCsVouchNo(vhNo);
									cs4.setCsDt(sqlDate);
									//cs4.setCsTvNo(tvNo);
									if(cheqNo != null){
										cs4.setCsTvNo(tvNo);
										cs4.setCsChequeType(chqT);
									}else{
										cs4.setCsTvNo("0000000000000");
									}
									/*cs4.setCsTvNo(tvNo);
									if(chqFlag){
										cs4.setCsChequeType(chqT);
									}*/
									session.save(cs4);
									
									
									CashStmtTemp cs5 = new CashStmtTemp();
									if(toBrhList.get(i).get("toBcode")!=null){
										cs5.setbCode((String)toBrhList.get(i).get("toBcode"));
									}else{
									cs5.setbCode(currentUser.getUserBranchCode());
									}
									cs5.setUserCode(currentUser.getUserCode());
									cs5.setCsDescription((String)toBrhList.get(i).get("toDesc"));
									cs5.setCsDrCr('C');
									cs5.setCsAmt(Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt"))));
									cs5.setCsType("Fund Transfer");
									if(toBrhList.get(i).get("toBcode")!=null){
										cs5.setCsVouchType((String)toBrhList.get(i).get("toBkVouchType"));
									}else{
									cs5.setCsVouchType("bank");
									}
									//csWTds.setCsPayTo(voucherService.getPayTo());
									cs5.setCsBrhFaCode((String) toBrhList.get(i).get("toBrCode"));
									cs5.setCsTvNo(ConstantsValues.CONST_TVNO);
									cs5.setCsFaCode(btbFundTransService.getFrBrCode());
									//cs5.setCsVouchNo(vhNo);
									cs5.setCsDt(sqlDate);
									//cs5.setCsTvNo(tvNo);
									if(cheqNo != null){
										cs5.setCsTvNo(tvNo);
										cs5.setCsChequeType(chqT);
									}else{
										cs5.setCsTvNo("0000000000000");
									}
									/*cs5.setCsTvNo(tvNo);
									if(chqFlag){
										cs5.setCsChequeType(chqT);
									}*/
									session.save(cs5);
									
									List<BankMstr> toBnkList = new ArrayList<>();
									cr = session.createCriteria(BankMstr.class);
									cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,toBrhList.get(i).get("toBkCode")));
									toBnkList = cr.list();
									
									if(!toBnkList.isEmpty()){
										BankMstr bank = toBnkList.get(0);
										double balAmt = bank.getBnkBalanceAmt();
										double addAmt = Double.valueOf(String.valueOf(toBrhList.get(i).get("toAmt")));
										bank.setBnkBalanceAmt(balAmt + addAmt);
										session.merge(bank);
									}
									
								}
							}
						}
						session.flush();
						session.clear();
						transaction.commit();
						
					}else{
						System.out.println("toBrhList is empty");
					}
				}
			}else{
				System.out.println("There is no css for "+btbFundTransService.getFundDt()+" date");
			}

		}catch(Exception e){
			voucherNo=0;
			e.printStackTrace();
			transaction.rollback();
			logger.info("Exception in btbFundTransfer= "+e);
		}finally{
			session.close();
		}
		return voucherNo;
	}

	@Override
	@Transactional
	public int saveEditCS(CashStmtService cashStmtService) {
		System.out.println("Entered into saveEditCS");
		
		int temp = 0;
		
		try {
			
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			CashStmt cashStmt = (CashStmt) session.get(CashStmt.class, cashStmtService.getCashStmt().getCsId());
			
			List<CashStmt> cashStmtList = cashStmtService.getCashStmtList();
			
			for (int i = 0; i < cashStmtList.size(); i++) {
				if (i==0) {
					cashStmt.setCsFaCode(cashStmtList.get(i).getCsFaCode());
					cashStmt.setCsDescription(cashStmtList.get(i).getCsDescription());
					cashStmt.setCsAmt(cashStmtList.get(i).getCsAmt());
					cashStmt.setCsDrCr(cashStmtList.get(i).getCsDrCr());
					cashStmt.setCsUpdate(true);
					session.merge(cashStmt);
				} else {
						
					CashStmt cs = new CashStmt();
					cs.setbCode(cashStmt.getbCode());
					cs.setCsAmt(cashStmtList.get(i).getCsAmt());
					cs.setCsChequeType(cashStmt.getCsChequeType());
					cs.setUserCode(cashStmt.getUserCode());
					cs.setCsDescription(cashStmtList.get(i).getCsDescription());
					cs.setCsDrCr(cashStmtList.get(i).getCsDrCr());
					cs.setCsDt(cashStmt.getCsDt());
					cs.setCsFaCode(cashStmtList.get(i).getCsFaCode());
					cs.setCsIsClose(cashStmt.isCsIsClose());
					cs.setCsIsVRev(cashStmt.isCsIsVRev());
					cs.setCsMrNo(cashStmt.getCsMrNo());
					cs.setCsPayTo(cashStmt.getCsPayTo());
					cs.setCsSFId(cashStmt.getCsSFId());
					cs.setCsTravIdList(cashStmt.getCsTravIdList());
					cs.setCsTvNo(cashStmt.getCsTvNo());
					cs.setCsType(cashStmt.getCsType());
					cs.setCsVouchNo(cashStmt.getCsVouchNo());
					cs.setCsVouchType(cashStmt.getCsVouchType());
					cs.setCsUpdate(true);
					
					CashStmtStatus csStatus = (CashStmtStatus) session.get(CashStmtStatus.class, cashStmt.getCsNo().getCssId());
					
					cs.setCsNo(csStatus);
					csStatus.getCashStmtList().add(cs);
					session.merge(csStatus);
				}
			}
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	/*@Override
	@Transactional
	public int editCsDesc() {
		System.out.println("CashStmtDAOImpl.editCsDesc()");
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.CS_UPDATE, true));
			
			List<CashStmt> cashStmtList = cr.list();
			
			temp = cashStmtList.size();
			
			System.out.println("cashStmtList size: "+cashStmtList.size());
			
			if (!cashStmtList.isEmpty()) {
				for (int i = 0; i < cashStmtList.size(); i++) {
					System.out.println("desc before: "+cashStmtList.get(i).getCsDescription());
					cashStmtList.get(i).setCsDescription(cashStmtList.get(i).getCsDescription().trim().substring(0, cashStmtList.get(i).getCsDescription().trim().lastIndexOf(" ")));
					System.out.println("desc after: "+cashStmtList.get(i).getCsDescription());
					session.update(cashStmtList.get(i));
				}
			}
			
			transaction.commit();
			session.flush();
		
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}*/
	
	public CashStmt getLastCashStmt(int cssId) {
		CashStmt cashStmt = null;
		
		Criteria cashStmtCriteria = this.sessionFactory.getCurrentSession().createCriteria(CashStmt.class);
		cashStmtCriteria.createAlias(CashStmtCNTS.CS_NO, CashStmtCNTS.CS_NO).add(Restrictions.eq("csNo.cssId", cssId));
		cashStmtCriteria.addOrder(Order.desc(CashStmtCNTS.CS_VOUCH_NO));
		cashStmtCriteria.setMaxResults(1);
		
		List<CashStmt> cashStmts = cashStmtCriteria.list();
		
		if (!cashStmts.isEmpty())
			cashStmt = cashStmts.get(0);
		
		return cashStmt;
	}
	
}
