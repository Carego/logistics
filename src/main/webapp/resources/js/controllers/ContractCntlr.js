'use strict';

var app = angular.module('application');

app.controller('ContractCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	$scope.contract = {};
	$scope.show = "true";
	
	
	
	$scope.Submit = function(contCode){
		console.log("enter into submit function--->");
		$scope.show = "false";
		var response = $http.post('/MyLogistics/contractdetails', contCode);
		response.success(function(data, status, headers, config) {
			$scope.contract = data.contract;	
			
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
}]);