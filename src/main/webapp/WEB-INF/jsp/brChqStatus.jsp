<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row">
		<form name="brChqStatusForm" ng-submit=brChqStatusSubmit(brChqStatusForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s3 input-field">
					<input class="validate" type="text" id="branchId" name="branchName" ng-model="branch.branchName" ng-required="true" ng-click="openBranchDB()" readonly="readonly">
					<label for="code">Branch</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="validate" type="text" id="bankId" name="bankName" ng-model="bank.bnkName" ng-required="true" ng-click="openBankDB()" disabled="disabled" readonly="readonly"> 
					<label for="code">Bank</label>
				</div>
				
				<div class="col s1 input-field">
		   		  <input type="checkbox" id="chqFlag" ng-model="chqUsed" ng-init="chqUsed=false" ng-click="chqCondition()"/>	
		   			<label>Used</label>
		   		</div>
		   		
		   		<div class="col s1 input-field">
		   		  <input type="checkbox" id="chqFlag" ng-model="chqunUsed" ng-init="chqunUsed=false" ng-click="chqunCondition()"/>	
		   			<label>Unused</label>
		   		</div>
		   		
		   		<div class="col s1 input-field">
		   		  <input type="checkbox" id="chqFlag" ng-model="chqCancel" ng-init="chqCancel=false" ng-click="chqCondition()"/>	
		   			<label>Cancel</label>
		   		</div>
		   		
		   		<div class="col s1 input-field">
		   		  <input type="checkbox" id="chqFlag" ng-model="chqAll" ng-init="chqAll=true" ng-click="chqConditionAll()"/>	
		   			<label>All</label>
		   		</div>
				
				<div class="col s3 input-field">
					<input class="col s12 btn teal white-text" type="submit" value="submit">
				</div>
				
			</div>
			
		</form>
	</div>

	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchNCIList | filter:filterTextbox">
		 	  <td><input type="radio"  name="faCodeName" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
          
      </table> 
	</div>
	
	<div id ="bankDB" ng-hide="bankDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank </th>
 	  	  	  <th> Bank A/C No. </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bankNCAIList | filter:filterTextbox">
		 	  <td><input type="radio"  name="faCodeName" value="{{ bnk }}" ng-click="saveBank(bnk)"></td>
              <td>{{bnk.bnkName}}</td>
              <td>{{bnk.bnkAcNo}}</td>
              <td>{{bnk.bnkFaCode}}</td>
          </tr>
      
      </table> 
	</div>
	
	<div class="row" ng-if="chequeLeaveList.length>0">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Cheque Leaves Detail</caption> 
       		
            <tr class="rowclr">
	            <!-- <th class="colclr">SNo</th> -->
	            <th class="colclr">Chq No.</th>
	            <th class="colclr">PTyp</th>
	            <th class="colclr">CTyp</th>
	            <th class="colclr">ChqAmt</th>
	            <th class="colclr">ChqMaxLt</th>
	            <th class="colclr">ChqCncl</th>
	            <th class="colclr">ChqCnclDT</th>
	            <th class="colclr">ChqUsd</th>
	            <th class="colclr">ChqUsdDT</th>
	            <th class="colclr" colspan="2">Action</th>
	        </tr>	
	         
	        <tr ng-repeat="chequeLeave in chequeLeaveList">
	            <td class="rowcel">{{chequeLeave.chqLChqNo}}</td>
	            <td class="rowcel">{{chequeLeave.chqLCType}}</td>
	            <td class="rowcel">
	            	<select ng-model="chequeLeave.chqLChqType">
							<option value='A'>A</option>
							<option value='B'>B</option>
					</select>
	            </td>
	            <td class="rowcel">{{chequeLeave.chqLChqAmt}}</td>
	            <td class="rowcel">
	            	<input type="text" ng-model="chequeLeave.chqLChqMaxLt" size="6">
	            </td>
	            <td class="rowcel">{{chequeLeave.chqLCancel}}</td>
	            <td class="rowcel">{{chequeLeave.chqLChqCancelDt | date:'dd-MM-yy'}}</td>
	            <td class="rowcel">{{chequeLeave.chqLUsed}}</td>
	            <td class="rowcel">{{chequeLeave.chqLUsedDt | date:'dd-MM-yy'}}</td> 
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="updateChq(chequeLeave, $index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeChq(chequeLeave, $index)" >
						<i class="mdi-action-delete white-text"></i>
					</a>
				</td>
	        </tr>
	        <!-- mdi-content-clear -->
         
         	<!-- <tr>
	          	<td colspan="5">
		          	<div class="col s12 center">
		          		<input type="button" id="submitCsId" value="Submit" ng-click="submitCs()">
		          	</div>
	          	</td>
          	</tr> -->
	
    	</table>
          
	</div>
	
	<div id="removeChqDB" ng-hide="removeChqDBFlag" >
		
		<div class="row">
			<div class="col s12 center"><h6 class="white-text">You are going to delete Cheque No. {{chequeLeave.chqLChqNo}}</h6></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="removeChqFinal()">Yes</a>
				<a class="btn white-text" ng-click="cancel()">Cancel</a>
			</div>
		</div>
	</div>

</div>